﻿Imports System.Windows.Forms

Public Class FKKCJZ

    Private Sub FKKCJZ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        FillDG()
    End Sub

    Private Function FillDG() As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgKCJZQJ, "select NY as 年月,ksrq as 起始日期,zzrq as 截止日期,KCJZ AS 库存结账 from FKZQ WHERE NY LIKE '" & FKG.myselfG.NianFen & "%'")
    End Function

    Private Sub JinDu()
        Dim dlgTS As New FKG.JinDu("正在结账，请稍候......", 1)
        dlgTS.ShowDialog()
    End Sub

    Private Sub btnKCJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCJZ.Click
        Dim mdbopen As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Try


            '首先判断是否存在读写冲突
            Dim iT As Integer = 0
            Do While FKG.myselfG.bCanBackup = False
                System.Threading.Thread.Sleep(50)
                iT = iT + 1

                If iT >= 150 Then
                    MsgBox("数据库正在使用当中，没有完成结帐！")
                    Exit Sub
                End If
            Loop

            '禁用自动定时备份
            FKG.myselfG.bCanBackup = False


            Try
                mdbopen.oleConnect.Open()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
                mdbopen.oleConnect.Close()
                '启用自动定时备份
                FKG.myselfG.bCanBackup = True
                Exit Sub
            End Try

            Dim iNY As String
            Dim ds As New DataSet
            'ds = mdb.Reader("select NY from FKZQ where KCJZ='false' and NY like '" & FKG.myselfG.NianFen & "%' order by NY")
            mdbopen.oleCommand.CommandText = "select NY from FKZQ where KCJZ='false' and NY like '" & FKG.myselfG.NianFen & "%' order by NY"
            mdbopen.oleDataAdapter.Fill(ds)

            If ds.Tables(0).Rows.Count = 0 Then
                '启用自动定时备份
                FKG.myselfG.bCanBackup = True
                Exit Sub
            Else
                iNY = ds.Tables(0).Rows(0).Item(0)
                Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

                If iyue <= FKG.myselfG.YueFen Then

                    Dim objTh As New Threading.Thread(AddressOf JinDu)
                    'objTh.Start()

                    '可以进行库存结账
                    'mdb.Write("update FKZQ SET KCJZ='true' WHERE NY='" & FKG.myselfG.NianFen & iyue.ToString.PadLeft(2, "0") & "'")
                    mdbopen.oleCommand.CommandText = "update FKZQ SET KCJZ='true' WHERE NY='" & FKG.myselfG.NianFen & iyue.ToString.PadLeft(2, "0") & "'"
                    mdbopen.oleCommand.ExecuteNonQuery()

                    '生成库存余额表
                    Dim dsKCYE As New DataSet
                    'dsKCYE = mdb.Reader("select 科目,仓库,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from kucun where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目,仓库")
                    mdbopen.oleCommand.CommandText = "select 科目,仓库,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from kucun where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目,仓库"
                    mdbopen.oleDataAdapter.Fill(dsKCYE)

                    Dim i As Integer
                    For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                        Dim dsbExsit As New DataSet
                        mdbopen.oleCommand.CommandText = "select KMDM from KCYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen
                        mdbopen.oleDataAdapter.Fill(dsbExsit)
                        'If mdb.bExsit("select KMDM from KCYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen) = True Then
                        If dsbExsit.Tables(0).Rows.Count > 0 Then
                            'update
                            'mdb.Write("update KCYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(5) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen)

                            mdbopen.oleCommand.CommandText = "update KCYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(5) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen
                            mdbopen.oleCommand.ExecuteNonQuery()

                        Else
                            'insert
                            'mdb.Write("insert into KCYE (kmdm,nian,CangKuID,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & "," & dsKCYE.Tables(0).Rows(i).Item(5) & ")")
                            mdbopen.oleCommand.CommandText = "insert into KCYE (kmdm,nian,CangKuID,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & "," & dsKCYE.Tables(0).Rows(i).Item(5) & ")"
                            mdbopen.oleCommand.ExecuteNonQuery()
                        End If
                    Next

                    '写入非末级科目的余额
                    dsKCYE.Clear()
                    dsKCYE = New DataSet
                    'dsKCYE = mdb.Reader("select KMDM,仓库,sum(借数),sum(借金),sum(贷数),sum(贷金) from (select KMDM, TYE.* from KMDM,(select 科目,仓库,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from kucun where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目,仓库) as TYE where KMDM.HSLBKC='true' AND KMDM.NIAN= " & FKG.myselfG.NianFen & " AND KMDM.SFMJ='false' AND (INSTR(TYE.科目,kmdm.kmdm)=1 )) group by KMDM,仓库")
                    mdbopen.oleCommand.CommandText = "select TA.KMDM AS 代码,TA.仓库 as CK,sum(TA.借数) as JS,sum(TA.借金) AS JJ,sum(TA.贷数) AS DS,sum(TA.贷金) AS DJ from (select KMDM, TYE.* from KMDM,(select 科目,仓库,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from kucun where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目,仓库) as TYE where KMDM.HSLBKC='true' AND KMDM.NIAN= " & FKG.myselfG.NianFen & " AND KMDM.SFMJ='false' AND (charindex(TYE.科目,kmdm.kmdm)=1 )) AS TA group by KMDM,仓库"
                    mdbopen.oleDataAdapter.Fill(dsKCYE)

                    'Dim i As Integer
                    For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                        Dim dsbExsit2 As New DataSet
                        mdbopen.oleCommand.CommandText = "select KMDM from KCYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen
                        mdbopen.oleDataAdapter.Fill(dsbExsit2)
                        'If mdb.bExsit("select KMDM from KCYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen) = True Then
                        If dsbExsit2.Tables(0).Rows.Count > 0 Then
                            'update
                            'mdb.Write("update KCYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(5) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen)
                            mdbopen.oleCommand.CommandText = "update KCYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(5) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and cangkuID =" & dsKCYE.Tables(0).Rows(i).Item(1) & " and Nian=" & FKG.myselfG.NianFen
                            mdbopen.oleCommand.ExecuteNonQuery()

                        Else
                            'insert
                            'mdb.Write("insert into KCYE (kmdm,nian,CangKuID,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & "," & dsKCYE.Tables(0).Rows(i).Item(5) & ")")
                            mdbopen.oleCommand.CommandText = "insert into KCYE (kmdm,nian,CangKuID,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & "," & dsKCYE.Tables(0).Rows(i).Item(5) & ")"
                            mdbopen.oleCommand.ExecuteNonQuery()
                        End If
                    Next


                    '生成委外型号余额表

                    dsKCYE.Clear()
                    dsKCYE = New DataSet
                    'dsKCYE = mdb.Reader("select 科目,对应科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from GongZi where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目,对应科目")

                    ' mdbopen.oleCommand.CommandText = "select 科目,kmdm.kmdm,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from  kmdm left join GongZi on KMDM.Nian=GongZi.年份 and CHARINDEX(kmdm.kmdm,GongZi.对应科目)=1  where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目,kmdm.kmdm"
                    '20240120 weiwaiye 委外型号余额表，只需要计算委外核算的加工户数据，其他的不需要写入该表。

                    'mdbopen.oleCommand.CommandText = "select 科目,kmdm.kmdm,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from  kmdm left join GongZi on KMDM.Nian=GongZi.年份 and CHARINDEX(kmdm.kmdm,GongZi.对应科目)=1  where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & "  and (kmdm.kmdm in (select KMDM from KMDM where Nian=2024 and HSLBWW =1)) group by 科目,kmdm.kmdm"
                    '上面的语句有问题，20250104更新


                    mdbopen.oleCommand.CommandText = "select 科目,kmdm.kmdm,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from  kmdm left join GongZi on KMDM.Nian=GongZi.年份 and CHARINDEX(kmdm.kmdm,GongZi.对应科目)=1  where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & "  and (1=1) group by 科目,kmdm.kmdm"
                    mdbopen.oleDataAdapter.Fill(dsKCYE)
                    'Dim i As Integer
                    For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                        Dim dsbExsit3 As New DataSet
                        mdbopen.oleCommand.CommandText = "select KMDM from WeiWaiYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(1) & "' and WWKHDM ='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and Nian=" & FKG.myselfG.NianFen
                        mdbopen.oleDataAdapter.Fill(dsbExsit3)
                        'If mdb.bExsit("select KMDM from WeiWaiYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(1) & "' and WWKHDM ='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and Nian=" & FKG.myselfG.NianFen) = True Then
                        If dsbExsit3.Tables(0).Rows.Count > 0 Then
                            'update
                            'mdb.Write("update WeiWaiYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(5) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(1) & "' and WWKHDM ='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and Nian=" & FKG.myselfG.NianFen)
                            mdbopen.oleCommand.CommandText = "update WeiWaiYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(5) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(1) & "' and WWKHDM ='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and Nian=" & FKG.myselfG.NianFen
                            mdbopen.oleCommand.ExecuteNonQuery()
                        Else
                            'insert
                            'mdb.Write("insert into WeiWaiYE (kmdm,nian,WWKHDM,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(1) & "'," & FKG.myselfG.NianFen & ",'" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & "," & dsKCYE.Tables(0).Rows(i).Item(5) & ")")
                            mdbopen.oleCommand.CommandText = "insert into WeiWaiYE (kmdm,nian,WWKHDM,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(1) & "'," & FKG.myselfG.NianFen & ",'" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & "," & dsKCYE.Tables(0).Rows(i).Item(5) & ")"
                            mdbopen.oleCommand.ExecuteNonQuery()
                        End If
                    Next
                    ' ''写入非末级科目的余额()
                    'dsKCYE.Clear()

                    'dsKCYE = mdbopen.Reader("select KMDM,对应科目,sum(借数),sum(借金),sum(贷数),sum(贷金) from (select KMDM, TYE.* from KMDM,(select 科目,对应科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from GongZi where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目,对应科目) as TYE where KMDM.HSLBWW='true' AND KMDM.NIAN= " & FKG.myselfG.NianFen & " AND KMDM.SFMJ='false' AND (INSTR(TYE.科目,kmdm.kmdm)=1 )) group by KMDM,对应科目")

                    ''Dim i As Integer
                    'For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                    '    If mdb.bExsit("select KMDM from WeiWaiYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(1) & "' and WWKHDM ='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and Nian=" & FKG.myselfG.NianFen) = True Then
                    '        'update
                    '        mdb.Write("update WeiWaiYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(5) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(1) & "' and WWKHDM ='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and Nian=" & FKG.myselfG.NianFen)

                    '    Else
                    '        'insert
                    '        mdb.Write("insert into WeiWaiYE (kmdm,nian,WWKHDM,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(1) & "'," & FKG.myselfG.NianFen & ",'" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & "," & dsKCYE.Tables(0).Rows(i).Item(5) & ")")
                    '    End If
                    'Next
                    mdbopen.oleConnect.Close() '重要，一定要关闭

                    FillDG()

                    objTh.Abort()

                    '启用自动定时备份
                    FKG.myselfG.bCanBackup = True
                Else
                    '启用自动定时备份
                    FKG.myselfG.bCanBackup = True
                    Exit Sub
                End If
            End If
            '启用自动定时备份
            FKG.myselfG.bCanBackup = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
            MsgBox("因出现异常,本次结帐未成功,请重新记账!", MsgBoxStyle.Information, "提示")

            btnKCFJZ_Click(sender, e)
        Finally
            mdbopen.oleConnect.Close()
        End Try
    End Sub

    Private Sub btnKCFJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCFJZ.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim iNY As String
        Dim ds As DataSet
        ds = mdb.Reader("select NY from FKZQ where KCJZ='true' and NY >= '" & FKG.myselfG.dQiYongRiQi.Year.ToString & FKG.myselfG.dQiYongRiQi.Month.ToString.PadLeft(2, "0") & "' and NY like '" & FKG.myselfG.NianFen & "%' order by NY desc")
        If ds.Tables(0).Rows.Count = 0 Then
            Exit Sub
        Else
            iNY = ds.Tables(0).Rows(0).Item(0)
            Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

            mdb.Write("update KCYE set jfsl" & iyue & "=0,jfje" & iyue & "=0,dfsl" & iyue & "=0,dfje" & iyue & "=0 where  Nian=" & FKG.myselfG.NianFen)
            mdb.Write("update FKZQ SET KCJZ='false' WHERE NY='" & FKG.myselfG.NianFen & iyue.ToString.PadLeft(2, "0") & "'")
            FillDG()
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
