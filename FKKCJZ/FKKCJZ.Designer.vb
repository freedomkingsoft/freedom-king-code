﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKKCJZ
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.dgKCJZQJ = New System.Windows.Forms.DataGridView
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.btnKCJZ = New System.Windows.Forms.Button
        Me.btnKCFJZ = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.dgKCJZQJ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "当年各会计期间"
        '
        'dgKCJZQJ
        '
        Me.dgKCJZQJ.AllowUserToAddRows = False
        Me.dgKCJZQJ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgKCJZQJ.Location = New System.Drawing.Point(28, 122)
        Me.dgKCJZQJ.Name = "dgKCJZQJ"
        Me.dgKCJZQJ.ReadOnly = True
        Me.dgKCJZQJ.RowHeadersVisible = False
        Me.dgKCJZQJ.RowTemplate.Height = 23
        Me.dgKCJZQJ.Size = New System.Drawing.Size(432, 321)
        Me.dgKCJZQJ.TabIndex = 2
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnKCJZ, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnKCFJZ, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btnClose, 0, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(463, 122)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.4!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(107, 321)
        Me.TableLayoutPanel1.TabIndex = 3
        '
        'btnKCJZ
        '
        Me.btnKCJZ.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnKCJZ.Location = New System.Drawing.Point(3, 32)
        Me.btnKCJZ.Name = "btnKCJZ"
        Me.btnKCJZ.Size = New System.Drawing.Size(100, 41)
        Me.btnKCJZ.TabIndex = 0
        Me.btnKCJZ.Text = "库存结账"
        Me.btnKCJZ.UseVisualStyleBackColor = True
        '
        'btnKCFJZ
        '
        Me.btnKCFJZ.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnKCFJZ.Location = New System.Drawing.Point(3, 138)
        Me.btnKCFJZ.Name = "btnKCFJZ"
        Me.btnKCFJZ.Size = New System.Drawing.Size(100, 41)
        Me.btnKCFJZ.TabIndex = 0
        Me.btnKCFJZ.Text = "库存反结账"
        Me.btnKCFJZ.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnClose.Location = New System.Drawing.Point(3, 246)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(100, 41)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "关闭"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("楷体_GB2312", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(24, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(504, 19)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "如果数据较多，结帐可能需要几分钟，请耐心等待..."
        '
        'FKKCJZ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(574, 461)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.dgKCJZQJ)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FKKCJZ"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "库存结账"
        CType(Me.dgKCJZQJ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgKCJZQJ As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnKCJZ As System.Windows.Forms.Button
    Friend WithEvents btnKCFJZ As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
