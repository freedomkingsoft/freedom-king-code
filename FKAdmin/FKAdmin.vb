﻿Public Class FKAdmin

    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '    Me.Close()
    'End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If yanZhengshuru() = False Then
            Exit Sub
        End If

        '复制数据库到指定位置
        Dim sSourse As String
        Dim sDircet As String

        '数据源要设置专用的模板数据库文件
        sSourse = My.Application.Info.DirectoryPath & "\FKData\FreedomKingdata.mdb"
        Dim dlg As New System.Windows.Forms.SaveFileDialog
        dlg.FileName = "FreedomKingData.mdb"
        'dlg.Filter = "MDB"
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sDircet = dlg.FileName
        Else
            Exit Sub
            ' End
        End If

        IO.File.Copy(sSourse, sDircet, True)
        '设置账套基本信息

        Dim mdbS As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim mdbD As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        mdbD.oleConnect.ConnectionString = mdbD.oleConnect.ConnectionString.Replace(mdbD.oleConnect.DataSource, sDircet)

        mdbD.Write("update ZT set QiYeMing='" & Me.zhangtaomingcheng.Text & "',QiYongRiQi='" & Me.qiyongriqi.Value & "',BianHaoWeiShu=" & Me.bianhaoweishu.Text & ",KeMuWeiShu='" & Me.kemujishu.Text & "',NianFenWeiShu=" & Me.nianfen.Checked)

        '导入科目代码
        'If Me.cbDairiKM.Checked Then

        mdbD.Write("delete from KMDM where nian <> (select max(nian) from kmdm)")
        mdbD.Write("update KMDM set Nian=" & Me.qiyongriqi.Value.Year)
        'End If

        '创建FKZQ
        Dim i As Integer
        Dim dateStart As Date
        dateStart = "'" & Me.qiyongriqi.Value.Year & "-1" & "-1'"
        mdbD.Write("delete from FKZQ")
        For i = 1 To 12
            If i < Me.qiyongriqi.Value.Month Then
                mdbD.Write("insert into FKZQ (NY,KSRQ,ZZRQ,KCJZ,GZJZ,JIZHANG,JIEZHANG) VALUES ('" & Me.qiyongriqi.Value.Year.ToString & dateStart.Month.ToString.PadLeft(2, "0") & "','" & dateStart & "','" & dateStart.AddMonths(1).AddDays(-1) & "',true,true,true,true)")
                dateStart = dateStart.AddMonths(1)
            Else
                mdbD.Write("insert into FKZQ (NY,KSRQ,ZZRQ,KCJZ,GZJZ,JIZHANG,JIEZHANG) VALUES ('" & Me.qiyongriqi.Value.Year.ToString & dateStart.Month.ToString.PadLeft(2, "0") & "','" & dateStart & "','" & dateStart.AddMonths(1).AddDays(-1) & "',false,false,false,false)")
                dateStart = dateStart.AddMonths(1)
            End If
        Next

        '修改系统管理员
        If Me.cbGuanLiyuan.Checked Then
            mdbD.Write("update YongHu set Yonghuname='" & Me.guanliyuan.Text & "',YongHuMiMa=pwdencrypt('" & Me.mima.Text & "') where YongHuJiBie=2")

        End If

        Me.Close()
    End Sub

    Private Function yanZhengshuru() As Boolean
        If Me.zhangtaomingcheng.Text = "" Then
            MsgBox("账套名称为必填!", MsgBoxStyle.Information, "提示")
            Return False
        End If

        Select Case Me.bianhaoweishu.Text
            Case 2, 3, 4, 5, 6
            Case Else
                MsgBox("账套名称为必填!", MsgBoxStyle.Information, "提示")
                Return False
        End Select

        If Me.kemujishu.Text.Length <> 9 Then
            MsgBox("各级科目级数输入错误!", MsgBoxStyle.Information, "提示")
            Return False
        End If
        Dim i As Integer
        For i = 0 To 8
            If Asc(Me.kemujishu.Text.Substring(i, 1)) < 50 Or Asc(Me.kemujishu.Text.Substring(i, 1)) > 53 Then
                MsgBox("各级科目级数输入错误!", MsgBoxStyle.Information, "提示")
                Return False
            End If
        Next

        If Me.cbGuanLiyuan.Checked Then

            If Me.guanliyuan.Text = "" Then
                MsgBox("管理员不能为空!", MsgBoxStyle.Information, "提示")
                Return False
            End If

            If Me.mima.Text = "" Then
                MsgBox("密码不能为空!", MsgBoxStyle.Information, "提示")
                Return False
            End If

            If Me.mima.Text <> Me.querenmima.Text Then
                MsgBox("两次输入密码不一致!", MsgBoxStyle.Information, "提示")
                Return False
            End If
        End If

        Return True
    End Function

  
    Private Sub FKAdmin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Icon = FKG.myselfG.FKIcon

        Dim mdbS As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdbS.DataBind(Me.xuanzekemu, "select distinct HangYe from KMDM ")
        If Me.xuanzekemu.Items.Count > 0 Then
            Me.xuanzekemu.SelectedIndex = 0
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Panel2.Visible = Not Me.Panel2.Visible
    End Sub
End Class