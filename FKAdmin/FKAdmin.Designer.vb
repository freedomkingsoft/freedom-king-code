﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKAdmin
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Button2 = New System.Windows.Forms.Button
        Me.qiyongriqi = New System.Windows.Forms.DateTimePicker
        Me.kemujishu = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.xuanzekemu = New System.Windows.Forms.ComboBox
        Me.nianfen = New System.Windows.Forms.CheckBox
        Me.cbDairiKM = New System.Windows.Forms.CheckBox
        Me.bianhaoweishu = New System.Windows.Forms.TextBox
        Me.zhangtaomingcheng = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.cbGuanLiyuan = New System.Windows.Forms.CheckBox
        Me.querenmima = New System.Windows.Forms.TextBox
        Me.mima = New System.Windows.Forms.TextBox
        Me.guanliyuan = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.btnOK = New System.Windows.Forms.Button
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel3)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(399, 551)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.qiyongriqi)
        Me.Panel1.Controls.Add(Me.kemujishu)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.xuanzekemu)
        Me.Panel1.Controls.Add(Me.nianfen)
        Me.Panel1.Controls.Add(Me.cbDairiKM)
        Me.Panel1.Controls.Add(Me.bianhaoweishu)
        Me.Panel1.Controls.Add(Me.zhangtaomingcheng)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(395, 364)
        Me.Panel1.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(365, 351)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(27, 12)
        Me.Button2.TabIndex = 23
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = True
        '
        'qiyongriqi
        '
        Me.qiyongriqi.Location = New System.Drawing.Point(115, 137)
        Me.qiyongriqi.Name = "qiyongriqi"
        Me.qiyongriqi.Size = New System.Drawing.Size(150, 21)
        Me.qiyongriqi.TabIndex = 22
        Me.qiyongriqi.Value = New Date(2010, 4, 6, 0, 0, 0, 0)
        '
        'kemujishu
        '
        Me.kemujishu.Location = New System.Drawing.Point(112, 328)
        Me.kemujishu.Name = "kemujishu"
        Me.kemujishu.Size = New System.Drawing.Size(115, 21)
        Me.kemujishu.TabIndex = 21
        Me.kemujishu.Text = "433222222"
        Me.kemujishu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(26, 331)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 12)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "各级科目位数:"
        '
        'xuanzekemu
        '
        Me.xuanzekemu.Enabled = False
        Me.xuanzekemu.FormattingEnabled = True
        Me.xuanzekemu.Location = New System.Drawing.Point(150, 183)
        Me.xuanzekemu.Name = "xuanzekemu"
        Me.xuanzekemu.Size = New System.Drawing.Size(185, 20)
        Me.xuanzekemu.TabIndex = 19
        '
        'nianfen
        '
        Me.nianfen.AutoSize = True
        Me.nianfen.Location = New System.Drawing.Point(49, 281)
        Me.nianfen.Name = "nianfen"
        Me.nianfen.Size = New System.Drawing.Size(300, 16)
        Me.nianfen.TabIndex = 17
        Me.nianfen.Text = "单证编号中的年份使用4位数字,不选该项则使用２位"
        Me.nianfen.UseVisualStyleBackColor = True
        '
        'cbDairiKM
        '
        Me.cbDairiKM.AutoSize = True
        Me.cbDairiKM.Location = New System.Drawing.Point(49, 185)
        Me.cbDairiKM.Name = "cbDairiKM"
        Me.cbDairiKM.Size = New System.Drawing.Size(102, 16)
        Me.cbDairiKM.TabIndex = 18
        Me.cbDairiKM.Text = "带入科目代码:"
        Me.cbDairiKM.UseVisualStyleBackColor = True
        '
        'bianhaoweishu
        '
        Me.bianhaoweishu.Location = New System.Drawing.Point(112, 234)
        Me.bianhaoweishu.Name = "bianhaoweishu"
        Me.bianhaoweishu.Size = New System.Drawing.Size(63, 21)
        Me.bianhaoweishu.TabIndex = 16
        Me.bianhaoweishu.Text = "3"
        '
        'zhangtaomingcheng
        '
        Me.zhangtaomingcheng.Location = New System.Drawing.Point(115, 89)
        Me.zhangtaomingcheng.Name = "zhangtaomingcheng"
        Me.zhangtaomingcheng.Size = New System.Drawing.Size(223, 21)
        Me.zhangtaomingcheng.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(234, 331)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(101, 12)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "默认为:433332222"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(181, 237)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 12)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "注:可选2,3,4,5,6"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(47, 237)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 12)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "编号位数:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(50, 141)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 12)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "启用日期:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(50, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 12)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "账套名称:"
        '
        'Label1
        '
        Me.Label1.AutoEllipsis = True
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("黑体", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(48, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(251, 40)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "设置账套的核心基本信息," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "完成后不可修改!"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cbGuanLiyuan)
        Me.Panel2.Controls.Add(Me.querenmima)
        Me.Panel2.Controls.Add(Me.mima)
        Me.Panel2.Controls.Add(Me.guanliyuan)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(3, 373)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(393, 123)
        Me.Panel2.TabIndex = 1
        Me.Panel2.Visible = False
        '
        'cbGuanLiyuan
        '
        Me.cbGuanLiyuan.AutoSize = True
        Me.cbGuanLiyuan.Location = New System.Drawing.Point(71, 8)
        Me.cbGuanLiyuan.Name = "cbGuanLiyuan"
        Me.cbGuanLiyuan.Size = New System.Drawing.Size(90, 16)
        Me.cbGuanLiyuan.TabIndex = 10
        Me.cbGuanLiyuan.Text = "系统管理员:"
        Me.cbGuanLiyuan.UseVisualStyleBackColor = True
        '
        'querenmima
        '
        Me.querenmima.Location = New System.Drawing.Point(161, 95)
        Me.querenmima.Name = "querenmima"
        Me.querenmima.Size = New System.Drawing.Size(160, 21)
        Me.querenmima.TabIndex = 8
        '
        'mima
        '
        Me.mima.Location = New System.Drawing.Point(161, 50)
        Me.mima.Name = "mima"
        Me.mima.Size = New System.Drawing.Size(160, 21)
        Me.mima.TabIndex = 9
        '
        'guanliyuan
        '
        Me.guanliyuan.Location = New System.Drawing.Point(161, 6)
        Me.guanliyuan.Name = "guanliyuan"
        Me.guanliyuan.Size = New System.Drawing.Size(160, 21)
        Me.guanliyuan.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(102, 98)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 12)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "确认密码:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(126, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "密码:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnOK)
        Me.Panel3.Location = New System.Drawing.Point(3, 502)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(401, 56)
        Me.Panel3.TabIndex = 2
        '
        'btnOK
        '
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(165, 3)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(93, 36)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "创建账套"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'FKAdmin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(399, 551)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FKAdmin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "账套核心设置"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents qiyongriqi As System.Windows.Forms.DateTimePicker
    Friend WithEvents kemujishu As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents xuanzekemu As System.Windows.Forms.ComboBox
    Friend WithEvents nianfen As System.Windows.Forms.CheckBox
    Friend WithEvents cbDairiKM As System.Windows.Forms.CheckBox
    Friend WithEvents bianhaoweishu As System.Windows.Forms.TextBox
    Friend WithEvents zhangtaomingcheng As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cbGuanLiyuan As System.Windows.Forms.CheckBox
    Friend WithEvents querenmima As System.Windows.Forms.TextBox
    Friend WithEvents mima As System.Windows.Forms.TextBox
    Friend WithEvents guanliyuan As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
