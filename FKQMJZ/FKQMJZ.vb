﻿Imports System.Windows.Forms

Public Class FKKCJZ

    Private Sub FKKCJZ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case FKG.myselfG.sBanBen
            Case "开发版"
            Case "正式版"
            Case Else
                MsgBox("试用版不提供期末结账功能！", MsgBoxStyle.Information, "提示")
                Me.Close()
        End Select
        FillDG()
        Me.Icon = FKG.myselfG.FKIcon
    End Sub

    Private Function FillDG() As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgKCJZQJ, "select NY as 年月,ksrq as 起始日期,zzrq as 截止日期,JieZhang AS 结帐 from FKZQ WHERE NY LIKE '" & FKG.myselfG.NianFen & "%'")
    End Function

    Private Sub btnKCJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCJZ.Click
        '首先判断是否存在读写冲突
        Dim iT As Integer = 0
        Do While FKG.myselfG.bCanBackup = False
            System.Threading.Thread.Sleep(50)
            iT = iT + 1

            If iT >= 150 Then
                MsgBox("数据库正在使用当中，没有完成结帐！")
                Exit Sub
            End If
        Loop

        '禁用自动定时备份
        FKG.myselfG.bCanBackup = False

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim iNY As String

        Dim ds As DataSet
        ds = mdb.Reader("select NY from FKZQ where jieZhang='false' and NY like '" & FKG.myselfG.NianFen & "%' order by NY")
        If ds.Tables(0).Rows.Count = 0 Then
            '启用自动定时备份
            FKG.myselfG.bCanBackup = True
            Exit Sub
        Else
        End If

        iNY = ds.Tables(0).Rows(0).Item(0)
        Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

        If mdb.bExsit("select 凭证号 from PZ WHERE 审核='' and 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue) = True Then
            MsgBox("你选定的月份还有凭证没有审核，无法结账，请先审核所有凭证并记帐！", MsgBoxStyle.Information, "提示")
            '启用自动定时备份
            FKG.myselfG.bCanBackup = True
            Exit Sub
        End If

        If mdb.bExsit("select 凭证号 from PZ WHERE 记帐='' and 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue) = True Then
            MsgBox("你选定的月份还有凭证没有记账，无法结帐，请先将所有凭证记账！", MsgBoxStyle.Information, "提示")
            '启用自动定时备份
            FKG.myselfG.bCanBackup = True
            Exit Sub
        End If


        If iyue <= FKG.myselfG.YueFen Then

            '可以进行记账
            mdb.Write("update FKZQ SET JIEZHANG='true' WHERE NY='" & iNY & "'")
            mdb.Write("update JiBenCaozuo set 结账='" & FKG.myselfG.YongHu & "' where nian=" & FKG.myselfG.NianFen & " and yue=" & iyue)

            FillDG()

            If iyue = 12 Then
                '年末结帐,生成下年度科目代码及下年度年初余额
                Dim ndjz As New FKNDJZ.FKNDJZ
                If ndjz.NDJZ() Then
                Else
                    btnKCFJZ_Click(sender, e)
                End If
                'MsgBox(ndjz.ToString)
            End If
        Else
            '启用自动定时备份
            FKG.myselfG.bCanBackup = True
            Exit Sub
        End If
        '启用自动定时备份
        FKG.myselfG.bCanBackup = True
    End Sub

    Private Sub btnKCFJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCFJZ.Click
        '下年度有记帐操作后，不可反结账

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.bExsit("select NY from FKZQ where jizhang='true' and NY like '" & FKG.myselfG.NianFen + 1 & "%'") = True Then
            MsgBox("下年度已有记帐月份，无法反结账", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        Dim iNY As String
        Dim ds As DataSet
        ds = mdb.Reader("select NY from FKZQ where JIEZHANG='true' and NY >= '" & FKG.myselfG.dQiYongRiQi.Year.ToString & FKG.myselfG.dQiYongRiQi.Month.ToString.PadLeft(2, "0") & "' and NY like '" & FKG.myselfG.NianFen & "%' order by NY desc")
        If ds.Tables(0).Rows.Count = 0 Then
            Exit Sub
        Else
            iNY = ds.Tables(0).Rows(0).Item(0)
            Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

            mdb.Write("update FKZQ SET JIEZHANG='false' WHERE NY='" & FKG.myselfG.NianFen & iyue.ToString.PadLeft(2, "0") & "'")

            mdb.Write("update JiBenCaozuo set 结账='' where nian=" & FKG.myselfG.NianFen & " and yue=" & iyue)
            FillDG()
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnZhengLi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnZhengLi.Click
        Dim dlg As New FKQMZhengLi
        dlg.Show()
    End Sub
End Class
