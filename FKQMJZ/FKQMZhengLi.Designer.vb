﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKQMZhengLi
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnChai = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.lbBak = New System.Windows.Forms.ListBox
        Me.lbMain = New System.Windows.Forms.ListBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.lbl1 = New System.Windows.Forms.Label
        Me.btnHebing = New System.Windows.Forms.Button
        Me.lblQY = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.RichTextBox1.ForeColor = System.Drawing.Color.Red
        Me.RichTextBox1.Location = New System.Drawing.Point(21, 12)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(378, 138)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = "当系统使用时间多长时(5年以上),数据库将会很大,系统运行可能会变慢.本功能将数据库中过往年份的数据拆分为一个独立的备份数据库,以提高系统的运行速度,当需要过往年" & _
            "份的数据时,将备份数据库中的数据再合并到主数据库中.使用此功能之前一定要做好数据备份工作."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(18, 173)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "当前主数据库包含年份"
        '
        'btnChai
        '
        Me.btnChai.Location = New System.Drawing.Point(79, 375)
        Me.btnChai.Name = "btnChai"
        Me.btnChai.Size = New System.Drawing.Size(107, 44)
        Me.btnChai.TabIndex = 3
        Me.btnChai.Text = "开始拆分"
        Me.btnChai.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(355, 382)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(63, 31)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "关闭"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(247, 173)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(152, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "备份数据库包含年份"
        '
        'lbBak
        '
        Me.lbBak.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbBak.FormattingEnabled = True
        Me.lbBak.ItemHeight = 16
        Me.lbBak.Location = New System.Drawing.Point(298, 201)
        Me.lbBak.Name = "lbBak"
        Me.lbBak.Size = New System.Drawing.Size(91, 148)
        Me.lbBak.TabIndex = 4
        '
        'lbMain
        '
        Me.lbMain.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbMain.FormattingEnabled = True
        Me.lbMain.ItemHeight = 16
        Me.lbMain.Location = New System.Drawing.Point(21, 201)
        Me.lbMain.Name = "lbMain"
        Me.lbMain.Size = New System.Drawing.Size(91, 148)
        Me.lbMain.TabIndex = 4
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(173, 269)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(81, 26)
        Me.TextBox1.TabIndex = 5
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl1
        '
        Me.lbl1.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbl1.ForeColor = System.Drawing.Color.Blue
        Me.lbl1.Location = New System.Drawing.Point(130, 298)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(152, 61)
        Me.lbl1.TabIndex = 1
        '
        'btnHebing
        '
        Me.btnHebing.Location = New System.Drawing.Point(225, 375)
        Me.btnHebing.Name = "btnHebing"
        Me.btnHebing.Size = New System.Drawing.Size(107, 44)
        Me.btnHebing.TabIndex = 3
        Me.btnHebing.Text = "开始合并"
        Me.btnHebing.UseVisualStyleBackColor = True
        '
        'lblQY
        '
        Me.lblQY.AutoSize = True
        Me.lblQY.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblQY.ForeColor = System.Drawing.Color.Blue
        Me.lblQY.Location = New System.Drawing.Point(109, 222)
        Me.lblQY.Name = "lblQY"
        Me.lblQY.Size = New System.Drawing.Size(128, 16)
        Me.lblQY.TabIndex = 1
        Me.lblQY.Text = "系统启用年份为:"
        '
        'FKQMZhengLi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(430, 431)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.lbMain)
        Me.Controls.Add(Me.lbBak)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnHebing)
        Me.Controls.Add(Me.btnChai)
        Me.Controls.Add(Me.lbl1)
        Me.Controls.Add(Me.lblQY)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Name = "FKQMZhengLi"
        Me.Text = "整理数据"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnChai As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbBak As System.Windows.Forms.ListBox
    Friend WithEvents lbMain As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents btnHebing As System.Windows.Forms.Button
    Friend WithEvents lblQY As System.Windows.Forms.Label
End Class
