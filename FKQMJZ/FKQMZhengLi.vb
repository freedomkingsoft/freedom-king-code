Public Class FKQMZhengLi

    Private Sub FKQMZhengLi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.btnHebing.Enabled = False
        Me.btnChai.Enabled = False

        CreatBak()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub CreatBak()
        Dim sPath As String
        sPath = My.Application.Info.DirectoryPath

        If IO.File.Exists(sPath & "\FKData\FreedomKingData_Bak.mdb") Then
            fillLB()
        Else
            IO.File.Copy(sPath & "\FKData\FreedomKingData.mdb", sPath & "\FKData\FreedomKingData_Bak.mdb")
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW.Replace("FreedomKingData.mdb", "FreedomKingData_Bak.mdb"))

            mdb.Write("delete from jibencaozuo")
            mdb.Write("delete from pz")
            mdb.Write("delete from kucun")
            mdb.Write("delete from gongzi")
            mdb.Write("delete from fkzq")

            fillLB()
        End If
    End Sub

    Private Sub fillLB()
        Me.lbMain.Items.Clear()
        Me.lbBak.Items.Clear()

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select distinct year(ksrq) from fkzq")

        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.lbMain.Items.Add(ds.Tables(0).Rows(i).Item(0))
        Next

        Dim mdbBak As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW.Replace("FreedomKingData.mdb", "FreedomKingData_Bak.mdb"))
        Dim dsBak As DataSet
        dsBak = mdbBak.Reader("select distinct year(ksrq) from fkzq")

        'Dim i As Integer
        For i = 0 To dsBak.Tables(0).Rows.Count - 1
            Me.lbBak.Items.Add(dsBak.Tables(0).Rows(i).Item(0))
        Next

        Me.lblQY.Text = "系统启用日期为" & mdb.Reader("select qiyongriqi from zt").Tables(0).Rows(0).Item(0)
    End Sub

    Private Sub lbMain_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lbMain.MouseDoubleClick
        If Me.lbMain.SelectedIndex <> -1 Then
            Me.TextBox1.Text = lbMain.SelectedItem.ToString

            Me.btnChai.Enabled = True
            Me.btnHebing.Enabled = False

            Me.lbl1.Text = "将拆分" & Me.TextBox1.Text & "年及之前的数据到备份数据库!"
        End If
    End Sub
    
  
    Private Sub lbBak_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lbBak.MouseDoubleClick
        If Me.lbBak.SelectedIndex <> -1 Then
            Me.TextBox1.Text = Me.lbBak.SelectedItem.ToString

            Me.btnChai.Enabled = False
            Me.btnHebing.Enabled = True

            Me.lbl1.Text = "将合并" & Me.TextBox1.Text & "年及之前的数据到主数据库!"

        End If
    End Sub

    Private Sub btnChai_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChai.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        '第一步,检查是否已结帐
        If mdb.Reader("select * from fkzq where KCJZ='true' and GZJZ='true' and Jiezhang='true' and year(ksrq)=" & Me.TextBox1.Text).Tables(0).Rows.Count <> 12 Then
            MsgBox("你选择的年份没有完成结帐操作,不能进行拆分!", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        '第二步,复制数据
        If mdb.Write("INSERT INTO [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].pz select * from pz where 年份<=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].GongZi select * from GongZi where 年份<=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].KuCun select * from KuCun where 年份<=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].JiBenCaoZuo select * from JiBenCaoZuo where nian <=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].FKZQ select * from FKZQ where year(ksrq) <=" & Me.TextBox1.Text) Then

        Else
            MsgBox("数据拆分没有成功!主数据库中的数据没有删除,请手动对备份数据库进行恢复!", MsgBoxStyle.Information, "严重警告!")
            Exit Sub
        End If

        '增加一步,复制往来核算的科目的余额为对账的期初余额,并且"备用"为"系统拆分对账"以防止删除,在合并数据库时统一删除
        If mdb.Write("insert into wlkhdzh (WLKHDM,DZHDATE,YEFX,YEJINE,SHUOMING,JZYWBH,JZWLBH,DZHTIME,备用) select kmye.kmdm,#" & Me.TextBox1.Text & "-12-31#,'应收',ncjfye,'拆分自动对账','','',now,'系统拆分对账' from kmye,kmdm where kmye.nian=" & Me.TextBox1.Text + 1 & " and kmye.kmdm=kmdm.kmdm and kmdm.hslbwl='true' and kmdm.nian=" & Me.TextBox1.Text & "  and ncjfye>0 and kmdm.sfmj='true' and kmye.kmdm not in (select wlkhdm from wlkhdzh where DZhDate >=#" & Me.TextBox1.Text & "-12-31#)") = True And mdb.Write("insert into wlkhdzh (WLKHDM,DZHDATE,YEFX,YEJINE,SHUOMING,JZYWBH,JZWLBH,DZHTIME,备用) select kmye.kmdm,#" & Me.TextBox1.Text & "-12-31#,'应付',ncjfye,'拆分自动对账','','',now,'系统拆分对账' from kmye,kmdm where kmye.nian=" & Me.TextBox1.Text + 1 & " and kmye.kmdm=kmdm.kmdm and kmdm.hslbwl='true' and kmdm.nian=" & Me.TextBox1.Text & "  and ncdfye>0 and kmdm.sfmj='true'  and kmye.kmdm not in (select wlkhdm from wlkhdzh where DZhDate >=#" & Me.TextBox1.Text & "-12-31#)") Then
        Else
            Exit Sub
        End If

        '第三步,删除主数据库数据
        mdb.Write("delete from pz where 年份<=" & Me.TextBox1.Text)
        mdb.Write("delete from GongZi where 年份<=" & Me.TextBox1.Text)
        mdb.Write("delete from KuCun where 年份<=" & Me.TextBox1.Text)
        mdb.Write("delete from JiBenCaoZuo where nian<=" & Me.TextBox1.Text)
        mdb.Write("delete from FKZQ  where year(ksrq) <=" & Me.TextBox1.Text)

        '第四步,刷新列表
        fillLB()

        Me.TextBox1.Clear()
        Me.btnChai.Enabled = False
    End Sub

    Private Sub btnHebing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHebing.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        ''第一步,检查是否已结帐
        'If mdb.Reader("select * from fkzq where KCJZ='true' and GZJZ='true' and Jiezhang='true' and year(ksrq)=" & Me.TextBox1.Text).Tables(0).Rows.Count <> 12 Then
        '    MsgBox("你选择的年份没有完成结帐操作,不能进行拆分!", MsgBoxStyle.Information, "提示")
        '    Exit Sub
        'End If

        '第二步,复制数据
        If mdb.Write("INSERT INTO pz select * from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].pz where 年份<=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO GongZi select * from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].GongZi where 年份<=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO KuCun select * from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].KuCun where 年份<=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO JiBenCaoZuo select * from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].JiBenCaoZuo where nian <=" & Me.TextBox1.Text) AndAlso mdb.Write("INSERT INTO FKZQ select * from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].FKZQ where year(ksrq) <=" & Me.TextBox1.Text) Then
        Else
            MsgBox("数据合并没有成功!备份数据库中的数据没有删除,请手动对主数据库进行恢复!", MsgBoxStyle.Information, "严重警告!")
            Exit Sub
        End If

        '增加一步,需要将系统自动对账的记录删除
        mdb.Write("delete from wlkhdzh where 备用='系统拆分对账' and DZHDATE=#" & Me.TextBox1.Text & "-12-31#")

        '第三步,删除主数据库数据
        mdb.Write("delete from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].pz where 年份<=" & Me.TextBox1.Text)
        mdb.Write("delete from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].GongZi where 年份<=" & Me.TextBox1.Text)
        mdb.Write("delete from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].KuCun where 年份<=" & Me.TextBox1.Text)
        mdb.Write("delete from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].JiBenCaoZuo where nian<=" & Me.TextBox1.Text)
        mdb.Write("delete from [';database=" & My.Application.Info.DirectoryPath & "\FKdata\Freedomkingdata_Bak.mdb;pwd=Zh!o04#Dn$3Tng09#'].FKZQ  where year(ksrq) <=" & Me.TextBox1.Text)

        '第四步,刷新列表
        fillLB()

        Me.TextBox1.Clear()
        Me.btnHebing.Enabled = False
    End Sub
End Class