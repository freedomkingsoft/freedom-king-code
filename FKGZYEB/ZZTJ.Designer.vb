﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZZTJ
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.myNian = New System.Windows.Forms.NumericUpDown
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.myYue = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.myMinKM = New System.Windows.Forms.TextBox
        Me.myMaxKM = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.myXJKM = New System.Windows.Forms.CheckBox
        Me.myFSKM = New System.Windows.Forms.CheckBox
        Me.myXSSL = New System.Windows.Forms.CheckBox
        Me.myWJZ = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.myNian, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.myYue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(190, 253)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 27)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 21)
        Me.OK_Button.TabIndex = 5
        Me.OK_Button.Text = "确定"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 21)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "取消"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(43, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "时　间："
        '
        'myNian
        '
        Me.myNian.Location = New System.Drawing.Point(102, 77)
        Me.myNian.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.myNian.Minimum = New Decimal(New Integer() {2008, 0, 0, 0})
        Me.myNian.Name = "myNian"
        Me.myNian.Size = New System.Drawing.Size(64, 21)
        Me.myNian.TabIndex = 1
        Me.myNian.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.myNian.Value = New Decimal(New Integer() {2008, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(175, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(17, 12)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "年"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(250, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "月"
        '
        'myYue
        '
        Me.myYue.Location = New System.Drawing.Point(198, 77)
        Me.myYue.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.myYue.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.myYue.Name = "myYue"
        Me.myYue.Size = New System.Drawing.Size(46, 21)
        Me.myYue.TabIndex = 2
        Me.myYue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.myYue.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(43, 123)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 12)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "科目范围："
        '
        'myMinKM
        '
        Me.myMinKM.Location = New System.Drawing.Point(112, 119)
        Me.myMinKM.Name = "myMinKM"
        Me.myMinKM.Size = New System.Drawing.Size(65, 21)
        Me.myMinKM.TabIndex = 3
        '
        'myMaxKM
        '
        Me.myMaxKM.Location = New System.Drawing.Point(215, 119)
        Me.myMaxKM.Name = "myMaxKM"
        Me.myMaxKM.Size = New System.Drawing.Size(65, 21)
        Me.myMaxKM.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(188, 123)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(17, 12)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "至"
        '
        'myXJKM
        '
        Me.myXJKM.AutoSize = True
        Me.myXJKM.Checked = True
        Me.myXJKM.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myXJKM.Location = New System.Drawing.Point(45, 200)
        Me.myXJKM.Name = "myXJKM"
        Me.myXJKM.Size = New System.Drawing.Size(96, 16)
        Me.myXJKM.TabIndex = 5
        Me.myXJKM.Text = "显示下级科目"
        Me.myXJKM.UseVisualStyleBackColor = True
        '
        'myFSKM
        '
        Me.myFSKM.AutoSize = True
        Me.myFSKM.Checked = True
        Me.myFSKM.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myFSKM.Location = New System.Drawing.Point(193, 162)
        Me.myFSKM.Name = "myFSKM"
        Me.myFSKM.Size = New System.Drawing.Size(132, 16)
        Me.myFSKM.TabIndex = 5
        Me.myFSKM.Text = "只显示有发生的科目"
        Me.myFSKM.UseVisualStyleBackColor = True
        '
        'myXSSL
        '
        Me.myXSSL.AutoSize = True
        Me.myXSSL.Location = New System.Drawing.Point(45, 162)
        Me.myXSSL.Name = "myXSSL"
        Me.myXSSL.Size = New System.Drawing.Size(72, 16)
        Me.myXSSL.TabIndex = 5
        Me.myXSSL.Text = "显示数量"
        Me.myXSSL.UseVisualStyleBackColor = True
        '
        'myWJZ
        '
        Me.myWJZ.AutoSize = True
        Me.myWJZ.Checked = True
        Me.myWJZ.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myWJZ.Location = New System.Drawing.Point(193, 200)
        Me.myWJZ.Name = "myWJZ"
        Me.myWJZ.Size = New System.Drawing.Size(96, 16)
        Me.myWJZ.TabIndex = 5
        Me.myWJZ.Text = "含未记账凭证"
        Me.myWJZ.UseVisualStyleBackColor = True
        '
        'ZZTJ
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(348, 291)
        Me.Controls.Add(Me.myWJZ)
        Me.Controls.Add(Me.myFSKM)
        Me.Controls.Add(Me.myXSSL)
        Me.Controls.Add(Me.myXJKM)
        Me.Controls.Add(Me.myMaxKM)
        Me.Controls.Add(Me.myMinKM)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.myYue)
        Me.Controls.Add(Me.myNian)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ZZTJ"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "工资余额表"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.myNian, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.myYue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents myNian As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents myYue As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents myMinKM As System.Windows.Forms.TextBox
    Friend WithEvents myMaxKM As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents myXJKM As System.Windows.Forms.CheckBox
    Friend WithEvents myFSKM As System.Windows.Forms.CheckBox
    Friend WithEvents myXSSL As System.Windows.Forms.CheckBox
    Friend WithEvents myWJZ As System.Windows.Forms.CheckBox

End Class
