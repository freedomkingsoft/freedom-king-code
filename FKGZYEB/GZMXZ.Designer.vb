﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GZMXZ
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GZMXZ))
        Me.dgMX = New System.Windows.Forms.DataGridView
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.导出 = New System.Windows.Forms.ToolStripButton
        Me.查看单证 = New System.Windows.Forms.ToolStripButton
        Me.日记帐 = New System.Windows.Forms.ToolStripButton
        Me.预览 = New System.Windows.Forms.ToolStripButton
        Me.打印 = New System.Windows.Forms.ToolStripButton
        Me.列设置 = New System.Windows.Forms.ToolStripButton
        Me.关闭 = New System.Windows.Forms.ToolStripButton
        Me.tscKMDM = New System.Windows.Forms.ToolStripComboBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblKM = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        CType(Me.dgMX, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgMX
        '
        Me.dgMX.AllowUserToAddRows = False
        Me.dgMX.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgMX.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgMX.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgMX.ColumnHeadersHeight = 25
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgMX.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgMX.Location = New System.Drawing.Point(1, 112)
        Me.dgMX.Name = "dgMX"
        Me.dgMX.ReadOnly = True
        Me.dgMX.RowHeadersVisible = False
        Me.dgMX.RowHeadersWidth = 15
        Me.dgMX.RowTemplate.Height = 23
        Me.dgMX.Size = New System.Drawing.Size(738, 512)
        Me.dgMX.TabIndex = 0
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.导出, Me.查看单证, Me.日记帐, Me.预览, Me.打印, Me.列设置, Me.关闭, Me.tscKMDM})
        Me.ToolStrip1.Location = New System.Drawing.Point(1, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Padding = New System.Windows.Forms.Padding(3, 0, 1, 0)
        Me.ToolStrip1.Size = New System.Drawing.Size(738, 37)
        Me.ToolStrip1.Stretch = True
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        '导出
        '
        Me.导出.Image = CType(resources.GetObject("导出.Image"), System.Drawing.Image)
        Me.导出.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.导出.Margin = New System.Windows.Forms.Padding(5, 1, 0, 2)
        Me.导出.Name = "导出"
        Me.导出.Size = New System.Drawing.Size(49, 34)
        Me.导出.Text = "导出"
        '
        '查看单证
        '
        Me.查看单证.Image = Global.FKGZYEB.My.Resources.Resources.image028
        Me.查看单证.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.查看单证.Margin = New System.Windows.Forms.Padding(5, 1, 0, 2)
        Me.查看单证.Name = "查看单证"
        Me.查看单证.Size = New System.Drawing.Size(73, 34)
        Me.查看单证.Text = "查看单证"
        '
        '日记帐
        '
        Me.日记帐.Image = Global.FKGZYEB.My.Resources.Resources.image137
        Me.日记帐.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.日记帐.Margin = New System.Windows.Forms.Padding(12, 1, 0, 2)
        Me.日记帐.Name = "日记帐"
        Me.日记帐.Size = New System.Drawing.Size(61, 34)
        Me.日记帐.Text = "日记帐"
        '
        '预览
        '
        Me.预览.Image = CType(resources.GetObject("预览.Image"), System.Drawing.Image)
        Me.预览.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.预览.Margin = New System.Windows.Forms.Padding(12, 1, 0, 2)
        Me.预览.Name = "预览"
        Me.预览.Size = New System.Drawing.Size(49, 34)
        Me.预览.Text = "预览"
        '
        '打印
        '
        Me.打印.Image = CType(resources.GetObject("打印.Image"), System.Drawing.Image)
        Me.打印.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.打印.Margin = New System.Windows.Forms.Padding(12, 1, 0, 2)
        Me.打印.Name = "打印"
        Me.打印.Size = New System.Drawing.Size(49, 34)
        Me.打印.Text = "打印"
        '
        '列设置
        '
        Me.列设置.Image = Global.FKGZYEB.My.Resources.Resources.image172
        Me.列设置.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.列设置.Margin = New System.Windows.Forms.Padding(12, 1, 0, 2)
        Me.列设置.Name = "列设置"
        Me.列设置.Size = New System.Drawing.Size(61, 34)
        Me.列设置.Text = "列设置"
        '
        '关闭
        '
        Me.关闭.Image = Global.FKGZYEB.My.Resources.Resources.image712
        Me.关闭.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.关闭.Margin = New System.Windows.Forms.Padding(12, 1, 0, 2)
        Me.关闭.Name = "关闭"
        Me.关闭.Size = New System.Drawing.Size(67, 34)
        Me.关闭.Text = "关闭(&X)"
        '
        'tscKMDM
        '
        Me.tscKMDM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.tscKMDM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.tscKMDM.DropDownWidth = 265
        Me.tscKMDM.MaxDropDownItems = 12
        Me.tscKMDM.Name = "tscKMDM"
        Me.tscKMDM.Size = New System.Drawing.Size(165, 37)
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblKM, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 40)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(738, 66)
        Me.TableLayoutPanel1.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("宋体", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.Location = New System.Drawing.Point(548, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 14)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "日期:"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 14)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "单位:"
        '
        'lblKM
        '
        Me.lblKM.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblKM.AutoSize = True
        Me.lblKM.Font = New System.Drawing.Font("宋体", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKM.Location = New System.Drawing.Point(194, 45)
        Me.lblKM.Name = "lblKM"
        Me.lblKM.Size = New System.Drawing.Size(42, 14)
        Me.lblKM.TabIndex = 3
        Me.lblKM.Text = "科目:"
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label1, 3)
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(220, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(298, 26)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "工　　资　　明　　细　　帐"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("宋体", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label5.Location = New System.Drawing.Point(515, 638)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 14)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "打印时间:"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("宋体", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 638)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 14)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "打印:"
        '
        'GZMXZ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(740, 661)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.dgMX)
        Me.Name = "GZMXZ"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "工资明细账"
        CType(Me.dgMX, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgMX As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents 导出 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 日记帐 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 预览 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 打印 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 关闭 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblKM As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents 查看单证 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 列设置 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tscKMDM As System.Windows.Forms.ToolStripComboBox
End Class
