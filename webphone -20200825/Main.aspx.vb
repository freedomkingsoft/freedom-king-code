﻿
Partial Class Main
    Inherits System.Web.UI.Page

    Public sQiYeName As String


    Dim dsLoginInfo As Data.DataSet

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            If Page.IsPostBack = False Then
                getBaoBiaoList()
            End If
        End If
    End Sub

    Private Sub getBaoBiaoList()
        dsLoginInfo = Session("LoginInfo")

        If IsNothing(dsLoginInfo) = False Then

            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim dsLieBiao As Data.DataSet
            dsLieBiao = mdb.Reader("select ID,ConText,SUBSTRING(TiShi,1,CHARINDEX(',',TiShi)) as shuoming from zdybb where [index]=99 order by [ParentID]")
            listbaobiao.DataSource = dsLieBiao
            listbaobiao.DataBind()

            '获取最新数据
            Dim dsYuE As Data.DataSet
            dsYuE = mdb.ReadStoredProcedure(getConnectString(), "yun_bb_Main", "")
            dsMain.DataSource = dsYuE
            dsMain.DataBind()

            sQiYeName = dsLoginInfo.Tables(0).Rows(0).Item("comName").ToString
        End If
    End Sub

    Private Function getConnectString() As String
        Dim scon As String

        scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & System.Configuration.ConfigurationManager.AppSettings("DATASOURCE")
        Return scon
    End Function


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sbh As String
        sbh = Request.Form("bianhao")

        If sbh <> "" Then
            Response.Redirect("detail.aspx?bh=" & sbh)
        End If
    End Sub
End Class
