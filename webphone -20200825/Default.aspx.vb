﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Public sSessionID As String

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Me.btnSubmit.Enabled = False

        Dim sName As String = Request.Form("username")
        Dim sPWS As String = Request.Form("passwd")

        If sName = "" OrElse sPWS = "" Then
            legend.Text = "用户名和密码不能为空"
            legend.Style("Display") = "Block"
            Exit Sub
        End If

        sSessionID = Session.SessionID

        Dim ws As New wsbdsp.Service
        Dim ds As New Data.DataSet


        'ds = ws.Login(getTok(sSessionID), sName, "", sPWS, 1)
        ds = ws.NewLogin(getTok(sSessionID), sName, "", sPWS, 1, "wap")

        If ds.Tables(0).Rows.Count = 0 Then
            legend.Style("Display") = "Block"
            Session.Add("LoginInfo", Nothing)
        Else


            Dim cookie As New HttpCookie("myLoginName")

            cookie.Values.Add("LoginName", sName)
            cookie.Values.Add("LoginPWD", sPWS)
            Response.AppendCookie(cookie)

            '我们取出Cookie值也很简单()

            legend.Style("Display") = "None"
            Session.Add("LoginInfo", ds)
            Session.Add("QiYeName", ds.Tables(0).Rows(0).Item("comName").ToString)

            Dim bChangePWS As Int16
            Try
                bChangePWS = ds.Tables(0).Rows(0).Item("ChangePWS").ToString

            Catch ex As Exception
                bChangePWS = 1
            End Try
            If bChangePWS = 0 Then

                Response.Redirect("changepws.aspx")
            End If

            Response.Redirect("main.aspx")
        End If

        Me.btnSubmit.Enabled = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scookie As HttpCookie = Request.Cookies("myLoginName")
        If Not IsNothing(scookie) Then
            Dim sName As String
            sName = scookie.Item("LoginName")
            Dim sPWS As String
            sPWS = scookie.Item("LoginPWD")

            If sName = "" OrElse sPWS = "" Then
                legend.Text = "用户名和密码不能为空"
                legend.Style("Display") = "Block"
                Exit Sub
            End If

            sSessionID = Session.SessionID

            Dim ws As New wsbdsp.Service
            Dim ds As New Data.DataSet

            ds = ws.NewLogin(getTok(sSessionID), sName, "", sPWS, 1, "wap")

            If ds.Tables(0).Rows.Count = 0 Then
                legend.Style("Display") = "Block"
                Session.Add("LoginInfo", Nothing)
            Else
                legend.Style("Display") = "None"
                Session.Add("LoginInfo", ds)
                Session.Add("QiYeName", ds.Tables(0).Rows(0).Item("comName").ToString)

                Dim bChangePWS As Int16
                Try
                    bChangePWS = ds.Tables(0).Rows(0).Item("ChangePWS").ToString

                Catch ex As Exception
                    bChangePWS = 1
                End Try
                If bChangePWS = 0 Then

                    Response.Redirect("changepws.aspx")
                End If

                Response.Redirect("main.aspx")
            End If

        End If
        legend.Style("Display") = "None"

    End Sub




    Private Function getTok(ByVal sTokenKey As String) As String
        Try
            Dim sName As String = sTokenKey
            'Dim sPWS As String = Split(sToken, ",").GetValue(1).ToString

            Dim sT As String = ""
            Dim i As Integer
            For i = 0 To sName.Length - 1
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            Next

            Dim sTR As String = "A"
            For i = 0 To sT.Length.ToString - 1
                If i Mod 7 = 0 Then
                    sTR = sTR & sT.Substring(i, 1)
                End If
            Next

            Return sName & "," & sTR
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class
