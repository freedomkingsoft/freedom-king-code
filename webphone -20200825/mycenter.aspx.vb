﻿
Partial Class mycenter
    Inherits System.Web.UI.Page

    Public dsLoginInfo As New Data.DataSet

    Public regPhone As String
    Public regDate As String
    Public freeEndDate As String
    Public VIP As String
    Public VIPMaturitydate As String
    Public LoginNumber As String
    Public comName As String
    Public eMail As String
    Public loginName As String
    Public loginPhone As String
    Public subUserID As String
    Public admin As String
    Public regTime As String


    Public tishiMM As String = ""
    Public tishiPhone As String = ""
    Public HideAdmin As String = "none"

    Public itemPhoneList As String = ""

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        'scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & System.Configuration.ConfigurationManager.AppSettings("DATASOURCE")
        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function

    Private Sub PhoneList()
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
        Dim sb As New StringBuilder

        Dim dsList As Data.DataSet
        dsList = mdb.Reader("select * from yunUserLoginName where userID=(select userid from yunUser where regPhone='" & regPhone & "')")
        Dim i As Integer
        For i = 0 To dsList.Tables(0).Rows.Count - 1
            sb.Append(" <div class=""layout-box"" data-act-type=""hover"">")
            sb.Append("<div class=""box-col txt-cut"">")
            sb.Append("<span id=""Span15"" class=""mct-a "" style=""width: 15%; display: -moz-inline-box; display: inline-block;"">")
            sb.Append("手机号</span><span class=""mct-a"" style=""text-align: right"">")
            sb.Append(dsList.Tables(0).Rows(i).Item("loginPhone").ToString)
            sb.Append("子账号")
            sb.Append(dsList.Tables(0).Rows(i).Item("subUserID").ToString)
            sb.Append("<a href=""action.aspx?act=reset&loginPhone=" & dsList.Tables(0).Rows(i).Item("loginPhone").ToString & """> 重置密码 </a>")
            sb.Append("<a href=""action.aspx?act=delete&loginPhone=" & dsList.Tables(0).Rows(i).Item("loginPhone").ToString & """ > 删除 </a>")
            sb.Append("</span></div></div>")
            'sb.Append("<span data-node=""arrow"" class=""plus plus-s"">")
            'sb.Append("<asp:Button ID=""Button2"" runat=""server"" Text=""删除"" /><i class=""icon-font icon-font-arrow-right txt-s"">")
            'sb.Append(" </i></span>")
            'sb.Append("</div>")

        Next

        itemPhoneList = sb.ToString
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            regPhone = dsLoginInfo.Tables(0).Rows(0).Item("regPhone").ToString
            regDate = dsLoginInfo.Tables(0).Rows(0).Item("regDate").ToString
            freeEndDate = dsLoginInfo.Tables(0).Rows(0).Item("freeEndDate").ToString
            VIP = dsLoginInfo.Tables(0).Rows(0).Item("VIP").ToString
            VIPMaturitydate = dsLoginInfo.Tables(0).Rows(0).Item("VIPMaturitydate").ToString
            LoginNumber = dsLoginInfo.Tables(0).Rows(0).Item("LoginNumber").ToString
            comName = dsLoginInfo.Tables(0).Rows(0).Item("comName").ToString
            eMail = dsLoginInfo.Tables(0).Rows(0).Item("eMail").ToString
            loginName = dsLoginInfo.Tables(0).Rows(0).Item("loginName").ToString
            loginPhone = dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString
            subUserID = dsLoginInfo.Tables(0).Rows(0).Item("subUserID").ToString
            admin = dsLoginInfo.Tables(0).Rows(0).Item("admin").ToString
            regTime = dsLoginInfo.Tables(0).Rows(0).Item("regTime").ToString

            If dsLoginInfo.Tables(0).Rows(0).Item("admin") = True Then
                HideAdmin = "block"
                PhoneList()
            End If
        End If
    End Sub

    Protected Sub btnUpdateComName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateComName.Click
        If Request.Form("newComName") <> "" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim sSql As String = "update yunUser set comName='" & Request.Form("newComName") & "' where regPhone='" & dsLoginInfo.Tables(0).Rows(0).Item("regPhone").ToString & "'"
            mdb.Write(sSql)
            comName = Request.Form("newComName")
            dsLoginInfo.Tables(0).Rows(0).Item("comName") = comName
            Session("LoginInfo") = dsLoginInfo
        End If
    End Sub

    Protected Sub btnUpdateEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateEmail.Click
        If Request.Form("newEmail") <> "" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim sSql As String = "update yunUser set eMail='" & Request.Form("newEmail") & "' where regPhone='" & dsLoginInfo.Tables(0).Rows(0).Item("regPhone").ToString & "'"
            mdb.Write(sSql)
            eMail = Request.Form("newEmail")
            dsLoginInfo.Tables(0).Rows(0).Item("eMail") = eMail
            Session("LoginInfo") = dsLoginInfo
        End If
    End Sub

    Protected Sub btnUpdatePWS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePWS.Click
        'dsLoginInfo = Session("LoginInfo")
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())

        Dim sss As String = "select  loginPhone from  yunUserLoginName where loginPhone='" + dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString + "' and  pwdcompare('" + Request.Form("oldPWS") + "',loginPhonePWS)=1"
        If Request.Form("oldPWS") = "" OrElse Request.Form("newPWS1") = "" OrElse Request.Form("newPWS2") = "" Then
            tishiMM = "密码为空，请输入密码"
            Exit Sub
        ElseIf Request.Form("newPWS1") <> Request.Form("newPWS2") Then
            tishiMM = "两次输入的密码不一致"
        ElseIf mdb.bExsit("select  loginPhone from  yunUserLoginName where loginPhone='" + dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString + "' and  pwdcompare('" + Request.Form("oldPWS") + "',loginPhonePWS)=1") = False Then
            tishiMM = "你输入的旧密码错误"
        Else
            Dim sSql As String = "update yunUserLoginName set loginPhonePWS=pwdencrypt('" & Request.Form("newPWS1") & "'),changepws=1 where loginname='" & dsLoginInfo.Tables(0).Rows(0).Item("loginname").ToString & "' and loginPhone='" & dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString & "'"
            mdb.Write(sSql)
        End If
    End Sub

    Protected Sub btnAddPhone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPhone.Click
        ' dsLoginInfo = Session("LoginInfo")

        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
        If Request.Form("newPhone") = "" OrElse Request.Form("newPhonePWS1") = "" OrElse Request.Form("newPhonePWS2") = "" Then
            tishiPhone = "手机号和密码不能为空"
            Exit Sub
        ElseIf Request.Form("newPhonePWS1") <> Request.Form("newPhonePWS2") Then
            tishiPhone = "两次输入的密码不一致"
        ElseIf Request.Form("newPhone").Length <> 11 Then
            tishiPhone = "你输入的手机号错误"
        Else
            Dim sSubUserid As String = ""

            Do
                sSubUserid = CType(Int(1000 + 8999 * Rnd()), String)
            Loop While mdb.bExsit("select loginName from yunUserLoginName where loginName='" & dsLoginInfo.Tables(0).Rows(0).Item("loginName") & "' and subUserID='" & sSubUserid & "'")

            Dim sSQL As String = "INSERT INTO [yunUserLoginName] ([Userid] ,[loginName] ,[loginPhone] ,[loginPhonePWS] ,[loginState] ,[subUserID] ,[subUserPWS] ,[admin])     VALUES ('" & dsLoginInfo.Tables(0).Rows(0).Item("Userid").ToString & "','" & dsLoginInfo.Tables(0).Rows(0).Item("loginName").ToString & "' ,'" & Request.Form("newPhone") & "',pwdencrypt('" & Request.Form("newPhonePWS1") & "') ,0 ,'" & sSubUserid & "' ,pwdencrypt('" & Request.Form("newPhonePWS1") & "') ,0)"
            ' Dim sSQL As String = "INSERT INTO [yunUserLoginName] ([Userid] ,[loginName] ,[loginPhone] ,[loginPhonePWS] ,[loginState] ,[subUserID] ,[subUserPWS] ,[admin])     VALUES ('" & dsLoginInfo.Tables(0).Rows(0).Item("Userid") & "','" & dsLoginInfo.Tables(0).Rows(0).Item("loginName") & "' ,'" & Request.Form("newPhonePWS1") & "',pwdencrypt('" & Request.Form("newPhonePWS1") & "') ,0 ,'" & sSubUserid & "' ,pwdencrypt('" & Request.Form("newPhonePWS1") & "') ,0)"

            'Dim sSql As String = "INSERT INTO yunUserLoginName] ([Userid] ,[loginName] ,[loginPhone] ,[loginPhonePWS] ,[loginState],[subUserID] ,[subUserPWS] ,[admin],[regTime]) VALUES ('" & dsLoginInfo.Tables(0).Rows(0).Item("Userid") & "','" & dsLoginInfo.Tables(0).Rows(0).Item("Userid") & "','" & dsLoginInfo.Tables(0).Rows(0).Item("Userid") & "','" & Request.Form("newPhone") & "','" & dsLoginInfo.Tables(0).Rows(0).Item("Userid") & "',0,'','',0)"
            mdb.Write(sSQL)
            tishiPhone = "增加手机号成功"
        End If
    End Sub

    Protected Sub btnExitLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitLogin.Click
        Session.Abandon()
        Response.Cookies("myLoginName").Expires = Now.AddDays(-1)
        Response.Redirect("default.aspx")
    End Sub


End Class
