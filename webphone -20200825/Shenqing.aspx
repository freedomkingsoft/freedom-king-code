﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Shenqing.aspx.vb" Inherits="Shenqing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>代理商申请</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />
</head>
<body style="padding-top: 50px; overflow-x: hidden;">
    <div class="fixed_topw">
        <div class="self_info">
            <img src="images/FKLogo.png" alt="logo" /></div>
        <ul>
            <li>
                <h1>
                    <a href="default.aspx">申请手机号：
                        <%=loginPhone %>
                    </a>
                </h1>
            </li>
        </ul>
    </div>
    <div id="box" class="container stage-setting">
        <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
    <div class="title-group">
      <h1 class="title txt-cut">提交资料</h1>
    </div>
  </header>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
            <h3 class="title mct-b txt-xs" data-node="gTitle">
            </h3>
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="boxId_1589690143613_3">
                    <a href="#" class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="mct-a ">个人资料</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span></a>
                </div>
            </div>
        </div>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
        </div>
        <form runat="server" id="shenqing" method="post" >
            <div class="card11 card-combine" data-node="group" id="Div14">
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div15">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut" style="text-align: right">
                                <span class="mct-a "><a href="shenqing.aspx">申请成为代理商 </a></span><span class="mct-a ">
                                    <a href="dlCenter.aspx">代理商管理入口</a></span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div3">
                <h3 class="title mct-c txt-xs" data-node="gTitle">
                    基本资料
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div4">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span16" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    当前登录手机： </span><span class="mct-a" style="text-align: right">
                                        <%=loginPhone %>
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><span data-node="arrow" class="plus plus-s">
                                >><i class="icon-font icon-font-arrow-right txt-s"> </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="shenheren" class="mct-a " style="width: 30%; display: -moz-inline-box;
                                    display: inline-block;">申请人名称： </span><span class="mct-a" style="text-align: right;width:62%">
                                        <input name="shenqingren" value="<%=shenqingren %>" id="TextBox4" placeholder="填写公司名称或个人姓名" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box">
                                <div class="picker">
                                    <%--<p>
                                    设置宽度</p>--%>
                                    <div class="demo" id="demo-3">
                                    </div>
                                    <%--<div>
                                    <button class="reset">
                                        重置</button>
                                </div>
      --%>
                                </div>
                                <div class="console" style="display: none">
                                    <p>
                                        选择结果：</p>
                                    <pre></pre>
                                    <input id="selectResult" name="selectResult" type="text" />
                                </div>

                                <script src="static/js/jquery.min.js"></script>

                                <script src="static/js/iPicker.min.js"></script>

                                <script>
            var $pre = $( "pre" );
            var $picker = $( ".picker" );
            $.getJSON( "static/json/area.json" ).done(function ( res ) {
               $( "#demo-3" ).iPicker({
                    data: res,
                    width: 120,
                    onSelect: function ( v, t, set ) {
                        var text = "[ " + t + " ]";
                        var value = "[ " + v + " ]";
//                        $pre.eq( 0 ).html( text + "<br><br>" + value );
                            $pre.eq( 0 ).html( text );
                           document.getElementById("selectResult").setAttribute("value",text);                                 
                    }
                });
               
            })
            $( "button.reset" ).each(function ( i ) {
                $( this ).click(function () {
                    $( this ).parents( ".picker" ).next().find( "pre" ).empty();
                    $picker.eq( i ).children( ".demo" ).iPicker( "reset" );
                })
            })
                                </script>

                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span1" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    详细地址： </span><span class="mct-a" style="text-align: right">
                                        <input name="xiangxidizhi"  value="<%=xiangxidizhi %>" id="Text2" placeholder="填写所在详细地址" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span2" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    微信号码： </span><span class="mct-a" style="text-align: right">
                                        <input name="weixin" value="<%=weixin %>" id="Text3"  placeholder="填写您的微信账号" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span3" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    QQ号码： </span><span class="mct-a" style="text-align: right">
                                        <input name="qqhao" value="<%=qqhao %>" id="Text4"  placeholder="填写您QQ号码" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span7" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    职业： </span><span class="mct-a" style="text-align: right">
                                        <input name="zhiye"  value="<%=zhiye %>"  id="Text7" placeholder="填写您的职业"  class="ui-autocomplete-input" />
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span5" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    个人情况说明： </span><span class="mct-a" style="text-align: right">
                                        <textarea name="gerenshuoming"   id="TextArea1" cols="25" rows="2"  placeholder="填写您的个人情况" ><%=gerenshuoming %></textarea>
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span6" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    申请进度： </span><span class="mct-a" style="text-align: right">
                                        <input name="jindu"  value="<%=jindu %>" id="jindu" class="ui-autocomplete-input"  readonly="readonly"/>
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span4" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    反馈说明： </span><span class="mct-a" style="text-align: right">
                                        <input name="fankui" value="<%=fankui %>" id="fank" class="ui-autocomplete-input" readonly="readonly" />
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span9" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    反馈时间： </span><span class="mct-a" style="text-align: right">
                                        <input name="fankuidate" value="<%=fankuidate %>" id="Text1" class="ui-autocomplete-input" readonly="readonly" />
                                    </span>
                            </div>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span8" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                </span><span class="mct-a" style="text-align: right">
                                    <asp:Button ID="btnSubmit" runat="server" Text="提交申请" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Div2">
            </div>
            <div class="layout-box" data-act-type="hover">
                <div class="box-col txt-cut">
                    <span id="Span17" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                        <%=tishiMM %>
                    </span><span class="mct-b" style="text-align: right">
                        <asp:Button ID="btnExitLogin" runat="server" Text="退出当前登录" />
                    </span>
                </div>
                <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                </i></span>
            </div>
        </form>
        <div class="card card6" id="boxId_1589690143613_13">
            <a href="#top" data-act-type="hover" class=" btn-red">返回顶部 </a>
        </div>
        <div id="boxId_1589690143613_14">
        </div>
        <div class="card card6" id="Div5">
            <a href="#top" data-act-type="hover" class=" btn-red"></a>
        </div>
        <div id="Div6">
        </div>
    </div>
    <div class="footer">
            <ul>
                <li class="active"><a href="main.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\home.png" /></i></p>
                    <h4>
                        首页</h4>
                </a></li>
                <li><a href="download.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\down.png" /></i></p>
                    <h4>
                        下载</h4>
                </a></li>
                <li><a href="help.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\help.png" /></i></p>
                    <h4>
                        帮助</h4>
                </a></li>
                <li><a href="mycenter.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\mine.png" /></i></p>
                    <h4>
                        我的</h4>
                </a></li>
            </ul>
        </div>
</body>
</html>
