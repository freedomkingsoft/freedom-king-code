﻿
Partial Class BaoBiao
    Inherits System.Web.UI.Page

    Dim dsLoginInfo As Data.DataSet    
    Public getminDate As String
    Public getmaxDate As String

    Public templeteTable As String = ""
    Public HideTiaoJian As String = "block"

    Public sHideBianHao As String = "none"
    Public sHideKeMu As String = "none"
    Public sHideXingHao As String = "none"

    Public sTitleTiaoJian As String = ""

    Public fromBtn As String = "false"

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            If IsNothing(Request.Params("ID")) Then
                Response.Redirect("Main.aspx")
            End If
            HideTJ()

            getminDate = Now.Year & "-01-01"
            getmaxDate = Now.ToShortDateString

            If Page.IsPostBack = False Then
                getBaoBiao(Request.Params("ID"))
            End If

        End If
    End Sub

    Public listKeMu As String
    Public listXingHao As String
    Private Sub HideTJ()
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
        Dim dsTJ As Data.DataSet
        dsTJ = mdb.Reader("select TiShi,Lie from ZDYBB where ID=" & Request.Params("ID"))

        Dim sTiShi As String()
        sTiShi = Split(dsTJ.Tables(0).Rows(0).Item(0).ToString, ",")

        If sTiShi(3) <> "" Then
            sHideBianHao = sTiShi(3)
        End If
        If sTiShi(4) <> "" Then
            sHideKeMu = sTiShi(4)
            Dim dsList As Data.DataSet
            dsList = mdb.Reader("select kmmc from kmdm where  Nian=" & Now.Year & " and SFMJ=1 " & Split(dsTJ.Tables(0).Rows(0).Item("Lie").ToString, ",")(0))
            Dim i As Integer
            Dim sbList As New StringBuilder
            For i = 0 To dsList.Tables(0).Rows.Count - 1
                sbList.Append("""")
                sbList.Append(dsList.Tables(0).Rows(i).Item(0).ToString)
                sbList.Append(""",")
            Next
            listKeMu = sbList.ToString
        End If
        If sTiShi(5) <> "" Then
            sHideXingHao = sTiShi(5)
            Dim dsList As Data.DataSet
            dsList = mdb.Reader("select kmmc from kmdm where  Nian=" & Now.Year & " and SFMJ=1 " & Split(dsTJ.Tables(0).Rows(0).Item("Lie").ToString, ",")(1))
            Dim i As Integer
            Dim sbList As New StringBuilder
            For i = 0 To dsList.Tables(0).Rows.Count - 1
                sbList.Append("""")
                sbList.Append(dsList.Tables(0).Rows(i).Item(0).ToString)
                sbList.Append(""",")
            Next
            listXingHao = sbList.ToString
        End If

    End Sub

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & System.Configuration.ConfigurationManager.AppSettings("DATASOURCE")
        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    'Public ItemTemplate As String
    Private Sub getBaoBiao(ByVal BaoBiaoID As Integer)
        Dim dsBaoBiao As New Data.DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim spara As New StringBuilder
        spara.Append("@MinDate")
        spara.Append(",")
        If fromBtn = "false" Then
            'If Request.Form.Item("fromBtn") = "false" OrElse IsNothing(Request.Form.Item("fromBtn")) Then
            spara.Append(Now.Year.ToString & "-01-01")
        Else
            spara.Append(Request.Form.Item("minDate"))
        End If
        spara.Append(";")

        spara.Append("@MaxDate")
        spara.Append(",")
        If fromBtn = "false" Then
            ' If Request.Form.Item("fromBtn") = "false" OrElse IsNothing(Request.Form.Item("fromBtn")) Then
            spara.Append(Now.Year.ToString & "-" & Now.Month.ToString & "-" & Now.Day.ToString)
        Else
            spara.Append(Request.Form.Item("maxDate"))
        End If
        spara.Append(";")

        spara.Append("@BianHao")
        spara.Append(",")
        If fromBtn = "false" Then
            ' If Request.Params("fromBtn") = "false" OrElse IsNothing(Request.Form.Item("fromBtn")) Then
            spara.Append(Request.Params("ck"))
            sTitleTiaoJian = Request.Params("ck")

        Else
            spara.Append(Request.Form.Item("bianhao"))
            sTitleTiaoJian = Request.Form.Item("bianhao")

        End If
        spara.Append(";")


        spara.Append("@KeMu")
        spara.Append(",")
        If fromBtn = "false" Then
            'If Request.Params("fromBtn") = "false" OrElse IsNothing(Request.Form.Item("fromBtn")) Then
            spara.Append(Request.Params("km"))
        Else
            spara.Append(Request.Form("kemu"))
        End If
        spara.Append(";")

        spara.Append("@Xinghao")
        spara.Append(",")
        If fromBtn = "false" Then
            'If Request.Params("fromBtn") = "false" OrElse IsNothing(Request.Form.Item("fromBtn")) Then
            spara.Append(Request.Params("xh"))
        Else
            spara.Append(Request.Form("xinghao"))
        End If

        'spara.Append(";")

        'Dim sss As Object = Request.Form
        Dim sp As String = spara.ToString

        Dim sproduce As String
        Dim sLieWidth As String()
        Dim dsZdybb As Data.DataSet
        dsZdybb = mdb.Reader("select MRTiaoJian,liesetup from ZDYBB where ID=" & BaoBiaoID)
        sproduce = dsZdybb.Tables(0).Rows(0).Item(0).ToString
        sLieWidth = Split(dsZdybb.Tables(0).Rows(0).Item(1).ToString, ",")
        Try
            dsBaoBiao = mdb.ReadStoredProcedure(getConnectString, sproduce, sp)
        Catch ex As Exception
            Exit Sub
        End Try

        If dsBaoBiao.Tables.Count = 0 Then
            Exit Sub
        End If

        Dim iBiao As Integer

        'Dim templeteTable As String
        For iBiao = 0 To dsBaoBiao.Tables.Count - 1
            templeteTable = templeteTable & "<div class=""card11 card-combine"" data-node=""group"" id=""boxId_1589690143613_8"">"

            Dim sBiaoTou As New StringBuilder
            Dim sBaoBiao As New StringBuilder
            If dsBaoBiao.Tables(iBiao).Rows.Count > 0 Then
                Dim i As Integer

                '编写报表表头行
                sBiaoTou.Append("<div data-node=""cardList"" class=""card-list"">")
                sBiaoTou.Append(" <div class=""card card4 line-around"" id=""boxId_1589690143613_9"">")
                sBiaoTou.Append("<div class=""layout-box"" data-act-type=""hover"">")
                sBiaoTou.Append("<div class=""box-col txt-cut""> ")
                For i = 0 To dsBaoBiao.Tables(iBiao).Columns.Count - 1
                    sBiaoTou.Append("<span class=""span-table"" style=""width:")
                    sBiaoTou.Append(sLieWidth(i * 4 + 2))
                    sBiaoTou.Append("%"" >")
                    If iBiao = 1 OrElse dsBaoBiao.Tables.Count = 1 Then
                        sBiaoTou.Append(sLieWidth(i * 4 + 1))
                    Else
                        sBiaoTou.Append(dsBaoBiao.Tables(iBiao).Columns(i).ColumnName)
                    End If
                    sBiaoTou.Append("</span>")
                Next
                sBiaoTou.Append("</div> </div> </div></div>")
                sBiaoTou.Append("")

                Dim n As Integer
                '编写报表内容

                For n = 0 To dsBaoBiao.Tables(iBiao).Rows.Count - 1
                    sBaoBiao.Append("<div data-node=""cardList"" class=""card-list"">")
                    sBaoBiao.Append(" <div class=""card card4 line-around"" id=""boxId_1589690143613_9"">")
                    If n Mod 2 = 0 Then
                        sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:" & System.Configuration.ConfigurationManager.AppSettings("backgroudcolorA") & """>")
                    Else
                        sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:" & System.Configuration.ConfigurationManager.AppSettings("backgroudcolorB") & """>")
                    End If

                    sBaoBiao.Append("<div class=""box-col txt-cut""> ")

                    For i = 0 To dsBaoBiao.Tables(iBiao).Columns.Count - 1
                        If sLieWidth(i * 4 + 1).Contains("编号") Then
                            sBaoBiao.Append("<a href=""detail.aspx?bh=")
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append(""">")

                            sBaoBiao.Append("<span class=""span-table"" style=""width:")
                            sBaoBiao.Append(sLieWidth(i * 4 + 2))
                            sBaoBiao.Append("%"">")
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append("</span> ")

                            sBaoBiao.Append("</a>")
                        ElseIf sLieWidth(i * 4 + 1).Contains("客户") Then
                            sBaoBiao.Append("<a href=""BaoBiao.aspx?id=73&name=应收明细账&km=")

                            'sKemuValue = dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append(""">")

                            sBaoBiao.Append("<span class=""span-table"" style=""width:")
                            sBaoBiao.Append(sLieWidth(i * 4 + 2))
                            sBaoBiao.Append("%"">")
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append("</span> ")

                            sBaoBiao.Append("</a>")
                        ElseIf sLieWidth(i * 4 + 1).Contains("供应商") Then
                            sBaoBiao.Append("<a href=""BaoBiao.aspx?id=74&name=应付明细账&km=")
                            'sKemuValue = dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append(""">")

                            sBaoBiao.Append("<span class=""span-table"" style=""width:")
                            sBaoBiao.Append(sLieWidth(i * 4 + 2))
                            sBaoBiao.Append("%"">")
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append("</span> ")

                            sBaoBiao.Append("</a>")
                        ElseIf sLieWidth(i * 4 + 1).Contains("型号") OrElse sLieWidth(i * 4 + 1).Contains("商品") Then
                            sBaoBiao.Append("<a href=""BaoBiao.aspx?id=75&name=库存商品明细账&ck=" & sTitleTiaoJian & "&xh=")
                            'sXinghaoValue = dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append(""">")

                            sBaoBiao.Append("<span class=""span-table"" style=""width:")
                            sBaoBiao.Append(sLieWidth(i * 4 + 2))
                            sBaoBiao.Append("%"">")
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append("</span> ")

                            sBaoBiao.Append("</a>")
                        ElseIf sLieWidth(i * 4 + 1).Contains("账户") Then
                            sBaoBiao.Append("<a href=""BaoBiao.aspx?id=72&name=资金明细账&ck=" & sTitleTiaoJian & "&km=")
                            'sXinghaoValue = dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append(""">")

                            sBaoBiao.Append("<span class=""span-table"" style=""width:")
                            sBaoBiao.Append(sLieWidth(i * 4 + 2))
                            sBaoBiao.Append("%"">")
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append("</span> ")

                            sBaoBiao.Append("</a>")
                        Else
                            sBaoBiao.Append("<span class=""span-table"" style=""width:")
                            sBaoBiao.Append(sLieWidth(i * 4 + 2).ToString)
                            sBaoBiao.Append("%"">")
                            sBaoBiao.Append(dsBaoBiao.Tables(iBiao).Rows(n).Item(i).ToString)
                            sBaoBiao.Append("</span> ")
                        End If

                    Next
                    sBaoBiao.Append("</div> ")
                    sBaoBiao.Append("</div> ")
                    sBaoBiao.Append(" </div></div>")
                    sBaoBiao.Append("")
                    sBaoBiao.Append("")

                Next

            End If
            templeteTable = templeteTable & sBiaoTou.ToString & sBaoBiao.ToString

            templeteTable = templeteTable & "</div>"
        Next

        HideTiaoJian = "none"
        btnHide.Text = "显示条件"

    End Sub

    Protected Sub btnChaXun_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChaXun.Click
        fromBtn = "true"
        getBaoBiao(Request.Params("ID"))
    End Sub

    Protected Sub btnHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHide.Click
        If Request.Form("isShow") = "block" Then
            btnHide.Text = "显示条件"
            HideTiaoJian = "none"
        Else
            btnHide.Text = "隐藏条件"
            HideTiaoJian = "block"
        End If
    End Sub
End Class
