﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="detail.aspx.vb" Inherits="detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%--<link rel="apple-touch-icon" href="http://u1.sinaimg.cn/upload/h5/img/apple-touch-icon.png">--%>
    <title>查看单证</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />

    <script src="js/datePicker.js" type="text/javascript"></script>

    <script type="text/javascript">
/* 
* 取得对应类和标签的HTML元素 
* clsName:给定类名 
* tagName：给定的专HTML元素，如果属为任意 tagName='*' 
*  
*/  
function getElementsByClassName(clsName, tagName) {  
    var ClassElements = [];  
    selElements = document.getElementsByTagName(tagName);  
   
    for (var i = 0; i < selElements.length; i++) {  
        if (selElements[i].className == clsName) { 
         
             selElements[i].innerText=cutZero(selElements[i].innerText);
             selElements[i].innerText=cutDateZero(selElements[i].innerText);

//            ClassElements[ClassElements.length] = selElements[i];  
        }  
    }  
//    console.log(ClassElements);
//    return ClassElements;  
}
function cutDateZero(old){
         newstr=old;
        if(old.indexOf("00:00:00")>-1){
            return newstr.replace("00:00:00","").replace(" ","")
        }
         if(old.indexOf("0:00:00")>-1){
            return newstr.replace("0:00:00","").replace(" ","")
        }
        return old
}
function cutZero(old){
         //拷贝一份 返回去掉零的新串
         newstr=old;
         //循环变量 小数部分长度
         var leng = old.length-old.indexOf(".")-1
         //判断是否有效数
         if(old.indexOf(".")>-1){
            //循环小数部分
            for(i=leng;i>0;i--){
                 //如果newstr末尾有0
                 if(newstr.lastIndexOf("0")>-1 && newstr.substr(newstr.length-1,1)==0){
                     var k = newstr.lastIndexOf("0");
                     //如果小数点后只有一个0 去掉小数点
                     if(newstr.charAt(k-1)=="."){
                         return  newstr.substring(0,k-1);
                     }else{
                     //否则 去掉一个0
                         newstr=newstr.substring(0,k);
                     }
                 }else{
                 //如果末尾没有0
                     return newstr;
                 }
             }
         }
         return old;
      }
    </script>

</head>
<body style="padding-top: 50px; overflow-x: hidden;" onload="getElementsByClassName('span-table','span')">
    <div class="fixed_topw">
        <div class="self_info">
            <img src="images/FKLogo.png" alt="logo" /></div>
        <ul>
            <li>
                <h1>
                    <a href="default.aspx">
                        <%=Session("QiYeName")%>
                    </a>
                </h1>
            </li>
        </ul>
    </div>
    <div id="box" class="container stage-setting">
        <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
    <div class="title-group">
      <h1 class="title txt-cut">查看单证</h1>
    </div>
  </header>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
            <h3 class="title mct-b txt-xs" data-node="gTitle">
            </h3>
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="boxId_1589690143613_3">
                    <a href="#" class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="mct-a ">个人资料</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span></a>
                </div>
            </div>
        </div>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
            <%=ItemTemplate%>
        </div>
        <form runat="server" id="shenhe" method="post">
            <div class="card11 card-combine" data-node="group" id="Div3">
                <h3 class="title mct-b txt-xs" data-node="gTitle">
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div4">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="shenheren" class="mct-a " style="width: 50%; display: -moz-inline-box;
                                    display: inline-block;" data-shenhe="<%=shenheren %>">审核人：<%=shenheren %>
                                </span>
                                <input name="inShenheren" type="hidden" value="<%=shenheren %>" />
                                <span class="mct-a" style="text-align: right">审核结果：<%= shenhejieguo %></span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div1">
                <h3 class="title mct-b txt-xs" data-node="gTitle">
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div9">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span1" class="mct-a " style="width: 50%; display: -moz-inline-box; display: inline-block;">
                                    <asp:Button ID="btnShenHe" Text="审核此业务" runat="server" />
                                </span><span class="mct-a" style="text-align: right">
                                    <asp:Button ID="btnQuXiao" Text="取消审核" runat="server" /></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div7">
                <h3 class="title mct-b txt-xs" data-node="gTitle">
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div8">
                        <div class="box-col txt-cut">
                        </div>
                    </div>
                </div>
            </div>
            <div id="Div2">
            </div>
        </form>
        <div class="card card6" id="boxId_1589690143613_13">
            <a href="#top" data-act-type="hover" class=" btn-red">返回顶部 </a>
        </div>
        <div id="boxId_1589690143613_14">
        </div>
        <div class="card card6" id="Div5">
            <a href="#top" data-act-type="hover" class=" btn-red"></a>
        </div>
        <div id="Div6">
        </div>
    </div>
    <div class="footer">
            <ul>
                <li class="active"><a href="main.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\home.png" /></i></p>
                    <h4>
                        首页</h4>
                </a></li>
                <li><a href="download.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\down.png" /></i></p>
                    <h4>
                        下载</h4>
                </a></li>
                <li><a href="help.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\help.png" /></i></p>
                    <h4>
                        帮助</h4>
                </a></li>
                <li><a href="mycenter.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\mine.png" /></i></p>
                    <h4>
                        我的</h4>
                </a></li>
            </ul>
        </div>
</body>
</html>
