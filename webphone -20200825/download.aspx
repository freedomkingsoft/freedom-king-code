﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="download.aspx.vb" Inherits="Download" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="http://u1.sinaimg.cn/upload/h5/img/apple-touch-icon.png">
    <title>下载中心</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />
</head>
<body style="padding-top: 50px; overflow-x: hidden;">
    <form id="Form1" runat="server">
        <div class="fixed_topw">
            <div class="self_info">
                <img src="images/FKLogo.png" alt="logo" /></div>
            <ul>
                <li>
                    <h1>
                        <a href="default.aspx">
                            <%=sQiYeName%>
                        </a>
                    </h1>
                </li>
            </ul>
        </div>
        <div id="box" class="container stage-setting">
            <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
      <div class="title-group">
        <h1 class="title txt-cut"> 下载中心</h1>
      </div>
    </header>
            <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
                <h3 class="title mct-b txt-xs" data-node="gTitle">
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="boxId_1589690143613_3">
                        <a href="/users/2368840273?set=1" class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a "></span>此段不显示
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span></a>
                    </div>
                </div>
            </div>
            
            <div class="card card6" id="Div1">
               <p style="margin:1.2rem"> 自由王进销存财务一体化系统有电脑端和手机端，日常操作通过电脑端进行。</p>
                <p style="margin:1.2rem"> <a title="马上下载自由王云进销存财务一体化系统" href="http://www.bdsp.top/download/自由王云进销存财务一体化.zip" target="_self">现在下载电脑端压缩包到手机</a></p>
               <p style="margin:1.2rem"> 请在电脑上访问 <a href ="http://www.bdsp.top">http://www.bdsp.top</a> ,点击首页广告下载电脑端压缩包。</p>
               <p style="margin:1.2rem"> 该系统是一款绿色软件，解压缩即可使用，无需安装。</p>
               <p style="margin:1.2rem"> 主程序为FreedomKing.exe ，双击打开即可启动程序。</p>
                <p style="margin:1.2rem">手机号登录之后，可以选择用户进行操作，默认管理员密码为： 888888 ，默认出纳密码为： 888888</p>

             <p style="margin:1.2rem"> <a title="马上下载自由王云进销存财务一体化系统" href="http://www.bdsp.top/download/自由王云进销存财务一体化.zip" target="_self">现在下载电脑端压缩包到手机</a></p>
              
              <p style="margin:1.2rem"> <a title="详细使用说明及常见问题请参考帮助" href="help.aspx" target="_self">详细使用说明及常见问题请参考帮助</a></p>
                
                
            </div>
            <%--<div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
                        <div data-node="cardList" class="card-list">
                            <div class="card card4 line-around" id="boxId_1589690143613_9">
                            电脑端程序下载
                                <a href="BaoBiao.aspx"
                                    class="layout-box" data-act-type="hover">
                                    <div class="box-col txt-cut">
                                        <span class="mct-a ">
                                          电脑端程序下载
                                        </span><span class="mct-b ">
                                          说明
                                        </span>
                                    </div>
                                    <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                                        》</i> </span></a>
                            </div>
                        </div>
            </div>--%>
            <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_10">
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="boxId_1589690143613_11">
                        <a href="http://www.bdsp.top" class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a ">官网首页</span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span></a>
                    </div>
                    <div class="card card4 line-around" id="boxId_1589690143613_12">
                        <a href="feedback.aspx" class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a ">意见反馈</span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span></a>
                    </div>
                </div>
            </div>
            <div class="card card6" id="boxId_1589690143613_13">
                <a href="#box" data-act-type="hover" class=" btn-red">返回顶部 </a>
            </div>
            <div id="boxId_1589690143613_14">
            </div>
        </div>
        <div class="footer">
            <ul>
                <li class="active"><a href="main.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\home.png" /></i></p>
                    <h4>
                        首页</h4>
                </a></li>
                <li><a href="download.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\down.png" /></i></p>
                    <h4>
                        下载</h4>
                </a></li>
                <li><a href="help.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\help.png" /></i></p>
                    <h4>
                        帮助</h4>
                </a></li>
                <li><a href="mycenter.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\mine.png" /></i></p>
                    <h4>
                        我的</h4>
                </a></li>
            </ul>
        </div>
    </form>
</body>
</html>
