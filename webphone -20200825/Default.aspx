﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<link rel="Stylesheet" href="css/StyleLogin.css" type="text/css" />
<title>欢迎使用自由王软件</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
</head>
<body load="true" pid="cnmail" class="" style="min-height: 600px;">
<section class="login-wrap"><span id="mailFnCoverAndroid" class="app_banner"><a id="callAndrBtn" _href="#mailFnCoverAndroid" class="callBtn"><img class="logo_img" src="images/app_banner.png"></a></span><span id="mailFnCoveriOS" class="app_banner" style="display: none;"><a id="callIosBtn" _href="#mailFnCoverAndroid" class="callBtn"><img class="logo_img" src="images/app_banner.png"></a></span>
  <header class="login-header">
    <h1 class="logo">自由王软件登录</h1>
  </header>
  <section class="login-content">
    <div class="login-box" style="margin-bottom:10px;">
      <form id="Form1" method="POST" action="Default.aspx" runat="server">
        <span class="tip"><span></span></span>
        <fieldset>
        <asp:Label ID="legend" runat="server" Text="用户名或密码错误" class="legend"></asp:Label>
        <ul class="login-items">
          <li class="item-username">
            <input name="username" id="uName" class="input-txt" placeholder="体验手机号：13012345678" spellcheck="false" autocomplete="off" autocorrect="off" autocapitalize="off" maxlength="256" type="text" >
            <div class="dark-bg login-suggestor" style="display: none; overflow: hidden; position: absolute; width: 249px;">
              <ul>
              </ul>
            </div>
            <label class="input-label" for=""><b class="ico ico-mail"></b></label>
          </li>
          <li class="item-password">
            <input name="passwd" class="input-txt" type="password" placeholder="体验密码：123456" maxlength="256">
            <label class="input-label" for=""><b class="ico ico-lock"></b></label>
          </li>
          <li class="item-captchap captchapanel" style="display: none;">
            <input name="captcha" class="input-txt" type="text" placeholder="验证码" maxlength="5" spellcheck="false" autocomplete="off" autocorrect="off" autocapitalize="off">
            <a node-type="loadcaptcha" href="#">看不清？</a>
            <label class="input-label" for=""><b class="ico ico-code"></b></label>
          </li>
          <li class="item-option clearfix">
            <label class="label savelogin savelogin-on J_cardHidden" for="">
            <input type="hidden" name="savelogin" value="1">
            <b class="ico ico-check2"></b>保持登录状态</label>
            <a class="forget-password" id="forgetPass" suda-uatrack="key=mail_61&amp;value=4" href="https://www.bdsp.top/iforgot.aspx" target="_blank" title="忘记密码">忘记密码</a></li>
          <li class="item-act">
            <asp:Button ID="btnSubmit" runat="server" Text="登 录" class="btn-b btn-submit btnLog"></asp:Button>
          </li>
        </ul>
        </fieldset>
      </form>
    </div>
    <div class="changeUrl" style="width: 248px;margin: 0 auto;">
      <div style="float:right;"><a class="link-vip" suda-uatrack="key=mail_61&amp;value=7" href="https://www.bdsp.top/help.aspx" title="帮助中心">帮助中心</a><a class="link-cn" suda-uatrack="key=mail_61&amp;value=12" href="https://www.bdsp.top/help.aspx" title="帮助中心">帮助中心</a></div>
      <div style="float:left;"><a class="link-vip" suda-uatrack="key=mail_61&amp;value=6" href="download.aspx" target="_blank">立即下载</a><a class="link-cn" suda-uatrack="key=mail_61&amp;value=15" href="download.aspx" target="_blank">立即下载</a></div>
    </div>
   
  </section>
</section>
<div />
<footer id="login-footer" class="show">
  <nav class="footer-nav">
    <div class="nav-list version"><a href="http://www.bdsp.top" target="_blank" title="网页版">网页版</a></div>
    <div class="nav-list act"><a href="http://www.bdsp.top/feedback.html" target="_blank">意见反馈</a></div>
    <div class="nav-list act"><a>公用体验手机号 13012345678 密码 123456</a> &nbsp;&nbsp;&nbsp;&nbsp;</div>
  </nav>
</footer>
</body>
</html>
