﻿
Partial Class clientDetail
    Inherits System.Web.UI.Page

    Public dsLoginInfo As Data.DataSet

    Private iNianshu As Integer
    Private iYongHu As Integer

    Public tishiMM As String = "none"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else

            getClientList(dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString)
            If Page.IsPostBack = False Then
                iNianshu = 1
                iYongHu = 1
                jisuanJiaGe()
            End If
        End If
    End Sub


    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Public templeteTable As String
    ' Public itemtemplate As Data.DataSet
    Public sGenZong As String

    Private Sub getClientList(ByVal dlPhone As String)
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)

        Dim dsClientList As Data.DataSet

        Try
            'Dim sSQL As String = "select  yunUser.regPhone as 客户注册手机,yunUser.comName as 客户名称,genzongLog as 跟踪进度,VIP,vipmaturitydate as VIP到期日,freeenddate as 试用到期日,yunUser.userLoginID as 客户ID,yunUser.regDate as 注册日期, diqu as 地区,dizhi as 详细地址,EMail from dltuijian,yunUser where dltuijian.yunuserphone=yunUser.regPhone and yunUser.regPhone='" & Request.Params("clientPhone").ToString & "'"
            dsClientList = mdb.Reader("select  yunUser.regPhone as 客户注册手机,yunUser.comName as 客户名称,VIP,vipmaturitydate as VIP到期日,freeenddate as 试用到期日,yunUser.userLoginID as 客户ID,yunUser.regDate as 注册日期, diqu as 地区,dizhi as 详细地址,EMail,genzongLog as 跟踪进度 from dltuijian,yunUser where dltuijian.yunuserphone=yunUser.regPhone and yunUser.regPhone='" & Request.Params("clientPhone").ToString & "'")

            'clientList.DataSource = dsClientList
            'clientList.DataBind()

        Catch ex As Exception
            Exit Sub
        End Try

        If dsClientList.Tables(0).Rows.Count > 0 Then
            sGenZong = dsClientList.Tables(0).Rows(0).Item("跟踪进度").ToString

            Dim i As Integer
            Dim n As Integer
            Dim sBaoBiao As New StringBuilder
            '编写报表内容

            For i = 0 To dsClientList.Tables(0).Columns.Count - 2
                sBaoBiao.Append("<div data-node=""cardList"" class=""card-list"">")
                sBaoBiao.Append(" <div class=""card card4 line-around"" id=""boxId_1589690143613_9"">")
                If i Mod 2 = 0 Then
                    sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:azure"">")
                Else
                    sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:cornsilk"">")
                End If
                sBaoBiao.Append("<div class=""box-col txt-cut""> ")

                sBaoBiao.Append("<span class=""span-table"" style=""width:")
                sBaoBiao.Append("40")
                sBaoBiao.Append("%"">")
                sBaoBiao.Append(dsClientList.Tables(0).Columns(i).ColumnName)
                sBaoBiao.Append("</span> ")

                For n = 0 To dsClientList.Tables(0).Rows.Count - 1
                    sBaoBiao.Append("<span class=""span-table"" style=""width:")
                    sBaoBiao.Append("55")
                    sBaoBiao.Append("%"">")
                    sBaoBiao.Append(dsClientList.Tables(0).Rows(n).Item(i).ToString)
                    sBaoBiao.Append("</span> ")

                Next
                sBaoBiao.Append("</div> ")
                sBaoBiao.Append("</div> ")
                sBaoBiao.Append(" </div></div>")
            Next

            'sBaoBiao.Append(" <div data-node=""cardList"" class=""card-list""> <div class=""card card4 line-around"" ><div class=""layout-box"" data-act-type=""hover""><div class=""box-col txt-cut""> <span class=""span-table"" style=""width:85%"">下一页</span>  </div> </div>  </div></div>")


            templeteTable = sBaoBiao.ToString
        End If

    End Sub


    Protected Sub btnGenZong_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenZong.Click
        Dim sGenZong As String
        sGenZong = Request.Form("genzonglog").ToString
        If sGenZong = "" Then
        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
            sGenZong = Date.Today.ToShortDateString & ":" & sGenZong & "<P>"
            Dim ssql As String = "update dltuijian set genzongLog=genzongLog+'" & sGenZong & "' where yunUserPhone='" & Request.Params("clientPhone").ToString & "'"
            mdb.Write("update dltuijian set genzongLog=isnull(genzongLog,'')+'" & sGenZong & "' where yunUserPhone='" & Request.Params("clientPhone").ToString & "'")
            Response.Redirect("clientDetail.aspx?clientPhone=" & Request.Params("clientPhone").ToString)
        End If
    End Sub

    Public FeiYong As Decimal = 0

    Protected Sub btnJiaoFei_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJiaoFei.Click
        Dim mJFJinE As Decimal
        iNianshu = ddNian.Text
        iYongHu = ddYongHu.Text
        mJFJinE = jisuanJiaGe()
        tishiMM = "你已选择为用户 " & Request.Params("clientPhone").ToString & " 进行续费，将从你的账户中扣除 " & mJFJinE & " 元，点击确认续费将扣费。"
    End Sub

    Private Function jisuanJiaGe() As Decimal
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim dsDaiLi As Data.DataSet
        dsDaiLi = mdb.Reader("select * from dllist,dljibie where dlList.dlJiBie=dlJiBie.ID and dlList.dlPhone='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'")
        If dsDaiLi.Tables(0).Rows.Count = 0 Then
            Return 99999
        Else
            Dim nZheKou As Decimal = 0
            '计算所需费用，费用=代理价格 * 年数 -  代理价格 * 年数 * 折扣 + （用户数 - 1）*50 * 年数
            Dim dlJiaGe As Decimal
            Dim dZheKou As Decimal

            Dim iMoRenYongHu As Integer
            Dim dXinYongEDu As Decimal

            dlJiaGe = dsDaiLi.Tables(0).Rows(0).Item("jiage")
            dXinYongEDu = dsDaiLi.Tables(0).Rows(0).Item("xinYongEdu")
            dZheKou = System.Configuration.ConfigurationManager.AppSettings("zhekou")
            iMoRenYongHu = System.Configuration.ConfigurationManager.AppSettings("morenYongHu")

            '计算余额,各个代理分别都有各自额度
            Dim dsYuE As Data.DataSet
            dsYuE = mdb.Reader("select isnull(SUM(jiefang)-SUM(daifang),0) as YuE from dlShouZhi where dlphone='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'")
            Dim dYuE As Decimal
            dYuE = dsYuE.Tables(0).Rows(0).Item(0)

            Dim dKouFeiJinE As Decimal
            dKouFeiJinE = dlJiaGe * iNianshu * (1 - dZheKou) + (Math.Max(iMoRenYongHu, iYongHu) - iMoRenYongHu) * 50 * iNianshu

            FeiYong = dKouFeiJinE
            If dKouFeiJinE <= Math.Max(dXinYongEDu, dYuE) Then

            Else
                '余额不足
                ' btnJiaoFei.Enabled = False
            End If
            '缴费

            Return FeiYong
        End If

    End Function

    Protected Sub ddNian_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddNian.SelectedIndexChanged
        iNianshu = ddNian.Text
        iYongHu = ddYongHu.Text
        jisuanJiaGe()
    End Sub

    Protected Sub ddYongHu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddYongHu.SelectedIndexChanged
        iYongHu = ddYongHu.Text
        iNianshu = ddNian.Text
        jisuanJiaGe()
    End Sub

    Protected Sub btnQueRenJF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQueRenJF.Click
        FeiYong = Request.Form("TXTFeiYong")

        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim dsDaiLi As Data.DataSet
        dsDaiLi = mdb.Reader("select * from dllist,dljibie where dlList.dlJiBie=dlJiBie.ID and dlList.dlPhone='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'")
        If dsDaiLi.Tables(0).Rows.Count = 0 Then
            tishiMM = "出现异常 10058 ，无法完成缴费"
        Else
           
            Dim dXinYongEDu As Decimal

            dXinYongEDu = dsDaiLi.Tables(0).Rows(0).Item("xinYongEdu")
            '计算余额,各个代理分别都有各自额度
            Dim dsYuE As Data.DataSet
            dsYuE = mdb.Reader("select isnull(SUM(jiefang)-SUM(daifang),0) as YuE from dlShouZhi where dlphone='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'")
            Dim dYuE As Decimal
            dYuE = dsYuE.Tables(0).Rows(0).Item(0)


            If FeiYong <= Math.Max(dXinYongEDu, dYuE) Then
                '进行缴费操作
                'dlshouzhi中扣除费用
                '用户表完成续费
                '代理商和上级代理商提点
                Dim sKouFei As String = "insert into dlShouZhi (shijian,dlPhone,jiefang,daifang,kehuphone,xjdlphone, shuoming) values  ('" & Now.ToString & "','" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "',0," & FeiYong & ",'" & Request.Params("clientPhone") & "','','客户续费') "

                ' Dim sYHXufei As String = "update yunUser set VIP=1,VIPMaturitydate=DATEADD(year," & Me.ddNian.Text & ",VIPMaturitydate) ,by2='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'  where regPhone='" & Request.Params("clientPhone") & "'"
                Dim sYHXufei As String = "update yunUser set VIP=1,VIPMaturitydate=DATEADD(year," & Me.ddNian.Text & ",VIPMaturitydate) ,by2='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "',LoginNumber=" & Me.ddYongHu.Text & "   where regPhone='" & Request.Params("clientPhone") & "'"


                Dim sDLTicheng As String = ""
                '代理可以有权限设置用户自己续费时的价格，价格差为代理提成
                '目前不支持客户自己续费，全部由代理商进行续费

                ' Dim sSJDLTicheng As String = "insert into dlShouZhi (shijian,dlPhone,jiefang,daifang,kehuphone,xjdlphone, shuoming) values  ('" & Now.ToString & "','上级代理电话',0,50,'','" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "','下级代理提成')"
                Try

                    Dim sSJDLPhone As String = mdb.Reader("select dlPhone   from dltuijian where yunUserPhone='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'").Tables(0).Rows(0).Item(0).ToString
                    Dim dEJiJiangLi As Decimal = mdb.Reader("select dlJiBie.erjiJiang  from dlJiBie,dlList  where dlJiBie.ID=dlList.dlJiBie and dlList.dlPhone='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'").Tables(0).Rows(0).Item(0)
                    Dim sSJDLTicheng As String = "insert into dlShouZhi (shijian,dlPhone,jiefang,daifang,kehuphone,xjdlphone, shuoming) values  ('" & Now.ToString & "','" & sSJDLPhone & "'," & dEJiJiangLi & "," & 0 & ",'" & Request.Params("clientPhone") & "','" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "','代理奖励') "

                    mdb.Write(sKouFei)
                    mdb.Write(sYHXufei)
                    mdb.Write(sSJDLTicheng)

                    tishiMM = "续费成功。"
                    Me.btnQueRenJF.Visible = False
                Catch ex As Exception
                    tishiMM = "续费过程出现异常，请联系管理员查看具体原因，错误代码100069 。"
                End Try

            Else
                tishiMM = "余额不足，无法完成缴费，请先进行充值。"

                '余额不足
                btnJiaoFei.Enabled = False
            End If
            '缴费
        End If

    End Sub
End Class