﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKZDZZPZ
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvPingZheng = New System.Windows.Forms.DataGridView
        Me.摘要 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.借方科目 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.贷方科目 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.借方数量 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.借方金额 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.贷方数量 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.贷方金额 = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvPingZheng, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvPingZheng
        '
        Me.dgvPingZheng.AllowUserToResizeColumns = False
        Me.dgvPingZheng.AllowUserToResizeRows = False
        Me.dgvPingZheng.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPingZheng.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.摘要, Me.借方科目, Me.贷方科目, Me.借方数量, Me.借方金额, Me.贷方数量, Me.贷方金额})
        Me.dgvPingZheng.Location = New System.Drawing.Point(13, 138)
        Me.dgvPingZheng.Name = "dgvPingZheng"
        Me.dgvPingZheng.RowHeadersVisible = False
        Me.dgvPingZheng.RowTemplate.Height = 23
        Me.dgvPingZheng.Size = New System.Drawing.Size(824, 187)
        Me.dgvPingZheng.TabIndex = 10
        Me.dgvPingZheng.TabStop = False
        '
        '摘要
        '
        Me.摘要.HeaderText = "摘要"
        Me.摘要.Name = "摘要"
        '
        '借方科目
        '
        Me.借方科目.HeaderText = "借方科目"
        Me.借方科目.Name = "借方科目"
        '
        '贷方科目
        '
        Me.贷方科目.HeaderText = "贷方科目"
        Me.贷方科目.Name = "贷方科目"
        '
        '借方数量
        '
        Me.借方数量.HeaderText = "借方数量"
        Me.借方数量.Name = "借方数量"
        '
        '借方金额
        '
        Me.借方金额.HeaderText = "借方金额"
        Me.借方金额.Name = "借方金额"
        '
        '贷方数量
        '
        Me.贷方数量.HeaderText = "贷方数量"
        Me.贷方数量.Name = "贷方数量"
        '
        '贷方金额
        '
        Me.贷方金额.HeaderText = "贷方金额"
        Me.贷方金额.Name = "贷方金额"
        '
        'FKZDZZPZ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(849, 373)
        Me.Controls.Add(Me.dgvPingZheng)
        Me.Name = "FKZDZZPZ"
        Me.Text = "FKZDZZPZ"
        CType(Me.dgvPingZheng, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvPingZheng As System.Windows.Forms.DataGridView
    Friend WithEvents 摘要 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 借方科目 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 贷方科目 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 借方数量 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 借方金额 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 贷方数量 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 贷方金额 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
