﻿Public Class FKF

    Public Sub New()

    End Sub

    Public Shared Function AutoBianHao(ByVal sQDF As String) As String
        '自动编号可以选择是否带有年份和月份，但最后必须为数字
        Dim iWeishu As Integer

        Dim sBianHao As String
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        iWeishu = FKG.myselfG.iBianHaoWeiShu

        Dim ds As DataSet
        ds = mdb.Reader("select top 1 BianHao from JiBenCaoZuo where Nian=" & FKG.myselfG.NianFen & " and yue=" & FKG.myselfG.YueFen & " and bianhao like '" & sQDF & "%' and LEN(bianhao)=" & sQDF.Length + iWeishu + FKG.myselfG.iNianFenWeiShu + 2 & " and 删除='false' order by Bianhao desc")

        If ds.Tables(0).Rows.Count = 0 Then
            sBianHao = sQDF
            sBianHao = sBianHao & FKG.myselfG.NianFen.ToString.Substring(4 - FKG.myselfG.iNianFenWeiShu, FKG.myselfG.iNianFenWeiShu) & FKG.myselfG.YueFen.ToString.PadLeft(2, "0")
            Dim i As Integer
            For i = 0 To iWeishu - 2
                sBianHao = sBianHao & "0"
            Next
            sBianHao = sBianHao & "1"
        Else
            sBianHao = ds.Tables(0).Rows(0).Item(0)
            Dim i As String
            i = (sBianHao.Substring(sBianHao.Length - iWeishu, iWeishu) + 1).ToString.PadLeft(iWeishu, "0").ToString
            sBianHao = sBianHao.Remove(sBianHao.Length - iWeishu, iWeishu) & i.ToString
        End If
        Return sBianHao
    End Function

    Public Shared Function MaxBianhao(ByVal sQDF As String) As Integer
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select top 1 BianHao from JiBenCaoZuo where Nian=" & FKG.myselfG.NianFen & " and yue=" & FKG.myselfG.YueFen & " and bianhao like '" & sQDF & "%' and LEN(bianhao)=" & sQDF.Length + FKG.myselfG.iBianHaoWeiShu + FKG.myselfG.iNianFenWeiShu + 2 & "  and 删除='false'  order by Bianhao desc")
        Dim ihao As Integer
        Dim sHao As String
        If ds.Tables(0).Rows.Count = 0 Then
            Return 1
        Else
            sHao = ds.Tables(0).Rows(0).Item(0)
            ihao = sHao.Substring(sHao.Length - FKG.myselfG.iBianHaoWeiShu, FKG.myselfG.iBianHaoWeiShu)
            Return ihao
        End If
    End Function

    Public Shared Function MinBianhao(ByVal sQDF As String) As Integer
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select top 1 BianHao from JiBenCaoZuo where Nian=" & FKG.myselfG.NianFen & " and yue=" & FKG.myselfG.YueFen & " and bianhao like '" & sQDF & "%' and LEN(bianhao)=" & sQDF.Length + FKG.myselfG.iBianHaoWeiShu + FKG.myselfG.iNianFenWeiShu + 2 & "  and 删除='false' order by Bianhao")
        Dim ihao As Integer
        Dim sHao As String
        If ds.Tables(0).Rows.Count = 0 Then
            Return 1
        Else
            sHao = ds.Tables(0).Rows(0).Item(0)
            ihao = sHao.Substring(sHao.Length - FKG.myselfG.iBianHaoWeiShu, FKG.myselfG.iBianHaoWeiShu)
            Return ihao
        End If
    End Function

    Public Shared Function YanZhengKMDM(ByVal sKMDM As String) As Boolean
        If sKMDM = "" Then
            Return True
        End If
        '首先确定是否为数字
        Dim i, n As Integer

        For i = 0 To sKMDM.Length - 1
            If Asc(sKMDM.Substring(i, 1)) >= 48 And Asc(sKMDM.Substring(i, 1)) <= 57 Then
            Else
                MsgBox("科目代码必须全部为数字！", MsgBoxStyle.Information, "提示")
                Return False
            End If
        Next

        Dim sSJDM As String = ""
        For i = 0 To 8
            n = FKG.myselfG.sKMJS.Substring(i, 1)
            If n = 0 Then
                Exit For
            End If
            If sKMDM.Length < n Then
                Dim i1, n1 As Integer
                Dim sTiShi As String = ""

                For i1 = 0 To 8
                    n1 = FKG.myselfG.sKMJS.Substring(i1, 1)
                    If n1 = 0 Then
                        Exit For
                    End If
                    If sTiShi = "" Then
                    Else
                        sTiShi = sTiShi & "."
                    End If
                    Dim m As Integer
                    For m = 0 To n1 - 1
                        sTiShi = sTiShi & "X"
                    Next
                Next

                MsgBox("正确的代码格式为： " & sTiShi, MsgBoxStyle.Information, "提示")
                Return False
                Exit Function
            End If
            If sKMDM.Length = n Then
                Exit For
            End If
            If sKMDM.Length > n Then
                sSJDM = sSJDM & sKMDM.Substring(0, n)
            End If
            sKMDM = sKMDM.Substring(n, sKMDM.Length - n)
        Next

        If sSJDM = "" Then
            Return True
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdb.bExsit("select KMDM from KMDM where KMDM='" & sSJDM & "'  and KMDM.Nian=" & FKG.myselfG.NianFen) Then
            Return True
        Else
            MsgBox("上级科目代码不存在！", MsgBoxStyle.Information, "提示")
            Return False
        End If

    End Function

    Public Shared Function getKMQM(ByVal sCase As String, Optional ByVal bXSKMDM As Boolean = True) As String
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        If bXSKMDM Then
            ds = mdb.Reader("select KMDM +'   ' + KMQM AS KM,KMDM,KMQM from kmdm where " & sCase & " and KMDM.Nian=" & FKG.myselfG.NianFen)
        Else
            ds = mdb.Reader("select KMQM from kmdm where " & sCase & " and KMDM.Nian=" & FKG.myselfG.NianFen)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        Else
            Return ds.Tables(0).Rows(0).Item(0).ToString
        End If
    End Function

    Public Shared Function getKMDM(ByVal sCase As String) As String
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select KMDM from kmdm where " & sCase & " and KMDM.Nian=" & FKG.myselfG.NianFen)
        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        Else
            Return ds.Tables(0).Rows(0).Item(0).ToString
        End If
    End Function

    Public Shared Function getKMMC(ByVal sCase As String, Optional ByVal bXSKMDM As Boolean = True) As String
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        If bXSKMDM Then
            ds = mdb.Reader("select KMDM + '   ' + KMMC AS KM,KMDM,KMMC from kmdm where " & sCase & " and KMDM.Nian=" & FKG.myselfG.NianFen)
        Else
            ds = mdb.Reader("select KMMC from kmdm where " & sCase & " and KMDM.Nian=" & FKG.myselfG.NianFen)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        Else
            Return ds.Tables(0).Rows(0).Item(0).ToString
        End If
    End Function

    Public Shared Function getSJKM(ByVal sKMDM As String) As String
        Dim sSJKM As String = ""
        If sKMDM = "" Then
            Return sSJKM
        End If

        Dim i, n As Integer
        For i = 0 To 8
            n = FKG.myselfG.sKMJS.Substring(i, 1)
            If sKMDM = "" Then
                Exit For
            End If
            sSJKM = sSJKM & sKMDM.Substring(0, n)
            sKMDM = sKMDM.Substring(n, sKMDM.Length - n)
        Next
        Return sSJKM.Substring(0, sSJKM.Length - FKG.myselfG.sKMJS.Substring(i - 1, 1)).ToString
    End Function

    Public Shared Function isMJKM(ByVal sKM As String) As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select KMMC from kmdm where KMDM like '" & sKM & "%' and Nian=" & FKG.myselfG.NianFen)
        If ds.Tables(0).Rows.Count = 0 Then
            Return False
        ElseIf ds.Tables(0).Rows.Count = 1 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function getKMDMfromQM(ByVal sKMQM As String, Optional ByVal sKey As String = " ") As String
        If sKMQM = "" Then
            Return ""
        ElseIf sKMQM.IndexOf(sKey) = -1 Then
            Return sKMQM
        Else
            Return sKMQM.Substring(0, sKMQM.IndexOf(sKey))
        End If
    End Function
End Class
