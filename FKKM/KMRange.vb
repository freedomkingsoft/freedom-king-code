﻿Public Class KMRange

    Private Sub KMRange_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        FillKM()
        fillFZSX()

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
      
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub FillKM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If Me.txtMinKM.Text = "" Then
            Me.txtMinKM.Text = mdb.Reader("select top 1 KMDM  from KMDM where Nian=" & FKG.myselfG.NianFen & " order by KMDM").Tables(0).Rows(0).Item(0).ToString
        End If
        If Me.txtMaxKM.Text = "" Then
            Me.txtMaxKM.Text = mdb.Reader("select top 1 KMDM  from KMDM where Nian=" & FKG.myselfG.NianFen & " order by KMDM desc").Tables(0).Rows(0).Item(0).ToString
        End If

    End Sub

    Private Sub fillFZSX()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim sSql As String
        sSql = "select distinct 辅助属性名称 from KMFZSX  where Nian=" & FKG.myselfG.NianFen & " and 科目代码>='" & Me.txtMinKM.Text & "' and 科目代码<='" & Me.txtMaxKM.Text & "'"
        mdb.DataBind(Me.cbFZSX, sSql)

    End Sub
End Class