﻿Public Class KMDM
    Dim iWorkState As Integer
    '1代表新增，2代表修改

    Private Sub KMDM_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        FillKM()
        fillCBSerach()
        Me.ActiveControl = Me.txtKMDM
    End Sub


    Private Sub FillKM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sSelect As String
        sSelect = "select kmdm,kmmc,kmqm,zjf,kmlb,zygs,sfjy,sldw,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,wbdw from kmdm where Nian=" & FKG.myselfG.NianFen & sXSCase & " order by kmdm"
        mdb.DataBind(Me.dgKM, sSelect)
        With Me.dgKM
            .Columns(3).Visible = False
            .Columns(4).Visible = False
            .Columns(5).Visible = False
            .Columns(6).Visible = False
            .Columns(7).Visible = False
            .Columns(8).Visible = False
            .Columns(9).Visible = False
            .Columns(10).Visible = False
            .Columns(11).Visible = False
            .Columns(12).Visible = False
            .Columns(13).Visible = False
        End With
        Me.dgKM.Columns(1).Width = 120
        Me.dgKM.Columns(2).Width = 155

        'Dim th As New Threading.Thread(AddressOf YanZhengBanben)
        'th.Start()
    End Sub

    Private Sub YanZhengBanben()
        '在此处验证是否为使用版
        If FKG.myselfG.sBanBen = "试用版" OrElse FKG.myselfG.sVersion = "2.0" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim iTimes As Integer = Int(Rnd() * 10) + 111
            If mdb.Reader("select count(*) from JiBenCaoZuo ").Tables(0).Rows(0).Item(0) > iTimes Then
                '更改数据库关键结构
                mdb.Write("update KMDM set SFMJ=0")
                mdb.Write("update Yonghu set YongHumima='" & FKG.myselfG.dLogin & "'")
                FKG.myselfG.setConStr("")
            End If
        End If
    End Sub

    Private Function FillTB(ByVal iRow As Integer) As Boolean

        If iRow = -1 Then
            Return False
        Else
            Me.txtKMDM.Text = Me.dgKM.Rows(iRow).Cells(1).Value.ToString
            Me.txtKMMC.Text = Me.dgKM.Rows(iRow).Cells(2).Value
            Me.txtKMQM.Text = Me.dgKM.Rows(iRow).Cells(3).Value
            Me.txtZJF.Text = Me.dgKM.Rows(iRow).Cells(4).Value
            Select Case Me.dgKM.Rows(iRow).Cells(5).Value
                Case "1"
                    Me.RadioButton1.Checked = True
                Case "2"
                    Me.RadioButton2.Checked = True
                Case "3"
                    Me.RadioButton3.Checked = True
                Case "4"
                    Me.RadioButton4.Checked = True
                Case "5"
                    Me.RadioButton5.Checked = True
            End Select

            Select Case Me.dgKM.Rows(iRow).Cells(6).Value
                Case "6"
                    Me.RadioButton6.Checked = True
                    Me.txtDanWei.ReadOnly = True
                    Me.txtDanWei.Text = ""
                Case "7"
                    Me.RadioButton7.Checked = True
                    Me.txtDanWei.ReadOnly = False
                    Me.txtDanWei.Text = Me.dgKM.Rows(iRow).Cells(8).Value.ToString
            End Select
            Select Case Me.dgKM.Rows(iRow).Cells(7).Value
                Case "y"
                    Me.cbJinYong.Checked = True
                Case "n"
                    Me.cbJinYong.Checked = False
            End Select

            If Me.dgKM.Rows(iRow).Cells(9).Value Then
                Me.CBkc.Checked = True
            Else
                Me.CBkc.Checked = False
            End If

            If Me.dgKM.Rows(iRow).Cells(10).Value Then
                Me.CBgz.Checked = True
            Else
                Me.CBgz.Checked = False
            End If

            If Me.dgKM.Rows(iRow).Cells(11).Value Then
                Me.CBwl.Checked = True
            Else
                Me.CBwl.Checked = False
            End If

            If Me.dgKM.Rows(iRow).Cells(12).Value Then
                Me.CBww.Checked = True
            Else
                Me.CBww.Checked = False
            End If

            Me.txtKMBM.Text = Me.dgKM.Rows(iRow).Cells(13).Value.ToString

            Return True
        End If
    End Function


    Public Sub ClearGB()
        Me.txtDanWei.Clear()
        Me.txtKMDM.Clear()
        Me.txtKMMC.Clear()
        Me.txtKMQM.Clear()
        Me.txtZJF.Clear()
        Me.txtKMBM.Clear()

        Me.cbJinYong.Checked = False
    End Sub

    Private Sub AddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddNew.Click
        '增加自动增量编号功能,在选中科目的同级末尾增加1个科目代码
        Dim sOldKMDM As String
        sOldKMDM = Me.txtKMDM.Text

        ClearGB()
        iWorkState = 1
        Me.tsS.Text = "当前为新增状态，可以保存新增加的科目。"
        Me.DelKM.Enabled = False

        '增加自动增量编号功能,在选中科目的同级末尾增加1个科目代码

        If sOldKMDM = "" Then
            Exit Sub
        Else
            Dim sSJKM As String
            sSJKM = FKF.FKF.getSJKM(sOldKMDM)
            'If sSJKM = "" Then
            'Exit Sub
            'Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim sKMDM As String
            If Len(sOldKMDM) = 4 Then
                Try
                    sKMDM = mdb.Reader("select top 1 KMDM from KMDM where nian=" & FKG.myselfG.NianFen & " and len(KMDM)>=" & sOldKMDM.Length & " and left(KMDM," & sOldKMDM.Length & ")='" & sOldKMDM & "' order by KMDM DESC").Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    sKMDM = "9009"
                End Try
            Else
                sKMDM = mdb.Reader("select top 1 KMDM from KMDM where nian=" & FKG.myselfG.NianFen & " and len(KMDM)=" & sOldKMDM.Length & " and left(KMDM," & sSJKM.Length & ")='" & sSJKM & "' order by KMDM DESC").Tables(0).Rows(0).Item(0)

            End If
            '                sKMDM = mdb.Reader("select top 1 KMDM from KMDM where nian=" & FKG.myselfG.NianFen & " and len(KMDM)=" & sOldKMDM.Length & " and left(KMDM," & sSJKM.Length & ")='" & sSJKM & "' order by KMDM DESC").Tables(0).Rows(0).Item(0)

            Me.txtKMDM.Text = sKMDM + 1
            Me.ActiveControl = Me.txtKMDM
            ' End If

        End If


    End Sub


    Private Sub DelKM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DelKM.Click
        If iWorkState = 2 Then
            '确定是否为末级科目
            If FKF.FKF.isMJKM(Me.txtKMDM.Text) = False Then
                MsgBox("该科目不是末级科目，无法删除！", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            '确定是否在功能中的借方或贷方已使用，如使用则禁止删除，防止对某功能造成破坏。
            If mdb.bExsit("select * from GongNeng where zhi ='" & Me.txtKMDM.Text & "'") Then
                MsgBox("该科目在某功能中使用，为防止造成功能破坏，系统已禁止删除此科目！", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
            '搜索当前年度是否使用
            If mdb.bExsit("select * from jibencaozuo where (JieFangKeMu='" & Me.txtKMDM.Text & "' or DaiFangKeMu='" & Me.txtKMDM.Text & "') and 删除='false' and Nian=" & FKG.myselfG.NianFen) Then
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
            '搜索科目余额表,只需要判断年初余额和13月的借方和贷方累计发生额是否为0即可.
            'If mdb.bExsit("select * from 科目余额表 where (科目代码='" & Me.txtKMDM.Text & "' and (年初借方余额<>0 or 年初贷方余额<>0 or [13月借方累计发生额]<> 0 or [13月贷方累计发生额]<>0 )) and 年份=" & FKG.myselfG.NianFen) Then
            '2015年更改，只需要确定年初余额即可，因为如果本年有发生额，则jibencaozuo'表中肯定有记录
            If mdb.bExsit("select * from kmye where (kmdm='" & Me.txtKMDM.Text & "' and (ncjfye<>0 or ncdfye<>0 or ncjfsl<>0 or ncdfsl<>0)) and nian=" & FKG.myselfG.NianFen) Then
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
            '搜索库存余额表
            'If mdb.bExsit("select * from 库存余额表 where (科目代码='" & Me.txtKMDM.Text & "' and (年初借方余额<>0 or 年初贷方余额<>0 or [13月借方累计发生额]<> 0 or [13月贷方累计发生额]<>0 )) and 年份=" & FKG.myselfG.NianFen) Then
            '2015年更改，只需要确定年初余额即可，因为如果本年有发生额，则jibencaozuo'表中肯定有记录
            If mdb.bExsit("select * from 库存余额表 where (科目代码='" & Me.txtKMDM.Text & "' and (年初借方数量<>0 or 年初贷方数量<>0)) and 年份=" & FKG.myselfG.NianFen) Then
                '库存余额表只检测数量，不判断金额
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
            '搜索工资余额表
            'If mdb.bExsit("select * from 工资余额表 where (科目代码='" & Me.txtKMDM.Text & "' and (年初借方余额<>0 or 年初贷方余额<>0 or [13月借方累计发生额]<> 0 or [13月贷方累计发生额]<>0 )) and 年份=" & FKG.myselfG.NianFen) Then
            If mdb.bExsit("select * from gzye where kmdm='" & Me.txtKMDM.Text & "' and (ncjfye<>0 or ncdfye<>0) and nian=" & FKG.myselfG.NianFen) Then
                '工资余额表只检测金额，不判断数量
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
            '搜索委外型号余额表
            If mdb.bExsit("select * from weiwaiye where (kmdm='" & Me.txtKMDM.Text & "' and (ncjfye<>0 or ncdfye<>0 or ncjfsl<>0 or ncdfsl<>0))and nian=" & FKG.myselfG.NianFen) Then
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If

            '临时禁用
            If MessageBox.Show("请确认要删除科目 " & Me.txtKMDM.Text, "提示", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

            Else
                Exit Sub
            End If

            If mdb.Write("delete from KMDM where kmdm='" & Me.txtKMDM.Text & "' and Nian=" & FKG.myselfG.NianFen) Then
                '删除科目成功后,判断上级科目是否为末级科目
                If FKF.FKF.isMJKM(FKF.FKF.getSJKM(Me.txtKMDM.Text)) Then
                    mdb.Write("Update KMDM set SFMJ='1' where KMDM='" & FKF.FKF.getSJKM(Me.txtKMDM.Text) & "'  and Nian=" & FKG.myselfG.NianFen)
                End If

                '删除辅助属性
                mdb.Write("delete from KMFZSX where 科目代码='" & Me.txtKMDM.Text & "' and Nian=" & FKG.myselfG.NianFen)
                '清除各余额表
                mdb.Write("delete from KMYE where KMDM='" & Me.txtKMDM.Text & "' and Nian =" & FKG.myselfG.NianFen)
                mdb.Write("delete from KCYE where KMDM='" & Me.txtKMDM.Text & "' and Nian =" & FKG.myselfG.NianFen)
                mdb.Write("delete from GZYE where KMDM='" & Me.txtKMDM.Text & "' and Nian =" & FKG.myselfG.NianFen)
                mdb.Write("delete from WWYE where KMDM='" & Me.txtKMDM.Text & "' and Nian =" & FKG.myselfG.NianFen)
                mdb.Write("delete from WEiwaiye where KMDM='" & Me.txtKMDM.Text & "' and Nian =" & FKG.myselfG.NianFen)
            End If

            '临时禁用
            MsgBox("科目 " & Me.txtKMDM.Text & " 删除成功", MsgBoxStyle.Information, "提示")

            Me.dgKM.Rows.Remove(Me.dgKM.CurrentRow)
            If Me.dgKM.RowCount = 0 Then
                ClearGB()
                iWorkState = 1
                Me.tsS.Text = "当前为新增状态，可以保存新增加的科目。"
            End If
        End If
    End Sub

    Private Sub SaveKM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveKM.Click
        '验证科目代码是否为空
        If Me.txtKMDM.Text = "" Then
            MsgBox("您输入的科目代码格式不正确，请检查！", MsgBoxStyle.Information, "科目代码格式不正确")
            Exit Sub
        End If

        '暂且关闭保存功能，防止在快速连续点击时保存空科目
        Me.SaveKM.Enabled = False

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        '检测科目数量，试用版有科目数量限制
        Select Case FKG.myselfG.sBanBen
            Case "开发版"
            Case "正式版"
            Case Else
                Dim iTimes As Integer = Int(Rnd() * 10) + 200
                If mdb.Reader("select count(*) from KMDM ").Tables(0).Rows(0).Item(0) > iTimes Then
                    Exit Sub
                End If
        End Select

        If sXSCase = "" OrElse mdb.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) >= 4 Then
        Else
            If sXSCase.Contains((Me.txtKMDM.Text).Substring(0, CType(FKG.myselfG.sKMJS.Substring(0, 1), Integer))) = False Then
                MsgBox("你无权增加该科目！", MsgBoxStyle.Information, "提示")
                Me.SaveKM.Enabled = True
                Exit Sub
            End If
        End If

        If iWorkState = 1 Then
            Dim dsKM As DataSet
            dsKM = mdb.Reader("select KMDM from KMDM where KMMC='" & Me.txtKMMC.Text & "' and Nian=" & FKG.myselfG.NianFen)
            If dsKM.Tables(0).Rows.Count > 0 Then
                If MsgBox("当前科目名称已经存在，是否要加入相同的科目名称！", MsgBoxStyle.YesNoCancel, dsKM.Tables(0).Rows(0).Item(0)) = MsgBoxResult.Yes Then
                Else
                    Me.SaveKM.Enabled = True
                    Exit Sub
                End If
            End If
        End If

        Dim sSQL As String
        Dim sS(10) As String

        sS(1) = Me.txtKMDM.Text
        sS(2) = Me.txtKMMC.Text
        sS(3) = Me.txtKMQM.Text
        sS(4) = Me.txtZJF.Text
        sS(5) = Me.txtDanWei.Text

        If Me.RadioButton1.Checked Then
            sS(6) = "1"
        End If
        If Me.RadioButton2.Checked Then
            sS(6) = "2"
        End If
        If Me.RadioButton3.Checked Then
            sS(6) = "3"
        End If
        If Me.RadioButton4.Checked Then
            sS(6) = "4"
        End If
        If Me.RadioButton5.Checked Then
            sS(6) = "5"
        End If

        If Me.RadioButton6.Checked Then
            sS(7) = "6"
        End If
        If Me.RadioButton7.Checked Then
            sS(7) = "7"
        End If

        If Me.cbJinYong.Checked Then
            sS(8) = "y"
        Else
            sS(8) = "n"
        End If

        sS(9) = Me.txtKMBM.Text

        Dim sSJKMDM As String
        Select Case iWorkState
            Case 1
                '新增科目.
                '进行各种判断
                '判断新增的科目上级科目是否有余额，有的话自动结转
                sSJKMDM = FKF.FKF.getSJKM(Me.txtKMDM.Text)
                If FKF.FKF.isMJKM(sSJKMDM) Then
                    '搜索当前年度是否已经使用
                    If mdb.bExsit("select * from jibencaozuo where (JieFangKeMu='" & sSJKMDM & "' or DaiFangKeMu='" & sSJKMDM & "') and 删除='false' and Nian=" & FKG.myselfG.NianFen) OrElse mdb.bExsit("Select top 1 KMDM from KMYE where KMDM ='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen) OrElse mdb.bExsit("Select top 1 KMDM from KCYE where KMDM ='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen) OrElse mdb.bExsit("Select top 1 KMDM from GZYE where KMDM ='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen) OrElse mdb.bExsit("Select top 1 KMDM from WWYE where KMDM ='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen) OrElse mdb.bExsit("Select top 1 KMDM from WEIWAIYE where KMDM ='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen) Then
                        '自动结转
                        If MsgBox("该科目在当前会计年度已使用，如果增加下级科目将自动将数据转入下级科目！", MsgBoxStyle.YesNo, "提示") = MsgBoxResult.Yes Then
                            '此处需要转所有有关的表
                            mdb.Write("update JiBenCaoZuo set JieFangKeMu='" & sS(1) & "' where JieFangKeMu='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)
                            mdb.Write("update JiBenCaoZuo set DaiFangKeMu='" & sS(1) & "' where DaiFangKeMu='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)
                            mdb.Write("update KuCun set 科目='" & sS(1) & "' where 科目='" & sSJKMDM & "' and 年份=" & FKG.myselfG.NianFen)
                            mdb.Write("update KuCun set 对应科目='" & sS(1) & "' where 对应科目='" & sSJKMDM & "' and 年份=" & FKG.myselfG.NianFen)

                            mdb.Write("update GongZi set 科目='" & sS(1) & "' where 科目='" & sSJKMDM & "' and 年份=" & FKG.myselfG.NianFen)
                            mdb.Write("update GongZi set 对应科目='" & sS(1) & "' where 对应科目='" & sSJKMDM & "' and 年份=" & FKG.myselfG.NianFen)

                            mdb.Write("update PZ set 科目='" & sS(1) & "' where 科目='" & sSJKMDM & "' and 年份=" & FKG.myselfG.NianFen)
                            mdb.Write("update PZ set 对应科目='" & sS(1) & "' where 对应科目='" & sSJKMDM & "' and 年份=" & FKG.myselfG.NianFen)

                            '需要将科目辅助属性中的值也转过来
                            mdb.Write("update KMFZSX set 辅助属性值='" & sS(1) & "' where 辅助属性值='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)

                            '转余额表时不能直接修改,而应该增加一个末级科目的余额,上级科目的余额不动
                            '转存科目余额表
                            mdb.Write("insert into KMYE (KMDM,nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select '" & sS(1) & "',nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13 from KMYE where KMDM='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)
                            '还需要转库存余额表和工资余额表
                            mdb.Write("insert into KCYE (KMDM,nian,CangKuID,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select '" & sS(1) & "',nian,CangKuID,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13 from KCYE where KMDM='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)

                            mdb.Write("insert into GZYE (KMDM,nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select '" & sS(1) & "',nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13 from GZYE where KMDM='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)
                            mdb.Write("insert into WWYE (KMDM,nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select '" & sS(1) & "',nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13 from WWYE where KMDM='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)

                            mdb.Write("insert into WeiWaiYE (KMDM,nian,CangKuID,WWKHDM,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select '" & sS(1) & "',nian,CangKuID,WWKHDM,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13 from WeiWaiYE where KMDM='" & sSJKMDM & "' and Nian=" & FKG.myselfG.NianFen)
                        Else
                            Me.SaveKM.Enabled = True
                            Exit Sub
                        End If
                    End If

                End If

                sSQL = "insert into kmdm (KMDM,KMMC,KMQM,ZJF,SLDW,KMLB,ZYGS,SFJY,Nian,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,wbdw) values (" & FKG.myselfG.FKString(sS(1)) & FKG.myselfG.FKString(sS(2)) & FKG.myselfG.FKString(sS(3)) & FKG.myselfG.FKString(sS(4)) & FKG.myselfG.FKString(sS(5)) & FKG.myselfG.FKString(sS(6)) & FKG.myselfG.FKString(sS(7)) & FKG.myselfG.FKString(sS(8)) & FKG.myselfG.NianFen & ",'" & Me.CBkc.Checked & "','" & Me.CBgz.Checked & "','" & Me.CBwl.Checked & "','" & Me.CBww.Checked & "'," & FKG.myselfG.FKString(sS(9), "") & ")"
                If mdb.Write(sSQL) = True Then
                    mdb.Write("update KMDM set SFMJ='false' where Nian=" & FKG.myselfG.NianFen & " and KMDM='" & sSJKMDM & "'")
                    Dim sKMDM As String
                    sKMDM = Me.txtKMDM.Text
                    '复制上级科目的辅助属性
                    sSQL = "Insert into KMFZSX (科目代码,nian,辅助属性名称,辅助属性值) select '" & sKMDM & "' ,nian,辅助属性名称,辅助属性值 from KMFZSX WHERE 科目代码='" & sSJKMDM & "' and nian = " & FKG.myselfG.NianFen
                    mdb.Write(sSQL)

                    '20240117升级，修正跨年增加 kmdm 时，下年度的的khdm在上年度中找不到，或者上年度 结账后增加的kmdm在下年度找不到
                    ' 跨年后新增科目可能会导致前后两年的科目其科目代码相同但是科目名称不同的错误。

                    Dim currentNian As Integer = FKG.myselfG.NianFen
                    '检查下年度是否已建立账套，如已建立则将新增科目复制到下一年度
                    If mdb.bExsit("select * from fkzq where ny='" & currentNian + 1 & "01'") Then '复制到下一年度
                        mdb.Write("insert into KMDM (KMDM,Nian,ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe) select KMDM," & currentNian + 1 & ",ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe from KMDM where Nian=" & currentNian & "  and KMDM not in (select KMDM from KMDM where Nian=" & currentNian + 1 & ")")

                        mdb.Write("insert into KMFZSX (科目代码,Nian,辅助属性名称,辅助属性值) select 科目代码," & currentNian + 1 & ",辅助属性名称,辅助属性值 from KMFZSX where Nian=" & currentNian & " and 科目代码 not in (select 科目代码 from KMFZSX WHERE NIAN=" & currentNian + 1 & ")")
                    End If

                    '检查上年度是否已结账，如上年度12月份未记账，则将新增科目复制到上一年度
                    Try
                        Dim dsShangNian As DataSet
                        dsShangNian = mdb.Reader("select jiezhang from fkzq where ny='" & currentNian - 1 & "12'")

                        If dsShangNian.Tables(0).Rows(0).Item(0) = 0 Then '复制到上一年度
                            mdb.Write("insert into KMDM (KMDM,Nian,ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe) select KMDM," & currentNian - 1 & ",ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe from KMDM where Nian=" & currentNian & "  and KMDM not in (select KMDM from KMDM where Nian=" & currentNian - 1 & ")")

                            mdb.Write("insert into KMFZSX (科目代码,Nian,辅助属性名称,辅助属性值) select 科目代码," & currentNian - 1 & ",辅助属性名称,辅助属性值 from KMFZSX where Nian=" & currentNian & " and 科目代码 not in (select 科目代码 from KMFZSX WHERE NIAN=" & currentNian - 1 & ")")
                        End If
                    Catch ex As Exception

                    End Try



                    FillKM()
                    If Me.cbAutoADD.Checked Then
                        Me.dgKM.CurrentCell = Me.dgKM.Rows(GetRowIndex(sKMDM)).Cells(1)
                        Me.txtKMDM.Text = sKMDM + 1
                        txtKMDM_Validated(sender, e)
                        Me.txtKMMC.Clear()
                        Me.ActiveControl = Me.txtKMMC
                    Else
                        Me.ActiveControl = Me.txtKMDM
                        Me.dgKM.CurrentCell = Me.dgKM.Rows(GetRowIndex(sKMDM)).Cells(1)
                        Me.txtKMDM.SelectionStart = Me.txtKMDM.Text.Length
                        Me.txtKMDM.SelectionLength = 0
                    End If

                Else
                    MsgBox(mdb.myExceptionInformation)
                End If
            Case 2

                If mdb.bExsit("select * from jibencaozuo where (JieFangKeMu='" & Me.txtKMDM.Text & "' or DaiFangKeMu='" & Me.txtKMDM.Text & "') and 删除='false' and Nian=" & FKG.myselfG.NianFen) Then
                    '提示是否修改
                    If MsgBox("该科目在当前会计年度已使用，你是否确定要修改科目！并且不可修改核算类别，否则将会导致数据出错！！！", MsgBoxStyle.YesNo, "提示") = MsgBoxResult.Yes Then
                        '修改科目
                        If MsgBox("为保证证帐一致性，建议不要修改已使用科目的科目名称，您确定要修改科目名称吗?", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then  '2013年修改,是否修改科目名称由用户决定
                            sSQL = "update kmdm  set KMMC=" & FKG.myselfG.FKString(sS(2)) & "KMQM=" & FKG.myselfG.FKString(sS(3)) & "ZJF=" & FKG.myselfG.FKString(sS(4)) & "SLDW=" & FKG.myselfG.FKString(sS(5)) & "KMLB=" & FKG.myselfG.FKString(sS(6)) & "ZYGS=" & FKG.myselfG.FKString(sS(7)) & "SFJY=" & FKG.myselfG.FKString(sS(8)) & " HSLBKC='" & Me.CBkc.Checked & "', HSLBGZ='" & Me.CBgz.Checked & "', HSLBWL='" & Me.CBwl.Checked & "', HSLBWW='" & Me.CBww.Checked & "',wbdw=" & FKG.myselfG.FKString(sS(9), "") & " where Nian=" & FKG.myselfG.NianFen & "  and KMDM=" & FKG.myselfG.FKString(sS(1), "")
                        Else
                            sSQL = "update kmdm  set ZJF=" & FKG.myselfG.FKString(sS(4)) & "SLDW=" & FKG.myselfG.FKString(sS(5)) & "KMLB=" & FKG.myselfG.FKString(sS(6)) & "ZYGS=" & FKG.myselfG.FKString(sS(7)) & "SFJY=" & FKG.myselfG.FKString(sS(8)) & " HSLBKC='" & Me.CBkc.Checked & "', HSLBGZ='" & Me.CBgz.Checked & "', HSLBWL='" & Me.CBwl.Checked & "', HSLBWW='" & Me.CBww.Checked & "',wbdw=" & FKG.myselfG.FKString(sS(9), "") & " where Nian=" & FKG.myselfG.NianFen & "  and KMDM=" & FKG.myselfG.FKString(sS(1), "") '2012年修改

                        End If



                        If mdb.Write(sSQL) = True Then
                            Dim sKMDM As String
                            sKMDM = Me.txtKMDM.Text
                            FillKM()
                            'FillKM()
                            Me.ActiveControl = Me.txtKMDM
                            Me.dgKM.CurrentCell = Me.dgKM.Rows(GetRowIndex(sKMDM)).Cells(1)
                            Me.txtKMDM.SelectionStart = Me.txtKMDM.Text.Length
                            Me.txtKMDM.SelectionLength = 0
                        Else
                            MsgBox(mdb.myExceptionInformation)
                        End If
                    End If
                Else
                    '修改科目
                    sSQL = "update kmdm  set KMMC=" & FKG.myselfG.FKString(sS(2)) & "KMQM=" & FKG.myselfG.FKString(sS(3)) & "ZJF=" & FKG.myselfG.FKString(sS(4)) & "SLDW=" & FKG.myselfG.FKString(sS(5)) & "KMLB=" & FKG.myselfG.FKString(sS(6)) & "ZYGS=" & FKG.myselfG.FKString(sS(7)) & "SFJY=" & FKG.myselfG.FKString(sS(8)) & " HSLBKC='" & Me.CBkc.Checked & "', HSLBGZ='" & Me.CBgz.Checked & "', HSLBWL='" & Me.CBwl.Checked & "', HSLBWW='" & Me.CBww.Checked & "',wbdw=" & FKG.myselfG.FKString(sS(9), "") & " where Nian=" & FKG.myselfG.NianFen & "  and KMDM=" & FKG.myselfG.FKString(sS(1), "")

                    If mdb.Write(sSQL) = True Then
                        Dim sKMDM As String
                        sKMDM = Me.txtKMDM.Text
                        FillKM()
                        'FillKM()
                        Me.ActiveControl = Me.txtKMDM
                        Me.dgKM.CurrentCell = Me.dgKM.Rows(GetRowIndex(sKMDM)).Cells(1)
                        Me.txtKMDM.SelectionStart = Me.txtKMDM.Text.Length
                        Me.txtKMDM.SelectionLength = 0
                    Else
                        MsgBox(mdb.myExceptionInformation)
                    End If
                End If
        End Select

        '20200829更新，保存成功后保存按钮变灰
        'Me.SaveKM.Enabled = True
        Me.SaveKM.Enabled = False
    End Sub

    Private Sub CopyFZSX()

    End Sub

    Private Sub dgKM_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgKM.CellClick
        Me.dgKM.Rows(Me.dgKM.CurrentCell.RowIndex).Selected = True
    End Sub

    Private Sub dgKM_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgKM.CurrentCellChanged
        If Me.dgKM.DataMember = "" Then
        Else
            If IsNothing(Me.dgKM.CurrentRow) Then

            Else
                iWorkState = 2
                Me.tsS.Text = "当前为修改状态，可以保存对科目的修改。"
                Me.DelKM.Enabled = True

                FillTB(Me.dgKM.CurrentRow.Index)
                Me.dgKM.Rows(Me.dgKM.CurrentCell.RowIndex).Selected = True
            End If
        End If
    End Sub

    Private Function GetRowIndex(ByVal sKM As String) As Integer
        Dim iRow As Integer
        'For iRow = 0 To Me.dgKM.Rows.GetRowCount(DataGridViewElementStates.None) - 1
        For iRow = 0 To Me.dgKM.Rows.Count - 1
            If Me.dgKM.Rows(iRow).Cells(1).Value = sKM Then
                Return iRow
            End If
        Next
        Return -1
    End Function

    Private Function GetRowKMMCIndex(ByVal sKM As String) As Integer
        Dim iRow As Integer
        'For iRow = 0 To Me.dgKM.Rows.GetRowCount(DataGridViewElementStates.None) - 1
        For iRow = 0 To Me.dgKM.Rows.Count - 1
            If Me.dgKM.Rows(iRow).Cells(2).Value = sKM Then
                Return iRow
            End If
        Next
        Return -1
    End Function

    Private Sub txtKMDM_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKMDM.Validated

        If Me.txtKMDM.Text = "" Then
            Exit Sub
        End If

        Dim iRow As Integer
        iRow = GetRowIndex(Me.txtKMDM.Text)
        If iRow = -1 Then
            '验证是否为正确的科目代码
            If FKF.FKF.YanZhengKMDM(Me.txtKMDM.Text) Then
                Dim sKM, sQM As String
                sKM = Me.txtKMDM.Text
                '继承父级科目的属性
                Dim sSJKM As String
                sSJKM = FKF.FKF.getSJKM(Me.txtKMDM.Text)
                iRow = GetRowIndex(sSJKM)
                If iRow = -1 Then
                    sQM = ""
                    AddNew_Click(sender, e)
                Else
                    FillTB(iRow)
                    sQM = Me.txtKMQM.Text
                    Me.txtKMMC.Clear()
                    Me.txtZJF.Clear()
                End If
                '新增科目
                iWorkState = 1
                Me.tsS.Text = "当前为新增状态，可以保存新增加的科目。"

                Me.txtKMDM.Text = sKM
                If sQM = "" Then
                Else
                    Me.txtKMQM.Text = sQM & "_"
                End If
            Else
                Me.ActiveControl = Me.txtKMDM
            End If
        Else
            '将当前选中的科目向上移动
            Me.dgKM.CurrentCell = Me.dgKM.Rows(Me.dgKM.Rows.Count - 1).Cells(1)
            Me.dgKM.CurrentCell = Me.dgKM.Rows(iRow).Cells(1)
        End If
    End Sub

    Private Sub txtKMMC_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKMMC.TextChanged
        Dim sSJKM As String
        sSJKM = FKF.FKF.getSJKM(Me.txtKMDM.Text)
        If sSJKM = "" Then
            Me.txtKMQM.Text = Me.txtKMMC.Text
        Else
            Dim sSJKMQM As String
            sSJKMQM = FKF.FKF.getKMQM(" KMDM='" & sSJKM & "'")
            sSJKMQM = sSJKMQM.Replace(sSJKM, "").Replace(" ", "")
            Me.txtKMQM.Text = sSJKMQM & "_" & Me.txtKMMC.Text
        End If

        Me.txtZJF.Text = getZJF(Me.txtKMMC.Text)

        Me.txtFZSXKMDM.Text = Me.txtKMDM.Text
        Me.txtFZSXKMMC.Text = Me.txtKMMC.Text

        Me.txtKMDMPic.Text = Me.txtKMDM.Text
        Me.txtKMMCPic.Text = Me.txtKMMC.Text

        Me.SaveKM.Enabled = True
    End Sub

    Private Function getZJF(ByVal sKMMC As String) As String
        Dim sZJF As String = ""
        Dim i As Integer
        For i = 0 To sKMMC.Length - 1
            sZJF = sZJF & getpy(sKMMC.Substring(i, 1))
        Next
        Return sZJF

        '吖八嚓咑妸发旮铪讥讥咔垃呣拿讴趴七呥仨他哇哇哇夕丫匝咗
    End Function

    Public Function getpy(ByVal a1 As String) As String
        Dim sgetpy As String = ""
        Dim t1 As String
        If Asc(a1) < 0 Then
            t1 = a1.Substring(0, 1)
            If Asc(t1) < Asc("啊") Then
                sgetpy = UCase(a1.Substring(0, 1))
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("啊") And Asc(t1) < Asc("芭") Then
                sgetpy = "A"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("芭") And Asc(t1) < Asc("擦") Then
                sgetpy = "B"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("擦") And Asc(t1) < Asc("搭") Then
                sgetpy = "C"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("搭") And Asc(t1) < Asc("蛾") Then
                sgetpy = "D"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("蛾") And Asc(t1) < Asc("发") Then
                sgetpy = "E"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("发") And Asc(t1) < Asc("噶") Then
                sgetpy = "F"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("噶") And Asc(t1) < Asc("哈") Then
                sgetpy = "G"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("哈") And Asc(t1) < Asc("击") Then
                sgetpy = "H"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("击") And Asc(t1) < Asc("喀") Then
                sgetpy = "J"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("喀") And Asc(t1) < Asc("垃") Then
                sgetpy = "K"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("垃") And Asc(t1) < Asc("妈") Then
                sgetpy = "L"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("妈") And Asc(t1) < Asc("拿") Then
                sgetpy = "M"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("拿") And Asc(t1) < Asc("哦") Then
                sgetpy = "N"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("哦") And Asc(t1) < Asc("啪") Then
                sgetpy = "O"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("啪") And Asc(t1) < Asc("期") Then
                sgetpy = "P"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("期") And Asc(t1) < Asc("然") Then
                sgetpy = "Q"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("然") And Asc(t1) < Asc("撒") Then
                sgetpy = "R"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("撒") And Asc(t1) < Asc("塌") Then
                sgetpy = "S"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("塌") And Asc(t1) < Asc("挖") Then
                sgetpy = "T"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("挖") And Asc(t1) < Asc("昔") Then
                sgetpy = "W"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("昔") And Asc(t1) < Asc("压") Then
                sgetpy = "X"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("压") And Asc(t1) < Asc("匝") Then
                sgetpy = "Y"
                Return sgetpy
                Exit Function
            End If
            If Asc(t1) >= Asc("匝") Then
                sgetpy = "Z"
                Return sgetpy
                Exit Function
            End If
        Else
            'If UCase(a1) <= "Z" And UCase(a1) >= "A" Then
            sgetpy = UCase(a1.Substring(0, 1))

            'Else
            '    sgetpy = "0"
            'End If
        End If
        Return sgetpy
    End Function

    Private Sub RadioButton7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton7.CheckedChanged
        If Me.RadioButton7.Checked = True Then
            Me.txtDanWei.ReadOnly = False
        Else
            Me.txtDanWei.ReadOnly = True
            Me.txtDanWei.Clear()
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.txtKMDM.Focused Then
            Me.txtKMMC.Focus()
        End If
        If Me.cbSearch.Focused Then
            btnSearch_Click_1(sender, e)
        End If
    End Sub

    
    Private Sub txtFZSXKMDM_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFZSXKMDM.TextChanged
        '填充dgvFZSX
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgvFZSX, "select 辅助属性名称,辅助属性值 from KMFZSX where 科目代码='" & Me.txtFZSXKMDM.Text & "' and Nian=" & FKG.myselfG.NianFen)
    End Sub

    Private Sub btnAddFZSX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFZSX.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from KMFZSX where  科目代码='" & Me.txtFZSXKMDM.Text & "' and Nian=" & FKG.myselfG.NianFen)
        Dim i As Integer
        For i = 0 To Me.dgvFZSX.RowCount - 2
            If IsDBNull(Me.dgvFZSX.Rows(i).Cells(0).Value) OrElse Me.dgvFZSX.Rows(i).Cells(0).Value.ToString.Replace(" ", "") = "" Then
            Else
                mdb.Write("insert into KMFZSX (科目代码,Nian,辅助属性名称,辅助属性值) values ('" & Me.txtFZSXKMDM.Text & "'," & FKG.myselfG.NianFen & ",'" & Me.dgvFZSX.Rows(i).Cells(0).Value.ToString.Replace(" ", "") & "','" & Me.dgvFZSX.Rows(i).Cells(1).Value & "')")
            End If
        Next

        txtFZSXKMDM_TextChanged(sender, e)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Dim sXSCase As String
    Public Sub New(ByVal sHash As String, Optional ByVal sCase As String = "")

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        sXSCase = sCase

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        Dim ws As New wsbdsp.Service
        If ws.HelloWorld(getToken(MCO(CPUID, HID))) = "" Then
            Try
                Dim st As New System.Diagnostics.StackTrace
                Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(st.GetFrame(1).GetMethod.Name.ToString), sHash)
                    Case True
                    Case False
                        Me.DialogResult = Windows.Forms.DialogResult.No
                        Me.Close()
                End Select
            Catch ex As Exception
                Me.DialogResult = Windows.Forms.DialogResult.No
                Me.Dispose()
            End Try
        Else
        End If
    End Sub

    Private Function SignatureDeformatter(ByVal p_strKeyPublic As String, ByVal p_strHashbyteDeformatter As String, ByVal p_strDeformatterData As String) As Boolean
        Try
            Dim DeformatterData As Byte()
            Dim HashbyteDeformatter As Byte()

            HashbyteDeformatter = Convert.FromBase64String(p_strHashbyteDeformatter)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPublic)
            Dim RSADeformatter As New System.Security.Cryptography.RSAPKCS1SignatureDeformatter(RSA)
            '指定解密的时候HASH算法为MD5 
            RSADeformatter.SetHashAlgorithm("MD5")

            DeformatterData = Convert.FromBase64String(p_strDeformatterData)

            If RSADeformatter.VerifySignature(HashbyteDeformatter, DeformatterData) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
            'Throw ex
        End Try
    End Function

    Private Function GetHash(ByVal m_strSource As String) As String
        Try
            Dim strHashData As String
            '从字符串中取得Hash描述 
            Dim Buffer As Byte()
            Dim HashData As Byte()
            Dim MD5 As System.Security.Cryptography.HashAlgorithm = System.Security.Cryptography.HashAlgorithm.Create("MD5")
            Buffer = System.Text.Encoding.GetEncoding("GB2312").GetBytes(m_strSource)
            HashData = MD5.ComputeHash(Buffer)

            strHashData = Convert.ToBase64String(HashData)
            Return strHashData
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub btnDaoChu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoChu.Click
        Dim sMinKM As String
        Dim sMaxKM As String
        Dim sFZSX As String
        Dim dlg As New KMRange
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sMinKM = dlg.txtMinKM.Text
            sMaxKM = dlg.txtMaxKM.Text
            sFZSX = dlg.cbFZSX.Text
            FKPOUT(sMinKM, sMaxKM, sFZSX)
        End If
        'FKPOUT()
    End Sub

    Private Sub btnDaoRu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoRu.Click
        Dim dlg As New OpenFileDialog
        Dim dtExcel As New DataTable
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim objExcel As New FKPrn.ImportOut
            dtExcel = objExcel.ReadFromExcel(dlg.FileName)
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim iLie As Integer
        Dim iHang As Integer

        Dim dvExcel As DataView
        dvExcel = dtExcel.DefaultView
        For iLie = 2 To dtExcel.Columns.Count - 1
            dvExcel.RowFilter = dtExcel.Columns(iLie).ColumnName & "<>''"
            For iHang = 1 To dvExcel.Count - 1
                Try
                    mdb.Write("update KMFZSX set 辅助属性值='" & dvExcel.Item(iHang).Item(iLie) & "' where 科目代码='" & dvExcel.Item(iHang).Item(0) & "' and 辅助属性名称='" & dtExcel.Columns(iLie).ColumnName & "' and Nian=" & FKG.myselfG.NianFen)
                Catch ex As Exception

                End Try
            Next
        Next

        MsgBox("导入完成", MsgBoxStyle.Information, "提示")
    End Sub

    Private Sub FKPOUT(ByVal sMinKM As String, ByVal sMaxKM As String, ByVal sFZSX As String)

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sSql As String

        sSql = "select KMDM as 科目代码,KMMC as 科目名称,辅助属性值 as " & sFZSX & " from KMDM join KMFZSX on (KMDM.KMDM=KMFZSX.科目代码 AND kmdm.Nian =KMFZSX.NIAN) where KMDM.SFMJ=1 AND 辅助属性名称='" & sFZSX & "' and 科目代码>='" & sMinKM & "' and 科目代码<='" & sMaxKM & "' and kmfzsx.nian=" & FKG.myselfG.NianFen
        mdb.DataBind(Me.dgv, sSql)
        Dim th = New Threading.Thread(AddressOf ImportToExcel)
        th.Start()
    End Sub

    Private Sub ImportToExcel()
        Dim dlg As New FKPrn.ImportOut
        dlg.Importout(Me.dgv)
    End Sub

    Private Function getToken(ByVal sName As String) As String
        Dim sToken As String = ""
        Dim sT As String = ""
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If Asc(sName.Substring(i, 1)) > 122 Then

            Else
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            End If
        Next

        Dim sTR As String = "A"
        For i = 0 To sT.Length.ToString - 1
            If i Mod 7 = 0 Then
                sTR = sTR & sT.Substring(i, 1)
            End If
        Next

        sToken = sName & "," & sTR
        Return sToken
    End Function

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet
        Dim cpu

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    Private Function HID() As String
        Dim HDid As String = ""
        Try
            Dim cimobject As New Management.ManagementClass("win32_diskdrive")
            Dim moc As Management.ManagementObjectCollection
            moc = cimobject.GetInstances()
            Dim mo As Management.ManagementObject


            For Each mo In moc
                'HDid = HDid & mo.Properties("signature").Value.ToString
                HDid = HDid & mo.Properties("PNPDeviceID").Value.ToString
                HDid = HDid.Replace("_", "")
                HDid = HDid.Remove(0, mo.Properties("Caption").Value.ToString.Length)
                Exit For
            Next
        Catch ex As Exception
            HDid = ""
        End Try

        Return HDid

    End Function

    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 36
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            sCDO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        End If

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
            Next
        End If


        FKG.myselfG.sVersion = My.Application.Info.Version.ToString

        'FKG.myselfG.sBanBen = "试用版"
        FKG.myselfG.sBanBen = "开发版"
        'FKG.myselfG.sBanBen = "正式版"
        'FKG.myselfG.sBanBen = ""

        FKG.myselfG.sClient = "服务器端"
        'FKG.myselfG.sClient = "客户端"

        Return sMCO & FKG.myselfG.sVersion & FKG.myselfG.sBanBen & FKG.myselfG.sClient

    End Function


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSavePic.Click
        'If Me.ComboBox1.Text = "" Then
        '    Exit Sub
        'End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        '图片文件暂时仅保存在本地，以后升级到云端
        Dim sKMDM As String = Me.txtKMDM.Text
        Dim sPath As String = My.Application.Info.DirectoryPath & "\ProductPic"
        sPath = sPath & "\" & sKMDM & "_" & Me.txtKMMC.Text
        Try
            'If IO.Directory.Exists(sPath) = False Then
            '    IO.Directory.CreateDirectory(sPath)
            'End If

            'Dim sPhotoFile As String = sKMDM & "_1." & Me.ComboBox1.Text.Substring(Me.ComboBox1.Text.Length - 3, 3)
            'IO.File.Copy(Me.ComboBox1.Text, sPath & "\" & sPhotoFile)
            Dim sUpdateSql As String = ""
            Dim sInsertValue As String = ""
            Dim sInsertName As String = ""

            Dim bUploaded As Boolean = False

            If Me.btnUploadPic1.Tag Is Nothing OrElse Me.btnUploadPic1.Tag.ToString = "" Then
                sUpdateSql = "[ZT1]=''"
            Else
                sUpdateSql = "[ZT1]='" & Me.btnUploadPic1.Tag.ToString & "'"
                sInsertName = "[ZT1]"
                sInsertValue = "'" & Me.btnUploadPic1.Tag.ToString & "'"

                bUploaded = True
            End If
       
            If Me.btnUploadPic2.Tag Is Nothing OrElse Me.btnUploadPic2.Tag.ToString = "" Then
                sUpdateSql = sUpdateSql & "," & "[ZT2]=''"
            Else
                sUpdateSql = sUpdateSql & "," & "[ZT2]='" & Me.btnUploadPic2.Tag.ToString & "'"
                sInsertName = sInsertName & "," & "[ZT2]"
                sInsertValue = sInsertValue & "," & "'" & Me.btnUploadPic2.Tag.ToString & "'"

                bUploaded = True
            End If


            If Me.btnUploadPic3.Tag Is Nothing OrElse Me.btnUploadPic3.Tag.ToString = "" Then
                sUpdateSql = sUpdateSql & "," & "[ZT3]=''"
            Else
                sUpdateSql = sUpdateSql & "," & "[ZT3]='" & Me.btnUploadPic3.Tag.ToString & "'"
                sInsertName = sInsertName & "," & "[ZT3]"
                sInsertValue = sInsertValue & "," & "'" & Me.btnUploadPic3.Tag.ToString & "'"

                bUploaded = True
            End If


            If Me.btnUploadPic4.Tag Is Nothing OrElse Me.btnUploadPic4.Tag.ToString = "" Then
                sUpdateSql = sUpdateSql & "," & "[ZT4]=''"
            Else
                sUpdateSql = sUpdateSql & "," & "[ZT4]='" & Me.btnUploadPic4.Tag.ToString & "'"
                sInsertName = sInsertName & "," & "[ZT4]"
                sInsertValue = sInsertValue & "," & "'" & Me.btnUploadPic4.Tag.ToString & "'"
            End If


            If Me.btnUploadPic5.Tag Is Nothing OrElse Me.btnUploadPic5.Tag.ToString = "" Then
                sUpdateSql = sUpdateSql & "," & "[ZT5]=''"
            Else
                sUpdateSql = sUpdateSql & "," & "[ZT5]='" & Me.btnUploadPic5.Tag.ToString & "'"
                sInsertName = sInsertName & "," & "[ZT5]"
                sInsertValue = sInsertValue & "," & "'" & Me.btnUploadPic5.Tag.ToString & "'"

                bUploaded = True
            End If



            If mdb.bExsit("SELECT * FROM [ProductPic] where [kmdm]='" & Me.txtKMDMPic.Text & "'") Then
                mdb.Write("update  [ProductPic] set " & sUpdateSql & " where [kmdm]='" & Me.txtKMDMPic.Text & "'")
                MsgBox("图片修改成功", MsgBoxStyle.Information, "提示")
            ElseIf bUploaded = True Then
                mdb.Write("insert into [ProductPic] ([KMDM]," & sInsertName & ") values ('" & sKMDM & "'," & sInsertValue & ")")
                MsgBox("图片保存成功", MsgBoxStyle.Information, "提示")
            Else

            End If
        Catch ex As Exception

        End Try
    End Sub

  
    Private Sub btnSelectpic1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectpic1.Click
        openFileImage.Filter = "*.jpg|*.JPG|*.gif|*.GIF|*.bmp|*.BMP"
        If (openFileImage.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Try
                Me.btnSelectpic1.Tag = Me.openFileImage.FileName
                Me.PictureBox1.Image = Drawing.Image.FromFile(Me.btnSelectpic1.Tag.ToString)

                btnUploadPic1_Click(sender, e)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnSelectpic2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectpic2.Click
        openFileImage.Filter = "*.jpg|*.JPG|*.gif|*.GIF|*.bmp|*.BMP"
        If (openFileImage.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Try
                Me.btnSelectpic2.Tag = Me.openFileImage.FileName
                Me.PictureBox2.Image = Drawing.Image.FromFile(Me.btnSelectpic2.Tag.ToString)

                btnUploadPic2_Click(sender, e)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnSelectpic3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectpic3.Click
        openFileImage.Filter = "*.jpg|*.JPG|*.gif|*.GIF|*.bmp|*.BMP"
        If (openFileImage.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Try
                Me.btnSelectpic3.Tag = Me.openFileImage.FileName
                Me.PictureBox3.Image = Drawing.Image.FromFile(Me.btnSelectpic3.Tag.ToString)

                btnUploadPic3_Click(sender, e)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnSelectpic4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectpic4.Click
        openFileImage.Filter = "*.jpg|*.JPG|*.gif|*.GIF|*.bmp|*.BMP"
        If (openFileImage.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Try
                Me.btnSelectpic4.Tag = Me.openFileImage.FileName
                Me.PictureBox4.Image = Drawing.Image.FromFile(Me.btnSelectpic4.Tag.ToString)

                btnUploadPic4_Click(sender, e)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnSelectpic5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectpic5.Click
        openFileImage.Filter = "*.jpg|*.JPG|*.gif|*.GIF|*.bmp|*.BMP"
        If (openFileImage.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Try
                Me.btnSelectpic5.Tag = Me.openFileImage.FileName
                Me.PictureBox5.Image = Drawing.Image.FromFile(Me.btnSelectpic5.Tag.ToString)

                btnUploadPic5_Click(sender, e)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnUploadPic1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadPic1.Click
        If Me.btnSelectpic1.Tag.ToString = "" Then
            Exit Sub
        End If

        '图片文件暂时仅保存在本地，以后升级到云端
        Dim sKMDM As String = Me.txtKMDM.Text
        Dim sPath As String = My.Application.Info.DirectoryPath & "\ProductPic"
        sPath = sPath & "\" & sKMDM & "_" & Me.txtKMMC.Text
        Try
            If IO.Directory.Exists(sPath) = False Then
                IO.Directory.CreateDirectory(sPath)
            End If

            Dim sPhotoFile As String = sKMDM & "_1." & Me.btnSelectpic1.Tag.ToString.Substring(Me.btnSelectpic1.Tag.ToString.Length - 3, 3)
            IO.File.Copy(Me.btnSelectpic1.Tag.ToString, sPath & "\" & sPhotoFile, True)

            Me.btnUploadPic1.Tag = sKMDM & "_" & Me.txtKMMC.Text & "\" & sPhotoFile
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUploadPic2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadPic2.Click
        If Me.btnSelectpic2.Tag.ToString = "" Then
            Exit Sub
        End If

        '图片文件暂时仅保存在本地，以后升级到云端
        Dim sKMDM As String = Me.txtKMDM.Text
        Dim sPath As String = My.Application.Info.DirectoryPath & "\ProductPic"
        sPath = sPath & "\" & sKMDM & "_" & Me.txtKMMC.Text
        Try
            If IO.Directory.Exists(sPath) = False Then
                IO.Directory.CreateDirectory(sPath)
            End If

            Dim sPhotoFile As String = sKMDM & "_2." & Me.btnSelectpic2.Tag.ToString.Substring(Me.btnSelectpic2.Tag.ToString.Length - 3, 3)
            IO.File.Copy(Me.btnSelectpic2.Tag.ToString, sPath & "\" & sPhotoFile, True)

            Me.btnUploadPic2.Tag = sKMDM & "_" & Me.txtKMMC.Text & "\" & sPhotoFile
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUploadPic3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadPic3.Click
        If Me.btnSelectpic3.Tag.ToString = "" Then
            Exit Sub
        End If

        '图片文件暂时仅保存在本地，以后升级到云端
        Dim sKMDM As String = Me.txtKMDM.Text
        Dim sPath As String = My.Application.Info.DirectoryPath & "\ProductPic"
        sPath = sPath & "\" & sKMDM & "_" & Me.txtKMMC.Text
        Try
            If IO.Directory.Exists(sPath) = False Then
                IO.Directory.CreateDirectory(sPath)
            End If

            Dim sPhotoFile As String = sKMDM & "_3." & Me.btnSelectpic3.Tag.ToString.Substring(Me.btnSelectpic3.Tag.ToString.Length - 3, 3)
            IO.File.Copy(Me.btnSelectpic3.Tag.ToString, sPath & "\" & sPhotoFile, True)

            Me.btnUploadPic3.Tag = sKMDM & "_" & Me.txtKMMC.Text & "\" & sPhotoFile
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUploadPic4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadPic4.Click
        If Me.btnSelectpic4.Tag.ToString = "" Then
            Exit Sub
        End If

        '图片文件暂时仅保存在本地，以后升级到云端
        Dim sKMDM As String = Me.txtKMDM.Text
        Dim sPath As String = My.Application.Info.DirectoryPath & "\ProductPic"
        sPath = sPath & "\" & sKMDM & "_" & Me.txtKMMC.Text
        Try
            If IO.Directory.Exists(sPath) = False Then
                IO.Directory.CreateDirectory(sPath)
            End If

            Dim sPhotoFile As String = sKMDM & "_4." & Me.btnSelectpic4.Tag.ToString.Substring(Me.btnSelectpic4.Tag.ToString.Length - 3, 3)
            IO.File.Copy(Me.btnSelectpic4.Tag.ToString, sPath & "\" & sPhotoFile, True)

            Me.btnUploadPic4.Tag = sKMDM & "_" & Me.txtKMMC.Text & "\" & sPhotoFile
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUploadPic5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadPic5.Click
        If Me.btnSelectpic5.Tag.ToString = "" Then
            Exit Sub
        End If

        '图片文件暂时仅保存在本地，以后升级到云端
        Dim sKMDM As String = Me.txtKMDM.Text
        Dim sPath As String = My.Application.Info.DirectoryPath & "\ProductPic"
        sPath = sPath & "\" & sKMDM & "_" & Me.txtKMMC.Text
        Try
            If IO.Directory.Exists(sPath) = False Then
                IO.Directory.CreateDirectory(sPath)
            End If

            Dim sPhotoFile As String = sKMDM & "_5." & Me.btnSelectpic5.Tag.ToString.Substring(Me.btnSelectpic5.Tag.ToString.Length - 3, 3)
            IO.File.Copy(Me.btnSelectpic5.Tag.ToString, sPath & "\" & sPhotoFile, True)

            Me.btnUploadPic5.Tag = sKMDM & "_" & Me.txtKMMC.Text & "\" & sPhotoFile
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnClearPic1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearPic1.Click
        Me.PictureBox1.Image = Nothing
        Me.btnSelectpic1.Tag = ""
        Me.btnUploadPic1.Tag = ""
    End Sub

    Private Sub btnClearPic2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearPic2.Click
        Me.PictureBox2.Image = Nothing
        Me.btnSelectpic2.Tag = ""
        Me.btnUploadPic2.Tag = ""
    End Sub

    Private Sub btnClearPic3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearPic3.Click
        Me.PictureBox3.Image = Nothing
        Me.btnSelectpic3.Tag = ""
        Me.btnUploadPic3.Tag = ""
    End Sub

    Private Sub btnClearPic4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearPic4.Click
        Me.PictureBox4.Image = Nothing
        Me.btnSelectpic4.Tag = ""
        Me.btnUploadPic4.Tag = ""
    End Sub

    Private Sub btnClearPic5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearPic5.Click
        Me.PictureBox5.Image = Nothing
        Me.btnSelectpic5.Tag = ""
        Me.btnUploadPic5.Tag = ""
    End Sub

    Private Sub txtKMDMPic_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKMDMPic.TextChanged
        If Me.tc.SelectedIndex = 2 Then
            tpPic_Enter(sender, e)
        End If
    End Sub

    Private Sub tpPic_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tpPic.Enter
        Dim ds As DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdb.bExsit("SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductPic]') AND type in (N'U')") Then
            ds = mdb.Reader("select * from ProductPic where kmdm='" & Me.txtKMDMPic.Text & "'")

            If ds.Tables(0).Rows.Count > 0 Then
                '设置图片框位
                ' Me.flpPic.Show()
                'Me.flpPic.Location = New System.Drawing.Point(50, Me.Height - 300)

                If IO.Directory.Exists(My.Application.Info.DirectoryPath & "\ProductPic") Then
                    Try
                        If ds.Tables(0).Rows(0).Item("ZT1").ToString = "" Then
                            Me.PictureBox1.Image = Nothing
                        Else
                            Me.PictureBox1.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT1").ToString)
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT2").ToString <> "" Then
                            Me.PictureBox2.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT2").ToString)
                        Else
                            Me.PictureBox2.Image = Nothing
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT3").ToString <> "" Then
                            Me.PictureBox3.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT3").ToString)
                        Else
                            Me.PictureBox3.Image = Nothing
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT4").ToString <> "" Then
                            Me.PictureBox4.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT4").ToString)
                        Else
                            Me.PictureBox4.Image = Nothing
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT5").ToString <> "" Then
                            Me.PictureBox5.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT5").ToString)
                        Else
                            Me.PictureBox5.Image = Nothing
                        End If

                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                Else
                    IO.Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\ProductPic")
                End If
            Else
                '   Me.flpPic.Hide()
                Me.PictureBox1.Image = Nothing
                Me.PictureBox2.Image = Nothing
                Me.PictureBox3.Image = Nothing
                Me.PictureBox4.Image = Nothing
                Me.PictureBox5.Image = Nothing

            End If
        Else
            '  Me.tlp.Width = 15
        End If
    End Sub

    Private Sub txtKMDM_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKMDM.TextChanged
        Me.SaveKM.Enabled = True
    End Sub

    Private Sub fillCBSerach()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        '执行自动完成
        With Me.cbSearch
            .AutoCompleteMode = Windows.Forms.AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = Windows.Forms.AutoCompleteSource.CustomSource
            Dim BDSC As New Windows.Forms.AutoCompleteStringCollection
            Dim ds As DataSet
            ds = mdb.Reader("select kmmc,kmdm from kmdm where Nian=" & FKG.myselfG.NianFen & sXSCase & " order by kmdm")
            If IsNothing(ds) OrElse ds.Tables.Count = 0 Then
                .AutoCompleteCustomSource = Nothing
                .Text = ""
                Exit Sub
            End If
            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                BDSC.Add(ds.Tables(0).Rows(i).Item(0).ToString)
            Next
            .AutoCompleteCustomSource = BDSC
        End With

    End Sub


    Private Sub btnSearch_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim iDGKM As Integer
        iDGKM = GetRowKMMCIndex(Me.cbSearch.Text.ToString)

        If iDGKM = -1 Then
            Exit Sub
        End If
        '将当前选中的科目向上移动
        Me.dgKM.CurrentCell = Me.dgKM.Rows(Me.dgKM.Rows.Count - 1).Cells(1)
        Me.dgKM.CurrentCell = Me.dgKM.Rows(iDGKM).Cells(1)
    End Sub
End Class