﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKXKM
    Inherits System.Windows.Forms.Form
    'Inherits FKime.noImeFrom

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.txtKey = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblTS = New System.Windows.Forms.Label
        Me.btnFindNext = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.txtKMQM = New System.Windows.Forms.TextBox
        Me.pbSelect = New System.Windows.Forms.PictureBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.dgKM = New FKKM.fkDGV
        Me.Column1 = New System.Windows.Forms.DataGridViewImageColumn
        CType(Me.pbSelect, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgKM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtKey
        '
        Me.txtKey.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKey.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtKey.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtKey.ForeColor = System.Drawing.Color.Red
        Me.txtKey.Location = New System.Drawing.Point(70, 12)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.Size = New System.Drawing.Size(439, 21)
        Me.txtKey.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "关键字"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(268, 460)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(83, 27)
        Me.btnOK.TabIndex = 4
        Me.btnOK.TabStop = False
        Me.btnOK.Text = "选　　择"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(156, 460)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 27)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "关  闭(&X)"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnNew.Location = New System.Drawing.Point(21, 460)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(104, 27)
        Me.btnNew.TabIndex = 4
        Me.btnNew.TabStop = False
        Me.btnNew.Text = "新建科目(&N)"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 12)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "科目代码　　　　　"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(143, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 12)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "科目名称　　　　　"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(291, 83)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 12)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "助记符"
        '
        'lblTS
        '
        Me.lblTS.AutoSize = True
        Me.lblTS.ForeColor = System.Drawing.Color.Red
        Me.lblTS.Location = New System.Drawing.Point(68, 36)
        Me.lblTS.Name = "lblTS"
        Me.lblTS.Size = New System.Drawing.Size(65, 12)
        Me.lblTS.TabIndex = 3
        Me.lblTS.Text = "未找到科目"
        Me.lblTS.Visible = False
        '
        'btnFindNext
        '
        Me.btnFindNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFindNext.Location = New System.Drawing.Point(511, 9)
        Me.btnFindNext.Name = "btnFindNext"
        Me.btnFindNext.Size = New System.Drawing.Size(104, 27)
        Me.btnFindNext.TabIndex = 4
        Me.btnFindNext.TabStop = False
        Me.btnFindNext.Text = "查找下一个(&F)"
        Me.btnFindNext.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClear.Location = New System.Drawing.Point(540, 10)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(53, 24)
        Me.btnClear.TabIndex = 6
        Me.btnClear.Text = "Button1"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'txtKMQM
        '
        Me.txtKMQM.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKMQM.BackColor = System.Drawing.Color.Snow
        Me.txtKMQM.ForeColor = System.Drawing.Color.Blue
        Me.txtKMQM.Location = New System.Drawing.Point(12, 54)
        Me.txtKMQM.Name = "txtKMQM"
        Me.txtKMQM.ReadOnly = True
        Me.txtKMQM.Size = New System.Drawing.Size(622, 21)
        Me.txtKMQM.TabIndex = 7
        Me.txtKMQM.Text = "3223"
        '
        'pbSelect
        '
        Me.pbSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbSelect.Location = New System.Drawing.Point(345, 154)
        Me.pbSelect.Name = "pbSelect"
        Me.pbSelect.Size = New System.Drawing.Size(300, 300)
        Me.pbSelect.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbSelect.TabIndex = 8
        Me.pbSelect.TabStop = False
        Me.pbSelect.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1200
        '
        'dgKM
        '
        Me.dgKM.AllowUserToAddRows = False
        Me.dgKM.AllowUserToDeleteRows = False
        Me.dgKM.AllowUserToResizeColumns = False
        Me.dgKM.AllowUserToResizeRows = False
        Me.dgKM.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgKM.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgKM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgKM.ColumnHeadersVisible = False
        Me.dgKM.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1})
        Me.dgKM.Location = New System.Drawing.Point(2, 98)
        Me.dgKM.MultiSelect = False
        Me.dgKM.Name = "dgKM"
        Me.dgKM.ReadOnly = True
        Me.dgKM.RowHeadersVisible = False
        Me.dgKM.RowTemplate.Height = 23
        Me.dgKM.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgKM.Size = New System.Drawing.Size(643, 356)
        Me.dgKM.StandardTab = True
        Me.dgKM.TabIndex = 1
        '
        'Column1
        '
        Me.Column1.HeaderText = "Column1"
        Me.Column1.Image = Global.FKKM.My.Resources.Resources.list2
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 15
        '
        'FKXKM
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClear
        Me.ClientSize = New System.Drawing.Size(646, 493)
        Me.ControlBox = False
        Me.Controls.Add(Me.pbSelect)
        Me.Controls.Add(Me.txtKMQM)
        Me.Controls.Add(Me.btnFindNext)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.lblTS)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtKey)
        Me.Controls.Add(Me.dgKM)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FKXKM"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " 科目选择"
        CType(Me.pbSelect, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgKM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    'Friend WithEvents dgKM As System.Windows.Forms.DataGridView
    Friend WithEvents dgKM As FKKM.fkDGV
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents txtKey As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblTS As System.Windows.Forms.Label
    Friend WithEvents btnFindNext As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents txtKMQM As System.Windows.Forms.TextBox
    Friend WithEvents pbSelect As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
