﻿Public Class FKXKM

    Dim bClose As Boolean

    Dim scase As String
    Dim bMJKM As Boolean '用于控制能否返回非末级科目
    Dim bXSFMJKM As Boolean '用于控制是否显示非末级科目，默认为显示所有科目
    Public sKey As String

    Private km As FKG.myselfG.KM
    Public Property myKM() As FKG.myselfG.KM
        Get
            Return km
        End Get
        Set(ByVal value As FKG.myselfG.KM)
            km = value
        End Set
    End Property

    Private Sub FKXKM_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = Not bClose
        'Me.dgKM.CurrentCell = Nothing
        'Me.txtKey.Text = ""
    End Sub


    Private Sub FKXKM_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If FillKM() = False Then

        Else
            BDtxtF()
            km = Nothing

            setMRKey(sender, e)
        End If
        'tT.Start()
        Me.Icon = FKG.myselfG.FKIcon

    End Sub

    Public Event LoadDone(ByVal sender As Object, ByVal e As EventArgs)
    Private Sub setMRKey(ByVal sender As Object, ByVal e As EventArgs) Handles Me.LoadDone
        Me.ActiveControl = Me.txtKey
        Me.txtKey.Text = sKey
    End Sub

    Private Sub BDtxtF()
        '2016年3月24日，取现自动完成关键字功能，有时候反而不方便
        'Dim ds As DataSet
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        'ds = mdb.Reader("select DISTINCT  kmmc from KMDM  where " & scase & " order by kmmc")

        'Dim sCS As New System.Windows.Forms.AutoCompleteStringCollection
        'Dim i As Integer
        'For i = 0 To ds.Tables(0).Rows.Count - 1
        '    sCS.Add(ds.Tables(0).Rows(i).Item(0))
        'Next

        'Me.txtKey.AutoCompleteCustomSource = sCS

    End Sub

    Private Function FillKM() As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)


        '挡用户级别低于2时，不能对科目进行更改
        If mdb.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) < 2 Then
            Me.btnNew.Enabled = False
        End If

        '当用户没有 '科目代码设置' 菜单权限时，不能对科目进行更改
        If mdb.Reader("select * from yonghucdqx where cdquanxian='科目代码设置' and yonghuid=" & FKG.myselfG.YongHuID).Tables(0).Rows.Count < 1 Then
            Me.btnNew.Enabled = False
        End If

        Dim sSelect As String
        If bXSFMJKM Then
            sSelect = "select kmdm as 科目代码,kmmc as 科目名称,zjf as 助记符,kmqm as 科目全称,kmlb,zygs,sfjy,isNull(sldw,'') as dw,SFMJ from kmdm where " & scase & " and Nian=" & FKG.myselfG.NianFen & " order by kmdm"
        Else
            sSelect = "select kmdm as 科目代码,kmmc as 科目名称,zjf as 助记符,kmqm as 科目全称,kmlb,zygs,sfjy,isNull(sldw,'') as dw,SFMJ from kmdm where " & scase & " and SFMJ='1'  and Nian=" & FKG.myselfG.NianFen & " order by kmdm"
        End If

        '新增代码
        Dim dsKM As DataSet
        dsKM = mdb.Reader(sSelect)

        dvKM = dsKM.Tables(0).DefaultView
        dvKM.RowFilter = ""

        Me.dgKM.DataSource = dvKM
        '新增代码

        'mdb.DataBind(Me.dgKM, sSelect)

        If Me.dgKM.RowCount = 0 Then
            Return False
            Exit Function
        End If

        With Me.dgKM
            '.Columns(3).Visible = False
            '.Columns(4).Visible = False
            .Columns(5).Visible = False
            .Columns(6).Visible = False
            .Columns(7).Visible = False
            .Columns(8).Visible = False
            .Columns(9).Visible = False

        End With
        Me.dgKM.Columns(1).Width = 120
        Me.dgKM.Columns(2).Width = 160
        Me.dgKM.Columns(3).Width = 80
        Me.dgKM.Columns(4).Width = 200
        If Me.dgKM.RowCount > 0 Then
            Me.dgKM.CurrentCell = Me.dgKM.Rows(0).Cells(0)
        End If
        Return True
    End Function

    Private Function SetCurrentRow(ByVal sKey As String, ByVal iStartRow As Integer) As Boolean
        If sKey = "" Then
            Return False
            Exit Function
        End If

        '2016年3月24日修改，只循环可见 行
        Dim iRow As Integer
        Dim sTemp As String
        For iRow = iStartRow To Me.dgKM.Rows.GetRowCount(DataGridViewElementStates.Visible) - 1
            sTemp = Me.dgKM.Rows(iRow).Cells(1).Value.ToString
            If sTemp.StartsWith(sKey, StringComparison.CurrentCultureIgnoreCase) Then
                Me.dgKM.CurrentCell = Me.dgKM.Rows(iRow).Cells(1)
                FillKMJG()
                Me.lblTS.Visible = False
                Return True
                Exit Function
            End If
        Next

        For iRow = iStartRow To Me.dgKM.Rows.GetRowCount(DataGridViewElementStates.Visible) - 1
            sTemp = Me.dgKM.Rows(iRow).Cells(2).Value.ToString
            If sTemp.Contains(sKey) Then
                'If sTemp.StartsWith(sKey, StringComparison.CurrentCultureIgnoreCase) Then
                Me.dgKM.CurrentCell = Me.dgKM.Rows(iRow).Cells(2)
                FillKMJG()
                Me.lblTS.Visible = False
                Return True
                Exit Function
            End If
        Next

        For iRow = iStartRow To Me.dgKM.Rows.GetRowCount(DataGridViewElementStates.Visible) - 1
            sTemp = Me.dgKM.Rows(iRow).Cells(3).Value.ToString
            If sTemp.StartsWith(sKey, StringComparison.CurrentCultureIgnoreCase) Then
                Me.dgKM.CurrentCell = Me.dgKM.Rows(iRow).Cells(3)
                FillKMJG()
                Me.lblTS.Visible = False
                Return True
                Exit Function
            End If
        Next

        km = Nothing
        Me.lblTS.Visible = True

    End Function

    Private Sub FillKMJG()
        With Me.dgKM.CurrentRow
            km.sKMDM = .Cells(1).Value
            km.sKMMC = .Cells(2).Value
            km.sZJF = .Cells(3).Value
            km.sKMQM = .Cells(4).Value
            Me.txtKMQM.Text = km.sKMQM

            km.iKMLB = .Cells(5).Value
            km.iZYGS = .Cells(6).Value

            km.bSFJY = .Cells(7).Value
            km.sSLDW = .Cells(8).Value
            'KM.bSFMJ = .Cells(9).Value
        End With
    End Sub

    Private Sub dgKM_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgKM.CellDoubleClick
        btnOK_Click(sender, e)
    End Sub

    Dim dsPic As New DataSet
    Private Sub dgKM_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgKM.CurrentCellChanged
        dsPic.Clear()
        'If Me.dgKM.DataMember = "" Or IsNothing(Me.dgKM.CurrentRow) Then
        '2016年3月24日修改
        If IsNothing(Me.dgKM.DataSource) Or IsNothing(Me.dgKM.CurrentRow) Then

        Else
            FillKMJG()
            '20200829升级，选中科目后自动弹出图片
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If mdb.Reader("select zhi from ZTSX where ShuXing='是否启用产品图片'").Tables(0).Rows.Count > 0 AndAlso mdb.Reader("select zhi from ZTSX where ShuXing='是否启用产品图片'").Tables(0).Rows(0).Item(0).ToString = "是" Then
                Dim sKMDMPic As String = Me.dgKM.CurrentRow.Cells(1).Value
                'Dim ds As DataSet
                If mdb.bExsit("SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductPic]') AND type in (N'U')") Then
                    dsPic = mdb.Reader("select * from ProductPic where kmdm='" & sKMDMPic & "'")
                    Do While dsPic.Tables(0).Rows.Count = 0 And sKMDMPic.Length > 7
                        dsPic.Clear()
                        Dim sSJKMDPic As String
                        sSJKMDPic = FKF.FKF.getSJKM(sKMDMPic)
                        dsPic = mdb.Reader("select * from ProductPic where kmdm='" & sSJKMDPic & "'")
                        sKMDMPic = sSJKMDPic
                    Loop

                    If dsPic.Tables(0).Rows.Count > 0 Then
                        '设置图片框位
                        Timer1_Tick(sender, e)
                        Me.Timer1.Start()

                    Else
                        '   Me.flpPic.Hide()
                        Me.pbSelect.Hide()
                        Me.Timer1.Stop()
                    End If
                Else
                    Me.pbSelect.Hide()
                    Me.Timer1.Stop()
                End If
            Else
                Me.pbSelect.Hide()
                Me.Timer1.Stop()
            End If

        End If
    End Sub

    Dim sKeyInput As String
    Private Sub dgKM_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgKM.KeyUp
        If e.KeyCode = Keys.Enter Then
            btnOK_Click(sender, e)
            Exit Sub
        End If

        If (e.KeyCode >= Keys.A And e.KeyCode <= Keys.Z) Or (e.KeyCode >= Keys.D0 And e.KeyCode <= Keys.D9) Or (e.KeyCode >= Keys.NumPad0 And e.KeyCode <= Keys.NumPad9) Then
            sKeyInput = sKeyInput & e.KeyCode.ToString.Chars(e.KeyCode.ToString.Length - 1).ToString
            Me.txtKey.Text = sKeyInput
            If SetCurrentRow(sKeyInput, 0) Then
                Me.lblTS.Visible = False
            Else
                km = Nothing
                Me.lblTS.Visible = True
            End If
        End If

    End Sub

    Private Sub txtKey_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtKey.KeyUp
        If e.KeyData = Keys.Down Or e.KeyData = Keys.Up Then
            Me.ActiveControl = Me.dgKM
        End If
    End Sub

    Private Sub txtKey_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKey.TextChanged

        sKeyInput = Me.txtKey.Text
       
        '2016年3月23日，增加按照关键字筛选功能，即只显示含有关键字的行。
        FilterDGKM(sKeyInput)

        SetCurrentRow(sKeyInput, 0)
    End Sub

    Dim dvKM As New DataView
    Private Sub FilterDGKM(ByVal sKey As String)
        If sKey = "" Then
            dvKM.RowFilter = ""
        Else
            dvKM.RowFilter = "科目代码 like '*" & sKey & "*' or 科目名称 like '*" & sKey & "*'  or 助记符 like '*" & sKey & "*' or 科目全称 like '*" & sKey & "*'"

        End If
        Me.dgKM.DataSource = dvKM

        'Me.dgKM.CurrentCell = Me.dgKM.Rows(dgKM.Rows.GetFirstRow(DataGridViewElementStates.Visible)).Cells(0)

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If IsNothing(km.sKMDM) Then
            bClose = False
        Else
            If bMJKM Then '当指定必须返回末级科目时，进行判断
                If FKF.FKF.isMJKM(km.sKMDM) Then
                    bClose = True
                Else
                    bClose = False
                End If
            Else
                bClose = True
            End If
        End If
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        myKM = Nothing
        bClose = True
        Me.Close()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim dlg As New FKKM.KMDM("yfKTZwh2PdEl/TAigXkNkpcbd0SxqISxvel5H7WUEq4Cc1l9fG542o5u5lwzYdpn2zn3Noz0HsL/4KP0yGKCkvZbfaNdVRlZZcCWdcxOCdfAlN+AcaSmxxakPXHCvsnqOA/SilwQkas+GlwVeOCosCG8fgiQOOSnkXdCycMYoPq7bJCrd1SNt5aGdGSa+Enu5nOYNDvOSK9BQ17556yp6oijLEWC1aci/LCVJScSXdRNTICRr1qjNCjzroGwkHI82TxeUVSLe6ZUo4KaIwKyL2ST3Ra1AJ82S4Z5D1+FsVFgzEpgs/chLDrX68k8M0HsseKm9CxhzfwpOVrHzxynggfwTosMlZYHOniYVTpWkFnRVp/8ICohthbs/EATlfXxzoER3uavf4vck8n+17qto4c7OtuauYi7PpYpMKPsb5Kla4UybARwhFZGTRJbC53ecbQjLQxT41JFuOlWYSIX6KXZLTsB9pKp00HOK3jK0tzC70NALfjI1/E1/E8WqEWUOrBB4apsgGoueGnkRagLGd62urjeEVOmgnouXotyhYH0zFq7UEHeTa46qbnF6LvJ0id8Esqek/KOFoe4BIugOmhKOvg45sDr7F7BwBGBJqt4CjdJVapNIqAjTzI468XoyfzKPu2I88s9YEC4jU9HIHWcMVeAntpN8RkCClBf5ao=", " and (" & scase & ")")
        If dlg.DialogResult = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        dlg.ShowDialog()
        FillKM()

        Me.ActiveControl = Me.btnOK
        'End If
    End Sub

    Public Sub New(ByVal sTiaoJian As String, ByVal MJKM As Boolean, Optional ByVal XSFMJKM As Boolean = True)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        scase = sTiaoJian
        bMJKM = MJKM
        bXSFMJKM = XSFMJKM
    End Sub

    Private Sub btnFindNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindNext.Click
        If Me.txtKey.Text = "" Then
            Exit Sub
        End If

        If SetCurrentRow(Me.txtKey.Text, Me.dgKM.CurrentRow.Index + 1) Then
        Else
            If MessageBox.Show("已搜索到最后，是否重新从头开始搜素？", My.Application.Info.Title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                SetCurrentRow(Me.txtKey.Text, 0)
            End If
        End If

        Me.ActiveControl = Me.btnOK
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Me.txtKey.Clear()
        km = Nothing

        Me.ActiveControl = Me.txtKey
        Me.txtKey.Focus()
    End Sub

    Dim iT As Integer = 0
    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If IO.Directory.Exists(My.Application.Info.DirectoryPath & "\ProductPic") Then
            Try
                iT = (iT + 1) Mod 5 + 1

                If dsPic.Tables(0).Rows(0).Item("ZT" & iT.ToString).ToString <> "" Then
                    Me.pbSelect.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & dsPic.Tables(0).Rows(0).Item("ZT" & iT.ToString).ToString)
                    Me.pbSelect.Show()

                Else
                    iT = 1
                    If dsPic.Tables(0).Rows(0).Item("ZT" & iT.ToString).ToString <> "" Then
                        Me.pbSelect.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & dsPic.Tables(0).Rows(0).Item("ZT" & iT.ToString).ToString)
                        Me.pbSelect.Show()

                    End If

                    iT = 0
                End If

            Catch ex As Exception
                Me.Timer1.Stop()
                'MsgBox(ex.ToString)
            End Try
        Else
            IO.Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\ProductPic")
        End If
    End Sub
End Class