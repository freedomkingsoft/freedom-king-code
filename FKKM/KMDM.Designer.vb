﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KMDM
    Inherits System.Windows.Forms.Form
    'Inherits FKime.imeForm
    'Inherits FKime.noImeFrom

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgKM = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewImageColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tc = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.txtKMBM = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.CBww = New System.Windows.Forms.CheckBox
        Me.CBwl = New System.Windows.Forms.CheckBox
        Me.CBgz = New System.Windows.Forms.CheckBox
        Me.CBkc = New System.Windows.Forms.CheckBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtKMMC = New System.Windows.Forms.TextBox
        Me.txtZJF = New System.Windows.Forms.TextBox
        Me.txtKMQM = New System.Windows.Forms.TextBox
        Me.txtKMDM = New System.Windows.Forms.TextBox
        Me.cbAutoADD = New System.Windows.Forms.CheckBox
        Me.cbJinYong = New System.Windows.Forms.CheckBox
        Me.Print = New System.Windows.Forms.Button
        Me.SaveKM = New System.Windows.Forms.Button
        Me.DelKM = New System.Windows.Forms.Button
        Me.AddNew = New System.Windows.Forms.Button
        Me.gbZHGS = New System.Windows.Forms.GroupBox
        Me.RadioButton7 = New System.Windows.Forms.RadioButton
        Me.txtDanWei = New System.Windows.Forms.TextBox
        Me.RadioButton6 = New System.Windows.Forms.RadioButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.gbKMXZ = New System.Windows.Forms.GroupBox
        Me.RadioButton5 = New System.Windows.Forms.RadioButton
        Me.RadioButton4 = New System.Windows.Forms.RadioButton
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.tcFZSX = New System.Windows.Forms.TabPage
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.Button2 = New System.Windows.Forms.Button
        Me.btnDaoRu = New System.Windows.Forms.Button
        Me.btnDaoChu = New System.Windows.Forms.Button
        Me.btnAddFZSX = New System.Windows.Forms.Button
        Me.txtFZSXKMMC = New System.Windows.Forms.TextBox
        Me.txtFZSXKMDM = New System.Windows.Forms.TextBox
        Me.dgvFZSX = New System.Windows.Forms.DataGridView
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.tpPic = New System.Windows.Forms.TabPage
        Me.Label14 = New System.Windows.Forms.Label
        Me.btnUploadPic4 = New System.Windows.Forms.Button
        Me.btnUploadPic5 = New System.Windows.Forms.Button
        Me.btnSelectpic4 = New System.Windows.Forms.Button
        Me.btnSelectpic5 = New System.Windows.Forms.Button
        Me.btnUploadPic3 = New System.Windows.Forms.Button
        Me.btnSelectpic3 = New System.Windows.Forms.Button
        Me.btnUploadPic2 = New System.Windows.Forms.Button
        Me.btnSelectpic2 = New System.Windows.Forms.Button
        Me.btnClearPic5 = New System.Windows.Forms.Button
        Me.btnClearPic4 = New System.Windows.Forms.Button
        Me.btnClearPic3 = New System.Windows.Forms.Button
        Me.btnClearPic2 = New System.Windows.Forms.Button
        Me.btnClearPic1 = New System.Windows.Forms.Button
        Me.btnUploadPic1 = New System.Windows.Forms.Button
        Me.btnSelectpic1 = New System.Windows.Forms.Button
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox4 = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.btnSavePic = New System.Windows.Forms.Button
        Me.txtKMMCPic = New System.Windows.Forms.TextBox
        Me.txtKMDMPic = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.PictureBox6 = New System.Windows.Forms.PictureBox
        Me.sts = New System.Windows.Forms.StatusStrip
        Me.状态 = New System.Windows.Forms.ToolStripStatusLabel
        Me.tsS = New System.Windows.Forms.ToolStripStatusLabel
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.DataGridView2 = New System.Windows.Forms.DataGridView
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.openFileImage = New System.Windows.Forms.OpenFileDialog
        Me.cbSearch = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.btnSearch = New System.Windows.Forms.Button
        CType(Me.dgKM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.tc.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.gbZHGS.SuspendLayout()
        Me.gbKMXZ.SuspendLayout()
        Me.tcFZSX.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvFZSX, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpPic.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sts.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgKM
        '
        Me.dgKM.AllowUserToAddRows = False
        Me.dgKM.AllowUserToDeleteRows = False
        Me.dgKM.AllowUserToResizeColumns = False
        Me.dgKM.AllowUserToResizeRows = False
        Me.dgKM.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgKM.CausesValidation = False
        Me.dgKM.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgKM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgKM.ColumnHeadersVisible = False
        Me.dgKM.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1})
        Me.dgKM.Location = New System.Drawing.Point(2, 38)
        Me.dgKM.MultiSelect = False
        Me.dgKM.Name = "dgKM"
        Me.dgKM.ReadOnly = True
        Me.dgKM.RowHeadersVisible = False
        Me.dgKM.RowTemplate.Height = 23
        Me.dgKM.Size = New System.Drawing.Size(312, 418)
        Me.dgKM.TabIndex = 0
        Me.dgKM.TabStop = False
        '
        'Column1
        '
        Me.Column1.HeaderText = "Column1"
        Me.Column1.Image = Global.FKKM.My.Resources.Resources.list2
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 15
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tc)
        Me.GroupBox1.Location = New System.Drawing.Point(314, 1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(454, 455)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'tc
        '
        Me.tc.Controls.Add(Me.TabPage1)
        Me.tc.Controls.Add(Me.tcFZSX)
        Me.tc.Controls.Add(Me.tpPic)
        Me.tc.Location = New System.Drawing.Point(6, 10)
        Me.tc.Name = "tc"
        Me.tc.SelectedIndex = 0
        Me.tc.Size = New System.Drawing.Size(437, 445)
        Me.tc.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtKMBM)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.btnExit)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.txtKMMC)
        Me.TabPage1.Controls.Add(Me.txtZJF)
        Me.TabPage1.Controls.Add(Me.txtKMQM)
        Me.TabPage1.Controls.Add(Me.txtKMDM)
        Me.TabPage1.Controls.Add(Me.cbAutoADD)
        Me.TabPage1.Controls.Add(Me.cbJinYong)
        Me.TabPage1.Controls.Add(Me.Print)
        Me.TabPage1.Controls.Add(Me.SaveKM)
        Me.TabPage1.Controls.Add(Me.DelKM)
        Me.TabPage1.Controls.Add(Me.AddNew)
        Me.TabPage1.Controls.Add(Me.gbZHGS)
        Me.TabPage1.Controls.Add(Me.gbKMXZ)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(429, 419)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "基本设置"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtKMBM
        '
        Me.txtKMBM.Location = New System.Drawing.Point(76, 90)
        Me.txtKMBM.Name = "txtKMBM"
        Me.txtKMBM.Size = New System.Drawing.Size(313, 21)
        Me.txtKMBM.TabIndex = 13
        Me.txtKMBM.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(23, 93)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 12)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "科目编码"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CBww)
        Me.GroupBox2.Controls.Add(Me.CBwl)
        Me.GroupBox2.Controls.Add(Me.CBgz)
        Me.GroupBox2.Controls.Add(Me.CBkc)
        Me.GroupBox2.Location = New System.Drawing.Point(22, 281)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(386, 55)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "核算类别"
        '
        'CBww
        '
        Me.CBww.AutoSize = True
        Me.CBww.Location = New System.Drawing.Point(302, 20)
        Me.CBww.Name = "CBww"
        Me.CBww.Size = New System.Drawing.Size(72, 16)
        Me.CBww.TabIndex = 1
        Me.CBww.Text = "委外核算"
        Me.CBww.UseVisualStyleBackColor = True
        '
        'CBwl
        '
        Me.CBwl.AutoSize = True
        Me.CBwl.Location = New System.Drawing.Point(207, 20)
        Me.CBwl.Name = "CBwl"
        Me.CBwl.Size = New System.Drawing.Size(72, 16)
        Me.CBwl.TabIndex = 1
        Me.CBwl.Text = "往来核算"
        Me.CBwl.UseVisualStyleBackColor = True
        '
        'CBgz
        '
        Me.CBgz.AutoSize = True
        Me.CBgz.Location = New System.Drawing.Point(112, 20)
        Me.CBgz.Name = "CBgz"
        Me.CBgz.Size = New System.Drawing.Size(72, 16)
        Me.CBgz.TabIndex = 1
        Me.CBgz.Text = "工资核算"
        Me.CBgz.UseVisualStyleBackColor = True
        '
        'CBkc
        '
        Me.CBkc.AutoSize = True
        Me.CBkc.Location = New System.Drawing.Point(17, 20)
        Me.CBkc.Name = "CBkc"
        Me.CBkc.Size = New System.Drawing.Size(72, 16)
        Me.CBkc.TabIndex = 1
        Me.CBkc.Text = "库存核算"
        Me.CBkc.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnExit.Location = New System.Drawing.Point(350, 368)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(58, 35)
        Me.btnExit.TabIndex = 2
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "退出&X"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(360, 374)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 29)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtKMMC
        '
        Me.txtKMMC.Location = New System.Drawing.Point(243, 20)
        Me.txtKMMC.Name = "txtKMMC"
        Me.txtKMMC.Size = New System.Drawing.Size(146, 21)
        Me.txtKMMC.TabIndex = 2
        '
        'txtZJF
        '
        Me.txtZJF.Location = New System.Drawing.Point(301, 53)
        Me.txtZJF.Name = "txtZJF"
        Me.txtZJF.Size = New System.Drawing.Size(88, 21)
        Me.txtZJF.TabIndex = 3
        '
        'txtKMQM
        '
        Me.txtKMQM.Location = New System.Drawing.Point(76, 53)
        Me.txtKMQM.Name = "txtKMQM"
        Me.txtKMQM.ReadOnly = True
        Me.txtKMQM.Size = New System.Drawing.Size(169, 21)
        Me.txtKMQM.TabIndex = 4
        Me.txtKMQM.TabStop = False
        '
        'txtKMDM
        '
        Me.txtKMDM.Location = New System.Drawing.Point(76, 20)
        Me.txtKMDM.Name = "txtKMDM"
        Me.txtKMDM.Size = New System.Drawing.Size(88, 21)
        Me.txtKMDM.TabIndex = 1
        '
        'cbAutoADD
        '
        Me.cbAutoADD.AutoSize = True
        Me.cbAutoADD.Location = New System.Drawing.Point(39, 343)
        Me.cbAutoADD.Name = "cbAutoADD"
        Me.cbAutoADD.Size = New System.Drawing.Size(96, 16)
        Me.cbAutoADD.TabIndex = 3
        Me.cbAutoADD.TabStop = False
        Me.cbAutoADD.Text = "连续新增科目"
        Me.cbAutoADD.UseVisualStyleBackColor = True
        '
        'cbJinYong
        '
        Me.cbJinYong.AutoSize = True
        Me.cbJinYong.Location = New System.Drawing.Point(300, 342)
        Me.cbJinYong.Name = "cbJinYong"
        Me.cbJinYong.Size = New System.Drawing.Size(108, 16)
        Me.cbJinYong.TabIndex = 3
        Me.cbJinYong.TabStop = False
        Me.cbJinYong.Text = "此科目禁止删除"
        Me.cbJinYong.UseVisualStyleBackColor = True
        Me.cbJinYong.Visible = False
        '
        'Print
        '
        Me.Print.Location = New System.Drawing.Point(269, 368)
        Me.Print.Name = "Print"
        Me.Print.Size = New System.Drawing.Size(58, 35)
        Me.Print.TabIndex = 2
        Me.Print.TabStop = False
        Me.Print.Text = "打印&P"
        Me.Print.UseVisualStyleBackColor = True
        '
        'SaveKM
        '
        Me.SaveKM.Location = New System.Drawing.Point(188, 368)
        Me.SaveKM.Name = "SaveKM"
        Me.SaveKM.Size = New System.Drawing.Size(58, 35)
        Me.SaveKM.TabIndex = 2
        Me.SaveKM.TabStop = False
        Me.SaveKM.Text = "保存&S"
        Me.SaveKM.UseVisualStyleBackColor = True
        '
        'DelKM
        '
        Me.DelKM.Location = New System.Drawing.Point(107, 368)
        Me.DelKM.Name = "DelKM"
        Me.DelKM.Size = New System.Drawing.Size(58, 35)
        Me.DelKM.TabIndex = 2
        Me.DelKM.TabStop = False
        Me.DelKM.Text = "删除&D"
        Me.DelKM.UseVisualStyleBackColor = True
        '
        'AddNew
        '
        Me.AddNew.Location = New System.Drawing.Point(26, 368)
        Me.AddNew.Name = "AddNew"
        Me.AddNew.Size = New System.Drawing.Size(58, 35)
        Me.AddNew.TabIndex = 2
        Me.AddNew.TabStop = False
        Me.AddNew.Text = "新增&N"
        Me.AddNew.UseVisualStyleBackColor = True
        '
        'gbZHGS
        '
        Me.gbZHGS.Controls.Add(Me.RadioButton7)
        Me.gbZHGS.Controls.Add(Me.txtDanWei)
        Me.gbZHGS.Controls.Add(Me.RadioButton6)
        Me.gbZHGS.Controls.Add(Me.Label5)
        Me.gbZHGS.Location = New System.Drawing.Point(22, 202)
        Me.gbZHGS.Name = "gbZHGS"
        Me.gbZHGS.Size = New System.Drawing.Size(386, 68)
        Me.gbZHGS.TabIndex = 9
        Me.gbZHGS.TabStop = False
        Me.gbZHGS.Text = "账户格式"
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Location = New System.Drawing.Point(112, 30)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(83, 16)
        Me.RadioButton7.TabIndex = 0
        Me.RadioButton7.Text = "数量金额式"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'txtDanWei
        '
        Me.txtDanWei.Location = New System.Drawing.Point(279, 28)
        Me.txtDanWei.Name = "txtDanWei"
        Me.txtDanWei.Size = New System.Drawing.Size(88, 21)
        Me.txtDanWei.TabIndex = 4
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Location = New System.Drawing.Point(15, 30)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(59, 16)
        Me.RadioButton6.TabIndex = 0
        Me.RadioButton6.Text = "三栏式"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(220, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 12)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "数量单位"
        '
        'gbKMXZ
        '
        Me.gbKMXZ.Controls.Add(Me.RadioButton5)
        Me.gbKMXZ.Controls.Add(Me.RadioButton4)
        Me.gbKMXZ.Controls.Add(Me.RadioButton3)
        Me.gbKMXZ.Controls.Add(Me.RadioButton2)
        Me.gbKMXZ.Controls.Add(Me.RadioButton1)
        Me.gbKMXZ.Location = New System.Drawing.Point(22, 126)
        Me.gbKMXZ.Name = "gbKMXZ"
        Me.gbKMXZ.Size = New System.Drawing.Size(386, 68)
        Me.gbKMXZ.TabIndex = 8
        Me.gbKMXZ.TabStop = False
        Me.gbKMXZ.Text = "科目性质"
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Location = New System.Drawing.Point(308, 30)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(59, 16)
        Me.RadioButton5.TabIndex = 0
        Me.RadioButton5.Text = "损益类"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(234, 30)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(59, 16)
        Me.RadioButton4.TabIndex = 0
        Me.RadioButton4.Text = "成本类"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(164, 30)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(59, 16)
        Me.RadioButton3.TabIndex = 0
        Me.RadioButton3.Text = "权益类"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(92, 30)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(59, 16)
        Me.RadioButton2.TabIndex = 0
        Me.RadioButton2.Text = "负债类"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(15, 30)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(59, 16)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.Text = "资产类"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(254, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 12)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "助记符"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "科目全称"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(184, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "科目名称"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "科目代码"
        '
        'tcFZSX
        '
        Me.tcFZSX.Controls.Add(Me.dgv)
        Me.tcFZSX.Controls.Add(Me.Button2)
        Me.tcFZSX.Controls.Add(Me.btnDaoRu)
        Me.tcFZSX.Controls.Add(Me.btnDaoChu)
        Me.tcFZSX.Controls.Add(Me.btnAddFZSX)
        Me.tcFZSX.Controls.Add(Me.txtFZSXKMMC)
        Me.tcFZSX.Controls.Add(Me.txtFZSXKMDM)
        Me.tcFZSX.Controls.Add(Me.dgvFZSX)
        Me.tcFZSX.Controls.Add(Me.Label6)
        Me.tcFZSX.Controls.Add(Me.Label7)
        Me.tcFZSX.Location = New System.Drawing.Point(4, 22)
        Me.tcFZSX.Name = "tcFZSX"
        Me.tcFZSX.Padding = New System.Windows.Forms.Padding(3)
        Me.tcFZSX.Size = New System.Drawing.Size(429, 419)
        Me.tcFZSX.TabIndex = 1
        Me.tcFZSX.Text = "辅助属性设置"
        Me.tcFZSX.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(9, 279)
        Me.dgv.Name = "dgv"
        Me.dgv.RowTemplate.Height = 23
        Me.dgv.Size = New System.Drawing.Size(96, 25)
        Me.dgv.TabIndex = 11
        Me.dgv.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(335, 294)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(58, 35)
        Me.Button2.TabIndex = 10
        Me.Button2.TabStop = False
        Me.Button2.Text = "退出&X"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btnDaoRu
        '
        Me.btnDaoRu.Location = New System.Drawing.Point(247, 349)
        Me.btnDaoRu.Name = "btnDaoRu"
        Me.btnDaoRu.Size = New System.Drawing.Size(146, 34)
        Me.btnDaoRu.TabIndex = 9
        Me.btnDaoRu.Text = "导入辅助属性"
        Me.btnDaoRu.UseVisualStyleBackColor = True
        '
        'btnDaoChu
        '
        Me.btnDaoChu.Location = New System.Drawing.Point(29, 349)
        Me.btnDaoChu.Name = "btnDaoChu"
        Me.btnDaoChu.Size = New System.Drawing.Size(161, 34)
        Me.btnDaoChu.TabIndex = 9
        Me.btnDaoChu.Text = "导出科目代码及辅助属性"
        Me.btnDaoChu.UseVisualStyleBackColor = True
        '
        'btnAddFZSX
        '
        Me.btnAddFZSX.Location = New System.Drawing.Point(162, 295)
        Me.btnAddFZSX.Name = "btnAddFZSX"
        Me.btnAddFZSX.Size = New System.Drawing.Size(90, 34)
        Me.btnAddFZSX.TabIndex = 9
        Me.btnAddFZSX.Text = "保存"
        Me.btnAddFZSX.UseVisualStyleBackColor = True
        '
        'txtFZSXKMMC
        '
        Me.txtFZSXKMMC.Location = New System.Drawing.Point(247, 22)
        Me.txtFZSXKMMC.Name = "txtFZSXKMMC"
        Me.txtFZSXKMMC.ReadOnly = True
        Me.txtFZSXKMMC.Size = New System.Drawing.Size(146, 21)
        Me.txtFZSXKMMC.TabIndex = 6
        '
        'txtFZSXKMDM
        '
        Me.txtFZSXKMDM.Location = New System.Drawing.Point(82, 22)
        Me.txtFZSXKMDM.Name = "txtFZSXKMDM"
        Me.txtFZSXKMDM.ReadOnly = True
        Me.txtFZSXKMDM.Size = New System.Drawing.Size(88, 21)
        Me.txtFZSXKMDM.TabIndex = 5
        '
        'dgvFZSX
        '
        Me.dgvFZSX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFZSX.Location = New System.Drawing.Point(6, 59)
        Me.dgvFZSX.Name = "dgvFZSX"
        Me.dgvFZSX.RowTemplate.Height = 23
        Me.dgvFZSX.Size = New System.Drawing.Size(410, 214)
        Me.dgvFZSX.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(188, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 12)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "科目名称"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 12)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "科目代码"
        '
        'tpPic
        '
        Me.tpPic.Controls.Add(Me.Label14)
        Me.tpPic.Controls.Add(Me.btnUploadPic4)
        Me.tpPic.Controls.Add(Me.btnUploadPic5)
        Me.tpPic.Controls.Add(Me.btnSelectpic4)
        Me.tpPic.Controls.Add(Me.btnSelectpic5)
        Me.tpPic.Controls.Add(Me.btnUploadPic3)
        Me.tpPic.Controls.Add(Me.btnSelectpic3)
        Me.tpPic.Controls.Add(Me.btnUploadPic2)
        Me.tpPic.Controls.Add(Me.btnSelectpic2)
        Me.tpPic.Controls.Add(Me.btnClearPic5)
        Me.tpPic.Controls.Add(Me.btnClearPic4)
        Me.tpPic.Controls.Add(Me.btnClearPic3)
        Me.tpPic.Controls.Add(Me.btnClearPic2)
        Me.tpPic.Controls.Add(Me.btnClearPic1)
        Me.tpPic.Controls.Add(Me.btnUploadPic1)
        Me.tpPic.Controls.Add(Me.btnSelectpic1)
        Me.tpPic.Controls.Add(Me.PictureBox3)
        Me.tpPic.Controls.Add(Me.PictureBox5)
        Me.tpPic.Controls.Add(Me.PictureBox2)
        Me.tpPic.Controls.Add(Me.PictureBox4)
        Me.tpPic.Controls.Add(Me.PictureBox1)
        Me.tpPic.Controls.Add(Me.btnSavePic)
        Me.tpPic.Controls.Add(Me.txtKMMCPic)
        Me.tpPic.Controls.Add(Me.txtKMDMPic)
        Me.tpPic.Controls.Add(Me.Label11)
        Me.tpPic.Controls.Add(Me.Label13)
        Me.tpPic.Controls.Add(Me.Label12)
        Me.tpPic.Controls.Add(Me.PictureBox6)
        Me.tpPic.Location = New System.Drawing.Point(4, 22)
        Me.tpPic.Name = "tpPic"
        Me.tpPic.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPic.Size = New System.Drawing.Size(429, 419)
        Me.tpPic.TabIndex = 2
        Me.tpPic.Text = "设置产品图片"
        Me.tpPic.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label14.Location = New System.Drawing.Point(49, 362)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(344, 16)
        Me.Label14.TabIndex = 11
        Me.Label14.Text = "对图片信息进行更改后，需要点击保存图片生效"
        '
        'btnUploadPic4
        '
        Me.btnUploadPic4.Location = New System.Drawing.Point(94, 245)
        Me.btnUploadPic4.Name = "btnUploadPic4"
        Me.btnUploadPic4.Size = New System.Drawing.Size(39, 24)
        Me.btnUploadPic4.TabIndex = 10
        Me.btnUploadPic4.Text = "上传"
        Me.btnUploadPic4.UseVisualStyleBackColor = True
        Me.btnUploadPic4.Visible = False
        '
        'btnUploadPic5
        '
        Me.btnUploadPic5.Location = New System.Drawing.Point(232, 245)
        Me.btnUploadPic5.Name = "btnUploadPic5"
        Me.btnUploadPic5.Size = New System.Drawing.Size(39, 24)
        Me.btnUploadPic5.TabIndex = 10
        Me.btnUploadPic5.Text = "上传"
        Me.btnUploadPic5.UseVisualStyleBackColor = True
        Me.btnUploadPic5.Visible = False
        '
        'btnSelectpic4
        '
        Me.btnSelectpic4.Location = New System.Drawing.Point(43, 271)
        Me.btnSelectpic4.Name = "btnSelectpic4"
        Me.btnSelectpic4.Size = New System.Drawing.Size(39, 24)
        Me.btnSelectpic4.TabIndex = 10
        Me.btnSelectpic4.Text = "选择"
        Me.btnSelectpic4.UseVisualStyleBackColor = True
        '
        'btnSelectpic5
        '
        Me.btnSelectpic5.Location = New System.Drawing.Point(181, 271)
        Me.btnSelectpic5.Name = "btnSelectpic5"
        Me.btnSelectpic5.Size = New System.Drawing.Size(39, 24)
        Me.btnSelectpic5.TabIndex = 10
        Me.btnSelectpic5.Text = "选择"
        Me.btnSelectpic5.UseVisualStyleBackColor = True
        '
        'btnUploadPic3
        '
        Me.btnUploadPic3.Location = New System.Drawing.Point(354, 102)
        Me.btnUploadPic3.Name = "btnUploadPic3"
        Me.btnUploadPic3.Size = New System.Drawing.Size(39, 24)
        Me.btnUploadPic3.TabIndex = 10
        Me.btnUploadPic3.Text = "上传"
        Me.btnUploadPic3.UseVisualStyleBackColor = True
        Me.btnUploadPic3.Visible = False
        '
        'btnSelectpic3
        '
        Me.btnSelectpic3.Location = New System.Drawing.Point(307, 124)
        Me.btnSelectpic3.Name = "btnSelectpic3"
        Me.btnSelectpic3.Size = New System.Drawing.Size(39, 24)
        Me.btnSelectpic3.TabIndex = 10
        Me.btnSelectpic3.Text = "选择"
        Me.btnSelectpic3.UseVisualStyleBackColor = True
        '
        'btnUploadPic2
        '
        Me.btnUploadPic2.Location = New System.Drawing.Point(232, 102)
        Me.btnUploadPic2.Name = "btnUploadPic2"
        Me.btnUploadPic2.Size = New System.Drawing.Size(39, 24)
        Me.btnUploadPic2.TabIndex = 10
        Me.btnUploadPic2.Text = "上传"
        Me.btnUploadPic2.UseVisualStyleBackColor = True
        Me.btnUploadPic2.Visible = False
        '
        'btnSelectpic2
        '
        Me.btnSelectpic2.Location = New System.Drawing.Point(181, 124)
        Me.btnSelectpic2.Name = "btnSelectpic2"
        Me.btnSelectpic2.Size = New System.Drawing.Size(39, 24)
        Me.btnSelectpic2.TabIndex = 10
        Me.btnSelectpic2.Text = "选择"
        Me.btnSelectpic2.UseVisualStyleBackColor = True
        '
        'btnClearPic5
        '
        Me.btnClearPic5.Location = New System.Drawing.Point(232, 271)
        Me.btnClearPic5.Name = "btnClearPic5"
        Me.btnClearPic5.Size = New System.Drawing.Size(39, 24)
        Me.btnClearPic5.TabIndex = 10
        Me.btnClearPic5.Text = "清除"
        Me.btnClearPic5.UseVisualStyleBackColor = True
        '
        'btnClearPic4
        '
        Me.btnClearPic4.Location = New System.Drawing.Point(94, 271)
        Me.btnClearPic4.Name = "btnClearPic4"
        Me.btnClearPic4.Size = New System.Drawing.Size(39, 24)
        Me.btnClearPic4.TabIndex = 10
        Me.btnClearPic4.Text = "清除"
        Me.btnClearPic4.UseVisualStyleBackColor = True
        '
        'btnClearPic3
        '
        Me.btnClearPic3.Location = New System.Drawing.Point(354, 124)
        Me.btnClearPic3.Name = "btnClearPic3"
        Me.btnClearPic3.Size = New System.Drawing.Size(39, 24)
        Me.btnClearPic3.TabIndex = 10
        Me.btnClearPic3.Text = "清除"
        Me.btnClearPic3.UseVisualStyleBackColor = True
        '
        'btnClearPic2
        '
        Me.btnClearPic2.Location = New System.Drawing.Point(232, 124)
        Me.btnClearPic2.Name = "btnClearPic2"
        Me.btnClearPic2.Size = New System.Drawing.Size(39, 24)
        Me.btnClearPic2.TabIndex = 10
        Me.btnClearPic2.Text = "清除"
        Me.btnClearPic2.UseVisualStyleBackColor = True
        '
        'btnClearPic1
        '
        Me.btnClearPic1.Location = New System.Drawing.Point(94, 124)
        Me.btnClearPic1.Name = "btnClearPic1"
        Me.btnClearPic1.Size = New System.Drawing.Size(39, 24)
        Me.btnClearPic1.TabIndex = 10
        Me.btnClearPic1.Text = "清除"
        Me.btnClearPic1.UseVisualStyleBackColor = True
        '
        'btnUploadPic1
        '
        Me.btnUploadPic1.Location = New System.Drawing.Point(94, 102)
        Me.btnUploadPic1.Name = "btnUploadPic1"
        Me.btnUploadPic1.Size = New System.Drawing.Size(39, 24)
        Me.btnUploadPic1.TabIndex = 10
        Me.btnUploadPic1.Text = "上传"
        Me.btnUploadPic1.UseVisualStyleBackColor = True
        Me.btnUploadPic1.Visible = False
        '
        'btnSelectpic1
        '
        Me.btnSelectpic1.Location = New System.Drawing.Point(43, 124)
        Me.btnSelectpic1.Name = "btnSelectpic1"
        Me.btnSelectpic1.Size = New System.Drawing.Size(39, 24)
        Me.btnSelectpic1.TabIndex = 10
        Me.btnSelectpic1.Text = "选择"
        Me.btnSelectpic1.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox3.Location = New System.Drawing.Point(298, 92)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(109, 96)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 9
        Me.PictureBox3.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox5.Location = New System.Drawing.Point(172, 236)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(109, 96)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 9
        Me.PictureBox5.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox2.Location = New System.Drawing.Point(172, 92)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(109, 96)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 9
        Me.PictureBox2.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox4.Location = New System.Drawing.Point(35, 236)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(109, 96)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 9
        Me.PictureBox4.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(35, 92)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(109, 96)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'btnSavePic
        '
        Me.btnSavePic.Location = New System.Drawing.Point(316, 260)
        Me.btnSavePic.Name = "btnSavePic"
        Me.btnSavePic.Size = New System.Drawing.Size(77, 46)
        Me.btnSavePic.TabIndex = 8
        Me.btnSavePic.Text = "保存图片"
        Me.btnSavePic.UseVisualStyleBackColor = True
        '
        'txtKMMCPic
        '
        Me.txtKMMCPic.Location = New System.Drawing.Point(261, 26)
        Me.txtKMMCPic.Name = "txtKMMCPic"
        Me.txtKMMCPic.ReadOnly = True
        Me.txtKMMCPic.Size = New System.Drawing.Size(146, 21)
        Me.txtKMMCPic.TabIndex = 6
        '
        'txtKMDMPic
        '
        Me.txtKMDMPic.Location = New System.Drawing.Point(94, 26)
        Me.txtKMDMPic.Name = "txtKMDMPic"
        Me.txtKMDMPic.ReadOnly = True
        Me.txtKMDMPic.Size = New System.Drawing.Size(88, 21)
        Me.txtKMDMPic.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(202, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 12)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "科目名称"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(33, 77)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 12)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "主图"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(41, 29)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 12)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "科目代码"
        '
        'PictureBox6
        '
        Me.PictureBox6.Location = New System.Drawing.Point(298, 236)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(109, 96)
        Me.PictureBox6.TabIndex = 9
        Me.PictureBox6.TabStop = False
        '
        'sts
        '
        Me.sts.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.状态, Me.tsS})
        Me.sts.Location = New System.Drawing.Point(0, 464)
        Me.sts.Name = "sts"
        Me.sts.Size = New System.Drawing.Size(759, 22)
        Me.sts.TabIndex = 2
        Me.sts.Text = "状态说明"
        '
        '状态
        '
        Me.状态.Name = "状态"
        Me.状态.Size = New System.Drawing.Size(32, 17)
        Me.状态.Text = "状态"
        '
        'tsS
        '
        Me.tsS.Name = "tsS"
        Me.tsS.Size = New System.Drawing.Size(134, 17)
        Me.tsS.Text = "ToolStripStatusLabel1"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(9, 279)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 23
        Me.DataGridView1.Size = New System.Drawing.Size(96, 25)
        Me.DataGridView1.TabIndex = 11
        Me.DataGridView1.Visible = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(335, 294)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(58, 35)
        Me.Button3.TabIndex = 10
        Me.Button3.TabStop = False
        Me.Button3.Text = "退出&X"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(247, 349)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(146, 34)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "导入辅助属性"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(29, 349)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(161, 34)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "导出科目代码及辅助属性"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(162, 295)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(90, 34)
        Me.Button6.TabIndex = 9
        Me.Button6.Text = "保存"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(247, 22)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(146, 21)
        Me.TextBox1.TabIndex = 6
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(82, 22)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(88, 21)
        Me.TextBox2.TabIndex = 5
        '
        'DataGridView2
        '
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(6, 59)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.RowTemplate.Height = 23
        Me.DataGridView2.Size = New System.Drawing.Size(410, 214)
        Me.DataGridView2.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(188, 27)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 12)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "科目名称"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(27, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 12)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "科目代码"
        '
        'cbSearch
        '
        Me.cbSearch.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cbSearch.FormattingEnabled = True
        Me.cbSearch.Location = New System.Drawing.Point(91, 10)
        Me.cbSearch.Name = "cbSearch"
        Me.cbSearch.Size = New System.Drawing.Size(169, 24)
        Me.cbSearch.TabIndex = 3
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label15.Location = New System.Drawing.Point(13, 14)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 16)
        Me.Label15.TabIndex = 4
        Me.Label15.Text = "快速检索"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(263, 8)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(48, 28)
        Me.btnSearch.TabIndex = 5
        Me.btnSearch.Text = "检索"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'KMDM
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(759, 486)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.cbSearch)
        Me.Controls.Add(Me.sts)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgKM)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "KMDM"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "科目代码设置"
        CType(Me.dgKM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.tc.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbZHGS.ResumeLayout(False)
        Me.gbZHGS.PerformLayout()
        Me.gbKMXZ.ResumeLayout(False)
        Me.gbKMXZ.PerformLayout()
        Me.tcFZSX.ResumeLayout(False)
        Me.tcFZSX.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvFZSX, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpPic.ResumeLayout(False)
        Me.tpPic.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sts.ResumeLayout(False)
        Me.sts.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgKM As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tc As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents tcFZSX As System.Windows.Forms.TabPage
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gbKMXZ As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gbZHGS As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Print As System.Windows.Forms.Button
    Friend WithEvents SaveKM As System.Windows.Forms.Button
    Friend WithEvents DelKM As System.Windows.Forms.Button
    Friend WithEvents AddNew As System.Windows.Forms.Button
    Friend WithEvents cbJinYong As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dgvFZSX As System.Windows.Forms.DataGridView
    Friend WithEvents txtKMMC As System.Windows.Forms.TextBox
    Friend WithEvents txtZJF As System.Windows.Forms.TextBox
    Friend WithEvents txtKMQM As System.Windows.Forms.TextBox
    Friend WithEvents txtKMDM As System.Windows.Forms.TextBox
    Friend WithEvents txtDanWei As System.Windows.Forms.TextBox
    Friend WithEvents txtFZSXKMMC As System.Windows.Forms.TextBox
    Friend WithEvents txtFZSXKMDM As System.Windows.Forms.TextBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents sts As System.Windows.Forms.StatusStrip
    Friend WithEvents 状态 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsS As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnAddFZSX As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CBkc As System.Windows.Forms.CheckBox
    Friend WithEvents CBww As System.Windows.Forms.CheckBox
    Friend WithEvents CBwl As System.Windows.Forms.CheckBox
    Friend WithEvents CBgz As System.Windows.Forms.CheckBox
    Friend WithEvents cbAutoADD As System.Windows.Forms.CheckBox
    Friend WithEvents btnDaoRu As System.Windows.Forms.Button
    Friend WithEvents btnDaoChu As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents txtKMBM As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tpPic As System.Windows.Forms.TabPage
    Friend WithEvents txtKMMCPic As System.Windows.Forms.TextBox
    Friend WithEvents txtKMDMPic As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnSavePic As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents openFileImage As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnUploadPic4 As System.Windows.Forms.Button
    Friend WithEvents btnUploadPic5 As System.Windows.Forms.Button
    Friend WithEvents btnSelectpic4 As System.Windows.Forms.Button
    Friend WithEvents btnSelectpic5 As System.Windows.Forms.Button
    Friend WithEvents btnUploadPic3 As System.Windows.Forms.Button
    Friend WithEvents btnSelectpic3 As System.Windows.Forms.Button
    Friend WithEvents btnUploadPic2 As System.Windows.Forms.Button
    Friend WithEvents btnSelectpic2 As System.Windows.Forms.Button
    Friend WithEvents btnUploadPic1 As System.Windows.Forms.Button
    Friend WithEvents btnSelectpic1 As System.Windows.Forms.Button
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents btnClearPic5 As System.Windows.Forms.Button
    Friend WithEvents btnClearPic4 As System.Windows.Forms.Button
    Friend WithEvents btnClearPic3 As System.Windows.Forms.Button
    Friend WithEvents btnClearPic2 As System.Windows.Forms.Button
    Friend WithEvents btnClearPic1 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cbSearch As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
End Class
