﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKBCX
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel11 = New System.Windows.Forms.TableLayoutPanel
        Me.txtCaseBH = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.myMaxDate = New System.Windows.Forms.DateTimePicker
        Me.myMinDate = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.TableLayoutPanel12 = New System.Windows.Forms.TableLayoutPanel
        Me.txtCaseKH = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.TableLayoutPanel14 = New System.Windows.Forms.TableLayoutPanel
        Me.txtCaseXH = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.btnGaoJi = New System.Windows.Forms.Button
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.cbYC = New System.Windows.Forms.CheckBox
        Me.lblName = New System.Windows.Forms.Label
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel
        Me.btnAddCase = New System.Windows.Forms.Button
        Me.btnIsert = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.cbXiang = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.cbYSF = New System.Windows.Forms.ComboBox
        Me.cbTiaoJian = New System.Windows.Forms.ComboBox
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel
        Me.rbOr = New System.Windows.Forms.RadioButton
        Me.rbAnd = New System.Windows.Forms.RadioButton
        Me.cbKJXiang = New System.Windows.Forms.ComboBox
        Me.txtCase = New System.Windows.Forms.TextBox
        Me.txtKJCase = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel
        Me.btnHuiZong = New System.Windows.Forms.Button
        Me.btnLieSetup = New System.Windows.Forms.Button
        Me.TableLayoutPanel15 = New System.Windows.Forms.TableLayoutPanel
        Me.btnPrintView = New System.Windows.Forms.Button
        Me.btnImortOut = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel
        Me.PNLYC = New System.Windows.Forms.Panel
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.TableLayoutPanel10 = New System.Windows.Forms.TableLayoutPanel
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.cmsDGV = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.HideCurrentLie = New System.Windows.Forms.ToolStripMenuItem
        Me.ShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.saveHideLie = New System.Windows.Forms.ToolStripMenuItem
        Me.Label4 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel13 = New System.Windows.Forms.TableLayoutPanel
        Me.Label9 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel16 = New System.Windows.Forms.TableLayoutPanel
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.TableLayoutPanel17 = New System.Windows.Forms.TableLayoutPanel
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel11.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel12.SuspendLayout()
        Me.TableLayoutPanel14.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        Me.TableLayoutPanel15.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.TableLayoutPanel10.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsDGV.SuspendLayout()
        Me.TableLayoutPanel13.SuspendLayout()
        Me.TableLayoutPanel16.SuspendLayout()
        Me.TableLayoutPanel17.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TableLayoutPanel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.TableLayoutPanel9)
        Me.SplitContainer1.Size = New System.Drawing.Size(1012, 725)
        Me.SplitContainer1.SplitterDistance = 285
        Me.SplitContainer1.SplitterWidth = 3
        Me.SplitContainer1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 244.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(281, 721)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel11, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel4, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel12, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel14, 0, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.btnGaoJi, 0, 5)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 83)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 6
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.42786!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.41791!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.41791!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.41791!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.41791!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9005!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(275, 238)
        Me.TableLayoutPanel3.TabIndex = 5
        '
        'TableLayoutPanel11
        '
        Me.TableLayoutPanel11.ColumnCount = 2
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97.0!))
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 172.0!))
        Me.TableLayoutPanel11.Controls.Add(Me.txtCaseBH, 1, 0)
        Me.TableLayoutPanel11.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel11.Location = New System.Drawing.Point(3, 76)
        Me.TableLayoutPanel11.Name = "TableLayoutPanel11"
        Me.TableLayoutPanel11.RowCount = 1
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel11.Size = New System.Drawing.Size(269, 33)
        Me.TableLayoutPanel11.TabIndex = 26
        '
        'txtCaseBH
        '
        Me.txtCaseBH.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtCaseBH.Location = New System.Drawing.Point(115, 6)
        Me.txtCaseBH.Name = "txtCaseBH"
        Me.txtCaseBH.Size = New System.Drawing.Size(136, 21)
        Me.txtCaseBH.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 33)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "按编号查询："
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 12)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "设定起止日期:"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 3
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.myMaxDate, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.myMinDate, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label6, 1, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 37)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(269, 33)
        Me.TableLayoutPanel4.TabIndex = 24
        '
        'myMaxDate
        '
        Me.myMaxDate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMaxDate.Location = New System.Drawing.Point(149, 6)
        Me.myMaxDate.Name = "myMaxDate"
        Me.myMaxDate.Size = New System.Drawing.Size(115, 21)
        Me.myMaxDate.TabIndex = 23
        '
        'myMinDate
        '
        Me.myMinDate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMinDate.Location = New System.Drawing.Point(4, 6)
        Me.myMinDate.Name = "myMinDate"
        Me.myMinDate.Size = New System.Drawing.Size(115, 21)
        Me.myMinDate.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(127, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 12)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "至"
        '
        'TableLayoutPanel12
        '
        Me.TableLayoutPanel12.ColumnCount = 2
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96.0!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173.0!))
        Me.TableLayoutPanel12.Controls.Add(Me.txtCaseKH, 1, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel12.Location = New System.Drawing.Point(3, 115)
        Me.TableLayoutPanel12.Name = "TableLayoutPanel12"
        Me.TableLayoutPanel12.RowCount = 1
        Me.TableLayoutPanel12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel12.Size = New System.Drawing.Size(269, 33)
        Me.TableLayoutPanel12.TabIndex = 27
        '
        'txtCaseKH
        '
        Me.txtCaseKH.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtCaseKH.Location = New System.Drawing.Point(114, 6)
        Me.txtCaseKH.Name = "txtCaseKH"
        Me.txtCaseKH.Size = New System.Drawing.Size(136, 21)
        Me.txtCaseKH.TabIndex = 22
        Me.ToolTip1.SetToolTip(Me.txtCaseKH, "右击可选择科目")
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 33)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "按客户查询："
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel14
        '
        Me.TableLayoutPanel14.ColumnCount = 2
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96.0!))
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173.0!))
        Me.TableLayoutPanel14.Controls.Add(Me.txtCaseXH, 1, 0)
        Me.TableLayoutPanel14.Controls.Add(Me.Label10, 0, 0)
        Me.TableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel14.Location = New System.Drawing.Point(3, 154)
        Me.TableLayoutPanel14.Name = "TableLayoutPanel14"
        Me.TableLayoutPanel14.RowCount = 1
        Me.TableLayoutPanel14.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel14.Size = New System.Drawing.Size(269, 33)
        Me.TableLayoutPanel14.TabIndex = 27
        '
        'txtCaseXH
        '
        Me.txtCaseXH.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtCaseXH.Location = New System.Drawing.Point(114, 6)
        Me.txtCaseXH.Name = "txtCaseXH"
        Me.txtCaseXH.Size = New System.Drawing.Size(136, 21)
        Me.txtCaseXH.TabIndex = 22
        Me.ToolTip1.SetToolTip(Me.txtCaseXH, "右击可选择科目")
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(16, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 33)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "按型号查询："
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnGaoJi
        '
        Me.btnGaoJi.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnGaoJi.Location = New System.Drawing.Point(39, 200)
        Me.btnGaoJi.Name = "btnGaoJi"
        Me.btnGaoJi.Size = New System.Drawing.Size(197, 27)
        Me.btnGaoJi.TabIndex = 28
        Me.btnGaoJi.Text = "显示高级条件"
        Me.btnGaoJi.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.cbYC, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblName, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(275, 74)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'cbYC
        '
        Me.cbYC.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbYC.AutoSize = True
        Me.cbYC.Checked = True
        Me.cbYC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbYC.Location = New System.Drawing.Point(200, 3)
        Me.cbYC.Name = "cbYC"
        Me.cbYC.Size = New System.Drawing.Size(72, 16)
        Me.cbYC.TabIndex = 3
        Me.cbYC.Text = "总是显示"
        Me.cbYC.UseVisualStyleBackColor = True
        '
        'lblName
        '
        Me.lblName.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblName.AutoSize = True
        Me.TableLayoutPanel2.SetColumnSpan(Me.lblName, 2)
        Me.lblName.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblName.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblName.Location = New System.Drawing.Point(101, 43)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(72, 16)
        Me.lblName.TabIndex = 4
        Me.lblName.Text = "报表名称"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.FlowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control
        Me.FlowLayoutPanel1.Controls.Add(Me.TableLayoutPanel5)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtKJCase)
        Me.FlowLayoutPanel1.Controls.Add(Me.TableLayoutPanel8)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 327)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(275, 391)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.18841!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.81159!))
        Me.TableLayoutPanel5.Controls.Add(Me.TableLayoutPanel7, 0, 5)
        Me.TableLayoutPanel5.Controls.Add(Me.Label5, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.cbXiang, 1, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.Label7, 0, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.Label8, 0, 3)
        Me.TableLayoutPanel5.Controls.Add(Me.cbYSF, 1, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.cbTiaoJian, 1, 3)
        Me.TableLayoutPanel5.Controls.Add(Me.TableLayoutPanel6, 0, 4)
        Me.TableLayoutPanel5.Controls.Add(Me.cbKJXiang, 1, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txtCase, 0, 1)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel5.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 6
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(281, 168)
        Me.TableLayoutPanel5.TabIndex = 7
        Me.TableLayoutPanel5.Visible = False
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 2
        Me.TableLayoutPanel5.SetColumnSpan(Me.TableLayoutPanel7, 2)
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.btnAddCase, 1, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.btnIsert, 0, 0)
        Me.TableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(0, 125)
        Me.TableLayoutPanel7.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 1
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(281, 43)
        Me.TableLayoutPanel7.TabIndex = 6
        '
        'btnAddCase
        '
        Me.btnAddCase.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAddCase.AutoSize = True
        Me.btnAddCase.Location = New System.Drawing.Point(172, 3)
        Me.btnAddCase.Name = "btnAddCase"
        Me.btnAddCase.Size = New System.Drawing.Size(77, 37)
        Me.btnAddCase.TabIndex = 0
        Me.btnAddCase.Text = "增加条件"
        Me.btnAddCase.UseVisualStyleBackColor = True
        '
        'btnIsert
        '
        Me.btnIsert.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnIsert.AutoSize = True
        Me.btnIsert.Location = New System.Drawing.Point(31, 3)
        Me.btnIsert.Name = "btnIsert"
        Me.btnIsert.Size = New System.Drawing.Size(77, 37)
        Me.btnIsert.TabIndex = 0
        Me.btnIsert.Text = "插入""("""
        Me.btnIsert.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 12)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "条件项"
        '
        'cbXiang
        '
        Me.cbXiang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbXiang.FormattingEnabled = True
        Me.cbXiang.Location = New System.Drawing.Point(68, 32)
        Me.cbXiang.Name = "cbXiang"
        Me.cbXiang.Size = New System.Drawing.Size(144, 20)
        Me.cbXiang.TabIndex = 4
        Me.cbXiang.Visible = False
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 12)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "运算符"
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 78)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 12)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "条件值"
        '
        'cbYSF
        '
        Me.cbYSF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbYSF.FormattingEnabled = True
        Me.cbYSF.Items.AddRange(New Object() {"等于", "不等于", "大于", "小于", "不大于", "不小于", "开始于", "不开始于", "包括", "不包括"})
        Me.cbYSF.Location = New System.Drawing.Point(68, 42)
        Me.cbYSF.Name = "cbYSF"
        Me.cbYSF.Size = New System.Drawing.Size(144, 20)
        Me.cbYSF.TabIndex = 4
        '
        'cbTiaoJian
        '
        Me.cbTiaoJian.FormattingEnabled = True
        Me.cbTiaoJian.Location = New System.Drawing.Point(68, 72)
        Me.cbTiaoJian.Name = "cbTiaoJian"
        Me.cbTiaoJian.Size = New System.Drawing.Size(144, 20)
        Me.cbTiaoJian.TabIndex = 4
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel5.SetColumnSpan(Me.TableLayoutPanel6, 2)
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.rbOr, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.rbAnd, 0, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(0, 100)
        Me.TableLayoutPanel6.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(281, 25)
        Me.TableLayoutPanel6.TabIndex = 5
        '
        'rbOr
        '
        Me.rbOr.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rbOr.AutoSize = True
        Me.rbOr.Location = New System.Drawing.Point(187, 4)
        Me.rbOr.Name = "rbOr"
        Me.rbOr.Size = New System.Drawing.Size(47, 16)
        Me.rbOr.TabIndex = 1
        Me.rbOr.Text = "或者"
        Me.rbOr.UseVisualStyleBackColor = True
        '
        'rbAnd
        '
        Me.rbAnd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rbAnd.AutoSize = True
        Me.rbAnd.Checked = True
        Me.rbAnd.Location = New System.Drawing.Point(46, 4)
        Me.rbAnd.Name = "rbAnd"
        Me.rbAnd.Size = New System.Drawing.Size(47, 16)
        Me.rbAnd.TabIndex = 0
        Me.rbAnd.TabStop = True
        Me.rbAnd.Text = "并且"
        Me.rbAnd.UseVisualStyleBackColor = True
        '
        'cbKJXiang
        '
        Me.cbKJXiang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbKJXiang.FormattingEnabled = True
        Me.cbKJXiang.Location = New System.Drawing.Point(68, 3)
        Me.cbKJXiang.Name = "cbKJXiang"
        Me.cbKJXiang.Size = New System.Drawing.Size(144, 20)
        Me.cbKJXiang.TabIndex = 4
        '
        'txtCase
        '
        Me.txtCase.Location = New System.Drawing.Point(3, 32)
        Me.txtCase.Multiline = True
        Me.txtCase.Name = "txtCase"
        Me.txtCase.ReadOnly = True
        Me.txtCase.Size = New System.Drawing.Size(59, 4)
        Me.txtCase.TabIndex = 9
        Me.txtCase.Visible = False
        '
        'txtKJCase
        '
        Me.txtKJCase.Location = New System.Drawing.Point(3, 171)
        Me.txtKJCase.Multiline = True
        Me.txtKJCase.Name = "txtKJCase"
        Me.txtKJCase.ReadOnly = True
        Me.txtKJCase.Size = New System.Drawing.Size(269, 64)
        Me.txtKJCase.TabIndex = 10
        Me.txtKJCase.Visible = False
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.ColumnCount = 2
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.Controls.Add(Me.btnHuiZong, 1, 1)
        Me.TableLayoutPanel8.Controls.Add(Me.btnLieSetup, 0, 1)
        Me.TableLayoutPanel8.Controls.Add(Me.TableLayoutPanel15, 0, 2)
        Me.TableLayoutPanel8.Controls.Add(Me.btnClear, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.btnOK, 1, 0)
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(3, 241)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 4
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(275, 154)
        Me.TableLayoutPanel8.TabIndex = 8
        '
        'btnHuiZong
        '
        Me.btnHuiZong.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnHuiZong.AutoSize = True
        Me.btnHuiZong.Location = New System.Drawing.Point(164, 54)
        Me.btnHuiZong.Name = "btnHuiZong"
        Me.btnHuiZong.Size = New System.Drawing.Size(83, 31)
        Me.btnHuiZong.TabIndex = 0
        Me.btnHuiZong.Text = "分类汇总"
        Me.btnHuiZong.UseVisualStyleBackColor = True
        '
        'btnLieSetup
        '
        Me.btnLieSetup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnLieSetup.AutoSize = True
        Me.btnLieSetup.Location = New System.Drawing.Point(27, 54)
        Me.btnLieSetup.Name = "btnLieSetup"
        Me.btnLieSetup.Size = New System.Drawing.Size(83, 31)
        Me.btnLieSetup.TabIndex = 0
        Me.btnLieSetup.Text = "设置列"
        Me.btnLieSetup.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel15
        '
        Me.TableLayoutPanel15.ColumnCount = 3
        Me.TableLayoutPanel8.SetColumnSpan(Me.TableLayoutPanel15, 2)
        Me.TableLayoutPanel15.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel15.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel15.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94.0!))
        Me.TableLayoutPanel15.Controls.Add(Me.btnPrintView, 1, 0)
        Me.TableLayoutPanel15.Controls.Add(Me.btnImortOut, 0, 0)
        Me.TableLayoutPanel15.Controls.Add(Me.btnPrint, 2, 0)
        Me.TableLayoutPanel15.Location = New System.Drawing.Point(3, 93)
        Me.TableLayoutPanel15.Name = "TableLayoutPanel15"
        Me.TableLayoutPanel15.RowCount = 1
        Me.TableLayoutPanel15.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel15.Size = New System.Drawing.Size(266, 41)
        Me.TableLayoutPanel15.TabIndex = 1
        '
        'btnPrintView
        '
        Me.btnPrintView.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnPrintView.AutoSize = True
        Me.btnPrintView.Location = New System.Drawing.Point(97, 4)
        Me.btnPrintView.Name = "btnPrintView"
        Me.btnPrintView.Size = New System.Drawing.Size(63, 32)
        Me.btnPrintView.TabIndex = 0
        Me.btnPrintView.Text = "预览"
        Me.btnPrintView.UseVisualStyleBackColor = True
        '
        'btnImortOut
        '
        Me.btnImortOut.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnImortOut.AutoSize = True
        Me.btnImortOut.Location = New System.Drawing.Point(11, 4)
        Me.btnImortOut.Name = "btnImortOut"
        Me.btnImortOut.Size = New System.Drawing.Size(63, 32)
        Me.btnImortOut.TabIndex = 0
        Me.btnImortOut.Text = "导出数据"
        Me.btnImortOut.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnPrint.AutoSize = True
        Me.btnPrint.Location = New System.Drawing.Point(187, 4)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(63, 32)
        Me.btnPrint.TabIndex = 0
        Me.btnPrint.Text = "打印"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnClear.AutoSize = True
        Me.btnClear.Location = New System.Drawing.Point(27, 9)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(83, 31)
        Me.btnClear.TabIndex = 0
        Me.btnClear.Text = "清空条件"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnOK.AutoSize = True
        Me.btnOK.Location = New System.Drawing.Point(164, 9)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(83, 31)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "查询"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 2
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5.0!))
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.Controls.Add(Me.PNLYC, 0, 0)
        Me.TableLayoutPanel9.Controls.Add(Me.pnlMain, 1, 0)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 1
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(720, 721)
        Me.TableLayoutPanel9.TabIndex = 0
        '
        'PNLYC
        '
        Me.PNLYC.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.PNLYC.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PNLYC.Location = New System.Drawing.Point(0, 0)
        Me.PNLYC.Margin = New System.Windows.Forms.Padding(0)
        Me.PNLYC.Name = "PNLYC"
        Me.PNLYC.Size = New System.Drawing.Size(5, 721)
        Me.PNLYC.TabIndex = 0
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.TableLayoutPanel10)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(5, 0)
        Me.pnlMain.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(715, 721)
        Me.pnlMain.TabIndex = 1
        '
        'TableLayoutPanel10
        '
        Me.TableLayoutPanel10.ColumnCount = 1
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel10.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel10.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel10.Name = "TableLayoutPanel10"
        Me.TableLayoutPanel10.RowCount = 2
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.854369!))
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95.14563!))
        Me.TableLayoutPanel10.Size = New System.Drawing.Size(715, 721)
        Me.TableLayoutPanel10.TabIndex = 1
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.BackgroundColor = System.Drawing.Color.Lavender
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.ContextMenuStrip = Me.cmsDGV
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(0, 35)
        Me.dgv.Margin = New System.Windows.Forms.Padding(0)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowTemplate.Height = 23
        Me.dgv.Size = New System.Drawing.Size(715, 686)
        Me.dgv.TabIndex = 0
        '
        'cmsDGV
        '
        Me.cmsDGV.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HideCurrentLie, Me.ShowAll, Me.saveHideLie})
        Me.cmsDGV.Name = "cmsDGV"
        Me.cmsDGV.Size = New System.Drawing.Size(149, 70)
        '
        'HideCurrentLie
        '
        Me.HideCurrentLie.Name = "HideCurrentLie"
        Me.HideCurrentLie.Size = New System.Drawing.Size(148, 22)
        Me.HideCurrentLie.Text = "隐藏当前列"
        '
        'ShowAll
        '
        Me.ShowAll.Name = "ShowAll"
        Me.ShowAll.Size = New System.Drawing.Size(148, 22)
        Me.ShowAll.Text = "显示所有列"
        '
        'saveHideLie
        '
        Me.saveHideLie.Name = "saveHideLie"
        Me.saveHideLie.Size = New System.Drawing.Size(148, 22)
        Me.saveHideLie.Text = "保存当前设置"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.Location = New System.Drawing.Point(637, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 19)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Label4"
        '
        'ToolTip1
        '
        Me.ToolTip1.Active = False
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 1
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        '
        'TableLayoutPanel13
        '
        Me.TableLayoutPanel13.ColumnCount = 2
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168.0!))
        Me.TableLayoutPanel13.Controls.Add(Me.Label9, 0, 0)
        Me.TableLayoutPanel13.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel13.Name = "TableLayoutPanel13"
        Me.TableLayoutPanel13.RowCount = 1
        Me.TableLayoutPanel13.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel13.Size = New System.Drawing.Size(200, 100)
        Me.TableLayoutPanel13.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 26)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(23, 48)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "设定科目:"
        '
        'TextBox2
        '
        Me.TextBox2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TextBox2.Location = New System.Drawing.Point(143, 4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(84, 21)
        Me.TextBox2.TabIndex = 22
        '
        'TableLayoutPanel16
        '
        Me.TableLayoutPanel16.ColumnCount = 3
        Me.TableLayoutPanel16.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel16.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel16.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94.0!))
        Me.TableLayoutPanel16.Controls.Add(Me.Button1, 1, 0)
        Me.TableLayoutPanel16.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel16.Name = "TableLayoutPanel16"
        Me.TableLayoutPanel16.RowCount = 1
        Me.TableLayoutPanel16.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel16.Size = New System.Drawing.Size(200, 100)
        Me.TableLayoutPanel16.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button1.AutoSize = True
        Me.Button1.Location = New System.Drawing.Point(60, 39)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(39, 22)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "预览"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button2.AutoSize = True
        Me.Button2.Location = New System.Drawing.Point(3, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(13, 19)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "导出数据"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel17
        '
        Me.TableLayoutPanel17.ColumnCount = 3
        Me.TableLayoutPanel17.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel17.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel17.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94.0!))
        Me.TableLayoutPanel17.Controls.Add(Me.Button3, 1, 0)
        Me.TableLayoutPanel17.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel17.Name = "TableLayoutPanel17"
        Me.TableLayoutPanel17.RowCount = 1
        Me.TableLayoutPanel17.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel17.Size = New System.Drawing.Size(200, 100)
        Me.TableLayoutPanel17.TabIndex = 0
        '
        'Button3
        '
        Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button3.AutoSize = True
        Me.Button3.Location = New System.Drawing.Point(60, 39)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(39, 22)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "预览"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button4.AutoSize = True
        Me.Button4.Location = New System.Drawing.Point(3, 3)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(13, 19)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "导出数据"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'FKBCX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1012, 725)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "FKBCX"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "自定义查询统计"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel11.ResumeLayout(False)
        Me.TableLayoutPanel11.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.TableLayoutPanel12.ResumeLayout(False)
        Me.TableLayoutPanel12.PerformLayout()
        Me.TableLayoutPanel14.ResumeLayout(False)
        Me.TableLayoutPanel14.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel7.PerformLayout()
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel8.PerformLayout()
        Me.TableLayoutPanel15.ResumeLayout(False)
        Me.TableLayoutPanel15.PerformLayout()
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.TableLayoutPanel10.ResumeLayout(False)
        Me.TableLayoutPanel10.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsDGV.ResumeLayout(False)
        Me.TableLayoutPanel13.ResumeLayout(False)
        Me.TableLayoutPanel13.PerformLayout()
        Me.TableLayoutPanel16.ResumeLayout(False)
        Me.TableLayoutPanel16.PerformLayout()
        Me.TableLayoutPanel17.ResumeLayout(False)
        Me.TableLayoutPanel17.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents myMaxDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents myMinDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbXiang As System.Windows.Forms.ComboBox
    Friend WithEvents cbYSF As System.Windows.Forms.ComboBox
    Friend WithEvents cbTiaoJian As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel8 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents txtCase As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel9 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents PNLYC As System.Windows.Forms.Panel
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnLieSetup As System.Windows.Forms.Button
    Friend WithEvents btnImortOut As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel10 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cbYC As System.Windows.Forms.CheckBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel11 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnHuiZong As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbKJXiang As System.Windows.Forms.ComboBox
    Friend WithEvents txtKJCase As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel12 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtCaseKH As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel14 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtCaseXH As System.Windows.Forms.TextBox
    Friend WithEvents btnGaoJi As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel13 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents btnPrintView As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel7 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnAddCase As System.Windows.Forms.Button
    Friend WithEvents btnIsert As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents rbOr As System.Windows.Forms.RadioButton
    Friend WithEvents rbAnd As System.Windows.Forms.RadioButton
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtCaseBH As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel15 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel16 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel17 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmsDGV As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents HideCurrentLie As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents saveHideLie As System.Windows.Forms.ToolStripMenuItem
End Class
