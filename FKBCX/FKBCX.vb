﻿Public Class FKBCX

    Dim sBiao As String '选择的是工资表还是库存表，2015年2月24日增加凭证表查询,
    '2015年4月8日增加从存储过程查询
    ' Public Function ReadStoredProcedure(ByVal ConnectString As String, ByVal spName As String, ByVal sPara As String) As DataSet
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Dim sXSLie As String = "" '选择的要显示的列
    Dim sMRCase As String '默认条件,对应明细查询来说,默认条件可以为空
    Dim sLieSetup As String = ""
    Dim isTongJi As Boolean

    Dim sHideLie As String

    '查询时不能跨年度选择日期
    Dim dsCX As DataSet
    Public sCXName As String
    Private Sub FKZHCX_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon


        Me.myMinDate.Value = "#" & FKG.myselfG.NianFen & "-" & FKG.myselfG.YueFen & "-1#"
        'Me.myMinDate.Value = FKG.myselfG.dQiYongRiQi
        Me.myMaxDate.Value = Today.ToShortDateString

        Me.myMinDate.MinDate = FKG.myselfG.dQiYongRiQi
        Me.myMaxDate.MinDate = FKG.myselfG.dQiYongRiQi

        Me.myMinDate.MaxDate = CType("#" & FKG.myselfG.NianFen + 1 & "-1-1#", Date).AddDays(-1)
        Me.myMaxDate.MaxDate = CType("#" & FKG.myselfG.NianFen + 1 & "-1-1#", Date).AddDays(-1)


        Me.rbAnd.Enabled = False
        Me.rbOr.Enabled = False

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        dsCX = mdb.Reader("select * from ZDYBB WHERE Context='" & sCXName & "'")
        isTongJi = Not dsCX.Tables(0).Rows(0).Item("isMingxi")
        sBiao = dsCX.Tables(0).Rows(0).Item("Biao").ToString
        sXSLie = dsCX.Tables(0).Rows(0).Item("Lie").ToString
        sMRCase = dsCX.Tables(0).Rows(0).Item("MRTiaoJian").ToString

        Try
            sHideLie = dsCX.Tables(0).Rows(0).Item("HideLie").ToString
        Catch ex As Exception

        End Try

        '2012.9.20修改，可自定义三个label
        'Me.Label1.Text = "按编号查询:"
        'Me.Label3.Text = "按客户查询:"
        'Me.Label10.Text = "按型号查询:"

        Dim sZdyLabel As String
        sZdyLabel = dsCX.Tables(0).Rows(0).Item("TiShi").ToString
        If sZdyLabel.IndexOf(",,,") = -1 Then

        Else
            sZdyLabel = Split(sZdyLabel, ",,,").GetValue(1)
            Try
                Me.Label1.Text = Split(sZdyLabel, ",").GetValue(0)
                Me.Label3.Text = Split(sZdyLabel, ",").GetValue(1)
                Me.Label10.Text = Split(sZdyLabel, ",").GetValue(2)
            Catch ex As Exception
                Me.Label1.Text = "按编号查询:"
                Me.Label3.Text = "按客户查询:"
                Me.Label10.Text = "按型号查询:"
            End Try
        End If




        If isTongJi Then
            sXSLie = sXSLie.Replace(Chr(34), "'")
            sMRCase = sMRCase.Replace(Chr(34), "'")
        Else
            sMRCase = sMRCase.Replace(" 等于 ", " = ").Replace(" 不等于 ", " <> ").Replace(" 大于 ", " > ").Replace(" 不大于 ", " <= ").Replace(" 小于 ", " < ").Replace(" 不小于 ", " >= ").Replace(" 开始于 ", " Like ").Replace(" 不开始于 ", " not Like ").Replace(" 包括 ", " Like ").Replace(" 不包括 ", " Not Like ")
            sMRCase = sMRCase.Replace(Chr(34), "'")
        End If

        sLieSetup = dsCX.Tables(0).Rows(0).Item("LieSetup").ToString

        Me.lblName.Text = sCXName
        '2015年4月8日增加从存储过程查询
        ' Public Function ReadStoredProcedure(ByVal ConnectString As String, ByVal spName As String, ByVal sPara As String) As DataSet
        'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值
        If sBiao = "库存表" OrElse sBiao = "工资表" OrElse sBiao = "凭证表" Then
            '按照原定流程执行
            FillXiang(False)
        Else
            '执行按照存储过程查询
            '在Biao字段存储存储过程名称
            'sBiao = "存储过程"
        End If
    End Sub

    Structure XiangType
        Public XiangName As String
        Public XiangType As String
    End Structure

    Dim sXiangType() As XiangType

    Private Function getXiangType(ByVal sName As String) As String
        Dim i As Integer
        For i = 0 To sXiangType.Length - 1
            If sXiangType(i).XiangName = sName Then
                Return sXiangType(i).XiangType
            End If
        Next

        Return "String"
    End Function
    Private Sub FillXiang(ByVal bXSAll As Boolean)
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsLie As DataSet
        dsLie = mdb.Reader("select * from " & sBiao & " where 1<1")
        If IsNothing(dsLie) Then
            Exit Sub
        End If

        Dim sYHKJTJ As String '用户可见条件项
        sYHKJTJ = mdb.Reader("select isnull(YHKJTJ,'') from ZDYBB where Context='" & sCXName & "'").Tables(0).Rows(0).Item(0)

        Array.Resize(sXiangType, dsLie.Tables(0).Columns.Count)
        Dim i As Integer

        Dim sKJTJ() As String
        sKJTJ = Split(sYHKJTJ, ",")

        Dim sKJTJYuanName(sKJTJ.Length - 1) As String

        For i = 0 To sKJTJYuanName.Length - 1
            sKJTJYuanName(i) = Split(sKJTJ(i)).GetValue(0)
        Next

        For i = 0 To dsLie.Tables(0).Columns.Count - 2
            If Array.IndexOf(sKJTJYuanName, dsLie.Tables(0).Columns(i).ColumnName) >= 0 Then
                Me.cbXiang.Items.Add(dsLie.Tables(0).Columns(i).ColumnName)
                If Split(sKJTJ(Array.IndexOf(sKJTJYuanName, dsLie.Tables(0).Columns(i).ColumnName))).Length = 1 OrElse Split(sKJTJ(Array.IndexOf(sKJTJYuanName, dsLie.Tables(0).Columns(i).ColumnName))).GetValue(1) = "" Then
                    Me.cbKJXiang.Items.Add(Split(sKJTJ(Array.IndexOf(sKJTJYuanName, dsLie.Tables(0).Columns(i).ColumnName))).GetValue(0))
                Else
                    Me.cbKJXiang.Items.Add(Split(sKJTJ(Array.IndexOf(sKJTJYuanName, dsLie.Tables(0).Columns(i).ColumnName))).GetValue(1))
                End If

                sXiangType(i).XiangName = dsLie.Tables(0).Columns(i).ColumnName
                sXiangType(i).XiangType = dsLie.Tables(0).Columns(i).DataType.Name
            End If

            'MsgBox(dsLie.Tables(0).Columns(i).DataType.Name)
        Next
        Me.cbKJXiang.Items.Insert(0, "")
        Me.cbXiang.Items.Insert(0, "")


    End Sub

    Private Sub cbXiang_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbXiang.SelectedIndexChanged
        If Me.cbXiang.SelectedIndex <= 0 Then
            Me.cbTiaoJian.Items.Clear()
        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim dsTiaojian As DataSet
            '20160729升级
            If sMRCase = "" Then
                sMRCase = "1=1"
            End If
            dsTiaojian = mdb.Reader("select Distinct " & Me.cbXiang.SelectedItem.ToString & " from " & sBiao & " where 日期>='" & myMinDate.Value.ToString("yyyy-MM-dd") & "' and 日期<='" & Me.myMaxDate.Value.ToString("yyyy-MM-dd") & "' and " & sMRCase)
            '2012.9.20修改，填充值时应满足默认条件

            Me.cbTiaoJian.Items.Clear()
            Me.cbTiaoJian.Text = ""

            Dim i As Integer
            For i = 0 To dsTiaojian.Tables(0).Rows.Count - 1
                Me.cbTiaoJian.Items.Add(dsTiaojian.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub


    Private Sub btnAddCase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddCase.Click
        If Me.cbYSF.SelectedIndex < 0 OrElse Me.cbTiaoJian.Text = "" Then
            Exit Sub
        End If

        Dim sTypeBit As String
        Select Case getXiangType(Me.cbXiang.SelectedItem.ToString)
            Case "String"
                sTypeBit = "'"

            Case "DateTime"
                sTypeBit = "'"

            Case Else
                sTypeBit = ""
        End Select

        'If IsDate(Me.cbTiaoJian.Text) Then
        'ElseIf IsNumeric(Me.cbTiaoJian.Text) Then
        'Else
        'End If

        Select Case Me.cbYSF.SelectedItem
            Case "开始于", "不开始于"
                'sTypeBit = "'"
                If Me.rbAnd.Enabled = False Then
                    Me.txtCase.Text = Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    Else
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    End If

                End If
            Case "包括", "不包括"
                'sTypeBit = "'"
                If Me.rbAnd.Enabled = False Then
                    Me.txtCase.Text = Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    Else
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    End If

                End If
            Case Else
                If Me.rbAnd.Enabled = False Then
                    Me.txtCase.Text = Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                    Else
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                    End If

                End If
        End Select

        Select Case Me.cbYSF.SelectedItem
            Case "开始于", "不开始于"
                'sTypeBit = "'"
                If Me.rbAnd.Enabled = False Then
                    Me.txtKJCase.Text = Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtKJCase.Text = Me.txtKJCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    Else
                        Me.txtKJCase.Text = Me.txtKJCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    End If

                End If
            Case "包括", "不包括"
                'sTypeBit = "'"
                If Me.rbAnd.Enabled = False Then
                    Me.txtKJCase.Text = Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtKJCase.Text = Me.txtKJCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    Else
                        Me.txtKJCase.Text = Me.txtKJCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    End If

                End If
            Case Else
                If Me.rbAnd.Enabled = False Then
                    Me.txtKJCase.Text = Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtKJCase.Text = Me.txtKJCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                    Else
                        Me.txtKJCase.Text = Me.txtKJCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbKJXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                    End If

                End If
        End Select

    End Sub

    Private Sub txtkjCase_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKJCase.TextChanged
        If Me.txtKJCase.Text <> "" Then
            Me.rbAnd.Enabled = True
            Me.rbOr.Enabled = True
        Else
            Me.rbAnd.Enabled = False
            Me.rbOr.Enabled = False
        End If
    End Sub

    Private Sub btnIsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIsert.Click
        If Me.btnIsert.Text = "插入""(""" Then
            Dim iGB As Integer
            iGB = Me.txtKJCase.SelectionStart
            'iGB = Me.txtKJCase.Text.Length

            Me.txtKJCase.Text = Me.txtKJCase.Text.Insert(iGB, "(")
            Me.txtCase.Text = Me.txtCase.Text.Insert(iGB, "(")

            Me.btnIsert.Text = "插入"")"""

        Else
            Dim iGB As Integer
            iGB = Me.txtKJCase.SelectionStart
            'iGB = Me.txtKJCase.Text.Length

            Me.txtKJCase.Text = Me.txtKJCase.Text.Insert(iGB, ")")
            Me.txtCase.Text = Me.txtCase.Text.Insert(iGB, ")")

            Me.btnIsert.Text = "插入""("""

        End If
        Me.txtKJCase.Refresh()
    End Sub

    'Dim sMYBH As String = " true "
    'Dim sMYKM As String = " true "
    'Dim sMYDYKM As String = " true "

    Private Function GetJBCase() As String
        Dim sJBCase As String = ""
        '编号条件
        If Me.txtCaseBH.Text = "" Then
            sJBCase = " 1=1 "
        Else
            sJBCase = " 编号 like '%" & Me.txtCaseBH.Text.ToString & "%' "
        End If
        If sBiao = "库存表" Then
            '客户条件
            If Me.txtCaseKH.Text = "" Then
            Else
                sJBCase = sJBCase & " and (对应科目 like '%" & Me.txtCaseKH.Text.ToString & "%' or 对应科目DM like '%" & Me.txtCaseKH.Text.ToString & "%') "
            End If
            '型号条件
            If Me.txtCaseXH.Text = "" Then
            Else
                sJBCase = sJBCase & " and (科目 like '%" & Me.txtCaseXH.Text.ToString & "%' or 科目DM like '%" & Me.txtCaseXH.Text.ToString & "%') "
            End If
        ElseIf sBiao = "工资表" Then
            '客户条件
            If Me.txtCaseKH.Text = "" Then
            Else
                'sJBCase = sJBCase & " and 科目 like '%" & Me.txtCaseKH.Text.ToString & "%' "
                sJBCase = sJBCase & " and (科目 like '%" & Me.txtCaseKH.Text.ToString & "%' or 科目DM like '%" & Me.txtCaseKH.Text.ToString & "%') "
            End If
            '型号条件
            If Me.txtCaseXH.Text = "" Then
            Else
                '需要在工资表和库存表查询中加入对应科目DM才可以用
                sJBCase = sJBCase & " and (对应科目 like '%" & Me.txtCaseXH.Text.ToString & "%' or 对应科目DM like '%" & Me.txtCaseXH.Text.ToString & "%') "
                'sJBCase = sJBCase & " and (对应科目 like '%" & Me.txtCaseXH.Text.ToString & "%') "
            End If
        ElseIf sBiao = "凭证表" Then
            '客户条件
            If Me.txtCaseKH.Text = "" Then
            Else
                'sJBCase = sJBCase & " and 科目 like '%" & Me.txtCaseKH.Text.ToString & "%' "
                sJBCase = sJBCase & " and (科目 like '%" & Me.txtCaseKH.Text.ToString & "%' or 科目DM like '%" & Me.txtCaseKH.Text.ToString & "%') "
            End If
            '型号条件
            If Me.txtCaseXH.Text = "" Then
            Else
                '需要在工资表和库存表查询中加入对应科目DM才可以用
                sJBCase = sJBCase & " and (对应科目 like '%" & Me.txtCaseXH.Text.ToString & "%' or 对应科目DM like '%" & Me.txtCaseXH.Text.ToString & "%') "
                'sJBCase = sJBCase & " and (对应科目 like '%" & Me.txtCaseXH.Text.ToString & "%') "
            End If
        Else

            Return "1=1"
        End If
        Return sJBCase
    End Function

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        bHuiZong = False

        ' Dim th As New Threading.Thread(AddressOf JinDu)
        'th.Start()

        Me.btnPrintView.Enabled = True
        Me.btnPrint.Enabled = True

        Me.dgv.DataSource = Nothing

        If isTongJi Then
            If TongJi() = False Then
                Exit Sub
            End If
        Else
            If ChaXun("") = False Then
                Exit Sub
            End If
        End If
        Me.dgv.Rows(Me.dgv.RowCount - 1).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.Blue, False)

        '20200426 增加隐藏某些列的功能，可以在后台sql中直接设置，格式为 列索引，中间以 ， 分割
        Try
            Dim sHide() As String = Split(sHideLie, ",")
            Dim sH As String
            For Each sH In sHide
                Me.dgv.Columns(CType(sH, Integer)).Visible = False
            Next
        Catch ex As Exception

        End Try
        'th.Abort()
    End Sub

    Private Function TongJi() As Boolean
        '处理textcase
        Dim sTextCase As String
        sTextCase = Me.txtCase.Text
        If sTextCase = "" Then
            sTextCase = GetJBCase()
        Else
            sTextCase = GetJBCase() & " and " & sTextCase.Replace(" 等于 ", " = ").Replace(" 不等于 ", " <> ").Replace(" 大于 ", " > ").Replace(" 不大于 ", " <= ").Replace(" 小于 ", " < ").Replace(" 不小于 ", " >= ").Replace(" 开始于 ", " Like ").Replace(" 不开始于 ", " not Like ").Replace(" 包括 ", " Like ").Replace(" 不包括 ", " Not Like ")
        End If

        Dim sXSLieC As String

        sXSLieC = sXSLie.Replace("FK_YHDYCASE", " and (" & sTextCase & ") and (日期>='" & Me.myMinDate.Value.ToString("yyyy-MM-dd") & "' and 日期<= '" & Me.myMaxDate.Value.ToString("yyyy-MM-dd") & "')")


        Dim sKMCaseNCYE As String = ""
        If Me.txtCaseXH.Text = "" Then
            'sKMCaseNCYE = " and Nian=" & Me.myMaxDate.Value.Year & " "
        Else
            sKMCaseNCYE = " and (KMMC like '%" & Me.txtCaseXH.Text.ToString & "%' or KMDM like '%" & Me.txtCaseXH.Text.ToString & "%')  "
        End If

        sXSLieC = sXSLieC.Replace("FK_QCQM_QC_YHDYCASE", " and (年份=" & Me.myMinDate.Value.Year & ") and (日期<'" & Me.myMinDate.Value.ToString("yyyy-MM-dd") & "')" & " and (科目 like '%" & Me.txtCaseXH.Text.ToString & "%' or 科目DM like '%" & Me.txtCaseXH.Text.ToString & "%') ")
        sXSLieC = sXSLieC.Replace("FK_QCQM_QC_YHDYNFCASE", " and (年份=" & Me.myMinDate.Value.Year & ") ") '取库存余额时，最小年份的余额
        sXSLieC = sXSLieC.Replace("FK_QCQM_YHDYKMDMNFCASE", Me.myMaxDate.Value.Year & sKMCaseNCYE) '取科目代码时，按照最大年份取值，并且顺便设定要查询的科目
        sXSLieC = sXSLieC.Replace("FK_QCQM_QM_YHDYCASE", " and (年份>=" & Me.myMinDate.Value.Year & ") and (日期<='" & Me.myMaxDate.Value.ToString("yyyy-MM-dd") & "')" & " and (科目 like '%" & Me.txtCaseXH.Text.ToString & "%' or 科目DM like '%" & Me.txtCaseXH.Text.ToString & "%') ")


        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim dsMJKM As DataSet
        dsMJKM = mdb.Reader(sXSLieC)

        If mdb.myExceptionInformation = "" Then
        Else
            MsgBox("请检查括号的位置是否正确！", MsgBoxStyle.Exclamation, "提示")
            Return False
        End If

        dt.Clear()
        dt = dsMJKM.Tables(0)

        Dim dvRow As DataRow
        dvRow = dt.NewRow

        dt.Rows.Add(dvRow)

        Dim iRow, iCol As Integer
        Dim dvHJ As New DataView
        dvHJ = dt.DefaultView

        'dvHJ.RowFilter = " len(科目代码) = " & FKG.myselfG.sKMJS.Substring(0, 1).ToString
        dvHJ.RowFilter = " isMJKM='1'"

        'Me.dgYE.DataSource = dvHJ
        'Exit Sub

        '求一级科目到合计值
        For iCol = 3 To dt.Columns.Count - 1
            Dim dHJ As Decimal = 0
            Select Case dt.Columns(iCol).DataType.Name
                Case "Int32", "Decimal", "Double", "Interger"
                    For iRow = 0 To dvHJ.Count - 1
                        If IsDBNull(dvHJ.Item(iRow).Item(iCol)) Then
                        Else
                            dHJ = dHJ + dvHJ.Item(iRow).Item(iCol)
                        End If
                    Next
                Case "String", "DateTime"

                Case Else
                    For iRow = 0 To dvHJ.Count - 1
                        If IsDBNull(dvHJ.Item(iRow).Item(iCol)) Then
                        Else
                            dHJ = dHJ + dvHJ.Item(iRow).Item(iCol)
                        End If
                    Next
            End Select
            dt.Rows(dt.Rows.Count - 1).Item(iCol) = dHJ
        Next
        dt.Rows(dt.Rows.Count - 1).Item(1) = "合计"
        dt.Rows(dt.Rows.Count - 1).Item(2) = ""

        dt.Columns.Remove("isMJKM")

        Dim dv As DataView
        dv = dt.DefaultView
        dv.RowFilter = ""

        dv.Sort = "科目代码"
        Me.dgv.DataSource = dv


        ''不显示0
        Dim i1, n1 As Int16
        For i1 = 0 To dv.Count - 1
            For n1 = 2 To dt.Columns.Count - 1
                If dt.Columns(n1).DataType.Name = "String" Then
                    Continue For
                End If

                If IsDBNull(dv.Item(i1).Item(n1)) Then
                    Continue For
                End If

                If dv.Item(i1).Item(n1).ToString = "" OrElse dv.Item(i1).Item(n1) = 0 Then
                    dv.Item(i1).Item(n1) = DBNull.Value
                End If
            Next
        Next

        SetupLie()

        Return True
    End Function

    Private Sub JinDu()
        Dim dlg As New FKG.JinDu("正在统计,请稍候...", 0)
        dlg.ShowDialog()
    End Sub

    Dim dt As New DataTable
    Private Function ChaXun(ByVal sCase As String) As Boolean
        '处理textcase
        Dim sTextCase As String
        sTextCase = Me.txtCase.Text

        If sTextCase = "" Then
            sTextCase = GetJBCase()
        Else
            sTextCase = GetJBCase() & " and " & sTextCase.Replace(" 等于 ", " = ").Replace(" 不等于 ", " <> ").Replace(" 大于 ", " > ").Replace(" 不大于 ", " <= ").Replace(" 小于 ", " < ").Replace(" 不小于 ", " >= ").Replace(" 开始于 ", " Like ").Replace(" 不开始于 ", " not Like ").Replace(" 包括 ", " Like ").Replace(" 不包括 ", " Not Like ")

        End If


        Dim dsDGV As DataSet

        '在这里区分是从表中读取数据还是从存储过程中读取数据
        '将存储过程的名称存入MRTiaoJian 列，参数保存在Lie 列中。
        '存储过程中设置5个默认的参数，分别对应五个基本条件
        '@MinDate ，@MaxDate ，@BianHao， @KeMu ，@Xinghao
        '其中两个为日期参数

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If sBiao = "存储过程" Then
            Dim sPara As String = ""
            sPara = "@MinDate," & Me.myMinDate.Value.ToString("yyyy-MM-dd")
            sPara = sPara & ";"
            sPara = sPara & "@MaxDate," & Me.myMaxDate.Value.ToString("yyyy-MM-dd")
            sPara = sPara & ";"
            sPara = sPara & "@BianHao," & Me.txtCaseBH.Text
            sPara = sPara & ";"
            sPara = sPara & "@KeMu," & Me.txtCaseKH.Text
            sPara = sPara & ";"
            sPara = sPara & "@Xinghao," & Me.txtCaseXH.Text()


            If sMRCase.Substring(0, 3) = "ws_" Then
                '存储过程以ws_开头则意味着是从webservice中读取报表
                Dim ws As New wsReporting.reporting
                Dim sLoginInfo As String
                sLoginInfo = FKG.myselfG.YunUserInfo.Item("loginID").ToString
                Try
                    dsDGV = ws.reproting(getToken(FKG.myselfG.YunUserInfo.Item("userID").ToString & FKG.myselfG.YunUserInfo.Item("LoginID").ToString), sLoginInfo, sMRCase.Replace("ws_", ""), sPara)
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    dsDGV = Nothing
                Finally

                End Try
            Else

                'Dim sPara As String = ""
                sPara = "@MinDate," & Me.myMinDate.Value.ToString("yyyy-MM-dd")
                sPara = sPara & ";"
                sPara = sPara & "@MaxDate," & Me.myMaxDate.Value.ToString("yyyy-MM-dd")
                sPara = sPara & ";"
                sPara = sPara & "@BianHao," & Me.txtCaseBH.Text
                sPara = sPara & ";"
                sPara = sPara & "@KeMu," & Me.txtCaseKH.Text
                sPara = sPara & ";"
                sPara = sPara & "@Xinghao," & Me.txtCaseXH.Text()

                If sXSLie = "" Then
                    sPara = sPara
                Else
                    sPara = sXSLie & ";" & sPara
                End If
                Try
                    dsDGV = mdb.ReadStoredProcedure(FKG.myselfG.asasR, sMRCase, sPara)
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "提示")
                    Exit Function
                End Try
            End If
        Else
            If sXSLie = "" Then
                Exit Function
            End If

            Dim sSQL As String = ""

            sSQL = "select " & sXSLie & " from " & sBiao & "  where (" & sMRCase & ") and (日期>='" & Me.myMinDate.Value.ToString("yyyy-MM-dd") & "' and 日期<= '" & Me.myMaxDate.Value.ToString("yyyy-MM-dd") & "') and (" & sTextCase & ") order by " & Split(sXSLie, ",").GetValue(0).ToString
            dsDGV = mdb.Reader(sSQL)

            If mdb.myExceptionInformation = "" Then
            Else
                MsgBox("请检查括号的位置是否正确！", MsgBoxStyle.Exclamation, "提示")
                Exit Function
            End If
        End If

        'Dim dt As DataTable
        dt.Clear()
        dt = dsDGV.Tables(0)

        dt.Rows.Add()

        Dim i As Integer

        If dt.Rows.Count = 1 Then
            '空表,不需要加合计
        Else
            For i = 0 To dt.Columns.Count - 1

                Dim j As Integer
                Dim ihj As Double = 0
                Try
                    For j = 0 To dt.Rows.Count - 2
                        If IsDBNull(dt.Rows(j).Item(i)) Then
                            Continue For
                        End If
                        ihj = ihj + dt.Rows(j).Item(i)
                    Next
                Catch ex As Exception
                    Continue For
                End Try

                Try
                    dt.Rows(dt.Rows.Count - 1).Item(i) = ihj
                Catch ex As Exception

                End Try
            Next
        End If


        Me.dgv.DataSource = dt.DefaultView
        SetupLie()

        Return True
    End Function

    Private Sub SetupLie()
        Dim dlg As New FKXSLie.LieSetup(Me.dgv, True, False, sLieSetup)
        dlg.SetLie(False)
    End Sub

    Private Sub PNLYC_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PNLYC.MouseHover
        If Me.cbYC.Checked Then
        Else
            Me.SplitContainer1.Panel1Collapsed = False
        End If
    End Sub

    Private Sub dgv_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.MouseHover
        If Me.cbYC.Checked Then
        Else
            Me.SplitContainer1.Panel1Collapsed = True
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Me.txtCaseXH.Clear()
        Me.txtCaseKH.Clear()
        Me.txtCaseBH.Clear()

        Me.txtCase.Clear()
        Me.txtKJCase.Clear()

        Me.btnIsert.Text = "插入""("""
    End Sub

    Private Sub btnLieSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLieSetup.Click
        Dim dlg As New FKXSLie.LieSetup(Me.dgv, True, False, "")
        dlg.ShowDialog()

    End Sub

    Private Sub btnImortOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImortOut.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        sGeShiFile = mdb.Reader("select isnull(GeShiFile,'') from ZDYBB where ConText='" & Me.sCXName & "'").Tables(0).Rows(0).Item(0).ToString
        Dim FKExcel As New FKPrn.ImportOut()

        If sGeShiFile = "" Then
            FKExcel.Importout(Me.dgv)
        ElseIf My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\Templates\" & sGeShiFile) = False Then
            FKExcel.Importout(Me.dgv)
        Else
            Dim thPrint As New Threading.Thread(AddressOf ImportOutToFile)
            thPrint.Start()
        End If

    End Sub


    Private Sub getKMDM(ByVal sender As Object)
        Dim dlg As New FKKM.FKXKM("HSLBKC='true'", False)
        dlg.sKey = CType(sender, Windows.Forms.TextBox).Text
        dlg.ShowDialog()
        CType(sender, Windows.Forms.TextBox).Text = FKF.FKF.getKMDMfromQM(dlg.myKM.sKMDM)
    End Sub

    Dim bHuiZong As Boolean = False '表明该报表是否点击了分类汇总，以决定在导出和打印到指定格式报表时是否删除最后一行。如没有经过汇总，则删除；如已汇总，则已经删除合计行，不能再删除。

    Private Sub btnHuiZong_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHuiZong.Click
        '如果当前所得到的查询为空，则不进行任何操作
        If dt.Rows.Count = 1 Then
            Exit Sub
        End If

        bHuiZong = True

        Dim dlg As New FLHZ
        dlg.myDG = Me.dgv
        If dlg.ShowDialog <> Windows.Forms.DialogResult.OK Then
            Exit Sub
        Else
            If dt.Rows.Count = 0 Then
                Exit Sub
            Else
                dt.Rows.Remove(dt.Rows(dt.Rows.Count - 1))
            End If
        End If

        Dim iFLZDGeShu As Integer
        iFLZDGeShu = dlg.clbFLZD.CheckedItems.Count

        Dim dtHuiZong As New DataTable
        Dim i As Integer
        Try
            For i = 0 To iFLZDGeShu - 1
                dtHuiZong.Columns.Add(Me.getNameFromHeaderText(Me.dgv, dlg.clbFLZD.CheckedItems(i)))
            Next

            For i = 0 To dlg.clbHZZD.CheckedItems.Count - 1
                dtHuiZong.Columns.Add(Me.getNameFromHeaderText(Me.dgv, dlg.clbHZZD.CheckedItems(i)), System.Type.GetType("System.Decimal"))
            Next
        Catch ex As Exception
            MsgBox("分类汇总字段设定错误,无法汇总!", MsgBoxStyle.Information, "提示")
            Exit Sub
        End Try


        Dim dv As DataView
        dv = dt.DefaultView

        Dim sFLZD() As String = {""} '分类字段名称
        Array.Resize(sFLZD, iFLZDGeShu)

        Dim sHZZD() As String = {""} '汇总字段名称
        Array.Resize(sHZZD, dlg.clbHZZD.CheckedItems.Count)

        Dim sFLZDZhi() As String = {""} '分类字段值
        Array.Resize(sFLZDZhi, iFLZDGeShu)

        Dim iHuiZongZhi() As Decimal = {0} '汇总字段值
        Array.Resize(iHuiZongZhi, dlg.clbHZZD.CheckedItems.Count)


        Dim dvSort As String = ""
        For i = iFLZDGeShu - 1 To 0 Step -1
            sFLZD(i) = Me.getNameFromHeaderText(Me.dgv, dlg.clbFLZD.CheckedItems(i))
            dvSort = dvSort & "," & sFLZD(i)
        Next
        dv.Sort = dvSort.Remove(0, 1)

        For i = 0 To dlg.clbHZZD.CheckedItems.Count - 1
            sHZZD(i) = Me.getNameFromHeaderText(Me.dgv, dlg.clbHZZD.CheckedItems(i))
        Next


        '当同时对多字段分类汇总时，对dt增加一列汇总项，内容为各汇总项的连接，以此为基础进行汇总，最后再删除此列
        Dim n As Integer
        '        If iFLZDGeShu > 1 Then
        dt.Columns.Add("汇总列标志", Type.GetType("System.String"))
        For i = 0 To dv.Count - 1
            dt.Rows(i).Item("汇总列标志") = ""
            For n = 0 To iFLZDGeShu - 1
                dt.Rows(i).Item("汇总列标志") = dt.Rows(i).Item("汇总列标志") & "    " & dt.Rows(i).Item(sFLZD(n))
            Next
        Next
        'dv.Sort = ""
        dv = dt.DefaultView
        dv.Sort = "汇总列标志"
        'End If

        sFLZDZhi(0) = dv.Item(0).Item("汇总列标志")

        Dim dvRow As DataRow
        For i = 0 To dv.Count - 1

            If dv.Item(i).Item("汇总列标志") = sFLZDZhi(0) Then
                For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
                    Try
                        iHuiZongZhi(n) = iHuiZongZhi(n) + dv.Item(i).Item(sHZZD(n))
                    Catch ex As Exception
                        If dv.Item(i).Item(sHZZD(n)) = "" Then

                        Else
                            iHuiZongZhi(n) = "0"
                        End If
                        Continue For
                    End Try
                Next
            Else
                dvRow = dtHuiZong.NewRow
                For n = 0 To dlg.clbFLZD.CheckedItems.Count - 1
                    dvRow.Item(sFLZD(n)) = dv.Item(i - 1).Item(sFLZD(n))
                Next
                For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
                    dvRow.Item(sHZZD(n)) = iHuiZongZhi(n)
                    iHuiZongZhi(n) = 0
                Next
                dtHuiZong.Rows.Add(dvRow)

                sFLZDZhi(0) = dv.Item(i).Item("汇总列标志")

                For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
                    Try
                        iHuiZongZhi(n) = dv.Item(i).Item(sHZZD(n))
                    Catch ex As Exception
                        iHuiZongZhi(n) = 0
                    End Try
                Next

            End If
        Next

        '增加最后一个
        dvRow = dtHuiZong.NewRow
        For n = 0 To dlg.clbFLZD.CheckedItems.Count - 1
            dvRow.Item(sFLZD(n)) = dv.Item(i - 1).Item(sFLZD(n))
        Next
        For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
            dvRow.Item(sHZZD(n)) = iHuiZongZhi(n)
            iHuiZongZhi(n) = 0
        Next
        dtHuiZong.Rows.Add(dvRow)

        Me.dgv.DataSource = dtHuiZong.DefaultView

        For i = 0 To iFLZDGeShu - 1
            Me.dgv.Columns(i).HeaderText = dlg.clbFLZD.CheckedItems(i)
        Next

        For i = 0 To dlg.clbHZZD.CheckedItems.Count - 1
            Me.dgv.Columns(dlg.clbFLZD.CheckedItems.Count + i).HeaderText = dlg.clbHZZD.CheckedItems(i)
        Next

        '当同时对多字段分类汇总时，对dt增加一列汇总项，内容为各汇总项的连接，以此为基础进行汇总，最后再删除此列

        'sFLZDZhi(0) = dv.Item(0).Item(sFLZD(0))

        'Dim dvRow As DataRow
        'For i = 0 To dv.Count - 1

        '    If dv.Item(i).Item(sFLZD(0)) = sFLZDZhi(0) Then
        '        For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
        '            Try
        '                iHuiZongZhi(n) = iHuiZongZhi(n) + dv.Item(i).Item(sHZZD(n))
        '            Catch ex As Exception
        '                If dv.Item(i).Item(sHZZD(n)) = "" Then

        '                Else
        '                    iHuiZongZhi(n) = "0"
        '                End If
        '                Continue For
        '            End Try
        '        Next
        '    Else
        '        dvRow = dtHuiZong.NewRow
        '        For n = 0 To dlg.clbFLZD.CheckedItems.Count - 1
        '            dvRow.Item(sFLZD(n)) = dv.Item(i - 1).Item(sFLZD(n))
        '        Next
        '        For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
        '            dvRow.Item(sHZZD(n)) = iHuiZongZhi(n)
        '            iHuiZongZhi(n) = 0
        '        Next
        '        dtHuiZong.Rows.Add(dvRow)

        '        sFLZDZhi(0) = dv.Item(i).Item(sFLZD(0))

        '        For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
        '            Try
        '                iHuiZongZhi(n) = dv.Item(i).Item(sHZZD(n))
        '            Catch ex As Exception
        '                iHuiZongZhi(n) = 0
        '            End Try
        '        Next

        '    End If
        'Next

        ''增加最后一个
        'dvRow = dtHuiZong.NewRow
        'For n = 0 To dlg.clbFLZD.CheckedItems.Count - 1
        '    dvRow.Item(sFLZD(n)) = dv.Item(i - 1).Item(sFLZD(n))
        'Next
        'For n = 0 To dlg.clbHZZD.CheckedItems.Count - 1
        '    dvRow.Item(sHZZD(n)) = iHuiZongZhi(n)
        '    iHuiZongZhi(n) = 0
        'Next
        'dtHuiZong.Rows.Add(dvRow)

        'Me.dgv.DataSource = dtHuiZong.DefaultView

        'For i = 0 To iFLZDGeShu - 1
        '    Me.dgv.Columns(i).HeaderText = dlg.clbFLZD.CheckedItems(i)
        'Next

        'For i = 0 To dlg.clbHZZD.CheckedItems.Count - 1
        '    Me.dgv.Columns(dlg.clbFLZD.CheckedItems.Count + i).HeaderText = dlg.clbHZZD.CheckedItems(i)
        'Next
    End Sub

    Private Sub dgv_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged
        Dim ce As System.Windows.Forms.DataGridViewCell
        Dim dHJ As Double = 0
        For Each ce In Me.dgv.SelectedCells
            Try
                If IsDBNull(ce.Value) Then
                    Continue For
                End If
                dHJ = dHJ + CType(ce.Value, Double)
            Catch ex As Exception
                dHJ = 0
            End Try
        Next

        'Me.ToolTip1.Show(dHJ.ToString, sender, 0, 400, 400)
        Me.ToolTip1.Active = True
        Me.ToolTip1.Show(dHJ.ToString, sender)
        Me.Label4.Text = "合计:　" & dHJ.ToString & "　　"

    End Sub

    Private Function getNameFromHeaderText(ByVal objDgv As System.Windows.Forms.DataGridView, ByVal sHeaderText As String) As String
        Dim sName As String = ""
        Dim objColumn As System.Windows.Forms.DataGridViewColumn
        For Each objColumn In objDgv.Columns
            If objColumn.HeaderText = sHeaderText Then
                Return objColumn.Name
            End If
        Next
        Return sName
    End Function

    Public Sub New(ByVal sHash As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        Dim ws As New wsbdsp.Service
        If ws.HelloWorld(getToken(MCO(CPUID, HID))) = "" Then
            Try
                Dim st As New System.Diagnostics.StackTrace
                Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(st.GetFrame(1).GetMethod.Name.ToString), sHash)
                    Case True
                    Case False
                        Me.DialogResult = Windows.Forms.DialogResult.No
                        Me.Close()
                End Select
            Catch ex As Exception
                Me.DialogResult = Windows.Forms.DialogResult.No
                Me.Dispose()
            End Try
        Else
        End If
    End Sub

    Private Function SignatureDeformatter(ByVal p_strKeyPublic As String, ByVal p_strHashbyteDeformatter As String, ByVal p_strDeformatterData As String) As Boolean
        Try
            Dim DeformatterData As Byte()
            Dim HashbyteDeformatter As Byte()

            HashbyteDeformatter = Convert.FromBase64String(p_strHashbyteDeformatter)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPublic)
            Dim RSADeformatter As New System.Security.Cryptography.RSAPKCS1SignatureDeformatter(RSA)
            '指定解密的时候HASH算法为MD5 
            RSADeformatter.SetHashAlgorithm("MD5")

            DeformatterData = Convert.FromBase64String(p_strDeformatterData)

            If RSADeformatter.VerifySignature(HashbyteDeformatter, DeformatterData) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
            'Throw ex
        End Try
    End Function

    Private Function GetHash(ByVal m_strSource As String) As String
        Try
            Dim strHashData As String
            '从字符串中取得Hash描述 
            Dim Buffer As Byte()
            Dim HashData As Byte()
            Dim MD5 As System.Security.Cryptography.HashAlgorithm = System.Security.Cryptography.HashAlgorithm.Create("MD5")
            Buffer = System.Text.Encoding.GetEncoding("GB2312").GetBytes(m_strSource)
            HashData = MD5.ComputeHash(Buffer)

            strHashData = Convert.ToBase64String(HashData)
            Return strHashData
        Catch ex As Exception
            Return ""
        End Try
    End Function

   
    Private Sub cbKJXiang_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbKJXiang.SelectedIndexChanged
        Me.cbXiang.SelectedIndex = Me.cbKJXiang.SelectedIndex
    End Sub

    Private Sub btnGaoJi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGaoJi.Click
        If Me.btnGaoJi.Text = "显示高级条件" Then
            Me.TableLayoutPanel5.Visible = True
            Me.txtKJCase.Visible = True
            Me.btnGaoJi.Text = "隐藏高级条件"
        Else
            Me.TableLayoutPanel5.Visible = False
            Me.txtKJCase.Visible = False
            Me.btnGaoJi.Text = "显示高级条件"
        End If
    End Sub

    Private Sub txtCaseKH_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtCaseKH.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            If sBiao = "库存表" Then
                Dim dlg As New FKKM.FKXKM(" (HSLBGZ='true' OR HSLBWL='true' OR HSLBWW='true') ", False, True)
                dlg.sKey = Me.txtCaseKH.Text
                dlg.ShowDialog()
                Me.txtCaseKH.Text = dlg.myKM.sKMMC
            ElseIf sBiao = "工资表" Then
                Dim dlg As New FKKM.FKXKM(" (HSLBGZ='true' OR HSLBWW='true') ", False, True)
                dlg.sKey = Me.txtCaseKH.Text
                dlg.ShowDialog()
                Me.txtCaseKH.Text = dlg.myKM.sKMMC
            Else
                Dim dlg As New FKKM.FKXKM("1=1", False, True)
                dlg.sKey = Me.txtCaseKH.Text
                dlg.ShowDialog()
                Me.txtCaseKH.Text = dlg.myKM.sKMMC
            End If
        End If
    End Sub

    Private Sub txtCaseXH_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtCaseXH.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim dlg As New FKKM.FKXKM(" HSLBKC='true' ", False, True)
            dlg.sKey = Me.txtCaseXH.Text
            dlg.ShowDialog()
            'If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtCaseXH.Text = dlg.myKM.sKMMC
            'End If
        End If
    End Sub

    Dim sGeShiFile As String = ""
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        sGeShiFile = mdb.Reader("select isnull(GeShiFile,'') from ZDYBB where ConText='" & Me.sCXName & "'").Tables(0).Rows(0).Item(0).ToString
        Dim FKExcel As New FKPrn.ImportOut()
        If sGeShiFile = "" Then
            FKExcel.Importout(Me.dgv)
        ElseIf My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\Templates\" & sGeShiFile) = False Then
            FKExcel.Importout(Me.dgv)
        Else
            Dim thPrint As New Threading.Thread(AddressOf BaoBiaoPrint)
            thPrint.Start()
        End If
    End Sub

    Private Sub BaoBiaoPrint()
        Dim fkP As New FKPrn.Print("报表", 0, 1)

        fkP.sGeShiFile = sGeShiFile
        fkP.dvBaoBiao = Me.dgv.DataSource

        If bHuiZong Then
            fkP.bDeleteLast = False
        Else
            fkP.bDeleteLast = True
        End If

        fkP.Print()
        fkP = Nothing
        System.GC.Collect()
    End Sub

    Private Sub BaoBiaoPrintView()
        Dim fkP As New FKPrn.Print("报表", 0, 1)

        fkP.sGeShiFile = sGeShiFile
        fkP.dvBaoBiao = Me.dgv.DataSource

        If bHuiZong Then
            fkP.bDeleteLast = False
        Else
            fkP.bDeleteLast = True
        End If

        fkP.PrintPreview()
        fkP = Nothing
        System.GC.Collect()
    End Sub

    Private Sub ImportOutToFile()
        Dim fkP As New FKPrn.Print("报表", 0, 1)

        fkP.sGeShiFile = sGeShiFile
        fkP.dvBaoBiao = Me.dgv.DataSource

        If bHuiZong Then
            fkP.bDeleteLast = False
        Else
            fkP.bDeleteLast = True
        End If

        fkP.PrintPreview(True)
        fkP = Nothing
        System.GC.Collect()
    End Sub

    Private Sub btnPrintView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintView.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        sGeShiFile = mdb.Reader("select isnull(GeShiFile,'') from ZDYBB where ConText='" & Me.sCXName & "'").Tables(0).Rows(0).Item(0).ToString
        Dim FKExcel As New FKPrn.ImportOut()
        If sGeShiFile = "" Then
            FKExcel.Importout(Me.dgv)
        ElseIf My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\Templates\" & sGeShiFile) = False Then
            FKExcel.Importout(Me.dgv)
        Else
            Dim thPrint As New Threading.Thread(AddressOf BaoBiaoPrintView)
            thPrint.Start()
        End If
    End Sub

    Private Sub dgv_Sorted(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Sorted
        Me.btnPrint.Enabled = False
        Me.btnPrintView.Enabled = False
    End Sub

    Private Function getToken(ByVal sName As String) As String
        Dim sToken As String = ""
        Dim sT As String = ""
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If Asc(sName.Substring(i, 1)) > 122 Then

            Else
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            End If
        Next

        Dim sTR As String = "A"
        For i = 0 To sT.Length.ToString - 1
            If i Mod 7 = 0 Then
                sTR = sTR & sT.Substring(i, 1)
            End If
        Next

        sToken = sName & "," & sTR
        Return sToken
    End Function

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet
        Dim cpu

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    Private Function HID() As String
        Dim HDid As String = ""
        Try
            Dim cimobject As New Management.ManagementClass("win32_diskdrive")
            Dim moc As Management.ManagementObjectCollection
            moc = cimobject.GetInstances()
            Dim mo As Management.ManagementObject


            For Each mo In moc
                'HDid = HDid & mo.Properties("signature").Value.ToString
                HDid = HDid & mo.Properties("PNPDeviceID").Value.ToString
                HDid = HDid.Replace("_", "")
                HDid = HDid.Remove(0, mo.Properties("Caption").Value.ToString.Length)
                Exit For
            Next
        Catch ex As Exception
            HDid = ""
        End Try

        Return HDid

    End Function

    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 36
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            sCDO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        End If

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
            Next
        End If


        FKG.myselfG.sVersion = My.Application.Info.Version.ToString

        'FKG.myselfG.sBanBen = "试用版"
        FKG.myselfG.sBanBen = "开发版"
        'FKG.myselfG.sBanBen = "正式版"
        'FKG.myselfG.sBanBen = ""

        FKG.myselfG.sClient = "服务器端"
        'FKG.myselfG.sClient = "客户端"

        Return sMCO & FKG.myselfG.sVersion & FKG.myselfG.sBanBen & FKG.myselfG.sClient

    End Function

    Private Sub HideCurrentLie_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HideCurrentLie.Click
        Dim iLie As Integer

        Try
            iLie = Me.dgv.CurrentCell.ColumnIndex
            If sHideLie = "" Then
                sHideLie = iLie.ToString
            Else
                sHideLie = sHideLie & "," & iLie.ToString
            End If

            Me.dgv.Columns(iLie).Visible = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowAll.Click
        Dim iLie As Integer
        For iLie = 0 To Me.dgv.Columns.Count - 1
            Me.dgv.Columns(iLie).Visible = True
        Next
        sHideLie = ""
    End Sub

    Private Sub saveHideLie_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles saveHideLie.Click
        Dim mdbw As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdbw.Write("update zdybb set HideLie='" + sHideLie + "' where ConText='" + sCXName + "'")
    End Sub

    Private Sub cmsDGV_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsDGV.Opening
        Try
            Me.HideCurrentLie.Text = "隐藏当前列_" & Me.dgv.Columns(Me.dgv.CurrentCell.ColumnIndex).HeaderText
        Catch ex As Exception

        End Try
    End Sub
End Class