﻿Imports System.Windows.Forms

Public Class FLHZ

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub FLHZ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FillCLB()
    End Sub

    Private Sub FillCLB()
        'Dim dtv As New DataView
        'dtv = CType(dg.DataSource, DataView)
        'Dim dtt As DataTable
        'dtt = dtv.Table

        'Dim i As Integer
        'For i = 0 To dg.ColumnCount - 1
        '    ''判断字段类型，如果为数值型，则只可以用作汇总项
        '    Select Case dtt.Columns(i).DataType.Name
        '        Case "Int32", "Decimal"
        '            Me.clbHZZD.Items.Add(dg.Columns(i).HeaderText)
        '            Me.clbHZZD.SetItemChecked(Me.clbHZZD.Items.Count - 1, True)
        '        Case Else
        '            Me.clbFLZD.Items.Add(dg.Columns(i).HeaderText)
        '    End Select
        'Next

        Dim i As Integer
        For i = 0 To dg.ColumnCount - 1
            Me.clbHZZD.Items.Add(dg.Columns(i).HeaderText)
            Me.clbFLZD.Items.Add(dg.Columns(i).HeaderText)
        Next
    End Sub

    Private dg As DataGridView
    Public WriteOnly Property myDG() As DataGridView
        Set(ByVal value As DataGridView)
            dg = value
        End Set
    End Property


    'Private Sub clbFLZD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbFLZD.SelectedIndexChanged
    '    Me.clbHZZD.Items(Me.clbFLZD.SelectedIndex)

    'End Sub

    Private Sub btnAllSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllSelect.Click
        Dim i As Integer
        For i = 0 To Me.clbHZZD.Items.Count - 1
            Me.clbHZZD.SetItemChecked(i, True)
        Next
    End Sub

    Private Sub btnAllCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllCancel.Click
        Dim i As Integer
        For i = 0 To Me.clbHZZD.Items.Count - 1
            Me.clbHZZD.SetItemChecked(i, False)
        Next
    End Sub
End Class
