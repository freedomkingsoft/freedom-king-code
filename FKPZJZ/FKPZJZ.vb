﻿Imports System.Windows.Forms

Public Class FKKCJZ

    Private Sub FKKCJZ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case FKG.myselfG.sBanBen
            Case "开发版"
            Case "正式版"
            Case Else
                MsgBox("试用版不提供期末记账功能！", MsgBoxStyle.Information, "提示")
                Me.Close()
        End Select
        Me.Icon = FKG.myselfG.FKIcon
        FillDG()
        'Me.txtNianfen.Text = FKG.myselfG.NianFen
        'ijzyue = FKG.myselfG.YueFen
        'If FKG.myselfG.NianFen = FKG.myselfG.dQiYongRiQi.Year Then
        '    Me.txtYueFen.Minimum = FKG.myselfG.dQiYongRiQi.Month
        'End If
    End Sub

    Private Function FillDG() As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgKCJZQJ, "select NY as 年月,ksrq as 起始日期,zzrq as 截止日期,JiZhang AS 记账 from FKZQ WHERE NY LIKE '" & FKG.myselfG.NianFen & "%'")
    End Function

    Private Sub btnKCJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCJZ.Click
        '将整个过程写入TRY块中,出现异常时自动执行反记账.
        Dim mdbopen As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Try

            '首先判断是否存在读写冲突
            Dim iT As Integer = 0
            Do While FKG.myselfG.bCanBackup = False
                System.Threading.Thread.Sleep(50)
                iT = iT + 1

                If iT >= 150 Then
                    MsgBox("数据库正在使用当中，没有完成记账！")
                    Exit Sub
                End If
            Loop

            Dim iJZYue As Integer
            For iJZYue = 1 To 12
                If Me.dgKCJZQJ.Rows(iJZYue - 1).Cells(3).Value = False Then
                    Exit For
                End If
            Next
            If iJZYue = 13 Then
                MsgBox("本年度所有月份均已记账！", MsgBoxStyle.Information, "信息")
                Exit Sub
            End If
            '禁用自动定时备份
            FKG.myselfG.bCanBackup = False

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            If mdb.bExsit("select * from FKZQ where NY<'" & FKG.myselfG.NianFen & iJZYue.ToString.PadLeft(2, "0") & "' and JiZhang='false'") Then
                MsgBox("你选定的月份以前还有没有记帐挡月份，无法记账，请先将以前月份记帐！", MsgBoxStyle.Information, "提示")
                '启用自动定时备份
                FKG.myselfG.bCanBackup = True
                Exit Sub
            End If

            Dim dsSH As DataSet
            dsSH = mdb.Reader("select 凭证号 from PZ WHERE 审核='' and 年份=" & FKG.myselfG.NianFen & " and 月份=" & iJZYue)

            'If mdb.bExsit("select 凭证号 from PZ WHERE 审核='' and 年份=" & FKG.myselfG.NianFen & " and 月份=" & ijzyue) = True Then
            If dsSH.Tables(0).Rows.Count > 0 Then
                MsgBox("你选定的月份还有凭证没有审核，无法记账，请先审核所有凭证！", MsgBoxStyle.Information, dsSH.Tables(0).Rows(0).Item(0).ToString)
                '启用自动定时备份
                FKG.myselfG.bCanBackup = True
                Exit Sub
            End If

            Dim objTh As New Threading.Thread(AddressOf JinDu)
            'objTh.Start()

            Dim iNY As String
            iNY = FKG.myselfG.NianFen.ToString & iJZYue.ToString.PadLeft(2, "0")
            Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

            If iyue <= FKG.myselfG.YueFen Then
                Try
                    mdbopen.oleConnect.Open()
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
                    mdbopen.oleConnect.Close()
                    '启用自动定时备份
                    FKG.myselfG.bCanBackup = True
                    Exit Sub
                End Try

                '可以进行记账
                mdbopen.oleCommand.CommandText = "update FKZQ SET JIZHANG='true' WHERE NY='" & iNY & "'"
                mdbopen.oleCommand.ExecuteNonQuery()

                '生成库存余额表
                Dim dsKCYE As New DataSet
                mdbopen.oleCommand.CommandText = "select 科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目"
                mdbopen.oleDataAdapter.Fill(dsKCYE)

                Dim i As Integer
                For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                    Dim dsbExsit As New DataSet
                    mdbopen.oleCommand.CommandText = "select KMDM from KMYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and  Nian=" & FKG.myselfG.NianFen
                    mdbopen.oleDataAdapter.Fill(dsbExsit)
                    If dsbExsit.Tables(0).Rows.Count > 0 Then
                        'update
                        mdbopen.oleCommand.CommandText = "update KMYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(1) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and  Nian=" & FKG.myselfG.NianFen
                        mdbopen.oleCommand.ExecuteNonQuery()
                    Else
                        'insert
                        mdbopen.oleCommand.CommandText = "insert into KMYE (kmdm,nian,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & ")"
                        mdbopen.oleCommand.ExecuteNonQuery()
                    End If
                Next

                '写入非末级科目的余额
                dsKCYE.Clear()
                dsKCYE = New DataSet

                mdbopen.oleCommand.CommandText = "select KMDM,sum(借数),sum(借金),sum(贷数),sum(贷金) from (select KMDM, TYE.* from KMDM,(select 科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目) as TYE where  KMDM.NIAN= " & FKG.myselfG.NianFen & " AND KMDM.SFMJ='false' AND (charindex(TYE.科目,kmdm.kmdm)=1 )) as tttt group by KMDM"
                mdbopen.oleDataAdapter.Fill(dsKCYE)

                'Dim i As Integer
                For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                    Dim dsbExsit2 As New DataSet
                    mdbopen.oleCommand.CommandText = "select KMDM from KMYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and  Nian=" & FKG.myselfG.NianFen
                    mdbopen.oleDataAdapter.Fill(dsbExsit2)

                    If dsbExsit2.Tables(0).Rows.Count > 0 Then
                        'update
                        mdbopen.oleCommand.CommandText = "update KMYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(1) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "' and  Nian=" & FKG.myselfG.NianFen
                        mdbopen.oleCommand.ExecuteNonQuery()

                    Else
                        'insert
                        mdbopen.oleCommand.CommandText = "insert into KMYE (kmdm,nian,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & ")"
                        mdbopen.oleCommand.ExecuteNonQuery()
                    End If
                Next
                mdbopen.oleCommand.CommandText = "update JiBenCaozuo set 记帐='" & FKG.myselfG.YongHu & "' where nian=" & FKG.myselfG.NianFen & " and yue=" & iyue
                mdbopen.oleCommand.ExecuteNonQuery()

                mdbopen.oleCommand.CommandText = "update PZ set 记帐='" & FKG.myselfG.YongHu & "' where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue
                mdbopen.oleCommand.ExecuteNonQuery()

                mdbopen.oleConnect.Close()

                FillDG()

                '启用自动定时备份
                FKG.myselfG.bCanBackup = True
            Else
                '启用自动定时备份
                FKG.myselfG.bCanBackup = True
                objTh.Abort()
                Exit Sub
            End If
            objTh.Abort()
            '启用自动定时备份
            FKG.myselfG.bCanBackup = True


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
            MsgBox("因出现异常,本次记账未成功,请重新记账!", MsgBoxStyle.Information, "提示")

            btnKCFJZ_Click(sender, e)
        Finally
            mdbopen.oleConnect.Close()
        End Try
    End Sub

    Private Sub JinDu()
        Dim dlgTS As New FKG.JinDu("正在记账，请稍候......", 1)
        dlgTS.ShowDialog()
    End Sub

    Private Sub btnKCFJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCFJZ.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim iNY As String

        Dim iJZYue As Integer
        For iJZYue = 12 To 1 Step -1
            If Me.dgKCJZQJ.Rows(iJZYue - 1).Cells(3).Value = True Then
                Exit For
            End If
        Next
        If iJZYue = 0 Then
            MsgBox("未发现已记账月份，无需进行反记账！", MsgBoxStyle.Information, "信息")
            Exit Sub
        End If

        iNY = FKG.myselfG.NianFen.ToString & ijzyue.ToString.PadLeft(2, "0")
        Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

        If mdb.Reader("select JieZhang from FKZQ where  NY='" & iNY & "'").Tables(0).Rows(0).Item(0) = True Then
            MsgBox("你选定的月份已经结账,请先反结账,然后再进行反记帐！", MsgBoxStyle.Information, "提示")
            Exit Sub

        End If

        mdb.Write("update KMYE set jfsl" & iyue & "=0,jfje" & iyue & "=0,dfsl" & iyue & "=0,dfje" & iyue & "=0 where  Nian=" & FKG.myselfG.NianFen)
        mdb.Write("update FKZQ SET JIZHANG='false' WHERE NY='" & iNY & "'")

        mdb.Write("update JiBenCaozuo set 记帐='' where nian=" & FKG.myselfG.NianFen & " and yue=" & iyue)

        mdb.Write("update PZ set 记帐='' where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue)

        FillDG()

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnHeBing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHeBing.Click
        'Dim dlg As New FKDRData
        'dlg.ShowDialog()
        Dim dlg As New FKVerifyData
        dlg.ShowDialog()

    End Sub
End Class
