Public Class FKVerifyData

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.pb1.MarqueeAnimationSpeed = 100

        Me.Button1.Enabled = False
        Dim sDone As String = "检查完成。"
        Dim ds As DataSet
        Dim i As Integer

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sSql As String
        '分年度检查借贷方发生额是否平衡
        sSql = "select 年份,sum(借方金额)-sum(贷方金额) as 差额 from pz group by 年份 having sum(借方金额)-sum(贷方金额)<>0"
        If mdb.Reader(sSql).Tables(0).Rows.Count > 0 Then
            '分月份检查借贷方发生额是否平衡
            sSql = "select 年份,月份,sum(借方金额)-sum(贷方金额) as 差额 from pz group by 年份,月份 having sum(借方金额)-sum(贷方金额)<>0"
            ds = mdb.Reader(sSql)
            sDone = sDone & "以下月份借贷方发生额不平衡，请检查。"
            For i = 0 To ds.Tables(0).Rows.Count - 1
                sDone = sDone & ds.Tables(0).Rows(i).Item(0).ToString & "年" & ds.Tables(0).Rows(i).Item(1).ToString & "月。"
            Next
        End If

        '分编号检查借贷方发生额是否平衡
        sSql = "select 编号,sum(借方金额)-sum(贷方金额) as 差额 from pz group by 编号 having sum(借方金额)-sum(贷方金额)<>0"
        ds = mdb.Reader(sSql)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            sDone = sDone & "编号：" & ds.Tables(0).Rows(i).Item(0).ToString & "借贷方发生额不平衡，请检查。"
        Next
        '检查科目和对应科目是否为空
        sSql = "select distinct 编号  from PZ where 科目='' or 对应科目=''"
        ds = mdb.Reader(sSql)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            sDone = sDone & "编号：" & ds.Tables(0).Rows(i).Item(0).ToString & "有科目为空，请检查。"
        Next

        If sDone = "检查完成。" Then
            sDone = sDone & " 你的数据正常。"
        Else
            sDone = sDone & Chr(10)
            sDone = sDone & "解决办法：一、将出现问题的编号记录下来。二、删除该编号内容。三重新检查数据。四、重新录入删除的内容。"
            sDone = sDone & Chr(10)
            sDone = sDone & "如无法解决，请联系售后服务人员。"
        End If

        Me.RichTextBox2.Text = sDone
        Me.pb1.MarqueeAnimationSpeed = 0
    End Sub

    Private Sub FKVerifyData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.pb1.MarqueeAnimationSpeed = 0
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.TextBox1.Text = "" Then
            Exit Sub
        End If

        Dim sBianhaoDelete As String
        If Me.TextBox1.Text = Me.TextBox2.Text Then
            sBianhaoDelete = Me.TextBox2.Text
            '检查单据是否已审核，是否已对账。
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim ds As DataSet
            ds = mdb.Reader("select * from jibencaozuo where bianhao ='" & sBianhaoDelete & "' and 审核<>'' and dzhtime<>''")
            If ds.Tables(0).Rows.Count > 0 Then
                MsgBox("你打算删除的单据已审核或者已对账，请先进行消审或者删除对账记录操作。", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If

            If MsgBox("请确认你要删除编号为" & sBianhaoDelete & " 的单据，删除后无法恢复。", MsgBoxStyle.YesNo, "请确认") = MsgBoxResult.Yes Then
                '执行删除操作
                Try
                    mdb.Write("update jibencaozuo set 删除=1,删除时间='" & Now.ToShortTimeString & "',删除人='异常单据' where bianhao='" & sBianhaoDelete & "'")
                    mdb.Write("delete from kucun where 编号='" & sBianhaoDelete & "'")
                    mdb.Write("delete from gongzi where 编号='" & sBianhaoDelete & "'")
                    mdb.Write("delete from pz where 编号='" & sBianhaoDelete & "'")
                Catch ex As Exception
                    MsgBox("出现异常，删除失败。错误信息：" & ex.ToString)
                    Exit Sub
                End Try

                MsgBox("编号：" & sBianhaoDelete & " 已成功删除。", MsgBoxStyle.Information, "提示")
            End If
        Else
            MsgBox("两次录入的编号不一致，请重新录入。", MsgBoxStyle.Information, "提示")
            Me.TextBox2.Text = ""
            Me.TextBox1.Text = ""
            Exit Sub
        End If
    End Sub
End Class