﻿Public Class FKDRData

    Private Sub FKDRData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case FKG.myselfG.sBanBen
            Case "开发版"
            Case "正式版"
            Case Else
                MsgBox("试用版不提供此功能！", MsgBoxStyle.Information, "提示")
                Me.Close()
        End Select
        Me.Icon = FKG.myselfG.FKIcon


        Me.dtpMonth.MinDate = FKG.myselfG.dQiYongRiQi
        Me.dtpMonth.MaxDate = Today

    End Sub

    Private Sub fillCBGongNeng()
        Dim mdb2 As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR.Replace(mdb2.oleConnect.DataSource, Me.TextBox1.Text))
        mdb.DataBind(Me.cbGongNeng, "select distinct context from gongnengshu,jibencaozuo where gongnengshu.id=jibencaozuo.A_gnsid and jibencaozuo.删除='false'")

        Me.cbGongNeng.Items.Insert(0, "所有功能")

        Me.cbGongNeng.SelectedIndex = 0

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.TextBox1.Text = Me.OpenFileDialog1.FileName

            Dim mdb2 As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If Me.TextBox1.Text = mdb2.oleConnect.DataSource Then
                Me.TextBox1.Text = ""
            End If

            If Me.TextBox1.Text = "" Then
                Exit Sub
            End If

            fillCBGongNeng()

        End If


    End Sub

    Private Sub btnDaoRu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoRu.Click
        If Me.TextBox1.Text = "" Then
            Exit Sub
        End If

        'Dim mdbSourse As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        If Me.cbGongNeng.SelectedItem.ToString = "所有功能" Then
            Try
                '复制数据()
                If mdb.Write("INSERT INTO pz select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].pz where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_JBID not in (select A_JBID from pz  where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & ")") AndAlso mdb.Write("INSERT INTO GongZi select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].GongZi where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_JBID not in (select A_JBID from GongZi  where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & ")") AndAlso mdb.Write("INSERT INTO KuCun select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].KuCun where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_JBID not in (select A_JBID from KuCun  where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & ")") AndAlso mdb.Write("INSERT INTO JiBenCaoZuo select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].JiBenCaoZuo where nian=" & Me.dtpMonth.Value.Year & " and yue=" & Me.dtpMonth.Value.Month & " and A_ID not in (select A_ID from JiBenCaoZuo  where nian=" & Me.dtpMonth.Value.Year & " and yue=" & Me.dtpMonth.Value.Month & ")") Then
                Else
                    MsgBox("数据导入没有成功!", MsgBoxStyle.Information, "严重警告!")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
                Exit Sub
            End Try

        Else
            Dim iGongNengID As Integer
            iGongNengID = mdb.Reader("select ID from gongnengshu where Context='" & Me.cbGongNeng.SelectedItem.ToString & "'").Tables(0).Rows(0).Item(0)

            Try
                '复制数据()
                If mdb.Write("INSERT INTO pz select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].pz where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & " and A_JBID not in (select A_JBID from pz  where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & ")") AndAlso mdb.Write("INSERT INTO GongZi select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].GongZi where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & " and A_JBID not in (select A_JBID from GongZi  where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & ")") AndAlso mdb.Write("INSERT INTO KuCun select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].KuCun where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & " and A_JBID not in (select A_JBID from KuCun  where 年份=" & Me.dtpMonth.Value.Year & " and 月份=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & ")") AndAlso mdb.Write("INSERT INTO JiBenCaoZuo select * from [';database=" & Me.TextBox1.Text & ";pwd=Zh!o04#Dn$3Tng09#'].JiBenCaoZuo where nian=" & Me.dtpMonth.Value.Year & " and yue=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & " and A_ID not in (select A_ID from JiBenCaoZuo  where nian=" & Me.dtpMonth.Value.Year & " and yue=" & Me.dtpMonth.Value.Month & " and A_GNSID=" & iGongNengID & ")") Then
                Else
                    MsgBox("数据导入没有成功!", MsgBoxStyle.Information, "严重警告!")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
                Exit Sub
            End Try

        End If

        MsgBox("数据导入成功，请检查是否需要进项重新记账等操作！", MsgBoxStyle.Information, "导入成功，重新记账")
    End Sub
End Class