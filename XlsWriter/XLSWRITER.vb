﻿Imports Microsoft.Office.Interop

Public Class XLSWRITER

    Dim eApptype As Type = Type.GetTypeFromProgID("Excel.Application")

    Dim eApp As Object = Activator.CreateInstance(eApptype)

    Dim eBook As Excel.Workbook
    Dim eSheet As Excel.Worksheet

    Private Sub openExcel(ByVal myExcelFileName As String)
        If myExcelFileName = "" Then
            eApp.Workbooks.Add()
        Else
            eApp.Workbooks.Open(myExcelFileName, , False)
        End If
        eBook = eApp.ActiveWorkbook
        eSheet = eBook.Sheets(1)
    End Sub

    Public Sub closeExcel()

        eBook.Close(False)
        eApp.Quit()

        System.Runtime.InteropServices.Marshal.ReleaseComObject(eSheet)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eBook)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eApp)

        eSheet = Nothing
        eBook = Nothing
        eApp = Nothing

        System.GC.Collect()
    End Sub

    Public Sub xlsReplace(ByVal sFileName As String, ByVal sOldString As String, ByVal sNewString As String)
        Try
            openExcel(sFileName)

            eApp.Visible = True

            eSheet.Cells.Replace(sOldString, sNewString)

            eBook.Save()

        Catch ex As Exception
        Finally
            closeExcel()
        End Try

    End Sub

End Class
