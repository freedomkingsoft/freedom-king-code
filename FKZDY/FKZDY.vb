﻿Public Class FKZDY

    Dim sFilePath As String = ""

    Private Sub FKZDY_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.bExsit("select * from DTZT where userid=" & FKG.myselfG.YongHuID) = False Then
            mdb.Write("insert into DTZT (USERID,GNDT,BBDT,MCDT,GNZT,BBZT,MCZT,MMZT,MCLIE,MCHANG) values (" & FKG.myselfG.YongHuID & ",'','','','','','','',0,0)")
        End If

        FillSet()
        Me.Icon = FKG.myselfG.FKIcon

        Dim xml As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKConfig.xml", True, "config")
        If xml.Read("HideTree", "name", "config") = "True" Then
            Me.cbHide.Checked = True
        Else
            Me.cbHide.Checked = False
        End If

        sFilePath = FKG.myselfG.getSetupStr("PicturePath")
    End Sub

    Private Sub FillSet()
        Dim MDB As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        'sFilePath = MDB.oleConnect.DataSource
        'sFilePath = sFilePath.ToLower.Replace("\freedomkingdata.mdb", "")

        Dim DS As DataSet
        DS = MDB.Reader("select * from DTZT where userid=" & FKG.myselfG.YongHuID)
        Try
            Me.NumericUpDown1.Value = DS.Tables(0).Rows(0).Item("MCHANG")
            Me.NumericUpDown2.Value = DS.Tables(0).Rows(0).Item("MCLIE")

            Me.cbMCDT.Text = DS.Tables(0).Rows(0).Item("MCDT")


            Me.cbGNSDT.Text = DS.Tables(0).Rows(0).Item("GNDT")
            Me.cbGNSZT.Text = DS.Tables(0).Rows(0).Item("GNZT")

            Me.cbBBSDT.Text = DS.Tables(0).Rows(0).Item("bbDT")
            Me.cbBBSZT.Text = DS.Tables(0).Rows(0).Item("bbZT")

            Me.cbCDZT.Text = DS.Tables(0).Rows(0).Item("MMZT")

            Me.tlpMain.Refresh()


            Me.tlpMain.BackgroundImage = Drawing.Image.FromFile(sFilePath & Me.cbMCDT.Text)

            Me.tvGNS.BackGroundImage = Drawing.Image.FromFile(sFilePath & DS.Tables(0).Rows(0).Item("GNDT"))
            Me.tvGNS.Font = StringToFont(DS.Tables(0).Rows(0).Item("GNZT"))

            Me.tvBBS.BackGroundImage = Drawing.Image.FromFile(sFilePath & DS.Tables(0).Rows(0).Item("bbDT"))
            Me.tvBBS.Font = StringToFont(DS.Tables(0).Rows(0).Item("bbZT"))

            Me.MenuStrip1.Font = StringToFont(DS.Tables(0).Rows(0).Item("MMZT"))
            Me.ToolStripStatusLabel1.Font = StringToFont(DS.Tables(0).Rows(0).Item("MMZT"))
            sGaoKuan = DS.Tables(0).Rows(0).Item("MCZT")
        Catch ex As Exception

        End Try

        DS.Clear()

        DS = MDB.Reader("select btntype,btnname,gnbbid,btntext,btndt,btnzt,irow,icolumn,btnsize,btnalign,tab from MCBTN WHERE userid=" & FKG.myselfG.YongHuID)
        If DS.Tables(0).Rows.Count = 0 Then
            Exit Sub
        End If

        Me.DataGridView1.Rows.Add(DS.Tables(0).Rows.Count)

        Dim i As Integer
        For i = 0 To DS.Tables(0).Rows.Count - 1
            Me.DataGridView1.Rows(i).Cells("命令来源").Value = DS.Tables(0).Rows(i).Item("btntype")
            Me.DataGridView1.Rows(i).Cells("命令名称").Value = DS.Tables(0).Rows(i).Item("btnname")
            Me.DataGridView1.Rows(i).Cells("GNID").Value = DS.Tables(0).Rows(i).Item("gnbbid")
            Me.DataGridView1.Rows(i).Cells("显示文本").Value = DS.Tables(0).Rows(i).Item("btntext")
            Me.DataGridView1.Rows(i).Cells("按钮底图").Value = DS.Tables(0).Rows(i).Item("btndt")
            Me.DataGridView1.Rows(i).Cells("字体").Value = DS.Tables(0).Rows(i).Item("btnzt")
            Me.DataGridView1.Rows(i).Cells("所在行").Value = DS.Tables(0).Rows(i).Item("irow")
            Me.DataGridView1.Rows(i).Cells("所在列").Value = DS.Tables(0).Rows(i).Item("icolumn")
            Me.DataGridView1.Rows(i).Cells("按钮大小").Value = DS.Tables(0).Rows(i).Item("btnsize")
            Me.DataGridView1.Rows(i).Cells("文字位置").Value = DS.Tables(0).Rows(i).Item("btnalign")
            Me.DataGridView1.Rows(i).Cells("附加信息").Value = DS.Tables(0).Rows(i).Item("tab")
        Next

        Me.Refresh()
        'MDB.DataBind(Me.DataGridView1, "select btntype AS 命令来源, btnname as 命令名称,gnbbid,btntext as 显示文本,btndt as 按钮底图,btnzt as 字体,irow  as 所在行,icolumn as 所在列,btnsize as 按钮大小,btnalign as 文字位置 from MCBTN WHERE userid=" & FKG.myselfG.YongHuID)
    End Sub

    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        Me.tlpMain.RowCount = Me.NumericUpDown1.Value

        Me.tlpMain.Refresh()
        sGaoKuan = ""
    End Sub

    Private Sub NumericUpDown2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown2.ValueChanged
        Me.tlpMain.ColumnCount = Me.NumericUpDown2.Value
        Me.tlpMain.Refresh()
        sGaoKuan = ""
    End Sub

    Private Sub ComboBox1_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbMCDT.DropDown
        Dim dlg As New Windows.Forms.OpenFileDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                System.IO.File.Copy(dlg.FileName, sFilePath & "\Pictrue\" & IO.Path.GetFileName(dlg.FileName), True)
            Catch ex As Exception

            End Try

            Me.cbMCDT.Text = "\Pictrue\" & IO.Path.GetFileName(dlg.FileName)
            Me.tlpMain.BackgroundImageLayout = Windows.Forms.ImageLayout.Stretch
            Me.tlpMain.BackgroundImage = Drawing.Image.FromFile(sFilePath & Me.cbMCDT.Text)
        Else
            Me.cbMCDT.Text = ""
        End If
    End Sub

    Dim btn() As System.Windows.Forms.Label

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, BtnSave.Click
        Dim i As Integer
        '设定行高和列宽
        If IsNothing(sGaoKuan) OrElse sGaoKuan = "" Then
        Else
            Dim sGao, sKuan As String
            sGao = Split(sGaoKuan, ",").GetValue(0)
            sKuan = Split(sGaoKuan, ",").GetValue(1)

            For i = 0 To Me.tlpMain.RowCount - 1
                Me.tlpMain.RowStyles(i).Height = CInt(Split(sGao).GetValue(i)) * Me.tlpMain.Height / 100
            Next
            For i = 0 To Me.tlpMain.ColumnCount - 1
                Me.tlpMain.ColumnStyles(i).Width = CInt(Split(sKuan).GetValue(i)) * Me.tlpMain.Width / 100
            Next

        End If
        Dim mdbCD As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        'Dim aass As String
        'aass = FKG.myselfG.asasW.Replace(mdb.oleConnect.DataSource, sfilepath &  "\sys.dat")
        'Dim mdbCD As New MDBReadWrite.MDBReadWrite(aass)

        Me.tlpMain.Controls.Clear()
        'If CType(sender, System.Windows.Forms.Button).Name = "Button1" Then

        'End If
        If CType(sender, System.Windows.Forms.Button).Name = "Button1" Then
        Else
            'mdbCD.Write("delete from dtzt where userid=" & FKG.myselfG.YongHuID)
            mdbCD.Write("delete from MCBTN where UserID=" & FKG.myselfG.YongHuID)
        End If

        Array.Resize(btn, Me.DataGridView1.RowCount - 1)
        For i = 0 To Me.DataGridView1.RowCount - 2
            btn(i) = New System.Windows.Forms.Label
            btn(i).AutoSize = False
            btn(i).Name = Me.DataGridView1.Rows(i).Cells("命令名称").Value
            btn(i).Text = Me.DataGridView1.Rows(i).Cells("显示文本").Value
            btn(i).Anchor = Windows.Forms.AnchorStyles.None
            btn(i).Width = Me.tlpMain.Width / Me.tlpMain.ColumnCount * Me.DataGridView1.Rows(i).Cells("按钮大小").Value / 100
            btn(i).Height = Me.tlpMain.Height / Me.tlpMain.RowCount * Me.DataGridView1.Rows(i).Cells("按钮大小").Value / 100
            Try
                btn(i).BackgroundImage = Drawing.Image.FromFile(sFilePath & Me.DataGridView1.Rows(i).Cells(4).Value)
                btn(i).BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom

            Catch ex As Exception

            End Try
            btn(i).Font = StringToFont(Me.DataGridView1.Rows(i).Cells("字体").Value)

            Select Case Me.DataGridView1.Rows(i).Cells("文字位置").Value
                Case "左上"
                    btn(i).TextAlign = Drawing.ContentAlignment.TopLeft
                Case "中上"
                    btn(i).TextAlign = Drawing.ContentAlignment.TopCenter
                Case "右上"
                    btn(i).TextAlign = Drawing.ContentAlignment.TopRight
                Case "左中"
                    btn(i).TextAlign = Drawing.ContentAlignment.MiddleLeft
                Case "中中"
                    btn(i).TextAlign = Drawing.ContentAlignment.MiddleCenter
                Case "右中"
                    btn(i).TextAlign = Drawing.ContentAlignment.MiddleRight
                Case "左下"
                    btn(i).TextAlign = Drawing.ContentAlignment.BottomLeft
                Case "中下"
                    btn(i).TextAlign = Drawing.ContentAlignment.BottomCenter
                Case "右下"
                    btn(i).TextAlign = Drawing.ContentAlignment.BottomRight
                Case Else
            End Select

            Me.tlpMain.Controls.Add(btn(i), Me.DataGridView1.Rows(i).Cells("所在列").Value, Me.DataGridView1.Rows(i).Cells("所在行").Value)

            If CType(sender, System.Windows.Forms.Button).Name = "Button1" Then
            Else
                mdbCD.Write("insert into MCBTN (UserID,btnName,btnText,iColumn,iRow,btnDT,btnZT,btnAlign,btnSize,BtnType,GNBBID,GNBBName,tab) values (" & FKG.myselfG.YongHuID & ",'" & Me.DataGridView1.Rows(i).Cells("命令名称").Value & "','" & Me.DataGridView1.Rows(i).Cells("显示文本").Value & "'," & Me.DataGridView1.Rows(i).Cells("所在列").Value & "," & Me.DataGridView1.Rows(i).Cells("所在行").Value & ",'" & Me.DataGridView1.Rows(i).Cells(4).Value & "','" & Me.DataGridView1.Rows(i).Cells("字体").Value & "','" & Me.DataGridView1.Rows(i).Cells("文字位置").Value & "'," & Me.DataGridView1.Rows(i).Cells("按钮大小").Value & ",'" & Me.DataGridView1.Rows(i).Cells("命令来源").Value & "'," & getGNBBID(Me.DataGridView1.Rows(i).Cells("命令来源").Value, Me.DataGridView1.Rows(i).Cells("命令名称").Value) & ",'" & Me.DataGridView1.Rows(i).Cells("命令名称").Value & "','" & Me.DataGridView1.Rows(i).Cells("附加信息").Value & "')")
            End If
        Next
        If CType(sender, System.Windows.Forms.Button).Name = "Button1" Then
            'Me.Button1.Text = "保存界面设置"
        Else
            If mdbCD.bExsit("select * from DTZT where  userid=" & FKG.myselfG.YongHuID) Then
                mdbCD.Write("update DTZT set MCDT='" & Me.cbMCDT.Text & "',MCLIE=" & Me.NumericUpDown2.Value & ",MCHANG=" & Me.NumericUpDown1.Value & ",MCZT='" & sGaoKuan & "' where UserID=" & FKG.myselfG.YongHuID)
            Else
                mdbCD.Write("insert into DTZT (USERID,GNDT,BBDT,MCDT,GNZT,BBZT,MCZT,MMZT,MCLIE,MCHANG) values (" & FKG.myselfG.YongHuID & ",'','','" & Me.cbMCDT.Text & "','','','" & sGaoKuan & "',''," & Me.NumericUpDown2.Value & "," & Me.NumericUpDown1.Value & ")")
            End If

            'Me.Button1.Text = "预览界面"
        End If

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        Select Case e.ColumnIndex
            Case 4
                Dim dlg As New System.Windows.Forms.OpenFileDialog
                If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Try
                        System.IO.File.Copy(dlg.FileName, sFilePath & "\Pictrue\" & IO.Path.GetFileName(dlg.FileName), True)
                    Catch ex As Exception

                    End Try
                    Me.DataGridView1.CurrentCell.Value = "\Pictrue\" & IO.Path.GetFileName(dlg.FileName)
                End If
            Case 5
                Dim dlg As New System.Windows.Forms.FontDialog
                If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Me.DataGridView1.CurrentCell.Value = FontToString(dlg.Font)
                End If
            Case Else

        End Select

    End Sub

    Private Function getGNBBID(ByVal sType As String, ByVal sName As String) As Integer
        Select Case sType
            Case "菜单命令"
                Return 0
            Case "功能树"
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                Dim ds As DataSet
                ds = mdb.Reader("select isnull(yonghuquanxian,0) from  yonghuquanxian,gongnengshu where  yonghuquanxian.yonghuquanxian=gongnengshu.id and yonghuquanxian.yonghuid=" & FKG.myselfG.YongHuID & " and gongnengshu.context='" & sName & "'")
                If ds.Tables(0).Rows.Count = 0 Then
                    Return 0
                Else
                    Return (ds.Tables(0).Rows(0).Item(0))
                End If

            Case "报表树"
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                Dim ds As DataSet
                ds = mdb.Reader("select isnull(yonghuquanxian,0) from  BaoBiaoQuanXian,zdybb where  BaoBiaoQuanXian.yonghuquanxian=zdybb.id and BaoBiaoQuanXian.yonghuid=" & FKG.myselfG.YongHuID & " and zdybb.context='" & sName & "'")
                If ds.Tables(0).Rows.Count = 0 Then
                    Return 0
                Else
                    Return ds.Tables(0).Rows(0).Item(0)
                End If

            Case Else
                Return 0
        End Select
    End Function

    Private Function FontToString(ByVal fFont As Drawing.Font) As String
        Dim sFont As String = ""
        sFont = fFont.Name & ","
        sFont = sFont & fFont.SizeInPoints.ToString & ","
        sFont = sFont & fFont.Style & ","
        sFont = sFont & fFont.Strikeout.ToString & ","
        sFont = sFont & fFont.Underline.ToString

        Return sFont
    End Function

    Private Function StringToFont(ByVal sFont As String)
        Dim fFont As Drawing.Font
        Dim sF() As String
        sF = Split(sFont, ",")
        If sF(0) = "" Then
            fFont = New Drawing.Font("宋体", 9, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point)
        Else
            fFont = New Drawing.Font(sF(0), sF(1), sF(2), Drawing.GraphicsUnit.Point)
        End If

        Return fFont
    End Function

    Private Sub cbGNSZT_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbGNSZT.DropDown
        Dim dlg As New System.Windows.Forms.FontDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.cbGNSZT.Text = FontToString(dlg.Font)
            Me.tvGNS.Font = dlg.Font
        End If

    End Sub


    Private Sub cbGNSDT_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbGNSDT.DropDown
        Dim dlg As New System.Windows.Forms.OpenFileDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                System.IO.File.Copy(dlg.FileName, sFilePath & "\Pictrue\" & IO.Path.GetFileName(dlg.FileName), True)
            Catch ex As Exception

            End Try

            Me.cbGNSDT.Text = "\Pictrue\" & IO.Path.GetFileName(dlg.FileName)
            Me.tvGNS.BackGroundImage = Drawing.Image.FromFile(sFilePath & "\Pictrue\" & IO.Path.GetFileName(dlg.FileName))
            Me.tvGNS.Refresh()
        End If
    End Sub

    Private Sub cbBBSZT_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbBBSZT.DropDown
        Dim dlg As New System.Windows.Forms.FontDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.cbBBSZT.Text = FontToString(dlg.Font)
            Me.tvBBS.Font = dlg.Font
        End If

    End Sub

    Private Sub cbBBSDT_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbBBSDT.DropDown
        Dim dlg As New System.Windows.Forms.OpenFileDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                System.IO.File.Copy(dlg.FileName, sFilePath & "\Pictrue\" & IO.Path.GetFileName(dlg.FileName), True)
            Catch ex As Exception

            End Try

            Me.cbBBSDT.Text = "\Pictrue\" & IO.Path.GetFileName(dlg.FileName)
            Me.tvBBS.BackGroundImage = Drawing.Image.FromFile(sFilePath & "\Pictrue\" & IO.Path.GetFileName(dlg.FileName))
            Me.tvBBS.Refresh()
        End If

    End Sub

    Private Sub btnGNSSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGNSSave.Click
        Dim mdbCD As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        mdbCD.Write("update DTZT set GNDT='" & Me.cbGNSDT.Text & "',BBDT='" & Me.cbBBSDT.Text & "',GNZT='" & Me.cbGNSZT.Text & "',BBZT='" & Me.cbBBSZT.Text & "' where Userid=" & FKG.myselfG.YongHuID)

    End Sub

    Private Sub cbCDZT_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCDZT.DropDown
        Dim dlg As New System.Windows.Forms.FontDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.cbCDZT.Text = FontToString(dlg.Font)
            Me.MenuStrip1.Font = dlg.Font
            Me.ToolStripStatusLabel1.Font = dlg.Font
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim mdbcd As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        mdbcd.Write("update DTZT set MMZT='" & Me.cbCDZT.Text & "' where UserID=" & FKG.myselfG.YongHuID)
    End Sub

    Private Sub tvBBS_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvBBS.AfterCollapse, tvBBS.AfterExpand
        Me.tvBBS.Refresh()
    End Sub

    Private Sub tvGNS_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvGNS.AfterCollapse, tvGNS.AfterExpand
        Me.tvGNS.Refresh()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim mdbcd As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        If Me.TextBox1.Text = "" OrElse Me.TextBox2.Text = "" OrElse Me.TextBox2.Text <> Me.TextBox3.Text Then
            MsgBox("密码设置不正确!", MsgBoxStyle.Information, "提示")
            Exit Sub
        Else
            If mdbcd.Reader("select pwdcompare('" & Me.TextBox1.Text & "',yonghumima)  from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) = 1 Then
                mdbcd.Write("update YongHu set YongHuMiMa=pwdencrypt('" & Me.TextBox2.Text & "') where YongHuID=" & FKG.myselfG.YongHuID)
                MsgBox("密码修改成功，请牢记新密码!", MsgBoxStyle.Information, "提示")

            Else
                MsgBox("没有权限修改密码!", MsgBoxStyle.Information, "提示")
            End If
        End If
    End Sub

    Private Sub cbHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbHide.Click
        Dim xml As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKConfig.xml", True, "config")
        If Me.cbHide.Checked Then
            xml.SaveInnerText("HideTree", "True", "name", "config")
        Else
            xml.SaveInnerText("HideTree", "False", "name", "config")
        End If
    End Sub

    Dim sGaoKuan As String = ""
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim dlg As New SetCellSize(Me.tlpMain.RowCount, Me.tlpMain.ColumnCount, sGaoKuan)
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sGaoKuan = dlg.sGaoKuan

            dlg.Dispose()
        End If
    End Sub
End Class