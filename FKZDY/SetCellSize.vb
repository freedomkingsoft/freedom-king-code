Imports System.Windows.Forms

Public Class SetCellSize

    Public sGaoKuan As String = ""

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        sGaoKuan = ""

        Dim i As Integer
        For i = 0 To Me.dgvRow.RowCount - 1
            sGaoKuan = sGaoKuan & " " & Me.dgvRow.Rows(i).Cells(1).Value
        Next
        sGaoKuan = sGaoKuan.TrimStart(" ") & ", "
        For i = 0 To Me.dgvCol.RowCount - 1
            sGaoKuan = sGaoKuan & " " & Me.dgvCol.Rows(i).Cells(1).Value
        Next

        sGaoKuan = sGaoKuan.Replace("  ", "")

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub SetCellSize_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim i As Integer
        For i = 0 To iRow - 1
            Me.dgvRow.Rows.Add()
            Me.dgvRow.Rows(i).Cells(0).Value = i + 1
            If IsNothing(sGaoKuan) OrElse sGaoKuan = "" Then
                Me.dgvRow.Rows(i).Cells(1).Value = CInt(100 / iRow)
            Else
                Me.dgvRow.Rows(i).Cells(1).Value = Split(Split(sGaoKuan, ",").GetValue(0)).GetValue(i)
            End If

        Next

        For i = 0 To iCol - 1
            Me.dgvCol.Rows.Add()
            Me.dgvCol.Rows(i).Cells(0).Value = i + 1
            If IsNothing(sGaoKuan) OrElse sGaoKuan = "" Then
                Me.dgvCol.Rows(i).Cells(1).Value = CInt(100 / iCol)
            Else
                Me.dgvCol.Rows(i).Cells(1).Value = Split(Split(sGaoKuan, ",").GetValue(1)).GetValue(i)
            End If
        Next

    End Sub

    Dim iRow, iCol As Integer
    Public Sub New(ByVal iRowCount As Integer, ByVal iColCount As Integer, ByVal sGK As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        iRow = iRowCount
        iCol = iColCount

        sGaoKuan = sGK
    End Sub


    Private Sub dgvRow_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRow.CellValidated
        If e.ColumnIndex = 1 Then
            If IsNumeric(Me.dgvRow.CurrentCell.Value) = False Then
                MsgBox("请输入1 - 99之间的数值！", MsgBoxStyle.Information, "输入错误")
                Me.dgvRow.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 15
                Exit Sub
            End If
            If Me.dgvRow.CurrentCell.Value < 1 OrElse Me.dgvRow.CurrentCell.Value > 99 Then
                MsgBox("请输入1 - 99之间的数值！", MsgBoxStyle.Information, "输入错误")
                Me.dgvRow.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 15
            End If
        End If
    End Sub

    Private Sub dgvCol_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCol.CellValidated
        If e.ColumnIndex = 1 Then
            If IsNumeric(Me.dgvCol.CurrentCell.Value) = False Then
                MsgBox("请输入1 - 99之间的数值！", MsgBoxStyle.Information, "输入错误")
                Me.dgvCol.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 15
                Exit Sub
            End If
            If Me.dgvCol.CurrentCell.Value < 1 OrElse Me.dgvCol.CurrentCell.Value > 99 Then
                MsgBox("请输入1 - 99之间的数值！", MsgBoxStyle.Information, "输入错误")
                Me.dgvCol.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 15
            End If
        End If
    End Sub

End Class
