﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SetCellSize
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.dgvRow = New System.Windows.Forms.DataGridView
        Me.行号 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.行高度 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvCol = New System.Windows.Forms.DataGridView
        Me.列号 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.列宽度 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgvRow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCol, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(206, 332)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 27)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 21)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "确定"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 21)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "取消"
        '
        'dgvRow
        '
        Me.dgvRow.AllowUserToAddRows = False
        Me.dgvRow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRow.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.行号, Me.行高度})
        Me.dgvRow.Location = New System.Drawing.Point(23, 43)
        Me.dgvRow.Name = "dgvRow"
        Me.dgvRow.RowHeadersVisible = False
        Me.dgvRow.RowTemplate.Height = 23
        Me.dgvRow.Size = New System.Drawing.Size(154, 273)
        Me.dgvRow.TabIndex = 1
        '
        '行号
        '
        Me.行号.HeaderText = "行号"
        Me.行号.Name = "行号"
        Me.行号.ReadOnly = True
        Me.行号.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.行号.Width = 55
        '
        '行高度
        '
        Me.行高度.HeaderText = "行高度"
        Me.行高度.Name = "行高度"
        Me.行高度.Width = 75
        '
        'dgvCol
        '
        Me.dgvCol.AllowUserToAddRows = False
        Me.dgvCol.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCol.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.列号, Me.列宽度})
        Me.dgvCol.Location = New System.Drawing.Point(198, 43)
        Me.dgvCol.Name = "dgvCol"
        Me.dgvCol.RowHeadersVisible = False
        Me.dgvCol.RowTemplate.Height = 23
        Me.dgvCol.Size = New System.Drawing.Size(154, 273)
        Me.dgvCol.TabIndex = 2
        '
        '列号
        '
        Me.列号.HeaderText = "列号"
        Me.列号.Name = "列号"
        Me.列号.ReadOnly = True
        Me.列号.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.列号.Width = 55
        '
        '列宽度
        '
        Me.列宽度.HeaderText = "列宽度"
        Me.列宽度.Name = "列宽度"
        Me.列宽度.Width = 75
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "行高设置"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(204, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "列宽设置"
        '
        'SetCellSize
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(373, 387)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvCol)
        Me.Controls.Add(Me.dgvRow)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SetCellSize"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "设定单元格大小"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgvRow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCol, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents dgvRow As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCol As System.Windows.Forms.DataGridView
    Friend WithEvents 行号 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 行高度 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 列号 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 列宽度 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
