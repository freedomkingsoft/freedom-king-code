﻿Public Class FKCKDZ

    '保存各表格数据源
    'Dim dvKuCun As DataView
    'Dim dvGongZi As DataView
    'Dim dvPZ As DataView

    Dim dvKCLK, dvKCDot As String
    Dim dvGZLK, dvGZDot As String
    Dim dvPZLK, dvPZDot As String


    Dim iGongNengID As Integer

    Private Function getGNID(ByVal BianHao As String) As Integer
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        'Dim sQDF As String
        'sQDF = BianHao.Substring(0, BianHao.Length - FKG.myselfG.iBianHaoWeiShu - 2 - FKG.myselfG.iNianFenWeiShu)
        iGongNengID = mdb.Reader("select distinct A_GNSID FROM JIBENCAOZUO WHERE BIANHAO='" & BianHao & "' AND 删除='false'").Tables(0).Rows(0).Item(0)
        Return iGongNengID
    End Function

    Private Function FillDG(ByVal Bianhao As String) As Boolean
        If Bianhao = "" Then
            Exit Function
        End If

        Me.lblPZH.Text = "单证编号：" & "  " & Bianhao
        Me.lblRiQi.Text = "日期：" & "  " & Bianhao.Substring(Bianhao.Length - FKG.myselfG.iNianFenWeiShu - 2 - FKG.myselfG.iBianHaoWeiShu, FKG.myselfG.iNianFenWeiShu) & " 年" & Bianhao.Substring(Bianhao.Length - FKG.myselfG.iBianHaoWeiShu - 2, 2) & " 月"

        Dim bHang As Boolean = False
        Dim iHangShu As Integer = -1
        '以上两个变量用于解决行数问题
        Dim dv As DataView

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim dsGNS As DataSet
        dsGNS = mdb.Reader("select * from GongNengShu where ID=" & getGNID(Bianhao))
        Me.tssNianYue.Text = "功能名称：" & dsGNS.Tables(0).Rows(0).Item("Context").ToString

        Dim bXSTable As Boolean
        If dsGNS.Tables(0).Rows(0).Item("KuCun") = True Then
            If dsGNS.Tables(0).Rows(0).Item("KuCunSQL") = "" Then
                Me.dgvPZ.Tag = "*"
            Else
                Me.dgvPZ.Tag = dsGNS.Tables(0).Rows(0).Item("KuCunSQL")
            End If
            dv = mdb.Reader("select A_JBID, " & Me.dgvPZ.Tag & " from 库存表 where 编号 ='" & Bianhao & "' and 功能名称='" & dsGNS.Tables(0).Rows(0).Item("Context") & "' and 显示='true' order by 行数").Tables(0).DefaultView

            Dim sss() As String
            sss = Split(Me.dgvPZ.Tag.ToString, ",")
            Dim stemp As String
            For Each stemp In sss
                If stemp.StartsWith("行数") Then
                    bHang = True
                    iHangShu = Array.IndexOf(sss, stemp)
                    Exit For
                End If
            Next

            JiSuanHeJiZhi(dv, "KuCunHeJi", bHang, iHangShu)
            Me.dgvPZ.DataSource = dv
            Me.dgvPZ.Columns(0).Visible = False
            SetHJColor(Me.dgvPZ)

            bXSTable = True
        End If

        If dsGNS.Tables(0).Rows(0).Item("GongZi") = True AndAlso bXSTable = False Then
            bHang = False

            If dsGNS.Tables(0).Rows(0).Item("GongZiSQL") = "" Then
                Me.dgvPZ.Tag = "*"
            Else
                Me.dgvPZ.Tag = dsGNS.Tables(0).Rows(0).Item("GongZiSQL")
            End If
            dv = mdb.Reader("select  A_JBID, " & Me.dgvPZ.Tag & " from 工资表 where 编号 ='" & Bianhao & "'  and 功能名称='" & dsGNS.Tables(0).Rows(0).Item("Context") & "' and 显示='true'  order by 行数").Tables(0).DefaultView

            Dim sss() As String
            sss = Split(Me.dgvPZ.Tag)
            Dim stemp As String
            bHang = False
            For Each stemp In sss
                If stemp.StartsWith("行数") Then
                    bHang = True
                    iHangShu = Array.IndexOf(sss, stemp)
                    Exit For
                End If
            Next

            JiSuanHeJiZhi(dv, "GongZiHeJi", bHang, iHangShu)
            Me.dgvPZ.DataSource = dv
            Me.dgvPZ.Columns(0).Visible = False
            SetHJColor(Me.dgvPZ)

            bXSTable = True
        End If

        If dsGNS.Tables(0).Rows(0).Item("PZ") = True AndAlso bXSTable = False Then
            If dsGNS.Tables(0).Rows(0).Item("pzSQL") = "" Then
                Me.dgvPZ.Tag = "*"
            Else
                Me.dgvPZ.Tag = dsGNS.Tables(0).Rows(0).Item("pzSQL")
            End If
            dv = mdb.Reader("select A_JBID, " & Me.dgvPZ.Tag & " from 凭证表 where 编号 ='" & Bianhao & "' and 功能名称='" & dsGNS.Tables(0).Rows(0).Item("Context") & "' and 显示='true' order by 行数").Tables(0).DefaultView

            Dim sss() As String
            sss = Split(Me.dgvPZ.Tag.ToString, ",")
            Dim stemp As String
            For Each stemp In sss
                If stemp.StartsWith("行数") Then
                    bHang = True
                    iHangShu = Array.IndexOf(sss, stemp)
                    Exit For
                End If
            Next

            JiSuanHeJiZhi(dv, "pzHeJi", bHang, iHangShu)
            Me.dgvPZ.DataSource = dv
            Me.dgvPZ.Columns(0).Visible = False
            SetHJColor(Me.dgvPZ)

            bXSTable = True

        End If



        Dim dsSH As DataSet
        dsSH = mdb.Reader("select isnull(审核,'') from JiBenCaoZuo where BianHao='" & Bianhao & "' and 删除='false'")

        If dsSH.Tables(0).Rows(0).Item(0).ToString = "" Then
            Me.tssState.Text = "未审核"
            Me.tssSHR.Text = ""
        Else
            Me.tssState.Text = "已审核"
            Me.tssSHR.Text = dsSH.Tables(0).Rows(0).Item(0).ToString
        End If
    End Function

    Private Function JiSuanHeJiZhi(ByVal dv As DataView, ByVal sBiaoHeJi As String, ByVal bXSHS As Boolean, Optional ByVal iHang As Integer = -1) As Boolean
        If dv.Count = 0 Then
            HideZero(dv)
            Return True
        Else
            dv.AddNew()

            Dim i As Integer
            Dim n As Integer
            Dim iMaxRowIndex As Integer
            Dim dHeJi As Double
            iMaxRowIndex = dv.Count - 1

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim sHJ As String
            sHJ = mdb.Reader("select " & sBiaoHeJi & "  from gongnengshu where id=" & iGongnengID).Tables(0).Rows(0).Item(0).ToString

            For n = 1 To dv.Table.Columns.Count - 1
                If sHJ = "" OrElse sHJ.Substring(n - 1, 1).ToString = "1" Then
                    dv.Item(iMaxRowIndex).Item(n) = DBNull.Value
                ElseIf sHJ.Substring(n - 1, 1).ToString = "2" Then
                    dHeJi = 0
                    For i = 0 To dv.Count - 2
                        Try
                            dHeJi = dHeJi + dv.Item(i).Item(n)
                        Catch ex As Exception
                            Exit For
                        End Try
                    Next
                    If dHeJi = 0 Then
                        dv.Item(iMaxRowIndex).Item(n) = DBNull.Value
                    Else
                        dv.Item(iMaxRowIndex).Item(n) = dHeJi
                    End If
                Else
                    dv.Item(iMaxRowIndex).Item(n) = "合计："
                End If

            Next
            '填充新行数
            If bXSHS Then
                For i = 0 To dv.Count - 2
                    dv.Item(i).Item(iHang + 1) = i + 1
                Next
            End If

            HideZero(dv)
        End If
    End Function

    Private Sub HideZero(ByVal dv As DataView)
        Dim i, n As Integer
        For i = 1 To dv.Table.Columns.Count - 1
            For n = 0 To dv.Count - 2
                Try
                    If dv.Item(n).Item(i) = 0 Then
                        dv.Item(n).Item(i) = DBNull.Value
                    End If
                Catch ex As Exception
                    Exit For
                End Try

            Next
        Next
    End Sub

    Private Sub SetHJColor(ByVal obj As System.Windows.Forms.DataGridView)
        If obj.RowCount = 0 Then
            Exit Sub
        Else
            obj.Rows(obj.Rows.GetLastRow(Windows.Forms.DataGridViewElementStates.None)).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.LightGray, False)
        End If

    End Sub

    Private Sub SetLK(ByVal obj As System.Windows.Forms.DataGridView)
        Dim i As Integer
        Dim sLK() As String
        Dim sDot() As String
        Select Case obj.Name
            Case "dgvKuCun"
                sLK = Split(dvKCLK)
                sDot = Split(dvKCDot)
            Case "dgvGongZi"
                sLK = Split(dvGZLK)
                sDot = Split(dvGZDot)
            Case "dgvPZ"
                sLK = Split(dvPZLK)
                sDot = Split(dvPZDot)
            Case Else
                Exit Sub
        End Select
        If sLK.Length = 1 Then
            Exit Sub
        End If
        For i = 1 To obj.ColumnCount - 1
            obj.Columns(i).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
            obj.Columns(i).Width = sLK.GetValue(i - 1) * obj.Width / 100
            If sDot.Length = 1 Then
            Else
                If sDot.GetValue(i - 1) = -1 Then
                Else
                    obj.Columns(i).DefaultCellStyle.Format = "N" & sDot(i - 1)
                End If
            End If
        Next

    End Sub

    Private Sub DispSize(Optional ByVal bLoad As Boolean = False)
        'Me.tlp.RowStyles(0).Height = Me.gbSelect.Height + 6

        Dim iXSNumber As Integer = 0
        If bLoad Then '初始化大小
            Dim ds As DataSet
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            ds = mdb.Reader("select * from GongNengShu where ID=" & iGongnengID)
            '设置是否显示
            If ds.Tables(0).Rows(0).Item("kucun") = False Then
                'Me.dgvKuCun.Visible = False
                'Me.lblKuCun.Visible = False

                'Me.tlp.RowStyles(1).Height = 0
            Else
                iXSNumber = iXSNumber + 1
                If ds.Tables(0).Rows(0).Item("KuCunSQL").ToString = "" Then
                    'Me.dgvKuCun.Tag = " * "
                    dvKCLK = ""
                    dvKCDot = ""
                Else
                    'Me.dgvKuCun.Tag = ds.Tables(0).Rows(0).Item("KuCunSQL").ToString
                    dvPZLK = ds.Tables(0).Rows(0).Item("KuCunLK").ToString
                    dvPZDot = ds.Tables(0).Rows(0).Item("KuCunDot").ToString
                    SetLK(dgvPZ)

                    Exit Sub
                End If
            End If

            If ds.Tables(0).Rows(0).Item("GongZi") = False Then
                'Me.dgvGongZi.Visible = False
                'Me.lblGongZi.Visible = False

                'Me.tlp.RowStyles(2).Height = 0
            Else
                iXSNumber = iXSNumber + 1
                If ds.Tables(0).Rows(0).Item("GongZiSQL").ToString = "" Then
                    'Me.dgvGongZi.Tag = " * "
                    dvGZLK = ""
                    dvGZDot = ""
                Else
                    'Me.dgvGongZi.Tag = ds.Tables(0).Rows(0).Item("GongZiSQL").ToString
                    dvPZLK = ds.Tables(0).Rows(0).Item("GongZiLK").ToString
                    dvPZDot = ds.Tables(0).Rows(0).Item("GongZiDot").ToString
                    SetLK(dgvPZ)

                    Exit Sub
                End If
            End If

            If ds.Tables(0).Rows(0).Item("pz") = False Then
                'Me.dgvPZ.Visible = False
                'Me.lblPZ.Visible = False

                'Me.tlp.RowStyles(3).Height = 0
            Else
                iXSNumber = iXSNumber + 1
                If ds.Tables(0).Rows(0).Item("pzSQL").ToString = "" Then
                    'Me.dgvPZ.Tag = " * "
                    dvPZLK = ""
                    dvPZDot = ""
                Else
                    'Me.dgvPZ.Tag = ds.Tables(0).Rows(0).Item("pzSQL").ToString
                    dvPZLK = ds.Tables(0).Rows(0).Item("PZLK").ToString
                    dvPZDot = ds.Tables(0).Rows(0).Item("PZDot").ToString
                    SetLK(dgvPZ)
                End If
            End If
        Else
            SetLK(dgvPZ)
            'If Me.dgvKuCun.Visible Then
            '    iXSNumber = iXSNumber + 1
            'End If
            'If Me.dgvGongZi.Visible Then
            '    iXSNumber = iXSNumber + 1
            'End If
            'If Me.dgvPZ.Visible Then
            '    iXSNumber = iXSNumber + 1
            'End If

            'Dim iHeight As Integer
            'iHeight = Me.tlp.Height - Me.gbSelect.Height - 16

            'If Me.dgvKuCun.Visible Then
            '    Me.dgvKuCun.Height = iHeight \ iXSNumber
            '    Me.lblKuCun.Height = Me.dgvKuCun.Height
            '    SetLK(Me.dgvKuCun)
            'End If
            'If Me.dgvGongZi.Visible Then
            '    Me.dgvGongZi.Height = iHeight \ iXSNumber
            '    Me.lblGongZi.Height = Me.dgvGongZi.Height
            '    SetLK(Me.dgvGongZi)
            'End If
            'If Me.dgvPZ.Visible Then
            '    Me.dgvPZ.Height = iHeight \ iXSNumber
            '    Me.lblPZ.Height = Me.dgvPZ.Height
            '    SetLK(Me.dgvPZ)
            'End If

        End If

    End Sub

    Private Sub FKSHDZ_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Icon = FKG.myselfG.FKIcon
        '        DispSize(True)
        If sBianhao = "" Then
            Exit Sub
        End If

        FillDG(sBianhao)
        DispSize(True)
    End Sub

    Private Sub FKGN_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        DispSize()
    End Sub

    Dim sBianhao As String
    Public Sub New(ByVal BianHao As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        sBianhao = BianHao
    End Sub

    Dim sGeShiFile As String
    Dim dvDaoChu As DataView
    Private Sub btnDaoChu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoChu.Click
        If sBianhao = "" Then
            Exit Sub
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        '通过编号反查功能，然后查出对应的模板文件
        'select top 1 A_gnsid from jibencaozuo where bianhao='XSCK2401015'  得到功能树id
        'select CASE WHEN KUCUN=1 THEN kucunpath when gongzi=1 then gongzipath else pzpath end from gongnengshu where id = A_gnsid 得到模板文件

        Dim sSqlFile As String
        Dim sSqlDaoChuLie As String
        Dim sSqlDaoChuBiao As String

        Dim sDaochuLie As String
        Dim sDaochuBiao As String

        sSqlFile = "select CASE WHEN KUCUN=1 THEN kucunpath when gongzi=1 then gongzipath else pzpath end from gongnengshu where id = (select top 1 A_gnsid from jibencaozuo where bianhao='" & sBianhao & "')"
        sSqlDaoChuLie = "select CASE WHEN KUCUN=1 THEN kucunprint when gongzi=1 then gongziprint else pzprint end from gongnengshu where id = (select top 1 A_gnsid from jibencaozuo where bianhao='" & sBianhao & "')"
        sSqlDaoChuBiao = "select CASE WHEN KUCUN=1 THEN '库存表' when gongzi=1 then '工资表' else '凭证表' end from gongnengshu where id = (select top 1 A_gnsid from jibencaozuo where bianhao='" & sBianhao & "')"
        Try
            'Dim ds As DataSet
            'ds = mdb.Reader(sSqlFile)

            sGeShiFile = mdb.Reader(sSqlFile).Tables(0).Rows(0).Item(0).ToString
            sDaochuLie = mdb.Reader(sSqlDaoChuLie).Tables(0).Rows(0).Item(0).ToString
            sDaochuBiao = mdb.Reader(sSqlDaoChuBiao).Tables(0).Rows(0).Item(0).ToString

            dvDaoChu = mdb.Reader("select " & sDaochuLie & " from " & sDaochuBiao & " where 编号='" & sBianhao & "' and 显示=1").Tables(0).DefaultView
        Catch ex As Exception
            MsgBox(ex.ToString)
            Exit Sub
        End Try
        ImportOutToFile()

    End Sub

    Private Sub ImportOutToFile()
        Dim fkP As New FKPrn.Print("报表", 0, 1)

        fkP.sGeShiFile = sGeShiFile

        '根据编号获取需要导出的数据源，不能使用 当前表格
        fkP.dvBaoBiao = dvDaoChu
        ' fkP.dvBaoBiao = Me.dgvPZ.DataSource

        'If bHuiZong Then
        fkP.bDeleteLast = False
        'Else
        'fkP.bDeleteLast = True
        'End If

        fkP.PrintPreview(True)
        fkP = Nothing
        System.GC.Collect()
    End Sub

    Private Sub btnFill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFill.Click
        If Me.txtBianhao.Text = "" Then
            Exit Sub
        End If

        Me.sBianhao = Me.txtBianhao.Text

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdb.Reader("select * from jibencaozuo where bianhao ='" & sBianhao & "' and 删除=0").Tables(0).Rows.Count = 0 Then
            MsgBox("编号不存在，请输入完整编号！", MsgBoxStyle.Information, "提示")
            Me.txtBianhao.Text = ""
            Me.sBianhao = Me.txtBianhao.Text
            Me.dgvPZ.DataSource = Nothing
        Else

            FillDG(sBianhao)
            DispSize(True)
        End If
    End Sub
End Class