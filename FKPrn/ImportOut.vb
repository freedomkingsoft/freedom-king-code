﻿Imports Microsoft.Office.Interop

Public Class ImportOut

    'Dim myType As Object = Type.GetTypeFromProgID("theProgramID")

    Dim eApptype As Type = Type.GetTypeFromProgID("Excel.Application")
    'Dim eApp As New Excel.Application
    Dim eApp As Object = Activator.CreateInstance(eApptype)

    Dim eBook As Excel.Workbook
    Dim eSheet As Excel.Worksheet

    Private Sub openExcel(ByVal myExcelFileName As String)
        If myExcelFileName = "" Then
            eApp.Workbooks.Add()
        Else
            eApp.Workbooks.Open(myExcelFileName, , True)
        End If
        eBook = eApp.ActiveWorkbook
        eSheet = eBook.Sheets(1)
    End Sub

    Public Sub closeExcel()

        eBook.Close(False)
        eApp.Quit()

        System.Runtime.InteropServices.Marshal.ReleaseComObject(eSheet)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eBook)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eApp)

        eSheet = Nothing
        eBook = Nothing
        eApp = Nothing

        System.GC.Collect()
    End Sub

    Public Sub Importout(ByVal dgv As System.Windows.Forms.DataGridView, Optional ByVal myExcelFileName As String = "", Optional ByVal bXSRowHeader As Boolean = True, Optional ByVal iStartRow As Integer = 1, Optional ByVal sStartCol As Integer = 1)
        openExcel(myExcelFileName)
        If bXSRowHeader = False Then
            Dim iExcelCol As Integer = sStartCol
            Dim iRow, iCol As Integer
            'For iCol = 0 To dgv.ColumnCount - 1
            '    If dgv.Columns(iCol).Visible Then
            '        eSheet.Cells(iStartRow, iExcelCol) = dgv.Columns(iCol).Name
            '        iExcelCol = iExcelCol + 1
            '    Else
            '        Continue For
            '    End If
            'Next

            'iExcelCol = sStartCol
            For iCol = 0 To dgv.ColumnCount - 1
                If dgv.Columns(iCol).Visible Then
                    For iRow = 0 To dgv.RowCount - 1
                        eSheet.Cells(iRow + iStartRow, iExcelCol) = dgv.Rows(iRow).Cells(iCol).Value.ToString
                    Next
                    iExcelCol = iExcelCol + 1
                Else
                    Continue For
                End If
            Next
        Else
            Dim iExcelCol As Integer = sStartCol
            Dim iRow, iCol As Integer
            For iCol = 0 To dgv.ColumnCount - 1
                If dgv.Columns(iCol).Visible Then
                    'eSheet.Cells(iStartRow, iExcelCol) = dgv.Columns(iCol).Name
                    eSheet.Cells(iStartRow, iExcelCol) = dgv.Columns(iCol).HeaderText
                    iExcelCol = iExcelCol + 1
                Else
                    Continue For
                End If
            Next

            iExcelCol = sStartCol
            For iCol = 0 To dgv.ColumnCount - 1
                If dgv.Columns(iCol).Visible Then
                    For iRow = 0 To dgv.RowCount - 1
                        eSheet.Cells(iRow + iStartRow + 1, iExcelCol) = dgv.Rows(iRow).Cells(iCol).Value.ToString
                    Next
                    iExcelCol = iExcelCol + 1
                Else
                    Continue For
                End If
            Next
        End If

        'eApp.Visible = Not bPrint
        eApp.visible = True

    End Sub

    'Public Sub Importout(ByVal dgv As System.Windows.Forms.DataGridView, Optional ByVal myExcelFileName As String = "")
    '    openExcel(myExcelFileName)
    '    Dim iExcelCol As Integer = 1
    '    Dim iRow, iCol As Integer
    '    For iCol = 0 To dgv.ColumnCount - 1
    '        If dgv.Columns(iCol).Visible Then
    '            eSheet.Cells(1, iExcelCol) = dgv.Columns(iCol).Name
    '            iExcelCol = iExcelCol + 1
    '        Else
    '            Continue For
    '        End If
    '    Next

    '    iExcelCol = 1
    '    For iCol = 0 To dgv.ColumnCount - 1
    '        If dgv.Columns(iCol).Visible Then
    '            For iRow = 0 To dgv.RowCount - 1
    '                eSheet.Cells(iRow + 2, iExcelCol) = dgv.Rows(iRow).Cells(iCol).Value.ToString
    '            Next
    '            iExcelCol = iExcelCol + 1
    '        Else
    '            Continue For
    '        End If
    '    Next
    '    eApp.Visible = True

    'End Sub

    Public Function ReadFromExcel(ByVal sFileName As String) As DataTable
        Dim dt As New DataTable
        Me.openExcel(sFileName)
        Dim iLie As Integer = 1 '
        Do While IsNothing(eSheet.Cells(1, iLie).value) = False
            dt.Columns.Add(eSheet.Cells(1, iLie).value)
            iLie = iLie + 1
        Loop

        Dim iHang As Integer = 0
        Dim i As Integer = 0
        Do While IsNothing(eSheet.Cells(iHang + 1, 1).value) = False
            For i = 0 To iLie - 2
                dt.Rows.Add()
                dt.Rows(iHang).Item(i) = eSheet.Cells(iHang + 1, i + 1).value
            Next
            iHang = iHang + 1
        Loop

        Me.closeExcel()
        Return dt
    End Function


    '以下为使用oficce引用时的程序
    'Dim eApp As New Excel.Application
    'Dim eBook As Excel.Workbook
    'Dim eSheet As Excel.Worksheet

    'Private Sub openExcel(ByVal myExcelFileName As String)
    '    eApp.Workbooks.Add()
    '    eBook = eApp.ActiveWorkbook
    '    eSheet = eBook.Sheets(1)
    'End Sub

    'Public Sub closeExcel()

    '    eBook.Close(False)
    '    eApp.Quit()

    '    System.Runtime.InteropServices.Marshal.ReleaseComObject(eSheet)
    '    System.Runtime.InteropServices.Marshal.ReleaseComObject(eBook)
    '    System.Runtime.InteropServices.Marshal.ReleaseComObject(eApp)

    '    eSheet = Nothing
    '    eBook = Nothing
    '    eApp = Nothing

    '    System.GC.Collect()
    'End Sub

    'Public Sub Importout(ByVal dgv As System.Windows.Forms.DataGridView)
    '    openExcel("")
    '    Dim iExcelCol As Integer = 1
    '    Dim iRow, iCol As Integer
    '    For iCol = 0 To dgv.ColumnCount - 1
    '        If dgv.Columns(iCol).Visible Then
    '            eSheet.Cells(1, iExcelCol) = dgv.Columns(iCol).Name
    '            iExcelCol = iExcelCol + 1
    '        Else
    '            Continue For
    '        End If
    '    Next

    '    iExcelCol = 1
    '    For iCol = 0 To dgv.ColumnCount - 1
    '        If dgv.Columns(iCol).Visible Then
    '            For iRow = 0 To dgv.RowCount - 1
    '                eSheet.Cells(iRow + 2, iExcelCol) = dgv.Rows(iRow).Cells(iCol).Value.ToString
    '            Next
    '            iExcelCol = iExcelCol + 1
    '        Else
    '            Continue For
    '        End If
    '    Next
    '    eApp.Visible = True

    'End Sub
End Class
