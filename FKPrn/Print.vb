Imports Microsoft.Office.Interop
Public Class Print
    Public iNian, iYue, iPZSCFS As Integer '打印凭证时使用
    'Public bGDHS As Boolean '打印凭证时需要设置为 true

    Dim Bianhao As String
    Dim GongNengID As Integer
    'Dim GongNengName As String暂且不设置，看是否必要
    Dim DanZhengType As Integer '1 库存单证,　２　工资单　　３　凭证
    Dim SortCollumn As String '打印时可以设置的排序列

    Dim eApptype As Type = Type.GetTypeFromProgID("Excel.Application")
    'Dim eApp As New Excel.Application 
    Dim eApp As Object = Activator.CreateInstance(eApptype)

    'Dim eApp As New Excel.Application
    Dim eBook As Excel.Workbook
    Dim eSheet As Excel.Worksheet

    Public Sub New(ByVal sBianHao As String, ByVal iGongnengID As Integer, ByVal idanzhengtype As Integer, Optional ByVal sSort As String = "")
        Bianhao = sBianHao
        GongNengID = iGongnengID
        'GongNengName = sGongNengName
        DanZhengType = idanzhengtype
        If sSort = "" Then
            SortCollumn = ""
        Else
            SortCollumn = " order by " & sSort
        End If

        '在第一次调用Print 时读取 templateTitle 放置到数组中
        '程序启动后只读取一次，重新设置后需要重启程序

        If FKG.myselfG.TemplateTitle.Length = 0 Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim ds As DataSet
            ds = mdb.Reader("select zhi from ZTSX where ShuXing like '%模板%'")
            Array.Resize(FKG.myselfG.TemplateTitle, ds.Tables(0).Rows.Count)
            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                FKG.myselfG.TemplateTitle(i) = ds.Tables(0).Rows(i).Item(0).ToString
            Next
        End If
    End Sub


    Dim sLie As String
    Private Function GetFileName() As String
        Dim sFile As String

        If iPZSCFS = 0 Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim ds As DataSet
            Select Case DanZhengType
                Case 1
                    ds = mdb.Reader("select isnull(KuCunPath,''),isnull(KuCunPrint,'') from GongNengShu where ID=" & GongNengID)
                Case 2
                    ds = mdb.Reader("select isnull(GongZiPath,''),isnull(GongZiPrint,'') from GongNengShu where ID=" & GongNengID)
                Case 3
                    ds = mdb.Reader("select isnull(PZPath,''),isnull(PZPrint,'') from GongNengShu where ID=" & GongNengID)
                Case Else
                    ds = Nothing
            End Select

            sFile = ds.Tables(0).Rows(0).Item(0)
            sLie = ds.Tables(0).Rows(0).Item(1)
        Else
            'sFile = "非自定义-单金额凭证模版-套打.xls"
            Dim xml As New FKSetting.XMLRWer("FKConfig.xml")
            sFile = xml.Read("凭证打印模板文件", "name", "config")
        End If

        Return sFile
    End Function

    Private Sub openExcel(ByVal myExcelFileName As String)
        eApp.Workbooks.Open(myExcelFileName, , True)
        eBook = eApp.ActiveWorkbook
        eSheet = eBook.Sheets(1)
    End Sub

    Private Sub closeExcel()

        eBook.Close(False)
        eApp.Quit()

        System.Runtime.InteropServices.Marshal.ReleaseComObject(eSheet)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eBook)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eApp)

        eSheet = Nothing
        eBook = Nothing
        eApp = Nothing

        System.GC.Collect()
    End Sub

    Private Sub fillDatasetTOExcel(ByVal myDataset As DataSet, ByVal iExcelRow As Integer, ByVal iExcelColumn As Integer, ByVal iStartHang As Integer, ByVal iHangShu As Integer)
        Dim iRow, iCol As Integer

        If iStartHang > 0 Then
            '如果不是第一页，首先清除上页填充的数据
            For iRow = 0 To iHangShu - 1
                For iCol = 0 To myDataset.Tables(0).Columns.Count - 1
                    '增加不填写 0 功能
                    'If IsNumeric(myDataset.Tables(0).Rows(iRow).Item(iCol)) AndAlso myDataset.Tables(0).Rows(iRow).Item(iCol) = 0 Then
                    'Else
                    eSheet.Cells(iExcelRow + iRow, iExcelColumn + iCol) = ""
                    'End If
                Next
            Next
        End If

        iHangShu = Math.Min(iStartHang + iHangShu, myDataset.Tables(0).Rows.Count)

        Dim iRowAdd As Integer = 0
        For iRow = iStartHang To iHangShu - 1
            For iCol = 0 To myDataset.Tables(0).Columns.Count - 1
                '增加不填写 0 功能
                If IsNumeric(myDataset.Tables(0).Rows(iRow).Item(iCol)) AndAlso myDataset.Tables(0).Rows(iRow).Item(iCol) = 0 Then
                Else
                    eSheet.Cells(iExcelRow + iRowAdd, iExcelColumn + iCol) = myDataset.Tables(0).Rows(iRow).Item(iCol)
                End If
            Next
            iRowAdd = iRowAdd + 1
        Next
    End Sub


    Private Sub fillDatasetTOExcel(ByVal myDataset As DataView, ByVal iExcelRow As Integer, ByVal iExcelColumn As Integer, ByVal iStartHang As Integer, ByVal iHangShu As Integer)
        Dim iRow, iCol As Integer

        If iStartHang > 0 Then
            '如果不是第一页，首先清除上页填充的数据
            For iRow = 0 To iHangShu - 1
                For iCol = 0 To myDataset.ToTable.Columns.Count - 1
                    '增加不填写 0 功能
                    'If IsNumeric(myDataset.Tables(0).Rows(iRow).Item(iCol)) AndAlso myDataset.Tables(0).Rows(iRow).Item(iCol) = 0 Then
                    'Else
                    eSheet.Cells(iExcelRow + iRow, iExcelColumn + iCol) = ""
                    'End If
                Next
            Next
        End If

        iHangShu = Math.Min(iStartHang + iHangShu, myDataset.Count)

        Dim iRowAdd As Integer = 0
        For iRow = iStartHang To iHangShu - 1
            For iCol = 0 To myDataset.ToTable.Columns.Count - 1
                '增加不填写 0 功能
                If IsNumeric(myDataset.Item(iRow).Item(iCol)) AndAlso myDataset.Item(iRow).Item(iCol) = 0 Then
                Else
                    eSheet.Cells(iExcelRow + iRowAdd, iExcelColumn + iCol) = myDataset.Item(iRow).Item(iCol)
                End If
            Next
            iRowAdd = iRowAdd + 1
        Next
    End Sub

    Private Sub fillStringTOExcel(ByVal s As Object, ByVal iExcelRow As Integer, ByVal iExcelColumn As Integer)
        eSheet.Cells(iExcelRow, iExcelColumn) = s
    End Sub

    Public Sub PrintPreview(Optional ByVal bImportToFile As Boolean = False)
        FillData(False, bImportToFile)
        'If FillData(False) Then
        '    eApp.Visible = True
        '    eSheet.PrintPreview()

        '    closeExcel()
        'End If
    End Sub

    Public Sub Print()
        FillData(True)
        'If FillData(True) Then
        '    eSheet.PrintOut()

        '    closeExcel()
        'End If
    End Sub

    Private Sub insertRow(ByVal iIndex As Integer, ByVal iNumberRow As Integer)
        'eApp.Visible = True
        Dim excelRange As Excel.Range
        excelRange = eSheet.Rows(iIndex.ToString & ":" & iIndex.ToString)

        Dim i As Integer = 0
        For i = 0 To iNumberRow - 1
            excelRange.Copy()
            excelRange.Insert(Excel.XlDirection.xlDown)
        Next
    End Sub

    Private Function getCell(ByVal iRowIndex As Integer, ByVal iColumnIndex As Integer) As Object
        Return eSheet.Cells(iRowIndex, iColumnIndex).value
    End Function

    Private Sub clearRows(ByVal iRowIndex As Integer)
        Dim er As Excel.Range
        er = eSheet.Rows(iRowIndex)
        er.ClearContents()
    End Sub

    '以下Public变量均用于“报表”打印
    Public dvBaoBiao As DataView
    Public dsHuiZong As DataSet
    Public sGeShiFile As String

    Public sKMDMBB As String = ""
    Public sZQ As String = ""

    Public bDeleteLast As Boolean = True

    Private Function FillData(ByVal bPrint As Boolean, Optional ByVal bImportOutToFile As Boolean = False) As Boolean
        Dim sFile As String
        Dim dvPrnBaoBiao As New DataView
        If Bianhao = "报表" Then
            sFile = sGeShiFile
            dvPrnBaoBiao = dvBaoBiao.ToTable.DefaultView
            If bDeleteLast Then
                dvPrnBaoBiao.Item(dvPrnBaoBiao.Count - 1).Delete()
            Else
                'dvPrnBaoBiao.Item(dvPrnBaoBiao.Count - 1).Delete()
            End If
        Else
            sFile = GetFileName()
        End If
        If sFile = "" Then
            MsgBox("未指定模板文件,不能完成打印", MsgBoxStyle.Information, My.Application.Info.Title)
            Return False
        Else
            sFile = My.Application.Info.DirectoryPath & "\Templates\" & sFile
        End If

        If System.IO.File.Exists(sFile) = False Then
            MsgBox("没有找到指定文件,不能完成打印", MsgBoxStyle.Information, My.Application.Info.Title)
            Return False
        End If

        Me.openExcel(sFile)

        Dim ds As DataSet
        If iPZSCFS = 0 Then
            ds = getPrintDS()
        Else
            ds = Me.dsHuiZong
        End If

        If Bianhao = "报表" Then
            If IsNothing(dvPrnBaoBiao) Then
                Return False
            End If
        Else
            If IsNothing(ds) Then
                Return False
            End If
        End If

        Dim iTableRow, iTableCol, iTableMinHang, iTableMaxHang As Integer

        Try
            iTableRow = eSheet.Cells(1, 1).Value
            iTableCol = eSheet.Cells(1, 2).value
            iTableMinHang = eSheet.Cells(1, 3).value
            iTableMaxHang = eSheet.Cells(1, 4).value
        Catch ex As Exception
            iTableRow = 6
            iTableCol = 2
            iTableMinHang = 5
            iTableMaxHang = 5
        End Try
        If iTableRow = 0 Then
            iTableRow = 6
        End If
        If iTableCol = 0 Then
            iTableCol = 2
        End If
        If iTableMinHang = 0 Then
            iTableMinHang = 5
        End If
        If iTableMaxHang = 0 OrElse iTableMaxHang < iTableMinHang Then
            iTableMaxHang = iTableMinHang
        End If
        'Excel行数处理
        If iTableMaxHang = iTableMinHang Then
            '固定行数
            Dim iDSHang, iCurPage, iTotalPage As Integer
            If Bianhao = "报表" Then
                iDSHang = dvPrnBaoBiao.Count
            Else
                iDSHang = ds.Tables(0).Rows.Count '数据行数
            End If


            If iDSHang <= iTableMaxHang Then
                '一共一页
                iCurPage = 1
                iTotalPage = 1

                eSheet.Cells(1, 9) = iCurPage
                eSheet.Cells(1, 10) = iTotalPage
                eSheet.Cells(1, 11) = FKG.myselfG.QiYeMingCheng
                eSheet.Cells(1, 12) = FKG.myselfG.YongHu

                eSheet.Cells(1, 13) = sKMDMBB
                eSheet.Cells(1, 14) = sZQ

                '从第十六列开始，放置都的模板title
                Dim iStartColumn As Integer
                For iStartColumn = 16 To FKG.myselfG.TemplateTitle.Length - 1 + 16
                    eSheet.Cells(1, iStartColumn) = FKG.myselfG.TemplateTitle(iStartColumn - 16)
                Next

                If Bianhao = "报表" Then
                    fillDatasetTOExcel(dvPrnBaoBiao, iTableRow, iTableCol, 0, iDSHang)
                Else
                    fillDatasetTOExcel(ds, iTableRow, iTableCol, 0, iDSHang)
                End If

                If bImportOutToFile Then
                    eApp.Visible = True
                Else
                    If bPrint Then
                        Try
                            eSheet.PrintOut()
                        Catch ex As Exception
                            MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                        Finally
                            closeExcel()
                        End Try
                    Else
                        eApp.Visible = True
                        Try
                            eSheet.PrintPreview()
                        Catch ex As Exception
                            MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                        Finally
                            closeExcel()
                        End Try
                    End If
                End If
                'closeExcel()

            Else
                '多页处理
                Dim imod As Decimal
                imod = iDSHang Mod iTableMaxHang
                If imod = 0 Then
                    iTotalPage = iDSHang / iTableMaxHang
                Else
                    iTotalPage = Math.Truncate(iDSHang / iTableMaxHang) + 1
                End If

                Dim iHJ(4) As Decimal

                For iCurPage = 1 To iTotalPage
                    eSheet.Cells(1, 9) = iCurPage
                    eSheet.Cells(1, 10) = iTotalPage
                    eSheet.Cells(1, 11) = FKG.myselfG.QiYeMingCheng
                    eSheet.Cells(1, 12) = FKG.myselfG.YongHu

                    eSheet.Cells(1, 13) = sKMDMBB
                    eSheet.Cells(1, 14) = sZQ

                    '从第十六列开始，放置都的模板title
                    Dim iStartColumn As Integer
                    For iStartColumn = 16 To FKG.myselfG.TemplateTitle.Length - 1 + 16
                        eSheet.Cells(1, iStartColumn) = FKG.myselfG.TemplateTitle(iStartColumn - 16)
                    Next

                    If Bianhao = "报表" Then
                        fillDatasetTOExcel(dvPrnBaoBiao, iTableRow, iTableCol, (iCurPage - 1) * iTableMaxHang, iTableMaxHang)
                    Else
                        fillDatasetTOExcel(ds, iTableRow, iTableCol, (iCurPage - 1) * iTableMaxHang, iTableMaxHang)
                    End If
                    Try
                        iHJ(0) = iHJ(0) + eSheet.Cells(1, 5).value
                        iHJ(1) = iHJ(1) + eSheet.Cells(1, 6).value
                        iHJ(2) = iHJ(2) + eSheet.Cells(1, 7).value
                        iHJ(3) = iHJ(3) + eSheet.Cells(1, 8).value
                        If iCurPage = iTotalPage Then
                            eSheet.Cells(1, 5) = iHJ(0)
                            eSheet.Cells(1, 6) = iHJ(1)
                            eSheet.Cells(1, 7) = iHJ(2)
                            eSheet.Cells(1, 8) = iHJ(3)
                        End If
                    Catch ex As Exception

                    End Try

                    If bImportOutToFile Then
                        eApp.Visible = True
                    Else
                        If bPrint Then
                            Try
                                eSheet.PrintOut()
                            Catch ex As Exception
                                MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                            Finally
                                'closeExcel()
                            End Try                        'closeExcel()
                        Else
                            eApp.Visible = True
                            Try
                                eSheet.PrintPreview()
                            Catch ex As Exception
                                MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                            Finally
                                'closeExcel()
                            End Try
                        End If
                    End If
                Next
                closeExcel()

            End If

        Else
            '可变行数
            Dim iDSHang, iCurPage, iTotalPage As Integer
            If Bianhao = "报表" Then
                iDSHang = dvPrnBaoBiao.Count  '数据行数
            Else
                iDSHang = ds.Tables(0).Rows.Count '数据行数
            End If

            If iDSHang <= iTableMinHang Then
                '一共一页
                iCurPage = 1
                iTotalPage = 1

                eSheet.Cells(1, 9) = iCurPage
                eSheet.Cells(1, 10) = iTotalPage
                eSheet.Cells(1, 11) = FKG.myselfG.QiYeMingCheng
                eSheet.Cells(1, 12) = FKG.myselfG.YongHu

                eSheet.Cells(1, 13) = sKMDMBB
                eSheet.Cells(1, 14) = sZQ

                '从第十六列开始，放置都的模板title
                Dim iStartColumn As Integer
                For iStartColumn = 16 To FKG.myselfG.TemplateTitle.Length - 1 + 16
                    eSheet.Cells(1, iStartColumn) = FKG.myselfG.TemplateTitle(iStartColumn - 16)
                Next

                If Bianhao = "报表" Then
                    fillDatasetTOExcel(dvPrnBaoBiao, iTableRow, iTableCol, 0, iDSHang)
                Else
                    fillDatasetTOExcel(ds, iTableRow, iTableCol, 0, iDSHang)
                End If

                If bImportOutToFile Then
                    eApp.Visible = True
                    'eSheet.Visible = Excel.XlSheetVisibility.xlSheetVisible
                Else
                    If bPrint Then
                        Try
                            eSheet.PrintOut()
                        Catch ex As Exception
                            MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                        Finally
                            closeExcel()
                        End Try

                    Else
                        eApp.Visible = True
                        Try
                            eSheet.PrintPreview()
                        Catch ex As Exception
                            MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                        Finally
                            closeExcel()
                        End Try
                    End If
                End If

            ElseIf iDSHang <= iTableMaxHang Then
                '自动加行
                insertRow(iTableRow + 1, iDSHang - iTableMinHang)

                iCurPage = 1
                iTotalPage = 1

                eSheet.Cells(1, 9) = iCurPage
                eSheet.Cells(1, 10) = iTotalPage
                eSheet.Cells(1, 11) = FKG.myselfG.QiYeMingCheng
                eSheet.Cells(1, 12) = FKG.myselfG.YongHu

                eSheet.Cells(1, 13) = sKMDMBB
                eSheet.Cells(1, 14) = sZQ

                '从第十六列开始，放置都的模板title
                Dim iStartColumn As Integer
                For iStartColumn = 16 To FKG.myselfG.TemplateTitle.Length - 1 + 16
                    eSheet.Cells(1, iStartColumn) = FKG.myselfG.TemplateTitle(iStartColumn - 16)
                Next

                If Bianhao = "报表" Then
                    fillDatasetTOExcel(dvPrnBaoBiao, iTableRow, iTableCol, 0, iDSHang)
                Else
                    fillDatasetTOExcel(ds, iTableRow, iTableCol, 0, iDSHang)
                End If

                If bImportOutToFile Then
                    eApp.Visible = True
                Else
                    If bPrint Then
                        Try
                            eSheet.PrintOut()
                        Catch ex As Exception
                            MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                        Finally
                            closeExcel()
                        End Try
                    Else
                        eApp.Visible = True
                        Try
                            eSheet.PrintPreview()
                        Catch ex As Exception
                            MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                        Finally
                            closeExcel()
                        End Try
                    End If
                End If
                'closeExcel()
            Else
                '自动加行，再分页，但每页行数均相等
                insertRow(iTableRow + 1, iTableMaxHang - iTableMinHang)

                Dim imod As Decimal
                imod = iDSHang Mod iTableMaxHang
                If imod = 0 Then
                    iTotalPage = iDSHang / iTableMaxHang
                Else
                    iTotalPage = Math.Truncate(iDSHang / iTableMaxHang) + 1
                End If

                Dim iHJ(4) As Decimal

                For iCurPage = 1 To iTotalPage
                    eSheet.Cells(1, 9) = iCurPage
                    eSheet.Cells(1, 10) = iTotalPage
                    eSheet.Cells(1, 11) = FKG.myselfG.QiYeMingCheng
                    eSheet.Cells(1, 12) = FKG.myselfG.YongHu

                    eSheet.Cells(1, 13) = sKMDMBB
                    eSheet.Cells(1, 14) = sZQ

                    '从第十六列开始，放置都的模板title
                    Dim iStartColumn As Integer
                    For iStartColumn = 16 To FKG.myselfG.TemplateTitle.Length - 1 + 16
                        eSheet.Cells(1, iStartColumn) = FKG.myselfG.TemplateTitle(iStartColumn - 16)
                    Next

                    If Bianhao = "报表" Then
                        fillDatasetTOExcel(dvPrnBaoBiao, iTableRow, iTableCol, (iCurPage - 1) * iTableMaxHang, iTableMaxHang)
                    Else
                        fillDatasetTOExcel(ds, iTableRow, iTableCol, (iCurPage - 1) * iTableMaxHang, iTableMaxHang)
                    End If
                    Try
                        iHJ(0) = iHJ(0) + eSheet.Cells(1, 5).value
                        iHJ(1) = iHJ(1) + eSheet.Cells(1, 6).value
                        iHJ(2) = iHJ(2) + eSheet.Cells(1, 7).value
                        iHJ(3) = iHJ(3) + eSheet.Cells(1, 8).value
                        If iCurPage = iTotalPage Then
                            eSheet.Cells(1, 5) = iHJ(0)
                            eSheet.Cells(1, 6) = iHJ(1)
                            eSheet.Cells(1, 7) = iHJ(2)
                            eSheet.Cells(1, 8) = iHJ(3)
                        End If
                    Catch ex As Exception

                    End Try


                    If bImportOutToFile Then
                        eApp.Visible = True
                        'eSheet.Visible = Excel.XlSheetVisibility.xlSheetVisible
                    Else
                        If bPrint Then
                            Try
                                eSheet.PrintOut()
                            Catch ex As Exception
                                MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                            Finally
                                'closeExcel()
                            End Try
                        Else
                            eApp.Visible = True
                            Try
                                eSheet.PrintPreview()
                            Catch ex As Exception
                                MsgBox("请检查是否未安装打印机", MsgBoxStyle.Exclamation, "提示")
                            Finally
                                'closeExcel()
                            End Try
                        End If
                    End If


                Next
                closeExcel()

            End If
        End If

        Return True
    End Function

    Private Function getPrintDS() As DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        If sLie = "" Then
            sLie = "*"
        End If
        Select Case DanZhengType
            Case 1
                ds = mdb.Reader("select  " & sLie & "  from 库存表 where 编号='" & Bianhao & "' and 显示='true' " & SortCollumn)
            Case 2
                ds = mdb.Reader("select  " & sLie & "  from 工资表 where 编号='" & Bianhao & "' and 显示='true'  " & SortCollumn)
            Case 3
                'ds = mdb.Reader("select  " & sLie & "  from 凭证表 where 编号='" & Bianhao & "' and 显示=true and 年份=" & iNian & " and 月份=" & iYue & " and 凭证号 not like '%-1'")
                '暂时使用，需要修改

                Select Case iPZSCFS
                    Case 1

                        Dim stemp As String
                        Dim sT As String = ""

                        Dim sLiep() As String
                        sLiep = Split(sLie, ",")
                        Dim sDispLie As String = ""
                        For Each stemp In sLiep
                            If stemp.StartsWith("行数") Then
                                'bHang = True
                                'iHangShu = Array.IndexOf(sLie, stemp)
                                'If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                '    stemp = "'行数' as n0000"
                                'Else
                                '    stemp = stemp.Replace("行数", "'行数'")
                                'End If
                                'Continue For
                            End If
                            If stemp.StartsWith("借方数量") Then
                                If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                    stemp = stemp.Replace("借方数量", "sum(借方数量) as n0001")
                                Else
                                    stemp = stemp.Replace("借方数量", "sum(借方数量)")
                                End If
                            End If
                            'If stemp.StartsWith("借方单价") Then
                            '    'stemp = stemp.Replace("借方单价", "sum(借方单价)")
                            'End If
                            If stemp.StartsWith("借方金额") Then
                                If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                    stemp = stemp.Replace("借方金额", "sum(借方金额) as n0003")
                                Else
                                    stemp = stemp.Replace("借方金额", "sum(借方金额)")
                                End If
                                'stemp = stemp.Replace("借方金额", "sum(借方金额)")
                            End If
                            If stemp.StartsWith("贷方数量") Then
                                If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                    stemp = stemp.Replace("贷方数量", "sum(贷方数量) as n0004")
                                Else
                                    stemp = stemp.Replace("贷方数量", "sum(贷方数量)")
                                End If
                                'stemp = stemp.Replace("贷方数量", "sum(贷方数量)")
                            End If
                            'If stemp.StartsWith("贷方单价") Then
                            '    'stemp = stemp.Replace("贷方单价", "sum(贷方单价)")
                            'End If
                            If stemp.StartsWith("贷方金额") Then
                                If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                    stemp = stemp.Replace("贷方金额", "sum(贷方金额) as n0006")
                                Else
                                    stemp = stemp.Replace("贷方金额", "sum(贷方金额)")
                                End If
                                'stemp = stemp.Replace("贷方金额", "sum(贷方金额)")
                            End If
                            sDispLie = sDispLie & stemp & ","

                        Next
                        sDispLie = sDispLie.Remove(sDispLie.Length - 1, 1)

                        Dim sLieG() As String
                        sLieG = Split(sLie, ",")
                        Dim sGroupLie As String = ""
                        For Each stemp In sLieG
                            sT = Split(stemp).GetValue(0)
                            If sT = "行数" OrElse sT = "借方数量" OrElse sT = "借方金额" OrElse sT = "贷方数量" OrElse sT = "贷方金额" Then
                            Else
                                sGroupLie = sGroupLie & sT & ","
                            End If
                        Next
                        sGroupLie = sGroupLie.Remove(sGroupLie.Length - 1, 1)


                        ds = mdb.Reader("select  " & sDispLie & "  from 凭证表 where (编号 ='" & Bianhao & "' or 凭证号 ='" & Bianhao & "') and 显示='true' group by " & sGroupLie & " ")

                    Case 0
                        ds = mdb.Reader("select " & sLie & "  from 凭证表 where 年份=" & iNian & " and (编号 ='" & Bianhao & "' or 凭证号 ='" & Bianhao & "') and 显示='true' and 月份=" & iYue & " order by 行数")
                    Case Else
                        ds = Nothing
                End Select
            Case Else
                ds = Nothing
        End Select
        Return ds
    End Function

    Private Sub DingYiZhi(ByVal iRowHeight As Integer)
        'Dim iHeight As Integer
        ''MsgBox(XlPaperSize.xlPaperUser)

        ''iWidth = eSheet.Range("Print_Area").Width
        'MsgBox(iRowHeight)
    End Sub
End Class
