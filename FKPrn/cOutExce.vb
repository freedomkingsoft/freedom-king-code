﻿Imports System.Reflection

Public Class cOutExce

    Private Sub Export2Excel(ByVal datagridview As Windows.Forms.DataGridView, ByVal captions As Boolean)
        Dim objApp_Late As Object
        Dim objBook_Late As Object
        Dim objBooks_Late As Object
        Dim objSheets_Late As Object
        Dim objSheet_Late As Object
        Dim objRange_Late As Object
        Dim Parameters As Object()

        Dim headers As String() = New String(datagridview.DisplayedColumnCount(True) - 1) {}
        Dim columns As String() = New String(datagridview.DisplayedColumnCount(True) - 1) {}
        Dim colName As String() = New String(datagridview.DisplayedColumnCount(True) - 1) {}

        Dim i As Integer = 0
        Dim c As Integer = 0
        Dim m As Integer = 0

        For c = 0 To datagridview.Columns.Count - 1
            For j As Integer = 0 To datagridview.Columns.Count - 1
                Dim tmpcol As Windows.Forms.DataGridViewColumn = datagridview.Columns(j)
                If tmpcol.DisplayIndex = c Then
                    If tmpcol.Visible Then
                        '不显示的隐藏列初始化为tag＝0 
                        headers(c - m) = tmpcol.HeaderText
                        i = c - m + 65
                        columns(c - m) = Convert.ToString(CChar(ChrW(i)))
                        colName(c - m) = tmpcol.Name
                    Else
                        m += 1
                    End If
                    Exit For
                End If
            Next
        Next

        Try
            ' Get the class type and instantiate Excel. 
            Dim objClassType As Type
            objClassType = Type.GetTypeFromProgID("Excel.Application")
            objApp_Late = Activator.CreateInstance(objClassType)
            'Get the workbooks collection. 
            objBooks_Late = objApp_Late.[GetType]().InvokeMember("Workbooks", BindingFlags.GetProperty, Nothing, objApp_Late, Nothing)
            'Add a new workbook. 
            objBook_Late = objBooks_Late.[GetType]().InvokeMember("Add", BindingFlags.InvokeMethod, Nothing, objBooks_Late, Nothing)
            'Get the worksheets collection. 
            objSheets_Late = objBook_Late.[GetType]().InvokeMember("Worksheets", BindingFlags.GetProperty, Nothing, objBook_Late, Nothing)
            'Get the first worksheet. 
            Parameters = New [Object](0) {}
            Parameters(0) = 1
            objSheet_Late = objSheets_Late.[GetType]().InvokeMember("Item", BindingFlags.GetProperty, Nothing, objSheets_Late, Parameters)

            If captions Then

                ' Create the headers in the first row of the sheet 
                For c = 0 To datagridview.DisplayedColumnCount(True) - 1
                    'Get a range object that contains cell. 
                    Parameters = New [Object](1) {}
                    Parameters(0) = columns(c) & "1"
                    Parameters(1) = Missing.Value
                    objRange_Late = objSheet_Late.[GetType]().InvokeMember("Range", BindingFlags.GetProperty, Nothing, objSheet_Late, Parameters)
                    'Write Headers in cell. 
                    Parameters = New [Object](0) {}
                    Parameters(0) = headers(c)
                    objRange_Late.[GetType]().InvokeMember("Value", BindingFlags.SetProperty, Nothing, objRange_Late, Parameters)
                Next
            End If

            ' Now add the data from the grid to the sheet starting in row 2 
            For i = 0 To datagridview.RowCount - 1
                c = 0
                For Each txtCol As String In colName
                    Dim col As Windows.Forms.DataGridViewColumn = datagridview.Columns(txtCol)
                    If col.Visible Then
                        'Get a range object that contains cell. 
                        Parameters = New [Object](1) {}
                        Parameters(0) = columns(c) + Convert.ToString(i + 2)
                        Parameters(1) = Missing.Value
                        objRange_Late = objSheet_Late.[GetType]().InvokeMember("Range", BindingFlags.GetProperty, Nothing, objSheet_Late, Parameters)
                        'Write Headers in cell. 
                        Parameters = New [Object](0) {}
                        'Parameters[0] = datagridview.Rows[i].Cells[headers[c]].Value.ToString(); 
                        Parameters(0) = datagridview.Rows(i).Cells(col.Name).Value.ToString()
                        objRange_Late.[GetType]().InvokeMember("Value", BindingFlags.SetProperty, Nothing, objRange_Late, Parameters)
                        c += 1

                    End If
                Next
            Next

            'Return control of Excel to the user. 
            Parameters = New [Object](0) {}
            Parameters(0) = True
            objApp_Late.[GetType]().InvokeMember("Visible", BindingFlags.SetProperty, Nothing, objApp_Late, Parameters)
            objApp_Late.[GetType]().InvokeMember("UserControl", BindingFlags.SetProperty, Nothing, objApp_Late, Parameters)
        Catch theException As Exception
            Dim errorMessage As [String]
            errorMessage = "Error: "
            errorMessage = [String].Concat(errorMessage, theException.Message)
            errorMessage = [String].Concat(errorMessage, " Line: ")
            errorMessage = [String].Concat(errorMessage, theException.Source)

            MsgBox(errorMessage, "Error")
        End Try
    End Sub

End Class
