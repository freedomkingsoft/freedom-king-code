﻿Public Class iniKMYEB

    Private Sub iniKMYEB_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        fillLable()
        FillDG()
        Me.Icon = FKG.myselfG.FKIcon
    End Sub

    Private Sub fillLable()
        Me.lblDanWei.Text = Me.lblDanWei.Text & FKG.myselfG.QiYeMingCheng
        Me.lblShijian.Text = "启用时间为：" & FKG.myselfG.dQiYongRiQi.ToShortDateString
    End Sub

    Private Sub FillDG()
        '如果启用月份为1月，则只可以设置年初余额
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sSQL As String = ""
        Dim iSnian As Integer
        Dim iSyue As Integer
        iSnian = FKG.myselfG.dQiYongRiQi.Year
        iSyue = FKG.myselfG.dQiYongRiQi.Month
        Dim i As Integer

        If iSyue = 0 Then
            sSQL = "select kmdm.kmdm as 科目代码,kmdm.kmmc as 科目名称,isNull( kmye.ncjfsl , 0 ) as 年初借方数量,isNull( kmye.ncjfye , 0 ) as 年初借方余额,isNull( kmye.ncdfsl , 0 ) as 年初贷方数量,isNull( kmye.ncdfye , 0 ) as 年初贷方余额 from kmdm left join kmye on kmdm.kmdm=kmye.kmdm where kmdm.sfmj='true' and sfjy='n' and kmdm.nian=" & iSnian & " order by kmdm.kmdm"
        Else
            For i = 1 To 11
                If sSQL = "" Then
                    sSQL = sSQL & "isNull( kmye.jfsl" & i & " , 0 ) as " & i & "月借方发生数量,isNull( kmye.jfje" & i & " , 0 ) as " & i & "月借方发生金额,isNull( kmye.dfsl" & i & " , 0 ) as " & i & "月贷方发生数量,isNull( kmye.dfje" & i & " , 0 ) as " & i & "月贷方发生金额"
                Else
                    sSQL = sSQL & ", isNull( kmye.jfsl" & i & " , 0 ) as " & i & "月借方发生数量,isNull( kmye.jfje" & i & " , 0 ) as " & i & "月借方发生金额,isNull( kmye.dfsl" & i & " , 0 ) as " & i & "月贷方发生数量,isNull( kmye.dfje" & i & " , 0 ) as " & i & "月贷方发生金额"
                End If

            Next

            sSQL = "select kmdm.kmdm as 科目代码,kmdm.kmmc as 科目名称,isNull( kmye.ncjfsl , 0 ) as 年初借方数量,isNull( kmye.ncjfye , 0 ) as 年初借方余额,isNull( kmye.ncdfsl , 0 ) as 年初贷方数量,isNull( kmye.ncdfye , 0 ) as 年初贷方余额, " & sSQL & " from kmdm left join kmye on kmdm.kmdm=kmye.kmdm where kmdm.sfmj='true' and sfjy='n' and kmdm.nian=" & iSnian & " order by kmdm.kmdm"
        End If

        Dim ds As DataSet
        ds = mdb.Reader(sSQL)
        Dim dr As DataRow
        dr = ds.Tables(0).NewRow
        dr.Item(1) = "合计"
        Dim iCol, iRow As Integer
        For iCol = 2 To ds.Tables(0).Columns.Count - 1
            dr.Item(iCol) = 0
            For iRow = 0 To ds.Tables(0).Rows.Count - 2
                dr.Item(iCol) = CType(dr.Item(iCol), Decimal) + CType(ds.Tables(0).Rows(iRow).Item(iCol), Decimal)
            Next

        Next
        ds.Tables(0).Rows.Add(dr)

        Me.dgKMYE.DataSource = ds.Tables(0).DefaultView
        'mdb.DataBind(Me.dgKMYE, sSQL)
        'Dim i As Integer
        For i = (iSyue) * 4 + 2 To 49
            Me.dgKMYE.Columns(i).Visible = False
        Next

        For i = 0 To Me.dgKMYE.Columns.GetColumnCount(Windows.Forms.DataGridViewElementStates.Visible) - 1
            Me.dgKMYE.Columns(i).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Next
        Me.dgKMYE.Columns(0).ReadOnly = True
        Me.dgKMYE.Columns(1).ReadOnly = True

        LieXSsetup()

        Me.dgKMYE.Rows(Me.dgKMYE.RowCount - 1).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.AliceBlue, False)
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '首先确定是否可以更改初始余额
        '一旦启动月份结账，在初始化数据即不可更改
        Dim sSQL As String
        Dim iSnian As Integer
        Dim iSyue As Integer
        iSnian = FKG.myselfG.dQiYongRiQi.Year
        iSyue = FKG.myselfG.dQiYongRiQi.Month

        '最先确定借贷方是否平衡
        If iSyue = 1 Then
            If PingHeng(True) Then
            Else
                Exit Sub
            End If
        Else
            If PingHeng(False) Then
            Else
                Exit Sub
            End If
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim mdbReader As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Try
            mdb.oleConnect.Open()
        Catch ex As Exception
            mdb.oleConnect.Close()
        End Try

        '首先判断帐套是否已经开始使用,但只需要判断启用月份是否结帐即可
        Dim ds As New DataSet
        sSQL = "select jizhang from FKZQ where ksrq<='" & FKG.myselfG.dQiYongRiQi & "' and ZZRQ >='" & FKG.myselfG.dQiYongRiQi & "'"
        mdb.oleCommand.CommandText = sSQL
        mdb.oleDataAdapter.Fill(ds)

        If ds.Tables(0).Rows(0).Item(0) = 0 Then
            '可以进行初始化设置

            '第一步：删除当前年度的科目余额
            sSQL = "delete from kmye where nian=" & iSnian
            mdb.oleCommand.CommandText = sSQL
            mdb.oleCommand.ExecuteNonQuery()

            '    '第二步：插入科目代码和科目余额
            Dim sLie As String = ""
            'sLie = "kmdm,nian,ncjfsl,ncjfye,ncdfsl,ncdfye"
            Dim i As Integer = 0
            'sZhi = "'" & Me.dgKMYE.Rows(i).Cells(0).Value & "'," & iSnian
            For i = 1 To 11
                sLie = sLie & ",jfsl" & i & ",jfje" & i & ",dfsl" & i & ",dfje" & i
            Next

            For i = 0 To Me.dgKMYE.Rows.Count - 2
                sSQL = "insert into kmye (kmdm,nian,ncjfsl,ncjfye,ncdfsl,ncdfye" & sLie & ") values ('" & Me.dgKMYE.Rows(i).Cells(0).Value & "'," & iSnian & "," & Me.dgKMYE.Rows(i).Cells(2).Value & "," & Me.dgKMYE.Rows(i).Cells(3).Value & "," & Me.dgKMYE.Rows(i).Cells(4).Value & "," & Me.dgKMYE.Rows(i).Cells(5).Value & "," & Me.dgKMYE.Rows(i).Cells(6).Value & "," & Me.dgKMYE.Rows(i).Cells(7).Value & "," & Me.dgKMYE.Rows(i).Cells(8).Value & "," & Me.dgKMYE.Rows(i).Cells(9).Value & "," & Me.dgKMYE.Rows(i).Cells(10).Value & "," & Me.dgKMYE.Rows(i).Cells(11).Value & "," & Me.dgKMYE.Rows(i).Cells(12).Value & "," & Me.dgKMYE.Rows(i).Cells(13).Value & "," & Me.dgKMYE.Rows(i).Cells(14).Value & "," & Me.dgKMYE.Rows(i).Cells(15).Value & "," & Me.dgKMYE.Rows(i).Cells(16).Value & "," & Me.dgKMYE.Rows(i).Cells(17).Value & "," & Me.dgKMYE.Rows(i).Cells(18).Value & "," & Me.dgKMYE.Rows(i).Cells(19).Value & "," & Me.dgKMYE.Rows(i).Cells(20).Value & "," & Me.dgKMYE.Rows(i).Cells(21).Value & "," & Me.dgKMYE.Rows(i).Cells(22).Value & "," & Me.dgKMYE.Rows(i).Cells(23).Value & "," & Me.dgKMYE.Rows(i).Cells(24).Value & "," & Me.dgKMYE.Rows(i).Cells(25).Value & "," & Me.dgKMYE.Rows(i).Cells(26).Value & "," & Me.dgKMYE.Rows(i).Cells(27).Value & "," & Me.dgKMYE.Rows(i).Cells(28).Value & "," & Me.dgKMYE.Rows(i).Cells(29).Value & "," & Me.dgKMYE.Rows(i).Cells(30).Value & "," & Me.dgKMYE.Rows(i).Cells(31).Value & "," & Me.dgKMYE.Rows(i).Cells(32).Value & "," & Me.dgKMYE.Rows(i).Cells(33).Value & "," & Me.dgKMYE.Rows(i).Cells(34).Value & "," & Me.dgKMYE.Rows(i).Cells(35).Value & "," & Me.dgKMYE.Rows(i).Cells(36).Value & "," & Me.dgKMYE.Rows(i).Cells(37).Value & "," & Me.dgKMYE.Rows(i).Cells(38).Value & "," & Me.dgKMYE.Rows(i).Cells(39).Value & "," & Me.dgKMYE.Rows(i).Cells(40).Value & "," & Me.dgKMYE.Rows(i).Cells(41).Value & "," & Me.dgKMYE.Rows(i).Cells(42).Value & "," & Me.dgKMYE.Rows(i).Cells(43).Value & "," & Me.dgKMYE.Rows(i).Cells(44).Value & "," & Me.dgKMYE.Rows(i).Cells(45).Value & "," & Me.dgKMYE.Rows(i).Cells(46).Value & "," & Me.dgKMYE.Rows(i).Cells(47).Value & "," & Me.dgKMYE.Rows(i).Cells(48).Value & "," & Me.dgKMYE.Rows(i).Cells(49).Value & ")"
                mdb.oleCommand.CommandText = sSQL
                mdb.oleCommand.ExecuteNonQuery()
            Next

            '第三步：核算非末级科目的初始余额

            Dim sZhi As String = ""
            For i = 1 To 11
                sZhi = sZhi & ",SUM(jfsl" & i & "),SUM(jfje" & i & "),SUM(dfsl" & i & "),SUM(dfje" & i & ")"
                'sZhiDai = sZhiDai & ",0,0,sum(dfsl" & i & ")-sum(jfsl" & i & "),SUM(dfje" & i & ")-SUM(jfje" & i & ")"
            Next

            Dim dsFMJKM As New DataSet
            sSQL = "select KMDM,YEFX from KMDM where SFMJ='0' and nian=" & FKG.myselfG.NianFen
            mdb.oleCommand.CommandText = sSQL
            mdb.oleDataAdapter.SelectCommand = mdb.oleCommand
            mdb.oleDataAdapter.Fill(dsFMJKM)
            Dim sKMDM As String
            For i = 0 To dsFMJKM.Tables(0).Rows.Count - 1
                sKMDM = dsFMJKM.Tables(0).Rows(i).Item(0).ToString
                'If dsFMJKM.Tables(0).Rows(i).Item(1).ToString = "0" Then
                sSQL = "insert into kmye (kmdm,nian,ncjfsl,ncjfye,ncdfsl,ncdfye" & sLie & ")  select '" & sKMDM & "'," & iSnian & ",case when  sum(ncjfye)-sum(ncdfye)>0 then sum(ncjfsl)-sum(ncdfsl) else 0 end ,case when  sum(ncjfye)-sum(ncdfye)>0 then sum(ncjfye)-sum(ncdfye) else 0 end ,case when  sum(ncjfye)-sum(ncdfye)<0 then sum(ncdfsl)-sum(ncjfsl) else 0 end ,case when  sum(ncjfye)-sum(ncdfye)<0 then sum(ncdfye)-sum(ncjfye) else 0 end " & sZhi & " from KMYE where  KMYE.nian=" & iSnian & " and KMYE.KMDM like '" & sKMDM & "%'"
                'Else
                'sSQL = "insert into kmye (kmdm,nian,ncjfsl,ncjfye,ncdfsl,ncdfye" & sLie & ")  select '" & sKMDM & "'," & iSnian & ",sum(ncjfsl)-sum(ncdfsl),sum(ncjfye)-sum(ncdfye),0,0" & sZhiJie & " from KMYE where  KMYE.nian=" & iSnian & " and KMYE.KMDM like '" & sKMDM & "%'"
                'End If

                mdb.oleCommand.CommandText = sSQL
                mdb.oleCommand.ExecuteNonQuery()
            Next

            MsgBox("科目余额初始化成功！", MsgBoxStyle.Information)

        Else
            MsgBox("系统已经开始使用，不能再修改初始余额！", MsgBoxStyle.Information)
            Exit Sub
        End If
        mdb.oleConnect.Close()
    End Sub

    Private Function PingHeng(ByVal bNianChu As Boolean) As Boolean
        Dim icol As Integer
        For icol = 2 To Me.dgKMYE.Columns.Count - 1
            If Me.dgKMYE.Columns(icol).Visible Then
                Select Case (icol - 2) Mod 4
                    Case 0
                        If Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol).Value = Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol + 2).Value Then
                        Else
                            MsgBox("借方和贷方初始余额不平,请检查合计值", MsgBoxStyle.Information, "提示")
                            Return False
                        End If
                    Case 1
                        If Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol).Value = Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol + 2).Value Then
                        Else
                            MsgBox("借方和贷方初始余额不平,请检查合计值", MsgBoxStyle.Information, "提示")
                            Return False
                        End If
                    Case 2
                        If Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol).Value = Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol - 2).Value Then
                        Else
                            MsgBox("借方和贷方初始余额不平,请检查合计值", MsgBoxStyle.Information, "提示")
                            Return False
                        End If
                    Case 3
                        If Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol).Value = Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(icol - 2).Value Then
                        Else
                            MsgBox("借方和贷方初始余额不平,请检查合计值", MsgBoxStyle.Information, "提示")
                            Return False
                        End If

                End Select
            Else
                Return True
            End If
        Next

    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If FKG.myselfG.dQiYongRiQi.Month = 1 Then
            If PingHeng(True) Then
                MsgBox("余额借贷平！", MsgBoxStyle.Information)
            End If
        Else
            If PingHeng(False) Then
                MsgBox("余额借贷平！", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Dim iZhiBeforChange As Decimal
    Private Sub dgKMYE_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgKMYE.CellBeginEdit
        If e.RowIndex = Me.dgKMYE.Rows.Count - 1 Or e.ColumnIndex <= 1 Then
            Exit Sub
        Else
            If IsDBNull(Me.dgKMYE.CurrentCell.Value) Then
                iZhiBeforChange = 0
            Else
                iZhiBeforChange = Me.dgKMYE.CurrentCell.Value
            End If
        End If
    End Sub

    Private Sub dgKMYE_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgKMYE.CellEndEdit
        If e.RowIndex = Me.dgKMYE.Rows.Count - 1 Or e.ColumnIndex <= 1 Then
            Exit Sub
        Else
            Dim dCurrCell As Decimal
            If IsDBNull(Me.dgKMYE.CurrentCell.Value) Then
                dCurrCell = 0
            Else
                dCurrCell = CType(Me.dgKMYE.CurrentCell.Value, Decimal)
            End If

            Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(e.ColumnIndex).Value = Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(e.ColumnIndex).Value - iZhiBeforChange + dCurrCell
        End If

        If IsDBNull(Me.dgKMYE.CurrentCell.Value) Then
            Me.dgKMYE.CurrentCell.Value = 0
        End If
    End Sub

    Private Sub dgKMYE_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgKMYE.DataError
        Me.dgKMYE.CurrentCell.Value = 0
        e.Cancel = False
        'Return True
    End Sub

    Private Sub LieXSsetup()
        Dim sLie As String
        Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
        sLie = xmlR.Read("iniKMlie")

        If sLie = "" Then
            Dim iqsyue As Integer = FKG.myselfG.dQiYongRiQi.Month
            Dim i As Integer
            sLie = "1,,,,1,,,,"
            For i = 0 To 11
                If i < iqsyue Then
                    sLie = sLie & "1,,,,1,,,,1,,,,1,,,,"
                Else
                    sLie = sLie & "2,,,,2,,,,2,,,,2,,,,"
                End If
            Next
            sLie = sLie.Remove(sLie.Length - 1, 1)

            xmlR.SaveInnerText("iniKMlie", sLie)
        End If

        Dim LSet As New FKXSLie.LieSetup(Me.dgKMYE, True, True, sLie)
        LSet.SetLie(False)
    End Sub

    Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
        Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then

            Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
            xmlR.SaveInnerText("iniKMlie", dlg.sSave)
        End If
    End Sub

    Private Sub GZMXZ_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        LieXSsetup()
    End Sub

    Private Sub btnLieSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLieSetup.Click

        Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
        SetupLie(Me.dgKMYE, xmlR.Read("iniKMlie"))

    End Sub


    'Private Sub btnLieSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLieSetup.Click
    '    'Dim sMoRenLie As String = ""

    '    'sMoRenLie = My.Settings.iniKMLie
    '    '   sMoRenLie = ""
    '    SetupLie(Me.dgKMYE, My.Settings.iniKMLie)

    'End Sub

    'Private Sub LieXSsetup()
    '    Dim sLie As String = My.Settings.iniKMLie
    '    'sLie = ""
    '    If sLie = "" Then
    '        Dim iqsyue As Integer = FKG.myselfG.dQiYongRiQi.Month
    '        Dim i As Integer
    '        sLie = "1,,,,1,,,,"
    '        For i = 0 To 11
    '            If i < iqsyue Then
    '                sLie = sLie & "1,,,,1,,,,1,,,,1,,,,"
    '            Else
    '                sLie = sLie & "2,,,,2,,,,2,,,,2,,,,"
    '            End If
    '        Next
    '        sLie = sLie.Remove(sLie.Length - 1, 1)
    '        My.Settings.iniKMLie = sLie
    '        My.Settings.Save()
    '    End If

    '    Dim LSet As New FKXSLie.LieSetup(Me.dgKMYE, False, True, sLie)
    '    LSet.SetLie()
    'End Sub

    'Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
    '    Dim dlg As New FKXSLie.LieSetup(dgv, False, True, sMorenlie)
    '    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
    '        My.Settings.iniKMLie = dlg.sSave
    '        My.Settings.Save()
    '    End If
    'End Sub
End Class