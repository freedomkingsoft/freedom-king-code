Imports System.Windows.Forms

Public Class MXTJ

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        '首先进行对条件的验证
        If (Me.myNian.Value = FKG.myselfG.dQiYongRiQi.Year And Me.myYue.Value >= FKG.myselfG.dQiYongRiQi.Month) OrElse Me.myNian.Value > FKG.myselfG.dQiYongRiQi.Year Then
            If Me.myYue.Value <= Me.myYue2.Value Then

            Else
                MsgBox("截止月份不能小于开始月份", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
        Else
            MsgBox("开始时间不能早于启用日期", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If
        If Me.myKMDM.Text = "" Then
            MsgBox("请选择一个科目", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub MXTJ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.myNian.Value = FKG.myselfG.NianFen
        If myNian.Value = FKG.myselfG.dQiYongRiQi.Year Then
            Me.myYue.Value = FKG.myselfG.dQiYongRiQi.Month
        End If
        Me.myYue2.Value = FKG.myselfG.YueFen

        Me.ActiveControl = Me.myKMDM
    End Sub

    Private Sub myKMDM_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles myKMDM.KeyUp
        If e.KeyCode = Keys.Enter Then
            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            'If mdb.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) >= 4 Then
            '    Me.ActiveControl = Me.OK_Button
            'Else
            '    If Me.myKMDM.Text > 1121 Then
            '        Me.myKMDM.Clear()
            '        Me.ActiveControl = myKMDM
            '    Else
            Me.ActiveControl = Me.OK_Button

            '    End If
            'End If
        End If
        bValided = False
    End Sub

    Dim bValided As Boolean
    Private Sub myKMDM_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles myKMDM.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            getKMDM()
        End If
    End Sub

    Private Sub myKMDM_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles myKMDM.Validating
        If bValided Then
            Exit Sub
        End If

        If Me.myKMDM.Text = "" Then
        Else
            Dim mdb2 As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If mdb2.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) >= 4 Then
                'Me.ActiveControl = Me.OK_Button
            Else
                If Me.myKMDM.Text > 1121 Then
                    Me.myKMDM.Clear()
                    e.Cancel = True

                    Exit Sub
                    'Me.ActiveControl = myKMDM
                Else
                    'Me.ActiveControl = Me.OK_Button

                End If
            End If

            Dim sKMQM As String
            sKMQM = FKF.FKF.getKMQM(" KMDM.SFJY='n' and KMDM='" & Me.myKMDM.Text & "'", False)
            If sKMQM = "" Then
                getKMDM()
            Else
                Me.myKMQM.Text = sKMQM
                '确定是否显示数量
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                If mdb.Reader("select zygs from kmdm where nian=" & FKG.myselfG.NianFen & " and kmdm='" & Me.myKMDM.Text & "'").Tables(0).Rows(0).Item(0) = 6 Then
                    Me.myXSSL.Enabled = False
                    Me.myXSSL.Checked = False
                Else
                    Me.myXSSL.Enabled = True
                    Me.myXSSL.Checked = True
                End If
            End If
        End If
    End Sub

    Private Sub getKMDM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dlg As FKKM.FKXKM
        If mdb.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) >= 4 Then
            dlg = New FKKM.FKXKM(" sfjy='n'", False)
            dlg.sKey = Me.myKMDM.Text
            dlg.ShowDialog()
        Else
            dlg = New FKKM.FKXKM(" sfjy='n' and KMDM<='1121'", False)
            dlg.sKey = Me.myKMDM.Text
            dlg.ShowDialog()
        End If
        Me.myKMDM.Text = dlg.myKM.sKMDM
        Me.myKMQM.Text = dlg.myKM.sKMQM
        If dlg.myKM.iZYGS = 6 Then
            Me.myXSSL.Enabled = False
            Me.myXSSL.Checked = False
        Else
            Me.myXSSL.Enabled = True
            Me.myXSSL.Checked = True
        End If
        bValided = True
    End Sub
End Class
