Public Class KMMXZ
    Dim iCNian As Integer
    Dim iCYue As Integer
    Dim iCYue2 As Integer
    'Dim iCCangKuID As Integer

    Dim bKM As Boolean   '是否来自科目余额表
    Dim bLoad As Boolean = False '该变量用于表明当前窗体还没有真正加载，防止在加载之前由于SizeChange而引起不必要的动作。
    Private Sub KMMXZ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If bKM Then

            FilltscKM()
            LieXSsetup(False)

            setColor()
        Else
            setCase()
        End If
        Me.Icon = FKG.myselfG.FKIcon
        bLoad = True
    End Sub

    Dim bXSSLLie As Boolean = False  '指示当前设定选中了显示数量
    Private Sub setCase()
        Dim dlg As New MXTJ
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            bXSSLLie = dlg.myXSSL.Checked

            FillDG(dlg.myKMDM.Text, dlg.myNian.Value, dlg.myYue.Value, dlg.myYue2.Value, dlg.myWJZ.Checked, bXSSLLie)

            Me.lblKM.Text = "科目: " & dlg.myKMDM.Text & "  " & dlg.myKMQM.Text

            Me.Label2.Text = "公司: " & FKG.myselfG.QiYeMingCheng
            Me.Label3.Text = "日期: " & dlg.myNian.Value & "年" & dlg.myYue.Value & "月 -- " & dlg.myYue2.Value & "月"

            Me.Label4.Text = "操作员: " & FKG.myselfG.YongHu
            Me.Label5.Text = "时间:　" & Today.ToShortDateString
            FilltscKM()
            LieXSsetup(bXSSLLie)

            setColor()
        Else
            Me.Close()
        End If
    End Sub


    Dim indexHJ(24) As Integer
    Dim iHj As Integer = 0

    Dim sKMDMMX As String '启动科目日记账时使用

    Public Sub FillDG(ByVal sKMDM As String, ByVal iNian As Integer, ByVal iBYue As Integer, ByVal iEYue As Integer, Optional ByVal bAll As Boolean = True, Optional ByVal bSL As Boolean = False, Optional ByVal bFromTscKMDM As Boolean = False)
        iHj = 0

        sKMDMMX = sKMDM

        iCNian = iNian
        iCYue = iBYue
        iCYue2 = iEYue
        'iCCangKuID = iCangkuID

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If bKM Then
            Dim sKMQM As String
            sKMQM = mdb.Reader("select KMQM from KMDM where KMDM = '" & sKMDM & "' and Nian=" & iNian).Tables(0).Rows(0).Item(0)

            Me.lblKM.Text = "科目: " & sKMDM & "  " & sKMQM
            Me.Label2.Text = "公司: " & FKG.myselfG.QiYeMingCheng
            Me.Label3.Text = "日期: " & iNian & "年" & iBYue & "月 -- " & iEYue & "月"

            Me.Label4.Text = "操作员: " & FKG.myselfG.YongHu
            Me.Label5.Text = "时间:　" & Today.ToShortDateString
        End If

        Dim sCase As String
        If bAll Then
            sCase = ""
        Else
            sCase = " and 记帐 <> ''"
        End If

        Dim sSQL As String
        Dim dtAll As New DataTable
        Dim dsQC As New DataSet

        Dim dr As DataRow
        Dim iNianLJ(9), iYueHJ(9) As Double
        Dim i As Integer
        For i = 0 To 9
            iNianLJ(i) = 0
            iYueHJ(i) = 0
        Next

        Dim iYue As Int16
        Try
            mdb.oleConnect.Open()
        Catch ex As Exception
            MsgBox(ex.ToString)
            mdb.oleConnect.Close()
            Exit Sub
        End Try
        Dim iStartYue As Integer
        If iNian = FKG.myselfG.dQiYongRiQi.Year Then
            iStartYue = FKG.myselfG.dQiYongRiQi.Month
        Else
            iStartYue = 1
        End If

        For iYue = iStartYue To iEYue
            Dim ds As New DataSet
            '取出凭证明细
            '根据“仓库”字段来确定是否要将同一张凭证上，同一科目进行汇总
            '此处只需增加对“仓库”字段进行分组汇总
            'sSQL = "SELECT 月份 AS 月, day(日期) AS 日, 凭证号, 摘要, sum(借方数量) as 借方数量, IIF(sum(借方数量)=0,0,sum(借方金额)/sum(借方数量)) AS 借方单价, sum(借方金额) as 借方金额, sum(贷方数量) as 贷方数量,  iif(sum(贷方数量)=0,0,sum(贷方金额)/sum(贷方数量)) AS 贷方单价,sum(贷方金额) as 贷方金额,'' as [借/贷],1.11 as 结余数量,1.11 as 结余单价,1.11 as 结余金额 FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' and 月份=" & iYue & sCase & " group by 月份,日期,凭证号,摘要"
            sSQL = "SELECT 月份 AS 月, day(日期) AS 日, 凭证号, 摘要, sum(借方数量) as 借方数量, case when  sum(借方数量)=0 then 0 else sum(借方金额)/sum(借方数量) end  AS 借方单价, sum(借方金额) as 借方金额, sum(贷方数量) as 贷方数量,  case when  sum(贷方数量)=0 then 0 else sum(贷方金额)/sum(贷方数量) end  AS 贷方单价,sum(贷方金额) as 贷方金额,'' as [借/贷],1.111 as 结余数量,1.11 as 结余单价,1.11 as 结余金额 FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' and 月份=" & iYue & sCase & "  and (借方金额<>0 or 贷方金额<>0)  group by 月份,日期,凭证号,摘要,仓库"
            '2012.9.20修正借贷方均为0时仍显示的问题，在上面的sql语句中增加 and (借方金额<>0 or 贷方金额<>0) 条件
            Try
                mdb.oleCommand.CommandText = sSQL

                mdb.oleDataAdapter.Fill(ds)
            Catch ex As Exception
                MsgBox(ex.ToString)
                mdb.oleConnect.Close()
                Exit Sub
            End Try

            'ds = mdb.Reader(sSQL)
            dtAll.Merge(ds.Tables(0))

            '取出年初结余作为月初余额
            If iYue = iStartYue Then
                If iYue = 1 Then
                    'sSQL = "select isNull( 年初借方数量 , 0 ) as 借方数量, case when  借方数量=0 then 0 else 借方金额/借方数量 end  AS 借方单价, isNull( 年初借方余额 , 0 ) as 借方金额, isNull( 年初贷方数量 , 0 ) as 贷方数量, case when  贷方数量=0 then 0 else 贷方金额/贷方数量 end  AS 贷方单价, isNull( 年初贷方余额 , 0 ) as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"

                    sSQL = "select isNull( ncjfsl , 0 ) as 借方数量, case when  isNull( ncjfsl , 0 )=0 then 0 else ncjfye /ncjfsl end  AS 借方单价, isNull( ncjfye, 0 ) as 借方金额, isNull( ncdfsl, 0 ) as 贷方数量, case when  isNull( ncdfsl, 0 )=0 then 0 else isNull( ncdfye, 0 )/isNull( ncdfsl, 0 ) end  AS 贷方单价, isNull( ncdfye, 0 ) as 贷方金额 from KMYE  where NIAN=" & iNian & " and KMDM='" & sKMDM & "'"
                Else
                    'sSQL = "select (isNull( 年初借方数量+[" & iYue - 1 & "月借方累计发生数量] , 0 ) ) as 借方数量, case when  借方数量=0 then 0 else 借方金额/借方数量 end  AS 借方单价, (isNull( 年初借方余额+[" & iYue - 1 & "月借方累计发生额] , 0 ) ) as 借方金额,(isNull( 年初贷方数量+[" & iYue - 1 & "月贷方累计发生数量] , 0 ) ) as 贷方数量, case when  贷方数量=0 then 0 else 贷方金额/贷方数量 end  AS 贷方单价, (isNull( 年初贷方余额+[" & iYue - 1 & "月贷方累计发生额] , 0 ) ) as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"
                    sSQL = "select isNull( ncjfsl , 0 ) as 借方数量, case when  isNull( ncjfsl , 0 )=0 then 0 else ncjfye /ncjfsl end  AS 借方单价, isNull( ncjfye, 0 ) as 借方金额, isNull( ncdfsl, 0 ) as 贷方数量, case when  isNull( ncdfsl, 0 )=0 then 0 else isNull( ncdfye, 0 )/isNull( ncdfsl, 0 ) end  AS 贷方单价, isNull( ncdfye, 0 ) as 贷方金额 from KMYE  where NIAN=" & iNian & " and KMDM='" & sKMDM & "'"

                    '所有的期初数据都保存到年初余额当中

                End If

                Try
                    'mdb.oleConnect.Open()
                    mdb.oleCommand.CommandText = sSQL

                    mdb.oleDataAdapter.Fill(dsQC)
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    mdb.oleConnect.Close()
                    Exit Sub
                End Try
                dsQC = mdb.Reader(sSQL)

                'dsQC = mdb.ReadStoredProcedure(FKG.myselfG.asasR, "kmyeb", "@iNian," & iNian & ";@iYue," & iYue & ";@sMinKM," & sKMDM & ";@sMaxKM," & sKMDM & "")

                Dim iQC(5) As Double
                If IsNothing(dsQC) OrElse dsQC.Tables(0).Rows.Count = 0 Then
                    iQC(0) = 0
                    iQC(1) = 0
                    iQC(2) = 0
                    iQC(3) = 0
                    iQC(4) = 0
                    iQC(5) = 0
                Else
                    iQC(0) = dsQC.Tables(0).Rows(0).Item(0)
                    iQC(1) = dsQC.Tables(0).Rows(0).Item(1)
                    iQC(2) = dsQC.Tables(0).Rows(0).Item(2)
                    iQC(3) = dsQC.Tables(0).Rows(0).Item(3)
                    iQC(4) = dsQC.Tables(0).Rows(0).Item(4)
                    iQC(5) = dsQC.Tables(0).Rows(0).Item(5)
                End If
                dr = dtAll.NewRow

                dr.Item(0) = iYue
                dr.Item(1) = DBNull.Value
                dr.Item(2) = DBNull.Value
                dr.Item(3) = "月初余额"
                dr.Item(4) = DBNull.Value
                dr.Item(5) = DBNull.Value
                dr.Item(6) = DBNull.Value
                dr.Item(7) = DBNull.Value
                dr.Item(8) = DBNull.Value
                dr.Item(9) = DBNull.Value

                If iQC(2) > iQC(5) Then
                    dr.Item(10) = "借"
                    dr.Item(11) = iQC(0) - iQC(3)
                ElseIf iQC(2) < iQC(5) Then
                    dr.Item(10) = "贷"
                    dr.Item(11) = iQC(3) - iQC(0)
                Else
                    dr.Item(10) = "平"
                    dr.Item(11) = iQC(0) - iQC(3)
                End If
                dr.Item(13) = System.Math.Abs(iQC(2) - iQC(5))
                If dr.Item(11) = 0 Then
                    dr.Item(12) = 0
                Else
                    dr.Item(12) = Math.Abs(dr.Item(13) / dr.Item(11))
                End If
                '以上为月初余额
                dtAll.Rows.InsertAt(dr, 0)
                indexHJ(iHj) = 0
                iHj = 1

            End If

            '设置本月合计行
            sSQL = "SELECT isNull( sum(借方数量) , 0 ) ,isNull( sum(借方金额) , 0 ) , isNull( sum(贷方数量) , 0 ) ,isNull( sum(贷方金额) , 0 ) FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' and 月份=" & iYue & sCase
            dsQC.Clear()
            dsQC = New DataSet
            Try
                'mdb.oleConnect.Open()
                mdb.oleCommand.CommandText = sSQL

                mdb.oleDataAdapter.Fill(dsQC)
            Catch ex As Exception
                MsgBox(ex.ToString)
                mdb.oleConnect.Close()
                Exit Sub
            End Try
            'dsQC = mdb.Reader(sSQL)
            If IsNothing(dsQC) OrElse dsQC.Tables(0).Rows.Count = 0 Then
                iYueHJ(0) = 0
                iYueHJ(1) = 0
                iYueHJ(2) = 0
                iYueHJ(3) = 0
            Else
                iYueHJ(0) = dsQC.Tables(0).Rows(0).Item(0)
                iNianLJ(0) = iNianLJ(0) + iYueHJ(0)
                iYueHJ(1) = dsQC.Tables(0).Rows(0).Item(1)
                iNianLJ(1) = iNianLJ(1) + iYueHJ(1)
                iYueHJ(2) = dsQC.Tables(0).Rows(0).Item(2)
                iNianLJ(2) = iNianLJ(2) + iYueHJ(2)
                iYueHJ(3) = dsQC.Tables(0).Rows(0).Item(3)
                iNianLJ(3) = iNianLJ(3) + iYueHJ(3)
            End If
            dr = dtAll.NewRow

            dr.Item(0) = iYue
            dr.Item(1) = DBNull.Value
            dr.Item(2) = DBNull.Value
            dr.Item(3) = "本月合计"
            dr.Item(4) = iYueHJ(0)
            dr.Item(5) = DBNull.Value
            dr.Item(6) = iYueHJ(1)
            dr.Item(7) = iYueHJ(2)
            dr.Item(8) = DBNull.Value
            dr.Item(9) = iYueHJ(3)
            dtAll.Rows.Add(dr)

            indexHJ(iHj) = dtAll.Rows.Count - 1
            iHj = iHj + 1

            '本年合计
            dr = dtAll.NewRow

            dr.Item(0) = iYue
            dr.Item(1) = DBNull.Value
            dr.Item(2) = DBNull.Value
            dr.Item(3) = "本年累计"
            dr.Item(4) = iNianLJ(0)
            dr.Item(5) = DBNull.Value
            dr.Item(6) = iNianLJ(1)
            dr.Item(7) = iNianLJ(2)
            dr.Item(8) = DBNull.Value
            dr.Item(9) = iNianLJ(3)
            dtAll.Rows.Add(dr)

            indexHJ(iHj) = dtAll.Rows.Count - 1
            iHj = iHj + 1

        Next
        mdb.oleConnect.Close()

        Dim dv As DataView
        dv = dtAll.DefaultView

        '计算余额
        Dim iJYShu, iJYE As Double
        dv.RowFilter = "摘要<>'本年累计'"
        Dim n As Int16
        For n = 1 To dv.Count - 1
            If dv.Item(n).Item("摘要") = "本月合计" Then
                dv.Item(n).Item(13) = dv.Item(n - 1).Item(13)
                dv.Item(n).Item(12) = dv.Item(n - 1).Item(12)
                dv.Item(n).Item(11) = dv.Item(n - 1).Item(11)
                dv.Item(n).Item(10) = dv.Item(n - 1).Item(10)
            Else
                If dv.Item(n - 1).Item(10) = "借" Then
                    iJYE = dv.Item(n - 1).Item(13) + dv.Item(n).Item(6) - dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) + dv.Item(n).Item(4) - dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                        dv.Item(n).Item(10) = "借"
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "贷"
                        dv.Item(n).Item(13) = Math.Abs(iJYE)
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = 0
                        dv.Item(n).Item(11) = iJYShu

                    End If

                ElseIf dv.Item(n - 1).Item(10) = "贷" Then
                    iJYE = dv.Item(n - 1).Item(13) - dv.Item(n).Item(6) + dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) - dv.Item(n).Item(4) + dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                        dv.Item(n).Item(10) = "贷"
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "借"
                        dv.Item(n).Item(13) = Math.Abs(iJYE)
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = 0
                        dv.Item(n).Item(11) = iJYShu

                    End If

                ElseIf dv.Item(n - 1).Item(10) = "平" Then
                    iJYE = dv.Item(n - 1).Item(13) + dv.Item(n).Item(6) - dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) + dv.Item(n).Item(4) - dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(10) = "借"
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "贷"
                        dv.Item(n).Item(13) = -iJYE
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu

                    End If
                Else
                    MsgBox("wrong")
                End If

                If dv.Item(n).Item(11) = 0 Then
                    dv.Item(n).Item(12) = 0
                Else
                    dv.Item(n).Item(12) = Math.Abs(dv.Item(n).Item(13) / dv.Item(n).Item(11))
                End If
            End If
        Next

        dv.RowFilter = Nothing

        '筛选所选月份
        If iBYue > 1 Then
            '已在上面的sql语句中修正
            '2012.9.20修正。解决借贷方均为0时仍显示的问题。
            dv.RowFilter = "月>=" & iBYue & " or (月=" & iBYue - 1 & " and 摘要='本月合计')"

            'dv.RowFilter = "(月>=" & iBYue & " or (月=" & iBYue - 1 & " and 摘要='本月合计')) and (借方金额<>0 or 贷方金额<>0)" '2012.9.20修正。解决借贷方均为0时仍显示的问题。

            dv.Item(0).Item("月") = iBYue
            dv.Item(0).Item("摘要") = "月初余额"

            '清除上月的本月合计数据，否则将显示到月初余额行
            dv.Item(0).Item("借方数量") = DBNull.Value
            dv.Item(0).Item("借方单价") = DBNull.Value
            dv.Item(0).Item("借方金额") = DBNull.Value
            dv.Item(0).Item("贷方数量") = DBNull.Value
            dv.Item(0).Item("贷方单价") = DBNull.Value
            dv.Item(0).Item("贷方金额") = DBNull.Value
        End If

        '不显示0
        Dim irow, icol As Integer
        For irow = 0 To dv.Count - 1
            For icol = 4 To dv.Table.Columns.Count - 1
                If Not icol = 10 AndAlso Not dv.Item(irow).Item(icol).ToString = "" AndAlso dv.Item(irow).Item(icol) = 0 Then
                    dv.Item(irow).Item(icol) = DBNull.Value
                End If
            Next
        Next

        Me.dgMX.DataSource = dv

        If bFromTscKMDM Then
            Exit Sub
            '如果是通过下拉框改变的科目代码，则不进行列的调整
        End If

        If bSL Then
        Else
            Me.dgMX.Columns(4).Visible = False
            Me.dgMX.Columns(5).Visible = False
            Me.dgMX.Columns(7).Visible = False
            Me.dgMX.Columns(8).Visible = False
            Me.dgMX.Columns(11).Visible = False
            Me.dgMX.Columns(12).Visible = False
        End If
        'MsgBox((Now - t).ToString)

        TiaoZhengLie()
    End Sub

    Private Sub setColor()
        Dim iRow As Integer
        For iRow = 0 To Me.dgMX.RowCount - 1
            If IsDBNull(Me.dgMX.Rows(iRow).Cells(1).Value) Then
                Me.dgMX.Rows(iRow).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.AliceBlue, False)
            End If
        Next
    End Sub
    Private Sub TiaoZhengLie()
        With Me.dgMX

            Dim i As Integer
            For i = 0 To .Columns.Count - 1
                .Columns(i).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Next

        End With

    End Sub

    Public Sub New(Optional ByVal bKMDM As Boolean = False)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        bKM = bKMDM
    End Sub

    Private Sub 关闭_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 关闭.Click
        Me.Close()
    End Sub

    Dim th As Threading.Thread
    Private Sub 导出_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出.Click
        th = New Threading.Thread(AddressOf FKPOUT)
        th.Start()
    End Sub

    Private Sub FKPOUT()
        Dim dlg As New FKPrn.ImportOut
        dlg.Importout(Me.dgMX)
        'dlg.DataGridviewShowToExcel(Me.dgMX, True)
    End Sub

    Private Sub KMYE_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsNothing(th) Then
        Else
            If th.IsAlive Then
                MsgBox("正在向Excel导出数据，请稍候...", MsgBoxStyle.Information, My.Application.Info.Title)
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub KMMXZ_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        GC.Collect()
    End Sub

    Private Sub 列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 列设置.Click
        SetupLie(Me.dgMX, "")
    End Sub


    Private Sub LieXSsetup(ByVal bXSSL As Boolean)
        Dim sLie As String
        Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
        sLie = xmlR.Read("iniKMMXlie")

        Dim LSet As New FKXSLie.LieSetup(Me.dgMX, False, True, sLie)
        'Dim LSet As New FKXSLie.LieSetup(Me.dgMX, True, True, sLie)
        LSet.SetLie(bXSSL)
    End Sub

    Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
        Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then

            Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
            xmlR.SaveInnerText("iniKMMXlie", dlg.sSave)
        End If
    End Sub

    Private Sub GZMXZ_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        If bLoad Then
            LieXSsetup(bXSSLLie)
        End If
    End Sub

  
  
    'Private Sub 列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 列设置.Click
    '    SetupLie(Me.dgMX, My.Settings.iniKMMXlie)
    'End Sub

    'Private Sub LieXSsetup(ByVal sLie As String)

    '    Dim LSet As New FKXSLie.LieSetup(Me.dgMX, True, True, sLie)
    '    LSet.SetLie()
    'End Sub

    'Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
    '    Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
    '    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
    '        My.Settings.iniKMMXlie = dlg.sSave
    '        My.Settings.Save()
    '    End If
    'End Sub

    'Private Sub KMMXZ_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
    '    LieXSsetup(My.Settings.iniKMMXlie)
    'End Sub

    Private Sub tscKMDM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tscKMDM.SelectedIndexChanged
        If bValided Then
            Exit Sub
        End If

        Me.lblKM.Text = Me.tscKMDM.Text

        FillDG(FKF.FKF.getKMDMfromQM(Me.tscKMDM.Text), iCNian, iCYue, iCYue2, True, False, True)
        setColor()
    End Sub

    Private Sub FilltscKM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        If mdb.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) >= 4 Then
            ds = mdb.Reader("select KMDM + '   ' + KMMC from KMDM where  sfjy='n' and Nian=" & iCNian & " order by KMDM")
        Else
            ds = mdb.Reader("select KMDM + '   ' + KMMC from KMDM where  sfjy='n' and kmdm <='1121' and Nian=" & iCNian & " order by KMDM")
        End If

        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.tscKMDM.Items.Add(ds.Tables(0).Rows(i).Item(0).ToString)
        Next

    End Sub

    Private Sub 查看凭证_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 查看凭证.Click
        '   If IsNothing(Me.dgMX.CurrentRow.Cells(1).Value) Then
        If IsDBNull(Me.dgMX.CurrentRow.Cells(1).Value) Then
            Exit Sub
        End If

        Dim dlg As New FKSHPZ.FKSHPZ(True, iCNian)

        dlg.dYEBYue = Me.dgMX.CurrentRow.Cells(0).Value
        dlg.sYEBPZH = Me.dgMX.CurrentRow.Cells(2).Value

        dlg.ShowDialog()
    End Sub

    Private Sub 日记帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 日记帐.Click
        'If IsDBNull(Me.dgMX.CurrentRow.Cells(1).Value) Then
        '    Exit Sub
        'End If
        'If Me.dgYE.CurrentRow.Index = -1 OrElse Me.dgYE.CurrentRow.Index = Me.dgYE.Rows.Count - 1 Then
        '    Exit Sub
        'End If
        Dim dlg As New FKKMYEB.KMRJZ(True)
        dlg.FillDG(sKMDMMX, iCNian, iCYue, iCYue2)
        dlg.ShowDialog()
    End Sub

    Private Sub dgMX_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgMX.CellDoubleClick
        查看凭证_Click(sender, e)
    End Sub

    Private Sub tscKMDM_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tscKMDM.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Enter Then
            Me.tscKMDM.DroppedDown = True
            If Me.tscKMDM.SelectedIndex = -1 Then
                '此处启动利用助记符来查询科目的功能
                bValided = True
                getKMDM()
                bValided = False
                If Me.tscKMDM.Text <> "   " OrElse IsNothing(Me.tscKMDM.Text) Then
                    tscKMDM_SelectedIndexChanged(sender, e)
                Else
                    Exit Sub
                End If
            Else
                tscKMDM_SelectedIndexChanged(sender, e)
            End If
        End If
    End Sub

    Dim bValided As Boolean = False
    Private Sub getKMDM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim sCaseKM As String
        If mdb.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) >= 4 Then
            sCaseKM = " sfjy='n'"
        Else
            sCaseKM = " sfjy='n' and kmdm <='1121' "
        End If

        Dim dlg As New FKKM.FKXKM(sCaseKM, False)
        dlg.sKey = Me.tscKMDM.Text
        dlg.ShowDialog()
        Me.tscKMDM.Text = dlg.myKM.sKMDM & "   " & dlg.myKM.sKMMC
    End Sub
End Class