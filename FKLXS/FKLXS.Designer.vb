﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKLXS
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FKLXS))
        Me.Label2 = New System.Windows.Forms.Label
        Me.dgvKX = New System.Windows.Forms.DataGridView
        Me.可选列 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvXD = New System.Windows.Forms.DataGridView
        Me.XDID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.选定列 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.可选别名 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.合计 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.列宽 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.小数位 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnDel = New System.Windows.Forms.Button
        Me.btnTop = New System.Windows.Forms.Button
        Me.btnBotton = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        CType(Me.dgvKX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvXD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "当前设置的为："
        '
        'dgvKX
        '
        Me.dgvKX.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvKX.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvKX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKX.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.可选列})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvKX.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvKX.Location = New System.Drawing.Point(14, 95)
        Me.dgvKX.Name = "dgvKX"
        Me.dgvKX.RowHeadersVisible = False
        Me.dgvKX.RowTemplate.Height = 23
        Me.dgvKX.Size = New System.Drawing.Size(142, 333)
        Me.dgvKX.TabIndex = 4
        '
        '可选列
        '
        Me.可选列.HeaderText = "可选列"
        Me.可选列.Name = "可选列"
        Me.可选列.ReadOnly = True
        Me.可选列.Width = 120
        '
        'dgvXD
        '
        Me.dgvXD.AllowUserToAddRows = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvXD.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvXD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvXD.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.XDID, Me.选定列, Me.可选别名, Me.合计, Me.列宽, Me.小数位})
        Me.dgvXD.Location = New System.Drawing.Point(199, 95)
        Me.dgvXD.Name = "dgvXD"
        Me.dgvXD.RowHeadersVisible = False
        Me.dgvXD.RowTemplate.Height = 23
        Me.dgvXD.Size = New System.Drawing.Size(378, 333)
        Me.dgvXD.TabIndex = 4
        '
        'XDID
        '
        Me.XDID.HeaderText = "XDID"
        Me.XDID.Name = "XDID"
        Me.XDID.Visible = False
        '
        '选定列
        '
        Me.选定列.HeaderText = "选定列"
        Me.选定列.Name = "选定列"
        Me.选定列.ReadOnly = True
        Me.选定列.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.选定列.Width = 115
        '
        '可选别名
        '
        Me.可选别名.HeaderText = "可选别名"
        Me.可选别名.Name = "可选别名"
        Me.可选别名.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        '合计
        '
        Me.合计.HeaderText = "合计"
        Me.合计.Name = "合计"
        Me.合计.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.合计.ToolTipText = "设定1为不合计；2为合计；3为显示""合计""。"
        Me.合计.Width = 40
        '
        '列宽
        '
        Me.列宽.HeaderText = "列宽"
        Me.列宽.Name = "列宽"
        Me.列宽.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.列宽.ToolTipText = "输入整个表格宽度的百分数"
        Me.列宽.Width = 50
        '
        '小数位
        '
        Me.小数位.HeaderText = "小数位"
        Me.小数位.Name = "小数位"
        Me.小数位.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.小数位.ToolTipText = "不超过6位"
        Me.小数位.Width = 55
        '
        'btnAdd
        '
        Me.btnAdd.AutoSize = True
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(163, 145)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(30, 26)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.AutoSize = True
        Me.btnDel.Image = CType(resources.GetObject("btnDel.Image"), System.Drawing.Image)
        Me.btnDel.Location = New System.Drawing.Point(163, 228)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(30, 26)
        Me.btnDel.TabIndex = 5
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'btnTop
        '
        Me.btnTop.AutoSize = True
        Me.btnTop.Image = CType(resources.GetObject("btnTop.Image"), System.Drawing.Image)
        Me.btnTop.Location = New System.Drawing.Point(583, 145)
        Me.btnTop.Name = "btnTop"
        Me.btnTop.Size = New System.Drawing.Size(30, 26)
        Me.btnTop.TabIndex = 5
        Me.btnTop.UseVisualStyleBackColor = True
        '
        'btnBotton
        '
        Me.btnBotton.AutoSize = True
        Me.btnBotton.Image = CType(resources.GetObject("btnBotton.Image"), System.Drawing.Image)
        Me.btnBotton.Location = New System.Drawing.Point(583, 228)
        Me.btnBotton.Name = "btnBotton"
        Me.btnBotton.Size = New System.Drawing.Size(30, 26)
        Me.btnBotton.TabIndex = 5
        Me.btnBotton.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(243, 447)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(147, 33)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "保存设置 (&S)"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(465, 447)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(78, 33)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "关闭 (&X)"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'FKLXS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(625, 503)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.btnBotton)
        Me.Controls.Add(Me.btnTop)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.dgvXD)
        Me.Controls.Add(Me.dgvKX)
        Me.Controls.Add(Me.Label2)
        Me.MaximizeBox = False
        Me.Name = "FKLXS"
        Me.Text = "设置显示列"
        CType(Me.dgvKX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvXD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvKX As System.Windows.Forms.DataGridView
    Friend WithEvents dgvXD As System.Windows.Forms.DataGridView
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents 可选列 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents btnTop As System.Windows.Forms.Button
    Friend WithEvents btnBotton As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents XDID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 选定列 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 可选别名 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 合计 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 列宽 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 小数位 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
