﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKPRT
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FKPRT))
        Me.lblTiShi = New System.Windows.Forms.Label
        Me.dgvKX = New System.Windows.Forms.DataGridView
        Me.可选列 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvXD = New System.Windows.Forms.DataGridView
        Me.XDID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.选定列 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnDel = New System.Windows.Forms.Button
        Me.btnTop = New System.Windows.Forms.Button
        Me.btnBotton = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        CType(Me.dgvKX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvXD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTiShi
        '
        Me.lblTiShi.AutoSize = True
        Me.lblTiShi.Location = New System.Drawing.Point(20, 75)
        Me.lblTiShi.Name = "lblTiShi"
        Me.lblTiShi.Size = New System.Drawing.Size(89, 12)
        Me.lblTiShi.TabIndex = 0
        Me.lblTiShi.Text = "当前设置的为："
        '
        'dgvKX
        '
        Me.dgvKX.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvKX.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvKX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKX.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.可选列})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvKX.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvKX.Location = New System.Drawing.Point(22, 123)
        Me.dgvKX.Name = "dgvKX"
        Me.dgvKX.RowHeadersVisible = False
        Me.dgvKX.RowTemplate.Height = 23
        Me.dgvKX.Size = New System.Drawing.Size(142, 309)
        Me.dgvKX.TabIndex = 4
        '
        '可选列
        '
        Me.可选列.HeaderText = "可选列"
        Me.可选列.Name = "可选列"
        Me.可选列.ReadOnly = True
        Me.可选列.Width = 120
        '
        'dgvXD
        '
        Me.dgvXD.AllowUserToAddRows = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvXD.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvXD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvXD.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.XDID, Me.选定列})
        Me.dgvXD.Location = New System.Drawing.Point(207, 123)
        Me.dgvXD.Name = "dgvXD"
        Me.dgvXD.RowHeadersVisible = False
        Me.dgvXD.RowTemplate.Height = 23
        Me.dgvXD.Size = New System.Drawing.Size(146, 309)
        Me.dgvXD.TabIndex = 4
        '
        'XDID
        '
        Me.XDID.HeaderText = "XDID"
        Me.XDID.Name = "XDID"
        Me.XDID.Visible = False
        '
        '选定列
        '
        Me.选定列.HeaderText = "选定列"
        Me.选定列.Name = "选定列"
        Me.选定列.ReadOnly = True
        Me.选定列.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.选定列.Width = 115
        '
        'btnAdd
        '
        Me.btnAdd.AutoSize = True
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(171, 200)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(30, 26)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.AutoSize = True
        Me.btnDel.Image = CType(resources.GetObject("btnDel.Image"), System.Drawing.Image)
        Me.btnDel.Location = New System.Drawing.Point(171, 283)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(30, 26)
        Me.btnDel.TabIndex = 5
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'btnTop
        '
        Me.btnTop.AutoSize = True
        Me.btnTop.Image = CType(resources.GetObject("btnTop.Image"), System.Drawing.Image)
        Me.btnTop.Location = New System.Drawing.Point(376, 200)
        Me.btnTop.Name = "btnTop"
        Me.btnTop.Size = New System.Drawing.Size(30, 26)
        Me.btnTop.TabIndex = 5
        Me.btnTop.UseVisualStyleBackColor = True
        '
        'btnBotton
        '
        Me.btnBotton.AutoSize = True
        Me.btnBotton.Image = CType(resources.GetObject("btnBotton.Image"), System.Drawing.Image)
        Me.btnBotton.Location = New System.Drawing.Point(376, 283)
        Me.btnBotton.Name = "btnBotton"
        Me.btnBotton.Size = New System.Drawing.Size(30, 26)
        Me.btnBotton.TabIndex = 5
        Me.btnBotton.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(46, 447)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(147, 33)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "保存设置 (&S)"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(236, 447)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(78, 33)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "关闭 (&X)"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 98)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "设定打印模板"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(266, 94)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(87, 21)
        Me.TextBox1.TabIndex = 7
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(358, 92)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(48, 22)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "选择"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Excel文件|*.xls"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(101, 94)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(165, 21)
        Me.TextBox2.TabIndex = 7
        '
        'Button2
        '
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(346, 447)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(60, 33)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "说明"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'FKPRT
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 503)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.btnBotton)
        Me.Controls.Add(Me.btnTop)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.dgvXD)
        Me.Controls.Add(Me.dgvKX)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblTiShi)
        Me.MaximizeBox = False
        Me.Name = "FKPRT"
        Me.Text = "单证打印设置"
        CType(Me.dgvKX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvXD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTiShi As System.Windows.Forms.Label
    Friend WithEvents dgvKX As System.Windows.Forms.DataGridView
    Friend WithEvents dgvXD As System.Windows.Forms.DataGridView
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents 可选列 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents btnTop As System.Windows.Forms.Button
    Friend WithEvents btnBotton As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents XDID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 选定列 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
