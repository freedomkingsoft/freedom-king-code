Public Class FKPRT

    Private sTableName As String
    Public WriteOnly Property myTableName() As String
        Set(ByVal value As String)
            sTableName = value
        End Set
    End Property

    Private iGongNengID As Integer
    Public WriteOnly Property myGongNengID() As Integer
        Set(ByVal value As Integer)
            iGongNengID = value
        End Set
    End Property
    Private Sub FKLXS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lblTiShi.Text = Me.lblTiShi.Text & sTableName & "打印设置"
        Me.Icon = FKG.myselfG.FKIcon
        FillDGKX(sTableName)
        FillDGXD(sTableName, iGongNengID)

        Me.TextBox2.Text = My.Application.Info.DirectoryPath & "\Templates\"
    End Sub

    Private Sub FillDGKX(ByVal TableName As String)
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select * from " & TableName & " where 1<1")
        Dim i As Integer
        For i = 0 To ds.Tables(0).Columns.Count - 2
            Me.dgvKX.Rows.Add(ds.Tables(0).Columns(i).ColumnName)
        Next

        Me.dgvKX.Columns(0).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
    End Sub

    Private Sub FillDGXD(ByVal TableName As String, ByVal GongNengID As Integer)
      
        Dim sLie As String
        Select Case TableName
            Case "库存表"
                sLie = "KuCunPrint"
            Case "工资表"
                sLie = "GongZiPrint"
            Case "凭证表"
                sLie = "PZPrint"
            Case Else
                Exit Sub
        End Select
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select " & sLie & "," & sLie.Replace("Print", "Path") & "  from GongNengShu where ID=" & GongNengID)
        sLie = ds.Tables(0).Rows(0).Item(0).ToString
        Me.TextBox1.Text = ds.Tables(0).Rows(0).Item(1).ToString

        If sLie = "" Then
            Exit Sub
        Else
            Dim sQM As String
            Do
                If sLie.IndexOf(",") = -1 Then
                    sQM = sLie
                Else
                    sQM = sLie.Substring(0, sLie.IndexOf(","))
                End If


                Me.dgvXD.Rows.Add(Me.dgvKX.CurrentRow.Index, sQM)

                If sLie.IndexOf(",") = -1 Then
                    sLie = sLie.Remove(0, sQM.Length)
                Else
                    sLie = sLie.Remove(0, sQM.Length + 1)
                End If

            Loop Until (sLie = "")
        End If

        HideYXLie()
    End Sub

    Private Sub HideYXLie()
        Dim i As Integer
        For i = 0 To Me.dgvXD.RowCount - 1
            Dim n As Integer
            For n = 0 To Me.dgvKX.RowCount - 1
                If Me.dgvKX.Rows(n).Cells(0).Value = Me.dgvXD.Rows(i).Cells(1).Value Then
                    Me.dgvXD.Rows(i).Cells(0).Value = n
                    Me.dgvKX.Rows(n).Visible = False
                End If
            Next
        Next
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If IsNothing(Me.dgvKX.CurrentRow) Then
            Exit Sub
        Else
            Me.dgvXD.Rows.Add(Me.dgvKX.CurrentRow.Index, Me.dgvKX.CurrentCell.Value)
            Me.dgvKX.CurrentRow.Visible = False
        End If
    End Sub

    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click
        If IsNothing(Me.dgvXD.CurrentRow) Then
            Exit Sub
        Else
            Me.dgvKX.Rows(Me.dgvXD.CurrentRow.Cells(0).Value).Visible = True
            Me.dgvXD.Rows.Remove(Me.dgvXD.CurrentRow)

        End If
    End Sub

    Private Sub btnTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTop.Click
        If IsNothing(Me.dgvXD.CurrentRow) Then
            Exit Sub
        Else
            If Me.dgvXD.CurrentRow.Index = 0 Then
                Exit Sub
            Else
                Me.dgvXD.Rows.InsertCopy(Me.dgvXD.CurrentRow.Index, Me.dgvXD.CurrentRow.Index - 1)
                Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index - 2).Cells(0).Value = Me.dgvXD.CurrentRow.Cells(0).Value
                Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index - 2).Cells(1).Value = Me.dgvXD.CurrentRow.Cells(1).Value
                'Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index - 2).Cells(2).Value = Me.dgvXD.CurrentRow.Cells(2).Value
                'Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index - 2).Cells(3).Value = Me.dgvXD.CurrentRow.Cells(3).Value
                Me.dgvXD.CurrentCell = Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index - 2).Cells(1)
                Me.dgvXD.Rows.RemoveAt(Me.dgvXD.CurrentRow.Index + 2)
            End If
        End If

    End Sub

    Private Sub btnBotton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBotton.Click
        If IsNothing(Me.dgvXD.CurrentRow) Then
            Exit Sub
        Else
            If Me.dgvXD.CurrentRow.Index = Me.dgvXD.Rows.GetLastRow(Windows.Forms.DataGridViewElementStates.None) Then
                Exit Sub
            Else
                Me.dgvXD.Rows.InsertCopy(Me.dgvXD.CurrentRow.Index, Me.dgvXD.CurrentRow.Index + 2)
                Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index + 2).Cells(0).Value = Me.dgvXD.CurrentRow.Cells(0).Value
                Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index + 2).Cells(1).Value = Me.dgvXD.CurrentRow.Cells(1).Value
                'Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index + 2).Cells(2).Value = Me.dgvXD.CurrentRow.Cells(2).Value
                'Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index + 2).Cells(3).Value = Me.dgvXD.CurrentRow.Cells(3).Value
                Me.dgvXD.CurrentCell = Me.dgvXD.Rows(Me.dgvXD.CurrentRow.Index + 2).Cells(1)
                Me.dgvXD.Rows.RemoveAt(Me.dgvXD.CurrentRow.Index - 2)
            End If
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        Dim sLieName As String
        Select Case sTableName
            Case "库存表"
                sLieName = "KuCunPrint"
            Case "工资表"
                sLieName = "GongZiPrint"
            Case "凭证表"
                sLieName = "PZPrint"
            Case Else
                Exit Sub
        End Select
        Dim sLie As String = ""
        mdb.Write("update gongnengshu set " & sLieName.Replace("Print", "Path") & " = '" & Me.TextBox1.Text & "' where ID=" & iGongNengID)
        If Me.dgvXD.RowCount = 0 Then
            mdb.Write("update gongnengshu  set " & sLieName & " ='" & sLie & "'  where ID=" & iGongNengID)
            Exit Sub
        End If
        Dim i As Integer
        For i = 0 To Me.dgvXD.RowCount - 1
            sLie = sLie & Me.dgvXD.Rows(i).Cells(1).Value.ToString.Replace(" ", "") & ","
        Next
        sLie = sLie.TrimEnd(",")

        mdb.Write("update gongnengshu  set " & sLieName & " ='" & sLie & "'  where ID=" & iGongNengID)

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Private Sub dgvXD_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvXD.CellValidated
    '    If e.ColumnIndex = 3 Then
    '        '是否合计
    '        If Me.dgvXD.CurrentCell.Value.ToString = "1" Or Me.dgvXD.CurrentCell.Value.ToString = "2" Or Me.dgvXD.CurrentCell.Value.ToString = "3" Or Me.dgvXD.CurrentCell.Value.ToString = "" Then
    '        Else
    '            MsgBox("合计设置列只可以输入""1，2，3""中的一个数字！", MsgBoxStyle.Information, "提示")
    '            Me.dgvXD.CurrentCell.Value = "1"
    '        End If
    '    End If
    '    If e.ColumnIndex = 4 Then
    '        '列宽，以百分比为单位
    '        If (IsNumeric(Me.dgvXD.CurrentCell.Value) And Me.dgvXD.CurrentCell.Value > 0) OrElse Me.dgvXD.CurrentCell.Value = "" Then
    '        Else
    '            MsgBox("列宽设置列只可以输入大于0的数字！", MsgBoxStyle.Information, "提示")
    '            Me.dgvXD.CurrentCell.Value = "5"
    '        End If
    '    End If
    '    If e.ColumnIndex = 5 Then
    '        '小数位数
    '        If Me.dgvXD.CurrentCell.Value = -1 OrElse Me.dgvXD.CurrentCell.Value = 0 OrElse Me.dgvXD.CurrentCell.Value = 1 OrElse Me.dgvXD.CurrentCell.Value = 2 OrElse Me.dgvXD.CurrentCell.Value = 3 OrElse Me.dgvXD.CurrentCell.Value = 4 OrElse Me.dgvXD.CurrentCell.Value = 5 OrElse Me.dgvXD.CurrentCell.Value = 6 Then
    '        Else
    '            MsgBox("小数位数最多可以设置6位小数，或者设置为 －1 不进行处理！", MsgBoxStyle.Information, "提示")
    '            Me.dgvXD.CurrentCell.Value = "-1"
    '        End If

    '    End If
    'End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.OpenFileDialog1.InitialDirectory = My.Application.Info.DirectoryPath & "\Templates"
        If Me.OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.TextBox1.Text = Me.OpenFileDialog1.FileName
            Me.TextBox1.Text = System.IO.Path.GetFileName(Me.TextBox1.Text)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            IO.File.Open(My.Application.Info.DirectoryPath & "\单证打印设置说明.txt", IO.FileMode.Open)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgvKX_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvKX.CellContentDoubleClick
        Me.btnAdd_Click(sender, e)
    End Sub

    Private Sub dgvXD_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvXD.CellContentDoubleClick
        Me.btnDel_Click(sender, e)
    End Sub
End Class