﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKQMSH
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbNian = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbYue = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnSH = New System.Windows.Forms.Button
        Me.btnXS = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.dgKCJZQJ = New System.Windows.Forms.DataGridView
        CType(Me.dgKCJZQJ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "请选择想要处理的月份"
        '
        'cbNian
        '
        Me.cbNian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbNian.FormattingEnabled = True
        Me.cbNian.Location = New System.Drawing.Point(58, 55)
        Me.cbNian.Name = "cbNian"
        Me.cbNian.Size = New System.Drawing.Size(74, 20)
        Me.cbNian.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(136, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(17, 12)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "年"
        '
        'cbYue
        '
        Me.cbYue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbYue.FormattingEnabled = True
        Me.cbYue.Location = New System.Drawing.Point(161, 55)
        Me.cbYue.Name = "cbYue"
        Me.cbYue.Size = New System.Drawing.Size(54, 20)
        Me.cbYue.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(221, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "月"
        '
        'btnSH
        '
        Me.btnSH.Location = New System.Drawing.Point(54, 111)
        Me.btnSH.Name = "btnSH"
        Me.btnSH.Size = New System.Drawing.Size(160, 45)
        Me.btnSH.TabIndex = 3
        Me.btnSH.Text = "审核"
        Me.btnSH.UseVisualStyleBackColor = True
        '
        'btnXS
        '
        Me.btnXS.Location = New System.Drawing.Point(55, 185)
        Me.btnXS.Name = "btnXS"
        Me.btnXS.Size = New System.Drawing.Size(98, 45)
        Me.btnXS.TabIndex = 3
        Me.btnXS.Text = "消审"
        Me.btnXS.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(188, 185)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(69, 45)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "关闭"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgKCJZQJ
        '
        Me.dgKCJZQJ.AllowUserToAddRows = False
        Me.dgKCJZQJ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgKCJZQJ.Location = New System.Drawing.Point(283, 10)
        Me.dgKCJZQJ.Name = "dgKCJZQJ"
        Me.dgKCJZQJ.ReadOnly = True
        Me.dgKCJZQJ.RowHeadersVisible = False
        Me.dgKCJZQJ.RowTemplate.Height = 23
        Me.dgKCJZQJ.Size = New System.Drawing.Size(316, 312)
        Me.dgKCJZQJ.TabIndex = 4
        '
        'FKQMSH
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(609, 330)
        Me.Controls.Add(Me.dgKCJZQJ)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnXS)
        Me.Controls.Add(Me.btnSH)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbYue)
        Me.Controls.Add(Me.cbNian)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FKQMSH"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "期末审核"
        CType(Me.dgKCJZQJ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbNian As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbYue As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnSH As System.Windows.Forms.Button
    Friend WithEvents btnXS As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents dgKCJZQJ As System.Windows.Forms.DataGridView
End Class
