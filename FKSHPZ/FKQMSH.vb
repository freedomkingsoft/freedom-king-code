﻿Public Class FKQMSH

    Private Sub FKQMSH_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        FileNianYue()
    End Sub

    Private Sub FileNianYue()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.cbNian, "select distinct Year(ksrq) from fkzq")
        mdb.DataBind(Me.cbYue, "select distinct Month(ksrq) from fkzq")

        Me.cbNian.Text = FKG.myselfG.NianFen
        Me.cbYue.Text = FKG.myselfG.YueFen

        FillDG(Me.cbNian.Text, Me.cbYue.Text)
    End Sub

    Private Function FillDG(ByVal iNian As String, ByVal iYue As String) As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgKCJZQJ, "select nian as 年份,yue as 月份,'未审' as 审核状态 from jibencaozuo where 删除=0  and (审核='' or isNull(审核,'')='') and nian=" & iNian & " group by nian,yue union select nian as 年份,yue as 月份,'已审核' as 审核状态 from jibencaozuo where 删除=0  and 审核<>''  and nian=" & iNian & " group by nian,yue order by yue")
    End Function

    Private Sub btnSH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSH.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("update JiBenCaoZuo set 审核='" & FKG.myselfG.YongHu & "' where Nian=" & Me.cbNian.Text & " and Yue=" & Me.cbYue.Text & " and 删除='false'  and (审核='' or 审核 is null)")
        mdb.Write("update PZ set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & Me.cbNian.Text & " and 月份=" & Me.cbYue.Text & "   and (审核='' or 审核 is null)")
        mdb.Write("update KuCun set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & Me.cbNian.Text & " and 月份=" & Me.cbYue.Text & "   and (审核='' or 审核 is null)")
        mdb.Write("update GongZi set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & Me.cbNian.Text & " and 月份=" & Me.cbYue.Text & "   and (审核='' or 审核 is null)")

        FillDG(Me.cbNian.Text, Me.cbYue.Text)
        MsgBox("审核完成！", MsgBoxStyle.Information, "提示")
    End Sub

    Private Sub btnXS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnXS.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        If mdb.Reader("select jiZhang from FKZQ WHERE NY='" & Me.cbNian.Text & Me.cbYue.Text.ToString.PadLeft(2, "0") & "'").Tables(0).Rows(0).Item(0) = True Then
            MsgBox("当月已经记帐,请先反记帐,然后再取消审核!", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        mdb.Write("update JiBenCaoZuo set 审核='' where Nian=" & Me.cbNian.Text & " and Yue=" & Me.cbYue.Text & " and 删除='false' and 审核='" & FKG.myselfG.YongHu & "' ")
        mdb.Write("update PZ set 审核='' where 年份=" & Me.cbNian.Text & " and 月份=" & Me.cbYue.Text & " and 审核='" & FKG.myselfG.YongHu & "' ")
        mdb.Write("update KuCun set 审核='' where 年份=" & Me.cbNian.Text & " and 月份=" & Me.cbYue.Text & " and 审核='" & FKG.myselfG.YongHu & "' ")
        mdb.Write("update GongZi set 审核='' where 年份=" & Me.cbNian.Text & " and 月份=" & Me.cbYue.Text & " and 审核='" & FKG.myselfG.YongHu & "' ")

        FillDG(Me.cbNian.Text, Me.cbYue.Text)
        MsgBox("消审完成！", MsgBoxStyle.Information, "提示")
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cbNian_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbNian.TextChanged
        If Me.cbNian.Text = "" Then
            Exit Sub
        End If
        FillDG(Me.cbNian.Text, Me.cbYue.Text)
    End Sub
End Class