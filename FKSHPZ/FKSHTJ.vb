﻿Imports System.Windows.Forms

Public Class FKSHTJ

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If (Me.rbPingZhengZi.Checked And Me.cbPZZ.Text = "") Or (Me.rbGongneng.Checked And Me.cgGongNengName.Text = "") Then

        Else
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        End If
        'Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub FKSHTJ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        'Dim dsGN As DataSet
        'dsGN = mdb.Reader("select DISTINCT ID,ConText,PZSCFS from GongNengShu,GN_PZ where isGongNeng='true' and PZSCFS=0 and GongNengShu.ID=GN_PZ.GNSID")
        'Dim i As Integer
        'For i = 0 To dsGN.Tables(0).Rows.Count - 1
        '    Me.cbGongNengID.Items.Add(dsGN.Tables(0).Rows(i).Item("ID"))
        '    Me.cgGongNengName.Items.Add(dsGN.Tables(0).Rows(i).Item("ConText"))
        '    Me.cbPzscfs.Items.Add(dsGN.Tables(0).Rows(i).Item("PZSCFS"))
        'Next

        'If Me.cgGongNengName.Items.Count > 0 Then
        '    Me.cgGongNengName.SelectedIndex = 0
        'End If

        'mdb.DataBind(Me.cbPZZ, "select  distinct pingzhengzi from jibencaozuo,GongNengShu where jibencaozuo.A_GNSID=GONGNENGSHU.ID AND  JIBENCAOZUO.nian=" & FKG.myselfG.NianFen & "  and  删除='false' AND GONGNENGSHU.PZSCFS=1")

        'If Me.cbPZZ.Items.Count > 0 Then
        '    Me.cbPZZ.SelectedIndex = 0
        'End If

        Me.txtNian.Text = FKG.myselfG.NianFen
        Me.nudYue.Value = FKG.myselfG.YueFen

    End Sub

    Private Sub cgGongNengName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cgGongNengName.SelectedIndexChanged, cbPZZ.SelectedIndexChanged
        Me.cbGongNengID.SelectedIndex = Me.cgGongNengName.SelectedIndex
        Me.cbPzscfs.SelectedIndex = Me.cgGongNengName.SelectedIndex
    End Sub

    Private Sub rbGongneng_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbGongneng.CheckedChanged
        If Me.rbGongneng.Checked Then
            Me.cgGongNengName.Visible = True
            Me.cbPZZ.Visible = False
        Else
            Me.cgGongNengName.Visible = False
            Me.cbPZZ.Visible = True
        End If
    End Sub

    Private Sub nudYue_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudYue.ValueChanged
        fillPZZ()
    End Sub

    Private Sub fillPZZ()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsGN As DataSet
        dsGN = mdb.Reader("select DISTINCT ID,ConText,PZSCFS from GongNengShu,GN_PZ where isGongNeng='true' and PZSCFS=0 and GongNengShu.ID=GN_PZ.GNSID")

        Me.cbGongNengID.Items.Clear()
        Me.cgGongNengName.Items.Clear()
        Me.cbPzscfs.Items.Clear()

        Dim i As Integer
        For i = 0 To dsGN.Tables(0).Rows.Count - 1
            Me.cbGongNengID.Items.Add(dsGN.Tables(0).Rows(i).Item("ID"))
            Me.cgGongNengName.Items.Add(dsGN.Tables(0).Rows(i).Item("ConText"))
            Me.cbPzscfs.Items.Add(dsGN.Tables(0).Rows(i).Item("PZSCFS"))
        Next

        If Me.cgGongNengName.Items.Count > 0 Then
            Me.cgGongNengName.SelectedIndex = 0
        End If

        Me.cbPZZ.Items.Clear()

        mdb.DataBind(Me.cbPZZ, "select  distinct pingzhengzi from jibencaozuo,GongNengShu where jibencaozuo.A_GNSID=GONGNENGSHU.ID AND  JIBENCAOZUO.nian=" & FKG.myselfG.NianFen & "  and  删除='false' AND GONGNENGSHU.PZSCFS=1 and jibencaozuo.yue=" & me.nudYue.Value)

        If Me.cbPZZ.Items.Count > 0 Then
            Me.cbPZZ.SelectedIndex = 0
        End If

        'Me.txtNian.Text = FKG.myselfG.NianFen
        'Me.nudYue.Value = FKG.myselfG.YueFen
    End Sub

  
End Class
