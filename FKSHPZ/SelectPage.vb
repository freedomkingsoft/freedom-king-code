Imports System.Windows.Forms

Public Class SelectPage

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Public iMinHao As Integer
    Public iMaxHao As Integer

    Private Sub SelectPage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.nuMin.Minimum = iMinHao
        Me.nuMin.Maximum = iMaxHao
        Me.nuMin.Value = iMinHao

        Me.nuMax.Minimum = iMinHao
        Me.nuMax.Maximum = iMaxHao
        Me.nuMax.Value = iMaxHao
    End Sub

    Private Sub rbAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        If rbAll.Checked Then
            Me.nuMax.Enabled = False
            Me.nuMin.Enabled = False
        Else
            Me.nuMax.Enabled = True
            Me.nuMin.Enabled = True
        End If
    End Sub
End Class
