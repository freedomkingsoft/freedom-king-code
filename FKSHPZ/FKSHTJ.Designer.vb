﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKSHTJ
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.cgGongNengName = New System.Windows.Forms.ComboBox
        Me.cbWeiShen = New System.Windows.Forms.CheckBox
        Me.cbGongNengID = New System.Windows.Forms.ComboBox
        Me.cbPzscfs = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNian = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.nudYue = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.rbGongneng = New System.Windows.Forms.RadioButton
        Me.rbPingZhengZi = New System.Windows.Forms.RadioButton
        Me.cbPZZ = New System.Windows.Forms.ComboBox
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.nudYue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(125, 260)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 27)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 21)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "确定"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 21)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "取消"
        '
        'cgGongNengName
        '
        Me.cgGongNengName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cgGongNengName.FormattingEnabled = True
        Me.cgGongNengName.Location = New System.Drawing.Point(29, 120)
        Me.cgGongNengName.Name = "cgGongNengName"
        Me.cgGongNengName.Size = New System.Drawing.Size(229, 20)
        Me.cgGongNengName.TabIndex = 2
        '
        'cbWeiShen
        '
        Me.cbWeiShen.AutoSize = True
        Me.cbWeiShen.Location = New System.Drawing.Point(29, 216)
        Me.cbWeiShen.Name = "cbWeiShen"
        Me.cbWeiShen.Size = New System.Drawing.Size(108, 16)
        Me.cbWeiShen.TabIndex = 3
        Me.cbWeiShen.Text = "只显示未审凭证"
        Me.cbWeiShen.UseVisualStyleBackColor = True
        '
        'cbGongNengID
        '
        Me.cbGongNengID.FormattingEnabled = True
        Me.cbGongNengID.Location = New System.Drawing.Point(127, 212)
        Me.cbGongNengID.Name = "cbGongNengID"
        Me.cbGongNengID.Size = New System.Drawing.Size(61, 20)
        Me.cbGongNengID.TabIndex = 2
        Me.cbGongNengID.Visible = False
        '
        'cbPzscfs
        '
        Me.cbPzscfs.FormattingEnabled = True
        Me.cbPzscfs.Location = New System.Drawing.Point(201, 212)
        Me.cbPzscfs.Name = "cbPzscfs"
        Me.cbPzscfs.Size = New System.Drawing.Size(61, 20)
        Me.cbPzscfs.TabIndex = 2
        Me.cbPzscfs.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 156)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 12)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "请指定待审核的期间"
        '
        'txtNian
        '
        Me.txtNian.Location = New System.Drawing.Point(29, 171)
        Me.txtNian.Name = "txtNian"
        Me.txtNian.ReadOnly = True
        Me.txtNian.Size = New System.Drawing.Size(72, 21)
        Me.txtNian.TabIndex = 4
        Me.txtNian.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(107, 180)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "年"
        '
        'nudYue
        '
        Me.nudYue.Location = New System.Drawing.Point(130, 172)
        Me.nudYue.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudYue.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudYue.Name = "nudYue"
        Me.nudYue.Size = New System.Drawing.Size(40, 21)
        Me.nudYue.TabIndex = 5
        Me.nudYue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudYue.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(178, 180)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 12)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "月"
        '
        'rbGongneng
        '
        Me.rbGongneng.AutoSize = True
        Me.rbGongneng.Checked = True
        Me.rbGongneng.Location = New System.Drawing.Point(29, 98)
        Me.rbGongneng.Name = "rbGongneng"
        Me.rbGongneng.Size = New System.Drawing.Size(83, 16)
        Me.rbGongneng.TabIndex = 6
        Me.rbGongneng.TabStop = True
        Me.rbGongneng.Text = "按功能审核"
        Me.rbGongneng.UseVisualStyleBackColor = True
        '
        'rbPingZhengZi
        '
        Me.rbPingZhengZi.AutoSize = True
        Me.rbPingZhengZi.Location = New System.Drawing.Point(163, 98)
        Me.rbPingZhengZi.Name = "rbPingZhengZi"
        Me.rbPingZhengZi.Size = New System.Drawing.Size(95, 16)
        Me.rbPingZhengZi.TabIndex = 6
        Me.rbPingZhengZi.Text = "按凭证字审核"
        Me.rbPingZhengZi.UseVisualStyleBackColor = True
        '
        'cbPZZ
        '
        Me.cbPZZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPZZ.FormattingEnabled = True
        Me.cbPZZ.Location = New System.Drawing.Point(29, 120)
        Me.cbPZZ.Name = "cbPZZ"
        Me.cbPZZ.Size = New System.Drawing.Size(229, 20)
        Me.cbPZZ.TabIndex = 2
        Me.cbPZZ.Visible = False
        '
        'FKSHTJ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(283, 299)
        Me.Controls.Add(Me.rbPingZhengZi)
        Me.Controls.Add(Me.rbGongneng)
        Me.Controls.Add(Me.nudYue)
        Me.Controls.Add(Me.txtNian)
        Me.Controls.Add(Me.cbWeiShen)
        Me.Controls.Add(Me.cbPzscfs)
        Me.Controls.Add(Me.cbGongNengID)
        Me.Controls.Add(Me.cbPZZ)
        Me.Controls.Add(Me.cgGongNengName)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FKSHTJ"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "凭证审核方式"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.nudYue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents cgGongNengName As System.Windows.Forms.ComboBox
    Friend WithEvents cbWeiShen As System.Windows.Forms.CheckBox
    Friend WithEvents cbGongNengID As System.Windows.Forms.ComboBox
    Friend WithEvents cbPzscfs As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNian As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents nudYue As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents rbGongneng As System.Windows.Forms.RadioButton
    Friend WithEvents rbPingZhengZi As System.Windows.Forms.RadioButton
    Friend WithEvents cbPZZ As System.Windows.Forms.ComboBox

End Class
