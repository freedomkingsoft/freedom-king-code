﻿Public Class FKSHPZ
   
    Dim dvPZ As DataView
    Dim dvPZLK, dvPZDot As String

    Dim pzhList As New System.Windows.Forms.ComboBox
    'Dim PZListHao As New System.Windows.Forms.ComboBox

    Dim iPZSCFS As Integer '凭证生成方式
    Private iGongnengID As Integer
    Private sGongnengName As String
    Private sPingZhengZi As String

    Dim sPZH As String
    Dim bALL As Boolean
    'Dim iWorkState As Integer  '0为新增单证，1为新增记录，2为修改记录，3为删除记录,9为浏览记录
    'Dim iJiBenID As Integer

    Dim iYue As Integer  '指定要审核的月份，年份是不能改变的

    Dim iNian As Integer
    '年份应该可以改变，否则查看往年时将发生错误

    Private Sub FKGN_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        GC.Collect()
    End Sub

    Public bFromYEB As Boolean

    Public dYEBYue As Integer
    Public sYEBPZH As String
    Private Sub iniHuanJing()
        If bFromYEB Then
            Me.Text = My.Application.Info.Title

            '需要通过凭证号和月份来确定凭证生产方式
            '否则将出现将直接凭证中同科目汇总的问题

            '修改直接凭证中，将所有同一科目，同样摘要自动汇总的错误，应该显示输入时的原数据
            '错误原因在来自余额表时凭证生产方式设定错误

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            iYue = dYEBYue

            Try
                iGongnengID = mdb.Reader("select top 1 A_GNSID from Jibencaozuo where Nian=" & iNian & " and yue=" & iYue & " and pingzhengzi='" & Split(sYEBPZH).GetValue(0) & "' and pingzhenghao=" & Split(sYEBPZH).GetValue(Split(sYEBPZH).Length - 1)).Tables(0).Rows(0).Item(0)

                Dim dsss As DataSet
                dsss = mdb.Reader("select pzscfs,Context from gongnengshu where ID=" & iGongnengID)

                iPZSCFS = dsss.Tables(0).Rows(0).Item(0)
                sGongnengName = dsss.Tables(0).Rows(0).Item(1)

            Catch ex As Exception
                iPZSCFS = 1
            End Try

            ReadSql()
            FillpzhList()

            FillDG(sYEBPZH)

            Me.SetLK(Me.dgvPZ)

            Me.GJ1.Enabled = False
            Me.GJ2.Enabled = False
            Me.GJ4.Enabled = False

            Me.lblPZH.Text = "凭证号：" & " " & sYEBPZH
            Me.tssNianYue.Text = "就绪"
        Else
            Dim dlg As New FKSHTJ
            If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                Me.Text = My.Application.Info.Title

                If dlg.rbGongneng.Checked Then
                    iGongnengID = dlg.cbGongNengID.SelectedItem
                    sGongnengName = dlg.cgGongNengName.SelectedItem
                    bALL = Not dlg.cbWeiShen.Checked
                    iPZSCFS = 0
                    iYue = dlg.nudYue.Value
                    iNian = dlg.txtNian.Text
                    ReadSql()
                    FillpzhList()
                    If Me.pzhList.Items.Count = 0 Then
                        sPZH = "-1"
                        FillDG(sPZH)
                    Else
                        Me.pzhList.SelectedIndex = 0
                        sPZH = Me.pzhList.SelectedItem
                        FillDG(sPZH)
                    End If

                    Me.SetLK(Me.dgvPZ)
                    Me.tssNianYue.Text = "就绪"
                Else
                    bALL = Not dlg.cbWeiShen.Checked
                    iPZSCFS = 1
                    sPingZhengZi = dlg.cbPZZ.SelectedItem.ToString
                    iYue = dlg.nudYue.Value
                    iNian = dlg.txtNian.Text
                    FillpzhList()
                    If Me.pzhList.Items.Count = 0 Then
                        sPZH = "-1"
                        FillDG(sPZH)
                    Else
                        Me.pzhList.SelectedIndex = 0
                        sPZH = Me.pzhList.SelectedItem
                        FillDG(sPZH)
                    End If

                    Me.SetLK(Me.dgvPZ)
                    Me.tssNianYue.Text = "就绪"
                End If
            Else
                Me.Dispose()

            End If
        End If
        Me.lblRiQi.Text = Me.lblRiQi.Text & " " & iNian & " 年" & " " & iYue & " 月"
    End Sub

    Private Sub FKGN_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        iniHuanJing()
        Me.Icon = FKG.myselfG.FKIcon

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        'If mdb.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) > 4 Then
        '    Me.预览.Visible = True
        '    Me.打印.Visible = True
        'Else
        '    Me.预览.Visible = False
        '    Me.打印.Visible = False
        'End If
    End Sub

    Private Sub ReadSql()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select * from GongNengShu where Id=" & iGongnengID)
        If ds.Tables(0).Rows(0).Item("pzSQL").ToString = "" Then
            Me.dgvPZ.Tag = " * "
            dvPZLK = ""
            dvPZDot = ""
        Else
            Me.dgvPZ.Tag = ds.Tables(0).Rows(0).Item("pzSQL").ToString
            dvPZLK = ds.Tables(0).Rows(0).Item("PZLK").ToString
            dvPZDot = ds.Tables(0).Rows(0).Item("PZDot").ToString
        End If
    End Sub

    Private Sub FillpzhList()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        Dim sCase As String
        If bALL Then
            sCase = ""
        Else
            sCase = " and 审核<>'' "
        End If

        If iPZSCFS = 0 Then
            ds = mdb.Reader("select DISTINCT  凭证号  from PZ where A_GNSID=" & iGongnengID & " and 年份=" & iNian & " and 月份=" & iYue & sCase)
            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                pzhList.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        Else
            ds = mdb.Reader("select DISTINCT   pingzhenghao  from jibencaozuo where pingzhengzi='" & sPingZhengZi & "' and Nian=" & iNian & " and 删除='false' and Yue=" & iYue & sCase)
            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                pzhList.Items.Add(sPingZhengZi & ds.Tables(0).Rows(i).Item(0).ToString.PadLeft(7, " "))
            Next
        End If


        Me.上单.Enabled = False
        Me.首单.Enabled = False
        If Me.pzhList.Items.Count <= 1 Then
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        End If
    End Sub

    Dim dsPrint As DataSet
    Private Function FillDG(ByVal PZH As String) As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim dvPZ As DataView
        Select Case iPZSCFS
            Case 0
                dvPZ = mdb.Reader("select   " & Me.dgvPZ.Tag & "  from 凭证表 where 凭证号 ='" & PZH & "' and  功能名称='" & sGongnengName & "' and 显示='true'  and (借方金额<>0 or 贷方金额<>0) and 年份 = " & iNian & " and 月份=" & iYue & "  order by 行数").Tables(0).DefaultView

                '2012.9.20修正借贷方均为0时仍显示的问题，增加了 and (借方金额<>0 or 贷方金额<>0) 条件。

            Case 1
                dsPrint = mdb.Reader("select  年份,月份,日期,凭证号,摘要,科目,科目全名 as 借方科目,对应科目,对应科目全名 as 贷方科目,SUM(借方金额) AS 金额,SUM(借方数量) as 借方数量,SUM(贷方数量) AS 贷方数量,SUM(贷方金额) AS 贷方金额 FROM 凭证表 WHERE  凭证号 ='" & PZH & "' and 年份 = " & iNian & " and 月份=" & iYue & " and 借方金额<>0  group by 摘要,年份,月份,日期,凭证号,科目,科目全名,对应科目,对应科目全名")

                '2012.9.20,默认为双金额方式显示。
                '2012.9.20修正无效，仍需坚持设计时就应满足单金额显示
                'dsPrint = mdb.Reader("select  年份,月份,日期,凭证号,摘要,科目,科目全名 as 科目,对应科目 as 对应科目DM,对应科目全名 as 对应科目,SUM(借方数量) as 借方数量,SUM(借方金额) AS 借方金额,SUM(贷方数量) AS 贷方数量,SUM(贷方金额) AS 贷方金额 FROM 凭证表 WHERE  凭证号 ='" & PZH & "'   and 年份 = " & iNian & " and 月份=" & iYue & " and (借方金额<>0 or 贷方金额<>0) group by 年份,月份,日期,凭证号,摘要,科目,科目全名,对应科目,对应科目全名")
                dvPZ = dsPrint.Tables(0).DefaultView
            Case Else
                Return False
        End Select

        JiSuanHeJiZhi(dvPZ, "PZHeJi", False, -1)
        Me.dgvPZ.DataSource = dvPZ

        If iPZSCFS = 1 Then
            Me.dgvPZ.Columns("年份").Visible = False
            Me.dgvPZ.Columns("月份").Visible = False
            Me.dgvPZ.Columns("日期").Visible = False
            Me.dgvPZ.Columns("凭证号").Visible = False
            Me.dgvPZ.Columns("科目").Visible = False
            Me.dgvPZ.Columns("对应科目").Visible = False
            'Me.dgvPZ.Columns("借方数量").Visible = False
            'Me.dgvPZ.Columns("贷方数量").Visible = False
            Me.dgvPZ.Columns("贷方金额").Visible = False
        End If

        SetHJColor(Me.dgvPZ)

        setStatus("-1")

        Return True

    End Function

    Private Sub setStatus(ByVal sSHR As String)
        If sSHR = "-1" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim DS As DataSet
            If Me.bFromYEB Then
                DS = mdb.Reader("select DISTINCT  审核 from pz where 年份=" & iNian & " and 月份=" & dYEBYue & " and 凭证号='" & sYEBPZH & "'")
            Else
                DS = mdb.Reader("select DISTINCT  审核 from pz where 年份=" & iNian & " and 月份=" & iYue & " and 凭证号='" & sPZH & "'")
            End If

            If DS.Tables(0).Rows.Count = 0 Then
                sSHR = ""
            Else
                sSHR = DS.Tables(0).Rows(0).Item(0).ToString
            End If
        End If

        If sSHR = "" Then
            Me.tssState.Text = "未审核"
            Me.tssSHR.Text = ""
        Else
            Me.tssState.Text = "已审核"
            Me.tssSHR.Text = sSHR
        End If
        If sPZH = "-1" Then
            Me.lblPZH.Text = "凭证号：" & " 无"

        Else
            Me.lblPZH.Text = "凭证号：" & " " & sPZH
        End If

    End Sub

    Private Function JiSuanHeJiZhi(ByVal dv As DataView, ByVal sBiaoHeJi As String, ByVal bXSHS As Boolean, Optional ByVal iHang As Integer = -1) As Boolean
        If dv.Count = 0 Then
            HideZero(dv)
            Return True
        Else
            dv.AddNew()

            Dim i As Integer
            Dim n As Integer
            Dim iMaxRowIndex As Integer
            Dim dHeJi As Double
            iMaxRowIndex = dv.Count - 1

            Dim sHJ As String
            If iPZSCFS = 0 Then
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                sHJ = mdb.Reader("select " & sBiaoHeJi & "  from gongnengshu where id=" & iGongnengID).Tables(0).Rows(0).Item(0).ToString
            Else
                sHJ = "1111311112222"
            End If
            
            For n = 0 To dv.Table.Columns.Count - 1
                If sHJ = "" OrElse sHJ.Substring(n, 1).ToString = "1" Then
                    dv.Item(iMaxRowIndex).Item(n) = DBNull.Value
                ElseIf sHJ.Substring(n, 1).ToString = "2" Then
                    dHeJi = 0
                    For i = 0 To dv.Count - 2
                        Try
                            dHeJi = dHeJi + dv.Item(i).Item(n)
                        Catch ex As Exception
                            Exit For
                        End Try
                    Next
                    If dHeJi = 0 Then
                        dv.Item(iMaxRowIndex).Item(n) = DBNull.Value
                    Else
                        dv.Item(iMaxRowIndex).Item(n) = dHeJi
                    End If
                Else
                    dv.Item(iMaxRowIndex).Item(n) = "合计："
                End If

            Next
            '填充新行数
            If bXSHS Then
                For i = 0 To dv.Count - 2
                    dv.Item(i).Item(iHang + 1) = i + 1
                Next
            End If

            HideZero(dv)
        End If
    End Function

    Private Sub HideZero(ByVal dv As DataView)
        Dim i, n As Integer
        For i = 1 To dv.Table.Columns.Count - 1
            For n = 0 To dv.Count - 2
                Try
                    If dv.Item(n).Item(i) = 0 Then
                        dv.Item(n).Item(i) = DBNull.Value
                    End If
                Catch ex As Exception
                    Exit For
                End Try

            Next
        Next
    End Sub

    Private Sub SetHJColor(ByVal obj As System.Windows.Forms.DataGridView)
        If obj.RowCount = 0 Then
            Exit Sub
        Else
            obj.Rows(obj.Rows.GetLastRow(Windows.Forms.DataGridViewElementStates.None)).DefaultCellStyle.BackColor = Drawing.Color.LightGray
        End If

    End Sub

    Private Sub SetLK(ByVal obj As System.Windows.Forms.DataGridView)
        If iPZSCFS = 0 Then

            Dim i As Integer
            Dim sLK() As String
            Dim sDot() As String
            Select Case obj.Name
                Case "dgvPZ"
                    sLK = Split(dvPZLK)
                    sDot = Split(dvPZDot)
                Case Else
                    Exit Sub
            End Select
            If sLK.Length = 1 Then
                Exit Sub
            End If
            For i = 1 To obj.ColumnCount - 1
                obj.Columns(i).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
                obj.Columns(i).Width = sLK.GetValue(i) * obj.Width / 100
                If sDot.Length = 1 Then
                Else
                    If sDot.GetValue(i) = -1 Then
                    Else
                        obj.Columns(i).DefaultCellStyle.Format = "N" & sDot(i)
                    End If
                End If
            Next
        Else 'ipzscfs=1

            obj.Columns("摘要").Width = 16 * obj.Width / 100
            obj.Columns("借方科目").Width = 22 * obj.Width / 100
            obj.Columns("贷方科目").Width = 22 * obj.Width / 100
            obj.Columns("金额").Width = 13 * obj.Width / 100
            'obj.Columns("贷方金额").Width = 13 * obj.Width / 100

            obj.Columns("摘要").DefaultCellStyle.Format = "N2"
            obj.Columns("借方科目").DefaultCellStyle.Format = "N2"
            obj.Columns("贷方科目").DefaultCellStyle.Format = "N2"
            obj.Columns("金额").DefaultCellStyle.Format = "N2"
            'obj.Columns("贷方金额").DefaultCellStyle.Format = "N2"

        End If

    End Sub


    Private Sub 显示凭证_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles 显示凭证.Click
        If Me.cbFPZHao.Text = "" Then
            Exit Sub
        End If

        sPZH = Me.cbFPZHao.Text
        FillDG(SPZH)

        Me.pzhList.SelectedIndex = Me.cbFPZHao.SelectedIndex

        If Me.pzhList.SelectedIndex = 0 Then
            Me.上单.Enabled = False
            Me.首单.Enabled = False
        Else
            Me.上单.Enabled = True
            Me.首单.Enabled = True
        End If

        If Me.pzhList.SelectedIndex = Me.pzhList.Items.Count - 1 Then
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        Else
            Me.下单.Enabled = True
            Me.尾单.Enabled = True
        End If

    End Sub

    Private Sub cbFPZHao_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbFPZHao.Enter
        Me.cbFPZHao.Text = ""

        Dim ds As DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sCase As String
        If bALL Then
            sCase = ""
        Else
            sCase = " and 审核<>'' "
        End If

        If iPZSCFS = 0 Then
            ds = mdb.Reader("select DISTINCT  凭证号  from PZ where A_GNSID=" & iGongnengID & " and 年份=" & iNian & " and 月份=" & iYue & sCase)
        Else
            ds = mdb.Reader("select DISTINCT  凭证号  from PZ where  年份=" & iNian & " and 月份=" & iYue & "  and left(凭证号,charindex(' ',凭证号)-1) = '" & sPingZhengZi & "'" & sCase)
        End If

        Me.cbFPZHao.ComboBox.DataSource = ds.Tables(0)
        Me.cbFPZHao.ComboBox.ValueMember = ds.Tables(0).Columns(0).ColumnName
        Me.cbFPZHao.ComboBox.DisplayMember = ds.Tables(0).Columns(0).ColumnName

    End Sub



    Private Sub 首单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 首单.Click

        Me.上单.Enabled = False
        Me.首单.Enabled = False
        Me.下单.Enabled = True
        Me.尾单.Enabled = True

        If Me.pzhList.Items.Count = 0 Then
            Exit Sub
        End If

        Me.pzhList.SelectedIndex = 0
        sPZH = Me.pzhList.SelectedItem.ToString
        FillDG(sPZH)

    End Sub

    Private Sub 上单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 上单.Click
        Me.下单.Enabled = True
        Me.尾单.Enabled = True

        If Me.pzhList.SelectedIndex = -1 Then
            Me.pzhList.SelectedIndex = Me.pzhList.Items.Count - 1
        Else
            Me.pzhList.SelectedIndex = Me.pzhList.SelectedIndex - 1
        End If
        sPZH = Me.pzhList.SelectedItem.ToString
        FillDG(sPZH)

        If Me.pzhList.SelectedIndex = 0 Then
            Me.上单.Enabled = False
            Me.首单.Enabled = False
            'Exit Sub
            'Else
        End If

    End Sub

    Private Sub 下单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 下单.Click
        Me.上单.Enabled = True
        Me.首单.Enabled = True

        Me.pzhList.SelectedIndex = Me.pzhList.SelectedIndex + 1
        sPZH = Me.pzhList.SelectedItem.ToString

        FillDG(sPZH)

        If Me.pzhList.SelectedIndex = Me.pzhList.Items.Count - 1 Then
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
            'Exit Sub
            'Else
        End If
    End Sub

    Private Sub 尾单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 尾单.Click
        Me.上单.Enabled = True
        Me.首单.Enabled = True
        Me.下单.Enabled = False
        Me.尾单.Enabled = False

        If Me.pzhList.Items.Count = 0 OrElse Me.pzhList.SelectedIndex = Me.pzhList.Items.Count - 1 OrElse Me.pzhList.SelectedIndex = -1 Then
            Exit Sub
        End If
        Me.pzhList.SelectedIndex = Me.pzhList.Items.Count - 1
        sPZH = Me.pzhList.SelectedItem.ToString

        FillDG(SPZH)
    
    End Sub

  

    Dim iDanZhengType As Integer = 3
    Private Sub PrintView()
        Select Case iDanZhengType
            Case 1, 2
                Dim obj As New FKPrn.Print(SPZH, iGongnengID, iDanZhengType)
                obj.PrintPreview()
            Case 3 'dayin pz
                Dim obj As New FKPrn.Print(SPZH, iGongnengID, iDanZhengType)
                obj.iNian = iNian
                obj.iYue = iYue
                obj.iPZSCFS = Me.iPZSCFS
                obj.dsHuiZong = dsPrint

                obj.PrintPreview()
        End Select
    End Sub

    Private Sub PrintOut()
        Select Case iDanZhengType
            Case 1, 2
                Dim obj As New FKPrn.Print(sPZH, iGongnengID, iDanZhengType)
                obj.Print()
                obj = Nothing
                System.GC.Collect()
            Case 3
                Dim obj As New FKPrn.Print(sPZH, iGongnengID, iDanZhengType)
                obj.iNian = iNian
                obj.iYue = iYue
                obj.iPZSCFS = Me.iPZSCFS
                obj.dsHuiZong = dsPrint

                obj.Print()
                obj = Nothing
                System.GC.Collect()
        End Select
    End Sub

    Private Sub LianXuPrintOut()
        Select Case iDanZhengType
            Case 1, 2
                Dim obj As New FKPrn.Print(sPZH, iGongnengID, iDanZhengType)
                obj.Print()
                obj = Nothing
                System.GC.Collect()
            Case 3
                Dim dlgPage As New SelectPage

                dlgPage.iMinHao = 1
                dlgPage.iMaxHao = Me.pzhList.Items.Count

                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

                If dlgPage.ShowDialog = Windows.Forms.DialogResult.OK Then
                    If dlgPage.rbAll.Checked Then
                        Dim i As Integer

                        For i = 0 To Me.pzhList.Items.Count - 1
                            Dim obj As New FKPrn.Print(sPZH, iGongnengID, iDanZhengType)
                            sPZH = Me.pzhList.Items(i)

                            dsPrint = mdb.Reader("select  年份,月份,日期,凭证号,摘要,科目,科目全名 as 借方科目,对应科目,对应科目全名 as 贷方科目,SUM(借方金额) AS 金额,SUM(借方数量) as 借方数量,SUM(贷方数量) AS 贷方数量,SUM(贷方金额) AS 贷方金额 FROM 凭证表 WHERE  凭证号 ='" & sPZH & "' and 年份 = " & iNian & " and 月份=" & iYue & " and 借方金额<>0  group by 年份,月份,日期,凭证号,摘要,科目,科目全名,对应科目,对应科目全名")
                            'FillDG(sPZH)

                            obj.iNian = iNian
                            obj.iYue = iYue
                            obj.iPZSCFS = Me.iPZSCFS
                            obj.dsHuiZong = dsPrint

                            obj.Print()
                            'obj.PrintPreview()

                            obj = Nothing
                            System.GC.Collect()
                        Next
                    Else
                        Dim i As Integer

                        For i = dlgPage.nuMin.Value To dlgPage.nuMax.Value
                            sPZH = Me.pzhList.Items(i - 1)
                            Dim obj As New FKPrn.Print(sPZH, iGongnengID, iDanZhengType)

                            dsPrint = mdb.Reader("select  年份,月份,日期,凭证号,摘要,科目,科目全名 as 借方科目,对应科目,对应科目全名 as 贷方科目,SUM(借方金额) AS 金额,SUM(借方数量) as 借方数量,SUM(贷方数量) AS 贷方数量,SUM(贷方金额) AS 贷方金额 FROM 凭证表 WHERE  凭证号 ='" & sPZH & "' and 年份 = " & iNian & " and 月份=" & iYue & " and 借方金额<>0  group by 年份,月份,日期,凭证号,摘要,科目,科目全名,对应科目,对应科目全名")
                            'FillDG(sPZH)

                            obj.iNian = iNian
                            obj.iYue = iYue
                            obj.iPZSCFS = Me.iPZSCFS
                            obj.dsHuiZong = dsPrint

                            obj.Print()
                            'obj.PrintPreview()

                            obj = Nothing
                            System.GC.Collect()
                        Next
                    End If

                End If

        End Select
    End Sub

    Private Sub 预览_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 预览.Click
        Dim thP As New Threading.Thread(AddressOf PrintView)
        thP.Start()
    End Sub

    Private Sub 打印_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 打印.Click
        Dim thP As New Threading.Thread(AddressOf PrintOut)
        thP.Start()
    End Sub

    Private Sub FKSHPZ_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        Me.SetLK(Me.dgvPZ)
    End Sub

    Public Sub New(ByVal bYEB As Boolean, ByVal iYear As Integer)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        bFromYEB = bYEB

        iNian = iYear
    End Sub

    Private Sub 审核_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 审核.Click
        If Me.tssSHR.Text <> "" Then
            Exit Sub
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("update JiBenCaoZuo set 审核='" & FKG.myselfG.YongHu & "' where Nian=" & iNian & " and Yue=" & iYue & " and 删除='false' and PingZhengZi='" & sPZH.Substring(0, sPZH.IndexOf(" ")).ToString & "' and PingZhengHao=" & sPZH.Replace(sPZH.Substring(0, sPZH.IndexOf(" ")), "").Replace(" ", "") & " and 审核<>'对账'  and (审核='' or 审核 is null)")
        mdb.Write("update PZ set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & iNian & " and 月份=" & iYue & "  and 凭证号='" & sPZH & "'  and 审核<>'对账' and (审核='' or 审核 is null)")

        mdb.Write("update kucun set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & iNian & " and 月份=" & iYue & "  and 编号 in (select distinct 编号 from PZ where 凭证号='" & sPZH & "')  and 审核<>'对账' and (审核='' or 审核 is null)")
        mdb.Write("update gongzi set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & iNian & " and 月份=" & iYue & "  and 编号 in (select distinct 编号 from PZ where 凭证号='" & sPZH & "')  and 审核<>'对账' and (审核='' or 审核 is null)")

        setStatus(FKG.myselfG.YongHu)
    End Sub

    Private Sub 销审_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 销审.Click
        'If Me.tssSHR.Text = "" Then
        '    Exit Sub
        'End If
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.Reader("select 记帐 from PZ WHERE 年份=" & iNian & " and 月份=" & iYue & "  and 凭证号='" & sPZH & "'").Tables(0).Rows(0).Item(0) <> "" Then
            MsgBox("该凭证已经记帐,请先反记帐,然后再反取消审核!", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        mdb.Write("update JiBenCaoZuo set 审核='' where Nian=" & iNian & " and Yue=" & iYue & " and 删除='false' and PingZhengZi='" & sPZH.Substring(0, sPZH.IndexOf(" ")).ToString & "' and PingZhengHao=" & sPZH.Replace(sPZH.Substring(0, sPZH.IndexOf(" ")), "").Replace(" ", "") & " and 审核='" & FKG.myselfG.YongHu & "'")
        mdb.Write("update PZ set 审核='' where 年份=" & iNian & " and 月份=" & iYue & "  and 凭证号='" & sPZH & "'and 审核='" & FKG.myselfG.YongHu & "'")

        mdb.Write("update Kucun set 审核='' where 年份=" & iNian & " and 月份=" & iYue & "  and  编号 in (select distinct 编号 from PZ where 凭证号='" & sPZH & "')  and 审核='" & FKG.myselfG.YongHu & "'")
        mdb.Write("update GongZi set 审核='' where 年份=" & iNian & " and 月份=" & iYue & "  and  编号 in (select distinct 编号 from PZ where 凭证号='" & sPZH & "')  and 审核='" & FKG.myselfG.YongHu & "'")

        setStatus("")
    End Sub

    Private Sub 全审_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 全审.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If iPZSCFS = 0 Then
            mdb.Write("update JiBenCaoZuo set 审核='" & FKG.myselfG.YongHu & "' where Nian=" & iNian & " and Yue=" & iYue & " and 删除='false' and A_GNSID=" & iGongnengID & " and (审核='' or 审核 is null)")
            mdb.Write("update PZ set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & iNian & " and 月份=" & iYue & "  and A_GNSID=" & iGongnengID & " and (审核='' or 审核 is null)")
        Else
            mdb.Write("update JiBenCaoZuo set 审核='" & FKG.myselfG.YongHu & "' where Nian=" & iNian & " and Yue=" & iYue & " and 删除='false' and PingZhengZi='" & sPingZhengZi & "'" & " and (审核='' or 审核 is null)")
            mdb.Write("update PZ set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & iNian & " and 月份=" & iYue & "  and left(凭证号,charindex(' ',凭证号)-1) = '" & sPingZhengZi & "'" & " and (审核='' or 审核 is null)")

            mdb.Write("update kucun set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & iNian & " and 月份=" & iYue & "  and 编号 in (select distinct 编号 from pz where left(凭证号,charindex(' ',凭证号)-1) = '" & sPingZhengZi & "')" & " and (审核='' or 审核 is null)")
            mdb.Write("update gongzi set 审核='" & FKG.myselfG.YongHu & "' where 年份=" & iNian & " and 月份=" & iYue & "  and 编号 in (select distinct 编号 from pz where left(凭证号,charindex(' ',凭证号)-1) = '" & sPingZhengZi & "')" & " and (审核='' or 审核 is null)")


        End If
        setStatus(FKG.myselfG.YongHu)
    End Sub

    Private Sub 全销_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 全销.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.Reader("select jiZhang from FKZQ WHERE NY='" & iNian & iYue.ToString.PadLeft(2, "0") & "'").Tables(0).Rows(0).Item(0) = True Then
            MsgBox("当月已经记帐,请先反记帐,然后再反取消审核!", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        If iPZSCFS = 0 Then
            mdb.Write("update JiBenCaoZuo set 审核='' where Nian=" & iNian & " and Yue=" & iYue & " and 删除='false' and A_GNSID=" & iGongnengID & "  and 审核<>'对账' and 审核='" & FKG.myselfG.YongHu & "' ")
            mdb.Write("update PZ set 审核='' where 年份=" & iNian & " and 月份=" & iYue & "  and A_GNSID=" & iGongnengID & "  and 审核<>'对账' and 审核='" & FKG.myselfG.YongHu & "' ")
        Else
            mdb.Write("update JiBenCaoZuo set 审核='' where Nian=" & iNian & " and Yue=" & iYue & " and 删除='false' and PingZhengZi='" & sPingZhengZi & "'  and 审核<>'对账' and 审核='" & FKG.myselfG.YongHu & "' ")
            mdb.Write("update PZ set 审核='' where 年份=" & iNian & " and 月份=" & iYue & "  and left(凭证号,charindex(' ',凭证号)-1) = '" & sPingZhengZi & "'  and 审核<>'对账' and 审核='" & FKG.myselfG.YongHu & "' ")

            mdb.Write("update kucun set 审核='' where 年份=" & iNian & " and 月份=" & iYue & "  and  编号 in (select distinct 编号 from pz where left(凭证号,charindex(' ',凭证号)-1) = '" & sPingZhengZi & "')" & "  and 审核<>'对账' and 审核='" & FKG.myselfG.YongHu & "' ")
            mdb.Write("update gongzi set 审核='' where 年份=" & iNian & " and 月份=" & iYue & "  and  编号 in (select distinct 编号 from pz where left(凭证号,charindex(' ',凭证号)-1) = '" & sPingZhengZi & "')" & "  and 审核<>'对账' and 审核='" & FKG.myselfG.YongHu & "' ")

        End If

        setStatus("")
    End Sub

    Private Sub 关闭_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 关闭.Click
        Me.Dispose()
    End Sub

    Private Sub 连打_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 连打.Click
        Dim thP As New Threading.Thread(AddressOf LianXuPrintOut)
        thP.Start()
    End Sub
End Class