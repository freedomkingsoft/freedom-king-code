﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKSHPZ
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FKSHPZ))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GJ1 = New System.Windows.Forms.ToolStrip
        Me.审核 = New System.Windows.Forms.ToolStripButton
        Me.销审 = New System.Windows.Forms.ToolStripButton
        Me.全审 = New System.Windows.Forms.ToolStripButton
        Me.全销 = New System.Windows.Forms.ToolStripButton
        Me.预览 = New System.Windows.Forms.ToolStripButton
        Me.打印 = New System.Windows.Forms.ToolStripButton
        Me.连打 = New System.Windows.Forms.ToolStripButton
        Me.关闭 = New System.Windows.Forms.ToolStripButton
        Me.dgvPZ = New System.Windows.Forms.DataGridView
        Me.Label1 = New System.Windows.Forms.Label
        Me.GJ2 = New System.Windows.Forms.ToolStrip
        Me.首单 = New System.Windows.Forms.ToolStripButton
        Me.上单 = New System.Windows.Forms.ToolStripButton
        Me.下单 = New System.Windows.Forms.ToolStripButton
        Me.尾单 = New System.Windows.Forms.ToolStripButton
        Me.GJ4 = New System.Windows.Forms.ToolStrip
        Me.cbFPZHao = New System.Windows.Forms.ToolStripComboBox
        Me.显示凭证 = New System.Windows.Forms.ToolStripButton
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.tssState = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.tssSHR = New System.Windows.Forms.ToolStripStatusLabel
        Me.sst = New System.Windows.Forms.StatusStrip
        Me.tssNianYue = New System.Windows.Forms.ToolStripStatusLabel
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.lblPZH = New System.Windows.Forms.Label
        Me.lblRiQi = New System.Windows.Forms.Label
        Me.GJ1.SuspendLayout()
        CType(Me.dgvPZ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GJ2.SuspendLayout()
        Me.GJ4.SuspendLayout()
        Me.sst.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GJ1
        '
        Me.GJ1.AllowItemReorder = True
        Me.GJ1.Dock = System.Windows.Forms.DockStyle.None
        Me.GJ1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.审核, Me.销审, Me.全审, Me.全销, Me.预览, Me.打印, Me.连打, Me.关闭})
        Me.GJ1.Location = New System.Drawing.Point(0, 0)
        Me.GJ1.Name = "GJ1"
        Me.GJ1.Size = New System.Drawing.Size(408, 40)
        Me.GJ1.TabIndex = 2
        Me.GJ1.Text = "GJ1"
        '
        '审核
        '
        Me.审核.Image = CType(resources.GetObject("审核.Image"), System.Drawing.Image)
        Me.审核.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.审核.Name = "审核"
        Me.审核.Size = New System.Drawing.Size(51, 37)
        Me.审核.Text = "审核(&S)"
        Me.审核.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '销审
        '
        Me.销审.Image = Global.FKSHPZ.My.Resources.Resources.image341
        Me.销审.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.销审.Name = "销审"
        Me.销审.Size = New System.Drawing.Size(48, 37)
        Me.销审.Text = "销　审"
        Me.销审.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '全审
        '
        Me.全审.Image = Global.FKSHPZ.My.Resources.Resources.image693
        Me.全审.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.全审.Name = "全审"
        Me.全审.Size = New System.Drawing.Size(48, 37)
        Me.全审.Text = "全　审"
        Me.全审.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '全销
        '
        Me.全销.Image = Global.FKSHPZ.My.Resources.Resources.image333
        Me.全销.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.全销.Name = "全销"
        Me.全销.Size = New System.Drawing.Size(48, 37)
        Me.全销.Text = "全　销"
        Me.全销.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '预览
        '
        Me.预览.Image = Global.FKSHPZ.My.Resources.Resources.image025
        Me.预览.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.预览.Name = "预览"
        Me.预览.Size = New System.Drawing.Size(50, 37)
        Me.预览.Text = "预览(&L)"
        Me.预览.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '打印
        '
        Me.打印.Image = Global.FKSHPZ.My.Resources.Resources.image004
        Me.打印.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.打印.Name = "打印"
        Me.打印.Size = New System.Drawing.Size(51, 37)
        Me.打印.Text = "打印(P)"
        Me.打印.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '连打
        '
        Me.连打.Image = Global.FKSHPZ.My.Resources.Resources.image004
        Me.连打.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.连打.Name = "连打"
        Me.连打.Size = New System.Drawing.Size(54, 37)
        Me.连打.Text = "连打(O)"
        Me.连打.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '关闭
        '
        Me.关闭.Image = Global.FKSHPZ.My.Resources.Resources.image712
        Me.关闭.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.关闭.Name = "关闭"
        Me.关闭.Size = New System.Drawing.Size(54, 37)
        Me.关闭.Text = "关闭(&Q)"
        Me.关闭.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'dgvPZ
        '
        Me.dgvPZ.AllowUserToAddRows = False
        Me.dgvPZ.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvPZ.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvPZ.BackgroundColor = System.Drawing.Color.Lavender
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPZ.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPZ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgvPZ, 3)
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPZ.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvPZ.Location = New System.Drawing.Point(0, 205)
        Me.dgvPZ.Margin = New System.Windows.Forms.Padding(0)
        Me.dgvPZ.Name = "dgvPZ"
        Me.dgvPZ.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPZ.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvPZ.RowHeadersVisible = False
        Me.dgvPZ.RowTemplate.Height = 23
        Me.dgvPZ.Size = New System.Drawing.Size(828, 289)
        Me.dgvPZ.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label1, 3)
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(343, 151)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(142, 25)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "审  核  凭  证"
        '
        'GJ2
        '
        Me.GJ2.AllowItemReorder = True
        Me.GJ2.Dock = System.Windows.Forms.DockStyle.None
        Me.GJ2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.首单, Me.上单, Me.下单, Me.尾单})
        Me.GJ2.Location = New System.Drawing.Point(408, 0)
        Me.GJ2.Name = "GJ2"
        Me.GJ2.Size = New System.Drawing.Size(219, 40)
        Me.GJ2.TabIndex = 3
        '
        '首单
        '
        Me.首单.Image = Global.FKSHPZ.My.Resources.Resources.image151
        Me.首单.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.首单.Name = "首单"
        Me.首单.Size = New System.Drawing.Size(51, 37)
        Me.首单.Text = "首单(&Z)"
        Me.首单.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '上单
        '
        Me.上单.Image = Global.FKSHPZ.My.Resources.Resources.image152
        Me.上单.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.上单.Name = "上单"
        Me.上单.Size = New System.Drawing.Size(52, 37)
        Me.上单.Text = "上单(&X)"
        Me.上单.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '下单
        '
        Me.下单.Image = Global.FKSHPZ.My.Resources.Resources.image153
        Me.下单.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.下单.Name = "下单"
        Me.下单.Size = New System.Drawing.Size(52, 37)
        Me.下单.Text = "下单(&C)"
        Me.下单.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '尾单
        '
        Me.尾单.Image = Global.FKSHPZ.My.Resources.Resources.image154
        Me.尾单.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.尾单.Name = "尾单"
        Me.尾单.Size = New System.Drawing.Size(52, 37)
        Me.尾单.Text = "尾单(&V)"
        Me.尾单.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GJ4
        '
        Me.GJ4.AllowItemReorder = True
        Me.GJ4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GJ4.Dock = System.Windows.Forms.DockStyle.None
        Me.GJ4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cbFPZHao, Me.显示凭证})
        Me.GJ4.Location = New System.Drawing.Point(632, 0)
        Me.GJ4.Name = "GJ4"
        Me.GJ4.Size = New System.Drawing.Size(196, 40)
        Me.GJ4.TabIndex = 6
        '
        'cbFPZHao
        '
        Me.cbFPZHao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbFPZHao.Name = "cbFPZHao"
        Me.cbFPZHao.Size = New System.Drawing.Size(115, 40)
        '
        '显示凭证
        '
        Me.显示凭证.Image = CType(resources.GetObject("显示凭证.Image"), System.Drawing.Image)
        Me.显示凭证.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.显示凭证.Name = "显示凭证"
        Me.显示凭证.Size = New System.Drawing.Size(36, 37)
        Me.显示凭证.Text = "显示"
        Me.显示凭证.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(68, 17)
        Me.ToolStripStatusLabel1.Text = "审核状态："
        '
        'tssState
        '
        Me.tssState.AutoSize = False
        Me.tssState.Name = "tssState"
        Me.tssState.Size = New System.Drawing.Size(200, 17)
        Me.tssState.Text = "未审"
        Me.tssState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(56, 17)
        Me.ToolStripStatusLabel2.Text = "审核人："
        '
        'tssSHR
        '
        Me.tssSHR.AutoSize = False
        Me.tssSHR.Name = "tssSHR"
        Me.tssSHR.Size = New System.Drawing.Size(250, 17)
        Me.tssSHR.Text = "未审核"
        Me.tssSHR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'sst
        '
        Me.sst.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.tssState, Me.ToolStripStatusLabel2, Me.tssSHR, Me.tssNianYue})
        Me.sst.Location = New System.Drawing.Point(0, 494)
        Me.sst.Name = "sst"
        Me.sst.Size = New System.Drawing.Size(828, 22)
        Me.sst.SizingGrip = False
        Me.sst.TabIndex = 5
        Me.sst.Text = "StatusStrip1"
        '
        'tssNianYue
        '
        Me.tssNianYue.Name = "tssNianYue"
        Me.tssNianYue.Size = New System.Drawing.Size(32, 17)
        Me.tssNianYue.Text = "年月"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 224.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 196.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgvPZ, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.GJ1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.GJ4, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.GJ2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblPZH, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lblRiQi, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(828, 494)
        Me.TableLayoutPanel1.TabIndex = 6
        '
        'lblPZH
        '
        Me.lblPZH.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblPZH.AutoSize = True
        Me.lblPZH.Location = New System.Drawing.Point(177, 185)
        Me.lblPZH.Name = "lblPZH"
        Me.lblPZH.Size = New System.Drawing.Size(53, 12)
        Me.lblPZH.TabIndex = 7
        Me.lblPZH.Text = "凭证号："
        '
        'lblRiQi
        '
        Me.lblRiQi.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblRiQi.AutoSize = True
        Me.lblRiQi.Location = New System.Drawing.Point(709, 185)
        Me.lblRiQi.Name = "lblRiQi"
        Me.lblRiQi.Size = New System.Drawing.Size(41, 12)
        Me.lblRiQi.TabIndex = 7
        Me.lblRiQi.Text = "日期："
        '
        'FKSHPZ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(828, 516)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.sst)
        Me.MaximizeBox = False
        Me.Name = "FKSHPZ"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "审核凭证"
        Me.GJ1.ResumeLayout(False)
        Me.GJ1.PerformLayout()
        CType(Me.dgvPZ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GJ2.ResumeLayout(False)
        Me.GJ2.PerformLayout()
        Me.GJ4.ResumeLayout(False)
        Me.GJ4.PerformLayout()
        Me.sst.ResumeLayout(False)
        Me.sst.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GJ1 As System.Windows.Forms.ToolStrip
    Friend WithEvents 审核 As System.Windows.Forms.ToolStripButton
    Friend WithEvents GJ2 As System.Windows.Forms.ToolStrip
    Friend WithEvents 首单 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 上单 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 下单 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 尾单 As System.Windows.Forms.ToolStripButton
    Friend WithEvents GJ4 As System.Windows.Forms.ToolStrip
    Friend WithEvents 显示凭证 As System.Windows.Forms.ToolStripButton
    Friend WithEvents cbFPZHao As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents 预览 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 打印 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents dgvPZ As System.Windows.Forms.DataGridView
    Friend WithEvents 销审 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 全审 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 全销 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tssState As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tssSHR As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents sst As System.Windows.Forms.StatusStrip
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tssNianYue As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblPZH As System.Windows.Forms.Label
    Friend WithEvents lblRiQi As System.Windows.Forms.Label
    Friend WithEvents 关闭 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 连打 As System.Windows.Forms.ToolStripButton
End Class
