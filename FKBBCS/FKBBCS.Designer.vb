﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKBBCS
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FKBBCS))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnDel = New System.Windows.Forms.Button
        Me.btnBotton = New System.Windows.Forms.Button
        Me.btnTop = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.dgvXD = New System.Windows.Forms.DataGridView
        Me.XDID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.已选条件项 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.显示名称 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvKX = New System.Windows.Forms.DataGridView
        Me.可选条件项 = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvXD, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvKX, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(383, 444)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(78, 33)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "关闭 (&X)"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(161, 444)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(147, 33)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "保存设置 (&S)"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.AutoSize = True
        Me.btnDel.Image = CType(resources.GetObject("btnDel.Image"), System.Drawing.Image)
        Me.btnDel.Location = New System.Drawing.Point(181, 233)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(30, 26)
        Me.btnDel.TabIndex = 12
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'btnBotton
        '
        Me.btnBotton.AutoSize = True
        Me.btnBotton.Image = CType(resources.GetObject("btnBotton.Image"), System.Drawing.Image)
        Me.btnBotton.Location = New System.Drawing.Point(477, 233)
        Me.btnBotton.Name = "btnBotton"
        Me.btnBotton.Size = New System.Drawing.Size(30, 26)
        Me.btnBotton.TabIndex = 13
        Me.btnBotton.UseVisualStyleBackColor = True
        '
        'btnTop
        '
        Me.btnTop.AutoSize = True
        Me.btnTop.Image = CType(resources.GetObject("btnTop.Image"), System.Drawing.Image)
        Me.btnTop.Location = New System.Drawing.Point(477, 150)
        Me.btnTop.Name = "btnTop"
        Me.btnTop.Size = New System.Drawing.Size(30, 26)
        Me.btnTop.TabIndex = 11
        Me.btnTop.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.AutoSize = True
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(181, 150)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(30, 26)
        Me.btnAdd.TabIndex = 10
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'dgvXD
        '
        Me.dgvXD.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvXD.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvXD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvXD.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.XDID, Me.已选条件项, Me.显示名称})
        Me.dgvXD.Location = New System.Drawing.Point(224, 100)
        Me.dgvXD.Name = "dgvXD"
        Me.dgvXD.RowHeadersVisible = False
        Me.dgvXD.RowTemplate.Height = 23
        Me.dgvXD.Size = New System.Drawing.Size(239, 333)
        Me.dgvXD.TabIndex = 8
        '
        'XDID
        '
        Me.XDID.HeaderText = "XDID"
        Me.XDID.Name = "XDID"
        Me.XDID.Visible = False
        '
        '已选条件项
        '
        Me.已选条件项.HeaderText = "已选条件项"
        Me.已选条件项.Name = "已选条件项"
        Me.已选条件项.ReadOnly = True
        Me.已选条件项.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.已选条件项.Width = 115
        '
        '显示名称
        '
        Me.显示名称.HeaderText = "显示名称"
        Me.显示名称.Name = "显示名称"
        Me.显示名称.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgvKX
        '
        Me.dgvKX.AllowUserToAddRows = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvKX.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvKX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKX.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.可选条件项})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvKX.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvKX.Location = New System.Drawing.Point(32, 100)
        Me.dgvKX.Name = "dgvKX"
        Me.dgvKX.RowHeadersVisible = False
        Me.dgvKX.RowTemplate.Height = 23
        Me.dgvKX.Size = New System.Drawing.Size(142, 333)
        Me.dgvKX.TabIndex = 9
        '
        '可选条件项
        '
        Me.可选条件项.HeaderText = "可选条件项"
        Me.可选条件项.Name = "可选条件项"
        Me.可选条件项.ReadOnly = True
        Me.可选条件项.Width = 120
        '
        'FKBBCS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 488)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.btnBotton)
        Me.Controls.Add(Me.btnTop)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.dgvXD)
        Me.Controls.Add(Me.dgvKX)
        Me.MaximizeBox = False
        Me.Name = "FKBBCS"
        Me.Text = "设置用户可设定条件项"
        CType(Me.dgvXD, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvKX, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents btnBotton As System.Windows.Forms.Button
    Friend WithEvents btnTop As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents dgvXD As System.Windows.Forms.DataGridView
    Friend WithEvents dgvKX As System.Windows.Forms.DataGridView
    Friend WithEvents XDID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 已选条件项 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 显示名称 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 可选条件项 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
