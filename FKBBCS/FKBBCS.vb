﻿Public Class FKBBCS

    Dim sBBName As String
    Dim sKuBiao As String
    Public Sub New(ByVal sName As String, ByVal kubiao As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        sBBName = sName
        sKuBiao = kubiao
    End Sub

    Private Sub FKBBCS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        FillDGVKX()
    End Sub

    Private Sub FillDgvKX()
        If sKuBiao = "存储过程" Then
            Exit Sub
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsLie As DataSet
        dsLie = mdb.Reader("select * from " & sKuBiao & " where 1<1")
        Dim i As Integer

        For i = 0 To dsLie.Tables(0).Columns.Count - 2
            Me.dgvKX.Rows.Add()
            Me.dgvKX.Rows(i).Cells(0).Value = dsLie.Tables(0).Columns(i).ColumnName
        Next

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If IsNothing(Me.dgvKX.CurrentCell) Then
            Exit Sub
        Else
            Me.dgvXD.Rows.Add()
            Me.dgvXD.Rows(Me.dgvXD.Rows.Count - 1).Cells(1).Value = Me.dgvKX.CurrentCell.Value
            Me.dgvKX.Rows.RemoveAt(Me.dgvKX.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click
        If IsNothing(Me.dgvXD.CurrentCell) Then
            Exit Sub
        Else
            Me.dgvKX.Rows.Add()
            Me.dgvKX.Rows(Me.dgvKX.Rows.Count - 1).Cells(0).Value = Me.dgvXD.CurrentRow.Cells(1).Value
            Me.dgvXD.Rows.RemoveAt(Me.dgvXD.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTop.Click
        If Me.dgvXD.CurrentRow.Index = 0 Then
            Exit Sub
        Else
            Me.dgvXD.Rows.Insert(Me.dgvXD.CurrentRow.Index - 1, Me.dgvXD.CurrentRow.Cells(0).Value, Me.dgvXD.CurrentRow.Cells(1).Value, Me.dgvXD.CurrentRow.Cells(2).Value)

            Me.dgvXD.Rows.RemoveAt(Me.dgvXD.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnBotton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBotton.Click
        If Me.dgvXD.CurrentRow.Index = Me.dgvXD.Rows.Count - 1 Then
            Exit Sub
        Else
            Me.dgvXD.Rows.Insert(Me.dgvXD.CurrentRow.Index + 2, Me.dgvXD.CurrentRow.Cells(0).Value, Me.dgvXD.CurrentRow.Cells(1).Value, Me.dgvXD.CurrentRow.Cells(2).Value)
            Me.dgvXD.Rows.RemoveAt(Me.dgvXD.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose(True)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim sBBCS As String = ""
        Dim i As Integer
        For i = 0 To Me.dgvXD.Rows.Count - 1
            sBBCS = sBBCS & Me.dgvXD.Rows(i).Cells(1).Value & " " & Me.dgvXD.Rows(i).Cells(2).Value & ","
        Next
        If i > 0 Then
            sBBCS = sBBCS.Remove(sBBCS.Length - 1, 1)
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("update ZDYBB set YHKJTJ ='" & sBBCS & "' WHERE ConText='" & sBBName & "'")

        Me.Dispose()

    End Sub

  
    Private Sub dgvKX_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvKX.CellDoubleClick
        btnAdd_Click(sender, e)
    End Sub

    Private Sub dgvXD_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvXD.CellContentClick
        btnDel_Click(sender, e)
    End Sub
End Class