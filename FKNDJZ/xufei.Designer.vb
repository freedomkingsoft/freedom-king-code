﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xufei
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtjiqima = New System.Windows.Forms.TextBox
        Me.txtxufeima = New System.Windows.Forms.TextBox
        Me.btnok = New System.Windows.Forms.Button
        Me.btnyz = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblOk = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(51, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(269, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "请在交纳维护服务费后，将本机缴费机器码发送给"
        '
        'txtjiqima
        '
        Me.txtjiqima.Location = New System.Drawing.Point(152, 112)
        Me.txtjiqima.Name = "txtjiqima"
        Me.txtjiqima.ReadOnly = True
        Me.txtjiqima.Size = New System.Drawing.Size(238, 21)
        Me.txtjiqima.TabIndex = 1
        '
        'txtxufeima
        '
        Me.txtxufeima.Location = New System.Drawing.Point(152, 153)
        Me.txtxufeima.Name = "txtxufeima"
        Me.txtxufeima.Size = New System.Drawing.Size(173, 21)
        Me.txtxufeima.TabIndex = 1
        '
        'btnok
        '
        Me.btnok.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnok.Enabled = False
        Me.btnok.Location = New System.Drawing.Point(317, 203)
        Me.btnok.Name = "btnok"
        Me.btnok.Size = New System.Drawing.Size(103, 50)
        Me.btnok.TabIndex = 2
        Me.btnok.Text = "完成续费"
        Me.btnok.UseVisualStyleBackColor = True
        Me.btnok.Visible = False
        '
        'btnyz
        '
        Me.btnyz.Location = New System.Drawing.Point(196, 203)
        Me.btnyz.Name = "btnyz"
        Me.btnyz.Size = New System.Drawing.Size(103, 50)
        Me.btnyz.TabIndex = 2
        Me.btnyz.Text = "验证续费码"
        Me.btnyz.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(51, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(173, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "技术支持人员，索要续费验证码"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(81, 121)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "缴费机器码"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(81, 162)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 12)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "续费验证码"
        '
        'lblOk
        '
        Me.lblOk.AutoSize = True
        Me.lblOk.BackColor = System.Drawing.SystemColors.Control
        Me.lblOk.Font = New System.Drawing.Font("楷体_GB2312", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblOk.ForeColor = System.Drawing.Color.Red
        Me.lblOk.Location = New System.Drawing.Point(22, 216)
        Me.lblOk.Name = "lblOk"
        Me.lblOk.Size = New System.Drawing.Size(156, 19)
        Me.lblOk.TabIndex = 0
        Me.lblOk.Text = "续费验证码错误"
        Me.lblOk.Visible = False
        '
        'xufei
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 284)
        Me.Controls.Add(Me.btnyz)
        Me.Controls.Add(Me.btnok)
        Me.Controls.Add(Me.txtxufeima)
        Me.Controls.Add(Me.txtjiqima)
        Me.Controls.Add(Me.lblOk)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "xufei"
        Me.Text = "年度服务费续费"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtjiqima As System.Windows.Forms.TextBox
    Friend WithEvents txtxufeima As System.Windows.Forms.TextBox
    Friend WithEvents btnok As System.Windows.Forms.Button
    Friend WithEvents btnyz As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblOk As System.Windows.Forms.Label
End Class
