﻿Public Class FKNDJZ
    '年底结账

    '如果下一年度有月份已结账,则本年12月不能反结账

    Dim iNian As Integer = FKG.myselfG.NianFen
    Dim mdbD As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
    Public Function NDJZ() As Boolean
        '提示年度续费，续费完成才进行年度结账

        '云端版本无需续费，直接年度结账 更新时间：20240613 .之前的版本可以结账

        Dim dlg As New xufei
        If 1 = 1 OrElse dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            'mdbD.oleCommand.CommandTimeout = 300 '超时时间设置为150秒，默认为30秒
            CopyKMDM()

            CopyKFZQ()

            CopyKMYE()

            CopyKCYE()

            CopyGZYE()

            CopyWeiWaiYE()

            MsgBox("结账完成", MsgBoxStyle.Information, "提示")
            Return True
        Else
            MsgBox("抱歉，你没有完成续费，年度结账功能无法使用！", MsgBoxStyle.Information, "请缴费")
            Return False
        End If

    End Function

    Private Sub CopyKMDM()
        '不能删除科目代码，否则当下年度有数据输入，但上年度进行反结账后重新结账时，有可能因为删除科目代码而导致出错。
        '不能删除()
        '已完成更正，二次结账时将只复制下年度没有的科目。（上年度反结账后增加的科目）
        'CopyKMFZSC
        'mdbD.Write("delete from KMDM where Nian=" & iNian + 1)
        mdbD.Write("insert into KMDM (KMDM,Nian,ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe) select KMDM," & iNian + 1 & ",ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe from KMDM where Nian=" & iNian & "  and KMDM not in (select KMDM from KMDM where Nian=" & iNian + 1 & ")")

        'mdbD.Write("delete from KMFZSX where Nian=" & iNian + 1)
        mdbD.Write("insert into KMFZSX (科目代码,Nian,辅助属性名称,辅助属性值) select 科目代码," & iNian + 1 & ",辅助属性名称,辅助属性值 from KMFZSX where Nian=" & iNian & " and 科目代码 not in (select 科目代码 from KMFZSX WHERE NIAN=" & iNian + 1 & ")")
    End Sub

    Private Sub CopyKFZQ()
        '创建FKZQ
        Dim i As Integer
        Dim dateStart As Date
        dateStart = "#" & iNian + 1 & "-1" & "-1#"
        mdbD.Write("delete from FKZQ where NY like '" & dateStart.Year & "%'")
        For i = 1 To 12

            mdbD.Write("insert into FKZQ (NY,KSRQ,ZZRQ,KCJZ,GZJZ,JIZHANG,JIEZHANG) VALUES ('" & (iNian + 1).ToString & dateStart.Month.ToString.PadLeft(2, "0") & "','" & dateStart & "','" & dateStart.AddMonths(1).AddDays(-1) & "','false','false','false','false')")
            dateStart = dateStart.AddMonths(1)
        Next
    End Sub

    Private Sub CopyKMYE()
        mdbD.ExecStoredProcedure(FKG.myselfG.asasR, "CopyKMYE", "@iNian," & iNian)
        'mdbD.Write("delete from KMYE where Nian=" & iNian + 1)
        'mdbD.Write("insert into KMYE (kmdm,nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select 科目代码," & iNian + 1 & ",'',[12月借方期末余额],[12月借方期末数量],[12月贷方期末余额],[12月贷方期末数量],0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from 科目余额表 where 年份=" & iNian)

    End Sub

    Private Sub CopyKCYE()
        mdbD.ExecStoredProcedure(FKG.myselfG.asasR, "CopyKCYE", "@iNian," & iNian)

        'mdbD.Write("delete from KCYE where Nian=" & iNian + 1)
        'mdbD.Write("insert into KCYE (kmdm,nian,CangKuID,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select 科目代码," & iNian + 1 & ",仓库编号,'',[12月借方期末余额]-[12月贷方期末余额],[12月借方期末数量]-[12月贷方期末数量],0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from 库存余额表 where 年份=" & iNian)

    End Sub

    Private Sub CopyGZYE()
        mdbD.ExecStoredProcedure(FKG.myselfG.asasR, "CopyGZYE", "@iNian," & iNian)

        '新增，复制委外余额表
        mdbD.ExecStoredProcedure(FKG.myselfG.asasR, "CopyWWYE", "@iNian," & iNian)

        'mdbD.Write("delete from GZYE where Nian=" & iNian + 1)
        'mdbD.Write("insert into GZYE (kmdm,nian,wbdm,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select 科目代码," & iNian + 1 & ",'',[12月借方期末余额],[12月借方期末数量],[12月贷方期末余额],[12月贷方期末数量],0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from 工资余额表 where 年份=" & iNian)

    End Sub

    Private Sub CopyWeiWaiYE()
        mdbD.Write("delete from WeiWaiYE where Nian=" & iNian + 1)
        mdbD.Write("insert into WeiWaiYE (kmdm,nian,CangKuID,WWKHDM,ncjfye,ncjfsl,ncdfye,ncdfsl,jfje1,jfsl1,jfje2,jfsl2,jfje3,jfsl3,jfje4,jfsl4,jfje5,jfsl5,jfje6,jfsl6,jfje7,jfsl7,jfje8,jfsl8,jfje9,jfsl9,jfje10,jfsl10,jfje11,jfsl11,jfje12,jfsl12,jfje13,jfsl13,dfje1,dfsl1,dfje2,dfsl2,dfje3,dfsl3,dfje4,dfsl4,dfje5,dfsl5,dfje6,dfsl6,dfje7,dfsl7,dfje8,dfsl8,dfje9,dfsl9,dfje10,dfsl10,dfje11,dfsl11,dfje12,dfsl12,dfje13,dfsl13) select KMDM," & iNian + 1 & ",CangKuID,WWKHDM,0,ncjfsl-ncdfsl+jfsl1+jfsl2+jfsl3+jfsl4+jfsl5+jfsl6+jfsl7+jfsl8+jfsl9+jfsl10+jfsl11+jfsl12-dfsl1-dfsl2-dfsl3-dfsl4-dfsl5-dfsl6-dfsl7-dfsl8-dfsl9-dfsl10-dfsl11-dfsl12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 from WeiWaiYE where nian=" & iNian)

    End Sub
End Class
