﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComDB
{
    public class COMDB222
    {
        public static void CompactAccessDB123(string connectionString, string mdwfilename)
        {
            string sDFile = connectionString.Replace("FreedomKingData.mdb", "tempData.mdb");
            //string sDFilename = Environment.CurrentDirectory +  "\\FKData\\tempData.mdb";
            //string sDFilename = Application.Info.DirectoryPath + "\\FKData\\tempData.mdb";
            //string sDFilename =Directory.GetCurrentDirectory() + "\\FKData\\tempData.mdb";
            string sDFilename = System.AppDomain.CurrentDomain.BaseDirectory + "\\FKData\\tempData.mdb";

            object[] oParams;
            //create an inctance of a Jet Replication Object
            object objJRO =
              Activator.CreateInstance(Type.GetTypeFromProgID("JRO.JetEngine"));
            //filling Parameters array
            //cnahge "Jet OLEDB:Engine Type=5" to an appropriate value
            // or leave it as is if you db is JET4X format (access 2000,2002)
            //(yes, jetengine5 is for JET4X, no misprint here)

            oParams = new object[] { 
            connectionString,
          sDFile};
            //    oParams = new object[] { 
        //connectionString,
        //"Provider=Microsoft.Jet.OLEDB.4.0;Data" + 
        //" Source=C:\\tempdb.mdb;jet oledb:database password=Zhao04Dan03Tong09;Jet OLEDB:Engine Type=5"};

            //invoke a CompactDatabase method of a JRO object
            //pass Parameters array
            try
            {
                objJRO.GetType().InvokeMember("CompactDatabase",
               System.Reflection.BindingFlags.InvokeMethod,
               null,
               objJRO,
               oParams);
            }
            catch
            {
                return;
            }

            //database is compacted now
            //to a new file C:\\tempdb.mdw
            //let's copy it over an old one and delete it
            System.IO.File.Delete(mdwfilename);
            System.IO.File.Move(sDFilename, mdwfilename);
            //clean up (just in case)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(objJRO);
            objJRO = null;
        }

    }
}
