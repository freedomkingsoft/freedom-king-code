﻿Public Class FKGN
    Public sZBMC As String
    Dim sObjName As String
    Dim mouseP As Drawing.Point
    Dim ctrlP As Drawing.Point

    '保存各表格数据源
    Dim dvKuCun As DataView
    Dim dvGongZi As DataView
    Dim dvPZ As DataView

    Dim dvKCLK, dvKCDot As String
    Dim dvGZLK, dvGZDot As String
    Dim dvPZLK, dvPZDot As String

    Dim BHList As New System.Windows.Forms.ComboBox

    '20170204 增加根据编号进行付款的特殊功能，在本模块打开，调用本模块，显示为 备用30 
    Dim sFuKuanBianHao As String = ""

    Dim iPZSCFS As Integer '凭证生成方式
    Public WriteOnly Property myPZSCFS() As Integer
        Set(ByVal value As Integer)
            iPZSCFS = value
        End Set
    End Property

    Dim iPZZhangShu As Integer '单据张数，生成一张凭证
    Public WriteOnly Property myPZZhangShu() As Integer
        Set(ByVal value As Integer)
            iPZZhangShu = value
        End Set
    End Property

    Dim KM1, KM2 As New FKG.myselfG.KM

    Private iGongnengID As Integer
    Public WriteOnly Property myGongneng() As Integer
        Set(ByVal value As Integer)
            iGongnengID = value
        End Set
    End Property

    Private sGongnengName As String
    Public WriteOnly Property myGongNengName() As String
        Set(ByVal value As String)
            sGongnengName = value
        End Set
    End Property

    Dim sBianHao As String
    Dim iWorkState As Integer  '0为新增单证，1为新增记录，2为修改记录，3为删除记录,9为浏览记录
    Dim iJiBenID As String

    '使用一个一直打开的只读Connect，看能否加快响应速度。
    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
    Private Sub OpenMDB()
        mdb.oleConnect.Open()
    End Sub

    Private Sub FKGN_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        FKG.myselfG.isOpendedGN = False
        mdb.oleConnect.Close()
        GC.Collect()
    End Sub

    Private Function YanZhengZhuangTai() As Boolean
        'Dim bState As Boolean = False
        '如果库存已结账,则涉及库存操作的功能不可新增和修改
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdb.Readeropen("select KCJZ from FKZQ where NY='" & FKG.myselfG.NianFen.ToString & FKG.myselfG.YueFen.ToString.PadLeft(2, "0") & "'").Tables(0).Rows(0).Item(0) = True Then
            If mdb.ReaderOpen("select * from GN_KuCun where GNSID=" & iGongnengID).Tables(0).Rows.Count > 0 Then
                Me.新增单证.Enabled = False
                Me.新增记录.Enabled = False
                Me.修改记录.Enabled = False
                Me.删除记录.Enabled = False
                Me.保存.Enabled = False

                Me.tssZhuangtai.Text = "库存已结账"
                Return True
            End If
        End If
        '如果工资已结账,则涉及工资的功能不可修改
        If mdb.Readeropen("select GZJZ from FKZQ where NY='" & FKG.myselfG.NianFen.ToString & FKG.myselfG.YueFen.ToString.PadLeft(2, "0") & "'").Tables(0).Rows(0).Item(0) = True Then
            If mdb.Readeropen("select * from GN_GongZi where GNSID=" & iGongnengID).Tables(0).Rows.Count > 0 Then
                Me.新增单证.Enabled = False
                Me.新增记录.Enabled = False
                Me.修改记录.Enabled = False
                Me.删除记录.Enabled = False
                Me.保存.Enabled = False

                Me.tssZhuangtai.Text = "工资已结账"
                Return True
            End If
        End If
        '如果凭证已审核,则对应单证不可修改,对审核的判断要放到FILLDG中去.

        '如果当前已结账,则所有数据不可修改
        If mdb.Readeropen("select JieZhang from FKZQ where NY='" & FKG.myselfG.NianFen.ToString & FKG.myselfG.YueFen.ToString.PadLeft(2, "0") & "'").Tables(0).Rows(0).Item(0) = True Then
            Me.新增单证.Enabled = False
            Me.新增记录.Enabled = False
            Me.修改记录.Enabled = False
            Me.删除记录.Enabled = False
            Me.保存.Enabled = False

            Me.tssZhuangtai.Text = "当月已结账"
            Return True
        End If

        Me.tssZhuangtai.Text = "就绪"

    End Function

    Private Sub FKGN_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = My.Application.Info.Title & " - 录入修改 " & FKG.myselfG.NianFen & " 年" & FKG.myselfG.YueFen & " 月的单据"
        Me.Icon = FKG.myselfG.FKIcon

        '打开数据库，并且保持连接的打开
        OpenMDB()

        Me.iniSetupData() '初始化所需数据

        Me.zbmc.Text = sZBMC
        SetGJLocation() '设置工具拦位置
        FillControlsFKXS()

        Dim obj As System.Windows.Forms.Control
        For Each obj In Me.gbSelect.Controls
            FillControlsText(obj) '填充默认值
        Next

        FillBHList()

        FillCangKu()

        YanZhengZhuangTai()

        '20200828升级，借方科目，贷方科目自动完成
        'FillFDKMAutoComplete()

        If Me.tssZhuangtai.Text = "就绪" Then
            DispSize(True)
            新增单证_Click(sender, e)
            DispSize(False)

        Else
            '设置显示尺寸
            DispSize(True)
            FillDG("-1")
            DispSize(False)

            HuanYuan(True)
        End If
        ''End If

        If Me.iPZSCFS = 1 Then
            Me.打印凭证.Visible = False
        End If

        setYETS()
        setDaYinSheZhi()
        Me.启用语音提示.Checked = My.Settings.YYTS

        bAutoSave = False '第一次加载时，自动保存均为False

        '如果来自特殊功能，将传递过来的参数赋值给 备用30
        If sFuKuanBianHao = "" Then
        Else
            Me.Beiyong30.Text = sFuKuanBianHao
        End If

        Me.ShowTSBCMD()

        '判断是否到期，到期则隐藏保存按钮
        If Now.Date > FKG.myselfG.YunUserInfo.Item("VIPMaturitydate") Then
            Me.保存.Visible = False
        End If
        '判断用户级别，超级用户隐藏保存按钮,以便于精确的控制用户数量
        If mdb.Reader("select YonghuJiBie from YongHu where Yonghuid=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) > 4 Then
            Me.保存.Visible = False
            Me.保存.Enabled = False
        End If

        If Me.Height < 300 Then
            Me.Height = 300
        End If
        If Me.Width < 350 Then
            Me.Width = 350
        End If

        'iniSetupData() '初始化所需数据

        Dim th As New Threading.Thread(AddressOf YanZhengBanben)
        th.Start()
    End Sub

    'Private Sub FillFDKMAutoComplete()
    '    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
    '    Dim ds As DataSet
    '    Dim sZhuKeMu As String

    '    sZhuKeMu = Me.DaiFangZhuKeMu.FKShuXing.Zhi
    '    If sZhuKeMu = "" Then
    '        ds = mdb.Reader("select distinct kmmc from kmdm where SFMJ=1 and Nian=" & FKG.myselfG.NianFen)
    '    Else
    '        sZhuKeMu = sZhuKeMu.Replace(",", "','")
    '        sZhuKeMu = "'" & sZhuKeMu & "'"
    '        ds = mdb.Reader("select kmmc from kmdm where kmdm in (" & sZhuKeMu & ") and SFMJ=1 and Nian=2020")
    '    End If

    '    Me.DaiFangKeMu.AutoCompleteMode = Windows.Forms.AutoCompleteMode.SuggestAppend
    '    Me.DaiFangKeMu.AutoCompleteSource = Windows.Forms.AutoCompleteSource.CustomSource
    '    Dim BDSC As New Windows.Forms.AutoCompleteStringCollection

    '    If IsNothing(ds) OrElse ds.Tables.Count = 0 Then
    '        Me.DaiFangKeMu.AutoCompleteCustomSource = Nothing
    '        '.Text = ""
    '        Exit Sub
    '    End If
    '    Dim i As Integer
    '    For i = 0 To ds.Tables(0).Rows.Count - 1
    '        BDSC.Add(ds.Tables(0).Rows(i).Item(0).ToString)
    '    Next
    '    Me.DaiFangKeMu.AutoCompleteCustomSource = BDSC

    '    sZhuKeMu = Me.JieFangZhuKemu.FKShuXing.Zhi
    '    If sZhuKeMu = "" Then
    '        ds = mdb.Reader("select distinct kmmc from kmdm where SFMJ=1 and Nian=" & FKG.myselfG.NianFen)
    '    Else
    '        sZhuKeMu = sZhuKeMu.Replace(",", "','")
    '        sZhuKeMu = "'" & sZhuKeMu & "'"
    '        ds = mdb.Reader("select kmmc from kmdm where kmdm in (" & sZhuKeMu & ") and SFMJ=1 and Nian=2020")
    '    End If

    '    Me.JieFangKeMu.AutoCompleteMode = Windows.Forms.AutoCompleteMode.SuggestAppend
    '    Me.JieFangKeMu.AutoCompleteSource = Windows.Forms.AutoCompleteSource.CustomSource
    '    Dim BDSCJF As New Windows.Forms.AutoCompleteStringCollection

    '    If IsNothing(ds) OrElse ds.Tables.Count = 0 Then
    '        Me.JieFangKeMu.AutoCompleteCustomSource = Nothing
    '        '.Text = ""
    '        Exit Sub
    '    End If
    '    'Dim i As Integer
    '    For i = 0 To ds.Tables(0).Rows.Count - 1
    '        BDSCJF.Add(ds.Tables(0).Rows(i).Item(0).ToString)
    '    Next
    '    Me.JieFangKeMu.AutoCompleteCustomSource = BDSC

    'End Sub

    Private Sub YanZhengBanben()
        '在此处验证是否为使用版
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ws As New wsbdsp.Service
        If ws.HelloWorld(getToken(MCO(CPUID, HID))) <> "" Then
            Exit Sub
        End If

        If FKG.myselfG.sBanBen = "试用版" OrElse FKG.myselfG.sVersion = "2.0" Then
            Dim iTimes As Integer = Int(Rnd() * 10) + 111
            If mdb.ReaderOpen("select count(*) from JiBenCaoZuo ").Tables(0).Rows(0).Item(0) > iTimes Then
                '更改数据库关键结构
                mdb.Write("update KMDM set SFMJ=0")
                mdb.Write("update Yonghu set YongHumima='" & FKG.myselfG.dLogin & "'")
                FKG.myselfG.setConStr("")
            End If
        End If

        If Now.Minute > 20 And Now.Minute < 35 Then
        Else
            Exit Sub
        End If

        Dim sPubCode As String
        sPubCode = mdb.ReaderOpen("select sPubKey from systemp where sComputerName='" & Me.MCO(CPUID, HID) & "'").Tables(0).Rows(0).Item(0).ToString

        If sPubCode = "" Then
            '更改数据库关键结构
            mdb.Write("update KMDM set SFMJ=0")
            mdb.Write("update Yonghu set YongHumima='" & FKG.myselfG.dLogin & "'")
            FKG.myselfG.setConStr("")
        End If
        '已改为从MDB读取


        sPubCode = Decrypt(sPubCode, My.Application.Info.DirectoryPath & "\pra.key")
        sPubCode = sPubCode.Remove(0, sPubCode.IndexOf("</BitStrength>") + 14)

        FKG.myselfG.sMC = MCO(CPUID, HID)

        '试用版进行数据量验证
        Select Case FKG.myselfG.sBanBen
            Case "开发版"

            Case "正式版"

            Case Else
                Dim iTimes As Integer = Int(Rnd() * 10) + 111
                If mdb.ReaderOpen("select count(*) from JiBenCaoZuo ").Tables(0).Rows(0).Item(0) > iTimes Then
                    '更改数据库关键结构
                    mdb.Write("update KMDM set SFMJ=0")
                    mdb.Write("update Yonghu set YongHumima='" & FKG.myselfG.dLogin & "'")
                    FKG.myselfG.setConStr("")
                End If
        End Select

        FKG.myselfG.sPubKey = sPubCode.Substring(0, 755)
        'FKG.myselfG.sPubKey = sPubCode.Substring(0, 755 + 15)
        sPubCode = sPubCode.Remove(0, FKG.myselfG.sPubKey.Length + 9)

        FKG.myselfG.sRC = (sPubCode.Substring(0, sPubCode.IndexOf("</RedCode>")))

        Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(FKG.myselfG.sMC), FKG.myselfG.sRC)
            Case True
            Case False
                '更改数据库关键结构
                mdb.Write("update KMDM set SFMJ=0")
                mdb.Write("update Yonghu set YongHumima='" & FKG.myselfG.dLogin & "'")
                FKG.myselfG.setConStr("")
        End Select
    End Sub

    Private Sub setDaYinSheZhi()
        If Me.dgvKuCun.Visible Then
            Me.打印库存单证.Visible = True
            Me.打印库存单证.Checked = True
        Else
            Me.打印库存单证.Visible = False
            Me.打印库存单证.Checked = False
        End If

        If Me.dgvGongZi.Visible Then
            Me.打印工资单证.Visible = True
            If Me.dgvKuCun.Visible = False Then
                Me.打印工资单证.Checked = True
            End If
        Else
            Me.打印工资单证.Visible = False
            Me.打印工资单证.Checked = False
        End If

        If Me.dgvPZ.Visible Then
            Me.打印凭证.Visible = True
            If Me.dgvKuCun.Visible = False AndAlso Me.dgvGongZi.Visible = False Then
                Me.打印凭证.Checked = True
            End If
        Else
            Me.打印凭证.Visible = False
            Me.打印凭证.Checked = False
        End If

    End Sub

    Private Sub setYETS()

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsYETS As DataSet
        'dsYETS = mdb.Readeropen("select iif(isnull(JFYETS),'1',JFYETS),iif(isnull(DFYETS),'1',DFYETS) from gongnengshu where id=" & iGongnengID)
        dsYETS = mdb.ReaderOpen("select isnull(JFYETS,'1'),isnull(DFYETS,'1') from gongnengshu where id=" & iGongnengID)
        My.Settings.YETSJ = dsYETS.Tables(0).Rows(0).Item(0)
        My.Settings.YETSD = dsYETS.Tables(0).Rows(0).Item(1)

        If My.Settings.YETSJ = "1" Then
            Me.JYE.Checked = True
            Me.JFSL.Checked = False
            Me.JClose.Checked = False
        ElseIf My.Settings.YETSJ = "9" Then
            Me.JYE.Checked = False
            Me.JFSL.Checked = False
            Me.JClose.Checked = True
        Else
            Me.JYE.Checked = False
            Me.JFSL.Checked = True
            Me.JClose.Checked = False
            Select Case My.Settings.YETSJ
                Case "2"
                    Me.包括所有仓库.Checked = True
                    Me.不包括废品库.Checked = False
                    Me.不包括外存库.Checked = False
                    Me.不包括废品库和外存库.Checked = False
                Case "3"
                    Me.包括所有仓库.Checked = False
                    Me.不包括废品库.Checked = True
                    Me.不包括外存库.Checked = False
                    Me.不包括废品库和外存库.Checked = False
                Case "4"
                    Me.包括所有仓库.Checked = False
                    Me.不包括废品库.Checked = False
                    Me.不包括外存库.Checked = True
                    Me.不包括废品库和外存库.Checked = False
                Case "5"
                    Me.包括所有仓库.Checked = False
                    Me.不包括废品库.Checked = False
                    Me.不包括外存库.Checked = False
                    Me.不包括废品库和外存库.Checked = True
                Case Else

            End Select
        End If

        If My.Settings.YETSD = "1" Then
            Me.DYE.Checked = True
            Me.DFSL.Checked = False
            Me.DClose.Checked = False
        ElseIf My.Settings.YETSD = "9" Then
            Me.DYE.Checked = False
            Me.DFSL.Checked = False
            Me.DClose.Checked = True
        Else
            Me.DFSL.Checked = True
            Me.DYE.Checked = False
            Me.DClose.Checked = False
            Select Case My.Settings.YETSD
                Case "2"
                    Me.包括所有仓库贷方.Checked = True
                    Me.不包括废品库贷方.Checked = False
                    Me.不包括外存库贷方.Checked = False
                    Me.不包括废品库和外存库贷方.Checked = False
                Case "3"
                    Me.包括所有仓库贷方.Checked = False
                    Me.不包括废品库贷方.Checked = True
                    Me.不包括外存库贷方.Checked = False
                    Me.不包括废品库和外存库贷方.Checked = False
                Case "4"
                    Me.包括所有仓库贷方.Checked = False
                    Me.不包括废品库贷方.Checked = False
                    Me.不包括外存库贷方.Checked = True
                    Me.不包括废品库和外存库贷方.Checked = False
                Case "5"
                    Me.包括所有仓库贷方.Checked = False
                    Me.不包括废品库贷方.Checked = False
                    Me.不包括外存库贷方.Checked = False
                    Me.不包括废品库和外存库贷方.Checked = True
            End Select
        End If
    End Sub

    Private Sub FillBHList()
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.ReaderOpen("select DISTINCT  Bianhao from jibencaozuo where A_GNSID=" & iGongnengID & " and nian=" & FKG.myselfG.NianFen & " and Yue=" & FKG.myselfG.YueFen & " and 删除='false' order by bianhao")
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            BHList.Items.Add(ds.Tables(0).Rows(i).Item(0))
        Next

        If i = 0 Then
            Me.首单.Enabled = False
            Me.上单.Enabled = False
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        End If
    End Sub


    Private Sub JiSuanTextBoxGongShi(ByVal obj As System.Windows.Forms.Control, Optional ByVal bSuoDing As Boolean = False)
        If bSuoDing Then
            Exit Sub
        End If
        '计算公式
        If obj.GetType.Name = "FKTextBox" Then
            With CType(obj, Freedom.FKTextBox)
                If .FKShuXing.GeShi = "公式" Then
                    .Text = YunSuan(.FKShuXing.Zhi)
                Else
                    '.Text = .FKShuXing.Zhi
                End If

                Select Case .Name
                    Case "JieFangZhuKemu", "DaiFangZhuKeMu"
                        .Text = FKF.FKF.getKMQM(" KMDM='" & .FKShuXing.Zhi & "'")
                    Case "JieFangKeMu", "DaiFangKeMu"
                        If .Text = "" OrElse FKF.FKF.isMJKM(.FKShuXing.Zhi) = False Then
                            .Text = ""
                        Else
                            .Text = FKF.FKF.getKMMC(" KMDM='" & .FKShuXing.Zhi & "'")
                        End If
                End Select
            End With
        End If
    End Sub

    Private Sub FillControlsFKXS() '填充各控件的自定义属性,仅仅在加载窗体的时候运行一次

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet

        ds = mdb.Readeropen("select gongneng.* from Gongneng,Gongnengshu where gongneng.gnsID=gongnengshu.ID and gongnengshu.id=" & iGongnengID)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            sObjName = ds.Tables(0).Rows(i).Item(0)
            Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
                Case "FKLable"

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = "无效"
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = 20
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case "FKTextBox"

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = ds.Tables(0).Rows(i).Item("GeShi")
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = ds.Tables(0).Rows(i).Item("TabIndex")
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case "FKComboBox"

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = "无效"
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = ds.Tables(0).Rows(i).Item("TabIndex")
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case "FKDateTimePicker"

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = "无效"
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = 20
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case "PictureBox"

                Case Else
                    'MsgBox("意外的控件类型", MsgBoxStyle.Information, "提示")
            End Select
        Next
        Me.BianHao.FKShuXing.SuoDing = True
        Me.PingZhengHao.FKShuXing.SuoDing = True

    End Sub

    Private Sub FillControlsText(ByVal obj As Object)
        Select Case obj.GetType.Name
            Case "FKLable"
                With CType(obj, Freedom.FKLable)
                    .Visible = .FKShuXing.QiYong
                    ctrlP.X = .FKShuXing.X
                    ctrlP.Y = .FKShuXing.Y
                    .Location = ctrlP
                    ctrlP.X = .FKShuXing.W
                    ctrlP.Y = .FKShuXing.H
                    .Size = ctrlP
                    .TabIndex = .FKShuXing.Tab
                    .ForeColor = .FKShuXing.YanSe
                    '.readonly = .FKShuXing.ZhiDu

                    .Text = .FKShuXing.Zhi
                    .Font = Me.StringToFont(.FKShuXing.ZiTi)
                End With
            Case "FKTextBox"
                With CType(obj, Freedom.FKTextBox)
                    .Visible = .FKShuXing.QiYong
                    ctrlP.X = .FKShuXing.X
                    ctrlP.Y = .FKShuXing.Y
                    .Location = ctrlP
                    ctrlP.X = .FKShuXing.W
                    ctrlP.Y = .FKShuXing.H
                    .Size = ctrlP
                    .TabIndex = .FKShuXing.Tab
                    .ForeColor = .FKShuXing.YanSe
                    .ReadOnly = .FKShuXing.ZhiDu
                    .Text = .FKShuXing.Zhi
                    .Font = Me.StringToFont(.FKShuXing.ZiTi)

                End With

            Case "FKComboBox"

                With CType(obj, Freedom.FKComboBox)
                    .Visible = .FKShuXing.QiYong
                    ctrlP.X = .FKShuXing.X
                    ctrlP.Y = .FKShuXing.Y
                    .Location = ctrlP
                    ctrlP.X = .FKShuXing.W
                    ctrlP.Y = .FKShuXing.H
                    .Size = ctrlP
                    .TabIndex = .FKShuXing.Tab
                    .ForeColor = .FKShuXing.YanSe
                    .Enabled = Not .FKShuXing.ZhiDu
                    .Font = Me.StringToFont(.FKShuXing.ZiTi)

                    .Text = .FKShuXing.Zhi
                End With
            Case "FKDateTimePicker"
                With CType(obj, Freedom.FKDateTimePicker)
                    .Visible = .FKShuXing.QiYong
                    ctrlP.X = .FKShuXing.X
                    ctrlP.Y = .FKShuXing.Y
                    .Location = ctrlP
                    ctrlP.X = .FKShuXing.W
                    ctrlP.Y = .FKShuXing.H
                    .Size = ctrlP
                    .TabIndex = .FKShuXing.Tab
                    .ForeColor = .FKShuXing.YanSe
                    .Enabled = Not .FKShuXing.ZhiDu
                    .Font = Me.StringToFont(.FKShuXing.ZiTi)

                    .Text = .FKShuXing.Zhi
                End With
            Case "PictureBox"

            Case Else
                'MsgBox("意外的控件类型")
        End Select
    End Sub


    Private Sub BangdingOBJ(ByVal obj As System.Windows.Forms.Control)
        Select Case obj.GetType.Name
            Case "FKLable"

            Case "FKTextBox"
                With CType(obj, Freedom.FKTextBox)
                    If .FKShuXing.BangDing = "" Then
                        Exit Select
                    Else
                        '执行自动完成
                        .AutoCompleteMode = Windows.Forms.AutoCompleteMode.SuggestAppend
                        .AutoCompleteSource = Windows.Forms.AutoCompleteSource.CustomSource
                        Dim BDSC As New Windows.Forms.AutoCompleteStringCollection
                        Dim ds As DataSet
                        ds = JiSuanBangding(.FKShuXing.BangDing)
                        If IsNothing(ds) OrElse ds.Tables.Count = 0 Then
                            .AutoCompleteCustomSource = Nothing
                            .Text = ""
                            Exit Sub
                        End If
                        Dim i As Integer
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            BDSC.Add(ds.Tables(0).Rows(i).Item(0).ToString)
                        Next
                        .AutoCompleteCustomSource = BDSC
                        If BDSC.Count > 0 Then
                            If .FKShuXing.ZHI = "-1" Then
                                .Text = BDSC.Item(0) '当将该控件的默认值设置为 “-1”时，设置最上面的选项被选中。否则填充默认值或留空
                            Else

                            End If
                        Else
                            .Text = ""
                        End If
                        '.FKShuXing.Zhi = .Text
                    End If
                End With

            Case "FKComboBox"

                With CType(obj, Freedom.FKComboBox)
                    If .FKShuXing.BangDing = "" Then
                        Exit Select
                    Else

                        '执行自动完成
                        Dim ds As DataSet
                        ds = JiSuanBangding(.FKShuXing.BangDing)
                        If IsNothing(ds) Then
                            .DataSource = Nothing
                            .Text = ""
                            Exit Sub
                        End If
                        .DataSource = ds.Tables(0)
                        .DisplayMember = ds.Tables(0).Columns(0).ColumnName

                        If .Items.Count > 0 Then
                            '.Text = .Items(0).ToString
                            If .FKShuXing.Zhi = "-1" Then
                                .SelectedIndex = 0 '当将该控件的默认值设置为 “-1”时，设置最上面的选项被选中。否则填充默认值或留空
                            Else
                                .Text = .FKShuXing.Zhi
                            End If
                        Else
                            .Text = .FKShuXing.Zhi
                        End If
                     
                    End If
                End With
            Case "FKDateTimePicker"
            Case "PictureBox"

            Case Else
                'MsgBox("意外的控件类型")
        End Select
        'Next
    End Sub

    '以下为绑定数据函数
    Private Function JiSuanBangding(ByVal sBangDing As String) As DataSet
        'Dim sBD As String
        Try
            If sBangDing.StartsWith("V_FZSX", StringComparison.CurrentCultureIgnoreCase) Then
                Return V_FZSX(sBangDing)
            ElseIf sBangDing.ToUpper.StartsWith("V_KM") Then
                Return V_KM(sBangDing)
            ElseIf sBangDing.StartsWith("V_TMCKM", StringComparison.CurrentCultureIgnoreCase) Then
                Return V_TMCKM(sBangDing)
                '增加V_Price的绑定
            ElseIf sBangDing.StartsWith("V_Price", StringComparison.CurrentCultureIgnoreCase) Then
                Return V_Price(sBangDing)
            ElseIf sBangDing.StartsWith("V_BDCCGC", StringComparison.CurrentCultureIgnoreCase) Then
                Return V_BDCCGC(sBangDing)
            Else
                MsgBox("目前没有此公式", MsgBoxStyle.Information, "提示")
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    '以下为绑定数据函数
    Private Function V_BDCCGC(ByVal sR As String) As DataSet
        sR = sR.ToUpper.Replace("V_BDCCGC(", "").Replace(")", "")

        Dim sCanShu() As String
        sCanShu = Split(sR, ",")
        '格式为'V_BDCCGC(存储过程名,存储过程参数)  参数格式为 参数名,参数值;参数名,参数值
        '邦定指定存储过程的计算得出的序列。
        Dim dResult As DataSet

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Try
            Dim sCCGCName As String = sCanShu(0)

            '  Dim sPName As String
            If sCanShu.Length > 1 Then
                Dim sPValue As String = ""

                Dim sTemp As String
                sTemp = sR.Remove(0, sCanShu(0).Length + 1)

                Dim sP() As String
                sP = Split(sTemp, ";")
                Dim ssP As String
                For Each ssP In sP
                    sPValue = sPValue & Split(ssP, ",")(0).ToString & "," & YunSuan(Split(ssP, ",")(1).ToString) & ";"
                Next
                sPValue = sPValue.Remove(sPValue.Length - 1, 1)

                'dResult = mdb.ReadStoredProcedure(FKG.myselfG.asasR, sCanShu(1), sPValue).Tables(0).Rows(0).Item(0)
                dResult = mdb.ReadStoredProcedure(FKG.myselfG.asasR, sCanShu(0), sPValue)
            Else
                dResult = mdb.ReadStoredProcedure(FKG.myselfG.asasR, sCCGCName, "")
            End If
           
        Catch ex As Exception
            dResult = Nothing
        End Try

        Return dResult
    End Function

    Private Function V_Price(ByVal sR As String) As DataSet
        'ByVal JFKM As String, ByVal DFKM As String, ByVal KucunOrGongzi As String, ByVal sQDF As String, Optional ByVal sItem As String = "DanJia", Optional ByVal iTimes As Integer = 1
        sR = sR.ToUpper.Replace("V_PRICE(", "").Replace(")", "")
        sR = sR.Replace(" ", "")

        Dim JFKM As String
        Dim DFKM As String
        Dim KuCunOrGongZi As String
        Dim sQDF As String
        Dim sItem As String
        Dim itimes As Integer

        JFKM = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, JFKM.Length + 1)
        DFKM = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, DFKM.Length + 1)
        KuCunOrGongZi = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, KuCunOrGongZi.Length + 1)
        sQDF = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, sQDF.Length + 1)
        sItem = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, sItem.Length + 1)
        itimes = sR


        Dim sSQL As String
        Dim sCase As String = ""
        Dim sKM As String = YunSuan(JFKM)
        'Dim bJiSuan As Boolean = False
        If JFKM = "" Then
            sCase = " 1=1 "
        Else
            If sKM = "" Then
                Return Nothing
            Else
                sCase = " 科目 ='" & FKF.FKF.getKMDMfromQM(sKM) & "' "
            End If
        End If


        Dim sDYKM As String = YunSuan(DFKM)
        If DFKM = "" Then

        Else
            If sDYKM = "" Then
                Return Nothing
            Else
                sCase = sCase & " and 对应科目 ='" & FKF.FKF.getKMDMfromQM(sDYKM) & "' "
            End If
        End If

        If sQDF = "" Then
        Else
            sQDF = CType(YunSuan(sQDF), String)
            Dim iLength As Integer
            iLength = FKG.myselfG.iBianHaoWeiShu + FKG.myselfG.iNianFenWeiShu + sQDF.Length + 2
            sCase = sCase & " and CHARINDEX('" & sQDF & "',编号)=1 and len(编号)=" & iLength
        End If


        'sSQL = "SELECT DISTINCT " & sItem & " FROM (select top " & YunSuan(itimes) & " " & YunSuan(sItem) & ",日期,A_JBID from " & YunSuan(KuCunOrGongZi) & " where " & sCase & " order by 日期 DESC,A_JBID desc) ORDER BY 日期 DESC,A_JBID desc"

        sSQL = "SELECT DISTINCT top " & YunSuan(itimes) & " " & YunSuan(sItem) & " FROM (select DISTINCT  " & YunSuan(sItem) & ",日期 from " & YunSuan(KuCunOrGongZi) & " where " & sCase & " ) as ttt ORDER BY " & YunSuan(sItem) & " DESC"

        Dim sZhi As DataSet
        Try
            sZhi = mdb.ReaderOpen(sSQL)
            Return sZhi
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function V_TMCKM(ByVal sText As String) As DataSet
        '格式为V_TMCKM（条码或编码，科目参数），通过条形码或者编码来得到科目代码或者科目名称等，暨通过条形码确定科目。
        Dim sCanShu As String
        sCanShu = sText.ToUpper.Replace("V_TMCKM(", "").Replace(")", "")

        Dim sKMTM As String
        sKMTM = Split(sCanShu, ",", 2, CompareMethod.Text).GetValue(0).ToString

        Dim sKMCanShu As String
        sKMCanShu = sCanShu.Remove(0, sKMTM.Length + 1)

        If isKongJian(sKMTM) Then
            sKMTM = Me.gbSelect.Controls.Item(sKMTM).Text
        End If

        If sKMTM = "" Then
            Return Nothing
        End If

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As New DataSet
        ds = mdb.Readeropen("select " & sKMCanShu & "  from KMDM where WBDW like '%" & sKMTM & "%'  and Nian=" & FKG.myselfG.NianFen)

        Return ds
    End Function

    Private Function V_FZSX(ByVal sText As String) As DataSet
        '格式为V_FZSX（科目代码，辅助属性名称）
        Dim sCanShu As String
        sCanShu = sText.ToUpper.Replace("V_FZSX(", "").Replace(")", "")

        Dim sKMDM As String
        Dim sName As String
        'Dim sXuanXiang As String

        sKMDM = FKF.FKF.getKMDMfromQM(sCanShu, ",")
        sCanShu = sCanShu.Replace(sKMDM & ",", "")
        sName = sCanShu
        '.Substring(0, sCanShu.IndexOf(","))
        'sCanShu = sCanShu.Replace(sName & ",", "")
        'sXuanXiang = sCanShu


        If isKongJian(sKMDM) Then
            If sKMDM.ToLower.IndexOf("kemu") = -1 OrElse Me.gbSelect.Controls.Item(sKMDM).Text.IndexOf(" ") = -1 Then
                sKMDM = Me.gbSelect.Controls.Item(sKMDM).Text
            Else
                sKMDM = Me.gbSelect.Controls.Item(sKMDM).Text
                sKMDM = FKF.FKF.getKMDMfromQM(sKMDM)
            End If
        End If

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.ReaderOpen("select isNull(辅助属性值,'') as 辅助属性  from KMFZSX where 科目代码='" & sKMDM & "' and 辅助属性名称='" & sName & "' and Nian=" & FKG.myselfG.NianFen)
        Return ds

    End Function

    Private Function V_KM(ByVal sText As String) As DataSet
        '格式为V_KM（科目代码，可选显示项，0|1） 可选显示项为 KMMC，KMQM等内容
        '默认为0,即包括下级科目，选择1时为不包括下级科目，选择2时为只显示下级科目而不显示本科目。该项必须有不能留空
        '新增：设置为3时，只显示指定科目为末级科目的“可选显示项”。

        Dim sCanShu As String
        sCanShu = sText.ToUpper.Replace("V_KM(", "").Replace(")", "")

        Dim sKMDM As String
        Dim sName As String
        Dim sXuanXiang As String

        sKMDM = FKF.FKF.getKMDMfromQM(sCanShu, ",")
        sCanShu = sCanShu.Replace(sKMDM & ",", "")
        sName = FKF.FKF.getKMDMfromQM(sCanShu, ",")
        sCanShu = sCanShu.Replace(sName & ",", "")
        sXuanXiang = sCanShu

        If isKongJian(sKMDM) Then
            If sKMDM.ToLower.IndexOf("kemu") = -1 OrElse Me.gbSelect.Controls.Item(sKMDM).Text.IndexOf(" ") = -1 Then
                sKMDM = Me.gbSelect.Controls.Item(sKMDM).Text
            Else
                sKMDM = Me.gbSelect.Controls.Item(sKMDM).Text
                sKMDM = FKF.FKF.getKMDMfromQM(sKMDM)
            End If
        End If

        'If sKMDM = "" OrElse CType(sKMDM, Integer) = 0 Then
        '    sKMDM = "-1"
        'End If
        If sKMDM = "" OrElse CType(sKMDM, Double) = 0 Then
            sKMDM = "-1"
        End If

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        If sXuanXiang = 0 Then
            ds = mdb.ReaderOpen("select isNull(" & sName & ",'')  from KMDM where KMDM like '" & sKMDM & "%' and Nian=" & FKG.myselfG.NianFen)
        ElseIf sXuanXiang = 1 Then
            ds = mdb.ReaderOpen("select isNull(" & sName & ",'')  from KMDM where KMDM = '" & sKMDM & "' and Nian=" & FKG.myselfG.NianFen)
        ElseIf sXuanXiang = 2 Then
            ds = mdb.ReaderOpen("select isNull(" & sName & ",'')  from KMDM where KMDM like '" & sKMDM & "%' and Nian=" & FKG.myselfG.NianFen & " and KMDM <>'" & sKMDM & "'")
        ElseIf sXuanXiang = 3 Then
            ds = mdb.ReaderOpen("select isNull(" & sName & ",'')  from KMDM where KMDM Like '" & sKMDM & "%' and Nian=" & FKG.myselfG.NianFen & " and SFMJ='true'")
        Else
            ds = mdb.ReaderOpen("select isNull(" & sName & ",'')  from KMDM where  KMDM like '" & sKMDM & "%' and  KMDM <> '" & sKMDM & "' and Nian=" & FKG.myselfG.NianFen)
        End If

        Return ds
    End Function

    '以上为绑定数据函数

    Private Function FillDG(ByVal Bianhao As String) As Boolean
        Dim bHang As Boolean = False
        Dim iHangShu As Integer = -1
        '以上两个变量用于解决行数问题

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If Me.dgvKuCun.Visible Then
            dvKuCun = mdb.ReaderOpen("select A_JBID, " & Me.dgvKuCun.Tag & " from 库存表 where 编号 ='" & Bianhao & "' and 功能名称='" & sGongnengName & "' and 显示='true' order by 行数").Tables(0).DefaultView

            Dim sss() As String
            sss = Split(Me.dgvKuCun.Tag.ToString, ",")
            Dim stemp As String
            For Each stemp In sss
                If stemp.StartsWith("行数") Then
                    bHang = True
                    iHangShu = Array.IndexOf(sss, stemp)
                    Exit For
                End If
            Next

            JiSuanHeJiZhi(dvKuCun, "KuCunHeJi", bHang, iHangShu)
            Me.dgvKuCun.DataSource = dvKuCun
            Me.dgvKuCun.Columns(0).Visible = False
            SetHJColor(Me.dgvKuCun)
        End If
        If Me.dgvGongZi.Visible Then
            dvGongZi = mdb.ReaderOpen("select  A_JBID, " & Me.dgvGongZi.Tag & " from 工资表 where 编号 ='" & Bianhao & "'  and 功能名称='" & sGongnengName & "' and 显示='true'  order by 行数").Tables(0).DefaultView

            Dim sss() As String
            sss = Split(Me.dgvGongZi.Tag)
            Dim stemp As String
            bHang = False
            For Each stemp In sss
                If stemp.StartsWith("行数") Then
                    bHang = True
                    iHangShu = Array.IndexOf(sss, stemp)
                    Exit For
                End If
            Next

            JiSuanHeJiZhi(dvGongZi, "GongZiHeJi", bHang, iHangShu)
            Me.dgvGongZi.DataSource = dvGongZi
            Me.dgvGongZi.Columns(0).Visible = False
            SetHJColor(Me.dgvGongZi)
        End If

        If Me.dgvPZ.Visible Then
            bHang = False
            Select Case iPZSCFS
                'Case 0
                '    dvPZ = mdb.Readeropen("select  A_JBID, " & Me.dgvPZ.Tag & "  from 凭证表 where 编号 ='" & Bianhao & "' and 显示='true'  order by 行数").Tables(0).DefaultView

                'Case 1
                '    dvPZ = mdb.Readeropen("select  A_JBID, " & Me.dgvPZ.Tag & "  from 凭证表 where 编号 ='" & Bianhao & "' and 显示='true'  order by 行数").Tables(0).DefaultView

                Case 111 '即便是逐单凭证，也不再汇总显示，而是直接显示录入的明细，以方便修改和删除。

                    Dim stemp As String
                    Dim sT As String = ""

                    Dim sLie() As String
                    sLie = Split(Me.dgvPZ.Tag.ToString, ",")
                    Dim sDispLie As String = ""
                    For Each stemp In sLie
                        If stemp.StartsWith("行数") Then
                            bHang = True
                            iHangShu = Array.IndexOf(sLie, stemp)
                            If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                stemp = "'行数' as n0000"
                            Else
                                stemp = stemp.Replace("行数", "'行数'")
                            End If
                            'Continue For
                        End If
                        If stemp.StartsWith("借方数量") Then
                            If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                stemp = stemp.Replace("借方数量", "sum(借方数量) as n0001")
                            Else
                                stemp = stemp.Replace("借方数量", "sum(借方数量)")
                            End If
                        End If
                        'If stemp.StartsWith("借方单价") Then
                        '    'stemp = stemp.Replace("借方单价", "sum(借方单价)")
                        'End If
                        If stemp.StartsWith("借方金额") Then
                            If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                stemp = stemp.Replace("借方金额", "sum(借方金额) as n0003")
                            Else
                                stemp = stemp.Replace("借方金额", "sum(借方金额)")
                            End If
                            'stemp = stemp.Replace("借方金额", "sum(借方金额)")
                        End If
                        If stemp.StartsWith("贷方数量") Then
                            If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                stemp = stemp.Replace("贷方数量", "sum(贷方数量) as n0004")
                            Else
                                stemp = stemp.Replace("贷方数量", "sum(贷方数量)")
                            End If
                            'stemp = stemp.Replace("贷方数量", "sum(贷方数量)")
                        End If
                        'If stemp.StartsWith("贷方单价") Then
                        '    'stemp = stemp.Replace("贷方单价", "sum(贷方单价)")
                        'End If
                        If stemp.StartsWith("贷方金额") Then
                            If stemp.IndexOf("as", System.StringComparison.CurrentCultureIgnoreCase) = -1 Then
                                stemp = stemp.Replace("贷方金额", "sum(贷方金额) as n0006")
                            Else
                                stemp = stemp.Replace("贷方金额", "sum(贷方金额)")
                            End If
                            'stemp = stemp.Replace("贷方金额", "sum(贷方金额)")
                        End If
                        sDispLie = sDispLie & stemp & ","

                    Next
                    sDispLie = sDispLie.Remove(sDispLie.Length - 1, 1)

                    Dim sLieG() As String
                    sLieG = Split(Me.dgvPZ.Tag.ToString, ",")
                    Dim sGroupLie As String = ""
                    For Each stemp In sLieG
                        sT = Split(stemp).GetValue(0)
                        If sT = "行数" OrElse sT = "借方数量" OrElse sT = "借方金额" OrElse sT = "贷方数量" OrElse sT = "贷方金额" Then
                        Else
                            sGroupLie = sGroupLie & sT & ","
                        End If
                    Next
                    sGroupLie = sGroupLie.Remove(sGroupLie.Length - 1, 1)


                    dvPZ = mdb.ReaderOpen("select  -1, " & sDispLie & "  from 凭证表 where 编号 ='" & Bianhao & "' and  功能名称='" & sGongnengName & "' and 显示='true' group by " & sGroupLie & " ").Tables(0).DefaultView

                Case 0, 1
                    dvPZ = mdb.ReaderOpen("select  A_JBID, " & Me.dgvPZ.Tag & "  from 凭证表 where 编号 ='" & Bianhao & "' and  功能名称='" & sGongnengName & "' and 显示='true'  order by 行数").Tables(0).DefaultView

            End Select

            JiSuanHeJiZhi(dvPZ, "PZHeJi", bHang, iHangShu)
            Me.dgvPZ.DataSource = dvPZ
            Me.dgvPZ.Columns(0).Visible = False
            SetHJColor(Me.dgvPZ)

            If iPZSCFS = 1 Then
                '设置列标题.headertext
                If Me.dgvPZ.Columns.IndexOf(Me.dgvPZ.Columns("n0000")) = -1 Then
                Else
                    Me.dgvPZ.Columns("n0000").HeaderText = "行"
                End If
                If Me.dgvPZ.Columns.IndexOf(Me.dgvPZ.Columns("n0001")) = -1 Then
                Else
                    Me.dgvPZ.Columns("n0001").HeaderText = "借方数量"
                End If
                If Me.dgvPZ.Columns.IndexOf(Me.dgvPZ.Columns("n0003")) = -1 Then
                Else
                    Me.dgvPZ.Columns("n0003").HeaderText = "借方金额"
                End If
                If Me.dgvPZ.Columns.IndexOf(Me.dgvPZ.Columns("n0004")) = -1 Then
                Else
                    Me.dgvPZ.Columns("n0004").HeaderText = "贷方数量"
                End If
                If Me.dgvPZ.Columns.IndexOf(Me.dgvPZ.Columns("n0006")) = -1 Then
                Else
                    Me.dgvPZ.Columns("n0006").HeaderText = "贷方金额"
                End If
            End If
        End If

        If Me.dgvKuCun.RowCount = 0 AndAlso Me.dgvGongZi.RowCount = 0 AndAlso Me.dgvPZ.RowCount = 0 AndAlso iWorkState > 0 Then
            '改单已全部被删除,进行删除单据的处理
            Me.pbDelete.Visible = True
            'Me.BHList.Items.Remove(sBianHao)'此处如果删除编号，将造成如果删除倒数1张或2张是溢出错误，将超过数组边界
            'Me.lblDelete.Top
            HuanYuan(True)
            '有选择地还原输入框为默认值，并清除显示表
            Me.BianHao.Text = Bianhao
            Me.保存.Enabled = False
            Exit Function
        Else
            '555 Me.保存.Visible = True
            Me.pbDelete.Visible = False
            '上单_Click(sender, e)
        End If

        'If Me.tssZhuangtai.Text <> "就绪" Then
        '    Exit Function
        'End If

        If Me.tssZhuangtai.Text.IndexOf("已") <> -1 Then
            Exit Function
        End If

        'If Me.tssZhuangtai.Text = "就绪" Then
        Dim dsSH As DataSet
        dsSH = mdb.ReaderOpen("select isnull(审核,'') from JiBenCaoZuo where BianHao='" & sBianHao & "' and 删除='false' and 审核<>''")
        If dsSH.Tables(0).Rows.Count = 0 Then
            Me.新增记录.Enabled = True
            '555Me.修改记录.Enabled = True
            '555Me.删除记录.Enabled = True
            '555 Me.保存.Enabled = True

            Me.tssZhuangtai.Text = "就绪"


            '20160718日修改对账纪录的问题。
            Dim dsDZh As DataSet '判断是否已对账
            dsDZh = mdb.ReaderOpen("select isnull(dzhtime,'') from JiBenCaoZuo where BianHao='" & sBianHao & "' and 删除='false' and dzhtime<>''")
            If dsDZh.Tables(0).Rows.Count = 0 Then
                Me.新增记录.Enabled = True
                '555Me.修改记录.Enabled = True
                '555Me.删除记录.Enabled = True
                '555 Me.保存.Enabled = True

                Me.tssZhuangtai.Text = "就绪"
            Else
                'Me.新增单证.Enabled = False
                Me.新增记录.Enabled = False
                Me.修改记录.Enabled = False
                Me.删除记录.Enabled = False
                Me.保存.Enabled = False

                Me.tssZhuangtai.Text = "对账"

            End If
        Else
            'Me.新增单证.Enabled = False
            Me.新增记录.Enabled = False
            Me.修改记录.Enabled = False
            Me.删除记录.Enabled = False
            Me.保存.Enabled = False

            Me.tssZhuangtai.Text = "审核"

        End If

        'End If

    End Function

    Private Function JiSuanHeJiZhi(ByVal dv As DataView, ByVal sBiaoHeJi As String, ByVal bXSHS As Boolean, Optional ByVal iHang As Integer = -1) As Boolean
        If dv.Count = 0 Then
            HideZero(dv)
            Return True
        Else
            dv.AddNew()

            Dim i As Integer
            Dim n As Integer
            Dim iMaxRowIndex As Integer
            Dim dHeJi As Double
            iMaxRowIndex = dv.Count - 1

            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim sHJ As String
            sHJ = mdb.Readeropen("select " & sBiaoHeJi & "  from gongnengshu where id=" & iGongnengID).Tables(0).Rows(0).Item(0).ToString

            For n = 1 To dv.Table.Columns.Count - 1
                If sHJ = "" OrElse sHJ.Substring(n - 1, 1).ToString = "1" Then
                    dv.Item(iMaxRowIndex).Item(n) = DBNull.Value
                ElseIf sHJ.Substring(n - 1, 1).ToString = "2" Then
                    dHeJi = 0
                    For i = 0 To dv.Count - 2
                        Try
                            dHeJi = dHeJi + dv.Item(i).Item(n)
                        Catch ex As Exception
                            Exit For
                        End Try
                    Next
                    If dHeJi = 0 Then
                        dv.Item(iMaxRowIndex).Item(n) = DBNull.Value
                    Else
                        dv.Item(iMaxRowIndex).Item(n) = dHeJi
                    End If
                Else
                    dv.Item(iMaxRowIndex).Item(n) = "合计："
                End If

            Next
            '填充新行数
            If bXSHS Then
                For i = 0 To dv.Count - 2
                    dv.Item(i).Item(iHang + 1) = i + 1
                Next
            End If

            HideZero(dv)
        End If
    End Function

    Private Sub HideZero(ByVal dv As DataView)
        Dim i, n As Integer
        For i = 1 To dv.Table.Columns.Count - 1
            For n = 0 To dv.Count - 2
                Try
                    If dv.Item(n).Item(i) = 0 Then
                        dv.Item(n).Item(i) = DBNull.Value
                    End If
                Catch ex As Exception
                    Exit For
                End Try

            Next
        Next
    End Sub

    Private Sub SetHJColor(ByVal obj As System.Windows.Forms.DataGridView)
        If obj.RowCount = 0 Then
            Exit Sub
        Else
            obj.Rows(obj.Rows.GetLastRow(Windows.Forms.DataGridViewElementStates.None)).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.LightGray, False)
        End If

    End Sub

    Private Sub SetLK(ByVal obj As System.Windows.Forms.DataGridView)
        Dim i As Integer
        Dim sLK() As String
        Dim sDot() As String
        Select Case obj.Name
            Case "dgvKuCun"
                sLK = Split(dvKCLK)
                sDot = Split(dvKCDot)
            Case "dgvGongZi"
                sLK = Split(dvGZLK)
                sDot = Split(dvGZDot)
            Case "dgvPZ"
                sLK = Split(dvPZLK)
                sDot = Split(dvPZDot)
            Case Else
                Exit Sub
        End Select
        If sLK.Length = 1 Then
            Exit Sub
        End If
        For i = 1 To obj.ColumnCount - 1
            obj.Columns(i).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
            obj.Columns(i).Width = sLK.GetValue(i - 1) * obj.Width / 100
            If sDot.Length = 1 Then
            Else
                If sDot.GetValue(i - 1) = -1 Then
                Else
                    obj.Columns(i).DefaultCellStyle.Format = "N" & sDot(i - 1)
                End If
            End If
        Next

    End Sub

    Private Sub DispSize(Optional ByVal bLoad As Boolean = False)
 
        Me.tlp.RowStyles(0).Height = Me.gbSelect.Height + 6

        Dim iXSNumber As Integer = 0
        If bLoad Then '初始化大小
            Dim ds As DataSet
            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            ds = mdb.ReaderOpen("select * from GongNengShu where ID=" & iGongnengID)
            '设置是否显示
            If ds.Tables(0).Rows(0).Item("kucun") = False Then
                Me.dgvKuCun.Visible = False
                'Me.lblKuCun.Visible = False

                ' Me.tlp.RowStyles(1).Height = 0
            Else
                iXSNumber = iXSNumber + 1
                If ds.Tables(0).Rows(0).Item("KuCunSQL").ToString = "" Then
                    Me.dgvKuCun.Tag = " * "
                    dvKCLK = ""
                    dvKCDot = ""
                Else
                    Me.dgvKuCun.Tag = ds.Tables(0).Rows(0).Item("KuCunSQL").ToString
                    dvKCLK = ds.Tables(0).Rows(0).Item("KuCunLK").ToString
                    dvKCDot = ds.Tables(0).Rows(0).Item("KuCunDot").ToString
                    'SetLK(dgvKuCun)
                End If

                '20200828升级，现实时FILL
                'Me.dgvKuCun.Dock = Windows.Forms.DockStyle.Fill
            End If

            If ds.Tables(0).Rows(0).Item("GongZi") = False Then
                Me.dgvGongZi.Visible = False
                'Me.lblGongZi.Visible = False

                ' Me.tlp.RowStyles(2).Height = 0
            Else
                iXSNumber = iXSNumber + 1
                If ds.Tables(0).Rows(0).Item("GongZiSQL").ToString = "" Then
                    Me.dgvGongZi.Tag = " * "
                    dvGZLK = ""
                    dvGZDot = ""
                Else
                    Me.dgvGongZi.Tag = ds.Tables(0).Rows(0).Item("GongZiSQL").ToString
                    dvGZLK = ds.Tables(0).Rows(0).Item("GongZiLK").ToString
                    dvGZDot = ds.Tables(0).Rows(0).Item("GongZiDot").ToString
                    'SetLK(dgvGongZi)
                End If
                '20200828升级，现实时FILL
                'Me.dgvGongZi.Dock = Windows.Forms.DockStyle.Fill
            End If

            If ds.Tables(0).Rows(0).Item("pz") = False Then
                Me.dgvPZ.Visible = False
                'Me.lblPZ.Visible = False

                '  Me.tlp.RowStyles(3).Height = 0
            Else
                iXSNumber = iXSNumber + 1
                If ds.Tables(0).Rows(0).Item("pzSQL").ToString = "" Then
                    Me.dgvPZ.Tag = " * "
                    dvPZLK = ""
                    dvPZDot = ""
                Else
                    Me.dgvPZ.Tag = ds.Tables(0).Rows(0).Item("pzSQL").ToString
                    dvPZLK = ds.Tables(0).Rows(0).Item("PZLK").ToString
                    dvPZDot = ds.Tables(0).Rows(0).Item("PZDot").ToString
                    'SetLK(dgvPZ)
                End If
                '20200828升级，现实时FILL
                'Me.dgvPZ.Dock = Windows.Forms.DockStyle.Fill
            End If
        Else

            If Me.dgvKuCun.Visible Then
                iXSNumber = iXSNumber + 1
            End If
            If Me.dgvGongZi.Visible Then
                iXSNumber = iXSNumber + 1
            End If
            If Me.dgvPZ.Visible Then
                iXSNumber = iXSNumber + 1
            End If

            Dim iHeight As Integer
            iHeight = Me.tlp.Height - Me.gbSelect.Height - 16

            If Me.dgvKuCun.Visible Then
                Me.dgvKuCun.Height = iHeight \ iXSNumber
                Me.dgvKuCun.Width = Me.FlowLayoutPanel1.Width
                'Me.lblKuCun.Height = Me.dgvKuCun.Height
                SetLK(Me.dgvKuCun)
            End If
            If Me.dgvGongZi.Visible Then
                Me.dgvGongZi.Height = iHeight \ iXSNumber
                Me.dgvGongZi.Width = Me.FlowLayoutPanel1.Width
                'Me.lblGongZi.Height = Me.dgvGongZi.Height
                SetLK(Me.dgvGongZi)
            End If
            If Me.dgvPZ.Visible Then
                Me.dgvPZ.Height = iHeight \ iXSNumber
                Me.dgvPZ.Width = Me.FlowLayoutPanel1.Width
                'Me.lblPZ.Height = Me.dgvPZ.Height
                SetLK(Me.dgvPZ)
            End If

        End If

    End Sub

    Private Sub FKGN_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        DispSize()
    End Sub

    Private Sub HuanYuan(ByVal isNewDanZheng As Boolean)
        If isNewDanZheng Then

            Dim obj As System.Windows.Forms.Control

            For Each obj In Me.gbSelect.Controls
                Select Case obj.GetType.Name
                    Case "FKLable"
                    Case "FKTextBox"
                        With CType(obj, Freedom.FKTextBox)
                            .Text = .FKShuXing.Zhi
                            .ReadOnly = .FKShuXing.ZhiDu

                        End With

                    Case "FKComboBox"
                        With CType(obj, Freedom.FKComboBox)
                            .Enabled = Not .FKShuXing.ZhiDu
                            .Text = .FKShuXing.Zhi
                        End With

                    Case "FKDateTimePicker"
                        With CType(obj, Freedom.FKDateTimePicker)
                            .Enabled = Not .FKShuXing.ZhiDu
                            .Text = FKG.myselfG.RiQi.Date
                        End With
                    Case "PictureBox"

                    Case Else
                        'MsgBox("意外的控件类型")
                End Select
            Next

            Dim i As Integer
            For i = 0 To dsKJID.Tables(0).Rows.Count - 1
                obj = Me.gbSelect.Controls(dsKJID.Tables(0).Rows(i).Item(0).ToString)
                If obj.GetType.Name <> "FKTextBox" AndAlso obj.GetType.Name <> "FKComboBox" Then
                    Continue For
                End If
                JiSuanTextBoxGongShi(obj)

                '计算绑定
                BangdingOBJ(obj)
            Next

        Else
            Dim obj As System.Windows.Forms.Control
            For Each obj In Me.gbSelect.Controls
                Select Case obj.GetType.Name
                    Case "FKLable"
                    Case "FKTextBox"
                        With CType(obj, Freedom.FKTextBox)
                            If .FKShuXing.SuoDing Then
                                .ReadOnly = True
                            Else
                                If obj.Name.ToLower = "zhaiyao" Then
                                    If Me.复制摘要.Checked Then

                                    Else
                                        .Text = .FKShuXing.Zhi
                                        .ReadOnly = .FKShuXing.ZhiDu
                                    End If
                                Else
                                    .Text = .FKShuXing.Zhi
                                    .ReadOnly = .FKShuXing.ZhiDu
                                End If
                                'JiSuanTextBoxGongShi(obj, .FKShuXing.SuoDing)
                            End If

                        End With

                    Case "FKComboBox"
                        With CType(obj, Freedom.FKComboBox)
                            If .FKShuXing.SuoDing Then
                                .Enabled = False
                            Else
                                If obj.Name.ToLower = "cangku" Then

                                Else

                                    .Text = .FKShuXing.Zhi
                                End If
                                .Enabled = Not .FKShuXing.ZhiDu
                            End If
                        End With

                    Case "FKDateTimePicker"
                        With CType(obj, Freedom.FKDateTimePicker)
                            If .FKShuXing.SuoDing Then
                                .Enabled = False
                            Else
                                .Enabled = Not .FKShuXing.ZhiDu
                                .Text = FKG.myselfG.RiQi.Date
                            End If
                        End With
                    Case "PictureBox"

                    Case Else
                        'MsgBox("意外的控件类型")
                End Select

            Next
            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            'Dim dsKJID As DataSet
            ''ds = mdb.Readeropen("select KJID from GongNeng where GNSID=" & iGongnengID & " and Nian=" & FKG.myselfG.NianFen & " order by Tabindex")
            'dsKJID = mdb.ReaderOpen("select KJID from GongNeng where GNSID=" & iGongnengID & " order by Tabindex")
            Dim i As Integer
            For i = 0 To dsKJID.Tables(0).Rows.Count - 1
                obj = Me.gbSelect.Controls(dsKJID.Tables(0).Rows(i).Item(0).ToString)
                If obj.GetType.Name <> "FKTextBox" AndAlso obj.GetType.Name <> "FKComboBox" Then
                    Continue For
                End If
                Select Case obj.GetType.Name
                    Case "FKTextBox"
                        JiSuanTextBoxGongShi(obj, CType(obj, Freedom.FKTextBox).FKShuXing.SuoDing)
                        '20180819修正，解决当选中同单锁定后，文本框重新填充，后发生改变且无法修改的问题
                        '计算绑定
                        If CType(obj, Freedom.FKTextBox).FKShuXing.SuoDing Then

                        Else
                            BangdingOBJ(obj)
                        End If
                    Case "FKComboBox"
                        JiSuanTextBoxGongShi(obj, CType(obj, Freedom.FKComboBox).FKShuXing.SuoDing)
                        '计算绑定
                        'BangdingOBJ(obj)

                        '2012.9.20修改，解决挡选中同单锁定后，下拉框重新填充，后发生改变且无法修改的问题
                        '计算绑定
                        If CType(obj, Freedom.FKComboBox).FKShuXing.SuoDing Then
                        Else
                            BangdingOBJ(obj)
                        End If
                End Select
            Next
        End If

        '自动保存设为False
        bAutoSave = False

    End Sub


    Private Sub KFtextbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles BianHao.Validating, Zhaiyao.Validating, ShuLiang.Validating, DanJia.Validating, JinE.Validating, Beiyong1.Validating, BeiYong2.Validating, BeiYong3.Validating, BeiYong6.Validating, BeiYong7.Validating, BeiYong8.Validating, BeiYong9.Validating, BeiYong10.Validating, BeiYong11.Validating, BeiYong12.Validating, BeiYong13.Validating, BeiYong14.Validating, BeiYong15.Validating, Beiyong16.Validating, Beiyong17.Validating, Beiyong18.Validating, Beiyong19.Validating, Beiyong20.Validating, Beiyong21.Validating, Beiyong22.Validating, Beiyong23.Validating, Beiyong24.Validating, Beiyong25.Validating, Beiyong26.Validating, Beiyong27.Validating, Beiyong28.Validating, Beiyong29.Validating, Beiyong30.Validating

        Dim obj As Freedom.FKTextBox
        obj = CType(sender, Freedom.FKTextBox)
        Select Case obj.FKShuXing.GeShi
            Case "数字"
                If IsNumeric(obj.Text) Then
                Else
                    MsgBox("输入格式错误，应该为数字！", MsgBoxStyle.Information, "格式错误")
                    obj.Text = 0
                    e.Cancel = True
                End If
            Case "公式"
                'JiSuanGongShi(obj, obj.FKShuXing.Zhi)
            Case "文本"

            Case Else

        End Select

    End Sub

    Private Sub BeiYong4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeiYong4.SelectedIndexChanged, BeiYong4.Validated
        Me.RefreshGongShi(CType(sender, System.Windows.Forms.Control).Name)
    End Sub


    Private Sub BianHao_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles BianHao.Validated, CangKu.Validated, DaiFangZhuKeMu.Validated, DanJia.Validated, DanWei.Validated, JieFangZhuKemu.Validated, JinE.Validated, JingShouRen.Validated, PingZhengHao.Validated, PingZhengZi.Validated, RiQi.Validated, ShuLiang.Validated, Zhaiyao.Validated, Beiyong1.Validated, BeiYong2.Validated, BeiYong3.Validated, BeiYong6.Validated, BeiYong7.Validated, BeiYong8.Validated, BeiYong9.Validated, BeiYong10.Validated, BeiYong11.Validated, BeiYong12.Validated, BeiYong13.Validated, BeiYong14.Validated, BeiYong15.Validated, Beiyong16.Validated, Beiyong17.Validated, Beiyong18.Validated, Beiyong19.Validated, Beiyong20.Validated, Beiyong21.Validated, Beiyong22.Validated, Beiyong23.Validated, Beiyong24.Validated, Beiyong25.Validated, Beiyong26.Validated, Beiyong27.Validated, Beiyong28.Validated, Beiyong29.Validated, Beiyong30.Validated

        Me.RefreshGongShi(CType(sender, System.Windows.Forms.Control).Name)
        '        MsgBox(sender.ToString)
        '验证完成暨公式全部运算完毕后，判断是否需要自动保存
        If bAutoSave = True Then
            Me.保存_Click(sender, e)
            bAutoSave = False
        End If
    End Sub

    Public Event Valited(ByVal sender As Object, ByVal e As EventArgs) '自定义一个事件，用于内容改变时刷新下一级公式
    Private Sub RefreshValited(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Valited
        Me.RefreshGongShi(CType(sender, System.Windows.Forms.Control).Name)
    End Sub

    Private Sub RefreshGongShi(ByVal sobjName As String) '当有数据变化时，改变相关公式或绑定
        Dim obj As System.Windows.Forms.Control
        For Each obj In Me.gbSelect.Controls
            Select Case obj.GetType.Name
                Case "FKTextBox"
                    With CType(obj, Freedom.FKTextBox)
                        If .FKShuXing.BangDing = "" OrElse .FKShuXing.BangDing.ToUpper.IndexOf(sobjName.ToUpper) = -1 Then

                        Else
                            BangdingOBJ(obj)
                            RaiseEvent Valited(obj, Nothing)
                        End If

                        If .FKShuXing.GeShi = "公式" AndAlso .FKShuXing.Zhi.ToUpper.IndexOf(sobjName.ToUpper) > -1 Then
                            '这种判断方式在涉及BeiYong1时会出现死循环情况。
                            Dim objGGZhi As Object = YunSuan(.FKShuXing.Zhi)
                            If IsNumeric(objGGZhi) AndAlso objGGZhi.ToString.Contains(".") Then
                                objGGZhi = objGGZhi.ToString.TrimEnd("0").TrimEnd(".")
                                '想将数字显示时不显示那么多小数点后的0
                                .Text = objGGZhi
                            Else
                                .Text = objGGZhi
                            End If
                            RaiseEvent Valited(obj, Nothing)
                        End If
                    End With
                Case "FKComboBox"
                    With CType(obj, Freedom.FKComboBox)
                        If .FKShuXing.BangDing = "" OrElse .FKShuXing.BangDing.ToUpper.IndexOf(sobjName.ToUpper) = -1 Then

                        Else
                            BangdingOBJ(obj)
                            RaiseEvent Valited(obj, Nothing)
                        End If

                    End With
            End Select
        Next
    End Sub

    ''以下为公式计算
    'Private Function YunSuanFu(ByVal sText As String) As String
    '    Dim ysf As String = ""

    '    'If sText.ToUpper.StartsWith("V_FZSX") Then
    '    '    Return "V_FZSX"
    '    'End If'
    '    '增加可扩展公式函数
    '    If sText.IndexOf("V_FKGSHS", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
    '        Return "V_FKGSHS"
    '    End If
    '    '增加最近单价提示功能,格式为:V_Price(JieFangkemu,DaiFangKemu,kucunORGongZi,IgongNengID)
    '    If sText.IndexOf("V_PRICE", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
    '        Return "V_PRICE"
    '    End If
    '    '增加取子字符串函数,相当于SubString
    '    If sText.IndexOf("V_SubString", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
    '        Return "V_SubString"
    '    End If
    '    '增加取整函数，即舍去所有小数
    '    If sText.IndexOf("V_QZS", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
    '        Return "V_QZS"
    '    End If
    '    '增加四舍五入函数
    '    If sText.IndexOf("V_SSWR", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
    '        Return "V_SSWR"
    '    End If
    '    '以上两个函数用法为“ V_SSWR Jine ”，不要加括号和其他符号

    '    If sText.IndexOf("&") <> -1 Then
    '        Return "&"
    '    End If

    '    If sText.IndexOf("+") <> -1 Then
    '        Return "+"
    '    End If

    '    If sText.IndexOf("-") <> -1 Then
    '        Return "-"
    '    End If

    '    If sText.IndexOf("*") <> -1 Then
    '        Return "*"
    '    End If

    '    If sText.IndexOf("/") <> -1 Then
    '        Return "/"
    '    End If
    '    Return ""
    'End Function

    'Private Function isKongJian(ByVal sText As String) As Boolean
    '    'MsgBox(Me.gbSelect.Controls.IndexOfKey(sText))
    '    If Me.gbSelect.Controls.IndexOfKey(sText) = -1 Then
    '        Return False
    '    Else
    '        Return True
    '    End If
    'End Function

    'Private Function YunSuan(ByVal sText As String) As Object
    '    sText = sText.Replace(" ", "")
    '    If sText = "" Then
    '        Return ""
    '    End If
    '    Dim sL As String
    '    Dim sR As String
    '    Dim sYSF As String

    '    sYSF = YunSuanFu(sText)
    '    'Select Case sYSF
    '    '    Case "V_FZSX"
    '    '        Return V_FZSX(sText)
    '    'End Select

    '    If sYSF = "V_QZS" OrElse sYSF = "V_SSWR" Or sYSF = "V_PRICE" Or sYSF = "V_SubString" Or sYSF = "V_FKGSHS" Then
    '        sR = sText.Substring(sYSF.Length, sText.Length - sYSF.Length)
    '        sL = ""
    '        'ElseIf sYSF = "" Then

    '    Else
    '        sL = sText.Substring(0, sText.IndexOf(sYSF))
    '        sR = sText.Substring(sText.IndexOf(sYSF) + 1, sText.Length - sText.IndexOf(sYSF) - 1)
    '    End If

    '    Try
    '        Select Case sYSF
    '            Case "V_FKGSHS"
    '                'Dim obj As New FKGSHS.FKGSHS
    '                Return FKHanShu(sR)
    '            Case "V_PRICE"
    '                Return getPrice(sR)
    '                ' Return ""
    '            Case "V_SubString"
    '                Return CType(YunSuan(Split(sR, ",")(0)), String).Substring(Split(sR, ",")(1), Split(sR, ",")(2))
    '                'Return CType(YunSuan(sR), String).Substring(0, 6)
    '            Case "V_QZS"
    '                Return Math.Truncate(CType(YunSuan(sR), Double))
    '            Case "V_SSWR"
    '                Return Math.Round(CType(YunSuan(sR), Double))

    '            Case "&"
    '                Return YunSuan(sL) & YunSuan(sR)
    '            Case "+"
    '                Return CType(YunSuan(sL), Double) + CType(YunSuan(sR), Double)
    '            Case "-"
    '                Return CType(YunSuan(sL), Double) - CType(YunSuan(sR), Double)
    '            Case "*"
    '                Return CType(YunSuan(sL), Double) * CType(YunSuan(sR), Double)
    '            Case "/"
    '                Return CType(YunSuan(sL), Double) / CType(YunSuan(sR), Double)
    '            Case ""
    '                If isKongJian(sText) Then
    '                    Return Me.gbSelect.Controls.Item(sText).Text
    '                Else
    '                    Return sText
    '                End If
    '            Case Else
    '                Return sText
    '        End Select
    '    Catch ex As Exception
    '        'MsgBox(ex.Message)
    '        Return ""
    '    End Try

    'End Function

    'Private Function getPrice(ByVal sR As String) As Object
    '    'ByVal JFKM As String, ByVal DFKM As String, ByVal KucunOrGongzi As String, ByVal sQDF As String, Optional ByVal sItem As String = "DanJia", Optional ByVal iTimes As Integer = 1
    '    Dim JFKM As String
    '    Dim DFKM As String
    '    Dim KuCunOrGongZi As String
    '    Dim sQDF As String
    '    Dim sItem As String
    '    Dim itimes As Integer

    '    Dim sTemp() As String
    '    sTemp = Split(sR, ",")
    '    JFKM = sTemp(0)
    '    DFKM = sTemp(1)
    '    KuCunOrGongZi = sTemp(2)
    '    sQDF = sTemp(3)
    '    sItem = sTemp(4)
    '    itimes = CType(sTemp(5), Integer)

    '    Dim sSQL As String
    '    Dim sCase As String = ""
    '    Dim sKM As String = YunSuan(JFKM)
    '    'Dim bJiSuan As Boolean = False
    '    If JFKM = "" Then
    '        sCase = " true "
    '    Else
    '        If sKM = "" Then
    '            Return 0
    '        Else
    '            sCase = " 科目 ='" & FKF.FKF.getKMDMfromQM(sKM) & "' "
    '        End If
    '    End If


    '    Dim sDYKM As String = YunSuan(DFKM)
    '    If DFKM = "" Then
    '    Else
    '        If sDYKM = "" Then
    '            Return 0
    '        Else
    '            sCase = sCase & " and 对应科目 ='" & FKF.FKF.getKMDMfromQM(sDYKM) & "' "
    '        End If
    '    End If

    '    If sQDF = "" Then
    '    Else
    '        Dim iLength As Integer
    '        iLength = FKG.myselfG.iBianHaoWeiShu + FKG.myselfG.iNianFenWeiShu + sQDF.Length + 2
    '        sCase = sCase & " and instr(编号,'" & sQDF & "')=1 and len(编号)=" & iLength
    '    End If

    '    sSQL = "select top " & itimes & " " & sItem & " from " & KuCunOrGongZi & " where " & sCase & " order by 日期 DESC"
    '    'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
    '    Dim sZhi As Object
    '    Try
    '        sZhi = mdb.Readeropen(sSQL).Tables(0).Rows(0).Item(0)
    '        Return sZhi
    '    Catch ex As Exception
    '        Return 0
    '    End Try
    'End Function

    '' 格式为：V_FKGSHS 函数名,参数1，参数2，参数3
    'Private Function FKHanShu(ByVal sGongShi As String) As Object
    '    'Dim hanshu As New FKHanShu.HanShu(sGongShi)
    '    'Return hanshu.zdyYunSuan

    '    Dim sYSF As String
    '    sYSF = Split(sGongShi, ",").GetValue(0).ToString
    '    Dim sCanShu() As String
    '    sCanShu = Split(sGongShi.Remove(0, sYSF.Length), ",")

    '    Try
    '        Select Case sYSF
    '            Case "V_WCSL" '外存数量。参数为： 委外客户代码，产品代码
    '                Dim iWCSL As Double
    '                'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
    '                Dim sSQL As String
    '                sSQL = "select ncjfsl-ncdfsl from WeiWaiYE where NIAN=" & FKG.myselfG.NianFen & " and WWKHDM='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(1))) & "' and KMDM ='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(2))) & "'"

    '                Dim dsYE As DataSet
    '                dsYE = mdb.Readeropen(sSQL)
    '                If dsYE.Tables(0).Rows.Count = 0 Then
    '                    iWCSL = 0
    '                Else
    '                    iWCSL = dsYE.Tables(0).Rows(0).Item(0)
    '                End If '得到年初余额

    '                sSQL = "SELECT iif(isnull(SUM(借方数量)),0,SUM(借方数量))-iif(isnull(SUM(贷方数量)),0,SUM(贷方数量))  FROM GONGZI where 年份= " & FKG.myselfG.NianFen & " and (对应科目='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(2))) & "' and 科目='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(1))) & "') group  by 对应科目"
    '                dsYE.Clear()
    '                dsYE = mdb.Readeropen(sSQL)

    '                If dsYE.Tables(0).Rows.Count = 0 Then
    '                    iWCSL = iWCSL + 0
    '                Else
    '                    iWCSL = iWCSL + dsYE.Tables(0).Rows(0).Item(0)
    '                End If '得到年初余额
    '                Return iWCSL
    '            Case Else
    '                Return ""
    '        End Select
    '    Catch ex As Exception
    '        Return 0
    '    End Try


    'End Function
    ''以上为公式计算

    '在保存之前先验证是否所有必填项均有合法数值
    Private Function VerifyBiTian() As Boolean
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim i As Integer
        For i = 0 To dsBiTianKJID.Tables(0).Rows.Count - 1
            Select Case Me.gbSelect.Controls.Item(dsBiTianKJID.Tables(0).Rows(i).Item(0)).GetType.Name
                Case "FKTextBox"
                    If CType(Me.gbSelect.Controls(dsBiTianKJID.Tables(0).Rows(i).Item(0)), Freedom.FKTextBox).Text = "" Then
                        MsgBox("有必填项为空，不能保存，请检查！", MsgBoxStyle.Information, "必填项为空")
                        Me.ActiveControl = Me.gbSelect.Controls(dsBiTianKJID.Tables(0).Rows(i).Item(0))
                        Return False
                        'Exit Function
                    End If
                Case "FKComboBox"
                    If CType(Me.gbSelect.Controls(dsBiTianKJID.Tables(0).Rows(i).Item(0)), Freedom.FKComboBox).Text = "" Then
                        MsgBox("有必填项为空，不能保存，请检查！", MsgBoxStyle.Information, "必填项为空")
                        Me.ActiveControl = Me.gbSelect.Controls(dsBiTianKJID.Tables(0).Rows(i).Item(0))
                        Return False
                        'Exit Function
                    End If
                    ' Case "FKDateTimePicker"
            End Select
        Next
        Return True
    End Function

    Dim mdbOpen As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
    Dim tNow As String

    Dim dsBiTianKJID As New DataSet
    ' Dim dsFirstKJID As New DataSet
    Dim dsKuCun As New DataSet
    Dim dsGongZi As New DataSet
    Dim dsPZ As New DataSet
    Dim dsKJID As New DataSet

    Dim bAutoRun As Boolean


    Private Sub iniSetupData() '将固定的数据一次性初始化，减少保存过程读取数据库次数。优化速度
        dsBiTianKJID = mdb.ReaderOpen("select KJID from gongneng where BiTian='true' and GNSID=" & iGongnengID)
        ' dsFirstKJID = mdb.ReaderOpen("select top 1 KJID from gongneng where GNSID=" & iGongnengID & " order by TabIndex")
        dsKJID = mdb.ReaderOpen("select KJID from GongNeng where GNSID=" & iGongnengID & " order by Tabindex")

        dsKuCun = mdb.ReaderOpen("select GN_KuCun.* from GN_KuCun,GongnengShu where GN_KuCun.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID)
        dsGongZi = mdb.ReaderOpen("select GN_GongZi.* from GN_GongZi,GongnengShu where GN_GongZi.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID)

        dsPZ = mdb.ReaderOpen("select GN_PZ.* from GN_PZ,GongnengShu where GN_PZ.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID)

        bAutoRun = mdbOpen.bExsit("select * from zdycmd where RunMode='Auto' and GNSID=" & iGongnengID)

    End Sub

    Private Sub 保存_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 保存.Click
        '将保存按钮关闭，防止出现极快速的连续单击时保存多次，在保存结束时重新启用
        Me.保存.Enabled = False

        '先关闭只读连接，在下次读取时打开。
        'mdb.oleConnect.Close() '不再关闭连接，看有没有问题，或者会不会影响速度
        '临时关闭自动备份
        FKG.myselfG.bCanBackup = False

        tNow = Format(Now(), "yyyy-MM-dd hh:mm:ss")

        '判断必填科目
        If VerifyBiTian() = False Then
            '将保存按钮关闭，防止出现极快速的连续单击时保存多次，在保存结束时重新启用
            Me.保存.Enabled = True

            Exit Sub
        End If

        Try
            mdbOpen.oleConnect.Open()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
            mdbOpen.oleConnect.Close()

            '将保存按钮关闭，防止出现极快速的连续单击时保存多次，在保存结束时重新启用
            Me.保存.Enabled = True

            Exit Sub
        End Try

        'Me.ActiveControl = Me.BianHao
        Me.ActiveControl = Me.gbSelect.Controls(dsKJID.Tables(0).Rows(0).Item(0).ToString)

        Dim ds As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from YHZhuangTai where YongHuName='" & FKG.myselfG.YongHu & "' and YHMAC='" & My.Computer.Name & "' and LoginTime='" & FKG.myselfG.dLogin & "'"
        'ds.Clear()
        mdbOpen.oleDataAdapter.Fill(ds)

        If ds.Tables(0).Rows.Count = 0 Then
            MsgBox("该用户已经在其他地方登录，你已被迫注销，若要继续进行操作，请重新登录！", MsgBoxStyle.Information, "提示")
            mdbOpen.oleConnect.Close() '关闭连接，重要
            Me.DialogResult = Windows.Forms.DialogResult.Abort
            Me.Close()
        Else
            Select Case iWorkState
                Case 0
                    SaveNew()
                    Me.BHList.Items.Add(Me.BianHao.Text)
                    Me.BHList.SelectedIndex = Me.BHList.Items.Count - 1
                Case 1
                    SaveNew()
                Case 2 '修改记录
                    updateSave(iJiBenID)
                    'Me.保存.Enabled = False
            End Select

            mdbOpen.oleConnect.Close() '关闭连接，重要
            FillDG(sBianHao)
        End If

        '打开自动备份功能
        FKG.myselfG.bCanBackup = True

        '将保存按钮关闭，防止出现极快速的连续单击时保存多次，在保存结束时重新启用
        '在修改状态下,不能启用保存按钮,否则将出现多次保存.
        If iWorkState = 2 Or iWorkState = 9 Then
        Else
            Me.保存.Enabled = True
        End If

    End Sub

    Dim sJiBenCaoZuoID As String = ""
    Private Sub SaveNew()
        Dim iJiBenCaoZuoID As String
        iJiBenCaoZuoID = saveJiBen()
        sJiBenCaoZuoID = iJiBenCaoZuoID '20200430升级，供自定义按钮命令使用，使用后设置为“”
        If iJiBenCaoZuoID = "-1" Then
            '出现了异常

            Exit Sub
        Else
            '其次调用基本操作的数据，生成各种数据
            '特别需要注意异常的处理
            Try

                mdbOpen.oleCommand.CommandText = "update YHZhuangTai set A_JBID='" & iJiBenCaoZuoID & "'  where YongHuName='" & FKG.myselfG.YongHu & "'"
                mdbOpen.oleCommand.ExecuteNonQuery()

                '保存库存
                If SaveKuCun(iJiBenCaoZuoID) = False OrElse SaveGongZi(iJiBenCaoZuoID) = False OrElse SavePZ(iJiBenCaoZuoID) = False Then
                    MsgBox("出现异常，未能成功保存,将自动撤销保存操作！", MsgBoxStyle.Exclamation, "警告")
                    mdbOpen.oleConnect.Close()
                    CancelSave(iJiBenCaoZuoID)
                    Exit Sub
                Else
                    mdbOpen.oleCommand.CommandText = "update YHZhuangTai set A_JBID='-1'  where YongHuName='" & FKG.myselfG.YongHu & "'"
                    mdbOpen.oleCommand.ExecuteNonQuery()

                    sBianHao = Me.BianHao.Text

                    新增记录_Click(Me, System.EventArgs.Empty)
                End If
            Catch ex As Exception
                MsgBox("出现异常，未能成功保存,将自动撤销保存操作！", MsgBoxStyle.Exclamation, "警告")
                mdbOpen.oleConnect.Close()
                CancelSave(iJiBenCaoZuoID)
            End Try

            If bAutoRun Then
                AutoRunAfterSave()
            End If

        End If
    End Sub


    Private Function SaveKuCun(ByVal iJiBenCaoZuoID As String) As Boolean
        '20170412 增加一个判断，当库存ID为 -1 时，不保存该行记录，直接跳过。更改 FKGN 模块，当仓库ID设置为-1时，该条数据不进行保存操作，以避免出现空行的情况。典型应用为生产流程。
        Dim bKuCunSkip As Boolean = False


        Dim dsJiBen As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from Jibencaozuo where A_ID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsJiBen)
        'dsJiBen = mdb.Readeropen()

        Dim sLieming As String
        sLieming = "年份,月份,行数,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20,制单,审核,记帐,显示,建立时间,建立人,A_GNSID,A_JBID"
        'sLieming = "年份,月份,行数,编号,日期,摘要"

        Dim sZhi As String
        Dim i As Integer
        For i = 0 To dsKuCun.Tables(0).Rows.Count - 1
            sZhi = FKG.myselfG.NianFen '年份
            sZhi = sZhi & "," & FKG.myselfG.YueFen '月份
            Dim iHang As Integer
            Dim dsHang As New DataSet
            Try
                'mdbOpen.oleCommand.CommandText = "select 年份 from KuCun where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and 编号='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("编号")) & "'"
                mdbOpen.oleCommand.CommandText = "select isNull(max(行数),0) from KuCun where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and 编号='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("编号")) & "'"

                mdbOpen.oleDataAdapter.Fill(dsHang)

                'iHang = dsHang.Tables(0).Rows.Count + 1 '行数
                If dsHang.Tables(0).Rows.Count = 0 Then
                    iHang = 1
                Else
                    iHang = dsHang.Tables(0).Rows(0).Item(0) + 1
                End If

            Catch ex As Exception
                MsgBox(ex.ToString)
                Return False
                Exit Function
            End Try

            sZhi = sZhi & "," & iHang
            With dsKuCun.Tables(0).Rows(i)
                If .Item("编号") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("编号")) & "'" '编号
                End If
                If .Item("日期") = "" Then
                    sZhi = sZhi & ",'" & tNow & "'"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("日期")) & "'" '日期
                End If
                If .Item("摘要") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("摘要")) & "'" '摘要
                End If
                If .Item("科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("科目"))) & "'"
                End If
                If .Item("对应科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("对应科目"))) & "'"
                End If
                If .Item("借方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方数量")) & "'"
                End If

                If .Item("借方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方单位")) & "'"
                End If

                If .Item("借方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方单价")) & "'"
                End If
                If .Item("借方金额") = "" Then
                    sZhi = sZhi & ",0.00"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方金额")), Decimal), 2) & "'"
                End If
                If .Item("贷方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方数量")) & "'"
                End If
                If .Item("贷方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方单位")) & "'"
                End If
                If .Item("贷方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方单价")) & "'"
                End If
                If .Item("贷方金额") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方金额")), Decimal), 2) & "'"

                End If

                bKuCunSkip = False
                If .Item("仓库") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & "," & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("仓库"))

                    If dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("仓库")) = "-1" Then
                        bKuCunSkip = True
                    End If
                End If


                If .Item("经手人") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("经手人")) & "'"
                End If

                If .Item("备用1") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用1")) & "'"
                End If
                If .Item("备用2") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用2")) & "'"
                End If
                If .Item("备用3") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用3")) & "'"
                End If
                If .Item("备用4") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用4")) & "'"
                End If
                If .Item("备用5") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用5")) & "'"
                End If

                If .Item("备用6") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用6")) & "'"
                End If
                If .Item("备用7") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用7")) & "'"
                End If
                If .Item("备用8") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用8")) & "'"
                End If
                If .Item("备用9") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用9")) & "'"
                End If
                If .Item("备用10") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用10")) & "'"
                End If
                If .Item("备用11") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用11")) & "'"
                End If
                If .Item("备用12") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用12")) & "'"
                End If
                If .Item("备用13") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用13")) & "'"
                End If
                If .Item("备用14") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用14")) & "'"
                End If
                If .Item("备用15") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用15")) & "'"
                End If
                If .Item("备用16") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用16")) & "'"
                End If
                If .Item("备用17") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用17")) & "'"
                End If
                If .Item("备用18") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用18")) & "'"
                End If
                If .Item("备用19") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用19")) & "'"
                End If
                If .Item("备用20") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用20")) & "'"
                End If


                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & ",''"
                sZhi = sZhi & ",''"
                sZhi = sZhi & "," & FKG.myselfG.FKString(dsKuCun.Tables(0).Rows(i).Item("显示").ToString, "")
                sZhi = sZhi & ",'" & tNow & "'"
                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & "," & iGongnengID
                sZhi = sZhi & ",'" & iJiBenCaoZuoID & "'"
            End With

            mdbOpen.oleCommand.CommandText = "insert into KuCun (" & sLieming & ") values (" & sZhi & ")"
            Try
                If bKuCunSkip Then
                    '跳过执行保存的语句。
                Else
                    mdbOpen.oleCommand.ExecuteNonQuery()
                End If
            Catch ex As Exception
                Return False
                Exit Function
            End Try

            'If mdb.Write("insert into KuCun () values (" & sZhi & ")") = False Then
            '    Return False
            '    Exit Function
            'End If
        Next
        Return True

    End Function

    Private Function SaveGongZi(ByVal iJiBenCaoZuoID As String) As Boolean
        '20170412 增加一个判断，当库存ID为 -1 时，不保存该行记录，直接跳过。更改 FKGN 模块，当仓库ID设置为-1时，该条数据不进行保存操作，以避免出现空行的情况。典型应用为生产流程。
        Dim bKuCunSkip As Boolean = False


        Dim dsJiBen As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from Jibencaozuo where A_ID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsJiBen)


        Dim sLieming As String
        sLieming = "年份,月份,行数,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20,制单,审核,记帐,显示,建立时间,建立人,A_GNSID,A_JBID"
        'sLieming = "年份,月份,行数,编号,日期,摘要"

        Dim sZhi As String
        Dim i As Integer
        For i = 0 To dsGongZi.Tables(0).Rows.Count - 1
            sZhi = FKG.myselfG.NianFen '年份
            sZhi = sZhi & "," & FKG.myselfG.YueFen '月份
            Dim iHang As Integer
            Dim dsHang As New DataSet
            Try
                'mdbOpen.oleCommand.CommandText = "select 年份 from GongZi where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and 编号='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("编号")) & "'"
                mdbOpen.oleCommand.CommandText = "select isNull(Max(行数),0) from GongZi where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and 编号='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("编号")) & "'"
                mdbOpen.oleDataAdapter.Fill(dsHang)
                If dsHang.Tables(0).Rows.Count = 0 Then
                    iHang = 1
                Else
                    iHang = dsHang.Tables(0).Rows(0).Item(0) + 1 '行数
                End If
                ' iHang = dsHang.Tables(0).Rows.Count + 1 '行数
            Catch ex As Exception
                MsgBox(ex.ToString)
                Return False
                Exit Function
            End Try

            sZhi = sZhi & "," & iHang
            With dsGongZi.Tables(0).Rows(i)
                If .Item("编号") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("编号")) & "'" '编号
                End If
                If .Item("日期") = "" Then
                    sZhi = sZhi & ",'" & tNow & "'"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("日期")) & "'" '日期
                End If
                If .Item("摘要") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("摘要")) & "'" '摘要
                End If
                If .Item("科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("科目"))) & "'"
                End If
                If .Item("对应科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("对应科目"))) & "'"
                End If
                If .Item("借方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方数量")) & "'"
                End If

                If .Item("借方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方单位")) & "'"
                End If

                If .Item("借方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方单价")) & "'"
                End If
                If .Item("借方金额") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方金额")), Decimal), 2) & "'"
                End If
                If .Item("贷方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方数量")) & "'"
                End If
                If .Item("贷方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方单位")) & "'"
                End If
                If .Item("贷方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方单价")) & "'"
                End If
                If .Item("贷方金额") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方金额")), Decimal), 2) & "'"
                End If

                bKuCunSkip = False
                If .Item("仓库") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & "," & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("仓库"))

                    If dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("仓库")) = "-1" Then
                        bKuCunSkip = True
                    End If
                End If


                If .Item("经手人") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("经手人")) & "'"
                End If
                If .Item("备用1") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用1")) & "'"
                End If
                If .Item("备用2") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用2")) & "'"
                End If
                If .Item("备用3") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用3")) & "'"
                End If
                If .Item("备用4") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用4")) & "'"
                End If
                If .Item("备用5") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用5")) & "'"
                End If

                If .Item("备用6") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用6")) & "'"
                End If
                If .Item("备用7") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用7")) & "'"
                End If
                If .Item("备用8") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用8")) & "'"
                End If
                If .Item("备用9") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用9")) & "'"
                End If
                If .Item("备用10") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用10")) & "'"
                End If
                If .Item("备用11") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用11")) & "'"
                End If
                If .Item("备用12") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用12")) & "'"
                End If
                If .Item("备用13") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用13")) & "'"
                End If
                If .Item("备用14") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用14")) & "'"
                End If
                If .Item("备用15") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用15")) & "'"
                End If
                If .Item("备用16") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用16")) & "'"
                End If
                If .Item("备用17") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用17")) & "'"
                End If
                If .Item("备用18") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用18")) & "'"
                End If
                If .Item("备用19") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用19")) & "'"
                End If
                If .Item("备用20") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用20")) & "'"
                End If

                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & ",''"
                sZhi = sZhi & ",''"
                sZhi = sZhi & "," & FKG.myselfG.FKString(dsGongZi.Tables(0).Rows(i).Item("显示").ToString, "")
                sZhi = sZhi & ",'" & tNow & "'"
                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & "," & iGongnengID
                sZhi = sZhi & ",'" & iJiBenCaoZuoID & "'"
            End With

            mdbOpen.oleCommand.CommandText = "insert into GongZi (" & sLieming & ") values (" & sZhi & ")"
            Try
                If bKuCunSkip Then
                    '跳过执行保存的语句。
                Else
                    mdbOpen.oleCommand.ExecuteNonQuery()
                End If
            Catch ex As Exception
                Return False
                Exit Function
            End Try
            'If mdb.Write("insert into GongZi () values (" & sZhi & ")") = False Then
            '    Return False
            '    Exit Function
            'End If
        Next
        Return True
    End Function

    Dim IMaxPZHao As Integer
    Private Sub getMaxPzHao(ByVal sPzLeiBie As String)
        If sPzLeiBie = "" Then
            IMaxPZHao = -2
            Exit Sub
        End If
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsPZ As New DataSet

        Select Case iPZSCFS
            Case 0
                'dsPZ = mdb.Readeropen("select top 1 凭证号 from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and 凭证号 like '" & sPzLeiBie & "%' and (not 凭证号 like '%-1%') order by 凭证号 DESC")
                dsPZ = mdb.Readeropen("select top 1 凭证号 from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and 凭证号 like '" & sPzLeiBie & "%' and (not 凭证号 like '%-1%') and len(凭证号)=" & sPzLeiBie.Length + 7 & " order by 凭证号 DESC")
                If dsPZ.Tables(0).Rows.Count = 0 Then
                    IMaxPZHao = 0
                Else
                    Dim sPzh As String
                    sPzh = dsPZ.Tables(0).Rows(0).Item(0)
                    IMaxPZHao = CType(sPzh.Replace(sPzLeiBie, "").Replace(" ", ""), Integer)
                End If
                'IMaxPZHao = -2
                'Exit Sub
            Case 1
                '取出该功能引导符
                Dim sQDF As String = Me.BianHao.FKShuXing.Zhi
                Dim iPZHao As Integer
                iPZHao = CType(sBianHao.Substring(sBianHao.Length - FKG.myselfG.iBianHaoWeiShu, FKG.myselfG.iBianHaoWeiShu), Integer)

                iPZHao = (iPZHao - 1) \ iPZZhangShu

                IMaxPZHao = iPZHao
                Exit Sub
        End Select
    End Sub


    Private Function SavePZ(ByVal iJiBenCaoZuoID As String) As Boolean
        '20170412 增加一个判断，当库存ID为 -1 时，不保存该行记录，直接跳过。更改 FKGN 模块，当仓库ID设置为-1时，该条数据不进行保存操作，以避免出现空行的情况。典型应用为生产流程。
        Dim bKuCunSkip As Boolean = False


        Dim dsJiBen As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from Jibencaozuo where A_ID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsJiBen)


        Dim sLieming As String
        sLieming = "年份,月份,行数,凭证号,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20,制单,审核,记帐,显示,建立时间,建立人,A_GNSID,A_JBID"

        Dim sZhi As String

        Dim i As Integer
        For i = 0 To dsPZ.Tables(0).Rows.Count - 1
            'If Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Double), 2) = 0 AndAlso Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Double), 2) Then
            '    Continue For
            'End If
            'Dim JinECase As Double = 0

            sZhi = FKG.myselfG.NianFen '年份
            sZhi = sZhi & "," & FKG.myselfG.YueFen '月份

            Dim iPzHao As String
            iPzHao = Me.PingZhengZi.Text & "   " & Me.PingZhengHao.Text.PadLeft(4)

            Dim iHang As Integer
            Dim dsHang As New DataSet
            Try
                'mdbOpen.oleCommand.CommandText = "select 凭证号 from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and     凭证号='" & iPzHao & "'"
                mdbOpen.oleCommand.CommandText = "select isNull(Max(行数),0) from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and     凭证号='" & iPzHao & "'"
                mdbOpen.oleDataAdapter.Fill(dsHang)
                If dsHang.Tables(0).Rows.Count = 0 Then
                    iHang = 1
                Else
                    iHang = dsHang.Tables(0).Rows(0).Item(0) + 1 '行数
                End If
                'iHang = dsHang.Tables(0).Rows.Count + 1 '行数
            Catch ex As Exception
                MsgBox(ex.ToString)
                Return False
                Exit Function
            End Try

            sZhi = sZhi & "," & iHang
            '凭证号需要处理
            sZhi = sZhi & ",'" & iPzHao & "'"

            With dsPZ.Tables(0).Rows(i)
                If .Item("编号") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("编号")) & "'" '编号
                End If
                If .Item("日期") = "" Then
                    sZhi = sZhi & ",'" & tNow & "'"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("日期")) & "'" '日期
                End If
                If .Item("摘要") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("摘要")) & "'" '摘要
                End If
                If .Item("科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("科目"))) & "'"
                End If
                If .Item("对应科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("对应科目"))) & "'"
                End If
                If .Item("借方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方数量")) & "'"
                End If

                If .Item("借方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方单位")) & "'"
                End If

                If .Item("借方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方单价")) & "'"
                End If
                If .Item("借方金额") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Double), 2) & "'"
                    'JinECase = Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Double), 2)
                End If
                If .Item("贷方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方数量")) & "'"
                End If
                If .Item("贷方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方单位")) & "'"
                End If
                If .Item("贷方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方单价")) & "'"
                End If
                If .Item("贷方金额") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Double), 2) & "'"
                    'JinECase = JinECase + Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Double), 2)
                End If

                'If CType(JinECase, Integer) = 0 Then
                '    Continue For
                'End If

                bKuCunSkip = False
                If .Item("仓库") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    If dsPZ.Tables(0).Rows(i).Item("仓库") = "A_ID" Then
                        sZhi = sZhi & ",'{" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("仓库")).ToString & "}'"
                    Else
                        sZhi = sZhi & "," & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("仓库"))
                        If dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("仓库")) = "-1" Then
                            bKuCunSkip = True
                        End If
                    End If

                End If


                If .Item("经手人") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("经手人")) & "'"
                End If
                If .Item("备用1") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用1")) & "'"
                End If
                If .Item("备用2") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用2")) & "'"
                End If
                If .Item("备用3") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用3")) & "'"
                End If
                If .Item("备用4") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用4")) & "'"
                End If
                If .Item("备用5") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用5")) & "'"
                End If


                If .Item("备用6") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用6")) & "'"
                End If
                If .Item("备用7") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用7")) & "'"
                End If
                If .Item("备用8") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用8")) & "'"
                End If
                If .Item("备用9") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用9")) & "'"
                End If
                If .Item("备用10") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用10")) & "'"
                End If
                If .Item("备用11") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用11")) & "'"
                End If
                If .Item("备用12") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用12")) & "'"
                End If
                If .Item("备用13") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用13")) & "'"
                End If
                If .Item("备用14") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用14")) & "'"
                End If
                If .Item("备用15") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用15")) & "'"
                End If
                If .Item("备用16") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用16")) & "'"
                End If
                If .Item("备用17") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用17")) & "'"
                End If
                If .Item("备用18") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用18")) & "'"
                End If
                If .Item("备用19") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用19")) & "'"
                End If
                If .Item("备用20") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用20")) & "'"
                End If

                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & ",''"
                sZhi = sZhi & ",''"
                sZhi = sZhi & "," & FKG.myselfG.FKString(dsPZ.Tables(0).Rows(i).Item("显示").ToString, "")
                sZhi = sZhi & ",'" & tNow & "'"
                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & "," & iGongnengID
                sZhi = sZhi & ",'" & iJiBenCaoZuoID & "'"
            End With

            mdbOpen.oleCommand.CommandText = "insert into PZ (" & sLieming & ") values (" & sZhi & ")"
            Try
                If bKuCunSkip Then
                    '跳过执行保存的语句。
                Else
                    mdbOpen.oleCommand.ExecuteNonQuery()
                End If
            Catch ex As Exception
                Return False
                Exit Function
            End Try
            'If mdb.Write("insert into PZ () values (" & sZhi & ")") = False Then
            '    Return False
            '    Exit Function
            'End If
        Next
        Return True
    End Function


    Private Sub CancelSave(ByVal iJiBenCaoZuoid As String)
        Dim mdbW As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdbW.Write("update YHZhuangTai set A_JBID='" & iJiBenCaoZuoid & "'  where YongHuName='" & FKG.myselfG.YongHu & "'")
        If mdbW.Write("delete from KuCun where A_JBID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
            Exit Sub
        End If

        If mdbW.Write("delete from GongZi where A_JBID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
            Exit Sub
        End If

        Dim sPZH As String = ""
        Dim dspzh As DataSet
        dspzh = mdbW.Reader("select distinct 凭证号 from pz where 编号='" & sBianHao & "'")
        If dspzh.Tables(0).Rows.Count > 0 Then
            sPZH = dspzh.Tables(0).Rows(0).Item(0).ToString
        Else
            sPZH = "-1"
        End If

        If mdbW.Write("delete from PZ where A_JBID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
            Exit Sub
        End If
        Dim sGongNeng As String
        sGongNeng = mdbW.Reader("select ConText from GongNengShu where ID=" & iGongnengID).Tables(0).Rows(0).Item(0)

        Dim tLogin As String
        tLogin = Format(Now, "yyyy-MM-dd hh:mm:ss")

        If mdbW.Write("update JiBenCaoZuo set 删除='true',删除时间='" & tLogin & "', 删除人='" & FKG.myselfG.YongHu & "',功能名称='" & sGongNeng & "' where A_ID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
        End If

        '行数不再重新编写，中间可以留空，以避免在重新编写行数时出错。
        ''重新编写行数
        'mdb.Write("UPDATE kucun AS t SET 行数 = dcount(""行数"",""Kucun"",""编号='" & sBianHao & "' and 行数<="" & t.行数) WHERE 编号='" & sBianHao & "'")
        'mdb.Write("UPDATE GongZi AS t SET 行数 = dcount(""行数"",""GongZi"",""编号='" & sBianHao & "' and 行数<="" & t.行数) WHERE 编号='" & sBianHao & "'")
        'If sPZH <> "-1" Then
        '    mdb.Write("UPDATE PZ AS t SET 行数 = dcount(""行数"",""PZ"",""凭证号='" & sPZH & "' and 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & "  and 行数<="" & t.行数) WHERE 凭证号='" & sPZH & "'  and 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen)
        'End If
        '        mdb.Write("UPDATE PZ AS t SET 行数 = dcount(""行数"",""PZ"",""凭证号=(select distinct 凭证号 from pz where 编号='" & sBianHao & "') and 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & "  and 行数<="" & t.行数) WHERE 凭证号=(select distinct 凭证号 from pz where 编号='" & sBianHao & "') and 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen)

        '关闭记录行数功能,对sql server而言,意义不大.
        ''记录当前各表的行数，以便当发生异常记录丢失时能够及时发现
        'Dim sFilePath As String = mdbW.oleConnect.DataSource
        'sFilePath = sFilePath.ToLower.Replace("\freedomkingdata.mdb", "")

        'Dim xml As New FKSetting.XMLRWer(sFilePath & "\FKRecord.xml", True, "config")
        'xml.SaveInnerText("NA", mdbW.Reader("select count(A_ID) from JiBenCaoZuo ").Tables(0).Rows(0).Item(0), "name", "config")
        'xml.SaveInnerText("NB", mdbW.Reader("select count(A_JBID) from KuCun ").Tables(0).Rows(0).Item(0), "name", "config")
        'xml.SaveInnerText("NC", mdbW.Reader("select count(A_JBID) from GongZi ").Tables(0).Rows(0).Item(0), "name", "config")
        'xml.SaveInnerText("ND", mdbW.Reader("select count(A_JBID) from PZ ").Tables(0).Rows(0).Item(0), "name", "config")

        mdbW.Write("update YHZhuangTai set A_JBID='-1'  where YongHuName='" & FKG.myselfG.YongHu & "'")

        'mdb.Write("UPDATE PZ AS t SET 行数 = dcount(""行数"",""PZ"",""编号='" & sBianHao & "' and 行数<="" & t.行数) WHERE 编号='" & sBianHao & "'")
    End Sub

    Private Function saveJiBen() As String
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asas)

        '首先保存到基本操作表
        Dim sInsert As String
        Dim sLieMing As String
        Dim sZhi As String

        sLieMing = "A_GNSID,Nian,Yue,BianHao,PingZhengZi,PingZhengHao,RiQi,JingShouRen,ZhaiYao,CangKu,JieFangZhuKemu,JieFangKeMu,DaiFangZhuKeMu,DaiFangKeMu,ShuLiang,DanWei,DanJia,JinE,BeiYong1,BeiYong2,BeiYong3,BeiYong4,BeiYong5,BeiYong6,BeiYong7,BeiYong8,BeiYong9,BeiYong10,BeiYong11,BeiYong12,BeiYong13,BeiYong14,BeiYong15,BeiYong16,BeiYong17,BeiYong18,BeiYong19,BeiYong20,BeiYong21,BeiYong22,BeiYong23,BeiYong24,BeiYong25,BeiYong26,BeiYong27,BeiYong28,BeiYong29,BeiYong30,制单,建立时间,建立人"

        sZhi = iGongnengID
        sZhi = sZhi & "," & FKG.myselfG.NianFen
        sZhi = sZhi & "," & FKG.myselfG.YueFen
        sZhi = sZhi & ",'" & Me.BianHao.Text & "'"
        sZhi = sZhi & ",'" & Me.PingZhengZi.Text & "'"
        sZhi = sZhi & ",'" & Me.PingZhengHao.Text & "'"
        sZhi = sZhi & ",'" & Me.RiQi.Text & "'"
        sZhi = sZhi & ",'" & Me.JingShouRen.Text & "'"

        sZhi = sZhi & ",'" & Me.Zhaiyao.Text & "'"
        If Me.CangKuID.Text = "" Then
            sZhi = sZhi & ",0"
        Else
            sZhi = sZhi & "," & Me.CangKuID.Text
        End If
        sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(Me.JieFangZhuKemu.Text) & "'"
        sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(Me.JieFangKeMu.Text) & "'"
        sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(Me.DaiFangZhuKeMu.Text) & "'"
        sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(Me.DaiFangKeMu.Text) & "'"
        sZhi = sZhi & ",'" & Me.ShuLiang.Text & "'"
        sZhi = sZhi & ",'" & Me.DanWei.Text & "'"
        sZhi = sZhi & ",'" & Me.DanJia.Text & "'"
        sZhi = sZhi & ",'" & Me.JinE.Text & "'"

        sZhi = sZhi & ",'" & Me.Beiyong1.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong2.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong3.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong4.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong5.Text & "'"

        sZhi = sZhi & ",'" & Me.BeiYong6.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong7.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong8.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong9.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong10.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong11.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong12.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong13.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong14.Text & "'"
        sZhi = sZhi & ",'" & Me.BeiYong15.Text & "'"

        sZhi = sZhi & ",'" & Me.Beiyong16.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong17.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong18.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong19.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong20.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong21.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong22.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong23.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong24.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong25.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong26.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong27.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong28.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong29.Text & "'"
        sZhi = sZhi & ",'" & Me.Beiyong30.Text & "'"


        sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
        Dim dDate As String
        dDate = tNow
        sZhi = sZhi & ",'" & dDate & "'"
        sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"

        sInsert = "insert into JiBenCaoZuo (" & sLieMing & ") values (" & sZhi & ")"

        mdbOpen.oleCommand.CommandText = sInsert
        mdbOpen.oleCommand.ExecuteNonQuery()
        'mdb.Write(sInsert)

        'Dim iJiBenCaoZuoID As Integer
        Dim iJiBenCaoZuoID As String

        Try
            Dim ds As New DataSet
            mdbOpen.oleCommand.CommandText = "Select A_ID from JiBenCaoZuo where 建立时间='" & dDate & "' and 建立人='" & FKG.myselfG.YongHu & "'"
            mdbOpen.oleDataAdapter.Fill(ds)

            iJiBenCaoZuoID = "{" & ds.Tables(0).Rows(0).Item(0).ToString & "}"
        Catch ex As Exception
            Return "-1" '异常
        End Try

        Return (iJiBenCaoZuoID) '返回当前保存的基本操作的id

        '其次调用基本操作的数据，生成各种数据
    End Function

    Private Function updateSave(ByVal iJibencaozuoid As String) As Boolean
        mdbOpen.oleCommand.CommandText = "update YHZhuangTai set A_JBID='" & iJibencaozuoid & "'  where YongHuName='" & FKG.myselfG.YongHu & "'"
        mdbOpen.oleCommand.ExecuteNonQuery()

        If UpdateJiBen(iJibencaozuoid) AndAlso UpdateKuCun(iJibencaozuoid) AndAlso UpdateGongZi(iJibencaozuoid) AndAlso UpdatePZ(iJibencaozuoid) Then
            '修改成功

            '修改成功后,工作状态改变为浏览而不是新增记录
            iWorkState = 9
            WorkStateChange(iWorkState)


            mdbOpen.oleCommand.CommandText = "update YHZhuangTai set A_JBID='-1'  where YongHuName='" & FKG.myselfG.YongHu & "'"
            mdbOpen.oleCommand.ExecuteNonQuery()

        Else
            MsgBox("操作失败,该记录被删除,请重新输入!", MsgBoxStyle.Information, My.Application.Info.Title)

            mdbOpen.oleConnect.Close()

            CancelSave(iJibencaozuoid)
            FillDG(sBianHao)
        End If

    End Function

    Private Function UpdateJiBen(ByVal iJibenCaozuoID As String) As Boolean
        Dim sInsert As String

        Dim dDate As String
        dDate = tNow

        sInsert = "update jibencaozuo set riqi= '" & Me.RiQi.Text & "',JingShouRen='" & Me.JingShouRen.Text & "',ZhaiYao='" & Me.Zhaiyao.Text & "' ,CangKu=" & Me.CangKuID.Text & ",JieFangZhuKeMu= '" & FKF.FKF.getKMDMfromQM(Me.JieFangZhuKemu.Text) & "',JieFangKeMu= '" & FKF.FKF.getKMDMfromQM(Me.JieFangKeMu.Text) & "',DaiFangZhuKeMu='" & FKF.FKF.getKMDMfromQM(Me.DaiFangZhuKeMu.Text) & "',DaiFangKeMu='" & FKF.FKF.getKMDMfromQM(Me.DaiFangKeMu.Text) & "',ShuLiang='" & Me.ShuLiang.Text & "',DanWei='" & Me.DanWei.Text & "',DanJia= '" & Me.DanJia.Text & "',JinE='" & Me.JinE.Text & "',BeiYong1='" & Me.Beiyong1.Text & "',BeiYong2='" & Me.BeiYong2.Text & "',BeiYong3='" & Me.BeiYong3.Text & "',BeiYong4='" & Me.BeiYong4.Text & "',BeiYong5='" & Me.BeiYong5.Text & "',BeiYong6='" & Me.BeiYong6.Text & "',BeiYong7='" & Me.BeiYong7.Text & "',BeiYong8='" & Me.BeiYong8.Text & "',BeiYong9='" & Me.BeiYong9.Text & "',BeiYong10='" & Me.BeiYong10.Text & "',BeiYong11='" & Me.BeiYong11.Text & "',BeiYong12='" & Me.BeiYong12.Text & "',BeiYong13='" & Me.BeiYong13.Text & "',BeiYong14='" & Me.BeiYong14.Text & "',BeiYong15='" & Me.BeiYong15.Text & "',BeiYong16='" & Me.Beiyong16.Text & "',BeiYong17='" & Me.Beiyong17.Text & "',BeiYong18='" & Me.Beiyong18.Text & "',BeiYong19='" & Me.Beiyong19.Text & "',BeiYong20='" & Me.Beiyong20.Text & "',BeiYong21='" & Me.Beiyong21.Text & "',BeiYong22='" & Me.Beiyong22.Text & "',BeiYong23='" & Me.Beiyong23.Text & "',BeiYong24='" & Me.Beiyong24.Text & "',BeiYong25='" & Me.Beiyong25.Text & "',BeiYong26='" & Me.Beiyong26.Text & "',BeiYong27='" & Me.Beiyong27.Text & "',BeiYong28='" & Me.Beiyong28.Text & "',BeiYong29='" & Me.Beiyong29.Text & "',BeiYong30='" & Me.Beiyong30.Text & "',修改时间='" & dDate & "',修改人='" & FKG.myselfG.YongHu & "'  where A_ID='" & iJibenCaozuoID & "'"
        mdbOpen.oleCommand.CommandText = sInsert
        Try
            mdbOpen.oleCommand.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            MsgBox(ex.ToString)
            Return False
        End Try
    End Function

    Private Function UpdateKuCun(ByVal iJiBenCaoZuoID As String) As Boolean
        '20170412 增加一个判断，当库存ID为 -1 时，不保存该行记录，直接跳过。更改 FKGN 模块，当仓库ID设置为-1时，该条数据不进行保存操作，以避免出现空行的情况。典型应用为生产流程。
        Dim bKuCunSkip As Boolean = False
        Dim nKuCunSkip As Int16 = 0
        Dim dsKuCunHangShu As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from kucun where A_JBID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsKuCunHangShu)

        'Dim dsKuCun As New DataSet
        'mdbOpen.oleCommand.CommandText = "select GN_KuCun.* from GN_KuCun,GongnengShu where GN_KuCun.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID
        'mdbOpen.oleDataAdapter.Fill(dsKuCun)

        Dim dsJiBen As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from Jibencaozuo where A_ID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsJiBen)

        Dim sZhi As String
        Dim i As Integer
        For i = 0 To dsKuCun.Tables(0).Rows.Count - 1
            With dsKuCun.Tables(0).Rows(i)
                If .Item("日期") = "" Then
                    sZhi = "日期= '" & tNow & "'"
                Else
                    sZhi = "日期= '" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("日期")) & "'" '日期
                End If
                If .Item("摘要") = "" Then
                    sZhi = sZhi & ",摘要=''"
                Else
                    sZhi = sZhi & ",摘要='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("摘要")) & "'" '摘要
                End If
                If .Item("科目") = "" Then
                    sZhi = sZhi & ",科目=''"
                Else
                    sZhi = sZhi & ",科目='" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("科目"))) & "'"
                End If
                If .Item("对应科目") = "" Then
                    sZhi = sZhi & ",对应科目=''"
                Else
                    sZhi = sZhi & ",对应科目='" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("对应科目"))) & "'"
                End If
                If .Item("借方数量") = "" Then
                    sZhi = sZhi & ",借方数量=0"
                Else
                    sZhi = sZhi & ",借方数量='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方数量")) & "'"
                End If

                If .Item("借方单位") = "" Then
                    sZhi = sZhi & ",借方单位=''"
                Else
                    sZhi = sZhi & ",借方单位='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方单位")) & "'"
                End If

                If .Item("借方单价") = "" Then
                    sZhi = sZhi & ",借方单价=0"
                Else
                    sZhi = sZhi & ",借方单价='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方单价")) & "'"
                End If
                If .Item("借方金额") = "" Then
                    sZhi = sZhi & ",借方金额=0.00"
                Else
                    sZhi = sZhi & ",借方金额='" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("借方金额")), Decimal), 2) & "'"
                End If
                If .Item("贷方数量") = "" Then
                    sZhi = sZhi & ",贷方数量=0"
                Else
                    sZhi = sZhi & ",贷方数量='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方数量")) & "'"
                End If
                If .Item("贷方单位") = "" Then
                    sZhi = sZhi & ",贷方单位=''"
                Else
                    sZhi = sZhi & ",贷方单位='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方单位")) & "'"
                End If
                If .Item("贷方单价") = "" Then
                    sZhi = sZhi & ",贷方单价=0"
                Else
                    sZhi = sZhi & ",贷方单价='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方单价")) & "'"
                End If
                If .Item("贷方金额") = "" Then
                    sZhi = sZhi & ",贷方金额=0"
                Else
                    sZhi = sZhi & ",贷方金额='" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("贷方金额")), Decimal), 2) & "'"
                End If

                bKuCunSkip = False
                If .Item("仓库") = "" Then
                    sZhi = sZhi & ",仓库=0"
                Else
                    sZhi = sZhi & ",仓库=" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("仓库"))
                    If dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("仓库")) = "-1" Then
                        bKuCunSkip = True
                    End If
                End If


                If .Item("经手人") = "" Then
                    sZhi = sZhi & ",经手人=''"
                Else
                    sZhi = sZhi & ",经手人='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("经手人")) & "'"
                End If
                If .Item("备用1") = "" Then
                    sZhi = sZhi & ",备用1=''"
                Else
                    sZhi = sZhi & ",备用1='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用1")) & "'"
                End If
                If .Item("备用2") = "" Then
                    sZhi = sZhi & ",备用2=''"
                Else
                    sZhi = sZhi & ",备用2='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用2")) & "'"
                End If
                If .Item("备用3") = "" Then
                    sZhi = sZhi & ",备用3=''"
                Else
                    sZhi = sZhi & ",备用3='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用3")) & "'"
                End If
                If .Item("备用4") = "" Then
                    sZhi = sZhi & ",备用4=''"
                Else
                    sZhi = sZhi & ",备用4='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用4")) & "'"
                End If
                If .Item("备用5") = "" Then
                    sZhi = sZhi & ",备用5=''"
                Else
                    sZhi = sZhi & ",备用5='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用5")) & "'"
                End If

                If .Item("备用6") = "" Then
                    sZhi = sZhi & ",备用6=''"
                Else
                    sZhi = sZhi & ",备用6='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用6")) & "'"
                End If
                If .Item("备用7") = "" Then
                    sZhi = sZhi & ",备用7=''"
                Else
                    sZhi = sZhi & ",备用7='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用7")) & "'"
                End If
                If .Item("备用8") = "" Then
                    sZhi = sZhi & ",备用8=''"
                Else
                    sZhi = sZhi & ",备用8='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用8")) & "'"
                End If
                If .Item("备用9") = "" Then
                    sZhi = sZhi & ",备用9=''"
                Else
                    sZhi = sZhi & ",备用9='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用9")) & "'"
                End If
                If .Item("备用10") = "" Then
                    sZhi = sZhi & ",备用10=''"
                Else
                    sZhi = sZhi & ",备用10='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用10")) & "'"
                End If
                If .Item("备用11") = "" Then
                    sZhi = sZhi & ",备用11=''"
                Else
                    sZhi = sZhi & ",备用11='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用11")) & "'"
                End If
                If .Item("备用12") = "" Then
                    sZhi = sZhi & ",备用12=''"
                Else
                    sZhi = sZhi & ",备用12='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用12")) & "'"
                End If
                If .Item("备用13") = "" Then
                    sZhi = sZhi & ",备用13=''"
                Else
                    sZhi = sZhi & ",备用13='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用13")) & "'"
                End If
                If .Item("备用14") = "" Then
                    sZhi = sZhi & ",备用14=''"
                Else
                    sZhi = sZhi & ",备用14='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用14")) & "'"
                End If
                If .Item("备用15") = "" Then
                    sZhi = sZhi & ",备用15=''"
                Else
                    sZhi = sZhi & ",备用15='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用15")) & "'"
                End If
                If .Item("备用16") = "" Then
                    sZhi = sZhi & ",备用16=''"
                Else
                    sZhi = sZhi & ",备用16='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用16")) & "'"
                End If
                If .Item("备用17") = "" Then
                    sZhi = sZhi & ",备用17=''"
                Else
                    sZhi = sZhi & ",备用17='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用17")) & "'"
                End If
                If .Item("备用18") = "" Then
                    sZhi = sZhi & ",备用18=''"
                Else
                    sZhi = sZhi & ",备用18='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用18")) & "'"
                End If
                If .Item("备用19") = "" Then
                    sZhi = sZhi & ",备用19=''"
                Else
                    sZhi = sZhi & ",备用19='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用19")) & "'"
                End If
                If .Item("备用20") = "" Then
                    sZhi = sZhi & ",备用20=''"
                Else
                    sZhi = sZhi & ",备用20='" & dsJiBen.Tables(0).Rows(0).Item(dsKuCun.Tables(0).Rows(i).Item("备用20")) & "'"
                End If


                'sZhi = sZhi & ",制单='" & FKG.myselfG.YongHu & "'"
                '制单人不能修改,否则可能导致同一单证制单人不同
                sZhi = sZhi & ",修改时间='" & tNow & "'"
                sZhi = sZhi & ",修改人='" & FKG.myselfG.YongHu & "'"

            End With
            Dim sUpdate As String
            Dim sCase1, sCase2, sCase3 As String
            sCase1 = "A_JBID = '" & iJiBenCaoZuoID & "'"
            sCase2 = " (行数 % " & dsKuCunHangShu.Tables(0).Rows.Count & "= " & nKuCunSkip + 1 & ")"
            sCase3 = " (行数 % " & dsKuCunHangShu.Tables(0).Rows.Count & "= 0)"

            'sUpdate = "Update KuCun set " & sZhi & " where  " & sCase1 & " and (" & sCase2 & " or " & sCase3 & ")"

            If nKuCunSkip = (dsKuCunHangShu.Tables(0).Rows.Count) - 1 Then
                sUpdate = "Update KuCun set " & sZhi & " where  " & sCase1 & " and " & sCase3
            Else
                sUpdate = "Update KuCun set " & sZhi & " where  " & sCase1 & " and " & sCase2
            End If

            mdbOpen.oleCommand.CommandText = sUpdate
            Try
                'mdbOpen.oleCommand.ExecuteNonQuery()

                If bKuCunSkip Then
                    '跳过执行保存的语句。
                Else
                    nKuCunSkip = nKuCunSkip + 1
                    mdbOpen.oleCommand.ExecuteNonQuery()
                End If
            Catch ex As Exception
                Return False
                Exit Function
            End Try

        Next
        Return True
    End Function

    Private Function UpdateGongZi(ByVal iJiBenCaoZuoID As String) As Boolean
        '20170412 增加一个判断，当库存ID为 -1 时，不保存该行记录，直接跳过。更改 FKGN 模块，当仓库ID设置为-1时，该条数据不进行保存操作，以避免出现空行的情况。典型应用为生产流程。
        Dim bKuCunSkip As Boolean = False
        Dim nGongZiSkip As Integer = 0
        Dim dsGongZiHangShu As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from gongzi where A_JBID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsGongZiHangShu)

        'Dim dsGongZi As New DataSet
        'mdbOpen.oleCommand.CommandText = "select GN_GongZi.* from GN_GongZi,GongnengShu where GN_GongZi.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID
        'mdbOpen.oleDataAdapter.Fill(dsGongZi)

        Dim dsJiBen As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from Jibencaozuo where A_ID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsJiBen)

        Dim sZhi As String
        Dim i As Integer
        For i = 0 To dsGongZi.Tables(0).Rows.Count - 1
            With dsGongZi.Tables(0).Rows(i)
                If .Item("日期") = "" Then
                    sZhi = "日期= '" & tNow & "'"
                Else
                    sZhi = "日期= '" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("日期")) & "'" '日期
                End If
                If .Item("摘要") = "" Then
                    sZhi = sZhi & ",摘要=''"
                Else
                    sZhi = sZhi & ",摘要='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("摘要")) & "'" '摘要
                End If
                If .Item("科目") = "" Then
                    sZhi = sZhi & ",科目=''"
                Else
                    sZhi = sZhi & ",科目='" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("科目"))) & "'"
                End If
                If .Item("对应科目") = "" Then
                    sZhi = sZhi & ",对应科目=''"
                Else
                    sZhi = sZhi & ",对应科目='" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("对应科目"))) & "'"
                End If
                If .Item("借方数量") = "" Then
                    sZhi = sZhi & ",借方数量=0"
                Else
                    sZhi = sZhi & ",借方数量='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方数量")) & "'"
                End If

                If .Item("借方单位") = "" Then
                    sZhi = sZhi & ",借方单位=''"
                Else
                    sZhi = sZhi & ",借方单位='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方单位")) & "'"
                End If

                If .Item("借方单价") = "" Then
                    sZhi = sZhi & ",借方单价=0"
                Else
                    sZhi = sZhi & ",借方单价='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方单价")) & "'"
                End If
                If .Item("借方金额") = "" Then
                    sZhi = sZhi & ",借方金额=0.00"
                Else
                    sZhi = sZhi & ",借方金额='" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("借方金额")), Decimal), 2) & "'"
                End If
                If .Item("贷方数量") = "" Then
                    sZhi = sZhi & ",贷方数量=0"
                Else
                    sZhi = sZhi & ",贷方数量='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方数量")) & "'"
                End If
                If .Item("贷方单位") = "" Then
                    sZhi = sZhi & ",贷方单位=''"
                Else
                    sZhi = sZhi & ",贷方单位='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方单位")) & "'"
                End If
                If .Item("贷方单价") = "" Then
                    sZhi = sZhi & ",贷方单价=0"
                Else
                    sZhi = sZhi & ",贷方单价='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方单价")) & "'"
                End If
                If .Item("贷方金额") = "" Then
                    sZhi = sZhi & ",贷方金额=0"
                Else
                    sZhi = sZhi & ",贷方金额='" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("贷方金额")), Decimal), 2) & "'"
                End If

                bKuCunSkip = False
                If .Item("仓库") = "" Then
                    sZhi = sZhi & ",仓库=0"
                Else
                    sZhi = sZhi & ",仓库=" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("仓库"))
                    If dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("仓库")) = "-1" Then
                        bKuCunSkip = True
                    End If
                End If


                If .Item("经手人") = "" Then
                    sZhi = sZhi & ",经手人=''"
                Else
                    sZhi = sZhi & ",经手人='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("经手人")) & "'"
                End If
                If .Item("备用1") = "" Then
                    sZhi = sZhi & ",备用1=''"
                Else
                    sZhi = sZhi & ",备用1='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用1")) & "'"
                End If
                If .Item("备用2") = "" Then
                    sZhi = sZhi & ",备用2=''"
                Else
                    sZhi = sZhi & ",备用2='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用2")) & "'"
                End If
                If .Item("备用3") = "" Then
                    sZhi = sZhi & ",备用3=''"
                Else
                    sZhi = sZhi & ",备用3='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用3")) & "'"
                End If
                If .Item("备用4") = "" Then
                    sZhi = sZhi & ",备用4=''"
                Else
                    sZhi = sZhi & ",备用4='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用4")) & "'"
                End If
                If .Item("备用5") = "" Then
                    sZhi = sZhi & ",备用5=''"
                Else
                    sZhi = sZhi & ",备用5='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用5")) & "'"
                End If


                If .Item("备用6") = "" Then
                    sZhi = sZhi & ",备用6=''"
                Else
                    sZhi = sZhi & ",备用6='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用6")) & "'"
                End If
                If .Item("备用7") = "" Then
                    sZhi = sZhi & ",备用7=''"
                Else
                    sZhi = sZhi & ",备用7='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用7")) & "'"
                End If
                If .Item("备用8") = "" Then
                    sZhi = sZhi & ",备用8=''"
                Else
                    sZhi = sZhi & ",备用8='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用8")) & "'"
                End If
                If .Item("备用9") = "" Then
                    sZhi = sZhi & ",备用9=''"
                Else
                    sZhi = sZhi & ",备用9='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用9")) & "'"
                End If
                If .Item("备用10") = "" Then
                    sZhi = sZhi & ",备用10=''"
                Else
                    sZhi = sZhi & ",备用10='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用10")) & "'"
                End If
                If .Item("备用11") = "" Then
                    sZhi = sZhi & ",备用11=''"
                Else
                    sZhi = sZhi & ",备用11='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用11")) & "'"
                End If
                If .Item("备用12") = "" Then
                    sZhi = sZhi & ",备用12=''"
                Else
                    sZhi = sZhi & ",备用12='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用12")) & "'"
                End If
                If .Item("备用13") = "" Then
                    sZhi = sZhi & ",备用13=''"
                Else
                    sZhi = sZhi & ",备用13='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用13")) & "'"
                End If
                If .Item("备用14") = "" Then
                    sZhi = sZhi & ",备用14=''"
                Else
                    sZhi = sZhi & ",备用14='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用14")) & "'"
                End If
                If .Item("备用15") = "" Then
                    sZhi = sZhi & ",备用15=''"
                Else
                    sZhi = sZhi & ",备用15='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用15")) & "'"
                End If
                If .Item("备用16") = "" Then
                    sZhi = sZhi & ",备用16=''"
                Else
                    sZhi = sZhi & ",备用16='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用16")) & "'"
                End If
                If .Item("备用17") = "" Then
                    sZhi = sZhi & ",备用17=''"
                Else
                    sZhi = sZhi & ",备用17='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用17")) & "'"
                End If
                If .Item("备用18") = "" Then
                    sZhi = sZhi & ",备用18=''"
                Else
                    sZhi = sZhi & ",备用18='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用18")) & "'"
                End If
                If .Item("备用19") = "" Then
                    sZhi = sZhi & ",备用19=''"
                Else
                    sZhi = sZhi & ",备用19='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用19")) & "'"
                End If
                If .Item("备用20") = "" Then
                    sZhi = sZhi & ",备用20=''"
                Else
                    sZhi = sZhi & ",备用20='" & dsJiBen.Tables(0).Rows(0).Item(dsGongZi.Tables(0).Rows(i).Item("备用20")) & "'"
                End If

                'sZhi = sZhi & ",制单='" & FKG.myselfG.YongHu & "'"
                '制单人不能修改,否则可能导致同一单证制单人不同
                sZhi = sZhi & ",修改时间='" & tNow & "'"
                sZhi = sZhi & ",修改人='" & FKG.myselfG.YongHu & "'"

            End With
            Dim sUpdate As String
            Dim sCase1, sCase2, sCase3 As String
            sCase1 = "A_JBID = '" & iJiBenCaoZuoID & "'"
            sCase2 = " (行数 % " & dsGongZiHangShu.Tables(0).Rows.Count & "= " & nGongZiSkip + 1 & ")"
            sCase3 = " (行数 % " & dsGongZiHangShu.Tables(0).Rows.Count & "= 0)"


            'sUpdate = "Update GongZi set " & sZhi & " where  " & sCase1 & " and (" & sCase2 & " or " & sCase3 & ")"
            If nGongZiSkip = (dsGongZiHangShu.Tables(0).Rows.Count) - 1 Then
                sUpdate = "Update gongzi set " & sZhi & " where  " & sCase1 & " and " & sCase3
            Else
                sUpdate = "Update gongzi set " & sZhi & " where  " & sCase1 & " and " & sCase2
            End If

            mdbOpen.oleCommand.CommandText = sUpdate
            Try
                'mdbOpen.oleCommand.ExecuteNonQuery()
                If bKuCunSkip Then
                    '跳过执行保存的语句。
                Else
                    nGongZiSkip = nGongZiSkip + 1
                    mdbOpen.oleCommand.ExecuteNonQuery()
                End If
            Catch ex As Exception
                Return False
                Exit Function
            End Try

        Next
        Return True
    End Function

    Private Function UpdatePZ(ByVal iJiBenCaoZuoID As String) As Boolean
        '20170412 增加一个判断，当库存ID为 -1 时，不保存该行记录，直接跳过。更改 FKGN 模块，当仓库ID设置为-1时，该条数据不进行保存操作，以避免出现空行的情况。典型应用为生产流程。
        Dim bKuCunSkip As Boolean = False
        Dim nPZSkip As Integer = 0
        Dim dsPZHangShu As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from PZ where A_JBID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsPZHangShu)

        'Dim dsPZ As New DataSet
        'mdbOpen.oleCommand.CommandText = "select GN_PZ.* from GN_PZ,GongnengShu where GN_PZ.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID
        'mdbOpen.oleDataAdapter.Fill(dsPZ)

        Dim dsJiBen As New DataSet
        mdbOpen.oleCommand.CommandText = "select * from Jibencaozuo where A_ID ='" & iJiBenCaoZuoID & "'"
        mdbOpen.oleDataAdapter.Fill(dsJiBen)

        'Dim JinECase As Double = 0
        'Dim iHnag As Integer = -1

        Dim sZhi As String
        Dim i As Integer

        For i = 0 To dsPZ.Tables(0).Rows.Count - 1
            With dsPZ.Tables(0).Rows(i)
                If .Item("日期") = "" Then
                    sZhi = "日期= '" & tNow & "'"
                Else
                    sZhi = "日期= '" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("日期")) & "'" '日期
                End If
                If .Item("摘要") = "" Then
                    sZhi = sZhi & ",摘要=''"
                Else
                    sZhi = sZhi & ",摘要='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("摘要")) & "'" '摘要
                End If
                If .Item("科目") = "" Then
                    sZhi = sZhi & ",科目=''"
                Else
                    sZhi = sZhi & ",科目='" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("科目"))) & "'"
                End If
                If .Item("对应科目") = "" Then
                    sZhi = sZhi & ",对应科目=''"
                Else
                    sZhi = sZhi & ",对应科目='" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("对应科目"))) & "'"
                End If
                If .Item("借方数量") = "" Then
                    sZhi = sZhi & ",借方数量=0"
                Else
                    sZhi = sZhi & ",借方数量='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方数量")) & "'"
                End If

                If .Item("借方单位") = "" Then
                    sZhi = sZhi & ",借方单位=''"
                Else
                    sZhi = sZhi & ",借方单位='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方单位")) & "'"
                End If

                If .Item("借方单价") = "" Then
                    sZhi = sZhi & ",借方单价=0"
                Else
                    sZhi = sZhi & ",借方单价='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方单价")) & "'"
                End If
                If .Item("借方金额") = "" Then
                    sZhi = sZhi & ",借方金额=0.00"
                Else
                    sZhi = sZhi & ",借方金额='" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Decimal), 2) & "'"
                    'JinECase = Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Double), 2)
                End If
                If .Item("贷方数量") = "" Then
                    sZhi = sZhi & ",贷方数量=0"
                Else
                    sZhi = sZhi & ",贷方数量='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方数量")) & "'"
                End If
                If .Item("贷方单位") = "" Then
                    sZhi = sZhi & ",贷方单位=''"
                Else
                    sZhi = sZhi & ",贷方单位='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方单位")) & "'"
                End If
                If .Item("贷方单价") = "" Then
                    sZhi = sZhi & ",贷方单价=0"
                Else
                    sZhi = sZhi & ",贷方单价='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方单价")) & "'"
                End If
                If .Item("贷方金额") = "" Then
                    sZhi = sZhi & ",贷方金额=0"
                Else
                    sZhi = sZhi & ",贷方金额='" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Decimal), 2) & "'"
                    'JinECase = JinECase + Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Double), 2)
                End If

                'If CType(JinECase, Integer) = 0 Then
                '    Continue For
                'Else
                '    iHnag = iHnag + 1
                'End If
                bKuCunSkip = False
                If .Item("仓库") = "" Then
                    sZhi = sZhi & ",仓库=0"
                Else
                    If dsPZ.Tables(0).Rows(i).Item("仓库") = "A_ID" Then
                        sZhi = sZhi & ",仓库='{" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("仓库")).ToString & "}'"
                    Else
                        sZhi = sZhi & ",仓库=" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("仓库"))
                        If dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("仓库")) = "-1" Then
                            bKuCunSkip = True
                        End If
                    End If
                End If

                If .Item("经手人") = "" Then
                    sZhi = sZhi & ",经手人=''"
                Else
                    sZhi = sZhi & ",经手人='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("经手人")) & "'"
                End If
                If .Item("备用1") = "" Then
                    sZhi = sZhi & ",备用1=''"
                Else
                    sZhi = sZhi & ",备用1='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用1")) & "'"
                End If
                If .Item("备用2") = "" Then
                    sZhi = sZhi & ",备用2=''"
                Else
                    sZhi = sZhi & ",备用2='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用2")) & "'"
                End If
                If .Item("备用3") = "" Then
                    sZhi = sZhi & ",备用3=''"
                Else
                    sZhi = sZhi & ",备用3='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用3")) & "'"
                End If
                If .Item("备用4") = "" Then
                    sZhi = sZhi & ",备用4=''"
                Else
                    sZhi = sZhi & ",备用4='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用4")) & "'"
                End If
                If .Item("备用5") = "" Then
                    sZhi = sZhi & ",备用5=''"
                Else
                    sZhi = sZhi & ",备用5='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用5")) & "'"
                End If

                If .Item("备用6") = "" Then
                    sZhi = sZhi & ",备用6=''"
                Else
                    sZhi = sZhi & ",备用6='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用6")) & "'"
                End If
                If .Item("备用7") = "" Then
                    sZhi = sZhi & ",备用7=''"
                Else
                    sZhi = sZhi & ",备用7='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用7")) & "'"
                End If
                If .Item("备用8") = "" Then
                    sZhi = sZhi & ",备用8=''"
                Else
                    sZhi = sZhi & ",备用8='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用8")) & "'"
                End If
                If .Item("备用9") = "" Then
                    sZhi = sZhi & ",备用9=''"
                Else
                    sZhi = sZhi & ",备用9='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用9")) & "'"
                End If
                If .Item("备用10") = "" Then
                    sZhi = sZhi & ",备用10=''"
                Else
                    sZhi = sZhi & ",备用10='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用10")) & "'"
                End If
                If .Item("备用11") = "" Then
                    sZhi = sZhi & ",备用11=''"
                Else
                    sZhi = sZhi & ",备用11='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用11")) & "'"
                End If
                If .Item("备用12") = "" Then
                    sZhi = sZhi & ",备用12=''"
                Else
                    sZhi = sZhi & ",备用12='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用12")) & "'"
                End If
                If .Item("备用13") = "" Then
                    sZhi = sZhi & ",备用13=''"
                Else
                    sZhi = sZhi & ",备用13='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用13")) & "'"
                End If
                If .Item("备用14") = "" Then
                    sZhi = sZhi & ",备用14=''"
                Else
                    sZhi = sZhi & ",备用14='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用14")) & "'"
                End If
                If .Item("备用15") = "" Then
                    sZhi = sZhi & ",备用15=''"
                Else
                    sZhi = sZhi & ",备用15='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用15")) & "'"
                End If
                If .Item("备用16") = "" Then
                    sZhi = sZhi & ",备用16=''"
                Else
                    sZhi = sZhi & ",备用16='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用16")) & "'"
                End If
                If .Item("备用17") = "" Then
                    sZhi = sZhi & ",备用17=''"
                Else
                    sZhi = sZhi & ",备用17='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用17")) & "'"
                End If
                If .Item("备用18") = "" Then
                    sZhi = sZhi & ",备用18=''"
                Else
                    sZhi = sZhi & ",备用18='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用18")) & "'"
                End If
                If .Item("备用19") = "" Then
                    sZhi = sZhi & ",备用19=''"
                Else
                    sZhi = sZhi & ",备用19='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用19")) & "'"
                End If
                If .Item("备用20") = "" Then
                    sZhi = sZhi & ",备用20=''"
                Else
                    sZhi = sZhi & ",备用20='" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用20")) & "'"
                End If


                'sZhi = sZhi & ",制单='" & FKG.myselfG.YongHu & "'"
                '制单人不能修改,否则可能导致同一单证制单人不同
                sZhi = sZhi & ",修改时间='" & tNow & "'"
                sZhi = sZhi & ",修改人='" & FKG.myselfG.YongHu & "'"

            End With
            Dim sUpdate As String
            Dim sCase1, sCase2, sCase3 As String
            sCase1 = "A_JBID = '" & iJiBenCaoZuoID & "'"
            sCase2 = " (行数 % " & dsPZHangShu.Tables(0).Rows.Count & "= " & nPZSkip + 1 & ")"
            sCase3 = " (行数 % " & dsPZHangShu.Tables(0).Rows.Count & "= 0 )"

            'sUpdate = "Update PZ set " & sZhi & " where  " & sCase1 & " and (" & sCase2 & " or " & sCase3 & ")"
            If nPZSkip = (dsPZHangShu.Tables(0).Rows.Count) - 1 Then
                sUpdate = "Update PZ set " & sZhi & " where  " & sCase1 & " and " & sCase3
            Else
                sUpdate = "Update PZ set " & sZhi & " where  " & sCase1 & " and " & sCase2
            End If

            mdbOpen.oleCommand.CommandText = sUpdate
            Try
                'mdbOpen.oleCommand.ExecuteNonQuery()
                If bKuCunSkip Then
                    '跳过执行保存的语句。
                Else
                    nPZSkip = nPZSkip + 1
                    mdbOpen.oleCommand.ExecuteNonQuery()
                End If
            Catch ex As Exception
                Return False
                Exit Function
            End Try

        Next
        Return True
    End Function

    Private Sub 显示_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 显示.Click
        If Me.cbFBianHao.Text = "" Then
            Exit Sub
        End If
        sBianHao = Me.cbFBianHao.Text
        FillDG(Me.cbFBianHao.Text)

        Me.BHList.SelectedIndex = Me.cbFBianHao.SelectedIndex
        If Me.cbFBianHao.SelectedIndex = 0 Then
            Me.上单.Enabled = False
            Me.首单.Enabled = False
        Else
            Me.上单.Enabled = True
            Me.首单.Enabled = True
        End If
        If Me.cbFBianHao.SelectedIndex = Me.cbFBianHao.Items.Count - 1 Then
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        Else
            Me.下单.Enabled = True
            Me.尾单.Enabled = True
        End If

        'sBianHao = Me.cbFBianHao.Text
        If Me.dgvKuCun.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvKuCun, Nothing)
        ElseIf Me.dgvGongZi.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvGongZi, Nothing)
        ElseIf Me.dgvPZ.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvPZ, Nothing)
        End If

        iWorkState = 9
        WorkStateChange(iWorkState)
    End Sub

    Private Sub cbFBianHao_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbFBianHao.Enter
        Me.cbFBianHao.Text = ""

        Dim ds As DataSet
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        ds = mdb.ReaderOpen("select DISTINCT  Bianhao from jibencaozuo where A_GNSID=" & iGongnengID & " and nian=" & FKG.myselfG.NianFen & " and Yue=" & FKG.myselfG.YueFen & " and 删除='false' order by bianhao")

        Me.cbFBianHao.ComboBox.DataSource = ds.Tables(0)
        Me.cbFBianHao.ComboBox.ValueMember = ds.Tables(0).Columns(0).ColumnName
        Me.cbFBianHao.ComboBox.DisplayMember = ds.Tables(0).Columns(0).ColumnName
    End Sub

    Private Sub 显示凭证_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles 显示凭证.Click
        If Me.cbFPZHao.Text = "" Then
            Exit Sub
        End If
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim pzz, pzhao As String
        Dim stemp() As String
        stemp = Split(Me.cbFPZHao.Text.Replace("    ", " "))
        pzz = stemp(0)
        pzhao = stemp(1)

        sBianHao = mdb.ReaderOpen("select top 1 Bianhao from jibencaozuo where pingzhengzi='" & pzz & "' and Pingzhenghao=" & pzhao & " and  nian=" & FKG.myselfG.NianFen & " and Yue=" & FKG.myselfG.YueFen).Tables(0).Rows(0).Item(0).ToString
        FillDG(sBianHao)
        'FillCE("编号='" & Me.txtFBianHao.Text & "'")

        If Me.dgvKuCun.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvKuCun, Nothing)
        ElseIf Me.dgvGongZi.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvGongZi, Nothing)
        ElseIf Me.dgvPZ.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvPZ, Nothing)
        End If

        iWorkState = 9
        WorkStateChange(iWorkState)
    End Sub

    Private Sub cbFPZHao_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbFPZHao.Enter
        Me.cbFPZHao.Text = ""

        Dim ds As DataSet
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        ds = mdb.ReaderOpen("select DISTINCT   pingzhengzi & ""    "" & pingzhenghao  from jibencaozuo where A_GNSID=" & iGongnengID & " and nian=" & FKG.myselfG.NianFen & " and   Yue=" & FKG.myselfG.YueFen & " and PingZhengHao>0 and 删除='false'")

        Me.cbFPZHao.ComboBox.DataSource = ds.Tables(0)
        Me.cbFPZHao.ComboBox.ValueMember = ds.Tables(0).Columns(0).ColumnName
        Me.cbFPZHao.ComboBox.DisplayMember = ds.Tables(0).Columns(0).ColumnName

    End Sub

    Private Sub 新增单证_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 新增单证.Click
        '还原所有输入框为默认值，并清除显示表
        HuanYuan(True)
        '自动编号
        Me.BianHao.Text = FKF.FKF.AutoBianHao(Me.BianHao.FKShuXing.Zhi)
        sBianHao = Me.BianHao.Text
        Me.BianHao.ReadOnly = True
        Me.RefreshGongShi(Me.BianHao.Name)

        getMaxPzHao(Me.PingZhengZi.Text)
        Me.PingZhengHao.Text = IMaxPZHao + 1
        Me.PingZhengHao.ReadOnly = True

        'FillCE(0)
        iWorkState = 0
        WorkStateChange(iWorkState)
        FillDG("-1")
        '将当前工作状态改为新增单证
        'Me.保存.Enabled = True
        'Me.删除记录.Enabled = False
        'Me.修改记录.Enabled = False
        'Me.新增记录.Enabled = False
        'Me.pbDelete.Visible = False
        If Me.BHList.Items.Count > 0 Then
            Me.上单.Enabled = True
            Me.首单.Enabled = True
        Else
            Me.上单.Enabled = False
            Me.首单.Enabled = False
        End If
        Me.下单.Enabled = False
        Me.尾单.Enabled = False

        Me.tssZhuangtai.Text = "就绪"

        BHList.SelectedIndex = -1
    End Sub

    Private Sub 新增记录_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 新增记录.Click
        HuanYuan(False)
        '有选择地还原输入框为默认值，并清除显示表
        Me.BianHao.Text = sBianHao
        Me.BianHao.ReadOnly = True
        Me.PingZhengHao.ReadOnly = True
        '将当前工作状态改为新增记录
        iWorkState = 1
        WorkStateChange(iWorkState)
        'Me.保存.Enabled = True
        'Me.删除记录.Enabled = False
        'Me.修改记录.Enabled = False
        'Me.新增记录.Enabled = True
    End Sub

    Private Sub 修改记录_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 修改记录.Click
        '将当前工作状态改为修改记录
        iWorkState = 2
        WorkStateChange(iWorkState)

        'Me.BianHao.ReadOnly = True
        'Me.PingZhengHao.ReadOnly = True
        'Me.保存.Enabled = True

    End Sub

    Private Sub 删除记录_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 删除记录.Click
        '先关闭只读连接，在下次读取时打开。
        mdb.oleConnect.Close()

        If MsgBox("请确认删除选定记录!", MsgBoxStyle.YesNo, "提示") = MsgBoxResult.Yes Then

            mdbOpen.oleCommand.CommandText = "select * from YHZhuangTai where YongHuName='" & FKG.myselfG.YongHu & "' and YHMAC='" & My.Computer.Name & "' and LoginTime='" & FKG.myselfG.dLogin & "'"
            Dim ds As New DataSet
            mdbOpen.oleDataAdapter.Fill(ds)

            If ds.Tables(0).Rows.Count = 0 Then
                MsgBox("该用户已经在其他地方登录，你已被迫注销，若要继续进行操作，请重新登录！", MsgBoxStyle.Information, "提示")
                mdbOpen.oleConnect.Close() '关闭连接，重要
                Me.DialogResult = Windows.Forms.DialogResult.Abort
                Me.Close()
            End If
        Else
            Exit Sub
        End If
        '删除记录
        iWorkState = 3
        CancelSave(iJiBenID)
        'FillCE("编号='" & sBianHao & "'")
        FillDG(sBianHao)

        iWorkState = 9
        WorkStateChange(iWorkState)

        If Me.dgvKuCun.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvKuCun, Nothing)
        ElseIf Me.dgvGongZi.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvGongZi, Nothing)
        ElseIf Me.dgvPZ.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvPZ, Nothing)
        End If
    End Sub

    'Private Sub 取消_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 取消.Click
    '    Me.新增记录.Enabled = True
    'End Sub


    Private Sub 首单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 首单.Click


        iWorkState = 9
        WorkStateChange(iWorkState)

        If Me.BHList.Items.Count > 1 Then
            Me.下单.Enabled = True
            Me.尾单.Enabled = True
        Else
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        End If
        Me.上单.Enabled = False
        Me.首单.Enabled = False

        If Me.BHList.Items.Count = 0 Then
            Exit Sub
        End If

        Me.BHList.SelectedIndex = 0
        sBianHao = Me.BHList.SelectedItem.ToString

        FillDG(sBianHao)

        If Me.dgvKuCun.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvKuCun, Nothing)
        ElseIf Me.dgvGongZi.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvGongZi, Nothing)
        ElseIf Me.dgvPZ.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvPZ, Nothing)
        End If

    End Sub

    Private Sub 上单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 上单.Click
        If Me.BHList.Items.Count > 1 Then
            Me.下单.Enabled = True
            Me.尾单.Enabled = True
        Else
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        End If
        iWorkState = 9
        WorkStateChange(iWorkState)

        If Me.BHList.Items.Count = 0 Or Me.BHList.SelectedIndex = 0 Then
            Exit Sub
        End If

        If Me.BHList.SelectedIndex = -1 Then
            Me.BHList.SelectedIndex = Me.BHList.Items.Count - 1
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        Else
            Me.BHList.SelectedIndex = Me.BHList.SelectedIndex - 1
        End If
        sBianHao = Me.BHList.SelectedItem.ToString
        FillDG(sBianHao)

        If Me.BHList.SelectedIndex <= 0 Then
            Me.上单.Enabled = False
            Me.首单.Enabled = False
        End If

        If Me.dgvKuCun.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvKuCun, Nothing)
        ElseIf Me.dgvGongZi.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvGongZi, Nothing)
        ElseIf Me.dgvPZ.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvPZ, Nothing)
        End If

    End Sub

    Private Sub 下单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 下单.Click
        Me.上单.Enabled = True
        Me.首单.Enabled = True
        iWorkState = 9
        WorkStateChange(iWorkState)

        'If Me.BHList.Items.Count = 0 OrElse Me.BHList.SelectedIndex = Me.BHList.Items.Count - 1 OrElse Me.BHList.SelectedIndex = -1 Then
        '    Exit Sub
        'End If

        Me.BHList.SelectedIndex = Me.BHList.SelectedIndex + 1
        sBianHao = Me.BHList.SelectedItem.ToString

        FillDG(sBianHao)

        If Me.BHList.SelectedIndex = Me.BHList.Items.Count - 1 Then
            Me.下单.Enabled = False
            Me.尾单.Enabled = False
        End If

        If Me.dgvKuCun.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvKuCun, Nothing)
        ElseIf Me.dgvGongZi.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvGongZi, Nothing)
        ElseIf Me.dgvPZ.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvPZ, Nothing)
        End If


    End Sub

    Private Sub 尾单_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 尾单.Click
        Me.上单.Enabled = True
        Me.首单.Enabled = True
        Me.下单.Enabled = False
        Me.尾单.Enabled = False
        iWorkState = 9
        WorkStateChange(iWorkState)

        If Me.BHList.Items.Count = 0 OrElse Me.BHList.SelectedIndex = Me.BHList.Items.Count - 1 OrElse Me.BHList.SelectedIndex = -1 Then
            Exit Sub
        End If
        Me.BHList.SelectedIndex = Me.BHList.Items.Count - 1
        sBianHao = Me.BHList.SelectedItem.ToString

        FillDG(sBianHao)
        If Me.dgvKuCun.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvKuCun, Nothing)
        ElseIf Me.dgvGongZi.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvGongZi, Nothing)
        ElseIf Me.dgvPZ.Visible Then
            Me.dgvKuCun_CellEnter(Me.dgvPZ, Nothing)
        End If

    End Sub

    Private Sub JieFangKeMu_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles JieFangKeMu.MouseDown, DaiFangKeMu.MouseDown
        If CType(sender, Freedom.FKTextBox).ReadOnly Then
            Exit Sub
        End If

        Me.ActiveControl = sender

        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim sTJ As String
            If CType(sender, Freedom.FKTextBox).Name.ToLower = "jiefangkemu" Then
                sTJ = "(" & getKMTJ(True) & ")"
            Else
                sTJ = "(" & getKMTJ(False) & ")"
            End If

            Dim dlg As FKKM.FKXKM
            If sTJ = "(*)" Then
                dlg = New FKKM.FKXKM("1=1", True, False)
            Else
                dlg = New FKKM.FKXKM(sTJ, True, False)
            End If
            'dlg.sKey = CType(sender, Freedom.FKTextBox).Text
            dlg.sKey = FKF.FKF.getKMDMfromQM(CType(sender, Freedom.FKTextBox).Text)
            dlg.ShowDialog()
            Me.Refresh()

            If dlg.myKM.sKMDM = "" Then
                CType(sender, Freedom.FKTextBox).Text = ""
            Else
                CType(sender, Freedom.FKTextBox).Text = dlg.myKM.sKMDM & "   " & dlg.myKM.sKMMC
                My.Computer.Keyboard.SendKeys("{Tab}")
            End If
        End If
    End Sub

    Private Sub JieFangZhuKemu_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles JieFangZhuKemu.MouseDown, DaiFangZhuKeMu.MouseDown
        If CType(sender, Freedom.FKTextBox).ReadOnly Then
            Exit Sub
        End If

        Me.ActiveControl = sender

        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim dlg As New FKKM.FKXKM("1=1", False)
            dlg.ShowDialog()
            Me.Refresh()

            If dlg.myKM.sKMDM = "" Then
                CType(sender, Freedom.FKTextBox).Text = ""
            Else
                CType(sender, Freedom.FKTextBox).Text = dlg.myKM.sKMDM & "   " & dlg.myKM.sKMQM
                My.Computer.Keyboard.SendKeys("{Tab}")
            End If
        End If
    End Sub

    Private Sub JieFangZhuKemu_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles JieFangZhuKemu.Validating

        With CType(sender, Freedom.FKTextBox)
            If .Text = "" Then
                Exit Sub
            End If

            Dim sDM As String
            Dim I As Integer
            I = .Text.IndexOf(" ")
            If I <= 0 Then
                sDM = .Text
            Else
                sDM = .Text.Substring(0, I)
            End If
            .Text = FKF.FKF.getKMQM(" KMDM='" & sDM & "'")

            If .Text = "" Then
                e.Cancel = True
                Dim dlg As New FKKM.FKXKM("1=1", False)
                dlg.ShowDialog()
                Me.Refresh()

                If dlg.myKM.sKMDM = "" Then

                Else
                    .Text = dlg.myKM.sKMDM & "   " & dlg.myKM.sKMQM
                    My.Computer.Keyboard.SendKeys("{Tab}")
                End If
            End If

        End With

    End Sub

    Private Sub DaiFangZhuKeMu_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DaiFangZhuKeMu.Validating

        With CType(sender, Freedom.FKTextBox)
            If .Text = "" Then
                Exit Sub
            End If

            Dim sDM As String
            Dim I As Integer
            I = .Text.IndexOf(" ")
            If I <= 0 Then
                sDM = .Text
            Else
                sDM = .Text.Substring(0, I)
            End If
            .Text = FKF.FKF.getKMQM(" KMDM='" & sDM & "'")

            If .Text = "" Then
                e.Cancel = True
                Dim dlg As New FKKM.FKXKM("1=1", False)
                dlg.ShowDialog()
                Me.Refresh()

                If dlg.myKM.sKMDM = "" Then
                Else
                    .Text = dlg.myKM.sKMDM & "   " & dlg.myKM.sKMQM
                    My.Computer.Keyboard.SendKeys("{Tab}")
                End If
            End If

        End With

    End Sub

    Private Sub JieFangKeMu_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JieFangKeMu.TextChanged
        'If Me.JieFangKeMu.Text = "" Then
        '    Exit Sub
        'End If
        ''If Me.JieFangKeMu.Text.IndexOf(" ") <= 0 Then
        '    Exit Sub
        'End If
        'If Me.JieFangKeMu.ReadOnly = False Then
        '    Read(Me.JieFangKeMu.Text.Substring(Me.JieFangKeMu.Text.IndexOf(" "), Me.JieFangKeMu.Text.Length - Me.JieFangKeMu.Text.IndexOf(" ")))
        'End If

        'YETS(Me.JieFangKeMu.Text, True, My.Settings.YETSJ)
    End Sub

    Private Sub JieFangKeMu_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles JieFangKeMu.Validated
        YETS(Me.JieFangKeMu.Text, True, My.Settings.YETSJ)

        '20200812无论在何种工作状态，都必须刷新公式，否则可能出现先点修改记录和后点修改记录数据不一致的问题。
        'If iWorkState = 9 Then
        '    Exit Sub
        'End If
        Me.RefreshGongShi(CType(sender, System.Windows.Forms.Control).Name)

        If Me.JieFangKeMu.Text = "" Then
            Exit Sub
        End If
        If Me.JieFangKeMu.Text.IndexOf(" ") <= 0 Then
            Exit Sub
        End If
        If Me.JieFangKeMu.ReadOnly = False Then
            'Read(Me.JieFangKeMu.Text.Substring(Me.JieFangKeMu.Text.IndexOf(" "), Me.JieFangKeMu.Text.Length - Me.JieFangKeMu.Text.IndexOf(" ")))
        End If

        '20200827升级到可以显示图片和视频
        Dim sKMDMPic As String = FKF.FKF.getKMDMfromQM(Me.JieFangKeMu.Text)
        '确定是否有图片
        Dim ds As DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdb.bExsit("SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductPic]') AND type in (N'U')") Then
            ds = mdb.Reader("select * from ProductPic where kmdm='" & sKMDMPic & "'")
            Do While ds.Tables(0).Rows.Count = 0 And sKMDMPic.Length > 7
                ds.Clear()
                Dim sSJKMDPic As String
                sSJKMDPic = FKF.FKF.getSJKM(sKMDMPic)
                ds = mdb.Reader("select * from ProductPic where kmdm='" & sSJKMDPic & "'")
                sKMDMPic = sSJKMDPic
            Loop

            If ds.Tables(0).Rows.Count > 0 Then
                '设置图片框位
                Me.flpPic.Show()
                'Me.flpPic.Location = New System.Drawing.Point(50, Me.Height - 300)

                If IO.Directory.Exists(My.Application.Info.DirectoryPath & "\ProductPic") Then
                    Try
                        Dim iPic As Integer
                        If ds.Tables(0).Rows(0).Item("ZT1").ToString = "" Then
                            iPic = 0
                            Me.picZT1.Image = Nothing
                            Me.picZT1.Hide()
                            ' Exit Sub
                        Else
                            iPic = 1
                            Me.picZT1.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT1").ToString)
                            Me.picZT1.Show()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT2").ToString <> "" Then
                            iPic = 2
                            Me.picZT2.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT2").ToString)
                            Me.picZT2.Show()
                        Else
                            Me.picZT2.Image = Nothing
                            Me.picZT2.Hide()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT3").ToString <> "" Then
                            iPic = 3
                            Me.picZT3.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT3").ToString)
                            Me.picZT3.Show()
                        Else
                            Me.picZT3.Image = Nothing
                            Me.picZT3.Hide()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT4").ToString <> "" Then
                            iPic = 4
                            Me.picZT4.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT4").ToString)
                            Me.picZT4.Show()
                        Else
                            Me.picZT4.Image = Nothing
                            Me.picZT4.Hide()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT5").ToString <> "" Then
                            iPic = 5
                            Me.picZT5.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT5").ToString)
                            Me.picZT5.Show()
                        Else
                            Me.picZT5.Image = Nothing
                            Me.picZT5.Hide()
                        End If

                    Catch ex As Exception
                        ' MsgBox(ex.ToString)
                    End Try
                Else
                    IO.Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\ProductPic")
                End If
            Else
                Me.flpPic.Hide()
                Me.picZT1.Hide()
                Me.picZT2.Hide()
                Me.picZT3.Hide()
                Me.picZT4.Hide()
                Me.picZT5.Hide()
            End If
        Else
            Me.flpPic.Hide()
        End If

    End Sub

    Private Sub DaiFangKeMu_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DaiFangKeMu.TextChanged
        'If Me.DaiFangKeMu.Text = "" Then
        '    Exit Sub
        'End If
        'If Me.DaiFangKeMu.Text.IndexOf(" ") <= 0 Then
        '    Exit Sub
        'End If
        'If Me.DaiFangKeMu.ReadOnly = False Then
        '    Read(Me.DaiFangKeMu.Text.Substring(Me.DaiFangKeMu.Text.IndexOf(" "), Me.DaiFangKeMu.Text.Length - Me.DaiFangKeMu.Text.IndexOf(" ")))
        'End If

        'YETS(Me.DaiFangKeMu.Text, False, My.Settings.YETSD)
    End Sub


    Private Sub DaiFangKeMu_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles DaiFangKeMu.Validated
        YETS(Me.DaiFangKeMu.Text, False, My.Settings.YETSD)

        '20200812无论在何种工作状态，都必须刷新公式，否则可能出现先点修改记录和后点修改记录数据不一致的问题。
        'If iWorkState = 9 Then
        '    Exit Sub
        'End If
        Me.RefreshGongShi(CType(sender, System.Windows.Forms.Control).Name)

        If Me.DaiFangKeMu.Text = "" Then
            Exit Sub
        End If
        If Me.DaiFangKeMu.Text.IndexOf(" ") <= 0 Then
            Exit Sub
        End If
        If Me.DaiFangKeMu.ReadOnly = False Then
            'Read(Me.DaiFangKeMu.Text.Substring(Me.DaiFangKeMu.Text.IndexOf(" "), Me.DaiFangKeMu.Text.Length - Me.DaiFangKeMu.Text.IndexOf(" ")))
        End If

        '20200827升级到可以显示图片和视频
        Dim sKMDMPic As String = FKF.FKF.getKMDMfromQM(Me.DaiFangKeMu.Text)
        '确定是否有图片
        Dim ds As DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdb.bExsit("SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductPic]') AND type in (N'U')") Then
            ds = mdb.Reader("select * from ProductPic where kmdm='" & sKMDMPic & "'")
            Do While ds.Tables(0).Rows.Count = 0 And sKMDMPic.Length > 7
                ds.Clear()
                Dim sSJKMDPic As String
                sSJKMDPic = FKF.FKF.getSJKM(sKMDMPic)
                ds = mdb.Reader("select * from ProductPic where kmdm='" & sSJKMDPic & "'")
                sKMDMPic = sSJKMDPic
            Loop

            If ds.Tables(0).Rows.Count > 0 Then
                '设置图片框位
                Me.flpPic.Show()
                'Me.flpPic.Location = New System.Drawing.Point(50, Me.Height - 300)

                If IO.Directory.Exists(My.Application.Info.DirectoryPath & "\ProductPic") Then
                    Try
                        Dim iPic As Integer
                        If ds.Tables(0).Rows(0).Item("ZT1").ToString = "" Then
                            iPic = 0
                            Me.picZT1.Image = Nothing
                            Me.picZT1.Hide()
                            ' Exit Sub
                        Else
                            iPic = 1
                            Me.picZT1.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT1").ToString)
                            Me.picZT1.Show()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT2").ToString <> "" Then
                            iPic = 2
                            Me.picZT2.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT2").ToString)
                            Me.picZT2.Show()
                        Else
                            Me.picZT2.Image = Nothing
                            Me.picZT2.Hide()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT3").ToString <> "" Then
                            iPic = 3
                            Me.picZT3.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT3").ToString)
                            Me.picZT3.Show()
                        Else
                            Me.picZT3.Image = Nothing
                            Me.picZT3.Hide()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT4").ToString <> "" Then
                            iPic = 4
                            Me.picZT4.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT4").ToString)
                            Me.picZT4.Show()
                        Else
                            Me.picZT4.Image = Nothing
                            Me.picZT4.Hide()
                        End If
                        If ds.Tables(0).Rows(0).Item("ZT5").ToString <> "" Then
                            iPic = 5
                            Me.picZT5.Image = Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\ProductPic\" & ds.Tables(0).Rows(0).Item("ZT5").ToString)
                            Me.picZT5.Show()
                        Else
                            Me.picZT5.Image = Nothing
                            Me.picZT5.Hide()
                        End If

                    Catch ex As Exception
                        'MsgBox(ex.ToString)
                    End Try
                Else
                    IO.Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\ProductPic")
                End If
            Else
                Me.flpPic.Hide()
                Me.picZT1.Hide()
                Me.picZT2.Hide()
                Me.picZT3.Hide()
                Me.picZT4.Hide()
                Me.picZT5.Hide()
            End If
        Else
            Me.flpPic.Hide()

        End If

    End Sub

    Private Function getKMTJ(ByVal JorD As Boolean) As String
        Dim sTJ As String = ""
        Dim i As Integer

        If JorD Then



            If JieFangZhuKemu.FKShuXing.Zhi = "" Then
            Else
                Dim sMRTJ As String() '默认条件,默认条件可以为多个，用‘，’分割，只支持科目代码
                sMRTJ = Split(JieFangZhuKemu.FKShuXing.Zhi.Replace(" ", ""), ",")
                For i = 0 To sMRTJ.Length - 1
                    If sTJ = "" Then
                        sTJ = sTJ & " KMDM like '" & sMRTJ(i) & "%'"
                    Else
                        sTJ = sTJ & "  or KMDM like '" & sMRTJ(i) & "%' "
                    End If
                Next
            End If

            If JieFangZhuKemu.Text = "" Then
                If sTJ = "" Then
                    sTJ = "*"
                End If
            Else
                If JieFangZhuKemu.Text.IndexOf(" ") > 0 Then
                    sTJ = " KMDM like '" & FKF.FKF.getKMDMfromQM(JieFangZhuKemu.Text) & "%'"
                End If
            End If
        Else
            If DaiFangZhuKeMu.FKShuXing.Zhi = "" Then
            Else
                Dim sMRTJ As String() '默认条件,默认条件可以为多个，用‘，’分割，只支持科目代码
                sMRTJ = Split(DaiFangZhuKeMu.FKShuXing.Zhi.Replace(" ", ""), ",")
                For i = 0 To sMRTJ.Length - 1
                    If sTJ = "" Then
                        sTJ = sTJ & " KMDM like '" & sMRTJ(i) & "%'"
                    Else
                        sTJ = sTJ & "  or KMDM like '" & sMRTJ(i) & "%' "
                    End If
                Next
            End If

            If DaiFangZhuKeMu.Text = "" Then
                If sTJ = "" Then
                    sTJ = "*"
                End If
            Else
                If DaiFangZhuKeMu.Text.IndexOf(" ") > 0 Then
                    sTJ = " KMDM like '" & FKF.FKF.getKMDMfromQM(DaiFangZhuKeMu.Text) & "%'"
                End If
            End If
        End If
        Return sTJ
    End Function

    Private Sub JieFangKeMu_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles JieFangKeMu.Validating
        Dim sTJ As String = ""
        sTJ = "(" & getKMTJ(True) & ")"
        Dim dlg As FKKM.FKXKM
        If sTJ = "(*)" Then
            dlg = New FKKM.FKXKM("1=1", True, False)
        Else
            dlg = New FKKM.FKXKM(sTJ, True, False)
        End If

        With CType(sender, Freedom.FKTextBox)
            If .Text = "" Then
                Exit Sub
            End If

            Dim sDM As String = Me.getKMDMFromKMQC(.Text)

            'Dim I As Integer
            'I = .Text.IndexOf(" ")
            'If I <= 0 Then
            '    sDM = .Text
            'Else
            '    sDM = .Text.Substring(0, I)
            'End If
            dlg.sKey = FKF.FKF.getKMDMfromQM(.Text)
            .Text = FKF.FKF.getKMMC(" KMDM='" & sDM & "'")

            If .Text = "" Then
                e.Cancel = True

                dlg.ShowDialog()
                Me.Refresh()

                KM1 = dlg.myKM
                If KM1.sKMDM = "" Then
                    .Text = ""
                    Exit Sub
                Else
                    .Text = KM1.sKMDM & "   " & KM1.sKMMC
                    My.Computer.Keyboard.SendKeys("{Tab}")
                End If
            End If

            KM1.sKMDM = Me.getKMDMFromKMQC(.Text)
            'I = .Text.IndexOf(" ")
            'If I <= 0 Then
            '    KM1.sKMDM = ""
            'Else
            '    KM1.sKMDM = .Text.Substring(0, I)
            'End If

            '判断是否为末级科目
            If FKF.FKF.isMJKM(KM1.sKMDM) Then
            Else
                '.Text = ""
                e.Cancel = True

                dlg.ShowDialog()
                Me.Refresh()

                KM1 = dlg.myKM
                If KM1.sKMDM = "" Then
                    .Text = ""
                    Exit Sub
                Else
                    .Text = KM1.sKMDM & "   " & KM1.sKMMC
                    My.Computer.Keyboard.SendKeys("{Tab}")
                End If
                'Exit Sub
                'JieFangKeMu_Validating(sender, e)
            End If

            '判断是否是主科目的下级科目
            'If IsNothing(KM1.sKMDM) Then
            '    Exit Sub
            'End If
            If JieFangZhuKemu.Text = "" Then
            Else
                If KM1.sKMDM.StartsWith(FKF.FKF.getKMDMfromQM(JieFangZhuKemu.Text)) Then
                Else
                    e.Cancel = True
                    '.Text = ""

                    dlg.ShowDialog()
                    Me.Refresh()

                    KM1 = dlg.myKM
                    If KM1.sKMDM = "" Then
                        .Text = ""
                        Exit Sub
                    Else
                        .Text = KM1.sKMDM & "   " & KM1.sKMMC
                        My.Computer.Keyboard.SendKeys("{Tab}")
                    End If
                    'Exit Sub
                    'JieFangKeMu_Validating(sender, e)
                End If
            End If


            If Me.DanWei.FKShuXing.Zhi = "借方单位" Then
                If .Text = "" Then
                    Me.DanWei.Text = ""
                Else
                    Me.DanWei.Text = V_KM(KM1.sKMDM & ",SLDW,0").Tables(0).Rows(0).Item(0)
                End If
            ElseIf Me.DanWei.FKShuXing.Zhi = "贷方单位" Then

            Else
                Me.DanWei.Text = Me.DanWei.FKShuXing.Zhi
            End If
        End With

    End Sub

    Private Function getKMDMFromKMQC(ByVal sKMQM As String) As String
        Dim sKMDM As String = ""
        Dim I As Integer
        I = sKMQM.IndexOf(" ")
        If I <= 0 Then
            sKMDM = sKMQM
        Else
            sKMDM = sKMQM.Substring(0, I)
        End If

        Return sKMDM
    End Function

    Private Sub DaiFangKeMu_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DaiFangKeMu.Validating
        Dim sTJ As String = ""
        sTJ = "(" & getKMTJ(False) & ")"

        Dim dlg As FKKM.FKXKM
        If sTJ = "(*)" Then
            dlg = New FKKM.FKXKM("1=1", True, False)
        Else
            dlg = New FKKM.FKXKM(sTJ, True, False)
        End If

        With CType(sender, Freedom.FKTextBox)
            If .Text = "" Then
                Exit Sub
            End If

            Dim sDM As String = Me.getKMDMFromKMQC(.Text)
            'Dim I As Integer
            'I = .Text.IndexOf(" ")
            'If I <= 0 Then
            '    sDM = .Text
            'Else
            '    sDM = .Text.Substring(0, I)
            'End If
            dlg.sKey = FKF.FKF.getKMDMfromQM(.Text)
            .Text = FKF.FKF.getKMMC(" KMDM='" & sDM & "'")

            If .Text = "" Then
                e.Cancel = True

                dlg.ShowDialog()
                Me.Refresh()

                KM2 = dlg.myKM
                If KM2.sKMDM = "" Then
                    .Text = ""
                    Exit Sub
                Else
                    .Text = KM2.sKMDM & "   " & KM2.sKMMC
                    My.Computer.Keyboard.SendKeys("{Tab}")
                End If
            End If
            KM2.sKMDM = Me.getKMDMFromKMQC(.Text)
            'I = .Text.IndexOf(" ")
            'If I <= 0 Then
            '    KM2.sKMDM = ""
            'Else
            '    KM2.sKMDM = .Text.Substring(0, I)
            'End If

            '判断是否为末级科目
            If FKF.FKF.isMJKM(KM2.sKMDM) Then
            Else
                '.Text = ""
                e.Cancel = True

                dlg.ShowDialog()
                Me.Refresh()

                KM2 = dlg.myKM
                If KM2.sKMDM = "" Then
                    .Text = ""
                    Exit Sub
                Else
                    .Text = KM2.sKMDM & "   " & KM2.sKMMC
                    My.Computer.Keyboard.SendKeys("{Tab}")
                End If
            End If

            '判断是否是主科目的下级科目
            If IsNothing(KM2.sKMDM) Then
                Exit Sub
            End If
            If DaiFangZhuKeMu.Text = "" Then
            Else
                If KM2.sKMDM.StartsWith(FKF.FKF.getKMDMfromQM(DaiFangZhuKeMu.Text)) Then
                Else
                    '.Text = ""
                    e.Cancel = True

                    dlg.ShowDialog()
                    Me.Refresh()

                    KM2 = dlg.myKM
                    If KM2.sKMDM = "" Then
                        .Text = ""
                        Exit Sub
                    Else
                        .Text = KM2.sKMDM & "   " & KM2.sKMMC
                        My.Computer.Keyboard.SendKeys("{Tab}")
                    End If
                End If
            End If

            If Me.DanWei.FKShuXing.Zhi = "借方单位" Then

            ElseIf Me.DanWei.FKShuXing.Zhi = "贷方单位" Then
                If .Text = "" Then
                    Me.DanWei.Text = ""
                Else
                    Me.DanWei.Text = V_KM(KM2.sKMDM & ",SLDW,0").Tables(0).Rows(0).Item(0)
                End If
            Else
                Me.DanWei.Text = Me.DanWei.FKShuXing.Zhi
            End If
        End With

    End Sub

    Private Sub RiQi_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles RiQi.Enter
        Me.RiQi.MinDate = "#" & FKG.myselfG.NianFen & "-" & FKG.myselfG.YueFen & "-1#"
        Me.RiQi.MaxDate = Me.RiQi.MinDate.AddMonths(1).AddDays(-1)
    End Sub


    Private Sub FillGB()
        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.ReaderOpen("select * from JiBenCaoZuo where A_ID ='" & iJiBenID & "'")
        If IsNothing(ds) Then
            Exit Sub
        End If

        Dim obj As System.Windows.Forms.Control
        For Each obj In Me.gbSelect.Controls
            Select Case obj.GetType.Name
                Case "FKLable"
                    If obj.Name = "DanWei" Then
                        CType(obj, Freedom.FKLable).Text = ds.Tables(0).Rows(0).Item(obj.Name)
                    End If
                    '需要处理单位()
                Case "FKTextBox"

                    With CType(obj, Freedom.FKTextBox)
                        .Text = ds.Tables(0).Rows(0).Item(obj.Name)
                        '处理一下小数位数。
                        If IsNumeric(.Text) Then
                            .Text = Math.Round(CType(.Text, Double), 3)
                        End If
                    End With

                Case "FKComboBox"
                    If obj.Name = "CangKu" Then
                        Try
                            With CType(obj, Freedom.FKComboBox)
                                CangKuID.Text = ds.Tables(0).Rows(0).Item(obj.Name)
                                Me.CangKu.SelectedIndex = Me.CangKuID.SelectedIndex
                            End With

                        Catch ex As Exception

                        End Try
                    Else
                        With CType(obj, Freedom.FKComboBox)
                            .Text = ds.Tables(0).Rows(0).Item(obj.Name)
                        End With
                    End If

                Case "FKDateTimePicker"

                    With CType(obj, Freedom.FKDateTimePicker)
                        .Text = ds.Tables(0).Rows(0).Item(obj.Name)
                    End With
                Case "PictureBox"

                Case Else
                    'MsgBox("意外的控件类型")
            End Select
        Next
        sBianHao = Me.BianHao.Text
    End Sub

    Private Sub SetGJLocation() '设置窗体的各种初始状态
        'Me.tsc.TopToolStripPanel.Controls.Clear()
        'Me.tsc.RightToolStripPanel.Controls.Clear()
        'Me.tsc.LeftToolStripPanel.Controls.Clear()
        'Me.tsc.BottomToolStripPanel.Controls.Clear()
        
        Dim myP As System.Drawing.Point
        myP = My.Settings.FormLocation
        If myP.X < 5 Or myP.X > My.Computer.Screen.Bounds.Right - 15 Then
            myP.X = 5
        End If
        If myP.Y < 5 Or myP.Y > My.Computer.Screen.Bounds.Bottom - 35 Then
            myP.Y = 5
        End If

        Me.Location = myP
        
        Me.Size = My.Settings.FormSize

        Select Case My.Settings.GJ1P
            Case "Top"
                Me.tsc.TopToolStripPanel.Join(GJ1, My.Settings.GJ1)
            Case "Right"
                Me.tsc.RightToolStripPanel.Join(GJ1, My.Settings.GJ1)
            Case "Left"
                Me.tsc.LeftToolStripPanel.Join(GJ1, My.Settings.GJ1)
            Case "Bottom"
                Me.tsc.BottomToolStripPanel.Join(GJ1, My.Settings.GJ1)
        End Select


        Select Case My.Settings.GJ2P
            Case "Top"
                Me.tsc.TopToolStripPanel.Join(GJ2, My.Settings.GJ2)
            Case "Right"
                Me.tsc.RightToolStripPanel.Join(GJ2, My.Settings.GJ2)
            Case "Left"
                Me.tsc.LeftToolStripPanel.Join(GJ2, My.Settings.GJ2)
            Case "Bottom"
                Me.tsc.BottomToolStripPanel.Join(GJ2, My.Settings.GJ2)
        End Select

        Select Case My.Settings.GJ3P
            Case "Top"
                Me.tsc.TopToolStripPanel.Join(GJ3, My.Settings.GJ3)
            Case "Right"
                Me.tsc.RightToolStripPanel.Join(GJ3, My.Settings.GJ3)
            Case "Left"
                Me.tsc.LeftToolStripPanel.Join(GJ3, My.Settings.GJ3)
            Case "Bottom"
                Me.tsc.BottomToolStripPanel.Join(GJ3, My.Settings.GJ3)
        End Select

        Select Case My.Settings.GJ4P
            Case "Top"
                Me.tsc.TopToolStripPanel.Join(GJ4, My.Settings.GJ4)
            Case "Right"
                Me.tsc.RightToolStripPanel.Join(GJ4, My.Settings.GJ4)
            Case "Left"
                Me.tsc.LeftToolStripPanel.Join(GJ4, My.Settings.GJ4)
            Case "Bottom"
                Me.tsc.BottomToolStripPanel.Join(GJ4, My.Settings.GJ4)
        End Select

    End Sub

    Private Sub FKGN_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        My.Settings.FormLocation = Me.Location
        My.Settings.FormSize = Me.Size
        Dim ts As System.Windows.Forms.ToolStrip
        For Each ts In Me.tsc.TopToolStripPanel.Controls
            Select Case ts.Name
                Case "GJ1"
                    My.Settings.GJ1P = "Top"
                Case "GJ2"
                    My.Settings.GJ2P = "Top"
                Case "GJ3"
                    My.Settings.GJ3P = "Top"
                Case "GJ4"
                    My.Settings.GJ4P = "Top"
            End Select
        Next

        For Each ts In Me.tsc.RightToolStripPanel.Controls
            Select Case ts.Name
                Case "GJ1"
                    My.Settings.GJ1P = "Right"
                Case "GJ2"
                    My.Settings.GJ2P = "Right"
                Case "GJ3"
                    My.Settings.GJ3P = "Right"
                Case "GJ4"
                    My.Settings.GJ4P = "Right"
            End Select
        Next

        For Each ts In Me.tsc.LeftToolStripPanel.Controls
            Select Case ts.Name
                Case "GJ1"
                    My.Settings.GJ1P = "Left"
                Case "GJ2"
                    My.Settings.GJ2P = "Left"
                Case "GJ3"
                    My.Settings.GJ3P = "Left"
                Case "GJ4"
                    My.Settings.GJ4P = "Left"
            End Select
        Next

        For Each ts In Me.tsc.BottomToolStripPanel.Controls
            Select Case ts.Name
                Case "GJ1"
                    My.Settings.GJ1P = "Bottom"
                Case "GJ2"
                    My.Settings.GJ2P = "Bottom"
                Case "GJ3"
                    My.Settings.GJ3P = "Bottom"
                Case "GJ4"
                    My.Settings.GJ4P = "Bottom"
            End Select
        Next

        My.Settings.GJ1 = Me.GJ1.Location
        My.Settings.GJ2 = Me.GJ2.Location
        My.Settings.GJ3 = Me.GJ3.Location
        My.Settings.GJ4 = Me.GJ4.Location
        My.Settings.Save()
        'Me.Parent.Show()
        If Me.DialogResult = Windows.Forms.DialogResult.Abort Then
        Else
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
    End Sub

    Private Sub dgvKuCun_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvKuCun.CellEnter, dgvGongZi.CellEnter, dgvPZ.CellEnter
        If CType(sender, System.Windows.Forms.DataGridView).Name = "dgvPZ" Then
            If iPZSCFS = 1 Then
                'Exit Sub 20170315测试取消此限制，试图对凭证表采取和库存表同样的处理方式。
            End If
        End If
        If CType(sender, System.Windows.Forms.DataGridView).RowCount = 0 Then
            iJiBenID = -1
            Me.新增记录_Click(sender, Nothing)
            Exit Sub
        End If
        If CType(sender, System.Windows.Forms.DataGridView).CurrentRow.Index = CType(sender, System.Windows.Forms.DataGridView).Rows.GetLastRow(Windows.Forms.DataGridViewElementStates.None) Then
            Exit Sub
        End If

        CType(sender, System.Windows.Forms.DataGridView).CurrentRow.Selected = True

        If iJiBenID = CType(sender, System.Windows.Forms.DataGridView).CurrentRow.Cells.Item(0).Value Then

        Else
            iJiBenID = CType(sender, System.Windows.Forms.DataGridView).CurrentRow.Cells.Item(0).Value
        End If
        FillGB()

        If Me.JieFangZhuKemu.Text = "" Then
        Else
            Me.JieFangZhuKemu.Text = FKF.FKF.getKMQM(" KMDM='" & Me.JieFangZhuKemu.Text & "'")
        End If
        If Me.DaiFangZhuKeMu.Text = "" Then
        Else
            Me.DaiFangZhuKeMu.Text = FKF.FKF.getKMQM(" KMDM='" & Me.DaiFangZhuKeMu.Text & "'")
        End If

        If Me.JieFangKeMu.Text = "" Then
        Else
            Me.JieFangKeMu.Text = FKF.FKF.getKMMC(" KMDM='" & Me.JieFangKeMu.Text & "'")
        End If
        If Me.DaiFangKeMu.Text = "" Then
        Else
            Me.DaiFangKeMu.Text = FKF.FKF.getKMMC(" KMDM='" & Me.DaiFangKeMu.Text & "'")
        End If

        iWorkState = 9
        WorkStateChange(9)
    End Sub

    Private Sub WorkStateChange(ByVal workstate As Integer)

        If Me.tssZhuangtai.Text <> "就绪" Then
            Exit Sub
        End If

        Select Case workstate
            Case 0
                Me.保存.Enabled = True
                'Me.新增单证.Enabled = True
                Me.新增记录.Enabled = False
                Me.删除记录.Enabled = False
                Me.修改记录.Enabled = False
            Case 1
                Me.保存.Enabled = True
                'Me.新增单证.Enabled = True
                Me.新增记录.Enabled = True
                Me.删除记录.Enabled = False
                Me.修改记录.Enabled = False
            Case 2
                Me.保存.Enabled = True
                'Me.新增单证.Enabled = True
                Me.新增记录.Enabled = True
                Me.删除记录.Enabled = False
                Me.修改记录.Enabled = False
            Case 3

            Case 9
                Me.保存.Enabled = False
                'Me.新增单证.Enabled = True
                Me.新增记录.Enabled = True
                Me.删除记录.Enabled = True
                Me.修改记录.Enabled = True
        End Select
    End Sub


    '实时余额提示
    Private Function YETS(ByVal sKMDM As String, ByVal bJorD As Boolean, ByVal iType As String) As Boolean
        If iType = "9" Then
            If bJorD Then
                Me.JFYE.Text = "余额提示已关闭"
            Else
                Me.DFYE.Text = "余额提示已关闭"
            End If
            Return True
        End If

        If sKMDM.IndexOf(" ") < 0 Then
            Exit Function
        End If

        If sKMDM.Substring(0, sKMDM.IndexOf(" ")) = "" Then
            Exit Function
        Else
            sKMDM = sKMDM.Substring(0, sKMDM.IndexOf(" "))
        End If

        'If bJorD Then
        '    If Me.JYE.Checked Then
        '        iType = 1
        '    Else

        '        iType = 2
        '    End If
        'Else
        '    If Me.DYE.Checked Then
        '        iType = 1
        '    Else
        '        iType = 2
        '    End If
        'End If


        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dNCYE As Decimal
        Dim dFSE As Decimal
        Dim sYE As String
        If iType = "1" Then '求的是结余余额
            Dim ds As DataSet

            If FKG.myselfG.dQiYongRiQi.Year < FKG.myselfG.NianFen Then
                ds = mdb.ReaderOpen("select isNUll(ncjfye-ncdfye,0) as ncye from KMYE where Nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'")
            Else
                Dim iYue As Integer = FKG.myselfG.dQiYongRiQi.Month
                If iYue = 1 Then
                    ds = mdb.ReaderOpen("select isNUll(ncjfye-ncdfye,0) as ncye from KMYE where Nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'")
                Else
                    ds = mdb.ReaderOpen("select isNUll(ncjfye-ncdfye,0) as ncye from KMYE where Nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'")
                    'ds = mdb.ReaderOpen("select isNUll(年初借方余额-年初贷方余额,0)+isnull([" & iYue - 1 & "月借方累计发生额]-[" & iYue - 1 & "月贷方累计发生额],0)-[" & iYue - 1 & "月贷方累计发生额]) as ncye from 科目余额表 where 年份=" & FKG.myselfG.NianFen & " and 科目代码 ='" & sKMDM & "'")
                End If
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                dNCYE = 0
            Else
                dNCYE = ds.Tables(0).Rows(0).Item(0)
            End If
            ds.Clear()
            ds = mdb.ReaderOpen("select isnull(sum(借方金额)-sum(贷方金额),0) as FSE from pz where 年份=" & FKG.myselfG.NianFen & " and 科目='" & sKMDM & "'")
            If ds.Tables(0).Rows.Count = 0 Then
                dFSE = 0
            Else
                dFSE = ds.Tables(0).Rows(0).Item(0)
            End If

            If (dNCYE + dFSE) >= 0 Then
                sYE = "借方 " & (dNCYE + dFSE).ToString
            Else
                sYE = "贷方 " & Decimal.Negate(dNCYE + dFSE).ToString
            End If
            If bJorD Then
                Me.JFYE.Text = sKMDM & "余额为:" & sYE
            Else
                Me.DFYE.Text = sKMDM & "余额为:" & sYE
            End If
        Else
            ' iType = 2 Then '求的是库存结余数量
            Dim sCaseNC As String = ""
            Dim sCaseNC1 As String = ""
            Dim sCaseFSE As String = ""
            Dim ds As DataSet

            Select Case iType
                Case "2"
                    'sCaseNC = ""
                    'sCaseNC1 = ""
                    'sCaseFSE = ""
                Case "3"
                    ds = mdb.Readeropen("select ID from CangKuInfo where ShuoMing='废品库'")
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sCaseNC = sCaseNC & " and CangKuID<>" & ds.Tables(0).Rows(i).Item(0)
                        sCaseNC1 = sCaseNC1 & " and 仓库编号<>" & ds.Tables(0).Rows(i).Item(0)
                        sCaseFSE = sCaseFSE & " and 仓库<>" & ds.Tables(0).Rows(i).Item(0)
                    Next
                Case "4"
                    ds = mdb.Readeropen("select ID from CangKuInfo where ShuoMing='外存库'")
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sCaseNC = sCaseNC & " and CangKuID<>" & ds.Tables(0).Rows(i).Item(0)
                        sCaseNC1 = sCaseNC1 & " and 仓库编号<>" & ds.Tables(0).Rows(i).Item(0)
                        sCaseFSE = sCaseFSE & " and 仓库<>" & ds.Tables(0).Rows(i).Item(0)
                    Next
                Case "5"
                    ds = mdb.Readeropen("select ID from CangKuInfo where ShuoMing='废品库' or ShuoMing='外存库'")
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sCaseNC = sCaseNC & " and CangKuID<>" & ds.Tables(0).Rows(i).Item(0)
                        sCaseNC1 = sCaseNC1 & " and 仓库编号<>" & ds.Tables(0).Rows(i).Item(0)
                        sCaseFSE = sCaseFSE & " and 仓库<>" & ds.Tables(0).Rows(i).Item(0)
                    Next
                Case Else

            End Select
            If FKG.myselfG.dQiYongRiQi.Year < FKG.myselfG.NianFen Then
                ds = mdb.ReaderOpen("select isNull(sum(ncjfsl)-sum(ncdfsl),0) as ncye from KcYE where Nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'" & sCaseNC)
            Else
                Dim iYue As Integer = FKG.myselfG.dQiYongRiQi.Month
                If iYue = 1 Then
                    ds = mdb.ReaderOpen("select isNull(sum(ncjfsl)-sum(ncdfsl),0) as ncye from KcYE where Nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'" & sCaseNC)
                Else
                    ds = mdb.ReaderOpen("select isNull(sum(ncjfsl)-sum(ncdfsl),0) as ncye from KcYE where Nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'" & sCaseNC)
                    'ds = mdb.ReaderOpen("select isNUll(sum(年初借方数量)-sum(年初贷方数量),0)+isnull(sum([" & iYue - 1 & "月借方累计发生数量])-sum([" & iYue - 1 & "月贷方累计发生数量]),0)-sum([" & iYue - 1 & "月贷方累计发生数量])) as ncye from 库存余额表 where 年份=" & FKG.myselfG.NianFen & " and 科目代码 ='" & sKMDM & "'" & sCaseNC1)
                End If
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                dNCYE = 0
            Else
                dNCYE = ds.Tables(0).Rows(0).Item(0)
            End If
            ds.Clear()

            ds = mdb.ReaderOpen("select isnull(sum(借方数量)-sum(贷方数量),0) as FSE from Kucun where 年份=" & FKG.myselfG.NianFen & " and 科目='" & sKMDM & "'" & sCaseFSE)
            If ds.Tables(0).Rows.Count = 0 Then
                dFSE = 0
            Else
                dFSE = ds.Tables(0).Rows(0).Item(0)
            End If
            If (dNCYE + dFSE) >= 0 Then
                sYE = "借方 " & Math.Round((dNCYE + dFSE), 2).ToString
            Else
                sYE = "贷方 " & Math.Round(Decimal.Negate(dNCYE + dFSE), 2).ToString
            End If
            If bJorD Then
                Me.JFYE.Text = sKMDM & "结余数为:" & sYE
            Else
                Me.DFYE.Text = sKMDM & "结余数为:" & sYE
            End If
        End If
    End Function


    Private Sub 预览_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 预览.Click
        iDanZhengType = getDanZhengType()
        If iDanZhengType = 0 Then
            MsgBox("请在设置中设定需要打印的单证类型！", MsgBoxStyle.Information, My.Application.Info.Title)
            Exit Sub
        End If
        Dim thP As New Threading.Thread(AddressOf PrintView)
        thP.Start()
    End Sub

    Dim iDanZhengType As Integer
    Private Sub PrintView()
        Try
            Select Case iDanZhengType
                Case 1, 2
                    'Dim obj As New FKPrn.Print(sBianHao, iGongnengID, iDanZhengType)
                    Dim obj As New FKPrn.Print(sBianHao, iGongnengID, iDanZhengType)
                    obj.PrintPreview()
                    obj = Nothing
                    System.GC.Collect()
                Case 3 'dayin pz
                    'Dim obj As New FKPrn.Print(sBianHao, iGongnengID, iDanZhengType)
                    Dim obj As New FKPrn.Print(sBianHao, iGongnengID, iDanZhengType)
                    obj.iNian = FKG.myselfG.NianFen
                    obj.iYue = FKG.myselfG.YueFen
                    'obj.iPZSCFS = Me.iPZSCFS 
                    obj.iPZSCFS = 0 '所有的打印均应按照编号进行


                    obj.PrintPreview()
                    obj = Nothing
                    System.GC.Collect()
            End Select
        Catch ex As Exception
            MsgBox("预览过程出现错误，请检查是否按照EXCEL或者WPS，请不要使用精简版EXCEL")
            MsgBox(ex.ToString)
        Finally
            System.GC.Collect()
        End Try
    End Sub

    Private Function getDanZhengType() As Integer
        'Dim i As Integer
        If Me.打印库存单证.Checked Then
            Return 1
        End If
        If Me.打印工资单证.Checked Then
            Return 2
        End If
        If Me.打印凭证.Checked Then
            Return 3
        End If

        Return 0
    End Function

    Private Sub PrintOut()
        Try
            Select Case iDanZhengType
                Case 1, 2
                    Dim obj As New FKPrn.Print(sBianHao, iGongnengID, iDanZhengType)
                    obj.Print()
                    obj = Nothing
                    System.GC.Collect()
                Case 3
                    Dim obj As New FKPrn.Print(sBianHao, iGongnengID, iDanZhengType)
                    obj.iNian = FKG.myselfG.NianFen
                    obj.iYue = FKG.myselfG.YueFen
                    '                obj.iPZSCFS = Me.iPZSCFS
                    obj.iPZSCFS = 0 '所有的打印均应按照编号进行

                    obj.Print()
                    obj = Nothing
                    System.GC.Collect()
            End Select
        Catch ex As Exception
            MsgBox("打印过程出现错误，原因一，未安装正确版本的EXCEL或者WPS。原因二，没有连接打印机或打印机驱动有问题。")
            MsgBox(ex.ToString)
        Finally
            System.GC.Collect()
        End Try

    End Sub


    Private Sub 打印_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 打印.Click
        iDanZhengType = getDanZhengType()
        If iDanZhengType = 0 Then
            MsgBox("请在设置中设定需要打印的单证类型！", MsgBoxStyle.Information, My.Application.Info.Title)
            Exit Sub
        End If
        Dim thP As New Threading.Thread(AddressOf PrintOut)
        thP.Start()
    End Sub


    Private Sub 打印库存单证_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 打印库存单证.Click
        If Me.打印库存单证.Checked = False Then
            'Me.打印库存单证.Checked = False
        Else
            Me.打印库存单证.Checked = True
            Me.打印工资单证.Checked = False
            Me.打印凭证.Checked = False
        End If
    End Sub

    Private Sub 打印工资单证_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 打印工资单证.Click
        If Me.打印工资单证.Checked = False Then
            'Me.打印工资单证.Checked = False
        Else
            Me.打印工资单证.Checked = True
            Me.打印库存单证.Checked = False
            Me.打印凭证.Checked = False
        End If
    End Sub

    Private Sub 打印凭证_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 打印凭证.Click
        If Me.打印凭证.Checked = False Then
            'Me.打印凭证.Checked = False
        Else
            Me.打印凭证.Checked = True
            Me.打印库存单证.Checked = False
            Me.打印工资单证.Checked = False
        End If
    End Sub

    Private Sub 工具栏显示文字_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 工具栏显示文字.Click

        If Me.工具栏显示文字.Checked Then
            '不显示工具栏文字
            Dim i As Integer
            For i = 0 To Me.GJ1.Items.Count - 1
                Me.GJ1.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.Image
            Next
            For i = 0 To Me.GJ2.Items.Count - 1
                Me.GJ2.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.Image
            Next
            For i = 0 To Me.GJ3.Items.Count - 1
                Me.GJ3.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.Image
            Next
            For i = 0 To Me.GJ4.Items.Count - 1
                Me.GJ4.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.Image
            Next

            Me.工具栏显示文字.Checked = False
        Else
            '显示文字
            Dim i As Integer
            For i = 0 To Me.GJ1.Items.Count - 1
                Me.GJ1.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.ImageAndText
            Next
            For i = 0 To Me.GJ2.Items.Count - 1
                Me.GJ2.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.ImageAndText
            Next
            For i = 0 To Me.GJ3.Items.Count - 1
                Me.GJ3.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.ImageAndText
            Next
            For i = 0 To Me.GJ4.Items.Count - 1
                Me.GJ4.Items(i).DisplayStyle = Windows.Forms.ToolStripItemDisplayStyle.ImageAndText
            Next

            Me.工具栏显示文字.Checked = True
        End If
    End Sub

    Private Function StringToFont(ByVal sFont As String)
        Dim fFont As Drawing.Font
        Dim sF() As String
        sF = Split(sFont, ",")
        If sF(0) = "" Then
            fFont = New Drawing.Font("宋体", 9, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point)
        Else
            fFont = New Drawing.Font(sF(0), sF(1), sF(2), Drawing.GraphicsUnit.Point)
        End If

        Return fFont
    End Function

    Private Sub FillCangKu()
        If Me.CangKu.FKShuXing.QiYong Then
            'Me.CangKu.Items.Add("原料库")
            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim ds As DataSet
            If Me.CangKu.FKShuXing.Zhi.Replace(" ", "") = "" Then
                ds = mdb.Readeropen("select ID,Name from cangkuinfo order by Name")
            Else
                Dim sCangKu() As String
                sCangKu = Split(Me.CangKu.FKShuXing.Zhi, ",")
                Dim n As Integer
                Dim sCaseCK As String = ""
                For n = 0 To sCangKu.Length - 1
                    sCaseCK = sCaseCK & " or  Name='" & sCangKu(n) & "'"
                Next
                sCaseCK = sCaseCK.Remove(0, 4)

                ds = mdb.Readeropen("select ID,Name from CangKuInfo where " & sCaseCK & " order by name")

                Me.CangKu.FKShuXing.Zhi = sCangKu(0)
            End If

            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Me.CangKu.Items.Add(ds.Tables(0).Rows(i).Item(1).ToString)
                Me.CangKuID.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
            'mdb.DataBind(Me.CangKu, "select Name from CangKuInfo")
        Else
            Me.CangKuID.Items.Add(0)
            Me.CangKuID.Text = 0
        End If
    End Sub

    Private Sub CangKu_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CangKu.SelectedIndexChanged
        Me.CangKuID.SelectedIndex = Me.CangKu.SelectedIndex
        'MsgBox(Me.CangKuID.Items(Me.CangKu.SelectedIndex).ToString)
    End Sub


    'Private Sub FKGN_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    '    If e.Control Then
    '        If e.KeyData = Windows.Forms.Keys.Up Then
    '            Me.上单_Click(sender, e)
    '        End If

    '        If e.KeyData = Windows.Forms.Keys.Down Then
    '            Me.下单_Click(sender, e)
    '        End If

    '        If e.KeyData = Windows.Forms.Keys.Home Then
    '            Me.首单_Click(sender, e)
    '        End If

    '        If e.KeyData = Windows.Forms.Keys.End Then
    '            Me.尾单_Click(sender, e)
    '        End If
    '    End If
    'End Sub

    Private Sub 包括所有仓库贷方_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 包括所有仓库贷方.Click
        If Me.包括所有仓库贷方.Checked Then
        Else
            UpdateYETS(False, "2")
            'setYETS()
            YETS(Me.DaiFangKeMu.Text, False, "2")
        End If
    End Sub

    Private Sub 不包括废品库贷方_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 不包括废品库贷方.Click
        If Me.不包括废品库贷方.Checked Then
        Else
            UpdateYETS(False, "3")
            'setYETS()
            YETS(Me.DaiFangKeMu.Text, False, "3")
        End If
    End Sub

    Private Sub 不包括外存库贷方_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 不包括外存库贷方.Click
        If Me.不包括外存库.Checked Then
        Else
            UpdateYETS(False, "4")
            'setYETS()
            YETS(Me.DaiFangKeMu.Text, False, "4")
        End If

    End Sub

    Private Sub 不包括废品库和外存库贷方_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 不包括废品库和外存库贷方.Click
        If Me.不包括废品库和外存库贷方.Checked Then
        Else
            UpdateYETS(False, "5")
            'setYETS()
            YETS(Me.DaiFangKeMu.Text, False, "5")
        End If

    End Sub

    Private Sub 包括所有仓库_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 包括所有仓库.Click
        If Me.包括所有仓库.Checked Then
        Else
            UpdateYETS(True, "2")
            'setYETS()
            YETS(Me.JieFangKeMu.Text, True, "2")
        End If

    End Sub

    Private Sub 不包括废品库_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 不包括废品库.Click
        If Me.不包括废品库.Checked Then
        Else
            UpdateYETS(True, "3")
            'setYETS()
            YETS(Me.JieFangKeMu.Text, True, "3")
        End If
    End Sub

    Private Sub 不包括外存库_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 不包括外存库.Click
        If Me.不包括外存库.Checked Then
        Else
            UpdateYETS(True, "4")
            'setYETS()
            YETS(Me.JieFangKeMu.Text, True, "4")
        End If
    End Sub

    Private Sub 不包括废品库和外存库_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 不包括废品库和外存库.Click
        If Me.不包括废品库和外存库.Checked Then
        Else
            UpdateYETS(True, "5")
            'setYETS()
            YETS(Me.JieFangKeMu.Text, True, "5")
        End If
    End Sub

    Private Sub JYE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles JYE.Click
        If Me.JYE.Checked Then
        Else
            UpdateYETS(True, "1")
            'setYETS()
            YETS(Me.JieFangKeMu.Text, True, "1")
        End If
    End Sub

    Private Sub DYE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DYE.Click
        If DYE.Checked Then
        Else
            UpdateYETS(False, "1")
            'setYETS()
            YETS(Me.DaiFangKeMu.Text, False, "1")
        End If
    End Sub

    Private Sub JClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JClose.Click
        If Me.JClose.Checked Then
        Else
            UpdateYETS(True, "9")
            'setYETS()
            YETS(Me.JieFangKeMu.Text, True, "9")
        End If
    End Sub

    Private Sub DClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DClose.Click
        If Me.DClose.Checked Then
        Else
            UpdateYETS(False, "9")
            'setYETS()
            YETS(Me.DaiFangKeMu.Text, False, "9")
        End If

    End Sub

    Private Sub UpdateYETS(ByVal bJorD As Boolean, ByVal sYETSType As Integer)
        Dim mdbW As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If bJorD Then
            mdbW.Write("update GongNengShu set JFYETS='" & sYETSType & "' WHERE id =" & iGongnengID)
            My.Settings.YETSJ = sYETSType
            My.Settings.Save()

            If My.Settings.YETSJ = "1" Then
                Me.JYE.Checked = True
                Me.JFSL.Checked = False
                Me.JClose.Checked = False
            ElseIf My.Settings.YETSJ = "9" Then
                Me.JYE.Checked = False
                Me.JFSL.Checked = False
                Me.JClose.Checked = True
            Else
                Me.JYE.Checked = False
                Me.JFSL.Checked = True
                Me.JClose.Checked = False
                Select Case My.Settings.YETSJ
                    Case "2"
                        Me.包括所有仓库.Checked = True
                        Me.不包括废品库.Checked = False
                        Me.不包括外存库.Checked = False
                        Me.不包括废品库和外存库.Checked = False
                    Case "3"
                        Me.包括所有仓库.Checked = False
                        Me.不包括废品库.Checked = True
                        Me.不包括外存库.Checked = False
                        Me.不包括废品库和外存库.Checked = False
                    Case "4"
                        Me.包括所有仓库.Checked = False
                        Me.不包括废品库.Checked = False
                        Me.不包括外存库.Checked = True
                        Me.不包括废品库和外存库.Checked = False
                    Case "5"
                        Me.包括所有仓库.Checked = False
                        Me.不包括废品库.Checked = False
                        Me.不包括外存库.Checked = False
                        Me.不包括废品库和外存库.Checked = True
                    Case Else

                End Select
            End If
        Else
            mdbW.Write("update GongNengShu set DFYETS='" & sYETSType & "' WHERE id =" & iGongnengID)
            My.Settings.YETSD = sYETSType
            My.Settings.Save()
            If My.Settings.YETSD = "1" Then
                Me.DYE.Checked = True
                Me.DFSL.Checked = False
                Me.DClose.Checked = False
            ElseIf My.Settings.YETSD = "9" Then
                Me.DYE.Checked = False
                Me.DFSL.Checked = False
                Me.DClose.Checked = True
            Else
                Me.DFSL.Checked = True
                Me.DYE.Checked = False
                Me.DClose.Checked = False
                Select Case My.Settings.YETSD
                    Case "2"
                        Me.包括所有仓库贷方.Checked = True
                        Me.不包括废品库贷方.Checked = False
                        Me.不包括外存库贷方.Checked = False
                        Me.不包括废品库和外存库贷方.Checked = False
                    Case "3"
                        Me.包括所有仓库贷方.Checked = False
                        Me.不包括废品库贷方.Checked = True
                        Me.不包括外存库贷方.Checked = False
                        Me.不包括废品库和外存库贷方.Checked = False
                    Case "4"
                        Me.包括所有仓库贷方.Checked = False
                        Me.不包括废品库贷方.Checked = False
                        Me.不包括外存库贷方.Checked = True
                        Me.不包括废品库和外存库贷方.Checked = False
                    Case "5"
                        Me.包括所有仓库贷方.Checked = False
                        Me.不包括废品库贷方.Checked = False
                        Me.不包括外存库贷方.Checked = False
                        Me.不包括废品库和外存库贷方.Checked = True
                End Select
            End If
        End If



    End Sub

    '20121028，取消语音提示功能，因为在Win7中出现错误，另外实际用途不大。
    '在Jiefangkemu.Valeted和Daifangkemu.Valeted中仍有两个Read功能被取消了。

    'Dim objspeech1 As New DotNetSpeech.SpVoice
    'Dim objspeech2 As New DotNetSpeech.SpVoice
    'Dim objspeech3 As New DotNetSpeech.SpVoice
    'Dim objspeech4 As New DotNetSpeech.SpVoice
    'Dim objspeech5 As New DotNetSpeech.SpVoice

    'Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork, BackgroundWorker2.DoWork, BackgroundWorker3.DoWork, BackgroundWorker4.DoWork, BackgroundWorker5.DoWork

    '    If My.Computer.Info.OSVersion >= "6" Then
    '        '对于win7以上的版本，不提供语音提示功能。
    '        Exit Sub
    '    End If

    '    Select Case iBGWorker
    '        Case 1
    '            objspeech1.Speak(e.Argument.ToString, DotNetSpeech.SpeechVoiceSpeakFlags.SVSFlagsAsync)
    '        Case 2
    '            objspeech2.Speak(e.Argument.ToString, DotNetSpeech.SpeechVoiceSpeakFlags.SVSFlagsAsync)
    '        Case 3
    '            objspeech3.Speak(e.Argument.ToString, DotNetSpeech.SpeechVoiceSpeakFlags.SVSFlagsAsync)
    '        Case 4
    '            objspeech4.Speak(e.Argument.ToString, DotNetSpeech.SpeechVoiceSpeakFlags.SVSFlagsAsync)
    '        Case 5
    '            objspeech5.Speak(e.Argument.ToString, DotNetSpeech.SpeechVoiceSpeakFlags.SVSFlagsAsync)
    '        Case Else
    '    End Select

    '    If iBGWorker = 5 Then
    '        iBGWorker = 1
    '    Else
    '        iBGWorker = iBGWorker + 1
    '    End If

    'End Sub
    ''    Dim objBGWorker(9) As System.ComponentModel.BackgroundWorker

    'Dim iBGWorker As Integer = 1
    ''Dim sBGW As String
    'Private Sub Read(ByVal sText As String)
    '    If My.Settings.YYTS = False Then
    '        Exit Sub
    '    End If
    '    'For iBGWorker = 0 To 5
    '    Try
    '        Select Case iBGWorker
    '            Case 1
    '                Me.BackgroundWorker5.CancelAsync()
    '                Me.BackgroundWorker1.RunWorkerAsync(sText)
    '            Case 2
    '                Me.BackgroundWorker1.CancelAsync()
    '                Me.BackgroundWorker2.RunWorkerAsync(sText)
    '            Case 3
    '                Me.BackgroundWorker2.CancelAsync()
    '                Me.BackgroundWorker3.RunWorkerAsync(sText)
    '            Case 4
    '                Me.BackgroundWorker3.CancelAsync()
    '                Me.BackgroundWorker4.RunWorkerAsync(sText)
    '            Case 5
    '                Me.BackgroundWorker4.CancelAsync()
    '                Me.BackgroundWorker5.RunWorkerAsync(sText)
    '            Case Else

    '        End Select
    '    Catch ex As Exception

    '    End Try
    '    'Next

    'End Sub

    'Private Sub Beiyong1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DanJia.KeyPress, JinE.KeyPress, ShuLiang.KeyPress, Beiyong1.KeyPress, BeiYong2.KeyPress, BeiYong3.KeyPress, BeiYong6.KeyPress, BeiYong7.KeyPress, BeiYong8.KeyPress, BeiYong9.KeyPress, BeiYong10.KeyPress, BeiYong11.KeyPress, BeiYong12.KeyPress, BeiYong13.KeyPress, BeiYong14.KeyPress, BeiYong15.KeyPress
    '    If CType(sender, Freedom.FKTextBox).ReadOnly = True Then
    '        Exit Sub
    '    End If
    '    Read(e.KeyChar.ToString)
    '    'objst.Start(e.KeyChar.ToString)

    'End Sub
    '需要解决同步朗读的问题
    '朗读中间间隔时间太长

    'Private Sub 启用语音提示_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 启用语音提示.Click
    '    If Me.启用语音提示.Checked Then
    '        Me.启用语音提示.Checked = False
    '        My.Settings.YYTS = False
    '        My.Settings.Save()

    '        Read("你选择了关闭语音提示功能!")
    '    Else
    '        Me.启用语音提示.Checked = True
    '        My.Settings.YYTS = True
    '        My.Settings.Save()

    '        Read("你选择了启用语音提示功能!")
    '    End If
    'End Sub

    Public Sub New(ByVal sHash As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        'Try
        '    Dim st As New System.Diagnostics.StackTrace
        '    Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(st.GetFrame(1).GetMethod.Name.ToString), sHash)
        '        Case True
        '        Case False
        '            Me.DialogResult = Windows.Forms.DialogResult.No
        '            Me.Close()
        '    End Select
        'Catch ex As Exception
        '    Me.DialogResult = Windows.Forms.DialogResult.No
        '    Me.Dispose()
        'End Try

    End Sub

    Private Function SignatureDeformatter(ByVal p_strKeyPublic As String, ByVal p_strHashbyteDeformatter As String, ByVal p_strDeformatterData As String) As Boolean
        Try
            Dim DeformatterData As Byte()
            Dim HashbyteDeformatter As Byte()

            HashbyteDeformatter = Convert.FromBase64String(p_strHashbyteDeformatter)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPublic)
            Dim RSADeformatter As New System.Security.Cryptography.RSAPKCS1SignatureDeformatter(RSA)
            '指定解密的时候HASH算法为MD5 
            RSADeformatter.SetHashAlgorithm("MD5")

            DeformatterData = Convert.FromBase64String(p_strDeformatterData)

            If RSADeformatter.VerifySignature(HashbyteDeformatter, DeformatterData) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
            'Throw ex
        End Try
    End Function

    Private Function GetHash(ByVal m_strSource As String) As String
        Try
            Dim strHashData As String
            '从字符串中取得Hash描述 
            Dim Buffer As Byte()
            Dim HashData As Byte()
            Dim MD5 As System.Security.Cryptography.HashAlgorithm = System.Security.Cryptography.HashAlgorithm.Create("MD5")
            Buffer = System.Text.Encoding.GetEncoding("GB2312").GetBytes(m_strSource)
            HashData = MD5.ComputeHash(Buffer)

            strHashData = Convert.ToBase64String(HashData)
            Return strHashData
        Catch ex As Exception
            Return ""
        End Try
    End Function

    '以下为公式计算，重新规划
    Private Function YunSuanFu(ByVal sText As String) As String
        Dim ysf As String = ""
        '增加自动保存函数，运算到此公式时自动保存当时数据，设计应用为扫描条码时使用
        If sText.StartsWith("V_AutoSave", StringComparison.CurrentCultureIgnoreCase) Then
            V_autosave(sText)
            '参数为空间名称，意思为当指定空间发生变化时触发自动保存，此公式可以使用单独的控件来保存和运行。
        End If
        '增加余额提示函数
        If sText.StartsWith("V_YETS", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_YETS"
        End If
        '增加可扩展公式函数
        If sText.StartsWith("V_FKGSHS", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_FKGSHS"
        End If
        '增加最近单价提示功能,格式为:V_Price(JieFangkemu,DaiFangKemu,kucunORGongZi,IgongNengID)
        If sText.StartsWith("V_PRICE", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_PRICE"
        End If
        '增加取子字符串函数,相当于SubString
        If sText.StartsWith("V_SubString", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_SubString"
        End If
        '增加取整函数，即舍去所有小数
        If sText.StartsWith("V_QZS", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_QZS"
        End If

        '增加四舍五入函数
        If sText.StartsWith("V_SSWR", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_SSWR"
        End If
        '以上两个函数用法为“ V_SSWR Jine ”，不要加括号和其他符号

        If sText.StartsWith("V_LianJie", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_LianJie"
        End If

        If sText.StartsWith("V_Sum", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_Sum"
        End If

        If sText.StartsWith("V_Jian", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_Jian"
        End If

        If sText.StartsWith("V_Cheng", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_Cheng"
        End If

        If sText.StartsWith("V_Chu", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_Chu"
        End If

        '增加逻辑判断函数V_FKIF，有5个参数，其中前三个共同组成判断条件，后两个为真值或假值时的值
        If sText.StartsWith("V_FKIF", StringComparison.CurrentCultureIgnoreCase) Then
            Return "V_FKIF"
        End If

        Return ""
    End Function

    Dim bAutoSave As Boolean = False
    Private Sub V_autosave(ByVal sOBJKongJian As String)
        '参数为空间名称，意思为当指定空间发生验证时触发自动保存，此公式可以使用单独的控件来保存和运行。
        bAutoSave = True
    End Sub

    Private Function isKongJian(ByVal sText As String) As Boolean
        If Me.gbSelect.Controls.IndexOfKey(sText) = -1 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function YunSuan(ByVal sText As String) As Object
        'sText = sText.Replace(" ", "")

        sText = sText.Trim()
        If sText = "" Then
            Return ""
        End If

        Dim sL As String
        Dim sYSF As String

        sYSF = YunSuanFu(sText)
        If sYSF = "" Then
            sL = sText
        Else
            '去掉运算符和括号，只取括号内的参数

            '20150420, 将SL前后的空格去掉，不能将参数内所有的空格都去掉。
            sL = sText.Substring(sYSF.Length + 1, sText.Length - (sYSF.Length + 2)).Trim()
        End If

        Try
            Select Case sYSF
                Case "V_FKIF"
                    Return V_FKIF(sL)
                Case "V_FKGSHS"
                    Return FKHanShu(sL)
                Case "V_PRICE"
                    Return getPrice(sL)
                Case "V_SubString"
                    Return Me.V_SubString(sL)
                Case "V_QZS"
                    Return Me.V_QZS(sL)
                    'Return Math.Truncate(CType(YunSuan(sR), Double))
                Case "V_SSWR"
                    Return Me.V_SSWR(sL)
                    'Return Math.Round(CType(YunSuan(sR), Double))
                Case "V_YETS"
                    Return Me.V_YETS(sL)
                Case "V_LianJie"
                    Return Me.V_LianJie(sL)
                Case "V_Sum"
                    Return Me.V_Sum(sL)
                Case "V_Jian"
                    Return Me.V_Jian(sL)
                Case "V_Cheng"
                    Return Me.V_Cheng(sL)
                Case "V_Chu"
                    Return Me.V_Chu(sL)
                Case ""
                    If isKongJian(sText) Then
                        Return Me.gbSelect.Controls.Item(sText).Text
                    Else
                        Return sText
                    End If
                Case Else
                    Return sText
            End Select
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Private Function getDouHaoIndex(ByVal stext As String) As Integer
        Dim iIndex As Integer = 0
        Dim iKouHaoNumber As Integer = 0
        For iIndex = 0 To stext.Length - 1
            Select Case stext.Substring(iIndex, 1)
                Case "("
                    iKouHaoNumber = iKouHaoNumber + 1
                Case ")"
                    iKouHaoNumber = iKouHaoNumber - 1
                Case ","
                    If iKouHaoNumber = 0 Then
                        Exit For
                    End If
            End Select
        Next
        Return iIndex
    End Function

    Private Function V_FKIF(ByVal sCanShu As String) As Object
        Dim objZhi As Object
        Dim sL, sYSF, sR, sTrue, sFalse As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sCanShu = sCanShu.Remove(0, sL.Length + 1)
            sYSF = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sCanShu = sCanShu.Remove(0, sYSF.Length + 1)
            sR = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sCanShu = sCanShu.Remove(0, sR.Length + 1)
            sTrue = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sCanShu = sCanShu.Remove(0, sTrue.Length + 1)
            sFalse = sCanShu

            Select Case sYSF.ToUpper
                Case "="
                    If YunSuan(sL) = YunSuan(sR) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case "<>"
                    If YunSuan(sL) <> YunSuan(sR) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case "<"
                    If YunSuan(sL) < YunSuan(sR) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case ">"
                    If YunSuan(sL) > YunSuan(sR) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case "<="
                    If YunSuan(sL) <= YunSuan(sR) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case ">="
                    If YunSuan(sL) >= YunSuan(sR) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case "STARTWITH"
                    If CType(YunSuan(sL), String).StartsWith(CType(YunSuan(sR), String)) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case "CONTAINS"
                    If CType(YunSuan(sL), String).Contains(CType(YunSuan(sR), String)) Then
                        objZhi = YunSuan(sTrue)
                    Else
                        objZhi = YunSuan(sFalse)
                    End If
                Case Else
                    objZhi = ""
            End Select
        Catch ex As Exception
            objZhi = ""
        End Try

        Return objZhi
    End Function

    Private Function V_Sum(ByVal sCanShu As String) As Object
        Dim objZhi As Decimal = 0
        Dim sL, sR As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sR = sCanShu.Remove(0, sL.Length + 1)
            objZhi = CType(YunSuan(sL), Decimal) + CType(YunSuan(sR), Decimal)
        Catch ex As Exception
            'msgbox("V_Sum函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = 0
        End Try
        Return objZhi
    End Function

    Private Function V_Jian(ByVal sCanShu As String) As Object
        Dim objZhi As Decimal = 0
        Dim sL, sR As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sR = sCanShu.Remove(0, sL.Length + 1)
            objZhi = YunSuan(sL) - YunSuan(sR)
        Catch ex As Exception
            'msgbox("V_Jia函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = 0
        End Try
        Return objZhi
    End Function

    Private Function V_Cheng(ByVal sCanShu As String) As Object
        Dim objZhi As Decimal = 0
        Dim sL, sR As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sR = sCanShu.Remove(0, sL.Length + 1)
            objZhi = YunSuan(sL) * YunSuan(sR)
        Catch ex As Exception
            'msgbox("V_Cheng函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = 0
        End Try
        Return objZhi
    End Function

    Private Function V_Chu(ByVal sCanShu As String) As Object
        Dim objZhi As Double = 0
        Dim sL, sR As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sR = sCanShu.Remove(0, sL.Length + 1)
            Dim iCS As Double = YunSuan(sR)
            If iCS = 0 Then
                Return 0
            End If
            objZhi = YunSuan(sL) / YunSuan(sR)
        Catch ex As Exception
            'msgbox("V_Chu函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = 0
        End Try
        Return objZhi
    End Function

    Private Function V_LianJie(ByVal sCanShu As String) As Object
        Dim objZhi As String = ""
        Dim sL, sR As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu))
            sR = sCanShu.Remove(0, sL.Length + 1)
            objZhi = YunSuan(sL).ToString & YunSuan(sR).ToString
        Catch ex As Exception
            'msgbox("V_LianJie函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = ""
        End Try
        Return objZhi
    End Function

    Private Function V_SSWR(ByVal sCanShu As String) As Object
        Dim objZhi As Double = 0
        Dim sL, sR As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu)) '需要四舍五入的数
            sR = sCanShu.Remove(0, sL.Length + 1) '需要保留的小数位数
            objZhi = Math.Round(CType(YunSuan(sL), Decimal), CType(YunSuan(sR), Integer))
        Catch ex As Exception
            'msgbox("V_SSWR函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = 0
        End Try
        Return objZhi
    End Function

    Private Function V_QZS(ByVal sCanShu As String) As Object
        Dim objZhi As Double = 0
        Dim sL As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu)) '需要取整的数
            objZhi = Math.Truncate(CType(YunSuan(sL), Double))
        Catch ex As Exception
            'msgbox("V_QZS函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = 0
        End Try
        Return objZhi
    End Function

    Private Function V_SubString(ByVal sCanShu As String) As Object
        Dim objZhi As String = ""
        Dim sL, sM, sR As String
        Try
            sL = sCanShu.Substring(0, getDouHaoIndex(sCanShu)) '源字符串
            sM = sCanShu.Remove(0, sL.Length + 1) '起始位置
            sM = sM.Substring(0, getDouHaoIndex(sM))
            sR = sCanShu.Remove(0, sL.Length + sM.Length + 2) '位数
            If CType(YunSuan(sL), String).Length < CType(YunSuan(sM), Integer) + CType(YunSuan(sR), Integer) Then
                Return ""
            End If
            objZhi = CType(YunSuan(sL), String).Substring(CType(YunSuan(sM), Integer), CType(YunSuan(sR), Integer))
        Catch ex As Exception
            'msgbox("V_SubString函数格式错误", MsgBoxStyle.Information, "提示")
            objZhi = ""
        End Try
        Return objZhi
    End Function

    Private Function getPrice(ByVal sR As String) As Object
        'ByVal JFKM As String, ByVal DFKM As String, ByVal KucunOrGongzi As String, ByVal sQDF As String, Optional ByVal sItem As String = "DanJia", Optional ByVal iTimes As Integer = 1
        Dim JFKM As String
        Dim DFKM As String
        Dim KuCunOrGongZi As String
        Dim sQDF As String
        Dim sItem As String
        Dim itimes As Integer

        sR = sR.Replace(" ", "")

        JFKM = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, JFKM.Length + 1)
        DFKM = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, DFKM.Length + 1)
        KuCunOrGongZi = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, KuCunOrGongZi.Length + 1)
        sQDF = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, sQDF.Length + 1)
        sItem = sR.Substring(0, Me.getDouHaoIndex(sR))
        sR = sR.Remove(0, sItem.Length + 1)
        itimes = sR

        'Dim sTemp() As String
        'sTemp = Split(sR, ",")
        'JFKM = sTemp(0)
        'DFKM = sTemp(1)
        'KuCunOrGongZi = sTemp(2)
        'sQDF = sTemp(3)
        'sItem = sTemp(4)
        'itimes = CType(sTemp(5), Integer)

        Dim sSQL As String
        Dim sCase As String = ""
        Dim sKM As String = YunSuan(JFKM)
        'Dim bJiSuan As Boolean = False

        ' '20200427 修改为只能提取显示行的数值，排除了不显示行的干扰
        If JFKM = "" Then
            sCase = " 1=1 "
        Else
            If sKM = "" Then
                Return 0
            Else
                sCase = " 科目 ='" & FKF.FKF.getKMDMfromQM(sKM) & "' "
            End If
        End If


        Dim sDYKM As String = YunSuan(DFKM)
        If DFKM = "" Then
        Else
            If sDYKM = "" Then
                Return 0
            Else
                sCase = sCase & " and 对应科目 ='" & FKF.FKF.getKMDMfromQM(sDYKM) & "' "
            End If
        End If

        If sQDF = "" Then
        Else
            sQDF = CType(YunSuan(sQDF), String)
            Dim iLength As Integer
            iLength = FKG.myselfG.iBianHaoWeiShu + FKG.myselfG.iNianFenWeiShu + sQDF.Length + 2
            sCase = sCase & " and CHARINDEX('" & sQDF & "',编号)=1 and len(编号)=" & iLength
        End If

        'sSQL = "select top " & YunSuan(itimes) & " " & YunSuan(sItem) & " from " & YunSuan(KuCunOrGongZi) & " where " & sCase & " order by 日期 DESC"

        '20200427 修改为只能提取显示行的数值，排除了不显示行的干扰  sSQL = "SELECT DISTINCT * FROM (select top " & YunSuan(itimes) & " " & YunSuan(sItem) & ",日期,A_JBID from " & YunSuan(KuCunOrGongZi) & " where " & sCase & " order by 日期 DESC,建立时间 desc) as ttt ORDER BY 日期 DESC,A_JBID desc"

        sSQL = "SELECT DISTINCT * FROM (select top " & YunSuan(itimes) & " " & YunSuan(sItem) & ",日期,A_JBID from " & YunSuan(KuCunOrGongZi) & " where 显示=1 and " & sCase & " order by 日期 DESC,建立时间 desc) as ttt ORDER BY 日期 DESC,A_JBID desc"

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sZhi As Object
        Try
            sZhi = mdb.Readeropen(sSQL).Tables(0).Rows(0).Item(0)
            Return sZhi
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function V_YETS(ByVal sCanShu As String) As Object
        If sCanShu = "" Then
            Return ""
        End If
        '计算当前余额函数，格式为V_YETS（科目，表名，余额|余数，仓库ID）
        '表名可以为”凭证；库存；工资“三者之一
        '仓库ID可以为多个，中间用：分割
        Dim sKMDM, sTable, sLie, sCKID As String
        Try
            sKMDM = FKF.FKF.getKMDMfromQM(YunSuan(Split(sCanShu, ",").GetValue(0)))
            If sKMDM = "" Then
                Return ""
            End If
            sTable = Split(sCanShu, ",").GetValue(1)
            sLie = Split(sCanShu, ",").GetValue(2)
            sCKID = Split(sCanShu, ",").GetValue(3)
        Catch ex As Exception
            Return ""
        End Try

        'Dim sTable2 As String
        'If sTable = "凭证" Then
        '    sTable = "科目余额表"
        '    sTable2 = "凭证表"
        'ElseIf sTable = "库存" Then
        '    sTable = "库存余额表"
        '    sTable2 = "库存表"
        'ElseIf sTable = "工资" Then
        '    sTable = "工资余额表"
        '    sTable2 = "工资表"
        'Else
        '    Return ""
        'End If
        '20121227日更新，直接访问表，不再使用查询
        Dim sTable2 As String
        If sTable = "凭证" Then
            sTable = "KMYE"
            sTable2 = "PZ"
        ElseIf sTable = "库存" Then
            sTable = "KCYE"
            sTable2 = "KuCun"
        ElseIf sTable = "工资" Then
            sTable = "GZYE"
            sTable2 = "GongZi"
        ElseIf sTable = "委外" Then
            sTable = "WWYE"
            sTable2 = "GongZi"
        Else
            Return ""
        End If

        Dim sCKIDCase As String = ""
        If sCKID = "" OrElse sCKID = "-1" Then
        Else
            Dim i As Integer
            For i = 0 To Split(sCKID, ":").Length - 1
                sCKIDCase = sCKIDCase & " CangKuID=" & Split(sCKID, ":").GetValue(i) & " or "
            Next
            sCKIDCase = sCKIDCase.Remove(sCKIDCase.Length - 4, 4)
            sCKIDCase = " and (" & sCKIDCase & ")"
        End If

        'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet

        Dim dNCYE, dFSE As Decimal
        If sLie = "余额" Then
            ds = mdb.ReaderOpen("select isNUll(NCJFYE-NCDFYE,0) as ncye from " & sTable & " where nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'" & sCKIDCase)
            If ds.Tables(0).Rows.Count = 0 Then
                dNCYE = 0
            Else
                dNCYE = ds.Tables(0).Rows(0).Item(0)
            End If
            ds.Clear()

            '20121227日更新，直接访问表，不再使用查询
            ds = mdb.ReaderOpen("select isnull(sum(借方金额)-sum(贷方金额),0) as FSE from " & sTable2 & " where 年份=" & FKG.myselfG.NianFen & " and 科目='" & sKMDM & "'" & sCKIDCase.Replace("CangKuID", "仓库"))
            'ds = mdb.ReaderOpen("select iif(isnull(sum(借方金额)-sum(贷方金额)),0,sum(借方金额)-sum(贷方金额)) as FSE from " & sTable2 & " where 年份=" & FKG.myselfG.NianFen & " and 科目DM='" & sKMDM & "'" & sCKIDCase.Replace("仓库编号", "仓库"))
            If ds.Tables(0).Rows.Count = 0 Then
                dFSE = 0
            Else
                dFSE = ds.Tables(0).Rows(0).Item(0)
            End If

            Return dNCYE + dFSE
        Else
            ds = mdb.ReaderOpen("select isNUll(NCJFSL-NCDFSL,0) as ncye from " & sTable & " where nian=" & FKG.myselfG.NianFen & " and KMDM ='" & sKMDM & "'" & sCKIDCase)
            If ds.Tables(0).Rows.Count = 0 Then
                dNCYE = 0
            Else
                dNCYE = ds.Tables(0).Rows(0).Item(0)
            End If
            ds.Clear()

            '20121227日更新，直接访问表，不再使用查询
            ds = mdb.ReaderOpen("select isnull(sum(借方数量)-sum(贷方数量),0) as FSE from " & sTable2 & " where 年份=" & FKG.myselfG.NianFen & " and 科目='" & sKMDM & "'" & sCKIDCase.Replace("CangKuID", "仓库"))

            'ds = mdb.ReaderOpen("select iif(isnull(sum(借方数量)-sum(贷方数量)),0,sum(借方数量)-sum(贷方数量)) as FSE from " & sTable2 & " where 年份=" & FKG.myselfG.NianFen & " and 科目DM='" & sKMDM & "'" & sCKIDCase.Replace("仓库编号", "仓库"))
            If ds.Tables(0).Rows.Count = 0 Then
                dFSE = 0
            Else
                dFSE = ds.Tables(0).Rows(0).Item(0)
            End If

            Return dNCYE + dFSE
        End If

    End Function

    ' 格式为：V_FKGSHS 函数名,参数1，参数2，参数3
    Private Function FKHanShu(ByVal sGongShi As String) As Object
        'Dim hanshu As New FKHanShu.HanShu(sGongShi)
        'Return hanshu.zdyYunSuan

        Dim sYSF As String
        sYSF = Split(sGongShi, ",").GetValue(0).ToString
        Dim sCanShu() As String
        sCanShu = Split(sGongShi.Remove(0, sYSF.Length), ",")

        Try
            Select Case sYSF
                Case "V_WCSL" '外存数量。参数为： 委外客户代码，产品代码
                    'V_FKGSHS(V_WCSL,DaiFangKeMu,BeiYong4) 实例
                    Dim iWCSL As Double
                    'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                    Dim sSQL As String
                    sSQL = "select ncjfsl-ncdfsl from WeiWaiYE where NIAN=" & FKG.myselfG.NianFen & " and WWKHDM='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(1))) & "' and KMDM ='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(2))) & "'"

                    Dim dsYE As DataSet
                    dsYE = mdb.ReaderOpen(sSQL)
                    If dsYE.Tables(0).Rows.Count = 0 Then
                        iWCSL = 0
                    Else
                        iWCSL = dsYE.Tables(0).Rows(0).Item(0)
                    End If '得到年初余额

                    sSQL = "SELECT isnull(SUM(借方数量),0)-isnull(SUM(贷方数量),0)  FROM GONGZI where 年份= " & FKG.myselfG.NianFen & " and (对应科目='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(2))) & "' and 科目='" & FKF.FKF.getKMDMfromQM(YunSuan(sCanShu(1))) & "') group  by 对应科目"
                    dsYE.Clear()
                    dsYE = mdb.ReaderOpen(sSQL)

                    If dsYE.Tables(0).Rows.Count = 0 Then
                        iWCSL = iWCSL + 0
                    Else
                        iWCSL = iWCSL + dsYE.Tables(0).Rows(0).Item(0)
                    End If '得到年初余额
                    Return iWCSL
                Case "V_ZDYCCGC"
                    '格式为'V_FKGSHS(V_ZDYCCGC,存储过程名,存储过程参数)  参数格式为 参数名,参数值;参数名,参数值
                    '返回指定存储过程的单个值。
                    Dim dResult As Object

                    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                    Try
                        Dim sCCGCName As String = sCanShu(1)

                        '  Dim sPName As String
                        Dim sPValue As String = ""

                        Dim sTemp As String
                        sTemp = sGongShi.Remove(0, sYSF.Length + 1 + sCanShu(1).Length + 1)

                        Dim sP() As String
                        sP = Split(sTemp, ";")
                        Dim ssP As String
                        For Each ssP In sP
                            sPValue = sPValue & Split(ssP, ",")(0).ToString & "," & YunSuan(Split(ssP, ",")(1).ToString) & ";"

                        Next
                        sPValue = sPValue.Remove(sPValue.Length - 1, 1)

                        dResult = mdb.ReadStoredProcedure(FKG.myselfG.asasR, sCanShu(1), sPValue).Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        dResult = 0
                    End Try

                    Return dResult
                Case Else
                    Return 0
            End Select
        Catch ex As Exception
            Return 0
        End Try


    End Function
    '以上为公式计算,重新规划，保持各公式的格式一致，以及运算优先级问题的解决


    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 36
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            sCDO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        End If

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
            Next
        End If


        FKG.myselfG.sVersion = My.Application.Info.Version.ToString

        'FKG.myselfG.sBanBen = "试用版"
        FKG.myselfG.sBanBen = "开发版"
        'FKG.myselfG.sBanBen = "正式版"
        'FKG.myselfG.sBanBen = ""

        FKG.myselfG.sClient = "服务器端"
        'FKG.myselfG.sClient = "客户端"

        Return sMCO & FKG.myselfG.sVersion & FKG.myselfG.sBanBen & FKG.myselfG.sClient

    End Function

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet
        Dim cpu

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    Private Function HID() As String
        Dim HDid As String = ""
        Try
            Dim cimobject As New Management.ManagementClass("win32_diskdrive")
            Dim moc As Management.ManagementObjectCollection
            moc = cimobject.GetInstances()
            Dim mo As Management.ManagementObject

            For Each mo In moc
                'HDid = HDid & mo.Properties("signature").Value.ToString
                HDid = HDid & mo.Properties("PNPDeviceID").Value.ToString
                HDid = HDid.Replace("_", "")
                HDid = HDid.Remove(0, mo.Properties("Caption").Value.ToString.Length)
                Exit For
            Next
        Catch ex As Exception
            HDid = ""
        End Try

        Return HDid

    End Function


    Private Function Decrypt(ByVal p_inputString As String, ByVal p_strKeyPath As String) As String
        Dim fileString As String = ""
        Dim outString As String = ""

        'If IO.File.Exists(p_strKeyPath) Then
        '    Dim streamReader As New IO.StreamReader(p_strKeyPath, True)
        '    fileString = streamReader.ReadToEnd
        '    streamReader.Close()
        'End If

        Try
            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            fileString = mdb.ReaderOpen("select sPraKey from systemp where sComputerName='FreedomKing'").Tables(0).Rows(0).Item(0).ToString

            If fileString.Contains("0") Then
            Else
                Return ""
            End If
        Catch ex As Exception
            fileString = ""
        End Try

        If fileString <> "" Then
            Dim bitStrengthString As String = fileString.Substring(0, fileString.IndexOf("</BitStrength>") + 14)
            fileString = fileString.Replace(bitStrengthString, "")
            Dim bitStrength As Int32 = Convert.ToInt32(bitStrengthString.Replace("<BitStrength>", "").Replace("</BitStrength>", ""))
            Try
                outString = DecryptString(p_inputString, bitStrength, fileString)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End If

        Return outString

    End Function

    Private Function DecryptString(ByVal inputString As String, ByVal dwKeySize As Integer, ByVal xmlString As String) As String
        Dim rsaCryptoServiceProvider As New System.Security.Cryptography.RSACryptoServiceProvider(dwKeySize)
        rsaCryptoServiceProvider.FromXmlString(xmlString)

        Dim base64BlockSize As Integer
        If (dwKeySize / 8) Mod 3 <> 0 Then
            base64BlockSize = Int(((dwKeySize / 8) / 3) * 4) + 4
        Else
            base64BlockSize = Int((dwKeySize / 8) / 3) * 4

        End If

        Dim iterations As Integer = Int(inputString.Length / base64BlockSize)
        Dim arrayList As New System.Collections.ArrayList

        Dim i As Integer
        For i = 0 To iterations
            Dim encryptedBytes() As Byte
            Dim itemp As Integer

            If inputString.Length >= base64BlockSize Then
                itemp = base64BlockSize - 2
            Else
                itemp = inputString.Length
            End If

            encryptedBytes = Convert.FromBase64String(inputString.Substring(itemp * i, itemp))

            Array.Reverse(encryptedBytes)

            arrayList.AddRange(rsaCryptoServiceProvider.Decrypt(encryptedBytes, True))

        Next

        Return System.Text.Encoding.UTF32.GetString(TryCast(arrayList.ToArray(Type.[GetType]("System.Byte")), Byte()))

    End Function


    Private Sub FKGN_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            My.Computer.Keyboard.SendKeys("{Tab}")
            e.Handled = True
        End If

    End Sub



   
    Private Sub Fkgns1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 特殊.Click
        '读取配置文件，获取功能相关信息
        Dim xml As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKConfig.xml", True, "config")

        'Dim dlg As New DZFKGNJGGZ.DZFKGNJGGZ
        Dim sGNID = xml.Read("特殊功能ID_" & iGongnengID, "name", "config")
        Dim sGNName = xml.Read("特殊功能名称_" & iGongnengID, "name", "config")

        If sGNID = "" Then
            MsgBox("该功能未设置子功能，请修改配置文件！", MsgBoxStyle.Information, "修改配置文件")
            Exit Sub
        End If
        'dlg.iGXNumber = 4
        'dlg.myPZSCFS = 0
        'dlg.Show(Me)

        Dim dlg2 As New FKGN("NMVsAQx4YC6PlShS/NfrGHLDkJDaJtzKGb0aTgYy/zbzz5Sifdxal4QzCbUEnP3f8aV9MuhRtpnfdKUkGcOBBfszFvvt60k8ANun5pmRLCI29hkPkxPZXw4WWh34UQflKEbfrqAoiWMMrVAg4yzmQBxK0Ap3fFkpBRDaH58dbwxgOVtJr715tNPD5KHchsnH0ackUkvK116or2BaNHnZs8gVTiDmMU/naY6awQ+akTimuK9nrzDmkW0diHBCvbiAhelrLZ3idIBh/6VM+5GW59Mw4pInsAsdxXIe7M8kutjCc4Djefanz8BICTsdPgQKFD/pR7dMR22iN96bssvYUT5VT68+4ZqQoHvVvmuhrJ8I9d74+i9i9IVPGMX+x0fY4jJrkboDzAZEIqhbLsf4xPfRdvgsKibUrgvaXKKrAKsTZMt2Rjm8e2+FVnB0XLawFF+hCvpE34TDm1WlHLaQ3SSuMuMRDL2C8qCA0LeTQDDer8weG147fIU1f/bZFoKy/GlYEzX8TUylCZOjMncEgndKkfLQniySYmeeYrqz0abkUpmuFqI5L80LHJev5148Z+VK/0HazhdYtbzbCZNHG2MeWOk5G3xQSiuwa9ymjSnS+0gPI1Py8eFg9yGsoYWxpJgsFzaXs9tS2ABXsUxpA5Ndb1SdYVdwFzOcAuQVmHQ=")
        'If dlg.DialogResult = Windows.Forms.DialogResult.No Then
        '    Exit Sub
        'End If
        dlg2.myGongneng = sGNID
        dlg2.myGongNengName = sGNName
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select PZSCFS,DJZhangShu from GongNengShu where id=" & sGNID)
        dlg2.myPZSCFS = ds.Tables(0).Rows(0).Item(0)
        dlg2.myPZZhangShu = ds.Tables(0).Rows(0).Item(1)

        dlg2.sZBMC = sZBMC
        'FKG.myselfG.isOpendedGN = True

        dlg2.sFuKuanBianHao = Me.sBianHao

        dlg2.Show(Me)
    End Sub

    Private Sub AutoRunAfterSave()
        Dim mdbR As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sCMD As Integer
        sCMD = 0 '0代表是自动运行的
        Dim spara As String = ""
        spara = spara & "@GNSID," & Me.iGongnengID.ToString & ";@BIANHAO," & Me.sBianHao & ";@USERID," & FKG.myselfG.YongHuID & ";@sJBID," & sJiBenCaoZuoID & ";@sCMDID," & sCMD.ToString

        Dim ds As DataSet
        Try
            ds = mdbR.ExecStoredProcedure(FKG.myselfG.asasR, "RunCommand", spara)

        Catch ex As Exception
            ' MsgBox(ex.ToString)
        Finally
            sJiBenCaoZuoID = ""
        End Try

        'FillDG(sBianHao)

    End Sub

    Private Sub tsbCmd1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbCmd1.Click, tsbCmd2.Click, tsbCmd3.Click, tsbCmd4.Click, tsbCmd5.Click
        '读取数据库，执行相应的存储过程命令
        Dim sCMD As String
        sCMD = CType(sender, Windows.Forms.ToolStripButton).Name
        sCMD = sCMD.Substring(sCMD.Length - 1, 1)

        Dim mdbR As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim spara As String = ""
        spara = spara & "@GNSID," & Me.iGongnengID.ToString & ";@BIANHAO," & Me.sBianHao & ";@USERID," & FKG.myselfG.YongHuID & ";@sJBID," & sJiBenCaoZuoID & ";@sCMDID," & sCMD

        Dim ds As DataSet
        Try
            ds = mdbR.ExecStoredProcedure(FKG.myselfG.asasR, "RunCommand", spara)
           
        Catch ex As Exception
            ' MsgBox(ex.ToString)
        Finally
            sJiBenCaoZuoID = ""
        End Try

        FillDG(sBianHao)
    End Sub

    Private Sub ShowTSBCMD()
        Dim mdbr As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        Try
            ds = mdbr.Reader("select * from zdycmd where USERID=" & FKG.myselfG.YongHuID & " AND GNSID= " & Me.iGongnengID & " and [Enable]=1")
            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Select Case ds.Tables(0).Rows(i).Item("CMDID")
                    Case 1
                        Me.tsbCmd1.Visible = True
                        Me.tsbCmd1.Text = ds.Tables(0).Rows(i).Item("Name").ToString
                        ' Me.tsbCmd1.BackgroundImage.FromFile(My.Application.Info.DirectoryPath & "Data\Picture\" & ds.Tables(0).Rows(i).Item("pic").ToString)
                    Case 2
                        Me.tsbCmd2.Visible = True
                        Me.tsbCmd2.Text = ds.Tables(0).Rows(i).Item("Name").ToString
                    Case 3
                        Me.tsbCmd3.Visible = True
                        Me.tsbCmd3.Text = ds.Tables(0).Rows(i).Item("Name").ToString
                    Case 4
                        Me.tsbCmd4.Visible = True
                        Me.tsbCmd4.Text = ds.Tables(0).Rows(i).Item("Name").ToString
                    Case 5
                        Me.tsbCmd5.Visible = True
                        Me.tsbCmd5.Text = ds.Tables(0).Rows(i).Item("Name").ToString
                    Case Else
                End Select
            Next

        Catch ex As Exception

        End Try
    End Sub

    Private Function getToken(ByVal sName As String) As String
        Dim sToken As String = ""
        Dim sT As String = ""
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If Asc(sName.Substring(i, 1)) > 122 Then

            Else
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            End If
        Next

        Dim sTR As String = "A"
        For i = 0 To sT.Length.ToString - 1
            If i Mod 7 = 0 Then
                sTR = sTR & sT.Substring(i, 1)
            End If
        Next

        sToken = sName & "," & sTR
        Return sToken
    End Function

    Private Sub picZT1_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles picZT1.MouseHover
        If Not (Me.picZT1.Image Is Nothing) Then
            Me.pbMax.Image = Me.picZT1.Image
            Me.pbMax.Show()
        End If
    End Sub

    Private Sub picZT1_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles picZT1.MouseLeave, picZT2.MouseLeave, picZT3.MouseLeave, picZT4.MouseLeave, picZT5.MouseLeave
        Me.pbMax.Hide()
    End Sub

    Private Sub picZT2_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles picZT2.MouseHover
        If Not (Me.picZT2.Image Is Nothing) Then
            Me.pbMax.Image = Me.picZT2.Image
            Me.pbMax.Show()
        End If
    End Sub
    Private Sub picZT3_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles picZT3.MouseHover
        If Not (Me.picZT3.Image Is Nothing) Then
            Me.pbMax.Image = Me.picZT3.Image
            Me.pbMax.Show()
        End If
    End Sub

    Private Sub picZT4_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles picZT4.MouseHover
        If Not (Me.picZT4.Image Is Nothing) Then
            Me.pbMax.Image = Me.picZT4.Image
            Me.pbMax.Show()
        End If
    End Sub

    Private Sub picZT5_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles picZT5.MouseHover
        If Not (Me.picZT5.Image Is Nothing) Then
            Me.pbMax.Image = Me.picZT5.Image
            Me.pbMax.Show()
        End If
    End Sub

End Class

'测试用时代码
'   Dim t As DateTime
'    t = Now
'    MsgBox((Now - t).ToString)

