﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Public sSessionID As String

    Public dlTitle As String
    Public dlName As String
    Public dlHomePic As String
    Public dlLogoPic As String
    Public dlLoginPic As String

    Public dlWebSite As String

    Public dlDownload As String
    Public dlHelp As String
    Public dlFeedback As String
    Public dlForget As String
    Public dlMyCenter As String

    Public dlFileURL As String

    Public sInputDLID As String
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Me.btnSubmit.Enabled = False

        Dim sName As String = Request.Form("username")
        Dim sPWS As String = Request.Form("passwd")

        Dim sInDlID As String = Request.Form("sInputDLID")

        If sName = "" OrElse sPWS = "" Then
            legend.Text = "用户名和密码不能为空"
            legend.Style("Display") = "Block"
            Exit Sub
        End If

        sSessionID = Session.SessionID

        Dim ws As New wsbdsp.Service
        Dim ds As New Data.DataSet


        'ds = ws.Login(getTok(sSessionID), sName, "", sPWS, 1)
        ds = ws.NewLogin(getTok(sSessionID), sName, "", sPWS, 1, "wap")

        If ds.Tables(0).Rows.Count = 0 Then
            legend.Style("Display") = "Block"
            Session.Add("LoginInfo", Nothing)
        Else


            Dim cookie As New HttpCookie("myLoginName")

            cookie.Values.Add("LoginName", sName)
            cookie.Values.Add("LoginPWD", sPWS)
            Response.AppendCookie(cookie)

            '我们取出Cookie值也很简单()

            legend.Style("Display") = "None"
            Session.Add("LoginInfo", ds)

 
            Session.Add("QiYeName", ds.Tables(0).Rows(0).Item("comName").ToString)

            Dim bChangePWS As Int16
            Try
                bChangePWS = ds.Tables(0).Rows(0).Item("ChangePWS").ToString

            Catch ex As Exception
                bChangePWS = 1
            End Try
            If bChangePWS = 0 Then

                Response.Redirect("changepws.aspx")
            End If

            Response.Redirect("main.aspx?dlid=" & sInDlID)
        End If

        Me.btnSubmit.Enabled = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack Then
            Exit Sub
        End If

        sInputDLID = Request.Params.Item("dlid")

        '根据网址参数确定所用代理商信息
        Dim sDL As DLInfo.dlInfo
        'DLInfo作为一个独立的dll文件，单独保存，需要更欣赏，只需要更新DLInfo项目即可。
        Dim sDLID As String = Request.Params.Item("dlid")
        sDL = New DLInfo.dlInfo(sDLID)
        Session.Add("DLInfo", sDL)
        'If IsNothing(Session("DLInfo")) Then
        'Else
        '    sDL = Session("DLInfo")
        'End If

        dlTitle = sDL.dlTitle
        dlName = sDL.dlName
        dlHomePic = sDL.dlHomePic
        dlLogoPic = sDL.dlLogoPic
        dlLoginPic = sDL.dlLoginPic
        dlWebSite = sDL.dlWebSite

        dlDownload = sDL.dlDownload
        dlHelp = sDL.dlHelp
        dlFeedback = sDL.dlFeedback
        dlForget = sDL.dlForget
        dlMyCenter = sDL.dlMyCenter

        dlFileURL = sDL.dlFileURL


        Dim scookie As HttpCookie = Request.Cookies("myLoginName")
        If Not IsNothing(scookie) Then
            Dim sName As String
            sName = scookie.Item("LoginName")
            Dim sPWS As String
            sPWS = scookie.Item("LoginPWD")

            If sName = "" OrElse sPWS = "" Then
                legend.Text = "用户名和密码不能为空"
                legend.Style("Display") = "Block"
                Exit Sub
            End If

            sSessionID = Session.SessionID

            Dim ws As New wsbdsp.Service
            Dim ds As New Data.DataSet

            ds = ws.NewLogin(getTok(sSessionID), sName, "", sPWS, 1, "wap")

            If ds.Tables(0).Rows.Count = 0 Then
                legend.Style("Display") = "Block"
                Session.Add("LoginInfo", Nothing)
            Else
                legend.Style("Display") = "None"
                Session.Add("LoginInfo", ds)
                Session.Add("QiYeName", ds.Tables(0).Rows(0).Item("comName").ToString)

                Dim bChangePWS As Int16
                Try
                    bChangePWS = ds.Tables(0).Rows(0).Item("ChangePWS").ToString

                Catch ex As Exception
                    bChangePWS = 1
                End Try
                If bChangePWS = 0 Then

                    Response.Redirect("changepws.aspx")
                End If

                Response.Redirect("main.aspx?dlid=" & sDLID)
            End If

        End If
        legend.Style("Display") = "None"

    End Sub




    Private Function getTok(ByVal sTokenKey As String) As String
        Try
            Dim sName As String = sTokenKey
            'Dim sPWS As String = Split(sToken, ",").GetValue(1).ToString

            Dim sT As String = ""
            Dim i As Integer
            For i = 0 To sName.Length - 1
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            Next

            Dim sTR As String = "A"
            For i = 0 To sT.Length.ToString - 1
                If i Mod 7 = 0 Then
                    sTR = sTR & sT.Substring(i, 1)
                End If
            Next

            Return sName & "," & sTR
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class
