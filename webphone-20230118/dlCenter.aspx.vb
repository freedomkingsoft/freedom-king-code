﻿
Partial Class dlCenter
    Inherits System.Web.UI.Page


    Public dsLoginInfo As New Data.DataSet

    Public loginPhone As String

    Public tishiMM As String = ""
    Public ShowAdmin As String = "none"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            loginPhone = dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim dsdl As Data.DataSet
            dsdl = mdb.Reader("select * from dlList where dlPhone='" & loginPhone & "'")
            '先判断是否为代理商
            If dsdl.Tables(0).Rows.Count = 0 Then
                tishiMM = "你还不是代理商，无权使用代理商功能"
                Response.Write("<script>alert('你目前还不是代理商，无权使用代理商功能，请先提交申请。');window.location.href='mycenter.aspx'</script>")
                If loginPhone.Replace(" ", "") = "13091288796" Then
                    ShowAdmin = "block"
                End If
            Else
                If loginPhone.Replace(" ", "") = "13091288796" Then
                    ShowAdmin = "block"
                End If
            End If

        End If
    End Sub

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function


    Protected Sub btnExitLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitLogin.Click
        Session.Abandon()
        Response.Cookies("myLoginName").Expires = Now.AddDays(-1)
        Response.Redirect("default.aspx")
    End Sub
End Class
