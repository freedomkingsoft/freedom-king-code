﻿
Partial Class help
    Inherits System.Web.UI.Page

    Public sQiYeName As String


    Dim dsLoginInfo As Data.DataSet
    Public dlID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dlID = Request.Params.Item("dlid")
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Page.IsPostBack = False Then
            getBaoBiaoList()
        End If

        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            'Response.Redirect("Default.aspx")
        Else

        End If
    End Sub

    Public qList As String
    Private Sub getBaoBiaoList()
        dsLoginInfo = Session("LoginInfo")

        ' If IsNothing(dsLoginInfo) = False Then

        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
        Dim dsLieBiao As Data.DataSet
        If Request.Params("kwd") = "" Then
            dsLieBiao = mdb.Reader("USE A_web_bdsp;select dt_article_category.title as category,dt_channel_article_down.title as title,dt_channel_article_down.img_url as img_url,dt_channel_article_down.id as id from dt_article_category  join dt_channel_article_down on dt_article_category.id=dt_channel_article_down.category_id order by dt_article_category.sort_id,dt_channel_article_down.sort_id")
        Else
            dsLieBiao = mdb.Reader("USE A_web_bdsp;select dt_article_category.title as category,dt_channel_article_down.title as title,dt_channel_article_down.img_url as img_url,dt_channel_article_down.id as id from dt_article_category  join dt_channel_article_down on dt_article_category.id=dt_channel_article_down.category_id  where CHARINDEX('" & Request.Params("kwd").ToString & "',dt_channel_article_down.content,1)>0 order by dt_article_category.sort_id,dt_channel_article_down.sort_id")
            'dsLieBiao = mdb.Reader("USE A_web_bdsp;SELECT title,img_url,id  from dt_channel_article_down where CHARINDEX('" & Request.Params("kwd").ToString & "',content,1)>0 order by channel_id ,sort_id ")
        End If
        Dim i As Integer
        Dim sb As New StringBuilder
        sb.Append(" <div data-node=""cardList"" class=""card-list"">")

        Dim iCKBox As Integer = 0

        For i = 0 To dsLieBiao.Tables(0).Rows.Count - 1
            If i = 0 Then
                iCKBox = i
                sb.Append("<div class=""card card4 line-around"" style=""background-color: InfoBackground"" onclick=""changeshow(divhelp" & iCKBox.ToString & ")"">")
                sb.Append(" <div class=""layout-box "">")
                sb.Append(" <div class=""box-col txt-cut"">")
                sb.Append("<span class=""mct-a "" style=""font-style: italic"">")
                sb.Append(dsLieBiao.Tables(0).Rows(i).Item("category").ToString)
                sb.Append("</span>")
                sb.Append(" </div>")
                sb.Append(" <span data-node=""arrow"" class=""plus plus-s"" style=""width: 4.5rem"">")
                sb.Append("点击显示")
                sb.Append(" </span>")
                sb.Append(" </div>")
                sb.Append("</div>")
                sb.Append("<div   style=""display:none"" id=""divhelp" & iCKBox.ToString & """>")
                sb.Append("<div class=""card card4 line-around"" >")
                sb.Append(" <a href=""helpdetail.aspx?id=" & dsLieBiao.Tables(0).Rows(i).Item("id").ToString & """ class=""layout-box"" data-act-type=""hover"">")
                sb.Append(" <div class=""box-col txt-cut"">")
                sb.Append("<span class=""mct-a "">")
                sb.Append(dsLieBiao.Tables(0).Rows(i).Item("title").ToString)
                sb.Append("</span>")
                sb.Append(" </div>")
                sb.Append(" <span data-node=""arrow"" class=""plus plus-s""><i class=""icon-font icon-font-arrow-right txt-s"">")
                sb.Append("》</i> </span></a>")
                sb.Append("</div>")

            Else
                If dsLieBiao.Tables(0).Rows(i).Item("category") = dsLieBiao.Tables(0).Rows(i - 1).Item("category") Then
                    sb.Append("<div class=""card card4 line-around"">")
                    sb.Append(" <a href=""helpdetail.aspx?id=" & dsLieBiao.Tables(0).Rows(i).Item("id").ToString & """ class=""layout-box"" data-act-type=""hover"">")
                    sb.Append(" <div class=""box-col txt-cut"">")
                    sb.Append("<span class=""mct-a "">")
                    sb.Append(dsLieBiao.Tables(0).Rows(i).Item("title").ToString)
                    sb.Append("</span>")
                    sb.Append(" </div>")
                    sb.Append(" <span data-node=""arrow"" class=""plus plus-s""><i class=""icon-font icon-font-arrow-right txt-s"">")
                    sb.Append("》</i> </span></a>")
                    sb.Append("</div>")
                Else
                    iCKBox = i
                    sb.Append("</div>")


                    sb.Append("<div class=""card card4 line-around"" style=""background-color: InfoBackground"" onclick=""changeshow(divhelp" & iCKBox.ToString & ")"">")
                    sb.Append(" <div class=""layout-box "">")
                    sb.Append(" <div class=""box-col txt-cut"">")
                    sb.Append("<span class=""mct-a "" style=""font-style: italic"">")
                    sb.Append(dsLieBiao.Tables(0).Rows(i).Item("category").ToString)
                    sb.Append("</span>")
                    sb.Append(" </div>")
                    sb.Append(" <span data-node=""arrow"" class=""plus plus-s"" style=""width: 4.5rem"">")
                    sb.Append("点击显示")
                    sb.Append(" </span>")
                    sb.Append(" </div>")
                    sb.Append("</div>")

                    sb.Append("<div   style=""display:none"" id=""divhelp" & iCKBox.ToString & """>")
                    sb.Append("<div class=""card card4 line-around"" >")

                    sb.Append(" <a href=""helpdetail.aspx?id=" & dsLieBiao.Tables(0).Rows(i).Item("id").ToString & """ class=""layout-box"" data-act-type=""hover"">")
                    sb.Append(" <div class=""box-col txt-cut"">")
                    sb.Append("<span class=""mct-a "">")
                    sb.Append(dsLieBiao.Tables(0).Rows(i).Item("title").ToString)
                    sb.Append("</span>")
                    sb.Append(" </div>")
                    sb.Append(" <span data-node=""arrow"" class=""plus plus-s""><i class=""icon-font icon-font-arrow-right txt-s"">")
                    sb.Append("》</i> </span></a>")
                    sb.Append("</div>")
                End If
            End If
        Next
        sb.Append(" </div>")
        sb.Append(" </div>")

        qList = sb.ToString

        'listbaobiao.DataSource = dsLieBiao
        'listbaobiao.DataBind()
        'sQiYeName = dsLoginInfo.Tables(0).Rows(0).Item("comName").ToString
        ' End If
    End Sub

    Private Function getConnectString() As String
        Dim scon As String

        scon = System.Configuration.ConfigurationManager.ConnectionStrings.Item("debugDatabase").ToString
        'scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & System.Configuration.ConfigurationManager.AppSettings("DATASOURCE")
        Return scon
    End Function


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sbh As String
        sbh = Request.Form("keyword")

        If sbh <> "" Then
            Response.Redirect("help.aspx?kwd=" & sbh)
        End If
    End Sub
End Class