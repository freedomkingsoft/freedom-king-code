﻿
Partial Class Main
    Inherits System.Web.UI.Page

    Public sQiYeName As String

    Dim dsLoginInfo As Data.DataSet

    Public dlTitle As String
    Public dlName As String
    Public dlHomePic As String
    Public dlLogoPic As String
    Public dlLoginPic As String
    Public dlWebSite As String

    Public dlDownload As String
    Public dlHelp As String
    Public dlFeedback As String
    Public dlForget As String
    Public dlMyCenter As String
    Public dlFileURL As String

    Public dlID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dlID = Request.Params.Item("dlid")

        Dim sDL As DLInfo.dlInfo
        If IsNothing(Session("DLInfo")) Then
            Dim sDLID As String = Request.Params.Item("dlid")
            sDL = New DLInfo.dlInfo(sDLID)
            Session.Add("DLInfo", sDL)
            'Response.Redirect("Default.aspx")
        Else
            sDL = Session("DLInfo")
        End If

        dlTitle = sDL.dlTitle
        dlName = sDL.dlName
        dlHomePic = sDL.dlHomePic
        dlLogoPic = sDL.dlLogoPic
        dlLoginPic = sDL.dlLoginPic
        dlWebSite = sDL.dlWebSite

        dlDownload = sDL.dlDownload
        dlHelp = sDL.dlHelp
        dlFeedback = sDL.dlFeedback
        dlForget = sDL.dlForget
        dlMyCenter = sDL.dlMyCenter

        dlFileURL = sDL.dlFileURL
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx?dlid=" & Request.Params.Item("dlid"))
        Else
            If Page.IsPostBack = False Then
                getBaoBiaoList()
            End If
        End If
    End Sub

    Private Sub getBaoBiaoList()
        dsLoginInfo = Session("LoginInfo")

        If IsNothing(dsLoginInfo) = False Then

            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim dsLieBiao As Data.DataSet
            dsLieBiao = mdb.Reader("select ID,ConText,SUBSTRING(TiShi,1,CHARINDEX(',',TiShi)) as shuoming from ZDYYUNBB where [index]=99 order by [ParentID]")
            listbaobiao.DataSource = dsLieBiao
            listbaobiao.DataBind()

            '获取最新数据
            Dim dsYuE As Data.DataSet
            dsYuE = mdb.ReadStoredProcedure(getConnectString(), "yun_bb_Main", "")
            dsMain.DataSource = dsYuE
            dsMain.DataBind()

            sQiYeName = dsLoginInfo.Tables(0).Rows(0).Item("comName").ToString
        End If
    End Sub

    Private Function getConnectString() As String
        Dim scon As String

        'scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & System.Configuration.ConfigurationManager.AppSettings("DATASOURCE")
        scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & dsLoginInfo.Tables(0).Rows(0).Item("DATASOURCE").ToString
        Return scon
    End Function


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sbh As String
        sbh = Request.Form("bianhao")

        If sbh <> "" Then
            Response.Redirect("detail.aspx?bh=" & sbh)
        End If
    End Sub
End Class

