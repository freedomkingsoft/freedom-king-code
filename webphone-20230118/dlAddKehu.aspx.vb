﻿
Partial Class dlAddKehu
    Inherits System.Web.UI.Page
    Public dsLoginInfo As New Data.DataSet

    Public loginPhone As String

    Public shenqingren As String
    Public xiangxidizhi As String
    Public weixin As String
    Public qqhao As String
    Public zhiye As String
    Public gerenshuoming As String
    Public jindu As String
    Public fankui As String
    Public fankuidate As String

    Public selecthangye As String = ""

    Public tishiMM As String = "none"

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            loginPhone = dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString

            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim dsdl As Data.DataSet
            dsdl = mdb.Reader("select * from dlList where dlPhone='" & loginPhone & "'")
            '先判断是否在申请表中已存在，如存在则进入修改功能页面
            If dsdl.Tables(0).Rows.Count = 0 Then
                tishiMM = "你还不是代理商，无权使用代理商功能"
            Else
                '填充行业列表
                Dim dsHangYe As Data.DataSet
                dsHangYe = mdb.Reader("select '<option>'+hangyename+'</option>' from hangye order by [orderby]")
                Dim i As Integer = 0
                For i = 0 To dsHangYe.Tables(0).Rows.Count - 1
                    selecthangye = selecthangye & dsHangYe.Tables(0).Rows(i).Item(0).ToString
                Next
            End If
        End If
    End Sub

    Private Function bangdingDL(ByVal sDLPHONE As String, ByVal sUserPhone As String, ByVal sUserLoginID As String) As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
        Dim ds As Data.DataSet
        ds = mdb.Reader("select * from yunUser where userLoginID='" & sUserLoginID & "' and regPhone='" & sUserPhone & "'")
        Dim sYunUserID As String
        If ds.Tables(0).Rows.Count > 0 And mdb.bExsit("select dlid from dltuijian where yunuserphone='" & sUserPhone & "' and yunuserloginid='" & sUserLoginID & "' ") = False Then
            sYunUserID = ds.Tables(0).Rows(0).Item(0).ToString
            Dim sb As New StringBuilder
            sb.Append("INSERT INTO [dlTuiJian]([dlID],[dlPhone],[yunUserID],[yunUserPhone],[yunUserLoginID]) VALUES ('")
            sb.Append(dsLoginInfo.Tables(0).Rows(0).Item("userID").ToString)
            sb.Append("','")
            sb.Append(sDLPHONE)
            sb.Append("','")
            sb.Append(sYunUserID)
            sb.Append("','")
            sb.Append(sUserPhone)
            sb.Append("','")
            sb.Append(sUserLoginID)
            sb.Append("')")

            mdb.Write(sb.ToString)
        Else
            tishiMM = "你填写的客户不存在，请检查注册手机号和用户ID"
        End If

    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            loginPhone = dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString
        End If

        If Request.Form("reg_phone").ToString = "" OrElse Request.Form("reg_pws").ToString = "" OrElse Request.Form("reg_pws2").ToString = "" OrElse Request.Form("reg_Clients").ToString = "" OrElse Request.Form("reg_comname").ToString = "" Then
            tishiMM = "有必填项为空通过，请检查输入。"
            Exit Sub

        End If
        If Request.Form("reg_pws").ToString <> Request.Form("reg_pws2").ToString Then
            tishiMM = "两次密码不一致，请检查输入。"
            Exit Sub
        Else
            Dim ws As New wsbdsp.Service
            Dim ds As Data.DataSet
            If ws.CheckPhone(getTok(loginPhone), Request.Form("reg_phone").ToString) Then
                tishiMM = "您输入的手机号已被注册！"
                Exit Sub
            Else
                Try
                    ds = ws.CreateNewUser(getTok(loginPhone), Request.Form("reg_phone").ToString, Request.Form("reg_pws").ToString, Request.Form("reg_comname").ToString, Request.Form("reg_hangye").ToString, "T" & Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & "_", Request.Form("reg_Clients").ToString, Request.Form("shiyongtianshu").ToString)

                Catch ex As Exception
                    Dim sEX As String = ex.ToString
                    ds = Nothing
                End Try

                If ds.Tables(0).Rows.Count > 0 Then
                    ' Me.lbl_result.Text = "注册成功，欢迎登录"
                    tishiMM = "注册成功，欢迎登录。您的公司号为：" & ds.Tables(0).Rows(0).Item("loginName").ToString & "，员工号为：" & ds.Tables(0).Rows(0).Item("subUserID").ToString & "，您可以使用您刚才注册的手机号 " & ds.Tables(0).Rows(0).Item("regPhone").ToString & " 或者员工号进行登录。"

                    bangdingDL(loginPhone, Request.Form("reg_phone").ToString, ds.Tables(0).Rows(0).Item("loginName").ToString)
                End If
            End If
        End If


    End Sub

    Private Function getTok(ByVal sTokenKey As String) As String
        Try
            Dim sName As String = sTokenKey
            'Dim sPWS As String = Split(sToken, ",").GetValue(1).ToString

            Dim sT As String = ""
            Dim i As Integer
            For i = 0 To sName.Length - 1
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            Next

            Dim sTR As String = "A"
            For i = 0 To sT.Length.ToString - 1
                If i Mod 7 = 0 Then
                    sTR = sTR & sT.Substring(i, 1)
                End If
            Next

            Return sName & "," & sTR
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Sub btnBangding_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBangding.Click
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            loginPhone = dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString
        End If
        If loginPhone = Request.Form("reg_bdUserID").ToString Then
            tishiMM = "代理商不能绑定自己。"
            Exit Sub
        End If

        bangdingDL(loginPhone, Request.Form("reg_bdPhone").ToString, Request.Form("reg_bdUserID").ToString)
        tishiMM = "绑定成功，请到客户列表中查看。"

    End Sub


    Protected Sub btnExitLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitLogin.Click
        Session.Abandon()
        Response.Cookies("myLoginName").Expires = Now.AddDays(-1)
        Response.Redirect("default.aspx")
    End Sub

End Class
