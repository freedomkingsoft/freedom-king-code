﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mycenter.aspx.vb" Inherits="mycenter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>用户中心</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />
</head>
<body style="padding-top: 50px; overflow-x: hidden;">
    <div class="fixed_topw">
        <div class="self_info">
            <img src="images/<%=dlLogoPic %>" alt="logo" /></div>
        <ul>
            <li>
                <h1>
                    <a href="default.aspx">
                        <%=Session("QiYeName")%>
                    </a>
                </h1>
            </li>
        </ul>
    </div>
    <div id="box" class="container stage-setting">
        <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
    <div class="title-group">
      <h1 class="title txt-cut">用户中心</h1>
    </div>
  </header>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
            <h3 class="title mct-b txt-xs" data-node="gTitle">
            </h3>
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="boxId_1589690143613_3">
                    <a href="#" class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="mct-a ">个人资料</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span></a>
                </div>
            </div>
        </div>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
        </div>
        <form runat="server" id="shenhe" method="post">
            <div class="card11 card-combine" data-node="group" id="Div14">
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div15">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut" style="text-align: right">
                                <span class="mct-a "><a href="Shenqing.aspx">申请成为代理商 </a></span>
                                 <span class="mct-a "><a href="dlCenter.aspx"> 代理商管理入口</a></span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">
                                <i class="icon-font icon-font-arrow-right txt-s"> </i></span>
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div3">
                <h3 class="title mct-c txt-xs" data-node="gTitle">
                    用户基本资料
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div4">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span16" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    当前登录手机： </span><span class="mct-a" style="text-align: right">
                                        <%=loginPhone %>
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><span data-node="arrow" class="plus plus-s">
                                >><i class="icon-font icon-font-arrow-right txt-s"> </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="shenheren" class="mct-a " style="width: 30%; display: -moz-inline-box;
                                    display: inline-block;">公司名称： </span><span class="mct-a" style="text-align: right">
                                        <input name="newComName" id="TextBox4" value="<%=comName %>" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">
                                <asp:Button ID="btnUpdateComName" runat="server" Text="修改" /><i class="icon-font icon-font-arrow-right txt-s">
                                </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span1" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    注册手机号： </span><span class="mct-a" style="text-align: right">
                                        <%=regPhone %>
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span15" class="mct-a " style="width: 30%; display: -moz-inline-box;
                                    display: inline-block;">审核签名： </span><span class="mct-a" style="text-align: right">
                                        <input name="SheHeSign" id="Text2" value="<%=LoginSign %>" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">
                                <asp:Button ID="btnUpdateSign" runat="server" Text="修改" /><i class="icon-font icon-font-arrow-right txt-s">
                                </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span2" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    注册日期： </span><span class="mct-a" style="text-align: right">
                                        <%=regDate %>
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span3" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    到期日期： </span><span class="mct-a" style="text-align: right">
                                        <%=VIPMaturitydate %>
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span6" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    公司ID： </span><span class="mct-a" style="text-align: right">
                                        <%=loginName %>
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span7" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    员工ID： </span><span class="mct-a" style="text-align: right">
                                        <%=subUserID %>
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span4" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    eMail： </span><span class="mct-a" style="text-align: right">
                                        <input name="newEmail" id="Text1" value="<%=eMail %>" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">
                                <asp:Button ID="btnUpdateEmail" runat="server" Text="修改" /></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div1">
                <h3 class="title mct-c txt-xs" data-node="gTitle">
                    修改密码
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div9">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span5" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请输入旧密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="oldPWS" id="oldPWS" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span8" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请输入新密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="newPWS1" id="Text3" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span9" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请确认新密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="newPWS2" id="Text4" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span10" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    <%=tishiMM %>
                                </span><span class="mct-b" style="text-align: right">
                                    <asp:Button ID="btnUpdatePWS" runat="server" Text="修改密码" Style="right: 12px" />
                                </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div10" style="display: <%=HideAdmin %>">
                <h3 class="title mct-c txt-xs" data-node="gTitle">
                    增加手机账号
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div11">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span11" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请输入手机号： </span><span class="mct-a" style="text-align: right">
                                        <input name="newPhone" id="newPhone" type="text" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                         <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span18" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请输入审核签名： </span><span class="mct-a" style="text-align: right">
                                        <input name="LoginSign" id="Text5" type="text" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span12" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请输密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="newPhonePWS1" id="Text6" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span13" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请确认密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="newPhonePWS2" id="Text7" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span14" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    <%=tishiPhone%>
                                </span><span class="mct-b" style="text-align: right">
                                    <asp:Button ID="btnAddPhone" runat="server" Text="增加手机用户" Style="right: 12px" />
                                </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div12" style="display: <%=HideAdmin %>">
                <h3 class="title mct-c txt-xs" data-node="gTitle">
                    管理手机账号
                </h3>
                <div data-node="cardList" class="card-list">
                    <%--<div class="card card4 line-around" id="Div14">
                        <a href="#" class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a ">个人资料</span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span></a>
                    </div>--%>
                    <div class="card card4 line-around" id="Div13">
                        <%=itemPhoneList %>
                        <%--<div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span15" class="mct-a " style="width: 15%; display: -moz-inline-box; display: inline-block;">
                                    手机号</span><span class="mct-a" style="text-align: right">
                                        <%=loginPhone %>
                                        子账号<%=subUserID%>
                                        <a href="#" data-act-type="hover">重置密码</a> <a href="#" data-act-type="hover">删除</a>
                                    </span>
                            </div>
                            
                        </div>--%>
                        <%--<div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span18" class="mct-a " style="width: 15%; display: -moz-inline-box; display: inline-block;">
                                    手机号</span><span class="mct-a" style="text-align: right">
                                        <%=loginPhone %>
                                        子账号<%=subUserID%>
                                        <a href="#" data-act-type="hover">重置密码</a> <a href="#" data-act-type="hover">删除</a>
                                    </span>
                            </div>
                            
                        </div>--%>
                    </div>
                </div>
            </div>
            <div id="Div2">
            </div>
            <div class="layout-box" data-act-type="hover">
                <div class="box-col txt-cut">
                    <span id="Span17" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                        <%=tishiMM %>
                    </span><span class="mct-b" style="text-align: right">
                        <asp:Button ID="btnExitLogin" runat="server" Text="退出当前登录" />
                    </span>
                </div>
                <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                </i></span>
            </div>
        </form>
        <div class="card card6" id="boxId_1589690143613_13">
            <a href="#top" data-act-type="hover" class=" btn-red">返回顶部 </a>
        </div>
        <div id="boxId_1589690143613_14">
        </div>
        <div class="card card6" id="Div5">
            <a href="#top" data-act-type="hover" class=" btn-red"></a>
        </div>
        <div id="Div6">
        </div>
    </div>
    <div class="footer">
            <ul>
                <li class="active"><a href="main.aspx?dlid=<%=dlid %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\home.png" /></i></p>
                    <h4>
                        首页</h4>
                </a></li>
                <li><a href="download.aspx?dlid=<%=dlid %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\down.png" /></i></p>
                    <h4>
                        下载</h4>
                </a></li>
                <li><a href="help.aspx?dlid=<%=dlid %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\help.png" /></i></p>
                    <h4>
                        帮助</h4>
                </a></li>
                <li><a href="mycenter.aspx?dlid=<%=dlid %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\mine.png" /></i></p>
                    <h4>
                        我的</h4>
                </a></li>
            </ul>
        </div>
</body>
</html>
