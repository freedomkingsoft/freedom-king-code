﻿
Partial Class dlChongZhi
    Inherits System.Web.UI.Page

    Public dsLoginInfo As Data.DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            getClientList(dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString)
        End If
    End Sub


    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Public templeteTable As String
    'Public itemtemplate As Data.DataSet

    Private Sub getClientList(ByVal dlPhone As String)
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)

        Dim dsClientList As Data.DataSet

        Try
            Dim sSQL As String = "select * from dlshouzhishenqing where dlphone='" & dlPhone & "' order by shijian desc"
            dsClientList = mdb.Reader(sSQL)

            clientList.DataSource = dsClientList
            clientList.DataBind()
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Public tishiMM As String

    Protected Sub btnChongZhi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChongZhi.Click

        Dim nJinE As Decimal
        nJinE = Request.Form("chzhJinE")
        If IsNumeric(nJinE) Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim sSQL As String
            sSQL = "INSERT INTO [dlShouZhiShenQing]([shijian],[dlPhone],[jiefang],[daifang],[shuoming]) VALUES "
            sSQL = sSQL & "('" & Now.ToString & "','" & dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString & "'," & nJinE & ",0,'代理商充值')"
            mdb.Write(sSQL)
            tishiMM = "充值成功,请等待审核通过。"
        Else
            tishiMM = "充值金额必须为数值，请重新提交"
        End If
    End Sub
End Class