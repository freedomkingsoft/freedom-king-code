﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="feedback.aspx.vb" Inherits="feedback" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Cache-Control" content="no-cache">
<meta name="keywords" content="手机新浪网,新浪首页,新闻资讯,新浪新闻，新浪无线">
<meta name="description" content="手机新浪网是新浪网的手机门户网站，为亿万用户打造一个手机联通世界的超级平台，提供24小时全面及时的中文资讯，内容覆盖国内外突发新闻事件、体坛赛事、娱乐时尚、产业资讯、实用信息等。手机新浪网iphone版 - sina.cn">
<meta content="width=device-width; initial-scale=1.0; minimum-scale=1.0; maximum-scale=1.0" name="viewport" id="viewport">
<link rel="apple-touch-icon" href="http://u1.sinaimg.cn/3g/image/upload/0/110/176/19509/64477c90.png?pos=108&amp;vt=4">
<title>手机新浪网留言板</title>
<link rel="stylesheet" type="text/css" href="http://mjs.sinaimg.cn/wap/dpool/sports/client/201311071535/style.css">
<link rel="stylesheet" type="text/css" href="http://mjs.sinaimg.cn/wap/dpool/public/201310112050/css/common.min.css">
<link rel="stylesheet" type="text/css" href="http://mjs.sinaimg.cn/wap/dpool/public/201309121800/css/blue2.css">

<!-- <script src="http://u1.sinaimg.cn/upload/2012/09/12/messagev2vt4/js/lyb_f.js"></script> -->
<meta name="keywords" content="手机新浪网,新浪首页,新闻资讯,新浪新闻，新浪无线">
<meta name="description" content="手机新浪网是新浪网的手机门户网站，为亿万用户打造一个手机联通世界的超级平台，提供24小时全面及时的中文资讯，内容覆盖国内外突发新闻事件、体坛赛事、娱乐时尚、产业资讯、实用信息等。手机新浪网触屏版 - sina.cn">

<script type="text/javascript">
//公共全局配置文件
var globalConfig = {

    startTime   :   new Date().getTime()    //页面开始渲染时间 ， 目前应用于：日志统计、性能统计。

}
</script>
</head>
<body>

	<header id="mainpage">
      <div id="j_artcileMain" class="articleHead">
		<div data-sudaclick="mainNav" class="mainNav">
			<a class="gIcon sinaLogo" href="http://sina.cn"></a>
			<span class="navLine"></span>
			<a class="gIcon levelOne">新浪邮箱留言板</a>
			<a class="gIcon navBtn" href="http://news.sina.cn/?sa=t124d8889597v84">网站导航</a>
		</div>
      </div>            
	</header>
	<!--页头主体-->
	<div>
		
			<div class="w_login" style="margin-bottom: 5px;">赵建涛
			<div data-sudaclick="login_yes" class="w_loginmate_r">
				<a class="w_login_2" href="http://passport.sina.cn/sso/logout?entry=wapsso&amp;r=http%3A%2F%2Fdp.sina.cn%2Fdpool%2Fmessagev2%2Findex.php%3Fboardid%3D94&amp;backTitle=%CA%D6%BB%FA%D0%C2%C0%CB%CD%F8%C1%F4%D1%D4%B0%E5">退出</a>
			</div>
		</div>
						
		
		
		
		
	</div>
    <div class="conttext">
        <div class="bt">
        			<strong>请您留言</strong>
        		</div>
        <p></p>
        <form method="post" action="addmsg.php" onsubmit="return checkForm();">
        <input type="hidden" name="boardid" value="94">
        <input type="hidden" name="uid" value="2368840273">
        <input type="hidden" name="referer" value="">
		
        		<div class="ly_box">
			<p><strong>留言类型</strong> <select name="msgtype">
                <option value="0">请选择</option>
                                <option value="0">其他</option>
                                <option value="1">投诉</option>
                                <option value="2">建议</option>
                                <option value="3">报错</option>
                                <option value="4">支持</option>
                                </select></p>
			<p><strong>留言内容<font class="f_red">*</font></strong></p>
			             <p><textarea class="inputtextarea" id="text_ctn" name="msg1"></textarea></p>
            </div>
		
        <div class="ly_box">
			<p><strong>手机号码</strong></p>
			<p><input type="text" class="inputtext" id="msg2" name="msg2" value=""></p>
			<p><strong>手机机型/浏览器信息</strong></p>
			<p><input type="text" class="inputtext" id="msg3" name="msg3" value=""></p>
		</div>
        <input type="hidden" name="needcheck[]" value="text_ctn 留言内容">
        
		<p align="right">带<font class="f_red">*</font>为必填选项</p>
        <p id="errormsg" class="f_red" align="center"></p>
		<p class="btnbox"><input type="submit" value="发言" class="linkbtn linkbtn_grey" id="linkbtn"></p>
		</form>
        		<!--标题-->
		<div class="bt">
			<strong>联系我们</strong>
		</div>
		<span class="ly_box" style="border:0;background:none;">
        客服热线: <a href="tel:4006900000" title="">4006900000</a>（仅收市话费）<br>
联系邮箱: sinacnfk#staff.sina.com.cn (请把#号替换为@)<br>
        </span>
        

<script language="javascript">

    function checkForm()
    {
        var needcheck = document.getElementsByName('needcheck[]');
        var errormsg =  document.getElementById('errormsg');
        for(var i=0; i < needcheck.length; i++)
        {
            tmparr = needcheck[i].value.split(' ');
            tmpid = tmparr[0];
            if (document.getElementById(tmpid).alt != 'y') continue;
            if (document.getElementById(tmpid).value == '')
            {
                errormsg.innerHTML = '对不起,'+tmparr[1]+'不能为空';
                return false;
            }
        }
        return true;
    }

    function bind_link(value)
    {
        var obj = document.getElementById('msg2');
        if (obj.value == value)
        {
            obj.value = '';
        }
        return true;
    }

    function bind_intro(value)
    {
        var obj = document.getElementById('msg3');
        if (obj.value == value)
        {
            obj.value = '';
        }
        return true;
    }
</script>

    
<div id="eject_overlay" class="eject_overlay_hidden"> </div>
<div class="ejectlistbox ejectlistbox_hidden" id="ejectlistbox">
<div class="clint_top"><strong>手机新浪网导航</strong><a href="#" title="关闭" class="btnlink" id="btnlink">关闭</a></div>
<ul class="navigation" id="navigation"></ul>
</div>
</div>
<footer>
		<div class="footer">
			<span class="j_backHistory gIcon backPrevPage"></span>
			<span class="j_backTop gIcon backTop"></span>

			<a href="http://sina.cn" class="linkico" style=" margin: 0;">报表中心</a>
			<!--a href="http://news.sina.cn/?sa=t124d8889597v84" class="linkico" margin:="" 0;"="">导航</a-->
			<a href="http://site.proc.sina.cn/search/index.php" class="linkico" margin:="" 0;"="">用户中心</a>

		</div>

</footer>
<script src="http://mjs.sinaimg.cn/wap/public/basejs/201309231200/zepto.js"></script>
<script src="http://mjs.sinaimg.cn/wap/public/header_footer/201310161100/header.footer.min.js"></script>
<!-- <script src="http://u1.sinaimg.cn/upload/2012/09/24/19563.js"></script> -->

<script type="text/javascript">
//SUDA日志统计
var sudaLogConfig = {
    
uId : '', // 用户uid，如果用户没登陆，可以为空，去哪儿合作方不用填
    url : '',
    //性能统计, 如果没有布置suda_map可省, prevPageClickTime 表示上页点击链接开始请求的时间。
    prevPageClickTime : ''  
    
};
</script>

<script src="http://mjs.sinaimg.cn/wap/public/suda/201501281600/suda_log.min.js"></script>

</body>
</html>
