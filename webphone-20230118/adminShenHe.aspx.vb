﻿
Partial Class adminShenHe
    Inherits System.Web.UI.Page

    Public dsLoginInfo As Data.DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            If dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString.Replace(" ", "") = "13091288796" Then
                getClientList(dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString)
                getShouZhi(dsLoginInfo.Tables(0).Rows(0).Item("LoginPhone").ToString)
            Else
                Response.Redirect("Default.aspx")
            End If
        End If
    End Sub


    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Public templeteTable As String
    'Public itemtemplate As Data.DataSet

    Private Sub getClientList(ByVal dlPhone As String)
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)

        Dim dsClientList As Data.DataSet

        Try
            Dim sSQL As String = "select * from shenqing where ISNULL(jindu,1) <> '通过'"
            dsClientList = mdb.Reader(sSQL)

            clientList.DataSource = dsClientList
            clientList.DataBind()
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Sub getShouZhi(ByVal dlPhone As String)
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)

        Dim dsShouZhi As Data.DataSet

        Try
            Dim sSQL As String = "select * from dlShouZhiShenQing  where ISNULL(shenhe,1) <>'审核通过'"
            dsShouZhi = mdb.Reader(sSQL)

            dlShouZhi.DataSource = dsShouZhi
            dlShouZhi.DataBind()
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

End Class
