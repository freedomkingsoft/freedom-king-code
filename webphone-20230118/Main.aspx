﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main.aspx.vb" Inherits="Main" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="http://u1.sinaimg.cn/upload/h5/img/apple-touch-icon.png">
    <title>报表中心</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />
</head>
<body style="padding-top: 50px; overflow-x: hidden;">
    <form id="Form1" runat="server">
        <div class="fixed_topw">
            <div class="self_info">
                <img src="images/<%=dlLogoPic%>" alt="logo" /></div>
            <ul>
                <li>
                    <h1>
                        <a href="#">
                            <%=sQiYeName%>
                        </a>
                    </h1>
                </li>
            </ul>
        </div>
        <div id="box" class="container stage-setting">
            <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
      <div class="title-group">
        <h1 class="title txt-cut"> 我的报表</h1>
      </div>
    </header>
            <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
                <h3 class="title mct-b txt-xs" data-node="gTitle">
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="boxId_1589690143613_3">
                        <a href="/users/2368840273?set=1" class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a "></span>此段不显示
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span></a>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_4">
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="boxId_1589690143613_5">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a ">搜索编号：</span> <span class="mct-a">
                                    <input type="text" name="bianhao" style="width: 10rem; font-size: 1.1rem" /></span>
                                <span class="mct-a">
                                    <asp:Button ID="btnSearch" Text="搜 索" runat="server" Style="font-size: 1.1rem" /></span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div1">
                <asp:Repeater ID="dsMain" runat="server">
                    <ItemTemplate>
                        <div data-node="cardList" class="card-list">
                            <div class="homereport">
                                <a href="Baobiao.aspx?id=8&name=销售及出库流水">
                                    <div class="homereport-left">
                                        <p class="p-top">
                                            近7天销售</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("sr7")%>
                                            元</p>
                                    </div>
                                </a><a href="Baobiao.aspx?id=8&name=销售及出库流水">
                                    <div class="homereport-right">
                                        <p class="p-top">
                                            近1个月销售</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("sr30")%>
                                            元</p>
                                    </div>
                                </a>
                            </div>
                            <div class="homereport">
                                <a href="BaoBiao.aspx?id=1&name=资金明细账">
                                    <div class="homereport-left">
                                        <p class="p-top">
                                            近7天收款</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("sk7")%>
                                            元</p>
                                    </div>
                                </a><a href="BaoBiao.aspx?id=1&name=资金明细账">
                                    <div class="homereport-right">
                                        <p class="p-top">
                                            近1个月收款</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("sk30")%>
                                            元</p>
                                    </div>
                                </a>
                            </div>
                            <div class="homereport">
                                <a href="BaoBiao.aspx?id=9&name=采购及入库流水">
                                    <div class="homereport-left">
                                        <p class="p-top">
                                            近7天采购</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("cg7")%>
                                            元</p>
                                    </div>
                                </a><a href="BaoBiao.aspx?id=9&name=采购及入库流水">
                                    <div class="homereport-right">
                                        <p class="p-top">
                                            近1个月采购</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("cg30")%>
                                            元</p>
                                    </div>
                                </a>
                            </div>
                            <div class="homereport">
                                <a href="BaoBiao.aspx?id=1&name=资金明细账">
                                    <div class="homereport-left">
                                        <p class="p-top">
                                            近7天付款</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("fk7")%>
                                            元</p>
                                    </div>
                                </a><a href="BaoBiao.aspx?id=1&name=资金明细账">
                                    <div class="homereport-right">
                                        <p class="p-top">
                                            近1个月付款</p>
                                        <p class="p-bottom">
                                            <%#Container.DataItem("fk30")%>
                                            元</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
                <asp:Repeater ID="listbaobiao" runat="server">
                    <ItemTemplate>
                        <div data-node="cardList" class="card-list">
                            <div class="card card4 line-around" id="boxId_1589690143613_9">
                                <a href="BaoBiao.aspx?id=<%#Container.DataItem("ID")%>&name=<%#Container.DataItem("ConText")%>"
                                    class="layout-box" data-act-type="hover">
                                    <div class="box-col txt-cut">
                                        <span class="mct-a ">
                                            <%#Container.DataItem("ConText")%>
                                        </span><span class="mct-b ">
                                            <%#Container.DataItem("shuoming")%>
                                        </span>
                                    </div>
                                    <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                                        》</i> </span></a>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_10">
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="boxId_1589690143613_11">
                        <a href="<%=dlWebSite%>" class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a ">官网首页</span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span></a>
                    </div>
                    <div class="card card4 line-around" id="boxId_1589690143613_12">
                        <a href="<%=dlFeedback%>" class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span class="mct-a ">意见反馈</span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span></a>
                    </div>
                </div>
            </div>
            <div class="card card6" id="boxId_1589690143613_13">
                <a href="#box" data-act-type="hover" class=" btn-red">返回顶部 </a>
            </div>
            <div id="boxId_1589690143613_14">
            </div>
        </div>
        <div class="footer">
            <ul>
                <li class="active"><a href="main.aspx?dlid=<%=dlid %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\home.png" /></i></p>
                    <h4>
                        首页</h4>
                </a></li>
                <li><a href="<%=dlDownload %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\down.png" /></i></p>
                    <h4>
                        下载</h4>
                </a></li>
                <li><a href="<%=dlHelp %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\help.png" /></i></p>
                    <h4>
                        帮助</h4>
                </a></li>
                <li><a href="mycenter.aspx?dlid=<%=dlid %>">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\mine.png" /></i></p>
                    <h4>
                        我的</h4>
                </a></li>
            </ul>
        </div>
    </form>
</body>
</html>
