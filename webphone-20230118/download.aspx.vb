﻿
Partial Class download
    Inherits System.Web.UI.Page

    Public sQiYeName As String
    Dim dsLoginInfo As Data.DataSet

    Public dlTitle As String
    Public dlName As String
    Public dlHomePic As String
    Public dlLogoPic As String
    Public dlLoginPic As String
    Public dlWebSite As String

    Public dlDownload As String
    Public dlHelp As String
    Public dlFeedback As String
    Public dlForget As String
    Public dlFileURL As String
    Public dlMyCenter As String

    Private sDL As DLInfo.dlInfo
    Public dlID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dlID = Request.Params.Item("dlid")

        If IsNothing(Session("DLInfo")) Then
            Dim sDLID As String = Request.Params.Item("dlid")
            sDL = New DLInfo.dlInfo(sDLID)
            'Response.Redirect("Default.aspx")
        Else
            sDL = Session("DLInfo")
        End If

        dlTitle = sDL.dlTitle
        dlName = sDL.dlName
        dlHomePic = sDL.dlHomePic
        dlLogoPic = sDL.dlLogoPic
        dlLoginPic = sDL.dlLoginPic
        dlWebSite = sDL.dlWebSite

        dlDownload = sDL.dlDownload
        dlHelp = sDL.dlHelp
        dlFeedback = sDL.dlFeedback
        dlForget = sDL.dlForget
        dlMyCenter = sDL.dlMyCenter

        dlFileURL = sDL.dlFileURL
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        dsLoginInfo = Session("LoginInfo")
    End Sub


End Class
