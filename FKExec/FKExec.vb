Public Class FKExec

    Dim sConnect As String
    Dim mdb As MDBReadWrite.MDBReadWrite

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        mdb = New MDBReadWrite.MDBReadWrite(sConnect)
        'Dim dlg As New System.Windows.Forms.OpenFileDialog
        'dlg.InitialDirectory = My.Application.Info.DirectoryPath & "\FKData"
        'If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    sConnect = sConnect.Replace(My.Application.Info.DirectoryPath & "\FKData\FreedomKingData.mdb", Me.txtName.Text)
        'End If
        Me.txtName.Text = "数据库连接完成，可以输入语句"
    End Sub

    Private Sub FKExec_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        sConnect = FKG.myselfG.asasW
        'sConnect = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\FreedomKing\FreedomKingData.mdb;Jet OLEDB:Database Password='Zhao04Dan03Tong09'"
    End Sub

    Private Sub btnRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRun.Click
        If Me.txtSQL.Text.Replace(" ", "") = "" Then
            Exit Sub
        End If

        Try
            Do While Me.txtSQL.Text.Substring(0, 1) = " "
                Me.txtSQL.Text = Me.txtSQL.Text.Remove(0, 1)
            Loop

            If Me.txtSQL.Text.StartsWith("Select", StringComparison.CurrentCultureIgnoreCase) Then
                If FKG.myselfG.sBanBen = "试用版" OrElse FKG.myselfG.sVersion = "2.0" OrElse FKG.myselfG.sBanBen = "正式版" OrElse FKG.myselfG.sBanBen <> "开发版" Then
                    MsgBox("无权运行此语句")
                Else
                    mdb.DataBind(Me.dg, Me.txtSQL.Text)
                End If

            Else
                mdb.Write(Me.txtSQL.Text)
                MsgBox("OK")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub
End Class
