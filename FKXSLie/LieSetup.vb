﻿Imports System.Windows.Forms

Public Class LieSetup
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub fillLie()

        If LieSetup.Length < 4 Then
            Dim i As Integer
            For i = 0 To dg.ColumnCount - 1
                'If LieSetup(i * 4) = 2 Then
                'Else
                If dg.Columns(i).Visible Then
                    Me.dgSetup.Rows.Add()
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(0).Value = True
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(1).Value = dg.Columns(i).HeaderText
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(2).Value = Math.Round(dg.Columns(i).Width / dg.Width * 100)
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(3).Value = dg.Columns(i).DefaultCellStyle.Format
                Else
                    Me.dgSetup.Rows.Add()
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(0).Value = False
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(1).Value = dg.Columns(i).HeaderText
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(2).Value = Math.Round(dg.Columns(i).Width / dg.Width * 100)
                    Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(3).Value = dg.Columns(i).DefaultCellStyle.Format
                    'End If
                End If
            Next

        Else
            Dim i As Integer
            For i = 0 To dg.ColumnCount - 1
                If LieSetup(i * 4) = 2 Then
                Else
                    If dg.Columns(i).Visible Then
                        Me.dgSetup.Rows.Add()
                        Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(0).Value = True
                        Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(1).Value = dg.Columns(i).HeaderText
                        Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(2).Value = Math.Round(dg.Columns(i).Width / dg.Width * 100)
                        Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(3).Value = dg.Columns(i).DefaultCellStyle.Format
                    Else
                        If bXSHide Then
                            Me.dgSetup.Rows.Add()
                            Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(0).Value = False
                            Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(1).Value = dg.Columns(i).HeaderText
                            Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(2).Value = Math.Round(dg.Columns(i).Width / dg.Width * 100)
                            Me.dgSetup.Rows(Me.dgSetup.RowCount - 1).Cells(3).Value = dg.Columns(i).DefaultCellStyle.Format
                        End If
                    End If
                End If
            Next
        End If
       
    End Sub


    Private Sub dgSetup_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgSetup.CellClick
        If e.ColumnIndex = 0 Then
            Me.dgSetup.Rows(e.RowIndex).Cells(0).Value = Not Me.dgSetup.Rows(e.RowIndex).Cells(0).Value
            'dg.Columns(Me.dgSetup.Rows(e.RowIndex).Cells(1).Value.ToString).Visible = Me.dgSetup.Rows(e.RowIndex).Cells(0).Value
            dg.Columns(e.RowIndex).Visible = Me.dgSetup.Rows(e.RowIndex).Cells(0).Value

            Me.Refresh()
        End If
        'MsgBox(dg.ColumnCount)
    End Sub

    Dim dg As DataGridView
    Dim bXSHide As Boolean
    Public sSave As String = ""
    Dim sMoren As String

    Public bSaveLie As Boolean = False

    Dim LieSetup() As Object
    Public Sub New(ByRef dgMain As DataGridView, ByVal bXSAll As Boolean, ByVal bSave As Boolean, ByVal sMorenLieSetup As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        'sMorenLieSetup的格式设定为: 是否显示,列名,列宽,小数位数,... 如(1,科目,12,2),其中列宽以百分比为单位.

        dg = dgMain '传过来Datagrid的地址
        bXSHide = bXSAll '是否可以控制默认不显示的列，比如余额表中的 数量
        bSaveLie = bSave '是否显示"保存为默认值按钮“
        LieSetup = Split(sMorenLieSetup, ",")
        'sMoren = sMorenLie '限制列，对应列的值如果为2，则不进行任何处理
    End Sub

    Private Sub LieSetup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        fillLie()
        If bSaveLie = False Then
            Me.btnSave.Visible = False
        End If
        Me.Icon = FKG.myselfG.FKIcon
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim i As Integer
        Dim n As Integer = 0
        For i = 0 To dg.Columns.Count - 1

            If LieSetup.Length < 4 Then

                sSave = sSave & CType(Me.dgSetup.Rows(i).Cells(0).Value, Integer) & "," & Me.dgSetup.Rows(i).Cells(1).Value & "," & Me.dgSetup.Rows(i).Cells(2).Value & "," & Me.dgSetup.Rows(i).Cells(3).Value & ","
            Else
                If LieSetup(i * 4) = 2 Then
                    sSave = sSave & "2,,,,"
                Else
                    sSave = sSave & CType(Me.dgSetup.Rows(n).Cells(0).Value, Integer) & "," & Me.dgSetup.Rows(n).Cells(1).Value & "," & Me.dgSetup.Rows(n).Cells(2).Value & "," & Me.dgSetup.Rows(n).Cells(3).Value & ","
                    n = n + 1
                End If
            End If
        Next
        sSave = sSave.Remove(sSave.Length - 1, 1)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub dgSetup_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgSetup.CellEndEdit
        Select Case e.ColumnIndex
            Case 1
                dg.Columns(e.RowIndex).HeaderText = dgSetup.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            Case 2
                dg.Columns(e.RowIndex).Width = dgSetup.Rows(e.RowIndex).Cells(e.ColumnIndex).Value / 100 * dg.Width
            Case 3
                dg.Columns(e.RowIndex).DefaultCellStyle.Format = dgSetup.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
        End Select
    End Sub

    Public Function SetLie(ByVal bXSAllSL As Boolean) As Boolean
        'LieSetup = Split(sMorenLieSetup, ",")
        If LieSetup.Length < 4 Then
            Return False
        Else
            Dim i As Integer
            For i = 0 To dg.ColumnCount - 1
                If LieSetup(i * 4) = 2 Then
                    dg.Columns(i).Visible = False
                Else
                    If bXSAllSL Then
                        dg.Columns(i).Visible = True
                    Else
                        dg.Columns(i).Visible = LieSetup(i * 4)
                    End If
                End If
                If LieSetup(4 * i + 1).ToString = "" Then
                Else
                    dg.Columns(i).HeaderText = LieSetup(i * 4 + 1).ToString
                End If
                If LieSetup(i * 4 + 2) = "" Then
                Else
                    dg.Columns(i).Width = LieSetup(i * 4 + 2) / 100 * dg.Width
                End If
                If LieSetup(i * 4 + 3) = "" Then
                Else
                    dg.Columns(i).DefaultCellStyle.Format = LieSetup(i * 4 + 3)
                End If
            Next
            Return True
        End If
    End Function

End Class
