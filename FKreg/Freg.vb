Public Class Freg

   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoRu.Click
        Dim dlg As New OpenFileDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            'IO.File.Copy(dlg.FileName, Application.StartupPath & "\Pub.Key", True)
            Dim sKey As String
            Dim streamReader As New IO.StreamReader(dlg.FileName, True)
            sKey = streamReader.ReadToEnd
            streamReader.Close()

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            If mdb.Reader("select * from sysTemp where sComputerName='" & Me.MCO(CPUID, HID) & "'").Tables(0).Rows.Count > 0 Then
                mdb.Write("Update systemp set sPubKey='" & sKey & "' where sComputerName='" & Me.MCO(CPUID, HID) & "'")
            Else
                mdb.Write("insert into systemp (sPubKey,sComputerName) values ('" & sKey & "','" & Me.MCO(CPUID, HID) & "')")
            End If

            Me.TabControl1.TabPages.Remove(Me.tpRCode)
            Me.TabControl1.TabPages.Add(Me.tpDone)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Me.Close()
    End Sub

    Private Sub tpMCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpMCode.Enter
        Dim sMC As String
        sMC = Me.MCO(CPUID, HID)
        Me.TextBox1.Text = sMC.Substring(0, 6)
        Me.TextBox2.Text = sMC.Substring(6, 6)
        Me.TextBox3.Text = sMC.Substring(12, 6)
        Me.TextBox4.Text = sMC.Substring(18, 6)
        Me.TextBox5.Text = sMC.Substring(24, 6)
        Me.TextBox6.Text = sMC.Substring(30, 6)
    End Sub

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet
        Dim cpu

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    Private Function HID() As String
        Dim HDid As String = ""
        Try
            Dim cimobject As New Management.ManagementClass("win32_diskdrive")
            Dim moc As Management.ManagementObjectCollection
            moc = cimobject.GetInstances()
            Dim mo As Management.ManagementObject

            For Each mo In moc
                'HDid = HDid & mo.Properties("signature").Value.ToString
                HDid = HDid & mo.Properties("PNPDeviceID").Value.ToString
                HDid = HDid.Replace("_", "")
                HDid = HDid.Remove(0, mo.Properties("Caption").Value.ToString.Length)
                Exit For
            Next
        Catch ex As Exception
            HDid = ""
        End Try

        Return HDid

    End Function

    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 36
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            sCDO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        End If

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
            Next
        End If


        FKG.myselfG.sVersion = My.Application.Info.Version.ToString

        'FKG.myselfG.sBanBen = "试用版"
        FKG.myselfG.sBanBen = "开发版"
        'FKG.myselfG.sBanBen = "正式版"
        'FKG.myselfG.sBanBen = ""

        FKG.myselfG.sClient = "服务器端"
        'FKG.myselfG.sClient = "客户端"

        Return sMCO & FKG.myselfG.sVersion & FKG.myselfG.sBanBen & FKG.myselfG.sClient

    End Function

    Private Sub Freg_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.TabControl1.TabPages.Remove(Me.tpDone)
        Me.TabControl1.TabPages.Remove(Me.tpMCode)
        Me.TabControl1.TabPages.Remove(Me.tpRCode)
        Me.TabControl1.TabPages.Remove(Me.tpXieYi)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Me.TabControl1.TabPages.Remove(Me.tpWelCome)
        Me.TabControl1.TabPages.Add(Me.tpXieYi)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPre2.Click
        Me.TabControl1.TabPages.Remove(Me.tpXieYi)
        Me.TabControl1.TabPages.Add(Me.tpWelCome)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgree.Click
        Me.TabControl1.TabPages.Remove(Me.tpXieYi)
        Me.TabControl1.TabPages.Add(Me.tpMCode)
        Me.tpMCode_Click(sender, e)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel2.Click
        Me.Close()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenFile.Click
        Dim dlg As New System.Windows.Forms.SaveFileDialog
        dlg.DefaultExt = ".txt"
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim sFile As String = dlg.FileName
            'IO.File.Create(sFile)
            IO.File.Delete(sFile)
            IO.File.AppendAllText(sFile, Me.TextBox1.Text & "_" & Me.TextBox2.Text & "_" & Me.TextBox3.Text & "_" & Me.TextBox4.Text & "_" & Me.TextBox5.Text & "_" & Me.TextBox6.Text & "_" & FKG.myselfG.sVersion & "_" & FKG.myselfG.sBanBen & "_" & FKG.myselfG.sClient)
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext3.Click
        Me.TabControl1.TabPages.Remove(Me.tpMCode)
        Me.TabControl1.TabPages.Add(Me.tpRCode)
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel3.Click
        Me.Close()
    End Sub
End Class
