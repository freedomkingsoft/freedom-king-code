﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Freg
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Freg))
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tpWelCome = New System.Windows.Forms.TabPage
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
        Me.tpXieYi = New System.Windows.Forms.TabPage
        Me.btnCancel2 = New System.Windows.Forms.Button
        Me.btnPre2 = New System.Windows.Forms.Button
        Me.btnAgree = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox
        Me.tpMCode = New System.Windows.Forms.TabPage
        Me.btnCancel3 = New System.Windows.Forms.Button
        Me.btnNext3 = New System.Windows.Forms.Button
        Me.btnGenFile = New System.Windows.Forms.Button
        Me.RichTextBox4 = New System.Windows.Forms.RichTextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tpRCode = New System.Windows.Forms.TabPage
        Me.RichTextBox5 = New System.Windows.Forms.RichTextBox
        Me.btnDaoRu = New System.Windows.Forms.Button
        Me.tpDone = New System.Windows.Forms.TabPage
        Me.RichTextBox6 = New System.Windows.Forms.RichTextBox
        Me.btnDone = New System.Windows.Forms.Button
        Me.TabControl1.SuspendLayout()
        Me.tpWelCome.SuspendLayout()
        Me.tpXieYi.SuspendLayout()
        Me.tpMCode.SuspendLayout()
        Me.tpRCode.SuspendLayout()
        Me.tpDone.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tpWelCome)
        Me.TabControl1.Controls.Add(Me.tpXieYi)
        Me.TabControl1.Controls.Add(Me.tpMCode)
        Me.TabControl1.Controls.Add(Me.tpRCode)
        Me.TabControl1.Controls.Add(Me.tpDone)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(777, 466)
        Me.TabControl1.TabIndex = 0
        '
        'tpWelCome
        '
        Me.tpWelCome.Controls.Add(Me.btnCancel)
        Me.tpWelCome.Controls.Add(Me.btnNext)
        Me.tpWelCome.Controls.Add(Me.RichTextBox2)
        Me.tpWelCome.Controls.Add(Me.RichTextBox1)
        Me.tpWelCome.Location = New System.Drawing.Point(4, 21)
        Me.tpWelCome.Name = "tpWelCome"
        Me.tpWelCome.Padding = New System.Windows.Forms.Padding(3)
        Me.tpWelCome.Size = New System.Drawing.Size(769, 441)
        Me.tpWelCome.TabIndex = 0
        Me.tpWelCome.Text = "欢迎使用"
        Me.tpWelCome.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(591, 380)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(104, 34)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "取消(&C)"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(446, 380)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(104, 34)
        Me.btnNext.TabIndex = 1
        Me.btnNext.Text = "下一步(&N)"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'RichTextBox2
        '
        Me.RichTextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox2.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.RichTextBox2.Location = New System.Drawing.Point(86, 111)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.ReadOnly = True
        Me.RichTextBox2.Size = New System.Drawing.Size(625, 231)
        Me.RichTextBox2.TabIndex = 0
        Me.RichTextBox2.Text = "单击""取消""按钮可以退出程序，或者单击""下一步""继续进行注册。" & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "警告:本计算机程序受版权法和国际条约保护。" & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "未经授权复制或散播本计算机程序或其中的一部分，将受" & _
            "到严厉的民事或刑事处罚，并将在法律许可的范围内受到最大可能的起诉。" & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "单价""下一步""继续注册。"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox1.Font = New System.Drawing.Font("楷体_GB2312", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.RichTextBox1.Location = New System.Drawing.Point(86, 37)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(625, 42)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = "欢迎使用自由王软件"
        '
        'tpXieYi
        '
        Me.tpXieYi.Controls.Add(Me.btnCancel2)
        Me.tpXieYi.Controls.Add(Me.btnPre2)
        Me.tpXieYi.Controls.Add(Me.btnAgree)
        Me.tpXieYi.Controls.Add(Me.Label6)
        Me.tpXieYi.Controls.Add(Me.RichTextBox3)
        Me.tpXieYi.Location = New System.Drawing.Point(4, 21)
        Me.tpXieYi.Name = "tpXieYi"
        Me.tpXieYi.Padding = New System.Windows.Forms.Padding(3)
        Me.tpXieYi.Size = New System.Drawing.Size(769, 441)
        Me.tpXieYi.TabIndex = 1
        Me.tpXieYi.Text = "软件许可协议"
        Me.tpXieYi.UseVisualStyleBackColor = True
        '
        'btnCancel2
        '
        Me.btnCancel2.Location = New System.Drawing.Point(624, 378)
        Me.btnCancel2.Name = "btnCancel2"
        Me.btnCancel2.Size = New System.Drawing.Size(104, 34)
        Me.btnCancel2.TabIndex = 3
        Me.btnCancel2.Text = "取消(&C)"
        Me.btnCancel2.UseVisualStyleBackColor = True
        '
        'btnPre2
        '
        Me.btnPre2.Location = New System.Drawing.Point(330, 378)
        Me.btnPre2.Name = "btnPre2"
        Me.btnPre2.Size = New System.Drawing.Size(104, 34)
        Me.btnPre2.TabIndex = 2
        Me.btnPre2.Text = "上一步(&P)"
        Me.btnPre2.UseVisualStyleBackColor = True
        '
        'btnAgree
        '
        Me.btnAgree.Location = New System.Drawing.Point(479, 378)
        Me.btnAgree.Name = "btnAgree"
        Me.btnAgree.Size = New System.Drawing.Size(104, 34)
        Me.btnAgree.TabIndex = 2
        Me.btnAgree.Text = "我同意(&I)"
        Me.btnAgree.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("黑体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label6.Location = New System.Drawing.Point(31, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 16)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "软件许可协议"
        '
        'RichTextBox3
        '
        Me.RichTextBox3.Location = New System.Drawing.Point(34, 60)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.ReadOnly = True
        Me.RichTextBox3.Size = New System.Drawing.Size(714, 293)
        Me.RichTextBox3.TabIndex = 0
        Me.RichTextBox3.Text = resources.GetString("RichTextBox3.Text")
        '
        'tpMCode
        '
        Me.tpMCode.Controls.Add(Me.btnCancel3)
        Me.tpMCode.Controls.Add(Me.btnNext3)
        Me.tpMCode.Controls.Add(Me.btnGenFile)
        Me.tpMCode.Controls.Add(Me.RichTextBox4)
        Me.tpMCode.Controls.Add(Me.Label5)
        Me.tpMCode.Controls.Add(Me.Label4)
        Me.tpMCode.Controls.Add(Me.Label3)
        Me.tpMCode.Controls.Add(Me.Label2)
        Me.tpMCode.Controls.Add(Me.TextBox6)
        Me.tpMCode.Controls.Add(Me.TextBox5)
        Me.tpMCode.Controls.Add(Me.TextBox4)
        Me.tpMCode.Controls.Add(Me.TextBox3)
        Me.tpMCode.Controls.Add(Me.TextBox2)
        Me.tpMCode.Controls.Add(Me.TextBox1)
        Me.tpMCode.Controls.Add(Me.Label1)
        Me.tpMCode.Location = New System.Drawing.Point(4, 21)
        Me.tpMCode.Name = "tpMCode"
        Me.tpMCode.Size = New System.Drawing.Size(769, 441)
        Me.tpMCode.TabIndex = 2
        Me.tpMCode.Text = "机器码"
        Me.tpMCode.UseVisualStyleBackColor = True
        '
        'btnCancel3
        '
        Me.btnCancel3.Location = New System.Drawing.Point(614, 338)
        Me.btnCancel3.Name = "btnCancel3"
        Me.btnCancel3.Size = New System.Drawing.Size(87, 29)
        Me.btnCancel3.TabIndex = 24
        Me.btnCancel3.Text = "取消(&C)"
        Me.btnCancel3.UseVisualStyleBackColor = True
        '
        'btnNext3
        '
        Me.btnNext3.Location = New System.Drawing.Point(458, 336)
        Me.btnNext3.Name = "btnNext3"
        Me.btnNext3.Size = New System.Drawing.Size(85, 32)
        Me.btnNext3.TabIndex = 23
        Me.btnNext3.Text = "下一步(&N)"
        Me.btnNext3.UseVisualStyleBackColor = True
        '
        'btnGenFile
        '
        Me.btnGenFile.Location = New System.Drawing.Point(314, 336)
        Me.btnGenFile.Name = "btnGenFile"
        Me.btnGenFile.Size = New System.Drawing.Size(85, 32)
        Me.btnGenFile.TabIndex = 23
        Me.btnGenFile.Text = "生成文件"
        Me.btnGenFile.UseVisualStyleBackColor = True
        '
        'RichTextBox4
        '
        Me.RichTextBox4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.RichTextBox4.Location = New System.Drawing.Point(86, 188)
        Me.RichTextBox4.Name = "RichTextBox4"
        Me.RichTextBox4.ReadOnly = True
        Me.RichTextBox4.Size = New System.Drawing.Size(616, 106)
        Me.RichTextBox4.TabIndex = 22
        Me.RichTextBox4.Text = "请将上面的机器码通过Email或短信形式发送给销售人员，以便进行正版注册工作。" & Global.Microsoft.VisualBasic.ChrW(10) & "本机器码是您获得自由王软件使用授权的唯一标志，自由王软件将并且只将对拥有本机器码的" & _
            "机器进行服务，如果您想把软件整体转移到其他机器，请预先与销售人员进行沟通，获得许可后再进行。" & Global.Microsoft.VisualBasic.ChrW(10) & "另外，请妥善保存您收到销售人员给您的与本机对应的注册码。"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(456, 134)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(11, 12)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "-"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(307, 134)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(11, 12)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "-"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(456, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 12)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "-"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(307, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(11, 12)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "-"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(473, 131)
        Me.TextBox6.MaxLength = 6
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(118, 21)
        Me.TextBox6.TabIndex = 17
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(324, 131)
        Me.TextBox5.MaxLength = 6
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(118, 21)
        Me.TextBox5.TabIndex = 16
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(173, 131)
        Me.TextBox4.MaxLength = 6
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(118, 21)
        Me.TextBox4.TabIndex = 15
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(473, 83)
        Me.TextBox3.MaxLength = 6
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(118, 21)
        Me.TextBox3.TabIndex = 14
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(324, 83)
        Me.TextBox2.MaxLength = 6
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(118, 21)
        Me.TextBox2.TabIndex = 13
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(173, 83)
        Me.TextBox1.MaxLength = 6
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(118, 21)
        Me.TextBox1.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(321, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "当前机器的机器码"
        '
        'tpRCode
        '
        Me.tpRCode.Controls.Add(Me.RichTextBox5)
        Me.tpRCode.Controls.Add(Me.btnDaoRu)
        Me.tpRCode.Location = New System.Drawing.Point(4, 21)
        Me.tpRCode.Name = "tpRCode"
        Me.tpRCode.Size = New System.Drawing.Size(769, 441)
        Me.tpRCode.TabIndex = 3
        Me.tpRCode.Text = "注册码"
        Me.tpRCode.UseVisualStyleBackColor = True
        '
        'RichTextBox5
        '
        Me.RichTextBox5.Location = New System.Drawing.Point(83, 63)
        Me.RichTextBox5.Name = "RichTextBox5"
        Me.RichTextBox5.ReadOnly = True
        Me.RichTextBox5.Size = New System.Drawing.Size(638, 143)
        Me.RichTextBox5.TabIndex = 1
        Me.RichTextBox5.Text = "请点击""导入许可文件""按钮，导入您从销售人员处获得的注册码文件。"
        '
        'btnDaoRu
        '
        Me.btnDaoRu.Location = New System.Drawing.Point(309, 279)
        Me.btnDaoRu.Name = "btnDaoRu"
        Me.btnDaoRu.Size = New System.Drawing.Size(200, 104)
        Me.btnDaoRu.TabIndex = 0
        Me.btnDaoRu.Text = "导入许可文件"
        Me.btnDaoRu.UseVisualStyleBackColor = True
        '
        'tpDone
        '
        Me.tpDone.Controls.Add(Me.RichTextBox6)
        Me.tpDone.Controls.Add(Me.btnDone)
        Me.tpDone.Location = New System.Drawing.Point(4, 21)
        Me.tpDone.Name = "tpDone"
        Me.tpDone.Size = New System.Drawing.Size(769, 441)
        Me.tpDone.TabIndex = 4
        Me.tpDone.Text = "完成注册"
        Me.tpDone.UseVisualStyleBackColor = True
        '
        'RichTextBox6
        '
        Me.RichTextBox6.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.RichTextBox6.Location = New System.Drawing.Point(113, 69)
        Me.RichTextBox6.Name = "RichTextBox6"
        Me.RichTextBox6.ReadOnly = True
        Me.RichTextBox6.Size = New System.Drawing.Size(582, 133)
        Me.RichTextBox6.TabIndex = 1
        Me.RichTextBox6.Text = "您已经完成了注册过程，请点击""注册完成""关闭窗体，如果注册码正确，系统将开始运行，否则请重新导入正确的注册码。"
        '
        'btnDone
        '
        Me.btnDone.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnDone.Location = New System.Drawing.Point(314, 274)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(174, 79)
        Me.btnDone.TabIndex = 0
        Me.btnDone.Text = "注册完成"
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'Freg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(777, 466)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Freg"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "欢迎使用自由王软件"
        Me.TopMost = True
        Me.TabControl1.ResumeLayout(False)
        Me.tpWelCome.ResumeLayout(False)
        Me.tpXieYi.ResumeLayout(False)
        Me.tpXieYi.PerformLayout()
        Me.tpMCode.ResumeLayout(False)
        Me.tpMCode.PerformLayout()
        Me.tpRCode.ResumeLayout(False)
        Me.tpDone.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpMCode As System.Windows.Forms.TabPage
    Friend WithEvents tpRCode As System.Windows.Forms.TabPage
    Friend WithEvents tpDone As System.Windows.Forms.TabPage
    Friend WithEvents btnDaoRu As System.Windows.Forms.Button
    Friend WithEvents btnDone As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnCancel2 As System.Windows.Forms.Button
    Friend WithEvents btnPre2 As System.Windows.Forms.Button
    Friend WithEvents btnAgree As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox3 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox4 As System.Windows.Forms.RichTextBox
    Friend WithEvents btnNext3 As System.Windows.Forms.Button
    Friend WithEvents btnGenFile As System.Windows.Forms.Button
    Friend WithEvents btnCancel3 As System.Windows.Forms.Button
    Friend WithEvents RichTextBox5 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox6 As System.Windows.Forms.RichTextBox
    Public WithEvents tpWelCome As System.Windows.Forms.TabPage
    Public WithEvents tpXieYi As System.Windows.Forms.TabPage

End Class
