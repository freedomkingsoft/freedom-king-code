﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectGNS
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnImportin = New System.Windows.Forms.Button
        Me.Fkgns1 = New FKImportInFun.myGNS
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(261, 197)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(87, 32)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "完　　成"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnImportin
        '
        Me.btnImportin.Location = New System.Drawing.Point(261, 120)
        Me.btnImportin.Name = "btnImportin"
        Me.btnImportin.Size = New System.Drawing.Size(87, 38)
        Me.btnImportin.TabIndex = 4
        Me.btnImportin.Text = "导入该功能"
        Me.btnImportin.UseVisualStyleBackColor = True
        '
        'Fkgns1
        '
        Me.Fkgns1.Location = New System.Drawing.Point(12, 190)
        Me.Fkgns1.Name = "Fkgns1"
        Me.Fkgns1.Size = New System.Drawing.Size(229, 57)
        Me.Fkgns1.TabIndex = 6
        Me.Fkgns1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 77)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(341, 12)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "你选择的文件中有如下功能，请单击导入该功能按钮进行下一步"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(14, 111)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(227, 136)
        Me.TextBox1.TabIndex = 8
        '
        'SelectGNS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 280)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Fkgns1)
        Me.Controls.Add(Me.btnImportin)
        Me.Controls.Add(Me.btnExit)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SelectGNS"
        Me.Text = "导入功能"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Fkgns1 As FKImportInFun.myGNS
    Friend WithEvents btnImportin As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
End Class
