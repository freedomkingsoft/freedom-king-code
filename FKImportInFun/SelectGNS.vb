﻿Public Class SelectGNS

    Dim sSourseFile As String

    Private Function SelectSourseFile() As Boolean
        Dim dlg As New Windows.Forms.OpenFileDialog

        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sSourseFile = dlg.FileName
            Return True
        Else
            Return False
            Me.Close()
        End If
    End Function


    Private Sub GNSGL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        If SelectSourseFile() Then

            Dim ds As New DataSet
            ds.ReadXml(sSourseFile)
            Me.TextBox1.Text = ds.Tables(0).Rows(0).Item("Context").ToString

            '    Me.Fkgns1.TvShows(sSourseFile, True)
            '    Me.Fkgns1.SelectedNode = Me.Fkgns1.Nodes(0)
        Else
            Me.Dispose()
        End If
    End Sub


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub


    'Private Sub Fkgns1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles Fkgns1.AfterSelect
    '    If Me.Fkgns1.SelectedNode.Name = "1" Then
    '        Me.stsText.Text = "您选择了根节点!"
    '        Me.btnImportin.Enabled = False
    '    Else
    '        If Me.Fkgns1.SelectedNode.Tag = False Then
    '            Me.stsText.Text = "您选择了一个节点"
    '            Me.btnImportin.Enabled = False
    '        Else
    '            Me.stsText.Text = "您选择了一个功能,不能给它建立下级节点!"
    '            Me.btnImportin.Enabled = True
    '        End If
    '    End If
    'End Sub


    Private Sub btnImportin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportin.Click
        Dim Import As New ImportInFun
        Import.ImportIn(0, sSourseFile)
    End Sub
End Class