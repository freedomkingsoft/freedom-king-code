﻿Imports System.Xml
Public Class XMLRWer

    Private XmlDoc As New XmlDocument
    Private XmlFile As String

    Public ReadOnly Property XmlFileName() As String
        Get
            Return XmlFile
        End Get
    End Property

    Public ReadOnly Property XmlText() As String
        Get
            Return XmlDoc.InnerXml
        End Get
    End Property

    Sub New(ByVal FileName As String, Optional ByVal CreateNew As Boolean = True, Optional ByVal Root As String = "LieSetup ", Optional ByRef IsOK As Boolean = False)
        'IsOK = False
        XmlFile = FileName
        'Dim reader As System.Xml.XmlReader = Nothing
        'Try
        '    reader = New System.Xml.XmlTextReader(FileName)
        '    reader.Read()
        'Catch ex As Exception
        '    If reader IsNot Nothing Then reader.Close()
        '    Debug.Print("New   -   " & ex.Message)
        '    If Not Create(FileName, Root) Then Return
        'Finally
        '    If reader IsNot Nothing Then reader.Close()
        'End Try

        'IsOK = True
        'XmlFile = FileName
        XmlDoc = New XmlDocument
        XmlDoc.Load(XmlFile)
    End Sub

    Public Function SaveInnerText(ByVal sKey As String, ByVal sInnerText As String, Optional ByVal sElement As String = "Biao", Optional ByVal sSection As String = "BiaoSetup") As Boolean
        Dim Lie As XmlNode
        Dim root As XmlNode = Me.XmlDoc.DocumentElement

        Lie = root.SelectSingleNode("descendant::" & sSection & "[" & sElement & "='" & sKey & "']")
        If IsNothing(Lie) Then
            Return False
        End If

        Lie.LastChild.InnerText = sInnerText
        XmlDoc.Save(XmlFile)
        Return True
    End Function

    'Public Function SaveAddNode() As Boolean

    'End Function

    Public Function Read(ByVal sKey As String, Optional ByVal sElement As String = "Biao", Optional ByVal sSection As String = "BiaoSetup") As String

        ''Create the XmlDocument.
        'Dim doc As XmlDocument = New XmlDocument()
        'doc.Load(My.Application.Info.DirectoryPath & "\FKLIESET.XML")

        Dim Lie As XmlNode
        Dim root As XmlNode = Me.XmlDoc.DocumentElement

        Lie = root.SelectSingleNode("descendant::" & sSection & "[" & sElement & "='" & sKey & "']")
        'Lie = root.SelectSingleNode("descendant::BiaoSetup[Biao='" & sKey & "']")
        If IsNothing(Lie) Then
            Return ""
        End If

        Return Lie.LastChild.InnerText

        'MsgBox(book.LastChild.InnerText)
        ''Change the price on the book.
        'book.LastChild.InnerText = "15.95"

        'Console.WriteLine("Display the modified XML document....")
        'doc.Save(Console.Out)

    End Function
End Class


