Imports System.Security.Cryptography
Public Class Form1

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnID.Click
        Me.TextBox2.Text = HID()
        Me.TextBox1.Text = CPUID()
    End Sub

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet
        Dim cpu

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    'Private Function HDDID() As String '得到的是硬盘的型号id
    '    Dim sHDDID As String = ""
    '    Dim objS = GetObject("winmgmts:\\.\root\cimv2")
    '    Dim cols = objS.ExecQuery("SELECT * FROM Win32_DiskDrive")
    '    On Error Resume Next
    '    Dim obj
    '    For Each obj In cols
    '        sHDDID = sHDDID & obj.PNPDeviceId.ToString
    '    Next

    '    Return sHDDID
    'End Function

    Private Function HID() As String
        Dim HDid As String = ""
        Try
            Dim cimobject As New Management.ManagementClass("win32_diskdrive")
            Dim moc As Management.ManagementObjectCollection
            moc = cimobject.GetInstances()
            Dim mo As Management.ManagementObject

            For Each mo In moc
                HDid = HDid & mo.Properties("signature").Value.ToString
                Exit For
            Next
        Catch ex As Exception
            HDid = ""
        End Try

        Return HDid

    End Function

    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 35
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            'Dim iSJ As Double
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                'Randomize(0)
                'iSJ = Rnd(iZ)
                'iJG = Int(iSJ * 25 + 65)
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
                'sMCO = sMCO & Chr(Int((Rnd(Asc(sCDO.Substring(i, 1))) * 25) + 65)).ToString
            Next
        End If

        Return sMCO
    End Function

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEN.Click
        'Me.txtMiWen.Text = encode(Me.txtMingWen.Text)
        Dim rsa As New RSA
        Me.txtMiWen.Text = rsa.Encrypt(Me.txtMingWen.Text.ToString, Application.StartupPath & "\pub.cyh_publickey")

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDE.Click
        Dim rsa As New RSA

        'Me.txtMingWen.Text = Decode(Me.txtMiWen.Text)
        Me.txtMingWen.Text = rsa.Decrypt(Me.txtMiWen.Text.ToString, Application.StartupPath & "\pra.cyh_primarykey")
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKEY.Click
        Dim rsa As New RSA
        rsa.SaveKey(Me.txtKeySize.Text)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMC.Click
        Me.txtMingWen.Text = MCO(CPUID, HID)
    End Sub

    Private Sub btnHash_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHash.Click
        Try
            Dim RC As New RSA()

            Dim m_strHashbyteSignature As String = ""
            If RC.GetHash(Me.txtMingWen.Text, m_strHashbyteSignature) = False Then
                MessageBox.Show(Me, "取Hash码错误！", "提示", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning)
            End If

            Me.txtHash.Text = m_strHashbyteSignature
        Catch ex As Exception
            MessageBox.Show(Me, ex.Message, "错误", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.[Error])
        End Try

    End Sub

    Private Sub btnQM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQM.Click
        Try
            Dim RC As New RSA()

            Dim m_strKeyPrivate As String = ""

            If System.IO.File.Exists(Application.StartupPath & "\pra.cyh_primarykey") Then
                Dim streamReader As New IO.StreamReader(Application.StartupPath & "\pra.cyh_primarykey", True)
                m_strKeyPrivate = streamReader.ReadToEnd
                Me.txtPriKey.Text = m_strKeyPrivate
                m_strKeyPrivate = m_strKeyPrivate.Remove(0, m_strKeyPrivate.IndexOf("</BitStrength>") + 14)
                streamReader.Close()
            End If

            Dim m_strEncryptedSignatureData As String = ""
            If RC.SignatureFormatter(m_strKeyPrivate, Me.txtHash.Text.ToString, m_strEncryptedSignatureData) = False Then
                MessageBox.Show(Me, "RSA数字签名错误！", "提示", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning)
            End If

            Me.txtQM.Text = m_strEncryptedSignatureData
        Catch ex As Exception
            MessageBox.Show(Me, ex.Message, "错误", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.[Error])
        End Try

    End Sub

    Private Sub btnGetHash_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetHash.Click

        Try
            Dim RC As New RSA()

            Dim m_strHashbyteSignature As String = ""
            If RC.GetHash(Me.txtMingWen.Text, m_strHashbyteSignature) = False Then
                MessageBox.Show(Me, "取Hash码错误！", "提示", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning)
            End If

            Me.txtGetHash.Text = m_strHashbyteSignature
        Catch ex As Exception
            MessageBox.Show(Me, ex.Message, "错误", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.[Error])
        End Try
    End Sub

    Private Sub btnYZH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYZH.Click
        Try
            Dim RC As New RSA()

            Dim m_strKeyPublic As String = ""

            If System.IO.File.Exists(Application.StartupPath & "\pub.cyh_publickey") Then
                Dim streamReader As New IO.StreamReader(Application.StartupPath & "\pub.cyh_publickey", True)
                m_strKeyPublic = streamReader.ReadToEnd
                Me.txtPubKey.Text = m_strKeyPublic
                m_strKeyPublic = m_strKeyPublic.Remove(0, m_strKeyPublic.IndexOf("</BitStrength>") + 14)
                streamReader.Close()
            End If

            If RC.SignatureDeformatter(m_strKeyPublic, Me.txtGetHash.Text.ToString, Me.txtQM.Text.ToString) = False Then
                MessageBox.Show(Me, "身份验证失败！", "提示", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning)
            Else
                MessageBox.Show(Me, "身份验证通过！", "提示", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning)
            End If
        Catch ex As Exception
            MessageBox.Show(Me, ex.Message, "错误", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.[Error])
        End Try

    End Sub

  
End Class

