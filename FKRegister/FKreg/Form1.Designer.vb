﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.btnMC = New System.Windows.Forms.Button
        Me.btnID = New System.Windows.Forms.Button
        Me.txtMingWen = New System.Windows.Forms.TextBox
        Me.txtMiWen = New System.Windows.Forms.TextBox
        Me.btnDE = New System.Windows.Forms.Button
        Me.btnEN = New System.Windows.Forms.Button
        Me.btnKEY = New System.Windows.Forms.Button
        Me.txtHash = New System.Windows.Forms.TextBox
        Me.btnHash = New System.Windows.Forms.Button
        Me.txtQM = New System.Windows.Forms.TextBox
        Me.btnQM = New System.Windows.Forms.Button
        Me.btnGetHash = New System.Windows.Forms.Button
        Me.txtGetHash = New System.Windows.Forms.TextBox
        Me.btnYZH = New System.Windows.Forms.Button
        Me.txtPriKey = New System.Windows.Forms.TextBox
        Me.txtPubKey = New System.Windows.Forms.TextBox
        Me.txtKeySize = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(47, 22)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(191, 21)
        Me.TextBox1.TabIndex = 0
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(47, 98)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(191, 21)
        Me.TextBox2.TabIndex = 1
        '
        'btnMC
        '
        Me.btnMC.Location = New System.Drawing.Point(73, 197)
        Me.btnMC.Name = "btnMC"
        Me.btnMC.Size = New System.Drawing.Size(153, 39)
        Me.btnMC.TabIndex = 2
        Me.btnMC.Text = "MC"
        Me.btnMC.UseVisualStyleBackColor = True
        '
        'btnID
        '
        Me.btnID.Location = New System.Drawing.Point(73, 138)
        Me.btnID.Name = "btnID"
        Me.btnID.Size = New System.Drawing.Size(153, 39)
        Me.btnID.TabIndex = 2
        Me.btnID.Text = "CPUID HDDID"
        Me.btnID.UseVisualStyleBackColor = True
        '
        'txtMingWen
        '
        Me.txtMingWen.Location = New System.Drawing.Point(47, 242)
        Me.txtMingWen.Multiline = True
        Me.txtMingWen.Name = "txtMingWen"
        Me.txtMingWen.Size = New System.Drawing.Size(207, 53)
        Me.txtMingWen.TabIndex = 0
        Me.txtMingWen.Text = "明文"
        '
        'txtMiWen
        '
        Me.txtMiWen.Location = New System.Drawing.Point(47, 304)
        Me.txtMiWen.Multiline = True
        Me.txtMiWen.Name = "txtMiWen"
        Me.txtMiWen.Size = New System.Drawing.Size(207, 234)
        Me.txtMiWen.TabIndex = 1
        Me.txtMiWen.Text = "密文"
        '
        'btnDE
        '
        Me.btnDE.Location = New System.Drawing.Point(110, 583)
        Me.btnDE.Name = "btnDE"
        Me.btnDE.Size = New System.Drawing.Size(78, 39)
        Me.btnDE.TabIndex = 2
        Me.btnDE.Text = "解密"
        Me.btnDE.UseVisualStyleBackColor = True
        '
        'btnEN
        '
        Me.btnEN.Location = New System.Drawing.Point(2, 583)
        Me.btnEN.Name = "btnEN"
        Me.btnEN.Size = New System.Drawing.Size(97, 39)
        Me.btnEN.TabIndex = 2
        Me.btnEN.Text = "加密"
        Me.btnEN.UseVisualStyleBackColor = True
        '
        'btnKEY
        '
        Me.btnKEY.Location = New System.Drawing.Point(210, 583)
        Me.btnKEY.Name = "btnKEY"
        Me.btnKEY.Size = New System.Drawing.Size(46, 38)
        Me.btnKEY.TabIndex = 3
        Me.btnKEY.Text = "Key"
        Me.btnKEY.UseVisualStyleBackColor = True
        '
        'txtHash
        '
        Me.txtHash.Location = New System.Drawing.Point(332, 35)
        Me.txtHash.Multiline = True
        Me.txtHash.Name = "txtHash"
        Me.txtHash.Size = New System.Drawing.Size(207, 43)
        Me.txtHash.TabIndex = 0
        Me.txtHash.Text = "HashCode"
        '
        'btnHash
        '
        Me.btnHash.Location = New System.Drawing.Point(559, 39)
        Me.btnHash.Name = "btnHash"
        Me.btnHash.Size = New System.Drawing.Size(72, 39)
        Me.btnHash.TabIndex = 2
        Me.btnHash.Text = "HashCode"
        Me.btnHash.UseVisualStyleBackColor = True
        '
        'txtQM
        '
        Me.txtQM.Location = New System.Drawing.Point(332, 98)
        Me.txtQM.Multiline = True
        Me.txtQM.Name = "txtQM"
        Me.txtQM.Size = New System.Drawing.Size(207, 222)
        Me.txtQM.TabIndex = 0
        Me.txtQM.Text = "RSA签名"
        '
        'btnQM
        '
        Me.btnQM.Location = New System.Drawing.Point(559, 185)
        Me.btnQM.Name = "btnQM"
        Me.btnQM.Size = New System.Drawing.Size(72, 39)
        Me.btnQM.TabIndex = 2
        Me.btnQM.Text = "RSA签名"
        Me.btnQM.UseVisualStyleBackColor = True
        '
        'btnGetHash
        '
        Me.btnGetHash.Location = New System.Drawing.Point(559, 344)
        Me.btnGetHash.Name = "btnGetHash"
        Me.btnGetHash.Size = New System.Drawing.Size(72, 39)
        Me.btnGetHash.TabIndex = 5
        Me.btnGetHash.Text = "getHash"
        Me.btnGetHash.UseVisualStyleBackColor = True
        '
        'txtGetHash
        '
        Me.txtGetHash.Location = New System.Drawing.Point(332, 340)
        Me.txtGetHash.Multiline = True
        Me.txtGetHash.Name = "txtGetHash"
        Me.txtGetHash.Size = New System.Drawing.Size(207, 43)
        Me.txtGetHash.TabIndex = 4
        Me.txtGetHash.Text = "getHashCode"
        '
        'btnYZH
        '
        Me.btnYZH.Location = New System.Drawing.Point(559, 454)
        Me.btnYZH.Name = "btnYZH"
        Me.btnYZH.Size = New System.Drawing.Size(72, 39)
        Me.btnYZH.TabIndex = 5
        Me.btnYZH.Text = "身份验证"
        Me.btnYZH.UseVisualStyleBackColor = True
        '
        'txtPriKey
        '
        Me.txtPriKey.Location = New System.Drawing.Point(262, 389)
        Me.txtPriKey.Multiline = True
        Me.txtPriKey.Name = "txtPriKey"
        Me.txtPriKey.Size = New System.Drawing.Size(277, 104)
        Me.txtPriKey.TabIndex = 4
        Me.txtPriKey.Text = "prikey"
        '
        'txtPubKey
        '
        Me.txtPubKey.Location = New System.Drawing.Point(262, 518)
        Me.txtPubKey.Multiline = True
        Me.txtPubKey.Name = "txtPubKey"
        Me.txtPubKey.Size = New System.Drawing.Size(277, 104)
        Me.txtPubKey.TabIndex = 4
        Me.txtPubKey.Text = "ｐｕｂＫｅｙ"
        '
        'txtKeySize
        '
        Me.txtKeySize.Location = New System.Drawing.Point(193, 556)
        Me.txtKeySize.Name = "txtKeySize"
        Me.txtKeySize.Size = New System.Drawing.Size(63, 21)
        Me.txtKeySize.TabIndex = 1
        Me.txtKeySize.Text = "keysize"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(643, 651)
        Me.Controls.Add(Me.btnYZH)
        Me.Controls.Add(Me.btnGetHash)
        Me.Controls.Add(Me.txtPubKey)
        Me.Controls.Add(Me.txtPriKey)
        Me.Controls.Add(Me.txtGetHash)
        Me.Controls.Add(Me.btnKEY)
        Me.Controls.Add(Me.btnEN)
        Me.Controls.Add(Me.btnID)
        Me.Controls.Add(Me.btnDE)
        Me.Controls.Add(Me.txtMiWen)
        Me.Controls.Add(Me.btnQM)
        Me.Controls.Add(Me.btnHash)
        Me.Controls.Add(Me.txtQM)
        Me.Controls.Add(Me.btnMC)
        Me.Controls.Add(Me.txtHash)
        Me.Controls.Add(Me.txtMingWen)
        Me.Controls.Add(Me.txtKeySize)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents btnMC As System.Windows.Forms.Button
    Friend WithEvents btnID As System.Windows.Forms.Button
    Friend WithEvents txtMingWen As System.Windows.Forms.TextBox
    Friend WithEvents txtMiWen As System.Windows.Forms.TextBox
    Friend WithEvents btnDE As System.Windows.Forms.Button
    Friend WithEvents btnEN As System.Windows.Forms.Button
    Friend WithEvents btnKEY As System.Windows.Forms.Button
    Friend WithEvents txtHash As System.Windows.Forms.TextBox
    Friend WithEvents btnHash As System.Windows.Forms.Button
    Friend WithEvents txtQM As System.Windows.Forms.TextBox
    Friend WithEvents btnQM As System.Windows.Forms.Button
    Friend WithEvents btnGetHash As System.Windows.Forms.Button
    Friend WithEvents txtGetHash As System.Windows.Forms.TextBox
    Friend WithEvents btnYZH As System.Windows.Forms.Button
    Friend WithEvents txtPriKey As System.Windows.Forms.TextBox
    Friend WithEvents txtPubKey As System.Windows.Forms.TextBox
    Friend WithEvents txtKeySize As System.Windows.Forms.TextBox

End Class
