﻿Public Class RSA

    Public Function Encrypt(ByVal p_inputString As String, ByVal p_strKeyPath As String) As String
        Dim fileString As String = ""
        Dim outString As String = ""
        If System.IO.File.Exists(p_strKeyPath) Then
            Dim streamReader As New IO.StreamReader(p_strKeyPath, True)
            fileString = streamReader.ReadToEnd
            streamReader.Close()
        End If

        If fileString <> "" Then
            Dim BitStrengthString As String = fileString.Substring(0, fileString.IndexOf("</BitStrength>") + 14)
            fileString = fileString.Replace(BitStrengthString, "")
            Dim bitStrength As Integer = Convert.ToInt32(BitStrengthString.Replace("<BitStrength>", "").Replace("</BitStrength>", ""))

            Try
                outString = Encryptstring(p_inputString, bitstrength, fileString)

            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End If

        Return outString

    End Function

    Public Function Decrypt(ByVal p_inputString As String, ByVal p_strKeyPath As String) As String
        Dim fileString As String = ""
        Dim outString As String = ""

        If IO.File.Exists(p_strKeyPath) Then
            Dim streamReader As New IO.StreamReader(p_strKeyPath, True)
            fileString = streamReader.ReadToEnd
            streamReader.Close()
        End If

        If fileString <> "" Then
            Dim bitStrengthString As String = fileString.Substring(0, fileString.IndexOf("</BitStrength>") + 14)
            fileString = fileString.Replace(bitStrengthString, "")
            Dim bitStrength As Int32 = Convert.ToInt32(bitStrengthString.Replace("<BitStrength>", "").Replace("</BitStrength>", ""))
            Try
                outString = DecryptString(p_inputString, bitStrength, fileString)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End If

        Return outString

    End Function

    Private Function EncryptString(ByVal p_inputString As String, ByVal p_dwKeySize As Integer, ByVal p_xmlString As String) As String
        Dim rsaCryptoServiceProvider As New System.Security.Cryptography.RSACryptoServiceProvider(p_dwKeySize)
        '定义Provider
        rsaCryptoServiceProvider.FromXmlString(p_xmlString)
        '导入公钥
        Dim keySize As Integer = p_dwKeySize / 8
        Dim bytes() As Byte = System.Text.Encoding.UTF32.GetBytes(p_inputString)
        Dim maxLength As Integer = keySize - 42
        Dim dataLength As Integer = bytes.Length
        Dim iterations As Integer = Int(dataLength / maxLength) '重复次数
        Dim StringBuilder As New System.Text.StringBuilder

        Dim i As Integer
        For i = 0 To iterations
            Dim itempLength As Integer
            If dataLength - maxLength * i > maxLength Then
                itempLength = maxLength - 1
            Else
                itempLength = dataLength - maxLength * i - 1
            End If
            Dim tempBytes(itempLength) As Byte

            Buffer.BlockCopy(bytes, maxLength * i, tempBytes, 0, tempBytes.Length)
            Dim encryptedBytes() As Byte = rsaCryptoServiceProvider.Encrypt(tempBytes, True)
            Array.Reverse(encryptedBytes)
            StringBuilder.Append(Convert.ToBase64String(encryptedBytes))
        Next

        Return StringBuilder.ToString

    End Function


    Private Function DecryptString(ByVal inputString As String, ByVal dwKeySize As Integer, ByVal xmlString As String) As String
        Dim rsaCryptoServiceProvider As New System.Security.Cryptography.RSACryptoServiceProvider(dwKeySize)
        rsaCryptoServiceProvider.FromXmlString(xmlString)

        'Dim base64BlockSize As Integer = If(((dwKeySize / 8) Mod 3 <> 0), (((dwKeySize / 8) / 3) * 4) + 4, ((dwKeySize / 8) / 3) * 4)

        Dim base64BlockSize As Integer
        If (dwKeySize / 8) Mod 3 <> 0 Then
            base64BlockSize = Int(((dwKeySize / 8) / 3) * 4) + 4
        Else
            base64BlockSize = Int((dwKeySize / 8) / 3) * 4

        End If

        Dim iterations As Integer = Int(inputString.Length / base64BlockSize)
        Dim arrayList As New System.Collections.ArrayList

        Dim i As Integer
        For i = 0 To iterations
            Dim encryptedBytes() As Byte
            Dim itemp As Integer

            If inputString.Length >= base64BlockSize Then
                itemp = base64BlockSize - 2
            Else
                itemp = inputString.Length
            End If
            'encryptedBytes = Convert.FromBase64String(inputString.Substring(base64BlockSize * i, base64BlockSize))
            Dim stemp As String = inputString.Substring(itemp * i, itemp)
            encryptedBytes = Convert.FromBase64String(stemp)

            Array.Reverse(encryptedBytes)
        
            arrayList.AddRange(rsaCryptoServiceProvider.Decrypt(encryptedBytes, True))


        Next

        Return System.Text.Encoding.UTF32.GetString(TryCast(arrayList.ToArray(Type.[GetType]("System.Byte")), Byte()))

    End Function

    Public Sub SaveKey(ByVal p_currentBitStrength As Integer)
        Dim RSAProvider As New Security.Cryptography.RSACryptoServiceProvider(p_currentBitStrength)
        Dim publicAndPrivateKeys As String = ("<BitStrength>" & p_currentBitStrength.ToString() & "</BitStrength>") + RSAProvider.ToXmlString(True)
        Dim justPublicKey As String = ("<BitStrength>" & p_currentBitStrength.ToString() & "</BitStrength>") + RSAProvider.ToXmlString(False)
        If saveFile("Save Public/Private Keys As", "Public/Private Keys Document( *.cyh_primarykey )|*.cyh_primarykey", publicAndPrivateKeys) Then
            While Not saveFile("Save Public Key As", "Public Key Document( *.cyh_publickey )|*.cyh_publickey", justPublicKey)


            End While
        End If
    End Sub

    Private Function saveFile(ByVal p_title As String, ByVal p_filterString As String, ByVal p_outputString As String) As Boolean
        Dim saveFileDialog As New SaveFileDialog()
        saveFileDialog.Title = p_title
        saveFileDialog.Filter = p_filterString
        saveFileDialog.FileName = ""
        If saveFileDialog.ShowDialog() = DialogResult.OK Then
            Try
                Dim streamWriter As New IO.StreamWriter(saveFileDialog.FileName, False)
                If p_outputString IsNot Nothing Then
                    streamWriter.Write(p_outputString)
                End If
                streamWriter.Close()
                Return True
            Catch Ex As Exception
                Console.WriteLine(Ex.Message)
                Return False
            End Try
        End If
        Return False
    End Function

    Public Function GetHash(ByVal m_strSource As String, ByRef strHashData As String) As Boolean
        Try
            '从字符串中取得Hash描述 
            Dim Buffer As Byte()
            Dim HashData As Byte()
            Dim MD5 As System.Security.Cryptography.HashAlgorithm = System.Security.Cryptography.HashAlgorithm.Create("MD5")
            Buffer = System.Text.Encoding.GetEncoding("GB2312").GetBytes(m_strSource)
            HashData = MD5.ComputeHash(Buffer)

            strHashData = Convert.ToBase64String(HashData)
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'RSA签名 
    Public Function SignatureFormatter(ByVal p_strKeyPrivate As String, ByVal m_strHashbyteSignature As String, ByRef m_strEncryptedSignatureData As String) As Boolean
        Try
            Dim HashbyteSignature As Byte()
            Dim EncryptedSignatureData As Byte()

            HashbyteSignature = Convert.FromBase64String(m_strHashbyteSignature)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPrivate)
            Dim RSAFormatter As New System.Security.Cryptography.RSAPKCS1SignatureFormatter(RSA)
            '设置签名的算法为MD5 
            RSAFormatter.SetHashAlgorithm("MD5")
            '执行签名 
            EncryptedSignatureData = RSAFormatter.CreateSignature(HashbyteSignature)

            m_strEncryptedSignatureData = Convert.ToBase64String(EncryptedSignatureData)

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SignatureDeformatter(ByVal p_strKeyPublic As String, ByVal p_strHashbyteDeformatter As String, ByVal p_strDeformatterData As String) As Boolean
        Try
            Dim DeformatterData As Byte()
            Dim HashbyteDeformatter As Byte()

            HashbyteDeformatter = Convert.FromBase64String(p_strHashbyteDeformatter)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPublic)
            Dim RSADeformatter As New System.Security.Cryptography.RSAPKCS1SignatureDeformatter(RSA)
            '指定解密的时候HASH算法为MD5 
            RSADeformatter.SetHashAlgorithm("MD5")

            DeformatterData = Convert.FromBase64String(p_strDeformatterData)

            If RSADeformatter.VerifySignature(HashbyteDeformatter, DeformatterData) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Class
