Public Class FKPZLBSCX

    Private Sub FKPZLBSCX_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.mmStart.Value = Today.AddDays(-Today.Day + 1)
        Me.mmEnd.Value = Today

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select KMDM from KMDM where Nian=" & FKG.myselfG.NianFen)
        If ds.Tables(0).Rows.Count = 0 Then
        Else
            Me.txtKMStart.Text = ds.Tables(0).Rows(0).Item(0)
            Me.txtKMEnd.Text = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(0)
        End If
    End Sub

    Private Sub btnChaXun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChaXun.Click
        If Me.txtKMStart.Text = "" OrElse Me.txtKMEnd.Text = "" Then
            MsgBox("请输入科目起至范围!", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        Me.dgvPZ.DataSource = Nothing

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim sSQL As String
        sSQL = "select 日期,凭证号,摘要,科目全名,对应科目全名,sum(借方数量) as 借方数量,sum(借方金额) as 借方金额,sum(贷方数量) as 贷方数量,sum(贷方金额) as 贷方金额 from 凭证表 where 日期>='" & Me.mmStart.Value & "' and 日期<='" & Me.mmEnd.Value & "' and 科目DM>='" & Me.txtKMStart.Text & "' and 科目DM <='" & Me.txtKMEnd.Text & "' and (借方金额<>0 or 贷方金额<>0) group by  日期,凭证号,科目全名,对应科目全名,摘要,仓库"
        mdb.DataBind(Me.dgvPZ, sSQL)

        If Me.cbNumber.Checked Then

        Else
            Me.dgvPZ.Columns("借方数量").Visible = False
            Me.dgvPZ.Columns("贷方数量").Visible = False
        End If
    End Sub

  
End Class