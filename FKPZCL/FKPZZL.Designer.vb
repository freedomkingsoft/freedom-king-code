﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKPZZL
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtMaxHao = New System.Windows.Forms.TextBox
        Me.txtMinHao = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.nudYue = New System.Windows.Forms.NumericUpDown
        Me.txtNian = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbPZZ = New System.Windows.Forms.ComboBox
        Me.lbSpace = New System.Windows.Forms.ListBox
        Me.Button3 = New System.Windows.Forms.Button
        CType(Me.nudYue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 384)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(257, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "此功能用于删除空号的直接凭证或对凭证号重编"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(29, 325)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(90, 39)
        Me.Button2.TabIndex = 31
        Me.Button2.Text = "整理凭证"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(220, 328)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(61, 33)
        Me.Button1.TabIndex = 32
        Me.Button1.Text = "关闭"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtMaxHao
        '
        Me.txtMaxHao.Location = New System.Drawing.Point(195, 179)
        Me.txtMaxHao.Name = "txtMaxHao"
        Me.txtMaxHao.ReadOnly = True
        Me.txtMaxHao.Size = New System.Drawing.Size(56, 21)
        Me.txtMaxHao.TabIndex = 27
        '
        'txtMinHao
        '
        Me.txtMinHao.Location = New System.Drawing.Point(121, 179)
        Me.txtMinHao.Name = "txtMinHao"
        Me.txtMinHao.ReadOnly = True
        Me.txtMinHao.Size = New System.Drawing.Size(56, 21)
        Me.txtMinHao.TabIndex = 28
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(42, 182)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 12)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "已存在凭证号"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(71, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(149, 12)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "请设定您要整理的凭证类别"
        '
        'nudYue
        '
        Me.nudYue.Enabled = False
        Me.nudYue.Location = New System.Drawing.Point(145, 66)
        Me.nudYue.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudYue.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudYue.Name = "nudYue"
        Me.nudYue.ReadOnly = True
        Me.nudYue.Size = New System.Drawing.Size(40, 21)
        Me.nudYue.TabIndex = 23
        Me.nudYue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudYue.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtNian
        '
        Me.txtNian.Location = New System.Drawing.Point(44, 65)
        Me.txtNian.Name = "txtNian"
        Me.txtNian.ReadOnly = True
        Me.txtNian.Size = New System.Drawing.Size(72, 21)
        Me.txtNian.TabIndex = 22
        Me.txtNian.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(178, 182)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(17, 12)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "至"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(42, 216)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 12)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "空白凭证号"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(193, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 12)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "月"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(180, 141)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(17, 12)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "字"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(122, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "年"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(42, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 12)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "当前可以操作的期间"
        '
        'cbPZZ
        '
        Me.cbPZZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPZZ.FormattingEnabled = True
        Me.cbPZZ.Location = New System.Drawing.Point(85, 138)
        Me.cbPZZ.Name = "cbPZZ"
        Me.cbPZZ.Size = New System.Drawing.Size(95, 20)
        Me.cbPZZ.TabIndex = 15
        '
        'lbSpace
        '
        Me.lbSpace.FormattingEnabled = True
        Me.lbSpace.ItemHeight = 12
        Me.lbSpace.Location = New System.Drawing.Point(113, 216)
        Me.lbSpace.Name = "lbSpace"
        Me.lbSpace.Size = New System.Drawing.Size(100, 100)
        Me.lbSpace.TabIndex = 33
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(124, 326)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(90, 39)
        Me.Button3.TabIndex = 34
        Me.Button3.Text = "手工编号"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FKPZZL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(293, 419)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.lbSpace)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtMaxHao)
        Me.Controls.Add(Me.txtMinHao)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.nudYue)
        Me.Controls.Add(Me.txtNian)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbPZZ)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FKPZZL"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "凭证整理"
        CType(Me.nudYue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtMaxHao As System.Windows.Forms.TextBox
    Friend WithEvents txtMinHao As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents nudYue As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtNian As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbPZZ As System.Windows.Forms.ComboBox
    Friend WithEvents lbSpace As System.Windows.Forms.ListBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class
