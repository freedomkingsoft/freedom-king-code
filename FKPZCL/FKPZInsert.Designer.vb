﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKPZInsert
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbPZZ = New System.Windows.Forms.ComboBox
        Me.nudYue = New System.Windows.Forms.NumericUpDown
        Me.txtNian = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.nuPZZhangShu = New System.Windows.Forms.NumericUpDown
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtMinHao = New System.Windows.Forms.TextBox
        Me.txtMaxHao = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.nuPZH = New System.Windows.Forms.NumericUpDown
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        CType(Me.nudYue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuPZZhangShu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuPZH, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 342)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(149, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "该功能用于插入空白凭证号"
        '
        'cbPZZ
        '
        Me.cbPZZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPZZ.FormattingEnabled = True
        Me.cbPZZ.Location = New System.Drawing.Point(74, 126)
        Me.cbPZZ.Name = "cbPZZ"
        Me.cbPZZ.Size = New System.Drawing.Size(95, 20)
        Me.cbPZZ.TabIndex = 3
        '
        'nudYue
        '
        Me.nudYue.Enabled = False
        Me.nudYue.Location = New System.Drawing.Point(134, 54)
        Me.nudYue.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudYue.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudYue.Name = "nudYue"
        Me.nudYue.ReadOnly = True
        Me.nudYue.Size = New System.Drawing.Size(40, 21)
        Me.nudYue.TabIndex = 10
        Me.nudYue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudYue.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtNian
        '
        Me.txtNian.Location = New System.Drawing.Point(33, 53)
        Me.txtNian.Name = "txtNian"
        Me.txtNian.ReadOnly = True
        Me.txtNian.Size = New System.Drawing.Size(72, 21)
        Me.txtNian.TabIndex = 9
        Me.txtNian.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(182, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 12)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "月"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(111, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "年"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 12)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "当前可以操作的期间"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(60, 108)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(149, 12)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "请设定您要插入的凭证类别"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(169, 129)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(17, 12)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "字"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(31, 220)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(101, 12)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "需要插入的凭证号"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 365)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(197, 12)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "然后在相应功能处进行凭证内容修改"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(31, 251)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(89, 12)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "插入的凭证张数"
        '
        'nuPZZhangShu
        '
        Me.nuPZZhangShu.Location = New System.Drawing.Point(134, 249)
        Me.nuPZZhangShu.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nuPZZhangShu.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nuPZZhangShu.Name = "nuPZZhangShu"
        Me.nuPZZhangShu.ReadOnly = True
        Me.nuPZZhangShu.Size = New System.Drawing.Size(69, 21)
        Me.nuPZZhangShu.TabIndex = 13
        Me.nuPZZhangShu.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(31, 170)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 12)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "已存在凭证号"
        '
        'txtMinHao
        '
        Me.txtMinHao.Location = New System.Drawing.Point(110, 167)
        Me.txtMinHao.Name = "txtMinHao"
        Me.txtMinHao.ReadOnly = True
        Me.txtMinHao.Size = New System.Drawing.Size(56, 21)
        Me.txtMinHao.TabIndex = 12
        '
        'txtMaxHao
        '
        Me.txtMaxHao.Location = New System.Drawing.Point(184, 167)
        Me.txtMaxHao.Name = "txtMaxHao"
        Me.txtMaxHao.ReadOnly = True
        Me.txtMaxHao.Size = New System.Drawing.Size(56, 21)
        Me.txtMaxHao.TabIndex = 12
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(167, 170)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(17, 12)
        Me.Label11.TabIndex = 8
        Me.Label11.Text = "至"
        '
        'nuPZH
        '
        Me.nuPZH.Location = New System.Drawing.Point(134, 218)
        Me.nuPZH.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nuPZH.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nuPZH.Name = "nuPZH"
        Me.nuPZH.Size = New System.Drawing.Size(69, 21)
        Me.nuPZH.TabIndex = 13
        Me.nuPZH.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(169, 294)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(79, 30)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "关闭"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(62, 290)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(90, 39)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "插入凭证"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'FKPZInsert
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(262, 386)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.nuPZH)
        Me.Controls.Add(Me.nuPZZhangShu)
        Me.Controls.Add(Me.txtMaxHao)
        Me.Controls.Add(Me.txtMinHao)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.nudYue)
        Me.Controls.Add(Me.txtNian)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbPZZ)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FKPZInsert"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "插入凭证"
        CType(Me.nudYue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuPZZhangShu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuPZH, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbPZZ As System.Windows.Forms.ComboBox
    Friend WithEvents nudYue As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtNian As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents nuPZZhangShu As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtMinHao As System.Windows.Forms.TextBox
    Friend WithEvents txtMaxHao As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents nuPZH As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
