Public Class FKPZZL

    Private Sub FKPZZL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.txtNian.Text = FKG.myselfG.NianFen
        Me.nudYue.Value = FKG.myselfG.YueFen

        FillPZZ()
    End Sub

    Private Sub FillPZZ()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsGN As DataSet
        dsGN = mdb.Reader("select Distinct PingZhengZi from JiBenCaoZuo,GongNengShu where JiBenCaoZuo.A_GNSID=GongNengShu.ID and Gongnengshu.PZSCFS=0  and ɾ��='false' ")

        Me.cbPZZ.Items.Clear()
        Dim i As Integer
        For i = 0 To dsGN.Tables(0).Rows.Count - 1
            Me.cbPZZ.Items.Add(dsGN.Tables(0).Rows(i).Item("PingZhengZi"))

        Next

        'If Me.cbPZZ.Items.Count > 0 Then
        '    Me.cbPZZ.Items.RemoveAt(0)
        'End If
    End Sub

    Private Sub cbPZZ_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPZZ.SelectedIndexChanged
        PZZChange()
    End Sub

    Private Sub PZZChange()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select Min(PingZhengHao),Max(PingZhenghao),Min(A_GNSID) from Jibencaozuo where Nian=" & Me.txtNian.Text & " and Yue=" & Me.nudYue.Value & " and ɾ��='false'  and PingZhengZi='" & Me.cbPZZ.Text & "'")

        If ds.Tables(0).Rows.Count = 0 Then
            Me.txtMinHao.Text = 0
            Me.txtMaxHao.Text = 0
        Else
            Me.txtMinHao.Text = ds.Tables(0).Rows(0).Item(0)
            Me.txtMaxHao.Text = ds.Tables(0).Rows(0).Item(1)

            FillSpacePZH()
        End If
    End Sub

    Private Sub FillSpacePZH()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select Distinct right(ƾ֤��,4) from pz where ���=" & Me.txtNian.Text & " and �·�=" & Me.nudYue.Value & "  and Left(ƾ֤��,len('" & Me.cbPZZ.Text & "'))='" & Me.cbPZZ.Text & "' and Len(ƾ֤��)=" & Me.cbPZZ.Text.Length + 7 & " order by right(ƾ֤��,4)")

        Me.lbSpace.Items.Clear()

        Dim iDS(ds.Tables(0).Rows.Count) As Integer
        'ds.Tables(0).Rows.CopyTo(iDS, 0)

        Dim iPZH As Integer
        Dim iRow As Integer = 0
        For iPZH = 1 To Me.txtMaxHao.Text
            If iPZH = ds.Tables(0).Rows(iRow).Item(0) Then
                iRow = iRow + 1
            Else
                Me.lbSpace.Items.Add(Me.cbPZZ.Text & iPZH.ToString.PadLeft(7))
            End If
        Next
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        Do While Me.lbSpace.Items.Count > 0
            Dim iPZH As String = Me.lbSpace.Items(0).ToString

            'mdb.Write("update pz set ƾ֤��=Left(ƾ֤��,len('" & Me.cbPZZ.Text & "')) & space(7-len(LTrim(Right(ƾ֤��,4))-1)) & LTrim(Right(ƾ֤��,4))-1,���=left(���,len(���)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) & right(���," & FKG.myselfG.iBianHaoWeiShu + 4 & ")-1  where ���=" & Me.txtNian.Text & " and �·�=" & Me.nudYue.Value & "  and Len(ƾ֤��)=" & Me.cbPZZ.Text.Length + 7 & " and Left(ƾ֤��,len('" & Me.cbPZZ.Text & "'))='" & Me.cbPZZ.Text & "' and ƾ֤��>'" & iPZH & "'")
            'mdb.Write("update JiBenCaoZuo set PingZhengHao=PingZhengHao-1,BianHao=left(BianHao,len(BianHao)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) & right(BianHao," & FKG.myselfG.iBianHaoWeiShu + 4 & ")-1  where Nian=" & Me.txtNian.Text & " and Yue=" & Me.nudYue.Value & " and ɾ��='false' and PingZhengZi='" & Me.cbPZZ.Text & "' and PingZhengHao>=" & CType(iPZH.Substring(Len(iPZH) - 4, 4), Integer))

            mdb.Write("update pz set ƾ֤��=Convert(nvarchar,Left(ƾ֤��,len('" & Me.cbPZZ.Text & "'))) + Convert(nvarchar,space(7-len(LTrim(Right(ƾ֤��,4))-1))) + Convert(nvarchar,LTrim(Right(ƾ֤��,4))-1),���=Convert(nvarchar,left(���,len(���)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & "))) + Convert(nvarchar,right(���," & FKG.myselfG.iBianHaoWeiShu + 4 & ")-1)  where ���=" & Me.txtNian.Text & " and �·�=" & Me.nudYue.Value & "  and Len(ƾ֤��)=" & Me.cbPZZ.Text.Length + 7 & " and Left(ƾ֤��,len('" & Me.cbPZZ.Text & "'))='" & Me.cbPZZ.Text & "' and ƾ֤��>'" & iPZH & "'")
            mdb.Write("update JiBenCaoZuo set PingZhengHao=PingZhengHao-1,BianHao=Convert(nvarchar,left(BianHao,len(BianHao)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & "))) + Convert(nvarchar,right(BianHao," & FKG.myselfG.iBianHaoWeiShu + 4 & ")-1)  where Nian=" & Me.txtNian.Text & " and Yue=" & Me.nudYue.Value & " and ɾ��='false' and PingZhengZi='" & Me.cbPZZ.Text & "' and PingZhengHao>=" & CType(iPZH.Substring(Len(iPZH) - 4, 4), Integer))


            PZZChange()
        Loop
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim dlg As New ZPHXG.Form1
        dlg.ShowDialog()
    End Sub
End Class