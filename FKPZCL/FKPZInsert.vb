Public Class FKPZInsert

    Private Sub FKPZInsert_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.txtNian.Text = FKG.myselfG.NianFen
        Me.nudYue.Value = FKG.myselfG.YueFen

        FillPZZ()
    End Sub

    Private Sub FillPZZ()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsGN As DataSet
        dsGN = mdb.Reader("select Distinct PingZhengZi from JiBenCaoZuo,GongNengShu where JiBenCaoZuo.A_GNSID=GongNengShu.ID and Gongnengshu.PZSCFS=0  and 删除='false' ")

        Me.cbPZZ.Items.Clear()
        Dim i As Integer
        For i = 0 To dsGN.Tables(0).Rows.Count - 1
            Me.cbPZZ.Items.Add(dsGN.Tables(0).Rows(i).Item("PingZhengZi"))

        Next

        If mdb.Reader("select JiZhang from FKZQ where NY='" & (Me.txtNian.Text & Me.nudYue.Value.ToString.PadLeft(2, "0")).ToString & "'").Tables(0).Rows(0).Item(0) = True Then
            Me.Button2.Enabled = False
        End If
        'If Me.cbPZZ.Items.Count > 0 Then
        '    Me.cbPZZ.Items.RemoveAt(0)
        'End If
    End Sub

    Dim iGongnengID As Integer
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If IsNothing(Me.cbPZZ.Text) OrElse Me.cbPZZ.Text = "" OrElse Me.nuPZH.Value = 0 OrElse Me.nuPZH.Value < Me.txtMinHao.Text OrElse Me.nuPZH.Value > Me.txtMaxHao.Text Then
            MsgBox("您要插入的凭证号不在合理范围之内！", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If


        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        ''第一步，将所插入位置后所有全部编号后移到最后之后。
        'mdb.Write("update pz set 凭证号=Left(凭证号,len('" & Me.cbPZZ.Text & "')) & space(7-len(LTrim(Right(凭证号,4))+" & Me.txtMaxHao.Text & ")) & LTrim(Right(凭证号,4))+" & Me.txtMaxHao.Text & ",编号=left(编号,len(编号)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) & right(编号," & FKG.myselfG.iBianHaoWeiShu + 4 & ")+" & Me.txtMaxHao.Text & "  where 年份=" & Me.txtNian.Text & " and 月份=" & Me.nudYue.Value & " and Left(凭证号,len('" & Me.cbPZZ.Text & "'))='" & Me.cbPZZ.Text & "'  and Len(凭证号)=" & Me.cbPZZ.Text.Length + 7 & " and LTrim(Right(凭证号,4))>=" & Me.nuPZH.Value)
        'mdb.Write("update JiBenCaoZuo set PingZhengHao=PingZhengHao+" & Me.txtMaxHao.Text & ", BianHao=left(BianHao,len(BianHao)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) & right(BianHao," & FKG.myselfG.iBianHaoWeiShu + 4 & ")+" & Me.txtMaxHao.Text & " where Nian=" & Me.txtNian.Text & " and Yue=" & Me.nudYue.Value & " and 删除='false'  and PingZhengZi='" & Me.cbPZZ.Text & "' and PingZhengHao>=" & Me.nuPZH.Value)

        '第一步，将所插入位置后所有全部编号后移到最后之后。
        mdb.Write("update pz set 凭证号=Left(凭证号,len('" & Me.cbPZZ.Text & "')) + space(7-len(convert(int,Right(凭证号,4))+" & Me.txtMaxHao.Text & ")) + convert(nvarchar,convert(int,(Right(凭证号,4)))+" & Me.txtMaxHao.Text & "),编号=left(编号,len(编号)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) + convert(nvarchar,convert(int,right(编号," & FKG.myselfG.iBianHaoWeiShu + 4 & "))+" & Me.txtMaxHao.Text & ")  where 年份=" & Me.txtNian.Text & " and 月份=" & Me.nudYue.Value & " and Left(凭证号,len('" & Me.cbPZZ.Text & "'))='" & Me.cbPZZ.Text & "'  and Len(凭证号)=" & Me.cbPZZ.Text.Length + 7 & " and LTrim(Right(凭证号,4))>=" & Me.nuPZH.Value)
        mdb.Write("update JiBenCaoZuo set PingZhengHao=PingZhengHao+" & Me.txtMaxHao.Text & ", BianHao=left(BianHao,len(BianHao)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) + convert(nvarchar,convert(int,right(BianHao," & FKG.myselfG.iBianHaoWeiShu + 4 & "))+" & Me.txtMaxHao.Text & ") where Nian=" & Me.txtNian.Text & " and Yue=" & Me.nudYue.Value & " and 删除='false'  and PingZhengZi='" & Me.cbPZZ.Text & "' and PingZhengHao>=" & Me.nuPZH.Value)

        '第二步，将移后的编号前移，但留出空位

 
        mdb.Write("update pz set 凭证号=Left(凭证号,len('" & Me.cbPZZ.Text & "')) + space(7-len(convert(int,(Right(凭证号,4)-" & Me.txtMaxHao.Text - Me.nuPZZhangShu.Value & ")))) + convert(nvarchar,convert(int,(Right(凭证号,4)))-" & Me.txtMaxHao.Text - Me.nuPZZhangShu.Value & "),编号=left(编号,len(编号)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) + convert(nvarchar,convert(int,right(编号," & FKG.myselfG.iBianHaoWeiShu + 4 & "))-" & Me.txtMaxHao.Text - Me.nuPZZhangShu.Value & ")  where 年份=" & Me.txtNian.Text & " and 月份=" & Me.nudYue.Value & "  and Len(凭证号)=" & Me.cbPZZ.Text.Length + 7 & " and Left(凭证号,len('" & Me.cbPZZ.Text & "'))='" & Me.cbPZZ.Text & "' and LTrim(Right(凭证号,4))>=" & Me.nuPZH.Value)
        mdb.Write("update JiBenCaoZuo set PingZhengHao=PingZhengHao-" & Me.txtMaxHao.Text - Me.nuPZZhangShu.Value & ",BianHao=left(BianHao,len(BianHao)-(" & FKG.myselfG.iBianHaoWeiShu + 4 & ")) + convert(nvarchar,convert(int,right(BianHao," & FKG.myselfG.iBianHaoWeiShu + 4 & "))-" & Me.txtMaxHao.Text - Me.nuPZZhangShu.Value & ")  where Nian=" & Me.txtNian.Text & " and Yue=" & Me.nudYue.Value & " and 删除='false'  and PingZhengZi='" & Me.cbPZZ.Text & "' and PingZhengHao>=" & Me.nuPZH.Value)


        '第三步，插入Jibencaozuo表中的数据
        Dim sLieMing As String
        Dim sZhi As String

        Dim i As Integer
        For i = 0 To Me.nuPZZhangShu.Value - 1
            sZhi = mdb.Reader("select Distinct A_GNSID from JiBenCaoZuo,GongNengShu where JiBenCaoZuo.A_GNSID=GongNengShu.ID and Gongnengshu.PZSCFS=0 and JiBenCaoZuo.PingZhengZi='" & Me.cbPZZ.Text & "'").Tables(0).Rows(0).Item(0)
            sZhi = sZhi & "," & Me.txtNian.Text & "," & Me.nudYue.Value & ","

            Dim sMAXBH As String
            sMAXBH = mdb.Reader("select Max(BianHao) from JiBenCaoZuo,GongNengShu where JiBenCaoZuo.A_GNSID=GongNengShu.ID and Gongnengshu.PZSCFS=0 and JiBenCaoZuo.PingZhengZi='" & Me.cbPZZ.Text & "' and JiBenCaoZuo.Nian=" & Me.txtNian.Text & " and JiBenCaoZuo.Yue = " & Me.nudYue.Value).Tables(0).Rows(0).Item(0)
            sMAXBH = sMAXBH.Substring(0, sMAXBH.Length - FKG.myselfG.iBianHaoWeiShu) & (Me.nuPZH.Value + i).ToString.PadLeft(FKG.myselfG.iBianHaoWeiShu, "0")

            sZhi = sZhi & "'" & sMAXBH & "',"
            sZhi = sZhi & "'" & Me.cbPZZ.Text & "'," & Me.nuPZH.Value + i & ","
            sZhi = sZhi & "'" & FKG.myselfG.RiQi.ToShortDateString & "','','',0,'',"
            sZhi = sZhi & "'" & mdb.Reader("select min(KMDM) from KMDM where sfmj='true' and Nian=" & Me.txtNian.Text).Tables(0).Rows(0).Item(0) & "',"
            sZhi = sZhi & "'','" & mdb.Reader("select min(KMDM) from KMDM where sfmj='true' and Nian=" & Me.txtNian.Text).Tables(0).Rows(0).Item(0) & "',"
            sZhi = sZhi & "0,'',0,0,'0','0','0','','" & Now.ToShortDateString & "','0','0','0','0','0','0','0','0','0','0','','','','','','','','','','','','','','','','" & FKG.myselfG.YongHu & "',"

            Dim tNow As Date
            tNow = Now
            sZhi = sZhi & "'" & tNow & "','" & FKG.myselfG.YongHu & "'"


            sLieMing = "A_GNSID,Nian,Yue,BianHao,PingZhengZi,PingZhengHao,RiQi,JingShouRen,ZhaiYao,CangKu,JieFangZhuKemu,JieFangKeMu,DaiFangZhuKeMu,DaiFangKeMu,ShuLiang,DanWei,DanJia,JinE,BeiYong1,BeiYong2,BeiYong3,BeiYong4,BeiYong5,BeiYong6,BeiYong7,BeiYong8,BeiYong9,BeiYong10,BeiYong11,BeiYong12,BeiYong13,BeiYong14,BeiYong15,BeiYong16,BeiYong17,BeiYong18,BeiYong19,BeiYong20,BeiYong21,BeiYong22,BeiYong23,BeiYong24,BeiYong25,BeiYong26,BeiYong27,BeiYong28,BeiYong29,BeiYong30,制单,建立时间,建立人"


            mdb.Write("insert into JiBenCaoZuo (" & sLieMing & ") values (" & sZhi & ")")
            '第四步，插入凭证数据
            Dim iJBID As String
            iJBID = "{" & mdb.Reader("Select A_ID from JiBenCaoZuo where 建立时间='" & tNow & "' and 建立人='" & FKG.myselfG.YongHu & "'").Tables(0).Rows(0).Item(0).ToString & "}"

            SavePZ(iJBID, Me.nuPZH.Value + i)

        Next
        MsgBox("空白凭证插入成功，请到相应功能模块对插入的凭证进行修改！", MsgBoxStyle.Information, "提示")
        Me.Close()
    End Sub


    Private Function SavePZ(ByVal iJiBenCaoZuoID As String, ByVal iPZH As Integer) As Boolean


        Dim mdbopen As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim dsPZ As New DataSet
        mdbopen.oleCommand.CommandText = "select GN_PZ.* from GN_PZ,GongnengShu where GN_PZ.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID
        mdbopen.oleDataAdapter.Fill(dsPZ)
        'dsPZ = mdb.Reader("select GN_PZ.* from GN_PZ,GongnengShu where GN_PZ.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID)

        Dim dsJiBen As New DataSet
        mdbopen.oleCommand.CommandText = "select * from Jibencaozuo where A_ID ='" & iJiBenCaoZuoID & "'"
        mdbopen.oleDataAdapter.Fill(dsJiBen)
        'dsJiBen = mdb.Reader("select * from Jibencaozuo where A_ID =" & iJiBenCaoZuoID)


        Dim sLieming As String
        sLieming = "年份,月份,行数,凭证号,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,制单,审核,记帐,显示,建立时间,建立人,A_GNSID,A_JBID"

        Dim sZhi As String

        Dim i As Integer
        For i = 0 To dsPZ.Tables(0).Rows.Count - 1
            'If Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Double), 2) = 0 AndAlso Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Double), 2) Then
            '    Continue For
            'End If
            'Dim JinECase As Double = 0

            sZhi = FKG.myselfG.NianFen '年份
            sZhi = sZhi & "," & FKG.myselfG.YueFen '月份

            Dim iPzHao As String
            iPzHao = Me.cbPZZ.Text & "   " & iPZH.ToString.PadLeft(4)

            Dim iHang As Integer
            Dim dsHang As New DataSet
            Try
                'mdbOpen.oleCommand.CommandText = "select 凭证号 from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and     凭证号='" & iPzHao & "'"
                mdbopen.oleCommand.CommandText = "select isNull(Max(行数),0) from PZ where 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & " and     凭证号='" & iPzHao & "'"
                mdbopen.oleDataAdapter.Fill(dsHang)
                If dsHang.Tables(0).Rows.Count = 0 Then
                    iHang = 1
                Else
                    iHang = dsHang.Tables(0).Rows(0).Item(0) + 1 '行数
                End If
                'iHang = dsHang.Tables(0).Rows.Count + 1 '行数
            Catch ex As Exception
                MsgBox(ex.ToString)
                Return False
                Exit Function
            End Try

            sZhi = sZhi & "," & iHang
            '凭证号需要处理
            sZhi = sZhi & ",'" & iPzHao & "'"

            With dsPZ.Tables(0).Rows(i)
                If .Item("编号") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("编号")) & "'" '编号
                End If
                If .Item("日期") = "" Then
                    sZhi = sZhi & ",'" & Now & "'"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("日期")) & "'" '日期
                End If
                If .Item("摘要") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("摘要")) & "'" '摘要
                End If
                If .Item("科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("科目"))) & "'"
                End If
                If .Item("对应科目") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & FKF.FKF.getKMDMfromQM(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("对应科目"))) & "'"
                End If
                If .Item("借方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方数量")) & "'"
                End If

                If .Item("借方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方单位")) & "'"
                End If

                If .Item("借方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方单价")) & "'"
                End If
                If .Item("借方金额") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Double), 2) & "'"
                    'JinECase = Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("借方金额")), Double), 2)
                End If
                If .Item("贷方数量") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方数量")) & "'"
                End If
                If .Item("贷方单位") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方单位")) & "'"
                End If
                If .Item("贷方单价") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方单价")) & "'"
                End If
                If .Item("贷方金额") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    sZhi = sZhi & ",'" & Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Double), 2) & "'"
                    'JinECase = JinECase + Math.Round(CType(dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("贷方金额")), Double), 2)
                End If

                'If CType(JinECase, Integer) = 0 Then
                '    Continue For
                'End If

                If .Item("仓库") = "" Then
                    sZhi = sZhi & ",0"
                Else
                    Dim cangku As Object
                    cangku = dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("仓库"))
                    If IsNumeric(cangku) Then
                    Else
                        cangku = "'{" & cangku.ToString & "}'"
                    End If
                    sZhi = sZhi & "," & cangku
                End If
                If .Item("经手人") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("经手人")) & "'"
                End If
                If .Item("备用1") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用1")) & "'"
                End If
                If .Item("备用2") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用2")) & "'"
                End If
                If .Item("备用3") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用3")) & "'"
                End If
                If .Item("备用4") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用4")) & "'"
                End If
                If .Item("备用5") = "" Then
                    sZhi = sZhi & ",''"
                Else
                    sZhi = sZhi & ",'" & dsJiBen.Tables(0).Rows(0).Item(dsPZ.Tables(0).Rows(i).Item("备用5")) & "'"
                End If
                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & ",''"
                sZhi = sZhi & ",''"
                sZhi = sZhi & ",'" & dsPZ.Tables(0).Rows(i).Item("显示") & "'"
                sZhi = sZhi & ",'" & Now & "'"
                sZhi = sZhi & ",'" & FKG.myselfG.YongHu & "'"
                sZhi = sZhi & "," & iGongnengID
                sZhi = sZhi & ",'" & iJiBenCaoZuoID & "'"
            End With

            '  mdbopen.oleCommand.CommandText = "insert into PZ (" & sLieming & ") values (" & sZhi & ")"
            Try
                mdbopen.Write("insert into PZ (" & sLieming & ") values (" & sZhi & ")")
            Catch ex As Exception
                Return False
                Exit Function
            End Try
            'If mdb.Write("insert into PZ () values (" & sZhi & ")") = False Then
            '    Return False
            '    Exit Function
            'End If
        Next
        Return True
    End Function

    Private Sub cbPZZ_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPZZ.SelectedIndexChanged
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select Min(PingZhengHao),Max(PingZhenghao),Min(A_GNSID) from Jibencaozuo where Nian=" & Me.txtNian.Text & " and Yue=" & Me.nudYue.Value & " and 删除='false'  and PingZhengZi='" & Me.cbPZZ.Text & "'")

        If ds.Tables(0).Rows.Count = 0 Then
            Me.txtMinHao.Text = 0
            Me.txtMaxHao.Text = 0

            Me.nuPZH.Minimum = Me.txtMinHao.Text
            Me.nuPZH.Maximum = Me.txtMaxHao.Text
        ElseIf IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
            Me.txtMinHao.Text = 0
            Me.txtMaxHao.Text = 0

            Me.nuPZH.Minimum = Me.txtMinHao.Text
            Me.nuPZH.Maximum = Me.txtMaxHao.Text
        Else
            iGongnengID = ds.Tables(0).Rows(0).Item(2)

            Me.txtMinHao.Text = ds.Tables(0).Rows(0).Item(0)
            Me.txtMaxHao.Text = ds.Tables(0).Rows(0).Item(1)

            Me.nuPZH.Minimum = Me.txtMinHao.Text
            Me.nuPZH.Maximum = Me.txtMaxHao.Text
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

   
End Class