﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKPZLBSCX
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbNumber = New System.Windows.Forms.CheckBox
        Me.txtKMEnd = New System.Windows.Forms.TextBox
        Me.txtKMStart = New System.Windows.Forms.TextBox
        Me.mmEnd = New System.Windows.Forms.DateTimePicker
        Me.mmStart = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.btnChaXun = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.dgvPZ = New System.Windows.Forms.DataGridView
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPZ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.cbNumber)
        Me.GroupBox1.Controls.Add(Me.txtKMEnd)
        Me.GroupBox1.Controls.Add(Me.txtKMStart)
        Me.GroupBox1.Controls.Add(Me.mmEnd)
        Me.GroupBox1.Controls.Add(Me.mmStart)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.btnChaXun)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(613, 140)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "查询条件"
        '
        'cbNumber
        '
        Me.cbNumber.AutoSize = True
        Me.cbNumber.Location = New System.Drawing.Point(504, 37)
        Me.cbNumber.Name = "cbNumber"
        Me.cbNumber.Size = New System.Drawing.Size(72, 16)
        Me.cbNumber.TabIndex = 4
        Me.cbNumber.Text = "显示数量"
        Me.cbNumber.UseVisualStyleBackColor = True
        '
        'txtKMEnd
        '
        Me.txtKMEnd.Location = New System.Drawing.Point(364, 92)
        Me.txtKMEnd.Name = "txtKMEnd"
        Me.txtKMEnd.Size = New System.Drawing.Size(108, 21)
        Me.txtKMEnd.TabIndex = 3
        '
        'txtKMStart
        '
        Me.txtKMStart.Location = New System.Drawing.Point(179, 92)
        Me.txtKMStart.Name = "txtKMStart"
        Me.txtKMStart.Size = New System.Drawing.Size(108, 21)
        Me.txtKMStart.TabIndex = 3
        '
        'mmEnd
        '
        Me.mmEnd.CustomFormat = "yyyy-MM"
        Me.mmEnd.Location = New System.Drawing.Point(364, 37)
        Me.mmEnd.Name = "mmEnd"
        Me.mmEnd.Size = New System.Drawing.Size(108, 21)
        Me.mmEnd.TabIndex = 2
        '
        'mmStart
        '
        Me.mmStart.CustomFormat = ""
        Me.mmStart.Location = New System.Drawing.Point(180, 37)
        Me.mmStart.Name = "mmStart"
        Me.mmStart.Size = New System.Drawing.Size(108, 21)
        Me.mmStart.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(308, 98)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 12)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "截至科目:"
        '
        'btnChaXun
        '
        Me.btnChaXun.Location = New System.Drawing.Point(504, 76)
        Me.btnChaXun.Name = "btnChaXun"
        Me.btnChaXun.Size = New System.Drawing.Size(80, 50)
        Me.btnChaXun.TabIndex = 1
        Me.btnChaXun.Text = "查询"
        Me.btnChaXun.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(308, 41)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 12)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "截至日期:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(124, 98)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 12)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "起始科目:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "科目范围:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(124, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "起始日期:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "凭证期间:"
        '
        'dgvPZ
        '
        Me.dgvPZ.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvPZ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPZ.Location = New System.Drawing.Point(19, 178)
        Me.dgvPZ.Name = "dgvPZ"
        Me.dgvPZ.RowTemplate.Height = 23
        Me.dgvPZ.Size = New System.Drawing.Size(607, 313)
        Me.dgvPZ.TabIndex = 1
        '
        'FKPZLBSCX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 504)
        Me.Controls.Add(Me.dgvPZ)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FKPZLBSCX"
        Me.Text = "列表式凭证查询"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvPZ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvPZ As System.Windows.Forms.DataGridView
    Friend WithEvents btnChaXun As System.Windows.Forms.Button
    Friend WithEvents mmStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents mmEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKMEnd As System.Windows.Forms.TextBox
    Friend WithEvents txtKMStart As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbNumber As System.Windows.Forms.CheckBox
End Class
