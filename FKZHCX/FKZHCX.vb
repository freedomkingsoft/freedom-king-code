﻿Public Class FKZHCX

    Dim sBiao As String '选择的是工资表还是库存表
    Dim sXSLie As String = "" '选择的要显示的列
    Dim sMRCase As String '默认条件,对应明细查询来说,默认条件可以为空

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.SplitContainer1.Panel1Collapsed = Not Me.SplitContainer1.Panel1Collapsed
    End Sub

    '查询时不能跨年度选择日期
    Private Sub FKZHCX_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.myMinDate.Value = "#" & FKG.myselfG.NianFen & "-" & FKG.myselfG.YueFen & "-1#"
        Me.myMaxDate.Value = Today.ToShortDateString

        Me.myMinDate.MinDate = "#" & FKG.myselfG.NianFen & "-1-1#"
        Me.myMaxDate.MinDate = "#" & FKG.myselfG.NianFen & "-1-1#"

        Me.myMinDate.MaxDate = CType("#" & FKG.myselfG.NianFen + 1 & "-1-1#", Date).AddDays(-1)
        Me.myMaxDate.MaxDate = CType("#" & FKG.myselfG.NianFen + 1 & "-1-1#", Date).AddDays(-1)

        Me.rbKC.Checked = True

        Me.rbAnd.Enabled = False
        Me.rbOr.Enabled = False

        Me.Icon = FKG.myselfG.FKIcon
        Me.Text = "综合查询"
    End Sub


    Private Sub rbKC_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbKC.CheckedChanged
        If Me.rbKC.Checked = True Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim dsLie As DataSet
            dsLie = mdb.Reader("select * from 库存表 where 1<1")
            sBiao = "库存表"

            Array.Resize(sXiangType, dsLie.Tables(0).Columns.Count)

            Me.lbLie.Items.Clear()
            Me.lbSelected.Items.Clear()
            Me.cbXiang.Items.Clear()

            Dim i As Integer
            For i = 0 To dsLie.Tables(0).Columns.Count - 2
                Me.lbLie.Items.Add(dsLie.Tables(0).Columns(i).ColumnName)
                Me.cbXiang.Items.Add(dsLie.Tables(0).Columns(i).ColumnName)
                sXiangType(i).XiangName = dsLie.Tables(0).Columns(i).ColumnName
                sXiangType(i).XiangType = dsLie.Tables(0).Columns(i).DataType.Name
            Next
            Me.cbXiang.Items.Insert(0, "")

        End If
        Me.txtCase.Clear()
    End Sub

    Private Sub rbGZ_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbGZ.CheckedChanged
        If Me.rbGZ.Checked = True Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim dsLie As DataSet
            dsLie = mdb.Reader("select * from 工资表 where 1<1")
            sBiao = "工资表"

            Array.Resize(sXiangType, dsLie.Tables(0).Columns.Count)
            Me.lbLie.Items.Clear()
            Me.lbSelected.Items.Clear()
            Me.cbXiang.Items.Clear()

            Dim i As Integer
            For i = 0 To dsLie.Tables(0).Columns.Count - 2
                Me.lbLie.Items.Add(dsLie.Tables(0).Columns(i).ColumnName)
                Me.cbXiang.Items.Add(dsLie.Tables(0).Columns(i).ColumnName)
                sXiangType(i).XiangName = dsLie.Tables(0).Columns(i).ColumnName
                sXiangType(i).XiangType = dsLie.Tables(0).Columns(i).DataType.Name
            Next
            Me.cbXiang.Items.Insert(0, "")
        End If
    End Sub

    Private Sub rbPZ_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPZ.CheckedChanged
        If Me.rbPZ.Checked = True Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim dsLie As DataSet
            dsLie = mdb.Reader("select * from 凭证表 where 1<1")
            sBiao = "凭证表"

            Array.Resize(sXiangType, dsLie.Tables(0).Columns.Count)
            Me.lbLie.Items.Clear()
            Me.lbSelected.Items.Clear()
            Me.cbXiang.Items.Clear()

            Dim i As Integer
            For i = 0 To dsLie.Tables(0).Columns.Count - 2
                Me.lbLie.Items.Add(dsLie.Tables(0).Columns(i).ColumnName)
                Me.cbXiang.Items.Add(dsLie.Tables(0).Columns(i).ColumnName)
                sXiangType(i).XiangName = dsLie.Tables(0).Columns(i).ColumnName
                sXiangType(i).XiangType = dsLie.Tables(0).Columns(i).DataType.Name
            Next
            Me.cbXiang.Items.Insert(0, "")
        End If
    End Sub

    Private Sub cbXiang_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbXiang.SelectedIndexChanged
        If Me.cbXiang.SelectedIndex <= 0 Then
            Me.cbTiaoJian.Items.Clear()
        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim dsTiaojian As DataSet
            If Me.rbKC.Checked Then
                dsTiaojian = mdb.Reader("select Distinct " & Me.cbXiang.SelectedItem.ToString & " from 库存表 where 日期>='" & myMinDate.Value & "' and 日期<='" & Me.myMaxDate.Value & "'")

            ElseIf Me.rbGZ.Checked Then
                dsTiaojian = mdb.Reader("select Distinct " & Me.cbXiang.SelectedItem.ToString & " from 工资表 where 日期>='" & myMinDate.Value & "' and 日期<='" & Me.myMaxDate.Value & "'")
            Else
                dsTiaojian = mdb.Reader("select Distinct " & Me.cbXiang.SelectedItem.ToString & " from 凭证表 where 日期>='" & myMinDate.Value & "' and 日期<='" & Me.myMaxDate.Value & "'")
            End If

            Me.cbTiaoJian.Items.Clear()
            Me.cbTiaoJian.Text = ""

            Dim i As Integer
            For i = 0 To dsTiaojian.Tables(0).Rows.Count - 1
                Me.cbTiaoJian.Items.Add(dsTiaojian.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub lbLie_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLie.DoubleClick
        If Me.lbLie.SelectedIndex >= 0 Then
            Me.lbSelected.Items.Add(Me.lbLie.SelectedItem.ToString)
            Me.lbLie.Items.Remove(Me.lbLie.SelectedItem)

            Me.btnSave.Enabled = False
        End If
    End Sub

  
    Private Sub lbSelected_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelected.DoubleClick
        If Me.lbSelected.SelectedIndex >= 0 Then
            Me.lbLie.Items.Add(Me.lbSelected.SelectedItem.ToString)
            Me.lbSelected.Items.Remove(Me.lbSelected.SelectedItem)

            Me.btnSave.Enabled = False
        End If
    End Sub

    Structure XiangType
        Public XiangName As String
        Public XiangType As String
    End Structure

    Dim sXiangType() As XiangType

    Private Function getXiangType(ByVal sName As String) As String
        Dim i As Integer
        For i = 0 To sXiangType.Length - 1
            If sXiangType(i).XiangName = sName Then
                Return sXiangType(i).XiangType
            End If
        Next

        Return "String"
    End Function

    Private Sub btnAddCase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddCase.Click
        If Me.cbYSF.SelectedIndex < 0 OrElse Me.cbTiaoJian.Text = "" Then
            Exit Sub
        End If

        Dim sTypeBit As String
        Select Case getXiangType(Me.cbXiang.SelectedItem.ToString)
            Case "String"
                sTypeBit = "'"

            Case "DateTime"
                sTypeBit = "'"

            Case Else
                sTypeBit = ""
        End Select

        'MsgBox(Me.cbTiaoJian.SelectedItem.GetType.Name)
        Select Case Me.cbYSF.SelectedItem
            Case "开始于", "不开始于"
                If Me.rbAnd.Enabled = False Then
                    Me.txtCase.Text = Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    Else
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    End If

                End If
            Case "包括", "不包括"
                If Me.rbAnd.Enabled = False Then
                    Me.txtCase.Text = Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    Else
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & "%" & Me.cbTiaoJian.Text.ToString & "%" & sTypeBit
                    End If

                End If
            Case Else
                If Me.rbAnd.Enabled = False Then
                    Me.txtCase.Text = Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                Else
                    If Me.rbAnd.Checked Then
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbAnd.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                    Else
                        Me.txtCase.Text = Me.txtCase.Text & " " & Me.rbOr.Name.Remove(0, 2).ToString & " " & Me.cbXiang.SelectedItem.ToString & " " & Me.cbYSF.SelectedItem.ToString & " " & sTypeBit & Me.cbTiaoJian.Text.ToString & sTypeBit
                    End If

                End If
        End Select

    End Sub

    Private Sub txtCase_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCase.TextChanged
        If Me.txtCase.Text <> "" Then
            Me.rbAnd.Enabled = True
            Me.rbOr.Enabled = True
        Else
            Me.rbAnd.Enabled = False
            Me.rbOr.Enabled = False
        End If
    End Sub

    Private Sub btnIsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIsert.Click
        If Me.btnIsert.Text = "插入""(""" Then
            Dim iGB As Integer
            iGB = Me.txtCase.SelectionStart
            'MsgBox(iGB.ToString)

            Me.txtCase.Text = Me.txtCase.Text.Insert(iGB, "(")

            Me.btnIsert.Text = "插入"")"""

        Else
            Dim iGB As Integer
            iGB = Me.txtCase.SelectionStart
            'MsgBox(iGB.ToString)

            Me.txtCase.Text = Me.txtCase.Text.Insert(iGB, ")")

            Me.btnIsert.Text = "插入""("""

        End If
        Me.txtCase.Refresh()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        '        ChaXun("")
        ChaXun("日期>='" & Me.myMinDate.Value & "' and 日期<='" & Me.myMaxDate.Value & "' ")
        Me.btnSave.Enabled = True
    End Sub

    Private Sub ChaXun(ByVal sCase As String)
        '处理textcase
        Dim sTextCase As String
        sTextCase = Me.txtCase.Text
        If sTextCase = "" Then
            sTextCase = " 1=1 "
            sMRCase = " 1=1 "
        Else
            sMRCase = sTextCase
            sMRCase = sMRCase.Replace("'", Chr(34))
            '     sTextCase = sTextCase.Replace(" 等于 ", " = ").Replace(" 不等于 ", " <> ").Replace(" 大于 ", " > ").Replace(" 不大于 ", " <= ").Replace(" 小于 ", " < ").Replace(" 不小于 ", " >= ").Replace(" 开始于 ", " Like '").Replace(" 不开始于 ", " not Like '").Replace(" 包括 ", " Like '%").Replace(" 不包括 ", " Not Like '%")
            sTextCase = sTextCase.Replace(" 等于 ", " = ").Replace(" 不等于 ", " <> ").Replace(" 大于 ", " > ").Replace(" 不大于 ", " <= ").Replace(" 小于 ", " < ").Replace(" 不小于 ", " >= ").Replace(" 开始于 ", " Like ").Replace(" 不开始于 ", " not Like ").Replace(" 包括 ", " Like ").Replace(" 不包括 ", " Not Like ")

        End If

        Dim sSQL As String = ""
        'Dim sLie As String = ""
        Dim i As Integer
        sXSLie = ""
        For i = 0 To Me.lbSelected.Items.Count - 1
            sXSLie = sXSLie & "," & Me.lbSelected.Items(i).ToString
        Next

        If sXSLie = "" Then
            Exit Sub
        End If

        sXSLie = sXSLie.Remove(0, 1).ToString
        Dim dsDGV As DataSet
        sSQL = "select " & sXSLie & " from " & sBiao & "  where " & sTextCase & " and " & sCase
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        dsDGV = mdb.Reader(sSQL)
        'mdb.DataBind(Me.dgv, sSQL)

        Dim dt As DataTable
        dt = dsDGV.Tables(0)

        dt.Rows.Add()
        'dt.Rows(dt.Rows.Count - 1).Item(0) = "合计"

        If dt.Rows.Count = 1 Then
            '空表,不需要加合计
        Else
            For i = 0 To dt.Columns.Count - 1

                Dim j As Integer
                Dim ihj As Double = 0
                Try
                    For j = 0 To dt.Rows.Count - 2
                        If IsDBNull(dt.Rows(j).Item(i)) Then
                            Continue For
                        End If
                        ihj = ihj + dt.Rows(j).Item(i)
                    Next
                Catch ex As Exception
                    Continue For
                End Try

                Try
                    dt.Rows(dt.Rows.Count - 1).Item(i) = ihj
                Catch ex As Exception

                End Try
            Next
        End If


        Me.dgv.DataSource = dt.DefaultView
        'MsgBox(sSQL)
    End Sub

    Private Sub PNLYC_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PNLYC.MouseHover
        If Me.cbYC.Checked Then
        Else
            Me.SplitContainer1.Panel1Collapsed = False
        End If
    End Sub

    Private Sub dgv_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.MouseHover
        If Me.cbYC.Checked Then
        Else
            Me.SplitContainer1.Panel1Collapsed = True
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Me.txtCase.Clear()

        Me.rbAnd.Enabled = False
        Me.rbOr.Enabled = False

        Me.btnIsert.Text = "插入""("""
    End Sub

    Private Sub btnLieSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLieSetup.Click
        Dim dlg As New FKXSLie.LieSetup(Me.dgv, True, False, "")
        dlg.ShowDialog()

    End Sub

    Private Function GetSaveLieSetup() As String
        Dim sLieSetup As String = ""
        Dim i As Integer
        For i = 0 To dgv.Columns.Count - 1
            sLieSetup = sLieSetup & CType(Me.dgv.Columns(i).Visible, Integer) & "," & Me.dgv.Columns(i).HeaderText & "," & Int(Me.dgv.Columns(i).Width * 100 / Me.dgv.Width) & "," & Me.dgv.Columns(i).DefaultCellStyle.Format & ","
        Next
        sLieSetup = sLieSetup.Remove(sLieSetup.Length - 1, 1)
        Return sLieSetup
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim slie As String
        slie = GetSaveLieSetup()

        Dim dlg As New FKMXBName
        Dim sMXB As String
        Dim sMXBShuoMing As String
        Dim sGeShiFile As String
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sMXB = dlg.txtMXBiao.Text
            If sMXB = "" Then
                MsgBox("报表名称不能为空!", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
            sMXBShuoMing = dlg.txtShuoMing.Text & ",,," & dlg.txtBianhao.Text & "," & dlg.txtKehu.Text & "," & dlg.txtXingHao.Text
            sGeShiFile = dlg.TextBox1.Text
        Else
            Exit Sub
        End If
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.bExsit("select * from ZDYBB WHERE CONTEXT='" & SMXB & "'") Then
            '同名报表已存在
            If MsgBox("同名报表已经存在,是否覆盖?", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then
                '保存报表
                If dlg.cbCCGC.Checked Then '保存为存储过程
                    sBiao = "存储过程"

                    mdb.Write("update ZDYBB set TiShi='" & sMXBShuoMing & "',Biao='" & sBiao & "',Lie='',MRTiaoJian='" & dlg.txtCCGCName.Text & "',liesetup='',GeShiFile='" & sGeShiFile & "' where Context='" & sMXB & "'")
                Else
                    mdb.Write("update ZDYBB set TiShi='" & sMXBShuoMing & "',Biao='" & sBiao & "',Lie='" & sXSLie & "',MRTiaoJian='" & sMRCase & "',liesetup='" & slie & "',GeShiFile='" & sGeShiFile & "' where Context='" & sMXB & "'")
                End If
            Else
                Exit Sub
            End If
        Else
            '保存报表
            If dlg.cbCCGC.Checked Then '保存为存储过程
                sBiao = "存储过程"
                mdb.Write("insert into ZDYBB (ConText,ParentID,[Index],TiShi,isBaoBiao,isMingXi,Biao,Lie,MRTiaoJian,LieSetup,GeShiFile) VALUES ('" & sMXB & "',1,1,'" & sMXBShuoMing & "','TRUE','TRUE','" & sBiao & "','','" & dlg.txtCCGCName.Text & "','','" & sGeShiFile & "')")
            Else
                mdb.Write("insert into ZDYBB (ConText,ParentID,[Index],TiShi,isBaoBiao,isMingXi,Biao,Lie,MRTiaoJian,LieSetup,GeShiFile) VALUES ('" & sMXB & "',1,1,'" & sMXBShuoMing & "','TRUE','TRUE','" & sBiao & "','" & sXSLie & "','" & sMRCase & "','" & slie & "','" & sGeShiFile & "')")
            End If
        End If

        Dim dlgtj As New FKBBCS.FKBBCS(sMXB, sBiao)
        dlgtj.ShowDialog()

    End Sub

    Private Sub btnImortOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImortOut.Click
        Dim FKExcel As New FKPrn.ImportOut()
        FKExcel.Importout(Me.dgv)
    End Sub

End Class