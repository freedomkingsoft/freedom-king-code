﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKZHCX
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.rbKC = New System.Windows.Forms.RadioButton
        Me.rbGZ = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbYC = New System.Windows.Forms.CheckBox
        Me.rbPZ = New System.Windows.Forms.RadioButton
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.myMaxDate = New System.Windows.Forms.DateTimePicker
        Me.myMinDate = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.lbSelected = New System.Windows.Forms.ListBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lbLie = New System.Windows.Forms.ListBox
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel
        Me.btnAddCase = New System.Windows.Forms.Button
        Me.btnIsert = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.cbXiang = New System.Windows.Forms.ComboBox
        Me.cbYSF = New System.Windows.Forms.ComboBox
        Me.cbTiaoJian = New System.Windows.Forms.ComboBox
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel
        Me.rbOr = New System.Windows.Forms.RadioButton
        Me.rbAnd = New System.Windows.Forms.RadioButton
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnLieSetup = New System.Windows.Forms.Button
        Me.btnImortOut = New System.Windows.Forms.Button
        Me.txtCase = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel
        Me.PNLYC = New System.Windows.Forms.Panel
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.TableLayoutPanel10 = New System.Windows.Forms.TableLayoutPanel
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.TableLayoutPanel10.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TableLayoutPanel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.TableLayoutPanel9)
        Me.SplitContainer1.Size = New System.Drawing.Size(706, 658)
        Me.SplitContainer1.SplitterDistance = 217
        Me.SplitContainer1.SplitterWidth = 3
        Me.SplitContainer1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel4, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel5, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel8, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.txtCase, 0, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 135.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(213, 654)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.Controls.Add(Me.rbKC, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.rbGZ, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cbYC, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.rbPZ, 2, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(207, 74)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'rbKC
        '
        Me.rbKC.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rbKC.AutoSize = True
        Me.rbKC.CheckAlign = System.Drawing.ContentAlignment.TopCenter
        Me.rbKC.Location = New System.Drawing.Point(3, 33)
        Me.rbKC.Name = "rbKC"
        Me.rbKC.Size = New System.Drawing.Size(60, 29)
        Me.rbKC.TabIndex = 0
        Me.rbKC.TabStop = True
        Me.rbKC.Text = "查询库存表"
        Me.rbKC.UseVisualStyleBackColor = True
        '
        'rbGZ
        '
        Me.rbGZ.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rbGZ.AutoSize = True
        Me.rbGZ.CheckAlign = System.Drawing.ContentAlignment.TopCenter
        Me.rbGZ.Location = New System.Drawing.Point(69, 33)
        Me.rbGZ.Name = "rbGZ"
        Me.rbGZ.Size = New System.Drawing.Size(60, 29)
        Me.rbGZ.TabIndex = 1
        Me.rbGZ.TabStop = True
        Me.rbGZ.Text = "查询工资表"
        Me.rbGZ.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "选择源表"
        '
        'cbYC
        '
        Me.cbYC.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbYC.AutoSize = True
        Me.cbYC.Checked = True
        Me.cbYC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.TableLayoutPanel2.SetColumnSpan(Me.cbYC, 2)
        Me.cbYC.Location = New System.Drawing.Point(132, 3)
        Me.cbYC.Name = "cbYC"
        Me.cbYC.Size = New System.Drawing.Size(72, 16)
        Me.cbYC.TabIndex = 3
        Me.cbYC.Text = "总是显示"
        Me.cbYC.UseVisualStyleBackColor = True
        '
        'rbPZ
        '
        Me.rbPZ.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rbPZ.AutoSize = True
        Me.rbPZ.CheckAlign = System.Drawing.ContentAlignment.TopCenter
        Me.rbPZ.Location = New System.Drawing.Point(135, 33)
        Me.rbPZ.Name = "rbPZ"
        Me.rbPZ.Size = New System.Drawing.Size(69, 29)
        Me.rbPZ.TabIndex = 1
        Me.rbPZ.TabStop = True
        Me.rbPZ.Text = "查询凭证表"
        Me.rbPZ.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.myMaxDate, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.myMinDate, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Label6, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 83)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(207, 114)
        Me.TableLayoutPanel3.TabIndex = 5
        '
        'myMaxDate
        '
        Me.myMaxDate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMaxDate.Location = New System.Drawing.Point(43, 87)
        Me.myMaxDate.Name = "myMaxDate"
        Me.myMaxDate.Size = New System.Drawing.Size(120, 21)
        Me.myMaxDate.TabIndex = 23
        '
        'myMinDate
        '
        Me.myMinDate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMinDate.Location = New System.Drawing.Point(43, 30)
        Me.myMinDate.Name = "myMinDate"
        Me.myMinDate.Size = New System.Drawing.Size(120, 21)
        Me.myMinDate.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(95, 62)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(17, 12)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "至"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 12)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "设定起止日期:"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.lbSelected, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label4, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.lbLie, 0, 1)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 203)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(207, 143)
        Me.TableLayoutPanel4.TabIndex = 6
        '
        'lbSelected
        '
        Me.lbSelected.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbSelected.FormattingEnabled = True
        Me.lbSelected.ItemHeight = 12
        Me.lbSelected.Location = New System.Drawing.Point(106, 23)
        Me.lbSelected.Name = "lbSelected"
        Me.lbSelected.Size = New System.Drawing.Size(98, 112)
        Me.lbSelected.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 4)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 12)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "可选列"
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(106, 4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 12)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "已选列"
        '
        'lbLie
        '
        Me.lbLie.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbLie.FormattingEnabled = True
        Me.lbLie.ItemHeight = 12
        Me.lbLie.Location = New System.Drawing.Point(3, 23)
        Me.lbLie.Name = "lbLie"
        Me.lbLie.Size = New System.Drawing.Size(97, 112)
        Me.lbLie.TabIndex = 4
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.18841!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.81159!))
        Me.TableLayoutPanel5.Controls.Add(Me.TableLayoutPanel7, 0, 4)
        Me.TableLayoutPanel5.Controls.Add(Me.Label5, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Label7, 0, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.Label8, 0, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.cbXiang, 1, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.cbYSF, 1, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.cbTiaoJian, 1, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.TableLayoutPanel6, 0, 3)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(0, 349)
        Me.TableLayoutPanel5.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 5
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(213, 135)
        Me.TableLayoutPanel5.TabIndex = 7
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 2
        Me.TableLayoutPanel5.SetColumnSpan(Me.TableLayoutPanel7, 2)
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.btnAddCase, 1, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.btnIsert, 0, 0)
        Me.TableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(0, 100)
        Me.TableLayoutPanel7.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 1
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(213, 35)
        Me.TableLayoutPanel7.TabIndex = 6
        '
        'btnAddCase
        '
        Me.btnAddCase.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAddCase.AutoSize = True
        Me.btnAddCase.Location = New System.Drawing.Point(121, 3)
        Me.btnAddCase.Name = "btnAddCase"
        Me.btnAddCase.Size = New System.Drawing.Size(77, 29)
        Me.btnAddCase.TabIndex = 0
        Me.btnAddCase.Text = "增加条件"
        Me.btnAddCase.UseVisualStyleBackColor = True
        '
        'btnIsert
        '
        Me.btnIsert.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnIsert.AutoSize = True
        Me.btnIsert.Location = New System.Drawing.Point(14, 3)
        Me.btnIsert.Name = "btnIsert"
        Me.btnIsert.Size = New System.Drawing.Size(77, 29)
        Me.btnIsert.TabIndex = 0
        Me.btnIsert.Text = "插入""("""
        Me.btnIsert.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(17, 12)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "项"
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(4, 31)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 12)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "运算符"
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(4, 56)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 12)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "条件值"
        '
        'cbXiang
        '
        Me.cbXiang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbXiang.FormattingEnabled = True
        Me.cbXiang.Location = New System.Drawing.Point(52, 3)
        Me.cbXiang.Name = "cbXiang"
        Me.cbXiang.Size = New System.Drawing.Size(153, 20)
        Me.cbXiang.TabIndex = 4
        '
        'cbYSF
        '
        Me.cbYSF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbYSF.FormattingEnabled = True
        Me.cbYSF.Items.AddRange(New Object() {"等于", "不等于", "大于", "小于", "不大于", "不小于", "开始于", "不开始于", "包括", "不包括"})
        Me.cbYSF.Location = New System.Drawing.Point(52, 28)
        Me.cbYSF.Name = "cbYSF"
        Me.cbYSF.Size = New System.Drawing.Size(153, 20)
        Me.cbYSF.TabIndex = 4
        '
        'cbTiaoJian
        '
        Me.cbTiaoJian.FormattingEnabled = True
        Me.cbTiaoJian.Location = New System.Drawing.Point(52, 53)
        Me.cbTiaoJian.Name = "cbTiaoJian"
        Me.cbTiaoJian.Size = New System.Drawing.Size(153, 20)
        Me.cbTiaoJian.TabIndex = 4
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel5.SetColumnSpan(Me.TableLayoutPanel6, 2)
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.rbOr, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.rbAnd, 0, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(0, 75)
        Me.TableLayoutPanel6.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(213, 25)
        Me.TableLayoutPanel6.TabIndex = 5
        '
        'rbOr
        '
        Me.rbOr.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rbOr.AutoSize = True
        Me.rbOr.Location = New System.Drawing.Point(136, 4)
        Me.rbOr.Name = "rbOr"
        Me.rbOr.Size = New System.Drawing.Size(47, 16)
        Me.rbOr.TabIndex = 1
        Me.rbOr.Text = "或者"
        Me.rbOr.UseVisualStyleBackColor = True
        '
        'rbAnd
        '
        Me.rbAnd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rbAnd.AutoSize = True
        Me.rbAnd.Checked = True
        Me.rbAnd.Location = New System.Drawing.Point(29, 4)
        Me.rbAnd.Name = "rbAnd"
        Me.rbAnd.Size = New System.Drawing.Size(47, 16)
        Me.rbAnd.TabIndex = 0
        Me.rbAnd.TabStop = True
        Me.rbAnd.Text = "并且"
        Me.rbAnd.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.ColumnCount = 3
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel8.Controls.Add(Me.btnClear, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.btnSave, 1, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.btnOK, 2, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.btnLieSetup, 2, 1)
        Me.TableLayoutPanel8.Controls.Add(Me.btnImortOut, 0, 1)
        Me.TableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(3, 572)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 2
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(207, 79)
        Me.TableLayoutPanel8.TabIndex = 8
        '
        'btnClear
        '
        Me.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnClear.AutoSize = True
        Me.btnClear.Location = New System.Drawing.Point(3, 7)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(63, 24)
        Me.btnClear.TabIndex = 0
        Me.btnClear.Text = "重置"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSave.AutoSize = True
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(72, 7)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(63, 24)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "保存"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnOK.AutoSize = True
        Me.btnOK.Location = New System.Drawing.Point(141, 7)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(63, 24)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "查询"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnLieSetup
        '
        Me.btnLieSetup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnLieSetup.AutoSize = True
        Me.btnLieSetup.Location = New System.Drawing.Point(142, 48)
        Me.btnLieSetup.Name = "btnLieSetup"
        Me.btnLieSetup.Size = New System.Drawing.Size(60, 22)
        Me.btnLieSetup.TabIndex = 0
        Me.btnLieSetup.Text = "设置列"
        Me.btnLieSetup.UseVisualStyleBackColor = True
        '
        'btnImortOut
        '
        Me.btnImortOut.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnImortOut.AutoSize = True
        Me.TableLayoutPanel8.SetColumnSpan(Me.btnImortOut, 2)
        Me.btnImortOut.Location = New System.Drawing.Point(37, 48)
        Me.btnImortOut.Name = "btnImortOut"
        Me.btnImortOut.Size = New System.Drawing.Size(63, 22)
        Me.btnImortOut.TabIndex = 0
        Me.btnImortOut.Text = "导出数据"
        Me.btnImortOut.UseVisualStyleBackColor = True
        '
        'txtCase
        '
        Me.txtCase.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCase.Location = New System.Drawing.Point(3, 487)
        Me.txtCase.Multiline = True
        Me.txtCase.Name = "txtCase"
        Me.txtCase.ReadOnly = True
        Me.txtCase.Size = New System.Drawing.Size(207, 79)
        Me.txtCase.TabIndex = 9
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 2
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5.0!))
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.Controls.Add(Me.PNLYC, 0, 0)
        Me.TableLayoutPanel9.Controls.Add(Me.pnlMain, 1, 0)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 1
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(482, 654)
        Me.TableLayoutPanel9.TabIndex = 0
        '
        'PNLYC
        '
        Me.PNLYC.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.PNLYC.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PNLYC.Location = New System.Drawing.Point(0, 0)
        Me.PNLYC.Margin = New System.Windows.Forms.Padding(0)
        Me.PNLYC.Name = "PNLYC"
        Me.PNLYC.Size = New System.Drawing.Size(5, 654)
        Me.PNLYC.TabIndex = 0
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.TableLayoutPanel10)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(5, 0)
        Me.pnlMain.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(477, 654)
        Me.pnlMain.TabIndex = 1
        '
        'TableLayoutPanel10
        '
        Me.TableLayoutPanel10.ColumnCount = 1
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel10.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel10.Name = "TableLayoutPanel10"
        Me.TableLayoutPanel10.RowCount = 2
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.94801!))
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.05199!))
        Me.TableLayoutPanel10.Size = New System.Drawing.Size(477, 654)
        Me.TableLayoutPanel10.TabIndex = 1
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(0, 136)
        Me.dgv.Margin = New System.Windows.Forms.Padding(0)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowTemplate.Height = 23
        Me.dgv.Size = New System.Drawing.Size(477, 518)
        Me.dgv.TabIndex = 0
        '
        'FKZHCX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 658)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "FKZHCX"
        Me.Text = "FKZHCX"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel7.PerformLayout()
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel8.PerformLayout()
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.TableLayoutPanel10.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents rbGZ As System.Windows.Forms.RadioButton
    Friend WithEvents rbKC As System.Windows.Forms.RadioButton
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents myMaxDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents myMinDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbLie As System.Windows.Forms.ListBox
    Friend WithEvents lbSelected As System.Windows.Forms.ListBox
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbXiang As System.Windows.Forms.ComboBox
    Friend WithEvents cbYSF As System.Windows.Forms.ComboBox
    Friend WithEvents cbTiaoJian As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel7 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents rbOr As System.Windows.Forms.RadioButton
    Friend WithEvents rbAnd As System.Windows.Forms.RadioButton
    Friend WithEvents btnAddCase As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel8 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents txtCase As System.Windows.Forms.TextBox
    Friend WithEvents btnIsert As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel9 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents PNLYC As System.Windows.Forms.Panel
    Friend WithEvents cbYC As System.Windows.Forms.CheckBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnLieSetup As System.Windows.Forms.Button
    Friend WithEvents btnImortOut As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel10 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents rbPZ As System.Windows.Forms.RadioButton
End Class
