﻿Imports System.Windows.Forms

Public Class FKMXBName

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim dlg As New System.Windows.Forms.OpenFileDialog
        dlg.DefaultExt = ".xls"
        dlg.InitialDirectory = My.Application.Info.DirectoryPath & "\Templates"
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.TextBox1.Clear()

            Me.TextBox1.Text = dlg.FileName.Remove(0, dlg.InitialDirectory.Length + 1)
        End If
    End Sub

    Private Sub cbCCGC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCCGC.CheckedChanged
        If Me.cbCCGC.Checked Then
            Me.txtCCGCName.Enabled = True
        Else
            Me.txtCCGCName.Enabled = False
        End If
    End Sub
End Class
