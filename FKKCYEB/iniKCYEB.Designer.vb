﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class iniKCYEB
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgKMYE = New System.Windows.Forms.DataGridView
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.lblDanWei = New System.Windows.Forms.Label
        Me.lblShijian = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbCangKu = New System.Windows.Forms.ComboBox
        Me.lblShuoming = New System.Windows.Forms.Label
        Me.cbCangKuID = New System.Windows.Forms.ComboBox
        Me.cbCKShuoMing = New System.Windows.Forms.ComboBox
        Me.btnLieSetup = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.cbWeiWaiKeHu = New System.Windows.Forms.ComboBox
        CType(Me.dgKMYE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgKMYE
        '
        Me.dgKMYE.AllowUserToAddRows = False
        Me.dgKMYE.AllowUserToDeleteRows = False
        Me.dgKMYE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgKMYE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.NullValue = "0"
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgKMYE.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgKMYE.Location = New System.Drawing.Point(1, 129)
        Me.dgKMYE.Name = "dgKMYE"
        Me.dgKMYE.RowTemplate.Height = 23
        Me.dgKMYE.Size = New System.Drawing.Size(734, 367)
        Me.dgKMYE.TabIndex = 0
        Me.dgKMYE.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(241, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(162, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "初始化库存余额装入"
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSave.Location = New System.Drawing.Point(313, 8)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(99, 37)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "保存设置"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button3.Location = New System.Drawing.Point(572, 8)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(74, 37)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "退出"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'lblDanWei
        '
        Me.lblDanWei.AutoSize = True
        Me.lblDanWei.Location = New System.Drawing.Point(14, 54)
        Me.lblDanWei.Name = "lblDanWei"
        Me.lblDanWei.Size = New System.Drawing.Size(41, 12)
        Me.lblDanWei.TabIndex = 3
        Me.lblDanWei.Text = "单位："
        '
        'lblShijian
        '
        Me.lblShijian.AutoSize = True
        Me.lblShijian.Location = New System.Drawing.Point(491, 54)
        Me.lblShijian.Name = "lblShijian"
        Me.lblShijian.Size = New System.Drawing.Size(41, 12)
        Me.lblShijian.TabIndex = 3
        Me.lblShijian.Text = "时间："
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(233, 12)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "请首先选择一个要装入初始化数据的仓库："
        '
        'cbCangKu
        '
        Me.cbCangKu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbCangKu.FormattingEnabled = True
        Me.cbCangKu.Location = New System.Drawing.Point(239, 88)
        Me.cbCangKu.Name = "cbCangKu"
        Me.cbCangKu.Size = New System.Drawing.Size(161, 20)
        Me.cbCangKu.TabIndex = 4
        '
        'lblShuoming
        '
        Me.lblShuoming.AutoSize = True
        Me.lblShuoming.Location = New System.Drawing.Point(406, 91)
        Me.lblShuoming.Name = "lblShuoming"
        Me.lblShuoming.Size = New System.Drawing.Size(65, 12)
        Me.lblShuoming.TabIndex = 3
        Me.lblShuoming.Text = "仓库说明："
        '
        'cbCangKuID
        '
        Me.cbCangKuID.FormattingEnabled = True
        Me.cbCangKuID.Location = New System.Drawing.Point(573, 34)
        Me.cbCangKuID.Name = "cbCangKuID"
        Me.cbCangKuID.Size = New System.Drawing.Size(66, 20)
        Me.cbCangKuID.TabIndex = 4
        Me.cbCangKuID.Visible = False
        '
        'cbCKShuoMing
        '
        Me.cbCKShuoMing.FormattingEnabled = True
        Me.cbCKShuoMing.Location = New System.Drawing.Point(657, 34)
        Me.cbCKShuoMing.Name = "cbCKShuoMing"
        Me.cbCKShuoMing.Size = New System.Drawing.Size(66, 20)
        Me.cbCKShuoMing.TabIndex = 4
        Me.cbCKShuoMing.Visible = False
        '
        'btnLieSetup
        '
        Me.btnLieSetup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnLieSetup.Location = New System.Drawing.Point(71, 8)
        Me.btnLieSetup.Name = "btnLieSetup"
        Me.btnLieSetup.Size = New System.Drawing.Size(99, 37)
        Me.btnLieSetup.TabIndex = 2
        Me.btnLieSetup.Text = "显示列设置"
        Me.btnLieSetup.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnLieSetup, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnSave, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Button3, 2, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 502)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(734, 53)
        Me.TableLayoutPanel1.TabIndex = 5
        '
        'cbWeiWaiKeHu
        '
        Me.cbWeiWaiKeHu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbWeiWaiKeHu.FormattingEnabled = True
        Me.cbWeiWaiKeHu.Location = New System.Drawing.Point(558, 88)
        Me.cbWeiWaiKeHu.Name = "cbWeiWaiKeHu"
        Me.cbWeiWaiKeHu.Size = New System.Drawing.Size(134, 20)
        Me.cbWeiWaiKeHu.TabIndex = 4
        Me.cbWeiWaiKeHu.Visible = False
        '
        'iniKCYEB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 551)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.cbCKShuoMing)
        Me.Controls.Add(Me.cbWeiWaiKeHu)
        Me.Controls.Add(Me.cbCangKuID)
        Me.Controls.Add(Me.cbCangKu)
        Me.Controls.Add(Me.lblShuoming)
        Me.Controls.Add(Me.lblShijian)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblDanWei)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgKMYE)
        Me.Name = "iniKCYEB"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "初始库存余额装入"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgKMYE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgKMYE As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents lblDanWei As System.Windows.Forms.Label
    Friend WithEvents lblShijian As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbCangKu As System.Windows.Forms.ComboBox
    Friend WithEvents lblShuoming As System.Windows.Forms.Label
    Friend WithEvents cbCangKuID As System.Windows.Forms.ComboBox
    Friend WithEvents cbCKShuoMing As System.Windows.Forms.ComboBox
    Friend WithEvents btnLieSetup As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cbWeiWaiKeHu As System.Windows.Forms.ComboBox
End Class
