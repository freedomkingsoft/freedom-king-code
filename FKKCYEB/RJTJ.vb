Imports System.Windows.Forms

Public Class RJTJ

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If Me.cbCangKuID.Items.Count = 0 Then
            MsgBox("当前你还没有设定仓库，所以无法使用仓库余额查询！", MsgBoxStyle.Information, "无法查询")
            Me.DialogResult = Windows.Forms.DialogResult.No
            Me.Close()
        End If

        '首先进行对条件的验证
        If (Me.myNian.Value = FKG.myselfG.dQiYongRiQi.Year And Me.myYue.Value >= FKG.myselfG.dQiYongRiQi.Month) OrElse Me.myNian.Value > FKG.myselfG.dQiYongRiQi.Year Then
            If Me.myYue.Value <= Me.myYue2.Value Then

            Else
                MsgBox("截止月份不能小于开始月份", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
        Else
            MsgBox("开始时间不能早于启用日期", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If
        If Me.myKMDM.Text = "" Then
            MsgBox("请选择一个科目", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub MXTJ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.myNian.Value = FKG.myselfG.NianFen
        If myNian.Value = FKG.myselfG.dQiYongRiQi.Year Then
            Me.myYue.Value = FKG.myselfG.dQiYongRiQi.Month
        End If
        Me.myYue2.Value = FKG.myselfG.YueFen

        Me.ActiveControl = Me.myKMDM

        FillCangKu()
    End Sub

    Private Sub myKMDM_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles myKMDM.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.ActiveControl = Me.OK_Button
        End If
        bValided = False
    End Sub

    Dim bValided As Boolean
    Private Sub myKMDM_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles myKMDM.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            getKMDM()
        End If
    End Sub

    Private Sub myKMDM_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles myKMDM.Validating
        If bValided Then
            Exit Sub
        End If

        If Me.myKMDM.Text = "" Then
        Else
            Dim sKMQM As String
            sKMQM = FKF.FKF.getKMQM(" KMDM='" & Me.myKMDM.Text & "' and HSLBKC='true'", False)
            If sKMQM = "" Then
                getKMDM()
            Else
                Me.myKMQM.Text = sKMQM
                '确定是否显示数量
                'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                'If mdb.Reader("select zygs from kmdm where nian=" & FKG.myselfG.NianFen & " and kmdm='" & Me.myKMDM.Text & "'").Tables(0).Rows(0).Item(0) = 6 Then
                '    Me.myXSSL.Enabled = False
                '    Me.myXSSL.Checked = False
                'Else
                '    Me.myXSSL.Enabled = True
                '    Me.myXSSL.Checked = True
                'End If
            End If
        End If
    End Sub

    Private Sub getKMDM()
        Dim dlg As New FKKM.FKXKM("HSLBKC='true'", False)
        dlg.sKey = Me.myKMDM.Text
        dlg.ShowDialog()
        Me.myKMDM.Text = dlg.myKM.sKMDM
        Me.myKMQM.Text = dlg.myKM.sKMQM
        'If dlg.myKM.iZYGS = 6 Then
        '    Me.myXSSL.Enabled = False
        '    Me.myXSSL.Checked = False
        'Else
        '    Me.myXSSL.Enabled = True
        '    Me.myXSSL.Checked = True
        'End If
        bValided = True
    End Sub

    Private Sub FillCangKu()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As New DataSet
        ds = mdb.Reader("select ID,Name,ShuoMing from CangkuInfo order by Name")
        Dim i As Integer

        Me.cbCangKu.Items.Add("所有仓库")
        Me.cbCangKuID.Items.Add(0)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.cbCangKu.Items.Add(ds.Tables(0).Rows(i).Item(1).ToString)
            Me.cbCangKuID.Items.Add(ds.Tables(0).Rows(i).Item(0))
        Next

        If Me.cbCangKu.Items.Count > 0 Then
            Me.cbCangKu.SelectedIndex = 0
        End If
    End Sub

    Private Sub cbCangKu_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCangKu.SelectedIndexChanged
        If IsNothing(Me.cbCangKu.SelectedIndex) = False Then
            Me.cbCangKuID.SelectedIndex = Me.cbCangKu.SelectedIndex
            If Me.cbCangKu.Text = "所有仓库" Then
                Me.myWCK.Visible = True
                Me.myFPK.Visible = True
            Else
                Me.myFPK.Visible = False
                Me.myWCK.Visible = False
            End If
        End If
    End Sub
End Class
