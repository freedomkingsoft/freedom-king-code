﻿Public Class KCYE

    Dim bLoad As Boolean = False '该变量用于表明当前窗体还没有真正加载，防止在加载之前由于SizeChange而引起不必要的动作。
    Private Sub KMYE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        Me.Label2.Text = Me.Label2.Text & "  " & FKG.myselfG.QiYeMingCheng
        Me.Label4.Text = Me.Label4.Text & "  " & FKG.myselfG.YongHu
        Me.Label5.Text = Me.Label5.Text & "  " & Today.ToShortDateString
        SetCase()
    End Sub



    Private Sub SetCase()
        Dim dlg As New FKKCYEB.ZZTJ
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            fillDG(dlg.myNian.Value, dlg.myYue.Value, dlg.myMinKM.Text, dlg.myMaxKM.Text, dlg.myXJKM.Checked, dlg.myFSKM.Checked, dlg.cbCangKuID.Text, dlg.myWCK.Checked, dlg.myFPK.Checked)

            Me.Label3.Text = Me.Label3.Text & "  " & dlg.myNian.Value & "年" & dlg.myYue.Value & "月"
            Me.Label6.Text = dlg.cbCangKu.Text
            Me.Label6.Tag = dlg.cbCangKuID.Text '保存仓库ID

            Me.LieXSsetup()
        Else
            Me.Close()
        End If
    End Sub

    Dim bWCK As Boolean = True
    Dim bFPK As Boolean = True

    '以下两个变量用于启动明细账和日记账时，期间与余额表保持一致。
    Dim iMXNian As Integer
    Dim iMXYue As Integer

    Private Sub fillDG(ByVal iNian As Integer, ByVal iYue As Integer, ByVal sMinKM As String, ByVal sMaxKM As String, ByVal bXSXJ As Boolean, ByVal bFaSheng As Boolean, ByVal iCangKuID As Object, ByVal bBKWCK As Boolean, ByVal bBKFPK As Boolean)

        Dim sCase As String = ""
        Dim sCaseID As String = ""

        bWCK = bBKWCK
        bFPK = bBKFPK

        iMXNian = iNian
        iMXYue = iYue

        'Dim sGroup As String

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If iCangKuID = 0 Then
            Dim dsKuCase As DataSet
            If bBKWCK Then
                dsKuCase = mdb.Reader("select ID from CangKuInfo where shuoming='外存库' ")
                Dim i As Integer
                For i = 0 To dsKuCase.Tables(0).Rows.Count - 1
                    sCase = sCase & " and 仓库<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                    sCaseID = sCaseID & " and CangKuID<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                Next
            Else

            End If

            If bBKFPK Then
                dsKuCase = mdb.Reader("select ID from CangKuInfo where shuoming='废品库' ")
                Dim i As Integer
                For i = 0 To dsKuCase.Tables(0).Rows.Count - 1
                    sCase = sCase & " and 仓库<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                    sCaseID = sCaseID & " and CangKuID<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                Next
            Else

            End If

            'sCase = ""
            'sCaseID = ""
            'sGroup = " ,仓库 "

        Else
            sCase = " and  仓库=" & iCangKuID
            sCaseID = " and CangKuID=" & iCangKuID
            'sGroup = ""
            'iCangKuID = "*"
        End If

        Dim sSQl(4) As String
        'Dim sLie As String

        ''sCase = " Nian=" & iNian & " and SFMJ='1' and CangKuID=" & iCangKuID & " and (kmdm.KMDM>='" & sMinKM & "' and KMDM.KMDM<='" & sMaxKM & "')"

        Dim ds As DataSet

        ds = mdb.ReadStoredProcedure(FKG.myselfG.asasR, "KCYEB", "@iNian," & iNian & ";@iYue," & iYue & ";@sMinKM," & sMinKM & ";@sMaxKM," & sMaxKM & ";@sCangku," & sCase & ";@sCangkuID," & sCaseID & "")

        ds.Merge(mdb.ReadStoredProcedure(FKG.myselfG.asasR, "KCYEB_FMJ", "@iNian," & iNian & ";@iYue," & iYue & ";@sMinKM," & sMinKM & ";@sMaxKM," & sMaxKM & ";@sCangku," & sCase & ";@sCangkuID," & sCaseID & ""))

        Dim dvRow As DataRow
        dvRow = ds.Tables(0).NewRow

        ds.Tables(0).Rows.Add(dvRow)

        Dim iRow, iCol As Integer
        Dim dvHJ As New DataView
        dvHJ = ds.Tables(0).DefaultView

        'dvHJ.RowFilter = " len(科目代码) = " & FKG.myselfG.sKMJS.Substring(0, 1).ToString
        'dvHJ.RowFilter = " len(科目代码) = " & sMinKM.Length
        dvHJ.RowFilter = " isMJKM='true'"
        'Me.dgYE.DataSource = dvHJ
        'Exit Sub

        For iCol = 4 To ds.Tables(0).Columns.Count - 1
            Dim dHJ As Decimal = 0
            'For iRow = 0 To ds.Tables(0).Rows.Count - 2
            For iRow = 0 To dvHJ.Count - 1
                'dHJ = dHJ + ds.Tables(0).Rows(iRow).Item(iCol)
                If IsDBNull(dvHJ.Item(iRow).Item(iCol)) Then
                Else
                    dHJ = dHJ + dvHJ.Item(iRow).Item(iCol)

                End If
            Next
            ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(iCol) = dHJ
        Next
        ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(1) = "合计"
        ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(2) = " "

        ds.Tables(0).Columns.Remove("isMJKM")

        Dim dv As DataView
        dv = ds.Tables(0).DefaultView

        Dim sFTJ As String = ""
        If bXSXJ Then
            '显示下级，无需特殊处理
            sFTJ = ""
        Else
            '不显示下级
            Dim iKMLong As Integer
            If sMinKM.Length > sMaxKM.Length Then
                iKMLong = sMinKM.Length
            Else
                iKMLong = sMaxKM.Length
            End If
            sFTJ = " (len(科目代码) <= " & iKMLong & " or 科目名称='合计')"
        End If

        If bFaSheng Then
            '只显示有发生的科目.只判断数量，不判断金额。20160108
            If sFTJ = "" Then
                sFTJ = "期初数量<>0 or 入库数量<>0 or 出库数量<>0 or 结余数量<>0 or 累计入库数量<>0 or 累计出库数量<>0"
            Else
                sFTJ = sFTJ & " and  (期初数量<>0 or 入库数量<>0 or 出库数量<>0 or 结余数量<>0 or 累计入库数量<>0 or 累计出库数量<>0)"
            End If
        Else
            '显示所有科目，无需特殊处理
        End If

        dv.RowFilter = sFTJ

        ''不显示0
        Dim i1, n1 As Int16
        For i1 = 0 To dv.Count - 1
            For n1 = 3 To ds.Tables(0).Columns.Count - 1
                If IsDBNull(dv.Item(i1).Item(n1)) Then
                    Continue For
                End If

                If dv.Item(i1).Item(n1) = 0 Then
                    dv.Item(i1).Item(n1) = DBNull.Value
                End If
            Next
        Next
        dv.Sort = "科目代码"
        Me.dgYE.DataSource = dv

        Dim i2 As Int16
        For i2 = 0 To Me.dgYE.Columns.Count - 1

        Next
        If Me.dgYE.RowCount > 0 Then
            Me.dgYE.Rows(Me.dgYE.Rows.Count - 1).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.AliceBlue, False)
        End If
    End Sub

    Private Sub dgYE_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgYE.CellDoubleClick
        If Me.dgYE.CurrentRow.Index = Me.dgYE.Rows.Count - 1 OrElse Me.dgYE.CurrentRow.Index = -1 Then
            Exit Sub
        End If
        Dim dlg As New FKKCYEB.KCMXZ(True)
        'dlg.FillDG(Me.dgYE.Rows(e.RowIndex).Cells(0).Value, FKG.myselfG.NianFen, 1, FKG.myselfG.YueFen, Me.Label6.Tag, True, True, Me.Label6.Text, bWCK, bFPK)
        Dim iBeginYue As Int16 = 1
        If iMXNian = FKG.myselfG.dQiYongRiQi.Year Then
            iBeginYue = FKG.myselfG.dQiYongRiQi.Month
        End If

        dlg.FillDG(Me.dgYE.Rows(e.RowIndex).Cells(0).Value, iMXNian, iBeginYue, iMXYue, Me.Label6.Tag, True, True, Me.Label6.Text, bWCK, bFPK)
        dlg.ShowDialog()
    End Sub

    Dim th As Threading.Thread

    Private Sub 导出_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出.Click
        th = New Threading.Thread(AddressOf FKPOUT)
        th.Start()
    End Sub

    Private Sub FKPOUT()
        Dim dlg As New FKPrn.ImportOut
        dlg.Importout(Me.dgYE)
    End Sub


    Private Sub 明细账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 明细账.Click
        If Me.dgYE.RowCount = 0 Then
            Exit Sub
        End If

        If Me.dgYE.CurrentRow.Index = -1 OrElse Me.dgYE.CurrentRow.Index = Me.dgYE.Rows.Count - 1 Then
            Exit Sub
        End If
        Dim dlg As New FKKCYEB.KCMXZ(True)

        Dim iBeginYue As Int16 = 1
        If iMXNian = FKG.myselfG.dQiYongRiQi.Year Then
            iBeginYue = FKG.myselfG.dQiYongRiQi.Month
        End If

        dlg.FillDG(Me.dgYE.CurrentRow.Cells(0).Value, iMXNian, iBeginYue, iMXYue, Me.Label6.Tag, True, True, Me.Label6.Text, bWCK, bFPK)
        dlg.ShowDialog()
    End Sub

    Private Sub 日记帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 日记帐.Click
        If Me.dgYE.RowCount = 0 Then
            Exit Sub
        End If

        If Me.dgYE.CurrentRow.Index = -1 OrElse Me.dgYE.CurrentRow.Index = Me.dgYE.Rows.Count - 1 Then
            Exit Sub
        End If
        Dim dlg As New FKKCYEB.KCRJZ(True)

        Dim iBeginYue As Int16 = 1
        If iMXNian = FKG.myselfG.dQiYongRiQi.Year Then
            iBeginYue = FKG.myselfG.dQiYongRiQi.Month
        End If

        dlg.FillDG(Me.dgYE.CurrentRow.Cells(0).Value, iMXNian, iBeginYue, iMXYue, Me.Label6.Tag, True, True, Me.Label6.Text, bWCK, bFPK)
        dlg.ShowDialog()
    End Sub

    Private Sub KMYE_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        GC.Collect()
    End Sub

    Private Sub KMYE_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsNothing(th) Then
        Else
            If th.IsAlive Then
                MsgBox("正在向Excel导出数据，请稍候...", MsgBoxStyle.Information, My.Application.Info.Title)
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub 列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 列设置.Click
        SetupLie(Me.dgYE, "")
    End Sub


    Private Sub LieXSsetup()
        Dim sLie As String
        Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
        sLie = xmlR.Read("iniKCYELie")

        Dim LSet As New FKXSLie.LieSetup(Me.dgYE, True, True, sLie)
        LSet.SetLie(False)
    End Sub

    Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
        Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then

            Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
            xmlR.SaveInnerText("iniKCYELie", dlg.sSave)
        End If
    End Sub

    Private Sub GZMXZ_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        If bLoad Then
            LieXSsetup()
        End If
    End Sub


    'Private Sub 列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 列设置.Click

    '    SetupLie(Me.dgYE, My.Settings.iniKCYELie)
    'End Sub


    'Private Sub LieXSsetup(ByVal sLie As String)

    '    Dim LSet As New FKXSLie.LieSetup(Me.dgYE, True, True, sLie)
    '    LSet.SetLie()
    'End Sub

    'Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
    '    Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
    '    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
    '        My.Settings.iniKCYELie = dlg.sSave
    '        My.Settings.Save()
    '    End If
    'End Sub

    'Private Sub KCYE_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
    '    LieXSsetup(My.Settings.iniKCYELie)
    'End Sub

    Private Sub 关闭_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 关闭.Click
        Me.Close()
    End Sub

End Class

''Dim iQSYue As Integer = FKG.myselfG.dQiYongRiQi.Month
''If iNian = FKG.myselfG.dQiYongRiQi.Year Then '启用年份
''    '取年初数，加起始月份的累积发生数量，
''    If iQSYue = 1 Then
''        '直接取年初数即可

''        sSQl(0) = "select '1' as isMJKM,科目代码,科目名称,单位,NCSL+JFDYQSL-DFDYQSL AS 期初数量,NCYE+JFDYQJE-DFDYQJE AS 期初余额,JFDYSL AS 入库数量,JFDYJE AS 入库金额,DFDYSL AS 出库数量,DFDYJE AS 出库金额,NCSL+JFDYQSL-DFDYQSL + JFDYSL - DFDYSL AS 结余数量,NCYE+JFDYQJE-DFDYQJE + JFDYJE-DFDYJE AS 结余金额,JFDYQSL+JFDYSL AS 累计入库数量,JFDYQJE+JFDYJE AS 累计入库金额,DFDYQSL+DFDYSL AS 累计出库数量,DFDYQJE+DFDYJE AS 累计出库金额 FROM (SELECT * FROM  (SELECT * FROM (SELECT 科目代码, 科目名称,单位,iif(isnull(sum(年初借方余额)),0,sum(年初借方余额)) as NCYE, iif(isnull(sum(年初借方数量)),0,sum(年初借方数量)) as NCSL FROM 库存余额表 WHERE 年份=" & iNian & sCaseID & " and SFMJ='true' and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "')  group by 科目代码,科目名称,单位) AS TNCYE LEFT JOIN ( SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYQSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYQJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYQSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYQJE from 库存表 WHERE 年份=" & iNian & " AND 月份<" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQFS ON TNCYE.科目代码=TDYQFS.科目DM) as TQCYE LEFT JOIN (SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYJE from 库存表 WHERE 年份=" & iNian & " AND 月份=" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYFS  ON TQCYE.科目代码=TDYFS.科目DM)"

''        sSQl(1) = "SELECT '0' as isMJKM,FMJKMDM AS 科目代码,FMJKMMC AS 科目名称,SUM(期初数量Q) as 期初数量,sum(期初余额Q) as 期初余额,SUM(入库数量Q) as 入库数量,SUM(入库金额Q) as 入库金额,SUM(出库数量Q) as 出库数量,SUM(出库金额Q) as 出库金额,SUM(结余数量Q) as 结余数量,SUM(结余金额Q) as 结余金额,SUM(累计入库数量Q) as 累计入库数量,SUM(累计入库金额Q) as 累计入库金额,SUM(累计出库数量Q) as 累计出库数量,sum(累计出库金额Q) as 累计出库金额 from (SELECT * FROM (select KMDM.KMDM AS FMJKMDM,KMDM.KMMC AS FMJKMMC FROM KMDM WHERE HSLBKC='true' AND NIAN= " & iNian & " AND SFMJ='false'  and (kmdm>='" & sMinKM & "' and kmdm<'" & sMaxKM + 1 & "')  ) AS FMJKMB LEFT JOIN ( select 科目代码,科目名称,NCSL+JFDYQSL-DFDYQSL AS 期初数量Q,NCYE+JFDYQJE-DFDYQJE AS 期初余额Q,JFDYSL AS 入库数量Q,JFDYJE AS 入库金额Q,DFDYSL AS 出库数量Q,DFDYJE AS 出库金额Q,NCSL+JFDYQSL-DFDYQSL + JFDYSL - DFDYSL AS 结余数量Q,NCYE+JFDYQJE-DFDYQJE + JFDYJE-DFDYJE AS 结余金额Q,JFDYQSL+JFDYSL AS 累计入库数量Q,JFDYQJE+JFDYJE AS 累计入库金额Q,DFDYQSL+DFDYSL AS 累计出库数量Q,DFDYQJE+DFDYJE AS 累计出库金额Q FROM (SELECT * FROM  (SELECT * FROM (SELECT 科目代码, 科目名称, iif(isnull(sum(年初借方余额)),0,sum(年初借方余额)) as NCYE, iif(isnull(sum(年初借方数量)),0,sum(年初借方数量)) as NCSL  FROM 库存余额表 WHERE 年份=" & iNian & sCaseID & " and SFMJ='true' and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "')   group by 科目代码,科目名称) AS TNCYE LEFT JOIN ( SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYQSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYQJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYQSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYQJE from 库存表 WHERE 年份=" & iNian & " AND 月份<" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQFS ON TNCYE.科目代码=TDYQFS.科目DM) as TQCYE LEFT JOIN (SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYJE from 库存表 WHERE 年份=" & iNian & " AND 月份=" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYFS  ON TQCYE.科目代码=TDYFS.科目DM)) AS TYEB ON (INSTR(TYEB.科目代码,FMJKMB.FMJKMDM)=1 )) as TMJKM group by FMJKMDM,FMJKMMC"
''    Else
''        '加上起始月份的累积发生额
''        sSQl(0) = "select '1' as isMJKM,科目代码,科目名称,单位,NCSL+QCJFFSSL+JFDYQSL-DFDYQSL AS 期初数量,NCYE+QCJFFSE+JFDYQJE-DFDYQJE AS 期初余额,JFDYSL AS 入库数量,JFDYJE AS 入库金额,DFDYSL AS 出库数量,DFDYJE AS 出库金额,NCSL+QCJFFSSL+JFDYQSL-DFDYQSL + JFDYSL - DFDYSL AS 结余数量,NCYE+QCJFFSE+JFDYQJE-DFDYQJE + JFDYJE-DFDYJE AS 结余金额,QCJFFSSL+JFDYQSL+JFDYSL AS 累计入库数量,QCJFFSE+JFDYQJE+JFDYJE AS 累计入库金额,DFDYQSL+DFDYSL AS 累计出库数量,DFDYQJE+DFDYJE AS 累计出库金额 FROM (SELECT * FROM  (SELECT * FROM (SELECT 科目代码, 科目名称,单位, iif(isnull(sum(年初借方余额)),0,sum(年初借方余额)) as NCYE, iif(isnull(sum(年初借方数量)),0,sum(年初借方数量)) as NCSL,IIF(ISNULL(SUM([" & iQSYue - 1 & "月借方累计发生数量])),0,SUM([" & iQSYue - 1 & "月借方累计发生数量])) AS QCJFFSSL,IIF(ISNULL(SUM([" & iQSYue - 1 & "月借方累计发生额])),0,SUM([" & iQSYue - 1 & "月借方累计发生额])) AS QCJFFSE  FROM 库存余额表 WHERE 年份=" & iNian & sCaseID & " and SFMJ='true' and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "')   group by 科目代码,科目名称,单位) AS TNCYE LEFT JOIN ( SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYQSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYQJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYQSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYQJE from 库存表 WHERE 年份=" & iNian & " AND 月份<" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQFS ON TNCYE.科目代码=TDYQFS.科目DM) as TQCYE LEFT JOIN (SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYJE from 库存表 WHERE 年份=" & iNian & " AND 月份=" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYFS  ON TQCYE.科目代码=TDYFS.科目DM)"

''        sSQl(1) = "SELECT '0' as isMJKM,FMJKMDM AS 科目代码,FMJKMMC AS 科目名称,SUM(期初数量Q) as 期初数量,sum(期初余额Q) as 期初余额,SUM(入库数量Q) as 入库数量,SUM(入库金额Q) as 入库金额,SUM(出库数量Q) as 出库数量,SUM(出库金额Q) as 出库金额,SUM(结余数量Q) as 结余数量,SUM(结余金额Q) as 结余金额,SUM(累计入库数量Q) as 累计入库数量,SUM(累计入库金额Q) as 累计入库金额,SUM(累计出库数量Q) as 累计出库数量,sum(累计出库金额Q) as 累计出库金额 from (SELECT * FROM (select KMDM.KMDM AS FMJKMDM,KMDM.KMMC AS FMJKMMC FROM KMDM WHERE HSLBKC='true' AND NIAN= " & iNian & " AND SFMJ='false'  and (kmdm>='" & sMinKM & "' and kmdm<'" & sMaxKM + 1 & "') ) AS FMJKMB LEFT JOIN ( select 科目代码,科目名称,NCSL+QCJFFSSL+JFDYQSL-DFDYQSL AS 期初数量Q,NCYE+QCJFFSE+JFDYQJE-DFDYQJE AS 期初余额Q,JFDYSL AS 入库数量Q,JFDYJE AS 入库金额Q,DFDYSL AS 出库数量Q,DFDYJE AS 出库金额Q,NCSL+QCJFFSSL+JFDYQSL-DFDYQSL + JFDYSL - DFDYSL AS 结余数量Q,NCYE+QCJFFSE+JFDYQJE-DFDYQJE + JFDYJE-DFDYJE AS 结余金额Q,JFDYQSL+JFDYSL AS 累计入库数量Q,JFDYQJE+JFDYJE AS 累计入库金额Q,DFDYQSL+DFDYSL AS 累计出库数量Q,DFDYQJE+DFDYJE AS 累计出库金额Q FROM (SELECT * FROM  (SELECT * FROM (SELECT 科目代码, 科目名称, iif(isnull(sum(年初借方余额)),0,sum(年初借方余额)) as NCYE, iif(isnull(sum(年初借方数量)),0,sum(年初借方数量)) as NCSL,IIF(ISNULL(SUM([" & iQSYue - 1 & "月借方累计发生数量])),0,SUM([" & iQSYue - 1 & "月借方累计发生数量])) AS QCJFFSSL,IIF(ISNULL(SUM([" & iQSYue - 1 & "月借方累计发生额])),0,SUM([" & iQSYue - 1 & "月借方累计发生额])) as  QCJFFSE   FROM 库存余额表 WHERE 年份=" & iNian & sCaseID & " and SFMJ='true' and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "')   group by 科目代码,科目名称) AS TNCYE LEFT JOIN ( SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYQSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYQJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYQSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYQJE from 库存表 WHERE 年份=" & iNian & " AND 月份<" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQFS ON TNCYE.科目代码=TDYQFS.科目DM) as TQCYE LEFT JOIN (SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYJE from 库存表 WHERE 年份=" & iNian & " AND 月份=" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYFS  ON TQCYE.科目代码=TDYFS.科目DM)) AS TYEB ON (INSTR(TYEB.科目代码,FMJKMB.FMJKMDM)=1 )) as TMJKM group by FMJKMDM,FMJKMMC"
''    End If
''Else
''    '不关心起始月份
''    sSQl(0) = "select '1' as isMJKM,科目代码,科目名称,单位,NCSL+JFDYQSL-DFDYQSL AS 期初数量,NCYE+JFDYQJE-DFDYQJE AS 期初余额,JFDYSL AS 入库数量,JFDYJE AS 入库金额,DFDYSL AS 出库数量,DFDYJE AS 出库金额,NCSL+JFDYQSL-DFDYQSL + JFDYSL - DFDYSL AS 结余数量,NCYE+JFDYQJE-DFDYQJE + JFDYJE-DFDYJE AS 结余金额,JFDYQSL+JFDYSL AS 累计入库数量,JFDYQJE+JFDYJE AS 累计入库金额,DFDYQSL+DFDYSL AS 累计出库数量,DFDYQJE+DFDYJE AS 累计出库金额 FROM (SELECT * FROM  (SELECT * FROM (SELECT 科目代码, 科目名称,单位, iif(isnull(sum(年初借方余额)),0,sum(年初借方余额)) as NCYE, iif(isnull(sum(年初借方数量)),0,sum(年初借方数量)) as NCSL FROM 库存余额表 WHERE 年份=" & iNian & sCaseID & " and SFMJ='true' and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "')   group by 科目代码,科目名称,单位) AS TNCYE LEFT JOIN ( SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYQSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYQJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYQSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYQJE from 库存表 WHERE 年份=" & iNian & " AND 月份<" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQFS ON TNCYE.科目代码=TDYQFS.科目DM) as TQCYE LEFT JOIN (SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYJE from 库存表 WHERE 年份=" & iNian & " AND 月份=" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYFS  ON TQCYE.科目代码=TDYFS.科目DM)"

''    sSQl(1) = "SELECT '0' as isMJKM,FMJKMDM AS 科目代码,FMJKMMC AS 科目名称,SUM(期初数量Q) as 期初数量,sum(期初余额Q) as 期初余额,SUM(入库数量Q) as 入库数量,SUM(入库金额Q) as 入库金额,SUM(出库数量Q) as 出库数量,SUM(出库金额Q) as 出库金额,SUM(结余数量Q) as 结余数量,SUM(结余金额Q) as 结余金额,SUM(累计入库数量Q) as 累计入库数量,SUM(累计入库金额Q) as 累计入库金额,SUM(累计出库数量Q) as 累计出库数量,sum(累计出库金额Q) as 累计出库金额 from (SELECT * FROM (select KMDM.KMDM AS FMJKMDM,KMDM.KMMC AS FMJKMMC FROM KMDM WHERE HSLBKC='true' AND NIAN= " & iNian & " AND SFMJ='false'  and (kmdm>='" & sMinKM & "' and kmdm<'" & sMaxKM + 1 & "') ) AS FMJKMB LEFT JOIN ( select 科目代码,科目名称,NCSL+JFDYQSL-DFDYQSL AS 期初数量Q,NCYE+JFDYQJE-DFDYQJE AS 期初余额Q,JFDYSL AS 入库数量Q,JFDYJE AS 入库金额Q,DFDYSL AS 出库数量Q,DFDYJE AS 出库金额Q,NCSL+JFDYQSL-DFDYQSL + JFDYSL - DFDYSL AS 结余数量Q,NCYE+JFDYQJE-DFDYQJE + JFDYJE-DFDYJE AS 结余金额Q,JFDYQSL+JFDYSL AS 累计入库数量Q,JFDYQJE+JFDYJE AS 累计入库金额Q,DFDYQSL+DFDYSL AS 累计出库数量Q,DFDYQJE+DFDYJE AS 累计出库金额Q FROM (SELECT * FROM  (SELECT * FROM (SELECT 科目代码, 科目名称, iif(isnull(sum(年初借方余额)),0,sum(年初借方余额)) as NCYE, iif(isnull(sum(年初借方数量)),0,sum(年初借方数量)) as NCSL FROM 库存余额表 WHERE 年份=" & iNian & sCaseID & " and SFMJ='true' and (科目代码>='" & sMinKM & "' and 科目代码<'" & sMaxKM + 1 & "')   group by 科目代码,科目名称) AS TNCYE LEFT JOIN ( SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYQSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYQJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYQSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYQJE from 库存表 WHERE 年份=" & iNian & " AND 月份<" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYQFS ON TNCYE.科目代码=TDYQFS.科目DM) as TQCYE LEFT JOIN (SELECT 科目DM,iif(isnull(sum(借方数量)),0,sum(借方数量)) as JFDYSL,iif(isnull(sum(借方金额)),0,sum(借方金额)) as JFDYJE, iif(isnull(sum(贷方数量)),0,sum(贷方数量)) as DFDYSL,iif(isnull(sum(贷方金额)),0,sum(贷方金额)) as DFDYJE from 库存表 WHERE 年份=" & iNian & " AND 月份=" & iYue & sCase & "   and (科目DM>='" & sMinKM & "' and 科目DM<'" & sMaxKM + 1 & "') group by 科目DM ) AS TDYFS  ON TQCYE.科目代码=TDYFS.科目DM)) AS TYEB ON (INSTR(TYEB.科目代码,FMJKMB.FMJKMDM)=1 )) as TMJKM group by FMJKMDM,FMJKMMC"

''End If

''ds = mdb.Reader(sSQl(0))

''Dim ds2 As DataSet
''ds2 = mdb.Reader(sSQl(1))

''ds.Merge(ds2)