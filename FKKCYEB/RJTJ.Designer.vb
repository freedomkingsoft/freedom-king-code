﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RJTJ
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.myKMQM = New System.Windows.Forms.TextBox
        Me.myKMDM = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.myYue = New System.Windows.Forms.NumericUpDown
        Me.myNian = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.myYue2 = New System.Windows.Forms.NumericUpDown
        Me.Label7 = New System.Windows.Forms.Label
        Me.cbCangKuID = New System.Windows.Forms.ComboBox
        Me.cbCangKu = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.myWCK = New System.Windows.Forms.CheckBox
        Me.myFPK = New System.Windows.Forms.CheckBox
        CType(Me.myYue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.myNian, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.myYue2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(96, 263)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(85, 44)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "确定"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(241, 275)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(69, 32)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "取消"
        '
        'myKMQM
        '
        Me.myKMQM.Location = New System.Drawing.Point(193, 210)
        Me.myKMQM.Name = "myKMQM"
        Me.myKMQM.ReadOnly = True
        Me.myKMQM.Size = New System.Drawing.Size(158, 21)
        Me.myKMQM.TabIndex = 12
        '
        'myKMDM
        '
        Me.myKMDM.Location = New System.Drawing.Point(88, 210)
        Me.myKMDM.Name = "myKMDM"
        Me.myKMDM.Size = New System.Drawing.Size(105, 21)
        Me.myKMDM.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(245, 161)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "月"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(170, 161)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(17, 12)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "年"
        '
        'myYue
        '
        Me.myYue.Location = New System.Drawing.Point(193, 159)
        Me.myYue.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.myYue.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.myYue.Name = "myYue"
        Me.myYue.Size = New System.Drawing.Size(46, 21)
        Me.myYue.TabIndex = 2
        Me.myYue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.myYue.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'myNian
        '
        Me.myNian.Location = New System.Drawing.Point(97, 159)
        Me.myNian.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.myNian.Minimum = New Decimal(New Integer() {2008, 0, 0, 0})
        Me.myNian.Name = "myNian"
        Me.myNian.Size = New System.Drawing.Size(64, 21)
        Me.myNian.TabIndex = 8
        Me.myNian.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.myNian.Value = New Decimal(New Integer() {2008, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(29, 213)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 12)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "科目代码："
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 161)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "时　间："
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(268, 161)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(17, 12)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "至"
        '
        'myYue2
        '
        Me.myYue2.Location = New System.Drawing.Point(291, 159)
        Me.myYue2.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.myYue2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.myYue2.Name = "myYue2"
        Me.myYue2.Size = New System.Drawing.Size(46, 21)
        Me.myYue2.TabIndex = 3
        Me.myYue2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.myYue2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(343, 161)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(17, 12)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "月"
        '
        'cbCangKuID
        '
        Me.cbCangKuID.FormattingEnabled = True
        Me.cbCangKuID.Location = New System.Drawing.Point(270, 102)
        Me.cbCangKuID.Name = "cbCangKuID"
        Me.cbCangKuID.Size = New System.Drawing.Size(60, 20)
        Me.cbCangKuID.TabIndex = 14
        Me.cbCangKuID.Visible = False
        '
        'cbCangKu
        '
        Me.cbCangKu.FormattingEnabled = True
        Me.cbCangKu.Location = New System.Drawing.Point(109, 102)
        Me.cbCangKu.Name = "cbCangKu"
        Me.cbCangKu.Size = New System.Drawing.Size(133, 20)
        Me.cbCangKu.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 105)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 12)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "仓  库："
        '
        'myWCK
        '
        Me.myWCK.AutoSize = True
        Me.myWCK.Checked = True
        Me.myWCK.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myWCK.Location = New System.Drawing.Point(84, 134)
        Me.myWCK.Name = "myWCK"
        Me.myWCK.Size = New System.Drawing.Size(96, 16)
        Me.myWCK.TabIndex = 19
        Me.myWCK.Text = "不包括外存库"
        Me.myWCK.UseVisualStyleBackColor = True
        '
        'myFPK
        '
        Me.myFPK.AutoSize = True
        Me.myFPK.Checked = True
        Me.myFPK.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myFPK.Location = New System.Drawing.Point(196, 134)
        Me.myFPK.Name = "myFPK"
        Me.myFPK.Size = New System.Drawing.Size(96, 16)
        Me.myFPK.TabIndex = 18
        Me.myFPK.Text = "不包括废品库"
        Me.myFPK.UseVisualStyleBackColor = True
        '
        'RJTJ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(377, 343)
        Me.Controls.Add(Me.myWCK)
        Me.Controls.Add(Me.myFPK)
        Me.Controls.Add(Me.cbCangKuID)
        Me.Controls.Add(Me.cbCangKu)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Cancel_Button)
        Me.Controls.Add(Me.OK_Button)
        Me.Controls.Add(Me.myKMQM)
        Me.Controls.Add(Me.myKMDM)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.myYue2)
        Me.Controls.Add(Me.myYue)
        Me.Controls.Add(Me.myNian)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "RJTJ"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "查看库存日记帐"
        CType(Me.myYue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.myNian, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.myYue2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents myKMQM As System.Windows.Forms.TextBox
    Friend WithEvents myKMDM As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents myYue As System.Windows.Forms.NumericUpDown
    Friend WithEvents myNian As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents myYue2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbCangKuID As System.Windows.Forms.ComboBox
    Friend WithEvents cbCangKu As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents myWCK As System.Windows.Forms.CheckBox
    Friend WithEvents myFPK As System.Windows.Forms.CheckBox

End Class
