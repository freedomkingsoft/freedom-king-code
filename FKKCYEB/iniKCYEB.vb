﻿Public Class iniKCYEB

    Private Sub iniKMYEB_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        fillLable()
        FillCangKu()
        FillWeiWaiKeHu()
        Me.Icon = FKG.myselfG.FKIcon
    End Sub

    Private Sub FillWeiWaiKeHu()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As New DataSet
        ds = mdb.Reader("select KMMC from KMDM where HSLBWW='true' AND SFMJ='true' AND NIAN=" & FKG.myselfG.NianFen)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.cbWeiWaiKeHu.Items.Add(ds.Tables(0).Rows(i).Item(0).ToString)
        Next

        If Me.cbWeiWaiKeHu.Items.Count > 0 Then
            Me.cbWeiWaiKeHu.SelectedIndex = 0
        End If
    End Sub

    Private Sub FillCangKu()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As New DataSet
        ds = mdb.Reader("select ID,Name,ShuoMing from CangkuInfo order by Name desc")
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.cbCangKu.Items.Add(ds.Tables(0).Rows(i).Item(1).ToString)
            Me.cbCangKuID.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Me.cbCKShuoMing.Items.Add(ds.Tables(0).Rows(i).Item(2).ToString)
        Next

        If Me.cbCangKu.Items.Count > 0 Then
            Me.cbCangKu.SelectedIndex = 0
        End If
    End Sub

    Private Sub cbCangKu_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCangKu.SelectedIndexChanged
        If IsNothing(Me.cbCangKu.SelectedIndex) = False Then
            Me.cbCangKuID.SelectedIndex = Me.cbCangKu.SelectedIndex
            Me.cbCKShuoMing.SelectedIndex = Me.cbCangKu.SelectedIndex

            Me.lblShuoming.Text = "仓库说明：" & Me.cbCKShuoMing.Text


            If Me.cbCKShuoMing.Text = "外存库" Then
                Me.cbWeiWaiKeHu.Visible = True
            Else
                Me.cbWeiWaiKeHu.Visible = False
            End If

            FillDG(Me.cbCangKuID.Text)

        End If
    End Sub

    Private Sub fillLable()
        Me.lblDanWei.Text = Me.lblDanWei.Text & FKG.myselfG.QiYeMingCheng
        Me.lblShijian.Text = "启用时间为：" & FKG.myselfG.dQiYongRiQi.ToShortDateString
    End Sub

    Dim iWeiWaiKeHu As String
    Private Sub FillDG(ByVal iCKID As Integer)
        '如果启用月份为1月，则只可以设置年初余额
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sSQL As String = ""
        Dim iSnian As Integer
        Dim iSyue As Integer
        iSnian = FKG.myselfG.dQiYongRiQi.Year
        iSyue = FKG.myselfG.dQiYongRiQi.Month
        Dim i As Integer

        If Me.cbWeiWaiKeHu.Visible Then
            If iSyue = 0 Then
                MsgBox("wrong")
            Else
                For i = 1 To 11
                    sSQL = sSQL & ", isNull( WWYE.jfsl" & i & " , 0 ) as [" & i & "月发生数量],isNull( WWYE.jfje" & i & " , 0 ) as [" & i & "月发生金额]"

                Next
                If Me.cbWeiWaiKeHu.SelectedIndex < 0 Then
                    MsgBox("必须指定委外核实的科目", MsgBoxStyle.Information, "提示")
                    Me.dgKMYE.DataSource = Nothing
                    Exit Sub
                End If
                iWeiWaiKeHu = mdb.Reader("select KMDM from KMDM where KMMC='" & Me.cbWeiWaiKeHu.Text & "' and Nian=" & FKG.myselfG.NianFen).Tables(0).Rows(0).Item(0)

                sSQL = "select KCKMDM.Name as 仓库名称,KCKMDM.kmdm as 科目代码,KCKMDM.kmmc as 科目名称,isNull( WWYE.ncjfsl , 0 ) as 年初数量,isNull( WWYE.ncjfye , 0 ) as 年初余额 " & sSQL & ",KCKMDM.SLDW as 单位 from (select CangKuInfo.ID as CangKuID,CangKuInfo.Name,KMDM.* from KMDM,CangKuInfo where KMDM.HSLBKC='true' and CangKuInfo.ID=" & iCKID & ") AS KCKMDM left join (SELECT * FROM WeiWaiYE where WWKHDM='" & iWeiWaiKeHu & "' AND NIAN=" & iSnian & ") AS WWYE on KCKMDM.kmdm=WWYE.kmdm and KCKMDM.CangKuID= WWYE.CangKuID where KCKMDM.sfmj='true' AND KCKMDM.nian=" & iSnian & " order by KCkmdm.kmdm"

            End If
        Else
            If iSyue = 0 Then
                MsgBox("wrong")
            Else
                For i = 1 To 11
                    sSQL = sSQL & ", isNull( kcye.jfsl" & i & " , 0 ) as [" & i & "月发生数量],isNull( kcye.jfje" & i & " , 0 ) as [" & i & "月发生金额]"

                Next
                sSQL = "select KCKMDM.Name as 仓库名称,KCKMDM.kmdm as 科目代码,KCKMDM.kmmc as 科目名称,isNull( kcye.ncjfsl , 0 ) as 年初数量,isNull( kcye.ncjfye , 0 ) as 年初余额 " & sSQL & ",KCKMDM.SLDW as 单位 from (select CangKuInfo.ID as CangKuID,CangKuInfo.Name,KMDM.* from KMDM,CangKuInfo where KMDM.HSLBKC='true' and CangKuInfo.ID=" & iCKID & ") AS KCKMDM left join kcye on KCKMDM.kmdm=kcye.kmdm and KCKMDM.CangKuID= KCYE.CangKuID and KCKMDM.NIAN=KCYE.NIAN where KCKMDM.sfmj='true' and KCKMDM.nian=" & iSnian & " order by KCkmdm.kmdm"

            End If
        End If


        Dim ds As DataSet
        ds = mdb.Reader(sSQL)
        Dim dr As DataRow
        dr = ds.Tables(0).NewRow
        dr.Item(2) = "合计"
        Dim iCol, iRow As Integer
        For iCol = 3 To ds.Tables(0).Columns.Count - 2
            dr.Item(iCol) = 0
            For iRow = 0 To ds.Tables(0).Rows.Count - 2
                dr.Item(iCol) = CType(dr.Item(iCol), Decimal) + CType(ds.Tables(0).Rows(iRow).Item(iCol), Decimal)
            Next

        Next
        ds.Tables(0).Rows.Add(dr)

        Me.dgKMYE.DataSource = ds.Tables(0).DefaultView
        'mdb.DataBind(Me.dgKMYE, sSQL)
        'Dim i As Integer
        For i = (iSyue) * 2 + 3 To 26
            Me.dgKMYE.Columns(i).Visible = False
        Next

        Me.dgKMYE.Columns(27).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgKMYE.Columns(27).ReadOnly = True

        For i = 0 To Me.dgKMYE.Columns.GetColumnCount(Windows.Forms.DataGridViewElementStates.Visible)
            Me.dgKMYE.Columns(i).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Next
        Me.dgKMYE.Columns(0).ReadOnly = True
        Me.dgKMYE.Columns(1).ReadOnly = True
        Me.dgKMYE.Columns(2).ReadOnly = True

        Me.dgKMYE.Rows(Me.dgKMYE.RowCount - 1).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.AliceBlue, False)

        LieXSsetup() ' 根据用户设置的默认值设置
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '首先确定是否可以更改初始余额
        '一旦启动月份结账，在初始化数据即不可更改
        Dim sSQL As String
        Dim iSnian As Integer
        Dim iSyue As Integer
        iSnian = FKG.myselfG.dQiYongRiQi.Year
        iSyue = FKG.myselfG.dQiYongRiQi.Month

        '库存余额不需要平
        ''最先确定借贷方是否平衡
        'If iSyue = 1 Then
        '    If PingHeng(True) Then
        '    Else
        '        Exit Sub
        '    End If
        'Else
        '    If PingHeng(False) Then
        '    Else
        '        Exit Sub
        '    End If
        'End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim mdbReader As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Try
            mdb.oleConnect.Open()
        Catch ex As Exception
            mdb.oleConnect.Close()
        End Try

        '首先判断帐套是否已经开始使用,但只需要判断启用月份是否结帐即可
        Dim ds As New DataSet
        sSQL = "select jiezhang from FKZQ where ksrq<='" & FKG.myselfG.dQiYongRiQi & "' and ZZRQ >='" & FKG.myselfG.dQiYongRiQi & "'"
        mdb.oleCommand.CommandText = sSQL
        mdb.oleDataAdapter.Fill(ds)

        If ds.Tables(0).Rows(0).Item(0) = 0 Then
            '可以进行初始化设置

            '第一步：删除当前年度的科目余额
            '将把所有客户的库存都删除,在保存时应从WeiWaiYE求合计值写入库存表
            sSQL = "delete from kcye where nian=" & iSnian & " and CangKuID=" & Me.cbCangKuID.Text
            mdb.oleCommand.CommandText = sSQL
            mdb.oleCommand.ExecuteNonQuery()

            '    '第二步：插入科目代码和科目余额
            Dim sLie As String = ""
            'sLie = "kmdm,nian,ncjfsl,ncjfye,ncdfsl,ncdfye"
            Dim i As Integer = 0
            'sZhi = "'" & Me.dgKMYE.Rows(i).Cells(0).Value & "'," & iSnian
            For i = 1 To 11
                sLie = sLie & ",jfsl" & i & ",jfje" & i
            Next

            If Me.cbWeiWaiKeHu.Visible AndAlso Me.cbWeiWaiKeHu.SelectedIndex >= 0 Then
                '需要同时写入客户外存表和库存余额表
                sSQL = "delete from WeiWaiYE where nian=" & iSnian & " and CangKuID=" & Me.cbCangKuID.Text & " and wwkhdm='" & iWeiWaiKeHu & "'"
                mdb.oleCommand.CommandText = sSQL
                mdb.oleCommand.ExecuteNonQuery()
                For i = 0 To Me.dgKMYE.Rows.Count - 2
                    sSQL = "insert into WeiWaiYE (CangKuID,kmdm,nian,wwkhdm,ncjfsl,ncjfye" & sLie & ") values (" & Me.cbCangKuID.Text & ",'" & Me.dgKMYE.Rows(i).Cells(1).Value & "'," & iSnian & ",'" & iWeiWaiKeHu & "'," & Me.dgKMYE.Rows(i).Cells(3).Value & "," & Me.dgKMYE.Rows(i).Cells(4).Value & "," & Me.dgKMYE.Rows(i).Cells(5).Value & "," & Me.dgKMYE.Rows(i).Cells(6).Value & "," & Me.dgKMYE.Rows(i).Cells(7).Value & "," & Me.dgKMYE.Rows(i).Cells(8).Value & "," & Me.dgKMYE.Rows(i).Cells(9).Value & "," & Me.dgKMYE.Rows(i).Cells(10).Value & "," & Me.dgKMYE.Rows(i).Cells(11).Value & "," & Me.dgKMYE.Rows(i).Cells(12).Value & "," & Me.dgKMYE.Rows(i).Cells(13).Value & "," & Me.dgKMYE.Rows(i).Cells(14).Value & "," & Me.dgKMYE.Rows(i).Cells(15).Value & "," & Me.dgKMYE.Rows(i).Cells(16).Value & "," & Me.dgKMYE.Rows(i).Cells(17).Value & "," & Me.dgKMYE.Rows(i).Cells(18).Value & "," & Me.dgKMYE.Rows(i).Cells(19).Value & "," & Me.dgKMYE.Rows(i).Cells(20).Value & "," & Me.dgKMYE.Rows(i).Cells(21).Value & "," & Me.dgKMYE.Rows(i).Cells(22).Value & "," & Me.dgKMYE.Rows(i).Cells(23).Value & "," & Me.dgKMYE.Rows(i).Cells(24).Value & "," & Me.dgKMYE.Rows(i).Cells(25).Value & "," & Me.dgKMYE.Rows(i).Cells(26).Value & ")"
                    mdb.oleCommand.CommandText = sSQL
                    mdb.oleCommand.ExecuteNonQuery()
                Next
            Else
                For i = 0 To Me.dgKMYE.Rows.Count - 2
                    sSQL = "insert into kcye (CangKuID,kmdm,nian,ncjfsl,ncjfye" & sLie & ") values (" & Me.cbCangKuID.Text & ",'" & Me.dgKMYE.Rows(i).Cells(1).Value & "'," & iSnian & "," & Me.dgKMYE.Rows(i).Cells(3).Value & "," & Me.dgKMYE.Rows(i).Cells(4).Value & "," & Me.dgKMYE.Rows(i).Cells(5).Value & "," & Me.dgKMYE.Rows(i).Cells(6).Value & "," & Me.dgKMYE.Rows(i).Cells(7).Value & "," & Me.dgKMYE.Rows(i).Cells(8).Value & "," & Me.dgKMYE.Rows(i).Cells(9).Value & "," & Me.dgKMYE.Rows(i).Cells(10).Value & "," & Me.dgKMYE.Rows(i).Cells(11).Value & "," & Me.dgKMYE.Rows(i).Cells(12).Value & "," & Me.dgKMYE.Rows(i).Cells(13).Value & "," & Me.dgKMYE.Rows(i).Cells(14).Value & "," & Me.dgKMYE.Rows(i).Cells(15).Value & "," & Me.dgKMYE.Rows(i).Cells(16).Value & "," & Me.dgKMYE.Rows(i).Cells(17).Value & "," & Me.dgKMYE.Rows(i).Cells(18).Value & "," & Me.dgKMYE.Rows(i).Cells(19).Value & "," & Me.dgKMYE.Rows(i).Cells(20).Value & "," & Me.dgKMYE.Rows(i).Cells(21).Value & "," & Me.dgKMYE.Rows(i).Cells(22).Value & "," & Me.dgKMYE.Rows(i).Cells(23).Value & "," & Me.dgKMYE.Rows(i).Cells(24).Value & "," & Me.dgKMYE.Rows(i).Cells(25).Value & "," & Me.dgKMYE.Rows(i).Cells(26).Value & ")"
                    mdb.oleCommand.CommandText = sSQL
                    mdb.oleCommand.ExecuteNonQuery()
                Next

            End If


            '第三步：核算非末级科目的初始余额
            '可以考虑是否在记账时写入数据()

            Dim sZhiJie As String = ""
            'Dim sZhiDai As String = ""
            For i = 1 To 11
                sZhiJie = sZhiJie & ",sum(jfsl" & i & "),SUM(jfje" & i & ")"
                'sZhiDai = sZhiDai & ",0,0,sum(dfsl" & i & ")-sum(jfsl" & i & "),SUM(dfje" & i & ")-SUM(jfje" & i & ")"
            Next

            Dim dsFMJKM As New DataSet
            sSQL = "select KMDM from KMDM where SFMJ='0' and HSLBKC='true' and nian=" & FKG.myselfG.NianFen
            mdb.oleCommand.CommandText = sSQL
            mdb.oleDataAdapter.SelectCommand = mdb.oleCommand
            mdb.oleDataAdapter.Fill(dsFMJKM)
            Dim sKMDM As String
            If Me.cbWeiWaiKeHu.Visible AndAlso Me.cbWeiWaiKeHu.SelectedIndex >= 0 Then
                For i = 0 To dsFMJKM.Tables(0).Rows.Count - 1
                    sKMDM = dsFMJKM.Tables(0).Rows(i).Item(0).ToString
                    sSQL = "insert into WeiWaiYE (CangKuID,kmdm,nian,wwkhdm,ncjfsl,ncjfye" & sLie & ")  select " & Me.cbCangKuID.Text & ",'" & sKMDM & "'," & iSnian & ",'" & iWeiWaiKeHu & "',sum(ncjfsl),sum(ncjfye)" & sZhiJie & " from WeiWaiYE where  CangKuID = " & Me.cbCangKuID.Text & "  and  WeiWaiYE.nian=" & iSnian & " and WeiWaiYE.WWKHDM='" & iWeiWaiKeHu & "' and WeiWaiYE.KMDM like '" & sKMDM & "%'"
                    mdb.oleCommand.CommandText = sSQL
                    mdb.oleCommand.ExecuteNonQuery()
                Next

                ''写入库存余额,合计各委外客户即可
                'sSQL = "insert into kcye (CangKuID,kmdm,nian,ncjfsl,ncjfye" & sLie & ")  select " & Me.cbCangKuID.Text & ",KMDM," & iSnian & ",sum(ncjfsl),sum(ncjfye)" & sZhiJie & " from WeiWaiYE where   WeiWaiYE.nian=" & iSnian & " and WeiWaiYE.WWKHDM='" & iWeiWaiKeHu & "' and   CangKuID = " & Me.cbCangKuID.Text & "  group by  kmdm"
                'mdb.oleCommand.CommandText = sSQL
                'mdb.oleCommand.ExecuteNonQuery()


                '写入库存余额,合计各委外客户即可,因为库存表中不分客户,所以应该将所有客户汇总到一起
                sSQL = "insert into kcye (CangKuID,kmdm,nian,ncjfsl,ncjfye" & sLie & ")  select " & Me.cbCangKuID.Text & ",KMDM," & iSnian & ",sum(ncjfsl),sum(ncjfye)" & sZhiJie & " from WeiWaiYE where   WeiWaiYE.nian=" & iSnian & " and   CangKuID = " & Me.cbCangKuID.Text & "  group by  kmdm"
                mdb.oleCommand.CommandText = sSQL
                mdb.oleCommand.ExecuteNonQuery()
            Else
                For i = 0 To dsFMJKM.Tables(0).Rows.Count - 1
                    sKMDM = dsFMJKM.Tables(0).Rows(i).Item(0).ToString
                    sSQL = "insert into kcye (CangKuID,kmdm,nian,ncjfsl,ncjfye" & sLie & ")  select " & Me.cbCangKuID.Text & ",'" & sKMDM & "'," & iSnian & ",sum(ncjfsl),sum(ncjfye)" & sZhiJie & " from KcYE where  CangKuID = " & Me.cbCangKuID.Text & "  and  KcYE.nian=" & iSnian & " and KcYE.KMDM like '" & sKMDM & "%'"
                    mdb.oleCommand.CommandText = sSQL
                    mdb.oleCommand.ExecuteNonQuery()
                Next
            End If

            MsgBox("库存余额初始化成功！", MsgBoxStyle.Information)

        Else
            MsgBox("系统已经开始使用，不能再修改初始余额！", MsgBoxStyle.Information)
            Exit Sub
        End If
        mdb.oleConnect.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Dim iZhiBeforChange As Decimal
    Private Sub dgKMYE_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgKMYE.CellBeginEdit
        If e.RowIndex = Me.dgKMYE.Rows.Count - 1 Or e.ColumnIndex <= 2 Then
            Exit Sub
        Else
            If IsDBNull(Me.dgKMYE.CurrentCell.Value) Then
                iZhiBeforChange = 0
            Else
                iZhiBeforChange = Me.dgKMYE.CurrentCell.Value
            End If
        End If
    End Sub

    Private Sub dgKMYE_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgKMYE.CellEndEdit
        If e.RowIndex = Me.dgKMYE.Rows.Count - 1 Or e.ColumnIndex <= 2 Then
            Exit Sub
        Else
            Dim dCurrCell As Decimal
            If IsDBNull(Me.dgKMYE.CurrentCell.Value) Then
                dCurrCell = 0
            Else
                dCurrCell = CType(Me.dgKMYE.CurrentCell.Value, Decimal)
            End If

            Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(e.ColumnIndex).Value = Me.dgKMYE.Rows(Me.dgKMYE.Rows.Count - 1).Cells(e.ColumnIndex).Value - iZhiBeforChange + dCurrCell
        End If

        If IsDBNull(Me.dgKMYE.CurrentCell.Value) Then
            Me.dgKMYE.CurrentCell.Value = 0
        End If
    End Sub

    Private Sub dgKMYE_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgKMYE.DataError
        Me.dgKMYE.CurrentCell.Value = 0
        e.Cancel = False
        'Return True
    End Sub

    Private Sub LieXSsetup()
        Dim sLie As String
        Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
        sLie = xmlR.Read("iniKCLie")

        If sLie = "" Then
            Dim iqsyue As Integer = FKG.myselfG.dQiYongRiQi.Month
            Dim i As Integer
            sLie = "1,,,,1,,,,1,,,,"
            For i = 0 To 11
                If i < iqsyue Then
                    sLie = sLie & "1,,,,1,,,,"
                Else
                    sLie = sLie & "2,,,,2,,,,"
                End If
            Next
            sLie = sLie & "1,,,"
            'sLie = sLie.Remove(sLie.Length - 1, 1)

            xmlR.SaveInnerText("iniKCLie", sLie)
        End If

        Dim LSet As New FKXSLie.LieSetup(Me.dgKMYE, True, True, sLie)
        LSet.SetLie(False)
    End Sub

    Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
        Dim dlg As New FKXSLie.LieSetup(dgv, True, True, sMorenlie)
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then

            Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
            xmlR.SaveInnerText("iniKCLie", dlg.sSave)
        End If
    End Sub

    Private Sub GZMXZ_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        LieXSsetup()
    End Sub

    Private Sub btnLieSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLieSetup.Click

        Dim xmlR As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKLIESET.XML")
        SetupLie(Me.dgKMYE, xmlR.Read("iniKCLie"))

    End Sub

    'Private Sub btnLieSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLieSetup.Click
    '    SetupLie(Me.dgKMYE, My.Settings.iniKCLie)
    '    'Dim sMoRenLie As String = ""
    '    'Dim i As Integer
    '    'For i = 0 To 27
    '    '    If (i < ((FKG.myselfG.dQiYongRiQi.Month) * 2 + 3)) OrElse i = 27 Then
    '    '        sMoRenLie = sMoRenLie & "1"
    '    '    Else
    '    '        sMoRenLie = sMoRenLie & "2"
    '    '        Me.dgKMYE.Columns(i).Visible = False
    '    '    End If
    '    'Next

    '    'Dim dlg As New FKXSLie.LieSetup(Me.dgKMYE, True, True, sMoRenLie)
    '    'If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
    '    '    My.Settings.iniKCLie = dlg.sSave
    '    '    My.Settings.Save()
    '    'End If
    'End Sub
    'Private Sub LieXSsetup()
    '    Dim sLie As String = My.Settings.iniKCLie
    '    sLie = ""
    '    If sLie = "" Then
    '        Dim iqsyue As Integer = FKG.myselfG.dQiYongRiQi.Month
    '        Dim i As Integer
    '        sLie = "1,,,,1,,,,1,,,,"
    '        For i = 0 To 11
    '            If i < iqsyue Then
    '                sLie = sLie & "1,,,,1,,,,"
    '            Else
    '                sLie = sLie & "2,,,,2,,,,"
    '            End If
    '        Next
    '        sLie = sLie & "1,,,"
    '        'sLie = sLie.Remove(sLie.Length - 1, 1)
    '        My.Settings.iniKCLie = sLie
    '        My.Settings.Save()
    '    End If

    '    Dim LSet As New FKXSLie.LieSetup(Me.dgKMYE, False, True, sLie)
    '    LSet.SetLie()
    'End Sub

    'Private Sub SetupLie(ByRef dgv As System.Windows.Forms.DataGridView, ByVal sMorenlie As String)
    '    Dim dlg As New FKXSLie.LieSetup(dgv, False, True, sMorenlie)
    '    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
    '        My.Settings.iniKCLie = dlg.sSave
    '        My.Settings.Save()
    '    End If
    'End Sub

    Private Sub cbWeiWaiKeHu_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbWeiWaiKeHu.SelectedIndexChanged
        FillDG(Me.cbCangKuID.Text)

    End Sub
End Class