在VB.NET中实现
在.NET中，由于TreeView控件的用法和VB6中的用法是不一样的！以前的VB6程序员会因为节点没有Key属性而烦恼！在.NET中，TreeView树的节点是一个集合，每个 TreeNode 都可以包含其他 TreeNode 对象的集合。要确定您在树结构中的位置，得使用 FullPath 属性。
我们知道，添加节点只能是在找到节点之后再此节点下添加。现在VB.NET少了Key属性，对操作是一个很大的不便。微软MSDN有一篇文章用继承和重载的方法，扩展了TreeView控件，给节点加了一个key属性。有兴趣的读者可以看一下HOW TO:Create a Key Property for a TreeView Node in Visual Basic .NET这篇文章。但是美中不足的是：这篇文章只是为TreeNode加了一个NodeKey属性，但是没有提供好的Key值检索功能。尽管这一切我们都可以用代码来扩展，但是代码冗长。
所以，添加许多层节点的树形结构，只能是递归调用。而且，我们下面的代码很精炼，只需要传给递归过程一个ParentID，就会将这个编号下的所有节点加载到树形结构中！
充分体现了：简单就是好的思想。

设计思想：从数据库中查询到所有节点的记录，添加到DataView中，利用DataView的.RowFilter属性得到某个父节点编号ParentID下的所有记录，依次递归循环。

在VB.net中实现：

Private ds As New DataSet ()
' AddTree递归函数每次都要用到数据集中的一个表，所以定义成private
Private Sub Form1_Load
(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
' '定义数据库连接
Dim CN As New SqlConnection()
Try
'初始化连接字符串
CN.ConnectionString = 
"data source=pmserver;initial catalog=
Benchmark;persist security info=False;user id=sa;Password=sa;"
CN.Open()
'添加命令，从数据库中得到数据
Dim sqlCmd As New SqlCommand()
sqlCmd.Connection = CN
sqlCmd.CommandText = "select * from tbtree"
sqlCmd.CommandType = CommandType.Text
Dim adp As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
adp.Fill(ds)
Catch ex As Exception
MsgBox(ex.Message)
Finally
'关闭连接
CN.Close()
End Try
'调用递归函数，完成树形结构的生成
AddTree(0, Nothing)
End Sub

'̀递归添加树的节点
Private Sub AddTree(ByVal ParentID As Integer, ByVal pNode As TreeNode)
Dim Node As TreeNode
Dim dvTree As New DataView()
dvTree = New DataView(ds.Tables(0))
'过滤ParentID,得到当前的所有子节点
dvTree.RowFilter = "PARENTID = " + ParentID.ToString

Dim Row As DataRowView
For Each Row In dvTree
If pNode Is Nothing Then '判断是否根节点
'̀添加根节点
Node = TreeView1.Nodes.Add(Row("context").ToString())
'̀再次递归
AddTree(Int32.Parse(Row("ID").ToString()), Node)
Else
‘添加当前节点的子节点
Node = pNode.Nodes.Add(Row("context").ToString())
'̀再次递归
AddTree(Int32.Parse(Row("ID").ToString()), Node)
End If
Node.EnsureVisible()
Next
End Sub



数据库设计
首先，我们在SQL SERVER 2000里建立一个表tbTree，表的结构设计如下：

列名 数据类型 描述 长度 主键
ID Int 节点编号 4 是
ConText Nvarchar 我们要显示的节点内容 50 
ParentID Int 父节点编号 4 



    Inherits TreeView


    Dim FKG As New FKG.myselfG

    Dim ds As DataSet

    Private Sub TvShows()
        Dim sConn As String
        sConn = FKG.asas
        Dim mdb As New MDBReadWrite.MDBReadWrite(sConn)
        ds = mdb.Reader("select * from GongnengShu order by index")
        ShowTV(0, Nothing)
    End Sub

    Private Sub AddNode(ByVal ParentID As Integer, ByVal sConText As String)
        Me.TV.Nodes.Add("context")
    End Sub

    Private Sub ModifyNode()


    End Sub

    Private Sub DelNode()

    End Sub


    '̀递归添加树的节点
    Private Sub ShowTV(ByVal ParentID As Integer, ByVal pNode As TreeNode)
        Dim Node As TreeNode
        Dim dvTree As New DataView()
        dvTree = New DataView(ds.Tables(0))
        '过滤ParentID,得到当前的所有子节点
        dvTree.RowFilter = "PARENTID = " + ParentID.ToString

        Dim Row As DataRowView
        For Each Row In dvTree
            If pNode Is Nothing Then '判断是否根节点
                '̀添加根节点
                Node = TV.Nodes.Add(Row("context").ToString())
                '̀再次递归
                ShowTV(Int32.Parse(Row("ID").ToString()), Node)
            Else
                '添加当前节点的子节点
                Node = pNode.Nodes.Add(Row("context").ToString())
                '̀再次递归
                ShowTV(Int32.Parse(Row("ID").ToString()), Node)
            End If
            Node.EnsureVisible()
        Next
    End Sub

    Private Sub FKGNS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TvShows()
    End Sub

    Private Sub TV_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TV.NodeMouseDoubleClick
        MsgBox(Me.TV.SelectedNode.Text)
    End Sub

    Private Sub 添加根节点ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 添加根节点ToolStripMenuItem.Click
        AddNode(0, "sldfkj")
    End Sub
