﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FreedomCore
    Inherits System.Windows.Forms.Form
    'Inherits FKime.noImeFrom

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FreedomCore))
        Me.gbSelect = New System.Windows.Forms.GroupBox
        Me.lblBeiyong30 = New Freedom.FKLable
        Me.lblBeiyong25 = New Freedom.FKLable
        Me.lblBeiyong20 = New Freedom.FKLable
        Me.lblBeiyong15 = New Freedom.FKLable
        Me.Beiyong30 = New Freedom.FKTextBox
        Me.Beiyong25 = New Freedom.FKTextBox
        Me.Beiyong20 = New Freedom.FKTextBox
        Me.BeiYong15 = New Freedom.FKTextBox
        Me.lblBeiyong29 = New Freedom.FKLable
        Me.lblBeiyong24 = New Freedom.FKLable
        Me.Beiyong29 = New Freedom.FKTextBox
        Me.Beiyong24 = New Freedom.FKTextBox
        Me.lblBeiyong19 = New Freedom.FKLable
        Me.lblBeiyong28 = New Freedom.FKLable
        Me.Beiyong19 = New Freedom.FKTextBox
        Me.lblBeiyong23 = New Freedom.FKLable
        Me.lblBeiyong14 = New Freedom.FKLable
        Me.Beiyong28 = New Freedom.FKTextBox
        Me.lblBeiyong18 = New Freedom.FKLable
        Me.Beiyong23 = New Freedom.FKTextBox
        Me.BeiYong14 = New Freedom.FKTextBox
        Me.lblBeiyong27 = New Freedom.FKLable
        Me.Beiyong18 = New Freedom.FKTextBox
        Me.lblBeiyong22 = New Freedom.FKLable
        Me.lblBeiyong13 = New Freedom.FKLable
        Me.Beiyong27 = New Freedom.FKTextBox
        Me.lblBeiyong17 = New Freedom.FKLable
        Me.Beiyong22 = New Freedom.FKTextBox
        Me.BeiYong13 = New Freedom.FKTextBox
        Me.lblBeiyong26 = New Freedom.FKLable
        Me.Beiyong17 = New Freedom.FKTextBox
        Me.lblBeiyong21 = New Freedom.FKLable
        Me.lblBeiyong12 = New Freedom.FKLable
        Me.Beiyong26 = New Freedom.FKTextBox
        Me.lblBeiyong16 = New Freedom.FKLable
        Me.Beiyong21 = New Freedom.FKTextBox
        Me.BeiYong12 = New Freedom.FKTextBox
        Me.Beiyong16 = New Freedom.FKTextBox
        Me.lblBeiyong11 = New Freedom.FKLable
        Me.BeiYong11 = New Freedom.FKTextBox
        Me.lblBeiyong10 = New Freedom.FKLable
        Me.BeiYong10 = New Freedom.FKTextBox
        Me.lblBeiyong9 = New Freedom.FKLable
        Me.BeiYong9 = New Freedom.FKTextBox
        Me.lblBeiyong8 = New Freedom.FKLable
        Me.BeiYong8 = New Freedom.FKTextBox
        Me.lblBeiyong7 = New Freedom.FKLable
        Me.BeiYong7 = New Freedom.FKTextBox
        Me.lblBeiyong6 = New Freedom.FKLable
        Me.BeiYong6 = New Freedom.FKTextBox
        Me.BeiYong4 = New Freedom.FKComboBox
        Me.BeiYong5 = New Freedom.FKDateTimePicker
        Me.RiQi = New Freedom.FKDateTimePicker
        Me.DaiFangKeMu = New Freedom.FKTextBox
        Me.DaiFangZhuKeMu = New Freedom.FKTextBox
        Me.JieFangKeMu = New Freedom.FKTextBox
        Me.JieFangZhuKemu = New Freedom.FKTextBox
        Me.lblBeiyong3 = New Freedom.FKLable
        Me.BeiYong3 = New Freedom.FKTextBox
        Me.lblBeiYong2 = New Freedom.FKLable
        Me.BeiYong2 = New Freedom.FKTextBox
        Me.lblBeiYong5 = New Freedom.FKLable
        Me.lblBeiYong4 = New Freedom.FKLable
        Me.lblBeiyong1 = New Freedom.FKLable
        Me.PingZhengHao = New Freedom.FKTextBox
        Me.Beiyong1 = New Freedom.FKTextBox
        Me.CangKu = New Freedom.FKComboBox
        Me.lblCangku = New Freedom.FKLable
        Me.lblName = New Freedom.FKLable
        Me.PingZhengZi = New Freedom.FKComboBox
        Me.JingShouRen = New Freedom.FKComboBox
        Me.lblJinE = New Freedom.FKLable
        Me.lblPzHao = New Freedom.FKLable
        Me.lblDate = New Freedom.FKLable
        Me.lblJingShouRem = New Freedom.FKLable
        Me.lblDanJia = New Freedom.FKLable
        Me.JinE = New Freedom.FKTextBox
        Me.BianHao = New Freedom.FKTextBox
        Me.lblShuLiang = New Freedom.FKLable
        Me.lblBianHao = New Freedom.FKLable
        Me.DanJia = New Freedom.FKTextBox
        Me.ShuLiang = New Freedom.FKTextBox
        Me.Zhaiyao = New Freedom.FKTextBox
        Me.DanWei = New Freedom.FKLable
        Me.lblZhaiYao = New Freedom.FKLable
        Me.lblMXkemu1 = New Freedom.FKLable
        Me.lblZhukemu1 = New Freedom.FKLable
        Me.lblMXkemu2 = New Freedom.FKLable
        Me.lblZhuKemu2 = New Freedom.FKLable
        Me.gbSX = New System.Windows.Forms.GroupBox
        Me.cbSXSuoDing = New Freedom.FKComboBox
        Me.FkLable5 = New Freedom.FKLable
        Me.btnSXColor = New System.Windows.Forms.Button
        Me.btnSXFont = New System.Windows.Forms.Button
        Me.cbSXgeshi = New Freedom.FKComboBox
        Me.cbSXBiTian = New Freedom.FKComboBox
        Me.cbSXzhidu = New Freedom.FKComboBox
        Me.cbSXID = New Freedom.FKComboBox
        Me.cbSXqiyong = New Freedom.FKComboBox
        Me.txtSXH = New Freedom.FKTextBox
        Me.txtSXY = New Freedom.FKTextBox
        Me.Label6 = New Freedom.FKLable
        Me.Label5 = New Freedom.FKLable
        Me.txtSXYanse = New Freedom.FKTextBox
        Me.txtSXFont = New Freedom.FKTextBox
        Me.txtSXBangDing = New Freedom.FKTextBox
        Me.txtSXTabindex = New Freedom.FKTextBox
        Me.txtSXzhi = New Freedom.FKTextBox
        Me.txtSXW = New Freedom.FKTextBox
        Me.txtSXX = New Freedom.FKTextBox
        Me.Label22 = New Freedom.FKLable
        Me.Label2 = New Freedom.FKLable
        Me.FkLable4 = New Freedom.FKLable
        Me.Label1 = New Freedom.FKLable
        Me.FkLable3 = New Freedom.FKLable
        Me.Label21 = New Freedom.FKLable
        Me.Label20 = New Freedom.FKLable
        Me.Label4 = New Freedom.FKLable
        Me.lblSXID = New Freedom.FKLable
        Me.Label3 = New Freedom.FKLable
        Me.FontDialog1 = New System.Windows.Forms.FontDialog
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog
        Me.dgvKuCun = New System.Windows.Forms.DataGridView
        Me.ckbKucun = New System.Windows.Forms.CheckBox
        Me.ckbGongZi = New System.Windows.Forms.CheckBox
        Me.dgvGongzi = New System.Windows.Forms.DataGridView
        Me.ckbPingZheng = New System.Windows.Forms.CheckBox
        Me.dgvPingZheng = New System.Windows.Forms.DataGridView
        Me.msFreedomCore = New System.Windows.Forms.MenuStrip
        Me.功能管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.保存 = New System.Windows.Forms.ToolStripMenuItem
        Me.另存为 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.功能导出 = New System.Windows.Forms.ToolStripMenuItem
        Me.导入功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.退出 = New System.Windows.Forms.ToolStripMenuItem
        Me.显示表设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.显示库存表 = New System.Windows.Forms.ToolStripMenuItem
        Me.显示工资表 = New System.Windows.Forms.ToolStripMenuItem
        Me.显示凭证表 = New System.Windows.Forms.ToolStripMenuItem
        Me.显示列设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存显示列设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资显示列设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.凭证显示列设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.打印设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存打印设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资打印设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.凭证打印设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.设计注意事项 = New System.Windows.Forms.ToolStripMenuItem
        Me.基本规则 = New System.Windows.Forms.ToolStripMenuItem
        Me.公式说明ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.绑定说明ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.显示列设置说明ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtTiShi = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.cbPZSCFS = New System.Windows.Forms.ComboBox
        Me.txtZhangShu = New System.Windows.Forms.TextBox
        Me.lblZhangShu = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.FkLable2 = New Freedom.FKLable
        Me.FkLable1 = New Freedom.FKLable
        Me.gbSelect.SuspendLayout()
        Me.gbSX.SuspendLayout()
        CType(Me.dgvKuCun, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGongzi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPingZheng, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.msFreedomCore.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbSelect
        '
        Me.gbSelect.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbSelect.AutoSize = True
        Me.gbSelect.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.gbSelect.Controls.Add(Me.lblBeiyong30)
        Me.gbSelect.Controls.Add(Me.lblBeiyong25)
        Me.gbSelect.Controls.Add(Me.lblBeiyong20)
        Me.gbSelect.Controls.Add(Me.lblBeiyong15)
        Me.gbSelect.Controls.Add(Me.Beiyong30)
        Me.gbSelect.Controls.Add(Me.Beiyong25)
        Me.gbSelect.Controls.Add(Me.Beiyong20)
        Me.gbSelect.Controls.Add(Me.BeiYong15)
        Me.gbSelect.Controls.Add(Me.lblBeiyong29)
        Me.gbSelect.Controls.Add(Me.lblBeiyong24)
        Me.gbSelect.Controls.Add(Me.Beiyong29)
        Me.gbSelect.Controls.Add(Me.Beiyong24)
        Me.gbSelect.Controls.Add(Me.lblBeiyong19)
        Me.gbSelect.Controls.Add(Me.lblBeiyong28)
        Me.gbSelect.Controls.Add(Me.Beiyong19)
        Me.gbSelect.Controls.Add(Me.lblBeiyong23)
        Me.gbSelect.Controls.Add(Me.lblBeiyong14)
        Me.gbSelect.Controls.Add(Me.Beiyong28)
        Me.gbSelect.Controls.Add(Me.lblBeiyong18)
        Me.gbSelect.Controls.Add(Me.Beiyong23)
        Me.gbSelect.Controls.Add(Me.BeiYong14)
        Me.gbSelect.Controls.Add(Me.lblBeiyong27)
        Me.gbSelect.Controls.Add(Me.Beiyong18)
        Me.gbSelect.Controls.Add(Me.lblBeiyong22)
        Me.gbSelect.Controls.Add(Me.lblBeiyong13)
        Me.gbSelect.Controls.Add(Me.Beiyong27)
        Me.gbSelect.Controls.Add(Me.lblBeiyong17)
        Me.gbSelect.Controls.Add(Me.Beiyong22)
        Me.gbSelect.Controls.Add(Me.BeiYong13)
        Me.gbSelect.Controls.Add(Me.lblBeiyong26)
        Me.gbSelect.Controls.Add(Me.Beiyong17)
        Me.gbSelect.Controls.Add(Me.lblBeiyong21)
        Me.gbSelect.Controls.Add(Me.lblBeiyong12)
        Me.gbSelect.Controls.Add(Me.Beiyong26)
        Me.gbSelect.Controls.Add(Me.lblBeiyong16)
        Me.gbSelect.Controls.Add(Me.Beiyong21)
        Me.gbSelect.Controls.Add(Me.BeiYong12)
        Me.gbSelect.Controls.Add(Me.Beiyong16)
        Me.gbSelect.Controls.Add(Me.lblBeiyong11)
        Me.gbSelect.Controls.Add(Me.BeiYong11)
        Me.gbSelect.Controls.Add(Me.lblBeiyong10)
        Me.gbSelect.Controls.Add(Me.BeiYong10)
        Me.gbSelect.Controls.Add(Me.lblBeiyong9)
        Me.gbSelect.Controls.Add(Me.BeiYong9)
        Me.gbSelect.Controls.Add(Me.lblBeiyong8)
        Me.gbSelect.Controls.Add(Me.BeiYong8)
        Me.gbSelect.Controls.Add(Me.lblBeiyong7)
        Me.gbSelect.Controls.Add(Me.BeiYong7)
        Me.gbSelect.Controls.Add(Me.lblBeiyong6)
        Me.gbSelect.Controls.Add(Me.BeiYong6)
        Me.gbSelect.Controls.Add(Me.BeiYong4)
        Me.gbSelect.Controls.Add(Me.BeiYong5)
        Me.gbSelect.Controls.Add(Me.RiQi)
        Me.gbSelect.Controls.Add(Me.DaiFangKeMu)
        Me.gbSelect.Controls.Add(Me.DaiFangZhuKeMu)
        Me.gbSelect.Controls.Add(Me.JieFangKeMu)
        Me.gbSelect.Controls.Add(Me.JieFangZhuKemu)
        Me.gbSelect.Controls.Add(Me.lblBeiyong3)
        Me.gbSelect.Controls.Add(Me.BeiYong3)
        Me.gbSelect.Controls.Add(Me.lblBeiYong2)
        Me.gbSelect.Controls.Add(Me.BeiYong2)
        Me.gbSelect.Controls.Add(Me.lblBeiYong5)
        Me.gbSelect.Controls.Add(Me.lblBeiYong4)
        Me.gbSelect.Controls.Add(Me.lblBeiyong1)
        Me.gbSelect.Controls.Add(Me.PingZhengHao)
        Me.gbSelect.Controls.Add(Me.Beiyong1)
        Me.gbSelect.Controls.Add(Me.CangKu)
        Me.gbSelect.Controls.Add(Me.lblCangku)
        Me.gbSelect.Controls.Add(Me.lblName)
        Me.gbSelect.Controls.Add(Me.PingZhengZi)
        Me.gbSelect.Controls.Add(Me.JingShouRen)
        Me.gbSelect.Controls.Add(Me.lblJinE)
        Me.gbSelect.Controls.Add(Me.lblPzHao)
        Me.gbSelect.Controls.Add(Me.lblDate)
        Me.gbSelect.Controls.Add(Me.lblJingShouRem)
        Me.gbSelect.Controls.Add(Me.lblDanJia)
        Me.gbSelect.Controls.Add(Me.JinE)
        Me.gbSelect.Controls.Add(Me.BianHao)
        Me.gbSelect.Controls.Add(Me.lblShuLiang)
        Me.gbSelect.Controls.Add(Me.lblBianHao)
        Me.gbSelect.Controls.Add(Me.DanJia)
        Me.gbSelect.Controls.Add(Me.ShuLiang)
        Me.gbSelect.Controls.Add(Me.Zhaiyao)
        Me.gbSelect.Controls.Add(Me.DanWei)
        Me.gbSelect.Controls.Add(Me.lblZhaiYao)
        Me.gbSelect.Controls.Add(Me.lblMXkemu1)
        Me.gbSelect.Controls.Add(Me.lblZhukemu1)
        Me.gbSelect.Controls.Add(Me.lblMXkemu2)
        Me.gbSelect.Controls.Add(Me.lblZhuKemu2)
        Me.gbSelect.Location = New System.Drawing.Point(0, 63)
        Me.gbSelect.Name = "gbSelect"
        Me.gbSelect.Size = New System.Drawing.Size(732, 369)
        Me.gbSelect.TabIndex = 0
        Me.gbSelect.TabStop = False
        '
        'lblBeiyong30
        '
        Me.lblBeiyong30.AutoSize = True
        Me.lblBeiyong30.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong30.Location = New System.Drawing.Point(604, 331)
        Me.lblBeiyong30.Name = "lblBeiyong30"
        Me.lblBeiyong30.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong30.TabIndex = 26
        Me.lblBeiyong30.Text = "备用数值"
        '
        'lblBeiyong25
        '
        Me.lblBeiyong25.AutoSize = True
        Me.lblBeiyong25.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong25.Location = New System.Drawing.Point(604, 308)
        Me.lblBeiyong25.Name = "lblBeiyong25"
        Me.lblBeiyong25.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong25.TabIndex = 26
        Me.lblBeiyong25.Text = "备用数值"
        '
        'lblBeiyong20
        '
        Me.lblBeiyong20.AutoSize = True
        Me.lblBeiyong20.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong20.Location = New System.Drawing.Point(604, 285)
        Me.lblBeiyong20.Name = "lblBeiyong20"
        Me.lblBeiyong20.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong20.TabIndex = 26
        Me.lblBeiyong20.Text = "备用数值"
        '
        'lblBeiyong15
        '
        Me.lblBeiyong15.AutoSize = True
        Me.lblBeiyong15.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong15.Location = New System.Drawing.Point(604, 258)
        Me.lblBeiyong15.Name = "lblBeiyong15"
        Me.lblBeiyong15.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong15.TabIndex = 26
        Me.lblBeiyong15.Text = "备用数值"
        '
        'Beiyong30
        '
        Me.Beiyong30.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong30.Location = New System.Drawing.Point(661, 328)
        Me.Beiyong30.Name = "Beiyong30"
        Me.Beiyong30.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong30.TabIndex = 27
        Me.Beiyong30.TabStop = False
        Me.Beiyong30.Text = "0"
        Me.Beiyong30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Beiyong25
        '
        Me.Beiyong25.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong25.Location = New System.Drawing.Point(661, 305)
        Me.Beiyong25.Name = "Beiyong25"
        Me.Beiyong25.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong25.TabIndex = 27
        Me.Beiyong25.TabStop = False
        Me.Beiyong25.Text = "0"
        Me.Beiyong25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Beiyong20
        '
        Me.Beiyong20.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong20.Location = New System.Drawing.Point(661, 282)
        Me.Beiyong20.Name = "Beiyong20"
        Me.Beiyong20.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong20.TabIndex = 27
        Me.Beiyong20.TabStop = False
        Me.Beiyong20.Text = "0"
        Me.Beiyong20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BeiYong15
        '
        Me.BeiYong15.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong15.Location = New System.Drawing.Point(661, 255)
        Me.BeiYong15.Name = "BeiYong15"
        Me.BeiYong15.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong15.TabIndex = 27
        Me.BeiYong15.TabStop = False
        Me.BeiYong15.Text = "0"
        Me.BeiYong15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong29
        '
        Me.lblBeiyong29.AutoSize = True
        Me.lblBeiyong29.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong29.Location = New System.Drawing.Point(457, 331)
        Me.lblBeiyong29.Name = "lblBeiyong29"
        Me.lblBeiyong29.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong29.TabIndex = 24
        Me.lblBeiyong29.Text = "备用数值"
        '
        'lblBeiyong24
        '
        Me.lblBeiyong24.AutoSize = True
        Me.lblBeiyong24.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong24.Location = New System.Drawing.Point(457, 308)
        Me.lblBeiyong24.Name = "lblBeiyong24"
        Me.lblBeiyong24.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong24.TabIndex = 24
        Me.lblBeiyong24.Text = "备用数值"
        '
        'Beiyong29
        '
        Me.Beiyong29.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong29.Location = New System.Drawing.Point(514, 328)
        Me.Beiyong29.Name = "Beiyong29"
        Me.Beiyong29.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong29.TabIndex = 25
        Me.Beiyong29.TabStop = False
        Me.Beiyong29.Text = "0"
        Me.Beiyong29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Beiyong24
        '
        Me.Beiyong24.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong24.Location = New System.Drawing.Point(514, 305)
        Me.Beiyong24.Name = "Beiyong24"
        Me.Beiyong24.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong24.TabIndex = 25
        Me.Beiyong24.TabStop = False
        Me.Beiyong24.Text = "0"
        Me.Beiyong24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong19
        '
        Me.lblBeiyong19.AutoSize = True
        Me.lblBeiyong19.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong19.Location = New System.Drawing.Point(457, 285)
        Me.lblBeiyong19.Name = "lblBeiyong19"
        Me.lblBeiyong19.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong19.TabIndex = 24
        Me.lblBeiyong19.Text = "备用数值"
        '
        'lblBeiyong28
        '
        Me.lblBeiyong28.AutoSize = True
        Me.lblBeiyong28.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong28.Location = New System.Drawing.Point(325, 331)
        Me.lblBeiyong28.Name = "lblBeiyong28"
        Me.lblBeiyong28.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong28.TabIndex = 22
        Me.lblBeiyong28.Text = "备用数值"
        '
        'Beiyong19
        '
        Me.Beiyong19.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong19.Location = New System.Drawing.Point(514, 282)
        Me.Beiyong19.Name = "Beiyong19"
        Me.Beiyong19.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong19.TabIndex = 25
        Me.Beiyong19.TabStop = False
        Me.Beiyong19.Text = "0"
        Me.Beiyong19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong23
        '
        Me.lblBeiyong23.AutoSize = True
        Me.lblBeiyong23.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong23.Location = New System.Drawing.Point(325, 308)
        Me.lblBeiyong23.Name = "lblBeiyong23"
        Me.lblBeiyong23.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong23.TabIndex = 22
        Me.lblBeiyong23.Text = "备用数值"
        '
        'lblBeiyong14
        '
        Me.lblBeiyong14.AutoSize = True
        Me.lblBeiyong14.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong14.Location = New System.Drawing.Point(457, 258)
        Me.lblBeiyong14.Name = "lblBeiyong14"
        Me.lblBeiyong14.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong14.TabIndex = 24
        Me.lblBeiyong14.Text = "备用数值"
        '
        'Beiyong28
        '
        Me.Beiyong28.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong28.Location = New System.Drawing.Point(382, 328)
        Me.Beiyong28.Name = "Beiyong28"
        Me.Beiyong28.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong28.TabIndex = 23
        Me.Beiyong28.TabStop = False
        Me.Beiyong28.Text = "0"
        Me.Beiyong28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong18
        '
        Me.lblBeiyong18.AutoSize = True
        Me.lblBeiyong18.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong18.Location = New System.Drawing.Point(325, 285)
        Me.lblBeiyong18.Name = "lblBeiyong18"
        Me.lblBeiyong18.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong18.TabIndex = 22
        Me.lblBeiyong18.Text = "备用数值"
        '
        'Beiyong23
        '
        Me.Beiyong23.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong23.Location = New System.Drawing.Point(382, 305)
        Me.Beiyong23.Name = "Beiyong23"
        Me.Beiyong23.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong23.TabIndex = 23
        Me.Beiyong23.TabStop = False
        Me.Beiyong23.Text = "0"
        Me.Beiyong23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BeiYong14
        '
        Me.BeiYong14.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong14.Location = New System.Drawing.Point(514, 255)
        Me.BeiYong14.Name = "BeiYong14"
        Me.BeiYong14.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong14.TabIndex = 25
        Me.BeiYong14.TabStop = False
        Me.BeiYong14.Text = "0"
        Me.BeiYong14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong27
        '
        Me.lblBeiyong27.AutoSize = True
        Me.lblBeiyong27.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong27.Location = New System.Drawing.Point(178, 331)
        Me.lblBeiyong27.Name = "lblBeiyong27"
        Me.lblBeiyong27.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong27.TabIndex = 20
        Me.lblBeiyong27.Text = "备用数值"
        '
        'Beiyong18
        '
        Me.Beiyong18.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong18.Location = New System.Drawing.Point(382, 282)
        Me.Beiyong18.Name = "Beiyong18"
        Me.Beiyong18.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong18.TabIndex = 23
        Me.Beiyong18.TabStop = False
        Me.Beiyong18.Text = "0"
        Me.Beiyong18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong22
        '
        Me.lblBeiyong22.AutoSize = True
        Me.lblBeiyong22.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong22.Location = New System.Drawing.Point(178, 308)
        Me.lblBeiyong22.Name = "lblBeiyong22"
        Me.lblBeiyong22.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong22.TabIndex = 20
        Me.lblBeiyong22.Text = "备用数值"
        '
        'lblBeiyong13
        '
        Me.lblBeiyong13.AutoSize = True
        Me.lblBeiyong13.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong13.Location = New System.Drawing.Point(325, 258)
        Me.lblBeiyong13.Name = "lblBeiyong13"
        Me.lblBeiyong13.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong13.TabIndex = 22
        Me.lblBeiyong13.Text = "备用数值"
        '
        'Beiyong27
        '
        Me.Beiyong27.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong27.Location = New System.Drawing.Point(235, 328)
        Me.Beiyong27.Name = "Beiyong27"
        Me.Beiyong27.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong27.TabIndex = 21
        Me.Beiyong27.TabStop = False
        Me.Beiyong27.Text = "0"
        Me.Beiyong27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong17
        '
        Me.lblBeiyong17.AutoSize = True
        Me.lblBeiyong17.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong17.Location = New System.Drawing.Point(178, 285)
        Me.lblBeiyong17.Name = "lblBeiyong17"
        Me.lblBeiyong17.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong17.TabIndex = 20
        Me.lblBeiyong17.Text = "备用数值"
        '
        'Beiyong22
        '
        Me.Beiyong22.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong22.Location = New System.Drawing.Point(235, 305)
        Me.Beiyong22.Name = "Beiyong22"
        Me.Beiyong22.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong22.TabIndex = 21
        Me.Beiyong22.TabStop = False
        Me.Beiyong22.Text = "0"
        Me.Beiyong22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BeiYong13
        '
        Me.BeiYong13.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong13.Location = New System.Drawing.Point(382, 255)
        Me.BeiYong13.Name = "BeiYong13"
        Me.BeiYong13.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong13.TabIndex = 23
        Me.BeiYong13.TabStop = False
        Me.BeiYong13.Text = "0"
        Me.BeiYong13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong26
        '
        Me.lblBeiyong26.AutoSize = True
        Me.lblBeiyong26.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong26.Location = New System.Drawing.Point(32, 331)
        Me.lblBeiyong26.Name = "lblBeiyong26"
        Me.lblBeiyong26.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong26.TabIndex = 18
        Me.lblBeiyong26.Text = "备用数值"
        '
        'Beiyong17
        '
        Me.Beiyong17.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong17.Location = New System.Drawing.Point(235, 282)
        Me.Beiyong17.Name = "Beiyong17"
        Me.Beiyong17.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong17.TabIndex = 21
        Me.Beiyong17.TabStop = False
        Me.Beiyong17.Text = "0"
        Me.Beiyong17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong21
        '
        Me.lblBeiyong21.AutoSize = True
        Me.lblBeiyong21.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong21.Location = New System.Drawing.Point(32, 308)
        Me.lblBeiyong21.Name = "lblBeiyong21"
        Me.lblBeiyong21.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong21.TabIndex = 18
        Me.lblBeiyong21.Text = "备用数值"
        '
        'lblBeiyong12
        '
        Me.lblBeiyong12.AutoSize = True
        Me.lblBeiyong12.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong12.Location = New System.Drawing.Point(178, 258)
        Me.lblBeiyong12.Name = "lblBeiyong12"
        Me.lblBeiyong12.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong12.TabIndex = 20
        Me.lblBeiyong12.Text = "备用数值"
        '
        'Beiyong26
        '
        Me.Beiyong26.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong26.Location = New System.Drawing.Point(89, 328)
        Me.Beiyong26.Name = "Beiyong26"
        Me.Beiyong26.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong26.TabIndex = 19
        Me.Beiyong26.TabStop = False
        Me.Beiyong26.Text = "0"
        Me.Beiyong26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong16
        '
        Me.lblBeiyong16.AutoSize = True
        Me.lblBeiyong16.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong16.Location = New System.Drawing.Point(32, 285)
        Me.lblBeiyong16.Name = "lblBeiyong16"
        Me.lblBeiyong16.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong16.TabIndex = 18
        Me.lblBeiyong16.Text = "备用数值"
        '
        'Beiyong21
        '
        Me.Beiyong21.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong21.Location = New System.Drawing.Point(89, 305)
        Me.Beiyong21.Name = "Beiyong21"
        Me.Beiyong21.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong21.TabIndex = 19
        Me.Beiyong21.TabStop = False
        Me.Beiyong21.Text = "0"
        Me.Beiyong21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BeiYong12
        '
        Me.BeiYong12.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong12.Location = New System.Drawing.Point(235, 255)
        Me.BeiYong12.Name = "BeiYong12"
        Me.BeiYong12.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong12.TabIndex = 21
        Me.BeiYong12.TabStop = False
        Me.BeiYong12.Text = "0"
        Me.BeiYong12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Beiyong16
        '
        Me.Beiyong16.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong16.Location = New System.Drawing.Point(89, 282)
        Me.Beiyong16.Name = "Beiyong16"
        Me.Beiyong16.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong16.TabIndex = 19
        Me.Beiyong16.TabStop = False
        Me.Beiyong16.Text = "0"
        Me.Beiyong16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong11
        '
        Me.lblBeiyong11.AutoSize = True
        Me.lblBeiyong11.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong11.Location = New System.Drawing.Point(32, 258)
        Me.lblBeiyong11.Name = "lblBeiyong11"
        Me.lblBeiyong11.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong11.TabIndex = 18
        Me.lblBeiyong11.Text = "备用数值"
        '
        'BeiYong11
        '
        Me.BeiYong11.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong11.Location = New System.Drawing.Point(89, 255)
        Me.BeiYong11.Name = "BeiYong11"
        Me.BeiYong11.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong11.TabIndex = 19
        Me.BeiYong11.TabStop = False
        Me.BeiYong11.Text = "0"
        Me.BeiYong11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong10
        '
        Me.lblBeiyong10.AutoSize = True
        Me.lblBeiyong10.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong10.Location = New System.Drawing.Point(604, 225)
        Me.lblBeiyong10.Name = "lblBeiyong10"
        Me.lblBeiyong10.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong10.TabIndex = 16
        Me.lblBeiyong10.Text = "备用数值"
        '
        'BeiYong10
        '
        Me.BeiYong10.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong10.Location = New System.Drawing.Point(661, 222)
        Me.BeiYong10.Name = "BeiYong10"
        Me.BeiYong10.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong10.TabIndex = 17
        Me.BeiYong10.TabStop = False
        Me.BeiYong10.Text = "0"
        Me.BeiYong10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong9
        '
        Me.lblBeiyong9.AutoSize = True
        Me.lblBeiyong9.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong9.Location = New System.Drawing.Point(457, 225)
        Me.lblBeiyong9.Name = "lblBeiyong9"
        Me.lblBeiyong9.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong9.TabIndex = 14
        Me.lblBeiyong9.Text = "备用数值"
        '
        'BeiYong9
        '
        Me.BeiYong9.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong9.Location = New System.Drawing.Point(514, 222)
        Me.BeiYong9.Name = "BeiYong9"
        Me.BeiYong9.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong9.TabIndex = 15
        Me.BeiYong9.TabStop = False
        Me.BeiYong9.Text = "0"
        Me.BeiYong9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong8
        '
        Me.lblBeiyong8.AutoSize = True
        Me.lblBeiyong8.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong8.Location = New System.Drawing.Point(325, 225)
        Me.lblBeiyong8.Name = "lblBeiyong8"
        Me.lblBeiyong8.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong8.TabIndex = 12
        Me.lblBeiyong8.Text = "备用数值"
        '
        'BeiYong8
        '
        Me.BeiYong8.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong8.Location = New System.Drawing.Point(382, 222)
        Me.BeiYong8.Name = "BeiYong8"
        Me.BeiYong8.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong8.TabIndex = 13
        Me.BeiYong8.TabStop = False
        Me.BeiYong8.Text = "0"
        Me.BeiYong8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong7
        '
        Me.lblBeiyong7.AutoSize = True
        Me.lblBeiyong7.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong7.Location = New System.Drawing.Point(178, 225)
        Me.lblBeiyong7.Name = "lblBeiyong7"
        Me.lblBeiyong7.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong7.TabIndex = 10
        Me.lblBeiyong7.Text = "备用数值"
        '
        'BeiYong7
        '
        Me.BeiYong7.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong7.Location = New System.Drawing.Point(235, 222)
        Me.BeiYong7.Name = "BeiYong7"
        Me.BeiYong7.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong7.TabIndex = 11
        Me.BeiYong7.TabStop = False
        Me.BeiYong7.Text = "0"
        Me.BeiYong7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong6
        '
        Me.lblBeiyong6.AutoSize = True
        Me.lblBeiyong6.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong6.Location = New System.Drawing.Point(32, 225)
        Me.lblBeiyong6.Name = "lblBeiyong6"
        Me.lblBeiyong6.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong6.TabIndex = 8
        Me.lblBeiyong6.Text = "备用数值"
        '
        'BeiYong6
        '
        Me.BeiYong6.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong6.Location = New System.Drawing.Point(89, 222)
        Me.BeiYong6.Name = "BeiYong6"
        Me.BeiYong6.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong6.TabIndex = 9
        Me.BeiYong6.TabStop = False
        Me.BeiYong6.Text = "0"
        Me.BeiYong6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BeiYong4
        '
        Me.BeiYong4.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong4.FormattingEnabled = True
        Me.BeiYong4.Location = New System.Drawing.Point(89, 190)
        Me.BeiYong4.Name = "BeiYong4"
        Me.BeiYong4.Size = New System.Drawing.Size(99, 20)
        Me.BeiYong4.TabIndex = 7
        Me.BeiYong4.TabStop = False
        '
        'BeiYong5
        '
        Me.BeiYong5.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong5.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.BeiYong5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.BeiYong5.Location = New System.Drawing.Point(287, 189)
        Me.BeiYong5.Name = "BeiYong5"
        Me.BeiYong5.Size = New System.Drawing.Size(91, 21)
        Me.BeiYong5.TabIndex = 6
        Me.BeiYong5.TabStop = False
        '
        'RiQi
        '
        Me.RiQi.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.RiQi.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.RiQi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.RiQi.Location = New System.Drawing.Point(432, 46)
        Me.RiQi.Name = "RiQi"
        Me.RiQi.Size = New System.Drawing.Size(91, 21)
        Me.RiQi.TabIndex = 6
        Me.RiQi.TabStop = False
        '
        'DaiFangKeMu
        '
        Me.DaiFangKeMu.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.DaiFangKeMu.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.DaiFangKeMu.Location = New System.Drawing.Point(639, 126)
        Me.DaiFangKeMu.Name = "DaiFangKeMu"
        Me.DaiFangKeMu.Size = New System.Drawing.Size(87, 21)
        Me.DaiFangKeMu.TabIndex = 5
        Me.DaiFangKeMu.TabStop = False
        Me.DaiFangKeMu.Text = "0"
        Me.DaiFangKeMu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DaiFangZhuKeMu
        '
        Me.DaiFangZhuKeMu.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.DaiFangZhuKeMu.Location = New System.Drawing.Point(459, 123)
        Me.DaiFangZhuKeMu.Name = "DaiFangZhuKeMu"
        Me.DaiFangZhuKeMu.Size = New System.Drawing.Size(84, 21)
        Me.DaiFangZhuKeMu.TabIndex = 5
        Me.DaiFangZhuKeMu.TabStop = False
        Me.DaiFangZhuKeMu.Text = "0"
        Me.DaiFangZhuKeMu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'JieFangKeMu
        '
        Me.JieFangKeMu.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.JieFangKeMu.Location = New System.Drawing.Point(262, 116)
        Me.JieFangKeMu.Name = "JieFangKeMu"
        Me.JieFangKeMu.Size = New System.Drawing.Size(84, 21)
        Me.JieFangKeMu.TabIndex = 5
        Me.JieFangKeMu.TabStop = False
        Me.JieFangKeMu.Text = "0"
        Me.JieFangKeMu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'JieFangZhuKemu
        '
        Me.JieFangZhuKemu.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.JieFangZhuKemu.Location = New System.Drawing.Point(89, 112)
        Me.JieFangZhuKemu.Name = "JieFangZhuKemu"
        Me.JieFangZhuKemu.Size = New System.Drawing.Size(84, 21)
        Me.JieFangZhuKemu.TabIndex = 5
        Me.JieFangZhuKemu.TabStop = False
        Me.JieFangZhuKemu.Text = "0"
        Me.JieFangZhuKemu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiyong3
        '
        Me.lblBeiyong3.AutoSize = True
        Me.lblBeiyong3.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong3.Location = New System.Drawing.Point(628, 192)
        Me.lblBeiyong3.Name = "lblBeiyong3"
        Me.lblBeiyong3.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong3.TabIndex = 0
        Me.lblBeiyong3.Text = "备用数值"
        '
        'BeiYong3
        '
        Me.BeiYong3.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong3.Location = New System.Drawing.Point(685, 189)
        Me.BeiYong3.Name = "BeiYong3"
        Me.BeiYong3.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong3.TabIndex = 3
        Me.BeiYong3.TabStop = False
        Me.BeiYong3.Text = "0"
        Me.BeiYong3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiYong2
        '
        Me.lblBeiYong2.AutoSize = True
        Me.lblBeiYong2.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiYong2.Location = New System.Drawing.Point(501, 192)
        Me.lblBeiYong2.Name = "lblBeiYong2"
        Me.lblBeiYong2.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiYong2.TabIndex = 0
        Me.lblBeiYong2.Text = "备用数值"
        '
        'BeiYong2
        '
        Me.BeiYong2.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BeiYong2.Location = New System.Drawing.Point(560, 187)
        Me.BeiYong2.Name = "BeiYong2"
        Me.BeiYong2.Size = New System.Drawing.Size(38, 21)
        Me.BeiYong2.TabIndex = 3
        Me.BeiYong2.TabStop = False
        Me.BeiYong2.Text = "0"
        Me.BeiYong2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBeiYong5
        '
        Me.lblBeiYong5.AutoSize = True
        Me.lblBeiYong5.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiYong5.Location = New System.Drawing.Point(228, 193)
        Me.lblBeiYong5.Name = "lblBeiYong5"
        Me.lblBeiYong5.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiYong5.TabIndex = 0
        Me.lblBeiYong5.Text = "备用日期"
        '
        'lblBeiYong4
        '
        Me.lblBeiYong4.AutoSize = True
        Me.lblBeiYong4.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiYong4.Location = New System.Drawing.Point(18, 193)
        Me.lblBeiYong4.Name = "lblBeiYong4"
        Me.lblBeiYong4.Size = New System.Drawing.Size(65, 12)
        Me.lblBeiYong4.TabIndex = 0
        Me.lblBeiYong4.Text = "备用可选框"
        '
        'lblBeiyong1
        '
        Me.lblBeiyong1.AutoSize = True
        Me.lblBeiyong1.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBeiyong1.Location = New System.Drawing.Point(387, 189)
        Me.lblBeiyong1.Name = "lblBeiyong1"
        Me.lblBeiyong1.Size = New System.Drawing.Size(53, 12)
        Me.lblBeiyong1.TabIndex = 0
        Me.lblBeiyong1.Text = "备用数值"
        '
        'PingZhengHao
        '
        Me.PingZhengHao.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.PingZhengHao.Location = New System.Drawing.Point(283, 48)
        Me.PingZhengHao.MaxLength = 4
        Me.PingZhengHao.Name = "PingZhengHao"
        Me.PingZhengHao.Size = New System.Drawing.Size(46, 21)
        Me.PingZhengHao.TabIndex = 3
        Me.PingZhengHao.TabStop = False
        Me.PingZhengHao.Text = "0"
        Me.PingZhengHao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Beiyong1
        '
        Me.Beiyong1.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Beiyong1.Location = New System.Drawing.Point(444, 186)
        Me.Beiyong1.Name = "Beiyong1"
        Me.Beiyong1.Size = New System.Drawing.Size(38, 21)
        Me.Beiyong1.TabIndex = 3
        Me.Beiyong1.TabStop = False
        Me.Beiyong1.Text = "0"
        Me.Beiyong1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CangKu
        '
        Me.CangKu.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.CangKu.FormattingEnabled = True
        Me.CangKu.Location = New System.Drawing.Point(477, 83)
        Me.CangKu.Name = "CangKu"
        Me.CangKu.Size = New System.Drawing.Size(75, 20)
        Me.CangKu.TabIndex = 1
        Me.CangKu.TabStop = False
        '
        'lblCangku
        '
        Me.lblCangku.AutoSize = True
        Me.lblCangku.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblCangku.Location = New System.Drawing.Point(430, 83)
        Me.lblCangku.Name = "lblCangku"
        Me.lblCangku.Size = New System.Drawing.Size(29, 12)
        Me.lblCangku.TabIndex = 0
        Me.lblCangku.Text = "仓库"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblName.Location = New System.Drawing.Point(290, 19)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(137, 12)
        Me.lblName.TabIndex = 1
        Me.lblName.Text = "请为您的功能起一个标题"
        '
        'PingZhengZi
        '
        Me.PingZhengZi.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.PingZhengZi.FormattingEnabled = True
        Me.PingZhengZi.Items.AddRange(New Object() {"进", "销", "收", "付", "记", "转"})
        Me.PingZhengZi.Location = New System.Drawing.Point(233, 48)
        Me.PingZhengZi.Name = "PingZhengZi"
        Me.PingZhengZi.Size = New System.Drawing.Size(50, 20)
        Me.PingZhengZi.TabIndex = 1
        Me.PingZhengZi.TabStop = False
        Me.PingZhengZi.Text = "记帐"
        '
        'JingShouRen
        '
        Me.JingShouRen.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.JingShouRen.FormattingEnabled = True
        Me.JingShouRen.Location = New System.Drawing.Point(586, 46)
        Me.JingShouRen.Name = "JingShouRen"
        Me.JingShouRen.Size = New System.Drawing.Size(75, 20)
        Me.JingShouRen.TabIndex = 1
        Me.JingShouRen.TabStop = False
        '
        'lblJinE
        '
        Me.lblJinE.AutoSize = True
        Me.lblJinE.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblJinE.Location = New System.Drawing.Point(411, 155)
        Me.lblJinE.Name = "lblJinE"
        Me.lblJinE.Size = New System.Drawing.Size(29, 12)
        Me.lblJinE.TabIndex = 0
        Me.lblJinE.Text = "金额"
        '
        'lblPzHao
        '
        Me.lblPzHao.AutoSize = True
        Me.lblPzHao.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblPzHao.Location = New System.Drawing.Point(194, 52)
        Me.lblPzHao.Name = "lblPzHao"
        Me.lblPzHao.Size = New System.Drawing.Size(41, 12)
        Me.lblPzHao.TabIndex = 0
        Me.lblPzHao.Text = "凭证号"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblDate.Location = New System.Drawing.Point(396, 52)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(29, 12)
        Me.lblDate.TabIndex = 0
        Me.lblDate.Text = "日期"
        '
        'lblJingShouRem
        '
        Me.lblJingShouRem.AutoSize = True
        Me.lblJingShouRem.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblJingShouRem.Location = New System.Drawing.Point(539, 47)
        Me.lblJingShouRem.Name = "lblJingShouRem"
        Me.lblJingShouRem.Size = New System.Drawing.Size(41, 12)
        Me.lblJingShouRem.TabIndex = 0
        Me.lblJingShouRem.Text = "经手人"
        '
        'lblDanJia
        '
        Me.lblDanJia.AutoSize = True
        Me.lblDanJia.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblDanJia.Location = New System.Drawing.Point(238, 157)
        Me.lblDanJia.Name = "lblDanJia"
        Me.lblDanJia.Size = New System.Drawing.Size(29, 12)
        Me.lblDanJia.TabIndex = 0
        Me.lblDanJia.Text = "单价"
        '
        'JinE
        '
        Me.JinE.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.JinE.Location = New System.Drawing.Point(444, 154)
        Me.JinE.Name = "JinE"
        Me.JinE.Size = New System.Drawing.Size(84, 21)
        Me.JinE.TabIndex = 3
        Me.JinE.TabStop = False
        Me.JinE.Text = "0"
        Me.JinE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BianHao
        '
        Me.BianHao.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.BianHao.Location = New System.Drawing.Point(92, 46)
        Me.BianHao.Name = "BianHao"
        Me.BianHao.Size = New System.Drawing.Size(84, 21)
        Me.BianHao.TabIndex = 0
        Me.BianHao.TabStop = False
        Me.BianHao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblShuLiang
        '
        Me.lblShuLiang.AutoSize = True
        Me.lblShuLiang.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblShuLiang.Location = New System.Drawing.Point(50, 155)
        Me.lblShuLiang.Name = "lblShuLiang"
        Me.lblShuLiang.Size = New System.Drawing.Size(29, 12)
        Me.lblShuLiang.TabIndex = 0
        Me.lblShuLiang.Text = "数量"
        '
        'lblBianHao
        '
        Me.lblBianHao.AutoSize = True
        Me.lblBianHao.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblBianHao.Location = New System.Drawing.Point(59, 49)
        Me.lblBianHao.Name = "lblBianHao"
        Me.lblBianHao.Size = New System.Drawing.Size(29, 12)
        Me.lblBianHao.TabIndex = 0
        Me.lblBianHao.Text = "编号"
        '
        'DanJia
        '
        Me.DanJia.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.DanJia.Location = New System.Drawing.Point(270, 152)
        Me.DanJia.Name = "DanJia"
        Me.DanJia.Size = New System.Drawing.Size(84, 21)
        Me.DanJia.TabIndex = 3
        Me.DanJia.TabStop = False
        Me.DanJia.Text = "0"
        Me.DanJia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ShuLiang
        '
        Me.ShuLiang.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.ShuLiang.Location = New System.Drawing.Point(85, 154)
        Me.ShuLiang.Name = "ShuLiang"
        Me.ShuLiang.Size = New System.Drawing.Size(91, 21)
        Me.ShuLiang.TabIndex = 3
        Me.ShuLiang.TabStop = False
        Me.ShuLiang.Text = "0"
        Me.ShuLiang.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Zhaiyao
        '
        Me.Zhaiyao.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Zhaiyao.Location = New System.Drawing.Point(89, 81)
        Me.Zhaiyao.Name = "Zhaiyao"
        Me.Zhaiyao.Size = New System.Drawing.Size(255, 21)
        Me.Zhaiyao.TabIndex = 3
        Me.Zhaiyao.TabStop = False
        Me.Zhaiyao.Text = "0"
        Me.Zhaiyao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DanWei
        '
        Me.DanWei.AutoSize = True
        Me.DanWei.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.DanWei.Location = New System.Drawing.Point(182, 157)
        Me.DanWei.Name = "DanWei"
        Me.DanWei.Size = New System.Drawing.Size(29, 12)
        Me.DanWei.TabIndex = 4
        Me.DanWei.Text = "单位"
        '
        'lblZhaiYao
        '
        Me.lblZhaiYao.AutoSize = True
        Me.lblZhaiYao.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblZhaiYao.Location = New System.Drawing.Point(56, 82)
        Me.lblZhaiYao.Name = "lblZhaiYao"
        Me.lblZhaiYao.Size = New System.Drawing.Size(29, 12)
        Me.lblZhaiYao.TabIndex = 0
        Me.lblZhaiYao.Text = "摘要"
        '
        'lblMXkemu1
        '
        Me.lblMXkemu1.AutoSize = True
        Me.lblMXkemu1.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblMXkemu1.Location = New System.Drawing.Point(197, 121)
        Me.lblMXkemu1.Name = "lblMXkemu1"
        Me.lblMXkemu1.Size = New System.Drawing.Size(53, 12)
        Me.lblMXkemu1.TabIndex = 0
        Me.lblMXkemu1.Text = "借方科目"
        '
        'lblZhukemu1
        '
        Me.lblZhukemu1.AutoSize = True
        Me.lblZhukemu1.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblZhukemu1.Location = New System.Drawing.Point(20, 117)
        Me.lblZhukemu1.Name = "lblZhukemu1"
        Me.lblZhukemu1.Size = New System.Drawing.Size(65, 12)
        Me.lblZhukemu1.TabIndex = 0
        Me.lblZhukemu1.Text = "借方主科目"
        '
        'lblMXkemu2
        '
        Me.lblMXkemu2.AutoSize = True
        Me.lblMXkemu2.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblMXkemu2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblMXkemu2.Location = New System.Drawing.Point(580, 132)
        Me.lblMXkemu2.Name = "lblMXkemu2"
        Me.lblMXkemu2.Size = New System.Drawing.Size(53, 12)
        Me.lblMXkemu2.TabIndex = 0
        Me.lblMXkemu2.Text = "贷方科目"
        '
        'lblZhuKemu2
        '
        Me.lblZhuKemu2.AutoSize = True
        Me.lblZhuKemu2.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblZhuKemu2.Location = New System.Drawing.Point(388, 126)
        Me.lblZhuKemu2.Name = "lblZhuKemu2"
        Me.lblZhuKemu2.Size = New System.Drawing.Size(65, 12)
        Me.lblZhuKemu2.TabIndex = 0
        Me.lblZhuKemu2.Text = "贷方主科目"
        '
        'gbSX
        '
        Me.gbSX.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbSX.Controls.Add(Me.cbSXSuoDing)
        Me.gbSX.Controls.Add(Me.FkLable5)
        Me.gbSX.Controls.Add(Me.btnSXColor)
        Me.gbSX.Controls.Add(Me.btnSXFont)
        Me.gbSX.Controls.Add(Me.cbSXgeshi)
        Me.gbSX.Controls.Add(Me.cbSXBiTian)
        Me.gbSX.Controls.Add(Me.cbSXzhidu)
        Me.gbSX.Controls.Add(Me.cbSXID)
        Me.gbSX.Controls.Add(Me.cbSXqiyong)
        Me.gbSX.Controls.Add(Me.txtSXH)
        Me.gbSX.Controls.Add(Me.txtSXY)
        Me.gbSX.Controls.Add(Me.Label6)
        Me.gbSX.Controls.Add(Me.Label5)
        Me.gbSX.Controls.Add(Me.txtSXYanse)
        Me.gbSX.Controls.Add(Me.txtSXFont)
        Me.gbSX.Controls.Add(Me.txtSXBangDing)
        Me.gbSX.Controls.Add(Me.txtSXTabindex)
        Me.gbSX.Controls.Add(Me.txtSXzhi)
        Me.gbSX.Controls.Add(Me.txtSXW)
        Me.gbSX.Controls.Add(Me.txtSXX)
        Me.gbSX.Controls.Add(Me.Label22)
        Me.gbSX.Controls.Add(Me.Label2)
        Me.gbSX.Controls.Add(Me.FkLable4)
        Me.gbSX.Controls.Add(Me.Label1)
        Me.gbSX.Controls.Add(Me.FkLable3)
        Me.gbSX.Controls.Add(Me.Label21)
        Me.gbSX.Controls.Add(Me.Label20)
        Me.gbSX.Controls.Add(Me.Label4)
        Me.gbSX.Controls.Add(Me.lblSXID)
        Me.gbSX.Controls.Add(Me.Label3)
        Me.gbSX.Location = New System.Drawing.Point(849, 27)
        Me.gbSX.Name = "gbSX"
        Me.gbSX.Size = New System.Drawing.Size(175, 385)
        Me.gbSX.TabIndex = 0
        Me.gbSX.TabStop = False
        Me.gbSX.Text = "属性设置"
        '
        'cbSXSuoDing
        '
        Me.cbSXSuoDing.FormattingEnabled = True
        Me.cbSXSuoDing.Items.AddRange(New Object() {"True", "False"})
        Me.cbSXSuoDing.Location = New System.Drawing.Point(69, 347)
        Me.cbSXSuoDing.Name = "cbSXSuoDing"
        Me.cbSXSuoDing.Size = New System.Drawing.Size(97, 20)
        Me.cbSXSuoDing.TabIndex = 15
        '
        'FkLable5
        '
        Me.FkLable5.AutoSize = True
        Me.FkLable5.Location = New System.Drawing.Point(13, 350)
        Me.FkLable5.Name = "FkLable5"
        Me.FkLable5.Size = New System.Drawing.Size(53, 12)
        Me.FkLable5.TabIndex = 5
        Me.FkLable5.Text = "同单锁定"
        '
        'btnSXColor
        '
        Me.btnSXColor.Location = New System.Drawing.Point(146, 265)
        Me.btnSXColor.Name = "btnSXColor"
        Me.btnSXColor.Size = New System.Drawing.Size(23, 21)
        Me.btnSXColor.TabIndex = 4
        Me.btnSXColor.TabStop = False
        Me.btnSXColor.Text = "Button1"
        Me.btnSXColor.UseVisualStyleBackColor = True
        '
        'btnSXFont
        '
        Me.btnSXFont.Location = New System.Drawing.Point(146, 238)
        Me.btnSXFont.Name = "btnSXFont"
        Me.btnSXFont.Size = New System.Drawing.Size(23, 21)
        Me.btnSXFont.TabIndex = 4
        Me.btnSXFont.TabStop = False
        Me.btnSXFont.Text = "Button1"
        Me.btnSXFont.UseVisualStyleBackColor = True
        '
        'cbSXgeshi
        '
        Me.cbSXgeshi.FormattingEnabled = True
        Me.cbSXgeshi.Items.AddRange(New Object() {"数字", "文本", "公式"})
        Me.cbSXgeshi.Location = New System.Drawing.Point(69, 105)
        Me.cbSXgeshi.Name = "cbSXgeshi"
        Me.cbSXgeshi.Size = New System.Drawing.Size(97, 20)
        Me.cbSXgeshi.TabIndex = 4
        '
        'cbSXBiTian
        '
        Me.cbSXBiTian.FormattingEnabled = True
        Me.cbSXBiTian.Items.AddRange(New Object() {"True", "False"})
        Me.cbSXBiTian.Location = New System.Drawing.Point(69, 292)
        Me.cbSXBiTian.Name = "cbSXBiTian"
        Me.cbSXBiTian.Size = New System.Drawing.Size(97, 20)
        Me.cbSXBiTian.TabIndex = 13
        '
        'cbSXzhidu
        '
        Me.cbSXzhidu.FormattingEnabled = True
        Me.cbSXzhidu.Items.AddRange(New Object() {"True", "False"})
        Me.cbSXzhidu.Location = New System.Drawing.Point(69, 185)
        Me.cbSXzhidu.Name = "cbSXzhidu"
        Me.cbSXzhidu.Size = New System.Drawing.Size(97, 20)
        Me.cbSXzhidu.TabIndex = 9
        '
        'cbSXID
        '
        Me.cbSXID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSXID.FormattingEnabled = True
        Me.cbSXID.Location = New System.Drawing.Point(68, 25)
        Me.cbSXID.Name = "cbSXID"
        Me.cbSXID.Size = New System.Drawing.Size(97, 20)
        Me.cbSXID.TabIndex = 1
        '
        'cbSXqiyong
        '
        Me.cbSXqiyong.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSXqiyong.FormattingEnabled = True
        Me.cbSXqiyong.Items.AddRange(New Object() {"True", "False"})
        Me.cbSXqiyong.Location = New System.Drawing.Point(69, 52)
        Me.cbSXqiyong.Name = "cbSXqiyong"
        Me.cbSXqiyong.Size = New System.Drawing.Size(97, 20)
        Me.cbSXqiyong.TabIndex = 2
        '
        'txtSXH
        '
        Me.txtSXH.Location = New System.Drawing.Point(118, 158)
        Me.txtSXH.Name = "txtSXH"
        Me.txtSXH.Size = New System.Drawing.Size(47, 21)
        Me.txtSXH.TabIndex = 8
        '
        'txtSXY
        '
        Me.txtSXY.Location = New System.Drawing.Point(118, 133)
        Me.txtSXY.Name = "txtSXY"
        Me.txtSXY.Size = New System.Drawing.Size(47, 21)
        Me.txtSXY.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(36, 163)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 12)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "大小"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(36, 136)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 12)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "位置"
        '
        'txtSXYanse
        '
        Me.txtSXYanse.Location = New System.Drawing.Point(69, 265)
        Me.txtSXYanse.Name = "txtSXYanse"
        Me.txtSXYanse.ReadOnly = True
        Me.txtSXYanse.Size = New System.Drawing.Size(100, 21)
        Me.txtSXYanse.TabIndex = 12
        '
        'txtSXFont
        '
        Me.txtSXFont.Location = New System.Drawing.Point(69, 238)
        Me.txtSXFont.Name = "txtSXFont"
        Me.txtSXFont.ReadOnly = True
        Me.txtSXFont.Size = New System.Drawing.Size(100, 21)
        Me.txtSXFont.TabIndex = 11
        '
        'txtSXBangDing
        '
        Me.txtSXBangDing.Location = New System.Drawing.Point(69, 318)
        Me.txtSXBangDing.Name = "txtSXBangDing"
        Me.txtSXBangDing.Size = New System.Drawing.Size(100, 21)
        Me.txtSXBangDing.TabIndex = 14
        '
        'txtSXTabindex
        '
        Me.txtSXTabindex.Location = New System.Drawing.Point(69, 211)
        Me.txtSXTabindex.Name = "txtSXTabindex"
        Me.txtSXTabindex.Size = New System.Drawing.Size(100, 21)
        Me.txtSXTabindex.TabIndex = 10
        '
        'txtSXzhi
        '
        Me.txtSXzhi.Location = New System.Drawing.Point(69, 78)
        Me.txtSXzhi.Name = "txtSXzhi"
        Me.txtSXzhi.Size = New System.Drawing.Size(100, 21)
        Me.txtSXzhi.TabIndex = 3
        '
        'txtSXW
        '
        Me.txtSXW.Location = New System.Drawing.Point(69, 159)
        Me.txtSXW.Name = "txtSXW"
        Me.txtSXW.Size = New System.Drawing.Size(43, 21)
        Me.txtSXW.TabIndex = 7
        '
        'txtSXX
        '
        Me.txtSXX.Location = New System.Drawing.Point(69, 132)
        Me.txtSXX.Name = "txtSXX"
        Me.txtSXX.Size = New System.Drawing.Size(43, 21)
        Me.txtSXX.TabIndex = 5
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(36, 109)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(29, 12)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "格式"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(36, 265)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "颜色"
        '
        'FkLable4
        '
        Me.FkLable4.AutoSize = True
        Me.FkLable4.Location = New System.Drawing.Point(34, 321)
        Me.FkLable4.Name = "FkLable4"
        Me.FkLable4.Size = New System.Drawing.Size(29, 12)
        Me.FkLable4.TabIndex = 0
        Me.FkLable4.Text = "绑定"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(36, 238)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "字体"
        '
        'FkLable3
        '
        Me.FkLable3.AutoSize = True
        Me.FkLable3.Location = New System.Drawing.Point(24, 295)
        Me.FkLable3.Name = "FkLable3"
        Me.FkLable3.Size = New System.Drawing.Size(41, 12)
        Me.FkLable3.TabIndex = 0
        Me.FkLable3.Text = "必填项"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(18, 214)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(47, 12)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "TAB顺序"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(36, 190)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(29, 12)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "只读"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(36, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 12)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "显示"
        '
        'lblSXID
        '
        Me.lblSXID.AutoSize = True
        Me.lblSXID.Location = New System.Drawing.Point(24, 28)
        Me.lblSXID.Name = "lblSXID"
        Me.lblSXID.Size = New System.Drawing.Size(41, 12)
        Me.lblSXID.TabIndex = 0
        Me.lblSXID.Text = "参数ID"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(48, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "值"
        '
        'dgvKuCun
        '
        Me.dgvKuCun.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvKuCun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKuCun.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvKuCun.Location = New System.Drawing.Point(3, 26)
        Me.dgvKuCun.Name = "dgvKuCun"
        Me.dgvKuCun.RowTemplate.Height = 23
        Me.dgvKuCun.Size = New System.Drawing.Size(824, 54)
        Me.dgvKuCun.TabIndex = 1
        Me.dgvKuCun.TabStop = False
        '
        'ckbKucun
        '
        Me.ckbKucun.AutoSize = True
        Me.ckbKucun.Checked = True
        Me.ckbKucun.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbKucun.Location = New System.Drawing.Point(3, 3)
        Me.ckbKucun.Name = "ckbKucun"
        Me.ckbKucun.Size = New System.Drawing.Size(96, 16)
        Me.ckbKucun.TabIndex = 10
        Me.ckbKucun.TabStop = False
        Me.ckbKucun.Text = "显示库存操作"
        Me.ckbKucun.UseVisualStyleBackColor = True
        '
        'ckbGongZi
        '
        Me.ckbGongZi.AutoSize = True
        Me.ckbGongZi.Checked = True
        Me.ckbGongZi.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbGongZi.Location = New System.Drawing.Point(3, 86)
        Me.ckbGongZi.Name = "ckbGongZi"
        Me.ckbGongZi.Size = New System.Drawing.Size(96, 16)
        Me.ckbGongZi.TabIndex = 10
        Me.ckbGongZi.TabStop = False
        Me.ckbGongZi.Text = "显示工资操作"
        Me.ckbGongZi.UseVisualStyleBackColor = True
        '
        'dgvGongzi
        '
        Me.dgvGongzi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGongzi.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvGongzi.Location = New System.Drawing.Point(3, 110)
        Me.dgvGongzi.Name = "dgvGongzi"
        Me.dgvGongzi.RowTemplate.Height = 23
        Me.dgvGongzi.Size = New System.Drawing.Size(824, 49)
        Me.dgvGongzi.TabIndex = 10
        Me.dgvGongzi.TabStop = False
        '
        'ckbPingZheng
        '
        Me.ckbPingZheng.AutoSize = True
        Me.ckbPingZheng.Checked = True
        Me.ckbPingZheng.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbPingZheng.Location = New System.Drawing.Point(3, 3)
        Me.ckbPingZheng.Name = "ckbPingZheng"
        Me.ckbPingZheng.Size = New System.Drawing.Size(85, 16)
        Me.ckbPingZheng.TabIndex = 10
        Me.ckbPingZheng.TabStop = False
        Me.ckbPingZheng.Text = "显示凭证操作"
        Me.ckbPingZheng.UseVisualStyleBackColor = True
        '
        'dgvPingZheng
        '
        Me.dgvPingZheng.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPingZheng.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPingZheng.Location = New System.Drawing.Point(3, 193)
        Me.dgvPingZheng.Name = "dgvPingZheng"
        Me.dgvPingZheng.RowTemplate.Height = 23
        Me.dgvPingZheng.Size = New System.Drawing.Size(824, 187)
        Me.dgvPingZheng.TabIndex = 9
        Me.dgvPingZheng.TabStop = False
        '
        'msFreedomCore
        '
        Me.msFreedomCore.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.功能管理, Me.显示表设置, Me.显示列设置, Me.打印设置, Me.设计注意事项})
        Me.msFreedomCore.Location = New System.Drawing.Point(0, 0)
        Me.msFreedomCore.Name = "msFreedomCore"
        Me.msFreedomCore.Size = New System.Drawing.Size(1024, 24)
        Me.msFreedomCore.TabIndex = 10
        Me.msFreedomCore.Text = "MenuStrip1"
        '
        '功能管理
        '
        Me.功能管理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.保存, Me.另存为, Me.ToolStripSeparator1, Me.功能导出, Me.导入功能, Me.ToolStripSeparator2, Me.退出})
        Me.功能管理.Name = "功能管理"
        Me.功能管理.Size = New System.Drawing.Size(65, 20)
        Me.功能管理.Text = "功能管理"
        '
        '保存
        '
        Me.保存.Image = CType(resources.GetObject("保存.Image"), System.Drawing.Image)
        Me.保存.Name = "保存"
        Me.保存.Size = New System.Drawing.Size(136, 22)
        Me.保存.Text = "保存"
        '
        '另存为
        '
        Me.另存为.Name = "另存为"
        Me.另存为.Size = New System.Drawing.Size(136, 22)
        Me.另存为.Text = "另存为..."
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(133, 6)
        '
        '功能导出
        '
        Me.功能导出.Name = "功能导出"
        Me.功能导出.Size = New System.Drawing.Size(136, 22)
        Me.功能导出.Text = "功能导出..."
        '
        '导入功能
        '
        Me.导入功能.Name = "导入功能"
        Me.导入功能.Size = New System.Drawing.Size(136, 22)
        Me.导入功能.Text = "导入功能..."
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(133, 6)
        '
        '退出
        '
        Me.退出.Name = "退出"
        Me.退出.Size = New System.Drawing.Size(136, 22)
        Me.退出.Text = "退出"
        '
        '显示表设置
        '
        Me.显示表设置.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.显示库存表, Me.显示工资表, Me.显示凭证表})
        Me.显示表设置.Name = "显示表设置"
        Me.显示表设置.Size = New System.Drawing.Size(77, 20)
        Me.显示表设置.Text = "显示表设置"
        '
        '显示库存表
        '
        Me.显示库存表.Checked = True
        Me.显示库存表.CheckState = System.Windows.Forms.CheckState.Checked
        Me.显示库存表.Name = "显示库存表"
        Me.显示库存表.Size = New System.Drawing.Size(130, 22)
        Me.显示库存表.Text = "显示库存表"
        '
        '显示工资表
        '
        Me.显示工资表.Checked = True
        Me.显示工资表.CheckState = System.Windows.Forms.CheckState.Checked
        Me.显示工资表.Name = "显示工资表"
        Me.显示工资表.Size = New System.Drawing.Size(130, 22)
        Me.显示工资表.Text = "显示工资表"
        '
        '显示凭证表
        '
        Me.显示凭证表.Checked = True
        Me.显示凭证表.CheckState = System.Windows.Forms.CheckState.Checked
        Me.显示凭证表.Name = "显示凭证表"
        Me.显示凭证表.Size = New System.Drawing.Size(130, 22)
        Me.显示凭证表.Text = "显示凭证表"
        '
        '显示列设置
        '
        Me.显示列设置.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.库存显示列设置, Me.工资显示列设置, Me.凭证显示列设置})
        Me.显示列设置.Name = "显示列设置"
        Me.显示列设置.Size = New System.Drawing.Size(77, 20)
        Me.显示列设置.Text = "显示列设置"
        '
        '库存显示列设置
        '
        Me.库存显示列设置.Name = "库存显示列设置"
        Me.库存显示列设置.Size = New System.Drawing.Size(154, 22)
        Me.库存显示列设置.Text = "库存显示列设置"
        '
        '工资显示列设置
        '
        Me.工资显示列设置.Name = "工资显示列设置"
        Me.工资显示列设置.Size = New System.Drawing.Size(154, 22)
        Me.工资显示列设置.Text = "工资显示列设置"
        '
        '凭证显示列设置
        '
        Me.凭证显示列设置.Name = "凭证显示列设置"
        Me.凭证显示列设置.Size = New System.Drawing.Size(154, 22)
        Me.凭证显示列设置.Text = "凭证显示列设置"
        '
        '打印设置
        '
        Me.打印设置.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.库存打印设置, Me.工资打印设置, Me.凭证打印设置})
        Me.打印设置.Name = "打印设置"
        Me.打印设置.Size = New System.Drawing.Size(65, 20)
        Me.打印设置.Text = "打印设置"
        '
        '库存打印设置
        '
        Me.库存打印设置.Name = "库存打印设置"
        Me.库存打印设置.Size = New System.Drawing.Size(142, 22)
        Me.库存打印设置.Text = "库存打印设置"
        '
        '工资打印设置
        '
        Me.工资打印设置.Name = "工资打印设置"
        Me.工资打印设置.Size = New System.Drawing.Size(142, 22)
        Me.工资打印设置.Text = "工资打印设置"
        '
        '凭证打印设置
        '
        Me.凭证打印设置.Name = "凭证打印设置"
        Me.凭证打印设置.Size = New System.Drawing.Size(142, 22)
        Me.凭证打印设置.Text = "凭证打印设置"
        '
        '设计注意事项
        '
        Me.设计注意事项.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.基本规则, Me.公式说明ToolStripMenuItem, Me.绑定说明ToolStripMenuItem, Me.显示列设置说明ToolStripMenuItem})
        Me.设计注意事项.Name = "设计注意事项"
        Me.设计注意事项.Size = New System.Drawing.Size(65, 20)
        Me.设计注意事项.Text = "设计帮助"
        '
        '基本规则
        '
        Me.基本规则.Name = "基本规则"
        Me.基本规则.Size = New System.Drawing.Size(154, 22)
        Me.基本规则.Text = "基本规则"
        '
        '公式说明ToolStripMenuItem
        '
        Me.公式说明ToolStripMenuItem.Name = "公式说明ToolStripMenuItem"
        Me.公式说明ToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.公式说明ToolStripMenuItem.Text = "公式说明"
        '
        '绑定说明ToolStripMenuItem
        '
        Me.绑定说明ToolStripMenuItem.Name = "绑定说明ToolStripMenuItem"
        Me.绑定说明ToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.绑定说明ToolStripMenuItem.Text = "绑定说明"
        '
        '显示列设置说明ToolStripMenuItem
        '
        Me.显示列设置说明ToolStripMenuItem.Name = "显示列设置说明ToolStripMenuItem"
        Me.显示列设置说明ToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.显示列设置说明ToolStripMenuItem.Text = "显示列设置说明"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(184, 41)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(170, 21)
        Me.txtName.TabIndex = 12
        Me.txtName.TabStop = False
        '
        'txtTiShi
        '
        Me.txtTiShi.Location = New System.Drawing.Point(433, 41)
        Me.txtTiShi.Name = "txtTiShi"
        Me.txtTiShi.Size = New System.Drawing.Size(290, 21)
        Me.txtTiShi.TabIndex = 14
        Me.txtTiShi.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(855, 418)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(169, 351)
        Me.TextBox1.TabIndex = 15
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "当前提供的公式有：１.　＆：用于连接文本　２.　＋、-、*、/:用于数字的加减乘除运算； 3. V_FZSX:用于读取相应科目的辅助属性值，格式为V_FZSX(科" & _
            "目代码，辅助属性名称） 。 目前不支持括号"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgvPingZheng, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.ckbKucun, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvKuCun, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.ckbGongZi, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvGongzi, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 409)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(830, 280)
        Me.TableLayoutPanel1.TabIndex = 16
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.19685!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.80315!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 388.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.ckbPingZheng, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cbPZSCFS, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtZhangShu, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblZhangShu, 3, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 165)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(824, 22)
        Me.TableLayoutPanel2.TabIndex = 11
        '
        'cbPZSCFS
        '
        Me.cbPZSCFS.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbPZSCFS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPZSCFS.FormattingEnabled = True
        Me.cbPZSCFS.Items.AddRange(New Object() {"逐条凭证", "逐单凭证"})
        Me.cbPZSCFS.Location = New System.Drawing.Point(179, 3)
        Me.cbPZSCFS.Name = "cbPZSCFS"
        Me.cbPZSCFS.Size = New System.Drawing.Size(179, 20)
        Me.cbPZSCFS.TabIndex = 11
        '
        'txtZhangShu
        '
        Me.txtZhangShu.Location = New System.Drawing.Point(364, 3)
        Me.txtZhangShu.Name = "txtZhangShu"
        Me.txtZhangShu.Size = New System.Drawing.Size(65, 21)
        Me.txtZhangShu.TabIndex = 12
        Me.txtZhangShu.Text = "1"
        '
        'lblZhangShu
        '
        Me.lblZhangShu.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblZhangShu.AutoSize = True
        Me.lblZhangShu.Location = New System.Drawing.Point(438, 10)
        Me.lblZhangShu.Name = "lblZhangShu"
        Me.lblZhangShu.Size = New System.Drawing.Size(113, 12)
        Me.lblZhangShu.TabIndex = 13
        Me.lblZhangShu.Text = "张单据生成一张凭证"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(4, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 12)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Label7"
        '
        'FkLable2
        '
        Me.FkLable2.AutoSize = True
        Me.FkLable2.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.FkLable2.Location = New System.Drawing.Point(374, 44)
        Me.FkLable2.Name = "FkLable2"
        Me.FkLable2.Size = New System.Drawing.Size(53, 12)
        Me.FkLable2.TabIndex = 13
        Me.FkLable2.Text = "功能提示"
        '
        'FkLable1
        '
        Me.FkLable1.AutoSize = True
        Me.FkLable1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.FkLable1.Location = New System.Drawing.Point(125, 44)
        Me.FkLable1.Name = "FkLable1"
        Me.FkLable1.Size = New System.Drawing.Size(53, 12)
        Me.FkLable1.TabIndex = 11
        Me.FkLable1.Text = "功能名称"
        '
        'FreedomCore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1024, 698)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.txtTiShi)
        Me.Controls.Add(Me.FkLable2)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.FkLable1)
        Me.Controls.Add(Me.gbSelect)
        Me.Controls.Add(Me.gbSX)
        Me.Controls.Add(Me.msFreedomCore)
        Me.Name = "FreedomCore"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FreedomCore"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.gbSelect.ResumeLayout(False)
        Me.gbSelect.PerformLayout()
        Me.gbSX.ResumeLayout(False)
        Me.gbSX.PerformLayout()
        CType(Me.dgvKuCun, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGongzi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPingZheng, System.ComponentModel.ISupportInitialize).EndInit()
        Me.msFreedomCore.ResumeLayout(False)
        Me.msFreedomCore.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ShuLiang As Freedom.FKTextBox
    Friend WithEvents lblShuLiang As Freedom.FKLable
    Friend WithEvents BianHao As Freedom.FKTextBox
    Friend WithEvents lblBianHao As Freedom.FKLable
    Friend WithEvents txtSXY As Freedom.FKTextBox
    Friend WithEvents txtSXX As Freedom.FKTextBox
    Friend WithEvents txtSXzhi As Freedom.FKTextBox
    Friend WithEvents Label6 As Freedom.FKLable
    Friend WithEvents Label5 As Freedom.FKLable
    Friend WithEvents Label4 As Freedom.FKLable
    Friend WithEvents Label3 As Freedom.FKLable
    Friend WithEvents cbSXqiyong As Freedom.FKComboBox
    Friend WithEvents gbSX As System.Windows.Forms.GroupBox
    Friend WithEvents lblJingShouRem As Freedom.FKLable
    Friend WithEvents lblBeiyong1 As Freedom.FKLable
    Friend WithEvents Beiyong1 As Freedom.FKTextBox
    Friend WithEvents lblZhaiYao As Freedom.FKLable
    Friend WithEvents Zhaiyao As Freedom.FKTextBox
    Friend WithEvents lblJinE As Freedom.FKLable
    Friend WithEvents JinE As Freedom.FKTextBox
    Friend WithEvents lblDate As Freedom.FKLable
    Friend WithEvents lblDanJia As Freedom.FKLable
    Friend WithEvents DanJia As Freedom.FKTextBox
    Friend WithEvents lblName As Freedom.FKLable
    Friend WithEvents JingShouRen As Freedom.FKComboBox
    Friend WithEvents lblZhukemu1 As Freedom.FKLable
    Friend WithEvents lblMXkemu1 As Freedom.FKLable
    Friend WithEvents lblZhuKemu2 As Freedom.FKLable
    Friend WithEvents lblMXkemu2 As Freedom.FKLable
    Friend WithEvents CangKu As Freedom.FKComboBox
    Friend WithEvents lblCangku As Freedom.FKLable
    Friend WithEvents DanWei As Freedom.FKLable
    Friend WithEvents DaiFangKeMu As Freedom.FKTextBox
    Friend WithEvents DaiFangZhuKeMu As Freedom.FKTextBox
    Friend WithEvents JieFangZhuKemu As Freedom.FKTextBox
    Public WithEvents JieFangKeMu As Freedom.FKTextBox
    Friend WithEvents txtSXTabindex As Freedom.FKTextBox
    Friend WithEvents Label22 As Freedom.FKLable
    Friend WithEvents Label21 As Freedom.FKLable
    Friend WithEvents Label20 As Freedom.FKLable
    Friend WithEvents cbSXgeshi As Freedom.FKComboBox
    Friend WithEvents cbSXzhidu As Freedom.FKComboBox
    Friend WithEvents lblSXID As Freedom.FKLable
    Friend WithEvents RiQi As Freedom.FKDateTimePicker
    Friend WithEvents txtSXH As Freedom.FKTextBox
    Friend WithEvents txtSXW As Freedom.FKTextBox
    Friend WithEvents btnSXFont As System.Windows.Forms.Button
    Friend WithEvents FontDialog1 As System.Windows.Forms.FontDialog
    Friend WithEvents txtSXFont As Freedom.FKTextBox
    Friend WithEvents btnSXColor As System.Windows.Forms.Button
    Friend WithEvents txtSXYanse As Freedom.FKTextBox
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents Label2 As Freedom.FKLable
    Friend WithEvents Label1 As Freedom.FKLable
    Friend WithEvents cbSXID As Freedom.FKComboBox
    Friend WithEvents gbSelect As System.Windows.Forms.GroupBox
    Friend WithEvents dgvKuCun As System.Windows.Forms.DataGridView
    Friend WithEvents dgvGongzi As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPingZheng As System.Windows.Forms.DataGridView
    Friend WithEvents ckbKucun As System.Windows.Forms.CheckBox
    Friend WithEvents ckbGongZi As System.Windows.Forms.CheckBox
    Friend WithEvents ckbPingZheng As System.Windows.Forms.CheckBox
    Friend WithEvents lblBeiyong3 As Freedom.FKLable
    Friend WithEvents BeiYong3 As Freedom.FKTextBox
    Friend WithEvents lblBeiYong2 As Freedom.FKLable
    Friend WithEvents BeiYong2 As Freedom.FKTextBox
    Friend WithEvents msFreedomCore As System.Windows.Forms.MenuStrip
    Friend WithEvents FkLable1 As Freedom.FKLable
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtTiShi As System.Windows.Forms.TextBox
    Friend WithEvents FkLable2 As Freedom.FKLable
    Friend WithEvents BeiYong4 As Freedom.FKComboBox
    Friend WithEvents BeiYong5 As Freedom.FKDateTimePicker
    Friend WithEvents lblBeiYong5 As Freedom.FKLable
    Friend WithEvents lblBeiYong4 As Freedom.FKLable
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbSXBiTian As Freedom.FKComboBox
    Friend WithEvents txtSXBangDing As Freedom.FKTextBox
    Friend WithEvents FkLable4 As Freedom.FKLable
    Friend WithEvents FkLable3 As Freedom.FKLable
    Friend WithEvents cbSXSuoDing As Freedom.FKComboBox
    Friend WithEvents FkLable5 As Freedom.FKLable
    Friend WithEvents PingZhengZi As Freedom.FKComboBox
    Friend WithEvents lblPzHao As Freedom.FKLable
    Friend WithEvents PingZhengHao As Freedom.FKTextBox
    Friend WithEvents 显示列设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存显示列设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资显示列设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 凭证显示列设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 保存 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 另存为 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 功能导出 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 导入功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 退出 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 显示表设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 显示库存表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 显示工资表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 显示凭证表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 设计注意事项 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 基本规则 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents 公式说明ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 绑定说明ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 显示列设置说明ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cbPZSCFS As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents 打印设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存打印设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资打印设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 凭证打印设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblBeiyong14 As Freedom.FKLable
    Friend WithEvents BeiYong14 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong13 As Freedom.FKLable
    Friend WithEvents BeiYong13 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong12 As Freedom.FKLable
    Friend WithEvents BeiYong12 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong11 As Freedom.FKLable
    Friend WithEvents BeiYong11 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong10 As Freedom.FKLable
    Friend WithEvents BeiYong10 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong9 As Freedom.FKLable
    Friend WithEvents BeiYong9 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong8 As Freedom.FKLable
    Friend WithEvents BeiYong8 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong7 As Freedom.FKLable
    Friend WithEvents BeiYong7 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong6 As Freedom.FKLable
    Friend WithEvents BeiYong6 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong15 As Freedom.FKLable
    Friend WithEvents BeiYong15 As Freedom.FKTextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtZhangShu As System.Windows.Forms.TextBox
    Friend WithEvents lblZhangShu As System.Windows.Forms.Label
    Friend WithEvents lblBeiyong30 As Freedom.FKLable
    Friend WithEvents lblBeiyong25 As Freedom.FKLable
    Friend WithEvents lblBeiyong20 As Freedom.FKLable
    Friend WithEvents Beiyong30 As Freedom.FKTextBox
    Friend WithEvents Beiyong25 As Freedom.FKTextBox
    Friend WithEvents Beiyong20 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong29 As Freedom.FKLable
    Friend WithEvents lblBeiyong24 As Freedom.FKLable
    Friend WithEvents Beiyong29 As Freedom.FKTextBox
    Friend WithEvents Beiyong24 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong19 As Freedom.FKLable
    Friend WithEvents lblBeiyong28 As Freedom.FKLable
    Friend WithEvents Beiyong19 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong23 As Freedom.FKLable
    Friend WithEvents Beiyong28 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong18 As Freedom.FKLable
    Friend WithEvents Beiyong23 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong27 As Freedom.FKLable
    Friend WithEvents Beiyong18 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong22 As Freedom.FKLable
    Friend WithEvents Beiyong27 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong17 As Freedom.FKLable
    Friend WithEvents Beiyong22 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong26 As Freedom.FKLable
    Friend WithEvents Beiyong17 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong21 As Freedom.FKLable
    Friend WithEvents Beiyong26 As Freedom.FKTextBox
    Friend WithEvents lblBeiyong16 As Freedom.FKLable
    Friend WithEvents Beiyong21 As Freedom.FKTextBox
    Friend WithEvents Beiyong16 As Freedom.FKTextBox
End Class
