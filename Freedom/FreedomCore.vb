﻿Public Class FreedomCore

    Dim sObjName As String
    Dim mouseP As Drawing.Point
    Dim ctrlP As Drawing.Point

    Private iGongnengID As Integer
    Public WriteOnly Property myGongneng() As Integer
        Set(ByVal value As Integer)
            iGongnengID = value
        End Set
    End Property

    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 36
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            sCDO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        End If

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
            Next
        End If

        Return sMCO & FKG.myselfG.sVersion & FKG.myselfG.sBanBen & FKG.myselfG.sClient
    End Function

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet
        Dim cpu

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    Private Function HID() As String
        Dim HDid As String = ""
        Try
            Dim cimobject As New Management.ManagementClass("win32_diskdrive")
            Dim moc As Management.ManagementObjectCollection
            moc = cimobject.GetInstances()
            Dim mo As Management.ManagementObject

            For Each mo In moc
                'HDid = HDid & mo.Properties("signature").Value.ToString
                HDid = HDid & mo.Properties("PNPDeviceID").Value.ToString
                HDid = HDid.Replace("_", "")
                HDid = HDid.Remove(0, mo.Properties("Caption").Value.ToString.Length)
                Exit For
            Next
        Catch ex As Exception
            HDid = ""
        End Try

        Return HDid

    End Function


    Private Sub FreedomCore_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        sObjName = Me.lblName.Name
        FillCBID()

        If iGongnengID = -1 Then
            FillFKSX()
            Me.显示列设置.Enabled = False
            Me.cbPZSCFS.SelectedIndex = 0
        Else
            ShowGongNeng(iGongnengID)
        End If

        FillKuCun()
        FillSX(Me.lblName)
        DispSize()
    End Sub

    Private Sub FillCBID()
        Dim obj As Windows.Forms.Control
        For Each obj In Me.gbSelect.Controls
            Me.cbSXID.Items.Add(obj.Name)
        Next
        Me.cbSXID.Sorted = True
    End Sub
    Private Sub FillFKSX()
        Dim obj As Windows.Forms.Control
        For Each obj In Me.gbSelect.Controls
            Select Case obj.GetType.Name
                Case "FKLable"
                    With CType(obj, Freedom.FKLable).FKShuXing
                        .ID = obj.Name
                        .QiYong = obj.Visible
                        .Zhi = obj.Text
                        .GeShi = "无效"
                        .X = obj.Location.X
                        .Y = obj.Location.Y
                        .W = obj.Size.Width
                        .H = obj.Size.Height
                        .ZhiDu = True
                        .Tab = 10
                        .ZiTi = Me.FontToString(obj.Font)
                        .YanSe = obj.ForeColor
                    End With
                Case "FKTextBox"
                    With CType(obj, Freedom.FKTextBox).FKShuXing
                        .ID = obj.Name
                        .QiYong = obj.Visible
                        .Zhi = obj.Text
                        .GeShi = "数字"
                        .X = obj.Location.X
                        .Y = obj.Location.Y
                        .W = obj.Size.Width
                        .H = obj.Size.Height
                        .ZhiDu = False
                        .Tab = 10
                        .ZiTi = Me.FontToString(obj.Font)
                        .YanSe = obj.ForeColor
                    End With
                Case "FKComboBox"
                    With CType(obj, Freedom.FKComboBox).FKShuXing
                        .ID = obj.Name
                        .QiYong = obj.Visible
                        .Zhi = obj.Text
                        .GeShi = "文本"
                        .X = obj.Location.X
                        .Y = obj.Location.Y
                        .W = obj.Size.Width
                        .H = obj.Size.Height
                        .ZhiDu = False
                        .Tab = 10
                        .ZiTi = Me.FontToString(obj.Font)
                        .YanSe = obj.ForeColor
                    End With
                Case "FKDateTimePicker"
                    With CType(obj, Freedom.FKDateTimePicker).FKShuXing
                        .ID = obj.Name
                        .QiYong = obj.Visible
                        .Zhi = obj.Text
                        .GeShi = "无效"
                        .X = obj.Location.X
                        .Y = obj.Location.Y
                        .W = obj.Size.Width
                        .H = obj.Size.Height
                        .ZhiDu = False
                        .Tab = 10
                        .ZiTi = Me.FontToString(obj.Font)
                        .YanSe = obj.ForeColor
                    End With
                Case Else
                    MsgBox("意外的控件类型")
            End Select
        Next
    End Sub

    Private Sub FillSX(ByVal obj As Object)
        Select Case obj.GetType.Name
            Case "FKLable"
                With CType(obj, Freedom.FKLable).FKShuXing
                    Me.cbSXID.Text = .ID
                    Me.cbSXqiyong.Text = .QiYong
                    Me.txtSXzhi.Text = .Zhi
                    Me.cbSXgeshi.Text = "无效"
                    Me.cbSXgeshi.Enabled = False
                    Me.txtSXX.Text = .X
                    Me.txtSXY.Text = .Y
                    Me.txtSXW.Text = .W
                    Me.txtSXH.Text = .H
                    Me.cbSXzhidu.Text = .ZhiDu
                    'Me.cbSXzhidu.Enabled = False
                    Me.txtSXTabindex.Text = .Tab
                    'Me.txtSXFont.Text = .ZiTi.Name
                    Me.txtSXYanse.Text = .YanSe.Name
                    Me.cbSXBiTian.Text = .BiTian
                    Me.cbSXSuoDing.Text = .SuoDing
                    Me.txtSXBangDing.Text = .BangDing
                End With
            Case "FKTextBox"
                With CType(obj, Freedom.FKTextBox).FKShuXing
                    Me.cbSXID.Text = .ID
                    Me.cbSXqiyong.Text = .QiYong
                    Me.txtSXzhi.Text = .Zhi
                    Me.cbSXgeshi.Enabled = True
                    Me.cbSXgeshi.Text = .GeShi
                    Me.txtSXX.Text = .X
                    Me.txtSXY.Text = .Y
                    Me.txtSXW.Text = .W
                    Me.txtSXH.Text = .H
                    'Me.cbSXzhidu.Enabled = .ZhiDu
                    Me.cbSXzhidu.Text = .ZhiDu
                    Me.txtSXTabindex.Text = .Tab
                    'Me.txtSXFont.Text = .ZiTi.Name
                    Me.txtSXYanse.Text = .YanSe.Name
                    Me.cbSXBiTian.Text = .BiTian
                    Me.cbSXSuoDing.Text = .SuoDing
                    Me.txtSXBangDing.Text = .BangDing
                End With
            Case "FKComboBox"
                With CType(obj, Freedom.FKComboBox).FKShuXing
                    Me.cbSXID.Text = .ID
                    Me.cbSXqiyong.Text = .QiYong
                    Me.txtSXzhi.Text = .Zhi
                    Me.cbSXgeshi.Enabled = False
                    Me.cbSXgeshi.Text = "无效"
                    Me.txtSXX.Text = .X
                    Me.txtSXY.Text = .Y
                    Me.txtSXW.Text = .W
                    Me.txtSXH.Text = .H
                    Me.cbSXzhidu.Text = .ZhiDu
                    'Me.cbSXzhidu.Enabled = False
                    Me.txtSXTabindex.Text = .Tab
                    'Me.txtSXFont.Text = .ZiTi.Name
                    Me.txtSXYanse.Text = .YanSe.Name
                    Me.cbSXBiTian.Text = .BiTian
                    Me.cbSXSuoDing.Text = .SuoDing
                    Me.txtSXBangDing.Text = .BangDing
                End With
            Case "FKDateTimePicker"
                With CType(obj, Freedom.FKDateTimePicker).FKShuXing
                    Me.cbSXID.Text = .ID
                    Me.cbSXqiyong.Text = .QiYong
                    Me.txtSXzhi.Text = .Zhi
                    Me.cbSXgeshi.Text = "无效"
                    Me.cbSXgeshi.Enabled = False
                    Me.txtSXX.Text = .X
                    Me.txtSXY.Text = .Y
                    Me.txtSXW.Text = .W
                    Me.txtSXH.Text = .H
                    Me.cbSXzhidu.Text = .ZhiDu
                    'Me.cbSXzhidu.Enabled = False
                    Me.txtSXTabindex.Text = .Tab
                    'Me.txtSXFont.Text = .ZiTi.Name
                    Me.txtSXYanse.Text = .YanSe.Name
                    Me.cbSXBiTian.Text = .BiTian
                    Me.cbSXSuoDing.Text = .SuoDing
                    Me.txtSXBangDing.Text = .BangDing
                End With
            Case Else
                MsgBox("意外的控件类型")

        End Select
    End Sub


    Private Sub mouse_down(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CangKu.MouseDown, JingShouRen.MouseDown, RiQi.MouseDown, lblBeiyong1.MouseDown, lblBianHao.MouseDown, lblCangku.MouseDown, lblDanJia.MouseDown, lblDate.MouseDown, lblJinE.MouseDown, lblJingShouRem.MouseDown, lblMXkemu1.MouseDown, lblMXkemu2.MouseDown, lblName.MouseDown, lblShuLiang.MouseDown, lblZhaiYao.MouseDown, lblZhukemu1.MouseDown, lblZhuKemu2.MouseDown, Beiyong1.MouseDown, BianHao.MouseDown, DanJia.MouseDown, JinE.MouseDown, JieFangKeMu.MouseDown, DaiFangKeMu.MouseDown, ShuLiang.MouseDown, Zhaiyao.MouseDown, JieFangZhuKemu.MouseDown, DaiFangZhuKeMu.MouseDown, lblBeiyong3.MouseDown, lblBeiYong2.MouseDown, BeiYong3.MouseDown, BeiYong2.MouseDown, DanWei.MouseDown, lblBeiYong5.MouseDown, lblBeiYong4.MouseDown, BeiYong5.MouseDown, BeiYong4.MouseDown, lblPzHao.MouseDown, PingZhengZi.MouseDown, PingZhengHao.MouseDown, lblBeiyong6.MouseDown, BeiYong6.MouseDown, lblBeiyong7.MouseDown, BeiYong7.MouseDown, lblBeiyong8.MouseDown, BeiYong8.MouseDown, lblBeiyong9.MouseDown, BeiYong9.MouseDown, lblBeiyong10.MouseDown, BeiYong10.MouseDown, lblBeiyong11.MouseDown, BeiYong11.MouseDown, lblBeiyong12.MouseDown, BeiYong12.MouseDown, lblBeiyong13.MouseDown, BeiYong13.MouseDown, lblBeiyong14.MouseDown, BeiYong14.MouseDown, lblBeiyong15.MouseDown, BeiYong15.MouseDown, lblBeiyong20.MouseDown, lblBeiyong19.MouseDown, lblBeiyong18.MouseDown, lblBeiyong17.MouseDown, lblBeiyong16.MouseDown, Beiyong24.MouseDown, Beiyong23.MouseDown, Beiyong22.MouseDown, Beiyong21.MouseDown, Beiyong20.MouseDown, Beiyong19.MouseDown, Beiyong18.MouseDown, Beiyong17.MouseDown, Beiyong30.MouseDown, Beiyong29.MouseDown, Beiyong28.MouseDown, Beiyong27.MouseDown, Beiyong26.MouseDown, Beiyong25.MouseDown, Beiyong16.MouseDown, lblBeiyong30.MouseDown, lblBeiyong29.MouseDown, lblBeiyong28.MouseDown, lblBeiyong27.MouseDown, lblBeiyong26.MouseDown, lblBeiyong25.MouseDown, lblBeiyong24.MouseDown, lblBeiyong23.MouseDown, lblBeiyong22.MouseDown, lblBeiyong21.MouseDown
        Select Case sender.GetType.Name
            Case "FKLable"
                sObjName = CType(sender, Freedom.FKLable).Name
            Case "FKTextBox"
                sObjName = CType(sender, Freedom.FKTextBox).Name
            Case "FKComboBox"
                sObjName = CType(sender, Freedom.FKComboBox).Name
            Case "FKDateTimePicker"
                sObjName = CType(sender, Freedom.FKDateTimePicker).Name
            Case Else
                MsgBox("意外的控件类型")

        End Select

        If e.Button = Windows.Forms.MouseButtons.Left Then
            mouseP = e.Location - Me.Location
            ctrlP = Me.gbSelect.Controls.Item(sObjName).Location
        End If

        FillSX(sender)
    End Sub

    '鼠标松开时，移动控件
    Private Sub mouse_up(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CangKu.MouseUp, JingShouRen.MouseUp, RiQi.MouseUp, lblBeiyong1.MouseUp, lblBianHao.MouseUp, lblCangku.MouseUp, lblDanJia.MouseUp, lblDate.MouseUp, lblJinE.MouseUp, lblJingShouRem.MouseUp, lblMXkemu1.MouseUp, lblMXkemu2.MouseUp, lblName.MouseUp, lblShuLiang.MouseUp, lblZhaiYao.MouseUp, lblZhukemu1.MouseUp, lblZhuKemu2.MouseUp, Beiyong1.MouseUp, BianHao.MouseUp, DanJia.MouseUp, JinE.MouseUp, JieFangKeMu.MouseUp, DaiFangKeMu.MouseUp, ShuLiang.MouseUp, Zhaiyao.MouseUp, JieFangZhuKemu.MouseUp, DaiFangZhuKeMu.MouseUp, lblBeiyong3.MouseUp, lblBeiYong2.MouseUp, BeiYong3.MouseUp, BeiYong2.MouseUp, DanWei.MouseUp, BeiYong5.MouseUp, lblBeiYong4.MouseUp, lblBeiYong5.MouseUp, BeiYong4.MouseUp, lblPzHao.MouseUp, PingZhengZi.MouseUp, PingZhengHao.MouseUp, lblBeiyong6.MouseUp, BeiYong6.MouseUp, lblBeiyong7.MouseUp, BeiYong7.MouseUp, lblBeiyong8.MouseUp, BeiYong8.MouseUp, lblBeiyong9.MouseUp, BeiYong9.MouseUp, lblBeiyong10.MouseUp, BeiYong10.MouseUp, lblBeiyong11.MouseUp, BeiYong11.MouseUp, lblBeiyong12.MouseUp, BeiYong12.MouseUp, lblBeiyong13.MouseUp, BeiYong13.MouseUp, lblBeiyong14.MouseUp, BeiYong14.MouseUp, lblBeiyong15.MouseUp, BeiYong15.MouseUp, lblBeiyong20.MouseUp, lblBeiyong19.MouseUp, lblBeiyong18.MouseUp, lblBeiyong17.MouseUp, lblBeiyong16.MouseUp, lblBeiyong21.MouseUp, lblBeiyong22.MouseUp, lblBeiyong23.MouseUp, lblBeiyong24.MouseUp, lblBeiyong25.MouseUp, lblBeiyong26.MouseUp, lblBeiyong27.MouseUp, lblBeiyong28.MouseUp, lblBeiyong29.MouseUp, lblBeiyong30.MouseUp, Beiyong16.MouseUp, Beiyong17.MouseUp, Beiyong18.MouseUp, Beiyong19.MouseUp, Beiyong20.MouseUp, Beiyong21.MouseUp, Beiyong22.MouseUp, Beiyong23.MouseUp, Beiyong24.MouseUp, Beiyong25.MouseUp, Beiyong26.MouseUp, Beiyong27.MouseUp, Beiyong28.MouseUp, Beiyong29.MouseUp, Beiyong30.MouseUp

        If e.Button = Windows.Forms.MouseButtons.Left Then
            Me.txtSXX.Text = Me.gbSelect.Controls.Item(sObjName).Location.X
            Me.txtSXY.Text = Me.gbSelect.Controls.Item(sObjName).Location.Y

            Select Case sender.GetType.Name
                Case "FKLable"
                    CType(sender, Freedom.FKLable).FKShuXing.X = Me.gbSelect.Controls.Item(sObjName).Location.X
                    CType(sender, Freedom.FKLable).FKShuXing.Y = Me.gbSelect.Controls.Item(sObjName).Location.Y
                Case "FKTextBox"
                    CType(sender, Freedom.FKTextBox).FKShuXing.X = Me.gbSelect.Controls.Item(sObjName).Location.X
                    CType(sender, Freedom.FKTextBox).FKShuXing.Y = Me.gbSelect.Controls.Item(sObjName).Location.Y
                Case "FKComboBox"
                    'If CType(sender, Freedom.FKComboBox).DroppedDown = True Then
                    '    Exit Sub
                    'Else
                    CType(sender, Freedom.FKComboBox).FKShuXing.X = Me.gbSelect.Controls.Item(sObjName).Location.X
                    CType(sender, Freedom.FKComboBox).FKShuXing.Y = Me.gbSelect.Controls.Item(sObjName).Location.Y
                    'End If

                Case "FKDateTimePicker"
                    'if ctype(sender,FKDateTimePicker).d
                    CType(sender, Freedom.FKDateTimePicker).FKShuXing.X = Me.gbSelect.Controls.Item(sObjName).Location.X
                    CType(sender, Freedom.FKDateTimePicker).FKShuXing.Y = Me.gbSelect.Controls.Item(sObjName).Location.Y
                Case Else
                    MsgBox("意外的控件类型")
            End Select

            Me.gbSelect.Controls.Item(sObjName).Location = ctrlP + (e.Location - Me.Location - mouseP)
            Me.Refresh()
        End If
    End Sub


    Private Sub txtSXX_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSXX.LostFocus
        ctrlP.X = Me.txtSXX.Text
        ctrlP.Y = Me.txtSXY.Text

        Me.gbSelect.Controls.Item(sObjName).Location = ctrlP

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.Y = ctrlP.Y
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.Y = ctrlP.Y
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.Y = ctrlP.Y
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.Y = ctrlP.Y
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub


    Private Sub txtSXY_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSXY.LostFocus
        ctrlP.X = Me.txtSXX.Text
        ctrlP.Y = Me.txtSXY.Text

        Me.gbSelect.Controls.Item(sObjName).Location = ctrlP

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.Y = ctrlP.Y
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.Y = ctrlP.Y
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.Y = ctrlP.Y
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.X = ctrlP.X
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.Y = ctrlP.Y
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub btnSXFont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSXFont.Click
        If Me.FontDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim fFont As Drawing.Font

            fFont = Me.FontDialog1.Font
            Me.txtSXFont.Text = FontToString(fFont)
            Me.gbSelect.Controls(sObjName).Font = fFont

            Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
                Case "FKLable"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.ZiTi = Me.txtSXFont.Text
                Case "FKTextBox"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.ZiTi = Me.txtSXFont.Text
                Case "FKComboBox"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.ZiTi = Me.txtSXFont.Text
                Case "FKDateTimePicker"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.ZiTi = Me.txtSXFont.Text
                Case Else
                    MsgBox("意外的控件类型")
            End Select
        End If
    End Sub

    Private Function FontToString(ByVal fFont As Drawing.Font) As String
        Dim sFont As String = ""
        sFont = fFont.Name & ","
        sFont = sFont & fFont.SizeInPoints.ToString & ","
        sFont = sFont & fFont.Style & ","
        sFont = sFont & fFont.Strikeout.ToString & ","
        sFont = sFont & fFont.Underline.ToString

        Return sFont
    End Function

    Private Function StringToFont(ByVal sFont As String)
        Dim fFont As Drawing.Font
        Dim sF() As String
        sF = Split(sFont, ",")
        If sF(0) = "" Then
            fFont = New Drawing.Font("宋体", 9, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point)
        Else
            fFont = New Drawing.Font(sF(0), sF(1), sF(2), Drawing.GraphicsUnit.Point)
        End If

        Return fFont
    End Function

    Private Sub btnSXColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSXColor.Click
        If Me.ColorDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.gbSelect.Controls.Item(sObjName).ForeColor = Me.ColorDialog1.Color
            Me.txtSXYanse.Text = Me.ColorDialog1.Color.Name

            Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
                Case "FKLable"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.YanSe = Me.ColorDialog1.Color
                Case "FKTextBox"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.YanSe = Me.ColorDialog1.Color
                Case "FKComboBox"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.YanSe = Me.ColorDialog1.Color
                Case "FKDateTimePicker"
                    CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.YanSe = Me.ColorDialog1.Color
                Case Else
                    MsgBox("意外的控件类型")
            End Select
        End If
    End Sub


    Private Sub cbSXID_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSXID.SelectedIndexChanged
        sObjName = Me.cbSXID.Text
        FillSX(Me.gbSelect.Controls.Item(sObjName))
    End Sub

    Private Sub cbSXqiyong_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSXqiyong.SelectedIndexChanged

        Me.gbSelect.Controls.Item(sObjName).Visible = Me.cbSXqiyong.SelectedItem

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.QiYong = Me.cbSXqiyong.SelectedItem
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.QiYong = Me.cbSXqiyong.SelectedItem
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.QiYong = Me.cbSXqiyong.SelectedItem
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.QiYong = Me.cbSXqiyong.SelectedItem
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub txtSXzhi_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSXzhi.LostFocus
        Me.gbSelect.Controls.Item(sObjName).Text = Me.txtSXzhi.Text

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.Zhi = Me.txtSXzhi.Text
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.Zhi = Me.txtSXzhi.Text
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.Zhi = Me.txtSXzhi.Text
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.Zhi = Me.txtSXzhi.Text
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub cbSXgeshi_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSXgeshi.SelectedIndexChanged

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.GeShi = Me.cbSXgeshi.SelectedItem
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.GeShi = Me.cbSXgeshi.SelectedItem
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.GeShi = Me.cbSXgeshi.SelectedItem
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.GeShi = Me.cbSXgeshi.SelectedItem
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub txtSXW_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSXW.LostFocus

        Dim sss As Drawing.Size
        sss.Width = Me.txtSXW.Text
        sss.Height = Me.txtSXH.Text
        Me.gbSelect.Controls.Item(sObjName).Size = sss

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.H = sss.Height
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.H = sss.Height
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.H = sss.Height
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.H = sss.Height
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub txtSXH_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSXH.LostFocus
        Dim sss As Drawing.Size
        sss.Width = Me.txtSXW.Text
        sss.Height = Me.txtSXH.Text
        Me.gbSelect.Controls.Item(sObjName).Size = sss

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.H = sss.Height
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.H = sss.Height
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.H = sss.Height
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.W = sss.Width
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.H = sss.Height
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub txtSXBangDing_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSXBangDing.LostFocus
        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.BangDing = Me.txtSXBangDing.Text
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.BangDing = Me.txtSXBangDing.Text
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.BangDing = Me.txtSXBangDing.Text
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.BangDing = Me.txtSXBangDing.Text
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub cbSXzhidu_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSXzhidu.SelectedIndexChanged

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.ZhiDu = Me.cbSXzhidu.SelectedItem

            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).ReadOnly = Me.cbSXzhidu.SelectedItem
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.ZhiDu = Me.cbSXzhidu.SelectedItem
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.ZhiDu = Me.cbSXzhidu.SelectedItem

            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.ZhiDu = Me.cbSXzhidu.SelectedItem

            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub cbSXSuoDing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSXSuoDing.SelectedIndexChanged
        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.SuoDing = Me.cbSXSuoDing.SelectedItem
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.SuoDing = Me.cbSXSuoDing.SelectedItem
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.SuoDing = Me.cbSXSuoDing.SelectedItem
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.SuoDing = Me.cbSXSuoDing.SelectedItem
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub


    Private Sub cbSXBiTian_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSXBiTian.SelectedIndexChanged
        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.BiTian = Me.cbSXBiTian.SelectedItem
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.BiTian = Me.cbSXBiTian.SelectedItem
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.BiTian = Me.cbSXBiTian.SelectedItem
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.BiTian = Me.cbSXBiTian.SelectedItem
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub txtSXTabindex_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSXTabindex.LostFocus
        'Me.gbSelect.Controls.Item(sObjName).Text = Me.txtSXzhi.Text

        Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
            Case "FKLable"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing.Tab = Me.txtSXTabindex.Text
            Case "FKTextBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing.Tab = Me.txtSXTabindex.Text
            Case "FKComboBox"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing.Tab = Me.txtSXTabindex.Text
            Case "FKDateTimePicker"
                CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing.Tab = Me.txtSXTabindex.Text
            Case Else
                MsgBox("意外的控件类型")
        End Select
    End Sub

    Private Sub btnSave()
        'Dim fk As New FKG.myselfG
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim FKSX As FKShuXing = Nothing
        Dim sSQL As String

        '保存功能设置
        '首先保存功能名称
        Dim iGNSID As Integer
        Dim sGNMingCheng As String
        sGNMingCheng = Me.txtName.Text.Replace(" ", "")

        If sGNMingCheng = "" Then
            MsgBox("提示！功能名称不能为空。", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        If iGongnengID = -1 Then
            mdb.Write("insert into GongNengShu (nian,context,ParentID,[Index],Tishi,isGongNeng,PZSCFS,DJZhangShu) values (" & FKG.myselfG.NianFen & ",'" & sGNMingCheng & "',1,9,'" & Me.txtTiShi.Text & "','true'," & Me.cbPZSCFS.SelectedIndex & "," & Me.txtZhangShu.Text & ")")
            iGNSID = mdb.Reader("select ID from GongNengShu where Context='" & sGNMingCheng & "'").Tables(0).Rows(0).Item(0)
            iGongnengID = iGNSID
        Else
            mdb.Write("update Gongnengshu set Context='" & Me.txtName.Text & "',Tishi='" & Me.txtTiShi.Text & "',PZSCFS=" & Me.cbPZSCFS.SelectedIndex & ", DJZhangShu=" & Me.txtZhangShu.Text & "   where ID=" & iGongnengID)

            iGNSID = iGongnengID
            mdb.Write("delete from GN_KuCun where GNSID=" & iGNSID)
            mdb.Write("delete from GN_GongZi where GNSID=" & iGNSID)
            mdb.Write("delete from GN_PZ where GNSID=" & iGNSID)
            mdb.Write("delete from GongNeng where GNSID=" & iGNSID)
        End If

        Dim iColor As Integer
        '其次保存功能设置
        Dim obj As System.Windows.Forms.Control
        
        For Each obj In Me.gbSelect.Controls
            Select Case obj.GetType.Name
                Case "FKLable"
                    Dim objKF As New FKLable
                    objKF = CType(obj, Freedom.FKLable)
                    FKSX = objKF.FKShuXing
                    iColor = FKSX.YanSe.ToArgb
                    sSQL = "insert into gongneng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,Tabindex,ziti,yanse,bitian,bangding,suoding) values (" & FKG.myselfG.FKString(FKSX.ID) & iGNSID & "," & FKG.myselfG.NianFen & "," & FKG.myselfG.FKString(FKSX.Zhi) & "'" & FKSX.QiYong & "'," & FKG.myselfG.FKString(FKSX.GeShi) & FKSX.X & "," & FKSX.Y & "," & FKSX.W & "," & FKSX.H & ",'" & FKSX.ZhiDu & "'," & FKSX.Tab & "," & FKG.myselfG.FKString(FKSX.ZiTi) & iColor & ",'" & FKSX.BiTian & "','" & FKSX.BangDing & "','" & FKSX.SuoDing & "') "
                    mdb.Write(sSQL)
                    'objKF.Dispose()
                Case "FKTextBox"
                    Dim objKF As New FKTextBox
                    objKF = CType(obj, Freedom.FKTextBox)
                    FKSX = objKF.FKShuXing
                    iColor = FKSX.YanSe.ToArgb
                    'sSQL = "insert into gongneng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,Tabindex,ziti,yanse,bitian,bangding,suoding) values (" & FKG.myselfG.FKString(FKSX.ID) & iGNSID & "," & FKG.myselfG.NianFen & "," & FKG.myselfG.FKString(FKSX.Zhi) & FKSX.QiYong & "," & FKG.myselfG.FKString(FKSX.GeShi) & FKSX.X & "," & FKSX.Y & "," & FKSX.W & "," & FKSX.H & "," & FKSX.ZhiDu & "," & FKSX.Tab & "," & FKG.myselfG.FKString(FKSX.ZiTi) & iColor & "," & FKSX.BiTian & ",'" & FKSX.BangDing & "'," & FKSX.SuoDing & ") "
                    sSQL = "insert into gongneng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,Tabindex,ziti,yanse,bitian,bangding,suoding) values (" & FKG.myselfG.FKString(FKSX.ID) & iGNSID & "," & FKG.myselfG.NianFen & "," & FKG.myselfG.FKString(FKSX.Zhi) & "'" & FKSX.QiYong & "'," & FKG.myselfG.FKString(FKSX.GeShi) & FKSX.X & "," & FKSX.Y & "," & FKSX.W & "," & FKSX.H & ",'" & FKSX.ZhiDu & "'," & FKSX.Tab & "," & FKG.myselfG.FKString(FKSX.ZiTi) & iColor & ",'" & FKSX.BiTian & "','" & FKSX.BangDing & "','" & FKSX.SuoDing & "') "
                    mdb.Write(sSQL)
                    'objKF.Dispose()
                Case "FKComboBox"
                    Dim objKF As New FKComboBox
                    objKF = CType(obj, Freedom.FKComboBox)
                    FKSX = objKF.FKShuXing
                    iColor = FKSX.YanSe.ToArgb
                    'sSQL = "insert into gongneng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,Tabindex,ziti,yanse,bitian,bangding,suoding) values (" & FKG.myselfG.FKString(FKSX.ID) & iGNSID & "," & FKG.myselfG.NianFen & "," & FKG.myselfG.FKString(FKSX.Zhi) & FKSX.QiYong & "," & FKG.myselfG.FKString(FKSX.GeShi) & FKSX.X & "," & FKSX.Y & "," & FKSX.W & "," & FKSX.H & "," & FKSX.ZhiDu & "," & FKSX.Tab & "," & FKG.myselfG.FKString(FKSX.ZiTi) & iColor & "," & FKSX.BiTian & ",'" & FKSX.BangDing & "'," & FKSX.SuoDing & ") "
                    sSQL = "insert into gongneng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,Tabindex,ziti,yanse,bitian,bangding,suoding) values (" & FKG.myselfG.FKString(FKSX.ID) & iGNSID & "," & FKG.myselfG.NianFen & "," & FKG.myselfG.FKString(FKSX.Zhi) & "'" & FKSX.QiYong & "'," & FKG.myselfG.FKString(FKSX.GeShi) & FKSX.X & "," & FKSX.Y & "," & FKSX.W & "," & FKSX.H & ",'" & FKSX.ZhiDu & "'," & FKSX.Tab & "," & FKG.myselfG.FKString(FKSX.ZiTi) & iColor & ",'" & FKSX.BiTian & "','" & FKSX.BangDing & "','" & FKSX.SuoDing & "') "
                    mdb.Write(sSQL)
                    'objKF.Dispose()
                Case "FKDateTimePicker"
                    Dim objKF As New FKDateTimePicker
                    objKF = CType(obj, Freedom.FKDateTimePicker)
                    FKSX = objKF.FKShuXing
                    iColor = FKSX.YanSe.ToArgb
                    'sSQL = "insert into gongneng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,Tabindex,ziti,yanse,bitian,bangding,suoding) values (" & FKG.myselfG.FKString(FKSX.ID) & iGNSID & "," & FKG.myselfG.NianFen & "," & FKG.myselfG.FKString(FKSX.Zhi) & FKSX.QiYong & "," & FKG.myselfG.FKString(FKSX.GeShi) & FKSX.X & "," & FKSX.Y & "," & FKSX.W & "," & FKSX.H & "," & FKSX.ZhiDu & "," & FKSX.Tab & "," & FKG.myselfG.FKString(FKSX.ZiTi) & iColor & "," & FKSX.BiTian & ",'" & FKSX.BangDing & "'," & FKSX.SuoDing & ") "
                    sSQL = "insert into gongneng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,Tabindex,ziti,yanse,bitian,bangding,suoding) values (" & FKG.myselfG.FKString(FKSX.ID) & iGNSID & "," & FKG.myselfG.NianFen & "," & FKG.myselfG.FKString(FKSX.Zhi) & "'" & FKSX.QiYong & "'," & FKG.myselfG.FKString(FKSX.GeShi) & FKSX.X & "," & FKSX.Y & "," & FKSX.W & "," & FKSX.H & ",'" & FKSX.ZhiDu & "'," & FKSX.Tab & "," & FKG.myselfG.FKString(FKSX.ZiTi) & iColor & ",'" & FKSX.BiTian & "','" & FKSX.BangDing & "','" & FKSX.SuoDing & "') "
                    mdb.Write(sSQL)
                    'objKF.Dispose()
                Case Else
                    MsgBox("意外的控件类型")
            End Select
        Next

        SaveKuCun(iGNSID)
        SaveGongZi(iGNSID)
        SavePZ(iGNSID)
    End Sub

    '从数据库中读取功能设置
    Private Sub ShowGongNeng(ByVal iGongnengID As Integer)
        'Dim fkC As New FKG.myselfG
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select * from gongnengshu where ID=" & iGongnengID)
        Me.txtName.Text = ds.Tables(0).Rows(0).Item("Context")
        Me.txtTiShi.Text = ds.Tables(0).Rows(0).Item("tishi")

        ds.Clear()
        ds = mdb.Reader("select gongneng.* from Gongneng,Gongnengshu where gongneng.gnsID=gongnengshu.ID and gongnengshu.id=" & iGongnengID)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            sObjName = ds.Tables(0).Rows(i).Item(0)
            Select Case Me.gbSelect.Controls.Item(sObjName).GetType.Name
                Case "FKLable"
                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable)
                        .Text = ds.Tables(0).Rows(i).Item("zhi")
                        .Visible = ds.Tables(0).Rows(i).Item("QiYong")
                        ctrlP.X = ds.Tables(0).Rows(i).Item("X")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("Y")
                        .Location = ctrlP
                        ctrlP.X = ds.Tables(0).Rows(i).Item("W")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("H")
                        .Size = ctrlP
                        .ForeColor = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .Font = Me.StringToFont(ds.Tables(0).Rows(i).Item("ZiTi"))
                    End With

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKLable).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = "无效"
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = 20
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case "FKTextBox"
                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox)
                        .Text = ds.Tables(0).Rows(i).Item("zhi")
                        .Visible = ds.Tables(0).Rows(i).Item("QiYong")
                        ctrlP.X = ds.Tables(0).Rows(i).Item("X")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("Y")
                        .Location = ctrlP
                        ctrlP.X = ds.Tables(0).Rows(i).Item("W")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("H")
                        .ReadOnly = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Size = ctrlP
                        .ForeColor = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .Font = Me.StringToFont(ds.Tables(0).Rows(i).Item("ZiTi"))
                    End With

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKTextBox).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = ds.Tables(0).Rows(i).Item("GeShi")
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = ds.Tables(0).Rows(i).Item("TabIndex")
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case "FKComboBox"
                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox)
                        .Text = ds.Tables(0).Rows(i).Item("zhi")
                        .Visible = ds.Tables(0).Rows(i).Item("QiYong")
                        ctrlP.X = ds.Tables(0).Rows(i).Item("X")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("Y")
                        .Location = ctrlP
                        ctrlP.X = ds.Tables(0).Rows(i).Item("W")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("H")
                        '.ReadOnly = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Size = ctrlP
                        .ForeColor = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .Font = Me.StringToFont(ds.Tables(0).Rows(i).Item("ZiTi"))
                    End With

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKComboBox).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = "无效"
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = ds.Tables(0).Rows(i).Item("TabIndex")
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case "FKDateTimePicker"
                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker)
                        .Text = ds.Tables(0).Rows(i).Item("zhi")
                        .Visible = ds.Tables(0).Rows(i).Item("QiYong")
                        ctrlP.X = ds.Tables(0).Rows(i).Item("X")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("Y")
                        .Location = ctrlP
                        ctrlP.X = ds.Tables(0).Rows(i).Item("W")
                        ctrlP.Y = ds.Tables(0).Rows(i).Item("H")
                        '.ReadOnly = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Size = ctrlP
                        .ForeColor = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .Font = Me.StringToFont(ds.Tables(0).Rows(i).Item("ZiTi"))
                    End With

                    With CType(Me.gbSelect.Controls.Item(sObjName), Freedom.FKDateTimePicker).FKShuXing
                        .ID = ds.Tables(0).Rows(i).Item("KJID")
                        .QiYong = ds.Tables(0).Rows(i).Item("QiYong")
                        .Zhi = ds.Tables(0).Rows(i).Item("zhi")
                        .GeShi = "无效"
                        .X = ds.Tables(0).Rows(i).Item("X")
                        .Y = ds.Tables(0).Rows(i).Item("Y")
                        .W = ds.Tables(0).Rows(i).Item("W")
                        .H = ds.Tables(0).Rows(i).Item("H")
                        .ZhiDu = ds.Tables(0).Rows(i).Item("ZhiDu")
                        .Tab = ds.Tables(0).Rows(i).Item("TabIndex")
                        .ZiTi = ds.Tables(0).Rows(i).Item("ZITI")
                        .YanSe = System.Drawing.Color.FromArgb(ds.Tables(0).Rows(i).Item("Yanse"))
                        .BiTian = ds.Tables(0).Rows(i).Item("BiTian")
                        .BangDing = ds.Tables(0).Rows(i).Item("BangDing")
                        .SuoDing = ds.Tables(0).Rows(i).Item("Suoding")
                    End With
                Case Else
                    MsgBox("意外的控件类型")
            End Select
        Next

    End Sub

    'Private Sub FillJiBen()
    '    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asas)
    '    mdb.DataBind(Me.dgvJiben, "select * from GN_JiBen")
    'End Sub

    Private Sub FillKuCun()
        '只显示有用的文本类型的数据即可
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If iGongnengID = -1 Then

        Else
            Dim ds As DataSet = mdb.Reader("select * from GongNengShu where ID=" & iGongnengID)
            If ds.Tables(0).Rows(0).Item("KuCun") = True Then
                Me.dgvKuCun.Visible = True
                Me.ckbKucun.Checked = True
            Else
                Me.dgvKuCun.Visible = False
                Me.ckbKucun.Checked = False
            End If
            If ds.Tables(0).Rows(0).Item("GongZi") = True Then
                Me.dgvGongzi.Visible = True
                Me.ckbGongZi.Checked = True
            Else
                Me.dgvGongzi.Visible = False
                Me.ckbGongZi.Checked = False
            End If
            If ds.Tables(0).Rows(0).Item("PZ") = True Then
                Me.dgvPingZheng.Visible = True
                Me.ckbPingZheng.Checked = True
            Else
                Me.dgvPingZheng.Visible = False
                Me.ckbPingZheng.Checked = False
            End If

            '读取凭证生成方式
            Me.cbPZSCFS.SelectedIndex = ds.Tables(0).Rows(0).Item("PZSCFS")
            Me.txtZhangShu.Text = ds.Tables(0).Rows(0).Item("DJZhangShu")

        End If
        mdb.DataBind(Me.dgvKuCun, "select 显示,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20 from GN_KuCun,GongnengShu where GN_KuCun.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID)

        mdb.DataBind(Me.dgvGongzi, "select  显示,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20  from GN_GongZi,GongnengShu where GN_GongZi.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID)

        mdb.DataBind(Me.dgvPingZheng, "select  显示,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20  from GN_PZ,GongnengShu where GN_PZ.GNSID=GongNengShu.ID and GongNengShu.ID=" & iGongnengID)
    End Sub

    Private Sub SaveKuCun(ByVal iGNSID As Integer)

        Dim iHang As Integer
        Dim sSQL As String
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        Dim bXS As Boolean
       
        For iHang = 0 To Me.dgvKuCun.RowCount - 2
            If IsDBNull(Me.dgvKuCun.Rows(iHang).Cells(0).Value) OrElse Me.dgvKuCun.Rows(iHang).Cells(0).Value = False Then
                bXS = False
            Else
                bXS = True
            End If
            sSQL = "insert into GN_KuCun (GNSID,Hang,显示,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20) values (" & iGNSID & "," & iHang & ",'" & bXS & "'," & FKG.myselfG.FKString("BianHao") & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells.Item(2).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(3).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(4).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(5).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(6).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(7).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(8).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(9).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(10).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(11).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(12).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(13).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(14).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(15).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(16).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(17).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(18).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(19).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(20).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(21).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(22).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(23).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(24).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(25).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(26).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(27).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(28).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(29).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(30).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(31).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(32).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(33).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(34).Value.ToString) & FKG.myselfG.FKString(Me.dgvKuCun.Rows(iHang).Cells(35).Value.ToString, "") & " )"
            mdb.Write(sSQL)
        Next
    End Sub

    Private Sub SaveGongZi(ByVal iGNSID As Integer)
        Dim iHang As Integer
        Dim sSQL As String
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        Dim iXS As Boolean
        For iHang = 0 To Me.dgvGongzi.RowCount - 2
            If IsDBNull(Me.dgvGongzi.Rows(iHang).Cells(0).Value) OrElse Me.dgvGongzi.Rows(iHang).Cells(0).Value = False Then
                iXS = False
            Else
                iXS = True
            End If
            sSQL = "insert into GN_GongZi (GNSID,Hang,显示,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20) values (" & iGNSID & "," & iHang & ",'" & iXS & "'," & FKG.myselfG.FKString("BianHao") & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells.Item(2).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(3).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(4).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(5).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(6).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(7).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(8).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(9).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(10).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(11).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(12).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(13).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(14).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(15).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(16).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(17).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(18).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(19).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(20).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(21).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(22).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(23).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(24).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(25).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(26).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(27).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(28).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(29).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(30).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(31).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(32).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(33).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(34).Value.ToString) & FKG.myselfG.FKString(Me.dgvGongzi.Rows(iHang).Cells(35).Value.ToString, "") & " )"
            mdb.Write(sSQL)
        Next
    End Sub

    Private Sub SavePZ(ByVal iGNSID As Integer)
        Dim iHang As Integer
        Dim sSQL As String
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim iXS As Boolean
        For iHang = 0 To Me.dgvPingZheng.RowCount - 2
            If IsDBNull(Me.dgvPingZheng.Rows(iHang).Cells(0).Value) OrElse Me.dgvPingZheng.Rows(iHang).Cells(0).Value = False Then
                iXS = False
            Else
                iXS = True
            End If
            sSQL = "insert into GN_PZ (GNSID,Hang,显示,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,备用6,备用7,备用8,备用9,备用10,备用11,备用12,备用13,备用14,备用15,备用16,备用17,备用18,备用19,备用20) values (" & iGNSID & "," & iHang & ",'" & iXS & "'," & FKG.myselfG.FKString("BianHao") & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells.Item(2).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(3).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(4).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(5).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(6).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(7).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(8).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(9).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(10).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(11).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(12).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(13).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(14).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(15).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(16).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(17).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(18).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(19).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(20).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(21).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(22).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(23).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(24).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(25).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(26).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(27).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(28).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(29).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(30).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(31).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(32).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(33).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(34).Value.ToString) & FKG.myselfG.FKString(Me.dgvPingZheng.Rows(iHang).Cells(35).Value.ToString, "") & " )"
            mdb.Write(sSQL)
        Next
    End Sub


    Private Sub dgvKuCun_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvKuCun.CellValidated
        If Me.dgvKuCun.CurrentCell.ColumnIndex = 0 Then
            Exit Sub
        End If
        '验证公式的输入是否正确
        If Me.dgvKuCun.CurrentCell.Value.ToString = "" Then
            Exit Sub
        End If
        If Me.gbSelect.Controls.IndexOf(Me.gbSelect.Controls.Item(Me.dgvKuCun.CurrentCell.Value.ToString)) = -1 Then
            MsgBox("您输入的不是正确的参数ID，请重新输入")
            Me.dgvKuCun.CurrentCell.Value = ""
        Else

        End If
    End Sub

    Private Sub dgvGongzi_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGongzi.CellValidated
        If Me.dgvGongzi.CurrentCell.ColumnIndex = 0 Then
            Exit Sub
        End If
        '验证公式的输入是否正确
        If Me.dgvGongzi.CurrentCell.Value.ToString = "" Then
            Exit Sub
        End If
        If Me.gbSelect.Controls.IndexOf(Me.gbSelect.Controls.Item(Me.dgvGongzi.CurrentCell.Value.ToString)) = -1 Then
            MsgBox("您输入的不是正确的参数ID，请重新输入")
            Me.dgvGongzi.CurrentCell.Value = ""
        Else

        End If
    End Sub

    Private Sub dgvPZ_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPingZheng.CellValidated
        If Me.dgvPingZheng.CurrentCell.ColumnIndex = 0 Then
            Exit Sub
        End If
        '验证公式的输入是否正确
        If Me.dgvPingZheng.CurrentCell.Value.ToString.ToUpper = "A_ID" Then
            Exit Sub '当仓库使用“A_ID”时，可以区分出凭证生成方式，以用于明细账时不汇总同单同科目直接凭证
        End If
        If Me.dgvPingZheng.CurrentCell.Value.ToString = "" Then
            Exit Sub
        End If
        If Me.gbSelect.Controls.IndexOf(Me.gbSelect.Controls.Item(Me.dgvPingZheng.CurrentCell.Value.ToString)) = -1 Then
            MsgBox("您输入的不是正确的参数ID，请重新输入")
            Me.dgvPingZheng.CurrentCell.Value = ""
        Else

        End If
    End Sub

    Private Sub SaveXSBiao()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("update GongNengShu set KuCun='" & Me.ckbKucun.Checked & "',Gongzi='" & Me.ckbGongZi.Checked & "',PZ='" & Me.ckbPingZheng.Checked & "' where ID =" & iGongnengID)
    End Sub

    Private Sub txtName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.Validated
        If iGongnengID = -1 Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If mdb.Reader("select ID from GongNengshu where Context='" & Me.txtName.Text & "'").Tables(0).Rows.Count <> 0 Then
                MsgBox("您输入的功能名称已经使用，请重新输入！", MsgBoxStyle.Information, "提示")
                Me.txtName.Text = ""
                Me.ActiveControl = Me.txtName
                Exit Sub
            End If
        Else

        End If
    End Sub

    Private Sub 保存_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 保存.Click
        '判断有关库存的操作是否显示了仓库
        'If Me.dgvKuCun.RowCount > 1 AndAlso Me.CangKu.Visible = False Then
        '    MsgBox("在进行有关库存的操作时,请启用 CangKu 组件.该次保存没有成功!", MsgBoxStyle.Information, "提示")
        '    Exit Sub
        'End If

        If iGongnengID = -1 Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If mdb.Reader("select ID from GongNengshu where Context='" & Me.txtName.Text.Replace(" ", "") & "'").Tables(0).Rows.Count <> 0 Then
                MsgBox("您输入的功能名称已经使用，请重新输入！", MsgBoxStyle.Information, "提示")
                Me.txtName.Text = ""
                Me.ActiveControl = Me.txtName
                Exit Sub
            End If
        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If mdb.Reader("select A_ID from JiBenCaoZuo where A_GNSID=" & iGongnengID & " and 删除='false'").Tables(0).Rows.Count <> 0 Then
                If MsgBox("您想要修改的功能已经使用，强行修改将有可能产生意外错误，建议不修改原有功能而用另存为新建其他功能！如果确定要修改，请点击Yes", MsgBoxStyle.YesNoCancel, "警告") = MsgBoxResult.Yes Then

                Else
                    Me.txtName.Text = ""
                    Me.ActiveControl = Me.txtName
                    Exit Sub
                End If
            End If
        End If
        btnSave()

        '保存是否现实三个表
        SaveXSBiao()
    End Sub

    Private Sub 另存为_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 另存为.Click
        ''判断有关库存的操作是否显示了仓库
        'If Me.dgvKuCun.RowCount > 1 AndAlso Me.CangKu.Visible = False Then
        '    MsgBox("在进行有关库存的操作时,请启用 CangKu 组件.该次保存没有成功!", MsgBoxStyle.Information, "提示")
        '    Exit Sub
        'End If

        If iGongnengID = -1 Then

        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If mdb.Reader("select ID from GongNengshu where Context='" & Me.txtName.Text & "'").Tables(0).Rows.Count <> 0 Then
                MsgBox("您输入的功能名称已经使用，请重新输入！", MsgBoxStyle.Information, "提示")
                Me.txtName.Text = ""
                Me.ActiveControl = Me.txtName
                Exit Sub
            End If
        End If
        Dim iSourseGongNengID As Integer = iGongnengID

        iGongnengID = -1

        btnSave()

        '保存是否现实三个表
        SaveXSBiao()

        '保存显示和打印设置
        Dim mdb2 As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim sSourse(20) As String
        Dim ds As DataSet
        ds = mdb2.Reader("select * from Gongnengshu where ID=" & iSourseGongNengID)

        sSourse(0) = ds.Tables(0).Rows(0).Item("KuCunSQL").ToString
        sSourse(1) = ds.Tables(0).Rows(0).Item("KuCunHeji").ToString
        sSourse(2) = ds.Tables(0).Rows(0).Item("KuCunLK").ToString
        sSourse(3) = ds.Tables(0).Rows(0).Item("KuCunDot").ToString
        sSourse(4) = ds.Tables(0).Rows(0).Item("GongZiSQL").ToString
        sSourse(5) = ds.Tables(0).Rows(0).Item("GongZiHeJi").ToString
        sSourse(6) = ds.Tables(0).Rows(0).Item("GongZiLK").ToString
        sSourse(7) = ds.Tables(0).Rows(0).Item("GongZiDot").ToString
        sSourse(8) = ds.Tables(0).Rows(0).Item("PZSQL").ToString
        sSourse(9) = ds.Tables(0).Rows(0).Item("PZHeJi").ToString
        sSourse(10) = ds.Tables(0).Rows(0).Item("PZLK").ToString
        sSourse(11) = ds.Tables(0).Rows(0).Item("PZDot").ToString
        sSourse(12) = ds.Tables(0).Rows(0).Item("KuCunPath").ToString
        sSourse(13) = ds.Tables(0).Rows(0).Item("GongZiPath").ToString
        sSourse(14) = ds.Tables(0).Rows(0).Item("PZPath").ToString
        sSourse(15) = ds.Tables(0).Rows(0).Item("KuCunPrint").ToString
        sSourse(16) = ds.Tables(0).Rows(0).Item("GongZiPrint").ToString
        sSourse(17) = ds.Tables(0).Rows(0).Item("PZPrint").ToString

        mdb2.Write("update Gongnengshu set KuCunSQL ='" & sSourse(0) & "',KuCunHeJi='" & sSourse(1) & "' , KucunLK='" & sSourse(2) & "', KucunDot='" & sSourse(3) & "' , GongziSQL='" & sSourse(4) & "', GongZiHeJi='" & sSourse(5) & "', GongZiLK='" & sSourse(6) & "',GongZiDot='" & sSourse(7) & "', PZsql='" & sSourse(8) & "', PZheji='" & sSourse(9) & "',pzlk='" & sSourse(10) & "',pzDot='" & sSourse(11) & "',KucunPath='" & sSourse(12) & "', GongZiPath ='" & sSourse(13) & "',PZPath='" & sSourse(14) & "',KuCunPrint='" & sSourse(15) & "',GongZiPrint='" & sSourse(16) & "',PZPrint='" & sSourse(17) & "'  where ID =" & iGongnengID)

    End Sub

    Private Sub 退出_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 退出.Click
        If MsgBox("是否确定要退出功能设置？", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub ckbKucun_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbKucun.CheckedChanged
        Me.dgvKuCun.Visible = Me.ckbKucun.Checked
        Me.显示库存表.Checked = Me.ckbKucun.Checked
        DispSize()
    End Sub

    Private Sub ckbGongZi_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbGongZi.CheckedChanged
        Me.dgvGongzi.Visible = Me.ckbGongZi.Checked
        Me.显示工资表.Checked = Me.ckbGongZi.Checked
        DispSize()
    End Sub

    Private Sub ckbPingZheng_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbPingZheng.CheckedChanged
        Me.dgvPingZheng.Visible = Me.ckbPingZheng.Checked
        Me.显示凭证表.Checked = Me.ckbPingZheng.Checked
        DispSize()
    End Sub

    Private Sub 显示库存表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 显示库存表.Click
        Me.显示库存表.Checked = Not Me.显示库存表.Checked
        Me.ckbKucun.Checked = Me.显示库存表.Checked
    End Sub

    Private Sub 显示工资表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 显示工资表.Click
        Me.显示工资表.Checked = Not Me.显示工资表.Checked
        Me.ckbGongZi.Checked = Me.显示工资表.Checked
    End Sub

    Private Sub 显示凭证表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 显示凭证表.Click
        Me.显示凭证表.Checked = Not Me.显示凭证表.Checked
        Me.ckbPingZheng.Checked = Me.显示凭证表.Checked
    End Sub

    Private Sub 库存显示列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存显示列设置.Click
        Dim dlg As New FKLXS.FKLXS
        dlg.myTableName = "库存表"
        dlg.myGongNengID = iGongnengID
        dlg.ShowDialog()
    End Sub

    Private Sub 工资显示列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 工资显示列设置.Click
        Dim dlg As New FKLXS.FKLXS
        dlg.myTableName = "工资表"
        dlg.myGongNengID = iGongnengID
        dlg.ShowDialog()
    End Sub

    Private Sub 凭证显示列设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 凭证显示列设置.Click
        Dim dlg As New FKLXS.FKLXS
        dlg.myTableName = "凭证表"
        dlg.myGongNengID = iGongnengID
        dlg.ShowDialog()
    End Sub

    Private Sub 基本规则_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 基本规则.Click
        Dim dlg As New Help
        dlg.ShowDialog()
    End Sub

    Private Sub DispSize()
        Dim iXS As Integer = 0
        If Me.dgvKuCun.Visible = True Then
            iXS = iXS + 1
        End If
        If Me.dgvGongzi.Visible Then
            iXS = iXS + 1
        End If
        If Me.dgvPingZheng.Visible Then
            iXS = iXS + 1
        End If

        If Me.dgvKuCun.Visible = True Then
            Me.dgvKuCun.Height = (Me.TableLayoutPanel1.Height - 80) / iXS
        End If
        If Me.dgvGongzi.Visible Then
            Me.dgvGongzi.Height = (Me.TableLayoutPanel1.Height - 80) / iXS
        End If
        If Me.dgvPingZheng.Visible Then
            Me.dgvPingZheng.Height = (Me.TableLayoutPanel1.Height - 80) / iXS
        End If

    End Sub

    Private Sub FreedomCore_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        DispSize()
    End Sub

    Private Sub 库存打印设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存打印设置.Click
        Dim dlg As New FKLXS.FKPRT
        dlg.myTableName = "库存表"
        dlg.myGongNengID = iGongnengID
        dlg.ShowDialog()
    End Sub

    Private Sub 工资打印设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 工资打印设置.Click
        Dim dlg As New FKLXS.FKPRT
        dlg.myTableName = "工资表"
        dlg.myGongNengID = iGongnengID
        dlg.ShowDialog()
    End Sub

    Private Sub 凭证打印设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 凭证打印设置.Click
        Dim dlg As New FKLXS.FKPRT
        dlg.myTableName = "凭证表"
        dlg.myGongNengID = iGongnengID
        dlg.ShowDialog()
    End Sub

   
    Public Sub New(ByVal sHash As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()
        'System.Diagnostics.str()
        ' 在 InitializeComponent() 调用之后添加任何初始化。
        Dim ws As New wsbdsp.Service
        If ws.HelloWorld(getToken(MCO(CPUID, HID))) = "" Then
            Try

                Dim st As New System.Diagnostics.StackTrace
                Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(st.GetFrame(1).GetMethod.Name.ToString), sHash)
                    Case True
                    Case False
                        Me.DialogResult = Windows.Forms.DialogResult.No
                        Me.Close()
                End Select
            Catch ex As Exception
                Me.DialogResult = Windows.Forms.DialogResult.No
                Me.Dispose()
            End Try
        Else
        End If
    End Sub

    Private Function SignatureDeformatter(ByVal p_strKeyPublic As String, ByVal p_strHashbyteDeformatter As String, ByVal p_strDeformatterData As String) As Boolean
        Try
            Dim DeformatterData As Byte()
            Dim HashbyteDeformatter As Byte()

            HashbyteDeformatter = Convert.FromBase64String(p_strHashbyteDeformatter)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPublic)
            Dim RSADeformatter As New System.Security.Cryptography.RSAPKCS1SignatureDeformatter(RSA)
            '指定解密的时候HASH算法为MD5 
            RSADeformatter.SetHashAlgorithm("MD5")

            DeformatterData = Convert.FromBase64String(p_strDeformatterData)

            If RSADeformatter.VerifySignature(HashbyteDeformatter, DeformatterData) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
            'Throw ex
        End Try
    End Function

    Private Function GetHash(ByVal m_strSource As String) As String
        Try
            Dim strHashData As String
            '从字符串中取得Hash描述 
            Dim Buffer As Byte()
            Dim HashData As Byte()
            Dim MD5 As System.Security.Cryptography.HashAlgorithm = System.Security.Cryptography.HashAlgorithm.Create("MD5")
            Buffer = System.Text.Encoding.GetEncoding("GB2312").GetBytes(m_strSource)
            HashData = MD5.ComputeHash(Buffer)

            strHashData = Convert.ToBase64String(HashData)
            Return strHashData
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub 功能导出_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 功能导出.Click
        Dim dlg As New FKImport.FKImportFun
        If iGongnengID = -1 Then
        Else
            dlg.ImportOut(iGongnengID)
        End If

    End Sub

    Private Sub gbSelect_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles gbSelect.MouseMove
        Me.Label7.Text = "X=" & e.Location.X & ",Y=" & e.Location.Y
    End Sub

    Private Sub cbPZSCFS_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPZSCFS.SelectedIndexChanged
        If Me.cbPZSCFS.SelectedIndex = 0 Then
            Me.txtZhangShu.Visible = False
            Me.lblZhangShu.Visible = False
        Else
            Me.txtZhangShu.Visible = True
            Me.lblZhangShu.Visible = True
        End If
    End Sub

    Private Function getToken(ByVal sName As String) As String
        Dim sToken As String = ""
        Dim sT As String = ""
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If Asc(sName.Substring(i, 1)) > 122 Then

            Else
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            End If
        Next

        Dim sTR As String = "A"
        For i = 0 To sT.Length.ToString - 1
            If i Mod 7 = 0 Then
                sTR = sTR & sT.Substring(i, 1)
            End If
        Next

        sToken = sName & "," & sTR
        Return sToken
    End Function
End Class