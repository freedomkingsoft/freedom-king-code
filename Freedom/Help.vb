﻿Public NotInheritable Class Help

    Private Sub AboutBox1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.rtxtHelp.Text = getHelp()
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub

    Private Function getHelp() As String
        Dim sHelp As String
        sHelp = "规则１:" & vbCrLf
        sHelp = sHelp & "　　功能名称不能为空，不能重复。如需改名，则直接更改功能名称，然后保存即可。" & vbCrLf
        sHelp = sHelp & "规则２：" & vbCrLf
        sHelp = sHelp & "　　编号为必选项，并且各表中的编号必须一致，都应为""bianhao""。" & vbCrLf
        sHelp = sHelp & "规则３：" & vbCrLf
        sHelp = sHelp & "　　注意数据格式。数量、单价和金额均为数字类型，日期为日期类型，其他无要求。：" & vbCrLf
        Return sHelp
    End Function


End Class
