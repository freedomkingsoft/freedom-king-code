Public Class YongHu

    Private Sub YongHu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FilldgvYongHu()
        FilldgvGongNeng()
        Me.Icon = FKG.myselfG.FKIcon
        Me.ckbAdd.Checked = False
    End Sub

    Private Sub FilldgvYongHu()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select Yonghuid,yonghuname from yonghu where yonghujibie<(select YongHuJiBie from yongHu where YongHuID=" & FKG.myselfG.YongHuID & " ) or yonghuid=" & FKG.myselfG.YongHuID & "  order by yonghuname")
        'ds = mdb.Reader("select YongHuID,YongHuName from YongHu　where YongHuJiBie<(select YongHuJiBie from YongHu Where YongHuName='" & FKG.myselfG.YongHu & "') or YongHuID=" & FKG.myselfG.YongHuID & " order by YongHuName")
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.dgvYongHu.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1))
        Next
    End Sub

    Private Sub FilldgvQuanXian()

    End Sub

    '此处有问题，导致权限设置失败
    Private Sub FilldgvGongNeng()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        '根据用户级别来判断显示的功能和报表
        Dim iYongHuJiBie As Integer
        iYongHuJiBie = mdb.Reader("select YongHuJiBie from Yonghu where yonghuid=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0)

        Dim i As Integer
        If iYongHuJiBie >= 4 Then
            ds = mdb.Reader("select ID,Context from GongNengShu where isGongneng='true' order by ConText")
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Me.dgvGongNeng.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1))
            Next

            ds.Clear()
            ds = mdb.Reader("select ID,Context from ZDYBB where (isBaoBiao='true' or isMingXi='true')  order by ConText")
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Me.dgvBaoBiao.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1))
            Next
        Else
            ds = mdb.Reader("select ID,Context from GongNengShu where isGongneng='true' and (ID in (select YongHuQuanXian from YongHuQuanXian where YongHuID=" & FKG.myselfG.YongHuID & ") ) order by ConText")
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Me.dgvGongNeng.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1))
            Next

            ds.Clear()
            ds = mdb.Reader("select ID,Context from ZDYBB where (isBaoBiao='true' or isMingXi='true') and (ID in (select YongHuQuanXian from BaoBiaoQuanXian where YongHuID=" & FKG.myselfG.YongHuID & ")) order by ConText")
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Me.dgvBaoBiao.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1))
            Next
        End If

    End Sub

    Private Sub dgvYongHu_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvYongHu.CellEnter
        If Me.dgvYongHu.CurrentRow.Index = -1 Then
            Exit Sub
        Else
            Me.dgvQuanXian.Rows.Clear()
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            Dim ds As DataSet
            ds = mdb.Reader("select YongHuID,YongHuQuanXian,Context from YongHuQuanxian,Gongnengshu where YongHuQuanXian.YonghuQuanXian=GongnengShu.ID and YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value & " order by ConText")
            Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Me.dgvQuanXian.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1), ds.Tables(0).Rows(i).Item(2))
            Next


            Me.dgvBBQuanXian.Rows.Clear()
            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            ' Dim ds As DataSet
            ds.Clear()
            ds = mdb.Reader("select YongHuID,YongHuQuanXian,Context from BaoBiaoQuanxian,ZDYBB where BaoBiaoQuanxian.YonghuQuanXian=ZDYBB.ID and YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value & " order by ConText")
            'Dim i As Integer
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Me.dgvBBQuanXian.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1), ds.Tables(0).Rows(i).Item(2))
            Next

            Me.btnAddAll.Enabled = True
            Me.btnAddGongNeng.Enabled = True
            Me.btnDelAll.Enabled = True
            Me.btnDelGongNeng.Enabled = True

            Me.btnAddAllBB.Enabled = True
            Me.btnAddBaoBiao.Enabled = True
            Me.btnDelBaoBiao.Enabled = True
            Me.btnDelAllBB.Enabled = True

            Me.txtYongHuName.Text = Me.dgvYongHu.CurrentRow.Cells(1).Value
        End If
    End Sub

    Private Sub btnAddAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAll.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If Me.dgvQuanXian.RowCount = 0 Then
            Dim i As Integer
            For i = 0 To Me.dgvGongNeng.RowCount - 1
                mdb.Write("insert into YongHuQuanXian (YongHuID,YongHuQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & "," & Me.dgvGongNeng.Rows(i).Cells(0).Value & ")")
                Me.dgvQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvGongNeng.Rows(i).Cells(0).Value, Me.dgvGongNeng.Rows(i).Cells(1).Value)
            Next
        Else
            mdb.Write("delete from YongHuQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)
            Me.dgvQuanXian.Rows.Clear()
            Dim i As Integer
            For i = 0 To Me.dgvGongNeng.RowCount - 1
                mdb.Write("insert into YongHuQuanXian (YongHuID,YongHuQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & "," & Me.dgvGongNeng.Rows(i).Cells(0).Value & ")")
                Me.dgvQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvGongNeng.Rows(i).Cells(0).Value, Me.dgvGongNeng.Rows(i).Cells(1).Value)
            Next
        End If

    End Sub

    Private Sub btnAddGongNeng_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddGongNeng.Click
        '首先确定是否已有该功能
        Dim i As Integer
        For i = 0 To Me.dgvQuanXian.RowCount - 1
            If Me.dgvQuanXian.Rows(i).Cells(1).Value = Me.dgvGongNeng.CurrentRow.Cells(0).Value Then
                Exit For
            End If
        Next

        If i = Me.dgvQuanXian.RowCount Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            mdb.Write("insert into YongHuQuanXian (YongHuID,YongHuQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & "," & Me.dgvGongNeng.CurrentRow.Cells(0).Value & ")")

            Me.dgvQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvGongNeng.CurrentRow.Cells(0).Value, Me.dgvGongNeng.CurrentRow.Cells(1).Value)
        Else

        End If

    End Sub

    Private Sub btnDelGongNeng_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelGongNeng.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from YongHuQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value & " and YongHuQuanXian=" & Me.dgvQuanXian.CurrentRow.Cells(1).Value)

        Me.dgvQuanXian.Rows.Remove(Me.dgvQuanXian.CurrentRow)
    End Sub

    Private Sub btnDelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelAll.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from YongHuQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)

        Me.dgvQuanXian.Rows.Clear()
    End Sub

    Private Sub ckbAdd_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbAdd.CheckedChanged
        Me.txtYongHuName.Clear()

        'Me.GroupBox1.Enabled = Me.ckbAdd.Checked
    End Sub


    Private Sub btnAddYongHu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddYongHu.Click
        'If Me.txtYongHuName.Text.Replace(" ", "") = "" Or Me.txtYongHuMiMa.Text.Replace(" ", "") = "" Then
        If Me.txtYongHuName.Text.Replace(" ", "") = "" Then
            MsgBox("用户名不能为空", MsgBoxStyle.Information, "提示")
        Else
            Dim i As Integer
            For i = 0 To Me.dgvYongHu.RowCount - 1
                If Me.dgvYongHu.Rows(i).Cells(1).Value.ToString.ToUpper = Me.txtYongHuName.Text.ToUpper Then
                    Exit For
                End If
            Next

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            If i = Me.dgvYongHu.RowCount Then
                '新增用户
                If mdb.Reader("select * from yonghu where yonghuname='" & Me.txtYongHuName.Text & "'").Tables(0).Rows.Count > 0 Then
                    MsgBox("当前用户已存在，请使用其他名称！", MsgBoxStyle.Information, "提示")
                    Exit Sub
                End If

                mdb.Write("insert into YongHu (YongHuName,YongHuMiMa,YongHuJiBie) values ('" & Me.txtYongHuName.Text & "',pwdencrypt('888888')," & Me.nudJiBie.Value & ")")

                Dim iUserID As Integer
                iUserID = mdb.Reader("Select YongHuID from YongHu where YongHuName='" & Me.txtYongHuName.Text & "'").Tables(0).Rows(0).Item(0)

                mdb.Write("insert into DTZT (USERID,GNDT,BBDT,MCDT,GNZT,BBZT,MCZT,MMZT,MCLIE,MCHANG) select " & iUserID & " as  USERID,GNDT,BBDT,MCDT,GNZT,BBZT,MCZT,MMZT,MCLIE,MCHANG from DTZT where UserID=" & FKG.myselfG.YongHuID)
                mdb.Write("insert into MCBTN (UserID,btnName,btnText,iColumn,iRow,btnDT,btnZT,[btnAlign],[btnSize],[BtnType],[GNBBID],[GNBBName],[tab]) select  " & iUserID & " as  UserID,btnName,btnText,iColumn,iRow,btnDT,btnZT,[btnAlign],[btnSize],[BtnType],[GNBBID],[GNBBName],[tab] from MCBTN where UserID=" & FKG.myselfG.YongHuID) '复制用户界面设置

                mdb.Write("INSERT INTO [YongHuQuanXian] ([YongHuID],[YongHuQuanXian]) select " & iUserID & ",[YongHuQuanXian] from [YongHuQuanXian] where  [YongHuID]=" & FKG.myselfG.YongHuID) '复制用户权限
                mdb.Write("INSERT INTO [BaoBiaoQuanXian] ([YongHuID],[YongHuQuanXian]) select " & iUserID & ",[YongHuQuanXian] from [BaoBiaoQuanXian] where  [YongHuID]=" & FKG.myselfG.YongHuID) '复制用户报表权限
                mdb.Write("insert into YongHuCDQX(yonghuid,CDQuanXian) select  " & iUserID & ",cdname from yonghu,cd where yonghuid=" & FKG.myselfG.YongHuID)   '复制用户菜单权限
                MsgBox("   用户增加成功，默认密码为 888888 ", MsgBoxStyle.Information, "用户增加成功")

                Me.dgvYongHu.Rows.Clear()
                FilldgvYongHu()
            Else
                If MsgBox("当前用户已经存在,请检查用户名。", MsgBoxStyle.YesNo, "提示") = MsgBoxResult.Yes Then
                    '修改密码
                    'mdb.Write("update  YongHu set YongHuJiBie=" & Me.nudJiBie.Value & " where YongHuName='" & Me.txtYongHuName.Text & "'")
                End If
            End If
        End If
    End Sub

    
    Private Sub btnDelYongHu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelYongHu.Click
        If Me.dgvYongHu.CurrentRow.Index = -1 Then
        Else
            If MsgBox("您确定要删除 " & Me.dgvYongHu.CurrentRow.Cells(1).Value & " 这个用户吗，请确认？", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
                mdb.Write("delete from YongHuQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)
                mdb.Write("delete from BaoBiaoQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)
                mdb.Write("delete from YongHu where YongHuName='" & Me.dgvYongHu.CurrentRow.Cells(1).Value & "'")

                mdb.Write("delete from DTZT where UserID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)
                mdb.Write("delete from mcbtn where UserID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)

                mdb.Write("delete from yonghucdqx where yonghuid=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)

                Me.dgvYongHu.Rows.Remove(Me.dgvYongHu.CurrentRow)
            End If
        End If

        If Me.dgvYongHu.RowCount = 0 Then
            Me.btnAddAll.Enabled = False
            Me.btnAddGongNeng.Enabled = False
            Me.btnDelAll.Enabled = False
            Me.btnDelGongNeng.Enabled = False

            Me.btnAddAllBB.Enabled = False
            Me.btnAddBaoBiao.Enabled = False
            Me.btnDelBaoBiao.Enabled = False
            Me.btnDelAllBB.Enabled = False
        End If
    End Sub

    Private Sub btnAddAllBB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAllBB.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If Me.dgvBBQuanXian.RowCount = 0 Then
            Dim i As Integer
            For i = 0 To Me.dgvBaoBiao.RowCount - 1
                mdb.Write("insert into BaoBiaoQuanXian (YongHuID,YongHuQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & "," & Me.dgvBaoBiao.Rows(i).Cells(0).Value & ")")
                Me.dgvBBQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvBaoBiao.Rows(i).Cells(0).Value, Me.dgvBaoBiao.Rows(i).Cells(1).Value)
            Next
        Else
            mdb.Write("delete from baobiaoQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)
            Me.dgvBBQuanXian.Rows.Clear()
            Dim i As Integer
            For i = 0 To Me.dgvBaoBiao.RowCount - 1
                mdb.Write("insert into BaoBiaoQuanXian (YongHuID,YongHuQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & "," & Me.dgvBaoBiao.Rows(i).Cells(0).Value & ")")
                Me.dgvBBQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvBaoBiao.Rows(i).Cells(0).Value, Me.dgvBaoBiao.Rows(i).Cells(1).Value)
            Next
        End If

    End Sub

    Private Sub btnAddBaobiao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBaoBiao.Click
        '首先确定是否已有该功能
        Dim i As Integer
        For i = 0 To Me.dgvBBQuanXian.RowCount - 1
            If Me.dgvBBQuanXian.Rows(i).Cells(1).Value = Me.dgvBaoBiao.CurrentRow.Cells(0).Value Then
                Exit For
            End If
        Next

        If i = Me.dgvBBQuanXian.RowCount Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            mdb.Write("insert into BaoBiaoQuanXian (YongHuID,YongHuQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & "," & Me.dgvBaoBiao.CurrentRow.Cells(0).Value & ")")

            Me.dgvBBQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvBaoBiao.CurrentRow.Cells(0).Value, Me.dgvBaoBiao.CurrentRow.Cells(1).Value)
        Else

        End If

    End Sub

    Private Sub btnDelBaoBiao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelBaoBiao.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from BaoBiaoQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value & " and YongHuQuanXian=" & Me.dgvBBQuanXian.CurrentRow.Cells(1).Value)

        Me.dgvBBQuanXian.Rows.Remove(Me.dgvBBQuanXian.CurrentRow)
    End Sub

    Private Sub btnDelAllBB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelAllBB.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from BaoBiaoQuanXian where YongHuID=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)

        Me.dgvBBQuanXian.Rows.Clear()
    End Sub

    Private Sub btnCDQX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCDQX.Click
        Dim dlg As New CaiDanQuanXian
        dlg.ShowDialog()
    End Sub

    Private Sub btnPSW_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSW.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        '修改密码
        Dim sPSW As String
        sPSW = (Int(Rnd() * 1000) + 9999).ToString
        mdb.Write("update  YongHu set YongHuMiMa=pwdencrypt('" & sPSW & "') where YongHuName='" & Me.txtYongHuName.Text & "'")
        MsgBox("用户 " & Me.txtYongHuName.Text & " 的密码初始化为 " & sPSW & "，请登录系统后更改！")
    End Sub

    Private Sub btnXiuGaiJiBie_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnXiuGaiJiBie.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        '修改用户级别
        Dim sPSW As String
        sPSW = Me.nudJiBie.Value
        mdb.Write("update  YongHu set YongHuJiBie=" & sPSW & " where YongHuName='" & Me.txtYongHuName.Text & "'")
        MsgBox("用户级别修改成功，重新登录系统后生效！")
    End Sub

    Private Sub bntRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntRename.Click
        If Me.txtYongHuName.Text.Replace(" ", "") = "" OrElse Me.txtNewName.Text.Replace(" ", "") = "" OrElse Me.txtNewName.Text.Replace(" ", "") = "新用户名" Then
            MsgBox("新旧用户名不能为空", MsgBoxStyle.Information, "提示")
        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            If mdb.bExsit("select YonghuName from yonghu where yonghuname='" & Me.txtNewName.Text.Replace(" ", "") & "'") Then
                MsgBox("新用户名已经存在，请重新输入新用户名", MsgBoxStyle.Information, "提示")
            Else
                If mdb.Write("update YongHu set YongHuName='" & Me.txtNewName.Text.Replace(" ", "") & "' where YongHuName='" & Me.txtYongHuName.Text.Replace(" ", "") & "'") Then
                    MsgBox("用户名修改成功", MsgBoxStyle.Information, "提示")
                    YongHu_Load(sender, e)
                End If
            End If
        End If
    End Sub
End Class