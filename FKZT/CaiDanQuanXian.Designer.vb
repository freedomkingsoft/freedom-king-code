﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CaiDanQuanXian
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvYongHu = New System.Windows.Forms.DataGridView
        Me.YongHuID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.用户名 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnDelGongNeng = New System.Windows.Forms.Button
        Me.btnDelAll = New System.Windows.Forms.Button
        Me.btnAddAll = New System.Windows.Forms.Button
        Me.btnAddGongNeng = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dgvGongNeng = New System.Windows.Forms.DataGridView
        Me.GongNengID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.菜单名称 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvQuanXian = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GNID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.可用菜单名称 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label4 = New System.Windows.Forms.Label
        CType(Me.dgvYongHu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGongNeng, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvQuanXian, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvYongHu
        '
        Me.dgvYongHu.AllowUserToAddRows = False
        Me.dgvYongHu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvYongHu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.YongHuID, Me.用户名})
        Me.dgvYongHu.Location = New System.Drawing.Point(12, 50)
        Me.dgvYongHu.Name = "dgvYongHu"
        Me.dgvYongHu.ReadOnly = True
        Me.dgvYongHu.RowHeadersVisible = False
        Me.dgvYongHu.RowTemplate.Height = 23
        Me.dgvYongHu.Size = New System.Drawing.Size(139, 278)
        Me.dgvYongHu.TabIndex = 1
        '
        'YongHuID
        '
        Me.YongHuID.HeaderText = "YongHuID"
        Me.YongHuID.Name = "YongHuID"
        Me.YongHuID.ReadOnly = True
        Me.YongHuID.Visible = False
        '
        '用户名
        '
        Me.用户名.HeaderText = "用户名"
        Me.用户名.Name = "用户名"
        Me.用户名.ReadOnly = True
        Me.用户名.Width = 115
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(49, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "用户列表"
        '
        'btnDelGongNeng
        '
        Me.btnDelGongNeng.Enabled = False
        Me.btnDelGongNeng.Location = New System.Drawing.Point(343, 210)
        Me.btnDelGongNeng.Name = "btnDelGongNeng"
        Me.btnDelGongNeng.Size = New System.Drawing.Size(43, 25)
        Me.btnDelGongNeng.TabIndex = 11
        Me.btnDelGongNeng.Text = "-->"
        Me.btnDelGongNeng.UseVisualStyleBackColor = True
        '
        'btnDelAll
        '
        Me.btnDelAll.Enabled = False
        Me.btnDelAll.Location = New System.Drawing.Point(343, 264)
        Me.btnDelAll.Name = "btnDelAll"
        Me.btnDelAll.Size = New System.Drawing.Size(43, 25)
        Me.btnDelAll.TabIndex = 12
        Me.btnDelAll.Text = "|->"
        Me.btnDelAll.UseVisualStyleBackColor = True
        '
        'btnAddAll
        '
        Me.btnAddAll.Enabled = False
        Me.btnAddAll.Location = New System.Drawing.Point(343, 102)
        Me.btnAddAll.Name = "btnAddAll"
        Me.btnAddAll.Size = New System.Drawing.Size(43, 25)
        Me.btnAddAll.TabIndex = 13
        Me.btnAddAll.Text = "<-|"
        Me.btnAddAll.UseVisualStyleBackColor = True
        '
        'btnAddGongNeng
        '
        Me.btnAddGongNeng.Enabled = False
        Me.btnAddGongNeng.Location = New System.Drawing.Point(343, 156)
        Me.btnAddGongNeng.Name = "btnAddGongNeng"
        Me.btnAddGongNeng.Size = New System.Drawing.Size(43, 25)
        Me.btnAddGongNeng.TabIndex = 14
        Me.btnAddGongNeng.Text = "<--"
        Me.btnAddGongNeng.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.Location = New System.Drawing.Point(425, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "菜单列表"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(229, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "用户权限"
        '
        'dgvGongNeng
        '
        Me.dgvGongNeng.AllowUserToAddRows = False
        Me.dgvGongNeng.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGongNeng.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GongNengID, Me.菜单名称})
        Me.dgvGongNeng.Location = New System.Drawing.Point(392, 50)
        Me.dgvGongNeng.Name = "dgvGongNeng"
        Me.dgvGongNeng.ReadOnly = True
        Me.dgvGongNeng.RowHeadersVisible = False
        Me.dgvGongNeng.RowTemplate.Height = 23
        Me.dgvGongNeng.Size = New System.Drawing.Size(139, 278)
        Me.dgvGongNeng.TabIndex = 7
        '
        'GongNengID
        '
        Me.GongNengID.HeaderText = "GongNengID"
        Me.GongNengID.Name = "GongNengID"
        Me.GongNengID.ReadOnly = True
        Me.GongNengID.Visible = False
        '
        '菜单名称
        '
        Me.菜单名称.HeaderText = "菜单名称"
        Me.菜单名称.Name = "菜单名称"
        Me.菜单名称.ReadOnly = True
        Me.菜单名称.Width = 115
        '
        'dgvQuanXian
        '
        Me.dgvQuanXian.AllowUserToAddRows = False
        Me.dgvQuanXian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvQuanXian.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.GNID, Me.可用菜单名称})
        Me.dgvQuanXian.Location = New System.Drawing.Point(198, 50)
        Me.dgvQuanXian.Name = "dgvQuanXian"
        Me.dgvQuanXian.ReadOnly = True
        Me.dgvQuanXian.RowHeadersVisible = False
        Me.dgvQuanXian.RowTemplate.Height = 23
        Me.dgvQuanXian.Size = New System.Drawing.Size(139, 278)
        Me.dgvQuanXian.TabIndex = 8
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "YongHuID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'GNID
        '
        Me.GNID.HeaderText = "GNID"
        Me.GNID.Name = "GNID"
        Me.GNID.ReadOnly = True
        Me.GNID.Visible = False
        '
        '可用菜单名称
        '
        Me.可用菜单名称.HeaderText = "可用菜单名称"
        Me.可用菜单名称.Name = "可用菜单名称"
        Me.可用菜单名称.ReadOnly = True
        Me.可用菜单名称.Width = 115
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(199, 356)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(332, 16)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "权限改变后自动保存,重新启动系统后生效."
        '
        'CaiDanQuanXian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(556, 408)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnDelGongNeng)
        Me.Controls.Add(Me.btnDelAll)
        Me.Controls.Add(Me.btnAddAll)
        Me.Controls.Add(Me.btnAddGongNeng)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvGongNeng)
        Me.Controls.Add(Me.dgvQuanXian)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvYongHu)
        Me.Name = "CaiDanQuanXian"
        Me.Text = "用户菜单权限设置"
        CType(Me.dgvYongHu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGongNeng, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvQuanXian, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvYongHu As System.Windows.Forms.DataGridView
    Friend WithEvents YongHuID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 用户名 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnDelGongNeng As System.Windows.Forms.Button
    Friend WithEvents btnDelAll As System.Windows.Forms.Button
    Friend WithEvents btnAddAll As System.Windows.Forms.Button
    Friend WithEvents btnAddGongNeng As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvGongNeng As System.Windows.Forms.DataGridView
    Friend WithEvents GongNengID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 菜单名称 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvQuanXian As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GNID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 可用菜单名称 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
