Public Class CaiDanQuanXian

    Private Sub CaiDanQuanXian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FilldgvYongHu()
        FilldgvGongNeng()
        Me.Icon = FKG.myselfG.FKIcon
    End Sub

    Private Sub FilldgvYongHu()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select Yonghuid,yonghuname from yonghu where yonghujibie<(select YongHuJiBie from yongHu where YongHuID=" & FKG.myselfG.YongHuID & " ) or yonghuid=" & FKG.myselfG.YongHuID & "  order by yonghuname")

        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.dgvYongHu.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1))
        Next
    End Sub


    Private Sub FilldgvGongNeng()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select main,cdname from cd where ismain='false' and jibie <=(select YongHuJiBie from Yonghu where yonghuid=" & FKG.myselfG.YongHuID & ")")

        'ds = mdb.Reader("select ID,Context from GongNengShu where isGongneng='true' and (ID in (select YongHuQuanXian from YongHuQuanXian where YongHuID=" & FKG.myselfG.YongHuID & ") or  (select YongHuJiBie from Yonghu where yonghuid=" & FKG.myselfG.YongHuID & ")=5) order by ConText")
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.dgvGongNeng.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1))
        Next



    End Sub

    Private Sub dgvYongHu_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvYongHu.CellContentClick

    End Sub

    Private Sub dgvYongHu_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvYongHu.CellEnter
        If Me.dgvYongHu.CurrentRow.Index = -1 Then
            Exit Sub
        End If

        Dim MDB As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim ds As DataSet
        ds = MDB.Reader("Select * from yonghucdqx where Yonghuid=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)

        Me.dgvQuanXian.Rows.Clear()

        Dim I As Integer
        For I = 0 To ds.Tables(0).Rows.Count - 1
            Me.dgvQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvYongHu.CurrentRow.Cells(0).Value, ds.Tables(0).Rows(I).Item("CDQuanXian"))
        Next

        Me.btnAddAll.Enabled = True
        Me.btnAddGongNeng.Enabled = True
        Me.btnDelAll.Enabled = True
        Me.btnDelGongNeng.Enabled = True


    End Sub

    Private Sub btnAddAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAll.Click
        If Me.dgvGongNeng.Rows.Count > 0 Then
            Me.dgvQuanXian.Rows.Clear()

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            mdb.Write("delete from yonghucdqx where  yonghuid=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)

            Dim i As Integer
            For i = 0 To Me.dgvGongNeng.Rows.Count - 1
                Me.dgvQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvGongNeng.Rows(i).Cells(0).Value, Me.dgvGongNeng.Rows(i).Cells(1).Value)
                mdb.Write("insert into yonghucdqx (YongHuID,CDQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & ",'" & Me.dgvGongNeng.Rows(i).Cells(1).Value & "')")
            Next

        End If
    End Sub

    Private Sub btnAddGongNeng_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddGongNeng.Click
        If Me.dgvGongNeng.CurrentRow.Index = -1 Then
            Exit Sub
        End If
        '确定是否已经具有相应权限

        Dim sCD As String = Me.dgvGongNeng.CurrentRow.Cells(1).Value

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.bExsit("select * from yonghucdqx where yonghuid=" & Me.dgvYongHu.CurrentRow.Cells(0).Value & " and CDQuanXian='" & Me.dgvGongNeng.CurrentRow.Cells(1).Value & "'") Then
            '已经存在,不用再增加
        Else
            Me.dgvQuanXian.Rows.Add(Me.dgvYongHu.CurrentRow.Cells(0).Value, Me.dgvGongNeng.CurrentRow.Cells(0).Value, Me.dgvGongNeng.CurrentRow.Cells(1).Value)
            mdb.Write("insert into yonghucdqx (YongHuID,CDQuanXian) values (" & Me.dgvYongHu.CurrentRow.Cells(0).Value & ",'" & Me.dgvGongNeng.CurrentRow.Cells(1).Value & "')")
        End If

    End Sub

    Private Sub btnDelGongNeng_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelGongNeng.Click
        If Me.dgvQuanXian.CurrentRow.Index = -1 Then
            Exit Sub
        End If


        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from yonghucdqx where  yonghuid=" & Me.dgvYongHu.CurrentRow.Cells(0).Value & " and CDQuanXian='" & Me.dgvQuanXian.CurrentRow.Cells(2).Value & "'")

        Me.dgvQuanXian.Rows.Remove(Me.dgvQuanXian.CurrentRow)


    End Sub

    Private Sub btnDelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelAll.Click
        If Me.dgvQuanXian.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from yonghucdqx where  yonghuid=" & Me.dgvYongHu.CurrentRow.Cells(0).Value)

        Me.dgvQuanXian.Rows.Clear()

    End Sub
End Class