﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CangKuInfo
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgQY = New System.Windows.Forms.DataGridView
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnDelete = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtShuoming = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbADD = New System.Windows.Forms.CheckBox
        Me.cbEdit = New System.Windows.Forms.CheckBox
        CType(Me.dgQY, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgQY
        '
        Me.dgQY.AllowUserToAddRows = False
        Me.dgQY.AllowUserToDeleteRows = False
        Me.dgQY.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgQY.Location = New System.Drawing.Point(14, 100)
        Me.dgQY.Name = "dgQY"
        Me.dgQY.ReadOnly = True
        Me.dgQY.RowHeadersVisible = False
        Me.dgQY.RowHeadersWidth = 85
        Me.dgQY.RowTemplate.Height = 23
        Me.dgQY.Size = New System.Drawing.Size(394, 202)
        Me.dgQY.TabIndex = 1
        Me.dgQY.TabStop = False
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(160, 459)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(88, 32)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "保   存 (&S)"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(299, 459)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(109, 32)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "退　　　出"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "已设置仓库列表"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(111, 308)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(196, 32)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "删除选中的仓库"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtShuoming)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 379)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(383, 60)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'txtShuoming
        '
        Me.txtShuoming.Location = New System.Drawing.Point(218, 20)
        Me.txtShuoming.Name = "txtShuoming"
        Me.txtShuoming.Size = New System.Drawing.Size(150, 21)
        Me.txtShuoming.TabIndex = 2
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(67, 20)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(101, 21)
        Me.txtName.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(183, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "说明"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "仓库名称"
        '
        'cbADD
        '
        Me.cbADD.AutoSize = True
        Me.cbADD.Location = New System.Drawing.Point(29, 357)
        Me.cbADD.Name = "cbADD"
        Me.cbADD.Size = New System.Drawing.Size(72, 16)
        Me.cbADD.TabIndex = 3
        Me.cbADD.Text = "添加仓库"
        Me.cbADD.UseVisualStyleBackColor = True
        '
        'cbEdit
        '
        Me.cbEdit.AutoSize = True
        Me.cbEdit.Checked = True
        Me.cbEdit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbEdit.Location = New System.Drawing.Point(134, 357)
        Me.cbEdit.Name = "cbEdit"
        Me.cbEdit.Size = New System.Drawing.Size(72, 16)
        Me.cbEdit.TabIndex = 3
        Me.cbEdit.Text = "修改仓库"
        Me.cbEdit.UseVisualStyleBackColor = True
        '
        'CangKuInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 514)
        Me.Controls.Add(Me.cbEdit)
        Me.Controls.Add(Me.cbADD)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.dgQY)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CangKuInfo"
        Me.Text = "ZTInfo"
        CType(Me.dgQY, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgQY As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cbADD As System.Windows.Forms.CheckBox
    Friend WithEvents cbEdit As System.Windows.Forms.CheckBox
    Friend WithEvents txtShuoming As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
