﻿Public Class ZTInfo

    Dim bChanged As Boolean
    Dim sName As String

    Private Sub ZTInfo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FillDG()
        Me.Icon = FKG.myselfG.FKIcon
    End Sub

    Private Sub FillDG()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgQY, "select shuxing as 企业信息名称,zhi as 企业信息值 from ZTSX")
        'Me.txtQiYeName.Text = mdb.Reader("select QiYeMing from ZT ").Tables(0).Rows(0).Item(0).ToString

        Dim ds As DataSet
        ds = mdb.Reader("select qiyeming,qiyongriqi,bianhaoweishu,kemuweishu,nianfenweishu,KeMuWeiShu from ZT ")
        Me.qiyongriqi.Text = ds.Tables(0).Rows(0).Item("qiyongriqi")
        Me.zhangtaomingcheng.Text = ds.Tables(0).Rows(0).Item("qiyeming")
        Me.bianhaoweishu.Text = ds.Tables(0).Rows(0).Item("bianhaoweishu")

        Me.nianfen.Checked = ds.Tables(0).Rows(0).Item("nianfenweishu")
        Me.kemujishu.Text = ds.Tables(0).Rows(0).Item("KeMuWeiShu")

        Me.dgQY.Columns(0).Width = 120
        Me.dgQY.Columns(1).Width = 270
        Me.ActiveControl = Nothing
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        If bChanged Then
            If MsgBox("当前数据已经被修改，确定要放弃修改吗？", MsgBoxStyle.YesNoCancel) = MsgBoxResult.Yes Then
                Me.Close()
            End If
        Else
            Me.Close()
        End If
        'Me.Close()
    End Sub

    Private Sub dgQY_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgQY.CellValueChanged
        bChanged = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        '保存企业资料
        mdb.Write("update zt set qiyeming='" & Me.zhangtaomingcheng.Text & "',qiyongriqi='" & Me.qiyongriqi.Text & "',bianhaoweishu=" & Me.bianhaoweishu.Text & ",nianfenweishu='" & Me.nianfen.Checked & "',KeMuWeiShu=" & Me.kemujishu.Text)

        If bChanged = False Then

        Else

            '第一步，清除原有数据
            mdb.Write("delete from ZTSX")

            '第二步，写入修改后的数据
            Dim i As Integer
            For i = 0 To Me.dgQY.Rows.Count - 2
                If Me.dgQY.Rows(i).Cells(0).Value.ToString.Replace(" ", "") = "" OrElse Me.dgQY.Rows(i).Cells(1).Value.ToString.Replace(" ", "") = "" Then
                    Exit For
                Else
                    mdb.Write("insert into ZTSX (shuxing,zhi) values ('" & Me.dgQY.Rows(i).Cells(0).Value & "','" & Me.dgQY.Rows(i).Cells(1).Value & "')")
                End If
            Next

            bChanged = False
        End If

        MsgBox("企业资料修改完成", MsgBoxStyle.Information, "提示")
    End Sub

    'Private Sub txtQiYeName_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    '    If Me.txtQiYeName.Text = sName Then
    '    Else
    '        If MsgBox("您确实要更改企业名称吗?", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then
    '            If Me.txtQiYeName.Text.Replace(" ", "") = "" Then

    '            Else
    '                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
    '                mdb.Write("update ZT set QiYeMing='" & Me.txtQiYeName.Text.Replace(" ", "") & "'")
    '            End If
    '        End If
    '    End If
    'End Sub

   
End Class