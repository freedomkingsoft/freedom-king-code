﻿Public Class CangKuInfo


    Private Sub CangKuInfo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        FillDG()
    End Sub

    Private Sub FillDG()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgQY, "select Name as 仓库名称,Shuoming as 说明,ID as 仓库编号 from CangKuInfo")
        'Me.txtQiYeName.Text = mdb.Reader("select QiYeMing from ZT ").Tables(0).Rows(0).Item(0).ToString
        'sName = Me.txtQiYeName.Text

        Me.dgQY.Columns(0).Width = 120
        Me.dgQY.Columns(1).Width = 270
        Me.ActiveControl = Nothing
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If Me.cbADD.Checked Then
            If mdb.bExsit("select ID from cangkuinfo where name='" & Me.txtName.Text & "'") Then
                MsgBox("该仓库已经存在，不能设置重名仓库！", MsgBoxStyle.Information, "添加未成功")
            Else
                mdb.Write("insert into CangKuInfo (Name,ShuoMing) values ('" & Me.txtName.Text & "','" & Me.txtShuoming.Text & "')")
                mdb.DataBind(Me.dgQY, "select Name as 仓库名称,Shuoming as 说明 from CangKuInfo")
                Me.txtName.Clear()
                Me.txtShuoming.Clear()
            End If
        End If

        If Me.cbEdit.Checked Then
            If mdb.bExsit("select ID from cangkuinfo where name='" & Me.txtName.Text & "'") And Me.txtName.Text.ToUpper.ToString <> Me.dgQY.CurrentRow.Cells(0).Value.ToString.ToUpper Then
                MsgBox("该仓库已经存在，不能设置重名仓库！", MsgBoxStyle.Information, "修改未成功")
            Else
                If MsgBox("警告：修改仓库名称将会同步修改之前的数据，建议正式启用之后不要修改仓库名称。无修改后出现问题，请及时联系售后服务人员。", MsgBoxStyle.OkCancel, "警告") = MsgBoxResult.Ok Then
                    '2020-09-21 更改仓库名称和说明
                    '需要同步更改功能和报表中用到的仓库名称
                    Dim sOldName, sNewName As String
                    sOldName = Me.dgQY.CurrentRow.Cells(0).Value
                    sNewName = Me.txtName.Text

                    mdb.Write("update CangkuInfo set name='" & sNewName & "' , Shuoming='" & Me.txtShuoming.Text & "' where name='" & sOldName & "'")
                    mdb.Write("update GongNeng  set zhi= REPLACE(zhi,'" & sOldName & "','" & sNewName & "') where KJID='cangku'")
                    mdb.Write("update KuCun set 备用15=REPLACE(备用15,'" & sOldName & "','" & sNewName & "')")
                    mdb.Write("update gongzi set 备用6=REPLACE(备用6,'" & sOldName & "','" & sNewName & "'),备用8=REPLACE(备用8,'" & sOldName & "','" & sNewName & "')")
                    mdb.Write("update JiBenCaoZuo set beiyong13=REPLACE(beiyong13,'" & sOldName & "','" & sNewName & "'),beiyong15=REPLACE(beiyong15,'" & sOldName & "','" & sNewName & "')")
                    mdb.Write("update KMFZSX set 辅助属性值=REPLACE(辅助属性值,'" & sOldName & "','" & sNewName & "')")
                    mdb.Write("update ZDYBB set ConText =REPLACE(conText,'" & sOldName & "','" & sNewName & "'),TiShi=REPLACE(TiShi,'" & sOldName & "','" & sNewName & "')")
                    mdb.Write("update zdyyunbb set ConText =REPLACE(conText,'" & sOldName & "','" & sNewName & "'),TiShi=REPLACE(TiShi,'" & sOldName & "','" & sNewName & "')")
                    mdb.Write("update GongNengShu set TiShi=REPLACE(tishi,'" & sOldName & "','" & sNewName & "')")

                    mdb.DataBind(Me.dgQY, "select Name as 仓库名称,Shuoming as 说明 from CangKuInfo")
                End If

            End If

        End If
       
    End Sub


    Private Sub dgQY_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgQY.CurrentCellChanged
        If IsNothing(Me.dgQY.CurrentRow) = False Then
            Me.txtName.Text = Me.dgQY.CurrentRow.Cells(0).Value.ToString
            Me.txtShuoming.Text = Me.dgQY.CurrentRow.Cells(1).Value.ToString
        End If
    End Sub

  
    Private Sub cbADD_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbADD.CheckedChanged
        Me.cbEdit.Checked = Not Me.cbADD.Checked
        Me.txtName.Clear()
        Me.txtShuoming.Clear()
    End Sub

    Private Sub cbEdit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbEdit.CheckedChanged
        Me.cbADD.Checked = Not Me.cbEdit.Checked
        If IsNothing(Me.dgQY.CurrentRow) = False Then
            Me.txtName.Text = Me.dgQY.CurrentRow.Cells(0).Value.ToString
            Me.txtShuoming.Text = Me.dgQY.CurrentRow.Cells(1).Value.ToString
        End If
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If IsNothing(Me.dgQY.CurrentRow) Then

        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            '先判断是否已使用
            If mdb.bExsit("select A_ID from JiBenCaoZuo,CangKuInfo where Jibencaozuo.cangku=cangkuinfo.ID and cangkuinfo.name='" & Me.dgQY.CurrentRow.Cells(0).Value & "' AND 删除='false'") Then
                MsgBox("你选中的仓库已经使用，无法删除！", MsgBoxStyle.Information, "提示")
            Else
                If MsgBox("你确定要删除仓库：" & Me.dgQY.CurrentRow.Cells(0).Value & "吗？如果有余额将一并删除！", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then
                    mdb.Write("delete from KCYE where CangKuID=(select ID from CangKuInfo where Name='" & Me.dgQY.CurrentRow.Cells(0).Value & "')")
                    mdb.Write("delete from CangKuInfo where Name='" & Me.dgQY.CurrentRow.Cells(0).Value & "'")
                    mdb.DataBind(Me.dgQY, "select Name as 仓库名称,Shuoming as 说明 from CangKuInfo")
                End If
            End If
        End If
    End Sub
End Class