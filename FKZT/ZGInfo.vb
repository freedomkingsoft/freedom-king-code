﻿Public Class ZGInfo

    Dim bChanged As Boolean

    
    Private Sub ZGInfo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select * from YongHu ")
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        dv.RowFilter = "YongHuJiBie=5"
        'Me.TextBox1.Text = dv.Item(0).Item(1).ToString
        dv.RowFilter = "YongHuJiBie < 5"
        Me.ComboBox1.DataSource = dv
        Me.ComboBox1.DisplayMember = dv.Table.Columns(1).ColumnName

        Me.CheckBox1.Checked = False
        Me.CheckBox2.Checked = False
        Me.Icon = FKG.myselfG.FKIcon
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.GroupBox1.Enabled = Me.CheckBox1.Checked

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        Me.GroupBox2.Enabled = Me.CheckBox2.Checked
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.Reader("select pwdcompare('" & Me.TextBox2.Text & "',YongHuMiMa) from YongHu where YongHuJiBie=5").Tables(0).Rows(0).Item(0) = 1 Then
            If mdb.Write("update YongHu set YongHuName='" & Me.TextBox1.Text & "', YongHuMiMa=pwdencrypt('" & Me.TextBox3.Text & "') where YongHuJiBie=5") = True Then
                MsgBox("密码修改成功!下次登录请使用新密码.", MsgBoxStyle.Information, "提示")
                Me.CheckBox1.Checked = False
            Else
                MsgBox("密码修改失败!用户名或者密码错误.", MsgBoxStyle.Information, "提示")

            End If
        Else
            MsgBox("您输入的系统管理员旧密码错误,没有权限修改该设置!", MsgBoxStyle.Information, "提示")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        If mdb.Write("update YongHu set YongHuJiBie=0 where YongHuJiBie=4") AndAlso mdb.Write("update YongHu set YongHuJiBie=4 where YongHuName='" & Me.ComboBox1.Text & "'") Then
            MsgBox("主管会计修改成功!", MsgBoxStyle.Information, "提示")
        End If
    End Sub
End Class