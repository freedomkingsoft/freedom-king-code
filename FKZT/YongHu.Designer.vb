﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class YongHu
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.dgvYongHu = New System.Windows.Forms.DataGridView
        Me.YongHuID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.用户名 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvQuanXian = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GNID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.可用功能名称 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvGongNeng = New System.Windows.Forms.DataGridView
        Me.GongNengID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.功能名称 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ckbAdd = New System.Windows.Forms.CheckBox
        Me.txtYongHuName = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnPSW = New System.Windows.Forms.Button
        Me.nudJiBie = New System.Windows.Forms.NumericUpDown
        Me.btnXiuGaiJiBie = New System.Windows.Forms.Button
        Me.bntRename = New System.Windows.Forms.Button
        Me.btnAddYongHu = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtNewName = New System.Windows.Forms.TextBox
        Me.btnDelYongHu = New System.Windows.Forms.Button
        Me.btnAddGongNeng = New System.Windows.Forms.Button
        Me.btnDelGongNeng = New System.Windows.Forms.Button
        Me.btnAddAll = New System.Windows.Forms.Button
        Me.btnDelAll = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.dgvBBQuanXian = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvBaoBiao = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.btnAddBaoBiao = New System.Windows.Forms.Button
        Me.btnAddAllBB = New System.Windows.Forms.Button
        Me.btnDelAllBB = New System.Windows.Forms.Button
        Me.btnDelBaoBiao = New System.Windows.Forms.Button
        Me.btnCDQX = New System.Windows.Forms.Button
        CType(Me.dgvYongHu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvQuanXian, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGongNeng, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nudJiBie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBBQuanXian, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBaoBiao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvYongHu
        '
        Me.dgvYongHu.AllowUserToAddRows = False
        Me.dgvYongHu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvYongHu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.YongHuID, Me.用户名})
        Me.dgvYongHu.Location = New System.Drawing.Point(12, 122)
        Me.dgvYongHu.Name = "dgvYongHu"
        Me.dgvYongHu.ReadOnly = True
        Me.dgvYongHu.RowHeadersVisible = False
        Me.dgvYongHu.RowTemplate.Height = 23
        Me.dgvYongHu.Size = New System.Drawing.Size(139, 278)
        Me.dgvYongHu.TabIndex = 0
        '
        'YongHuID
        '
        Me.YongHuID.HeaderText = "YongHuID"
        Me.YongHuID.Name = "YongHuID"
        Me.YongHuID.ReadOnly = True
        Me.YongHuID.Visible = False
        '
        '用户名
        '
        Me.用户名.HeaderText = "用户名"
        Me.用户名.Name = "用户名"
        Me.用户名.ReadOnly = True
        Me.用户名.Width = 115
        '
        'dgvQuanXian
        '
        Me.dgvQuanXian.AllowUserToAddRows = False
        Me.dgvQuanXian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvQuanXian.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.GNID, Me.可用功能名称})
        Me.dgvQuanXian.Location = New System.Drawing.Point(163, 122)
        Me.dgvQuanXian.Name = "dgvQuanXian"
        Me.dgvQuanXian.ReadOnly = True
        Me.dgvQuanXian.RowHeadersVisible = False
        Me.dgvQuanXian.RowTemplate.Height = 23
        Me.dgvQuanXian.Size = New System.Drawing.Size(139, 278)
        Me.dgvQuanXian.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "YongHuID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'GNID
        '
        Me.GNID.HeaderText = "GNID"
        Me.GNID.Name = "GNID"
        Me.GNID.ReadOnly = True
        Me.GNID.Visible = False
        '
        '可用功能名称
        '
        Me.可用功能名称.HeaderText = "可用功能名称"
        Me.可用功能名称.Name = "可用功能名称"
        Me.可用功能名称.ReadOnly = True
        Me.可用功能名称.Width = 115
        '
        'dgvGongNeng
        '
        Me.dgvGongNeng.AllowUserToAddRows = False
        Me.dgvGongNeng.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGongNeng.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GongNengID, Me.功能名称})
        Me.dgvGongNeng.Location = New System.Drawing.Point(357, 122)
        Me.dgvGongNeng.Name = "dgvGongNeng"
        Me.dgvGongNeng.ReadOnly = True
        Me.dgvGongNeng.RowHeadersVisible = False
        Me.dgvGongNeng.RowTemplate.Height = 23
        Me.dgvGongNeng.Size = New System.Drawing.Size(139, 278)
        Me.dgvGongNeng.TabIndex = 0
        '
        'GongNengID
        '
        Me.GongNengID.HeaderText = "GongNengID"
        Me.GongNengID.Name = "GongNengID"
        Me.GongNengID.ReadOnly = True
        Me.GongNengID.Visible = False
        '
        '功能名称
        '
        Me.功能名称.HeaderText = "功能名称"
        Me.功能名称.Name = "功能名称"
        Me.功能名称.ReadOnly = True
        Me.功能名称.Width = 115
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "用户列表"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(189, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "用户权限"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.Location = New System.Drawing.Point(385, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 15)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "功能列表"
        '
        'ckbAdd
        '
        Me.ckbAdd.AutoSize = True
        Me.ckbAdd.Checked = True
        Me.ckbAdd.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbAdd.Location = New System.Drawing.Point(128, 420)
        Me.ckbAdd.Name = "ckbAdd"
        Me.ckbAdd.Size = New System.Drawing.Size(48, 16)
        Me.ckbAdd.TabIndex = 2
        Me.ckbAdd.Text = "新增"
        Me.ckbAdd.UseVisualStyleBackColor = True
        '
        'txtYongHuName
        '
        Me.txtYongHuName.Location = New System.Drawing.Point(6, 16)
        Me.txtYongHuName.Name = "txtYongHuName"
        Me.txtYongHuName.Size = New System.Drawing.Size(91, 21)
        Me.txtYongHuName.TabIndex = 3
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnPSW)
        Me.GroupBox1.Controls.Add(Me.nudJiBie)
        Me.GroupBox1.Controls.Add(Me.btnXiuGaiJiBie)
        Me.GroupBox1.Controls.Add(Me.bntRename)
        Me.GroupBox1.Controls.Add(Me.btnAddYongHu)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtNewName)
        Me.GroupBox1.Controls.Add(Me.txtYongHuName)
        Me.GroupBox1.Location = New System.Drawing.Point(182, 400)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(522, 47)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'btnPSW
        '
        Me.btnPSW.Location = New System.Drawing.Point(444, 15)
        Me.btnPSW.Name = "btnPSW"
        Me.btnPSW.Size = New System.Drawing.Size(71, 28)
        Me.btnPSW.TabIndex = 7
        Me.btnPSW.Text = "重置密码"
        Me.btnPSW.UseVisualStyleBackColor = True
        '
        'nudJiBie
        '
        Me.nudJiBie.Location = New System.Drawing.Point(135, 15)
        Me.nudJiBie.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudJiBie.Name = "nudJiBie"
        Me.nudJiBie.Size = New System.Drawing.Size(35, 21)
        Me.nudJiBie.TabIndex = 6
        '
        'btnXiuGaiJiBie
        '
        Me.btnXiuGaiJiBie.Location = New System.Drawing.Point(374, 15)
        Me.btnXiuGaiJiBie.Name = "btnXiuGaiJiBie"
        Me.btnXiuGaiJiBie.Size = New System.Drawing.Size(64, 28)
        Me.btnXiuGaiJiBie.TabIndex = 5
        Me.btnXiuGaiJiBie.Text = "修改级别"
        Me.btnXiuGaiJiBie.UseVisualStyleBackColor = True
        '
        'bntRename
        '
        Me.bntRename.Location = New System.Drawing.Point(330, 15)
        Me.bntRename.Name = "bntRename"
        Me.bntRename.Size = New System.Drawing.Size(38, 28)
        Me.bntRename.TabIndex = 5
        Me.bntRename.Text = "改名"
        Me.bntRename.UseVisualStyleBackColor = True
        '
        'btnAddYongHu
        '
        Me.btnAddYongHu.Location = New System.Drawing.Point(174, 13)
        Me.btnAddYongHu.Name = "btnAddYongHu"
        Me.btnAddYongHu.Size = New System.Drawing.Size(64, 28)
        Me.btnAddYongHu.TabIndex = 5
        Me.btnAddYongHu.Text = "增加用户"
        Me.ToolTip1.SetToolTip(Me.btnAddYongHu, "新增用户默认密码为888888")
        Me.btnAddYongHu.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label7.Location = New System.Drawing.Point(104, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(29, 12)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "级别"
        '
        'txtNewName
        '
        Me.txtNewName.Location = New System.Drawing.Point(244, 18)
        Me.txtNewName.Name = "txtNewName"
        Me.txtNewName.Size = New System.Drawing.Size(84, 21)
        Me.txtNewName.TabIndex = 3
        Me.txtNewName.Text = "新用户名"
        '
        'btnDelYongHu
        '
        Me.btnDelYongHu.Location = New System.Drawing.Point(11, 406)
        Me.btnDelYongHu.Name = "btnDelYongHu"
        Me.btnDelYongHu.Size = New System.Drawing.Size(104, 35)
        Me.btnDelYongHu.TabIndex = 5
        Me.btnDelYongHu.Text = "删除所选用户"
        Me.btnDelYongHu.UseVisualStyleBackColor = True
        '
        'btnAddGongNeng
        '
        Me.btnAddGongNeng.Enabled = False
        Me.btnAddGongNeng.Location = New System.Drawing.Point(308, 228)
        Me.btnAddGongNeng.Name = "btnAddGongNeng"
        Me.btnAddGongNeng.Size = New System.Drawing.Size(43, 25)
        Me.btnAddGongNeng.TabIndex = 6
        Me.btnAddGongNeng.Text = "<--"
        Me.ToolTip1.SetToolTip(Me.btnAddGongNeng, "增加选定功能")
        Me.btnAddGongNeng.UseVisualStyleBackColor = True
        '
        'btnDelGongNeng
        '
        Me.btnDelGongNeng.Enabled = False
        Me.btnDelGongNeng.Location = New System.Drawing.Point(308, 282)
        Me.btnDelGongNeng.Name = "btnDelGongNeng"
        Me.btnDelGongNeng.Size = New System.Drawing.Size(43, 25)
        Me.btnDelGongNeng.TabIndex = 6
        Me.btnDelGongNeng.Text = "-->"
        Me.ToolTip1.SetToolTip(Me.btnDelGongNeng, "清除选定功能")
        Me.btnDelGongNeng.UseVisualStyleBackColor = True
        '
        'btnAddAll
        '
        Me.btnAddAll.Enabled = False
        Me.btnAddAll.Location = New System.Drawing.Point(308, 174)
        Me.btnAddAll.Name = "btnAddAll"
        Me.btnAddAll.Size = New System.Drawing.Size(43, 25)
        Me.btnAddAll.TabIndex = 6
        Me.btnAddAll.Text = "<-|"
        Me.ToolTip1.SetToolTip(Me.btnAddAll, "增加全部功能")
        Me.btnAddAll.UseVisualStyleBackColor = True
        '
        'btnDelAll
        '
        Me.btnDelAll.Enabled = False
        Me.btnDelAll.Location = New System.Drawing.Point(308, 336)
        Me.btnDelAll.Name = "btnDelAll"
        Me.btnDelAll.Size = New System.Drawing.Size(43, 25)
        Me.btnDelAll.TabIndex = 6
        Me.btnDelAll.Text = "|->"
        Me.ToolTip1.SetToolTip(Me.btnDelAll, "清除所有功能")
        Me.btnDelAll.UseVisualStyleBackColor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 50
        Me.ToolTip1.ReshowDelay = 100
        '
        'dgvBBQuanXian
        '
        Me.dgvBBQuanXian.AllowUserToAddRows = False
        Me.dgvBBQuanXian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBBQuanXian.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.dgvBBQuanXian.Location = New System.Drawing.Point(516, 122)
        Me.dgvBBQuanXian.Name = "dgvBBQuanXian"
        Me.dgvBBQuanXian.ReadOnly = True
        Me.dgvBBQuanXian.RowHeadersVisible = False
        Me.dgvBBQuanXian.RowTemplate.Height = 23
        Me.dgvBBQuanXian.Size = New System.Drawing.Size(139, 278)
        Me.dgvBBQuanXian.TabIndex = 0
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "YongHuID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "GNID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "可用报表名称"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 115
        '
        'dgvBaoBiao
        '
        Me.dgvBaoBiao.AllowUserToAddRows = False
        Me.dgvBaoBiao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBaoBiao.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.dgvBaoBiao.Location = New System.Drawing.Point(710, 122)
        Me.dgvBaoBiao.Name = "dgvBaoBiao"
        Me.dgvBaoBiao.ReadOnly = True
        Me.dgvBaoBiao.RowHeadersVisible = False
        Me.dgvBaoBiao.RowTemplate.Height = 23
        Me.dgvBaoBiao.Size = New System.Drawing.Size(139, 278)
        Me.dgvBaoBiao.TabIndex = 0
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "GongNengID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "报表名称"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 115
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label5.Location = New System.Drawing.Point(542, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 15)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "报表权限"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("宋体", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label6.Location = New System.Drawing.Point(738, 104)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 15)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "报表列表"
        '
        'btnAddBaoBiao
        '
        Me.btnAddBaoBiao.Enabled = False
        Me.btnAddBaoBiao.Location = New System.Drawing.Point(661, 228)
        Me.btnAddBaoBiao.Name = "btnAddBaoBiao"
        Me.btnAddBaoBiao.Size = New System.Drawing.Size(43, 25)
        Me.btnAddBaoBiao.TabIndex = 6
        Me.btnAddBaoBiao.Text = "<--"
        Me.btnAddBaoBiao.UseVisualStyleBackColor = True
        '
        'btnAddAllBB
        '
        Me.btnAddAllBB.Enabled = False
        Me.btnAddAllBB.Location = New System.Drawing.Point(661, 174)
        Me.btnAddAllBB.Name = "btnAddAllBB"
        Me.btnAddAllBB.Size = New System.Drawing.Size(43, 25)
        Me.btnAddAllBB.TabIndex = 6
        Me.btnAddAllBB.Text = "<-|"
        Me.btnAddAllBB.UseVisualStyleBackColor = True
        '
        'btnDelAllBB
        '
        Me.btnDelAllBB.Enabled = False
        Me.btnDelAllBB.Location = New System.Drawing.Point(661, 336)
        Me.btnDelAllBB.Name = "btnDelAllBB"
        Me.btnDelAllBB.Size = New System.Drawing.Size(43, 25)
        Me.btnDelAllBB.TabIndex = 6
        Me.btnDelAllBB.Text = "|->"
        Me.btnDelAllBB.UseVisualStyleBackColor = True
        '
        'btnDelBaoBiao
        '
        Me.btnDelBaoBiao.Enabled = False
        Me.btnDelBaoBiao.Location = New System.Drawing.Point(661, 282)
        Me.btnDelBaoBiao.Name = "btnDelBaoBiao"
        Me.btnDelBaoBiao.Size = New System.Drawing.Size(43, 25)
        Me.btnDelBaoBiao.TabIndex = 6
        Me.btnDelBaoBiao.Text = "-->"
        Me.btnDelBaoBiao.UseVisualStyleBackColor = True
        '
        'btnCDQX
        '
        Me.btnCDQX.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCDQX.Location = New System.Drawing.Point(731, 413)
        Me.btnCDQX.Name = "btnCDQX"
        Me.btnCDQX.Size = New System.Drawing.Size(118, 28)
        Me.btnCDQX.TabIndex = 5
        Me.btnCDQX.Text = "设置菜单权限"
        Me.btnCDQX.UseVisualStyleBackColor = True
        '
        'YongHu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(868, 457)
        Me.Controls.Add(Me.btnDelBaoBiao)
        Me.Controls.Add(Me.btnCDQX)
        Me.Controls.Add(Me.btnDelGongNeng)
        Me.Controls.Add(Me.btnDelAllBB)
        Me.Controls.Add(Me.btnDelAll)
        Me.Controls.Add(Me.btnAddAllBB)
        Me.Controls.Add(Me.btnAddAll)
        Me.Controls.Add(Me.btnAddBaoBiao)
        Me.Controls.Add(Me.btnAddGongNeng)
        Me.Controls.Add(Me.ckbAdd)
        Me.Controls.Add(Me.btnDelYongHu)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvBaoBiao)
        Me.Controls.Add(Me.dgvBBQuanXian)
        Me.Controls.Add(Me.dgvGongNeng)
        Me.Controls.Add(Me.dgvQuanXian)
        Me.Controls.Add(Me.dgvYongHu)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "YongHu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "用户管理"
        CType(Me.dgvYongHu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvQuanXian, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGongNeng, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nudJiBie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBBQuanXian, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBaoBiao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvYongHu As System.Windows.Forms.DataGridView
    Friend WithEvents dgvQuanXian As System.Windows.Forms.DataGridView
    Friend WithEvents dgvGongNeng As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ckbAdd As System.Windows.Forms.CheckBox
    Friend WithEvents txtYongHuName As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAddYongHu As System.Windows.Forms.Button
    Friend WithEvents btnDelYongHu As System.Windows.Forms.Button
    Friend WithEvents YongHuID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 用户名 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GongNengID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 功能名称 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GNID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 可用功能名称 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAddGongNeng As System.Windows.Forms.Button
    Friend WithEvents btnDelGongNeng As System.Windows.Forms.Button
    Friend WithEvents btnAddAll As System.Windows.Forms.Button
    Friend WithEvents btnDelAll As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents dgvBBQuanXian As System.Windows.Forms.DataGridView
    Friend WithEvents dgvBaoBiao As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnAddBaoBiao As System.Windows.Forms.Button
    Friend WithEvents btnAddAllBB As System.Windows.Forms.Button
    Friend WithEvents btnDelAllBB As System.Windows.Forms.Button
    Friend WithEvents btnDelBaoBiao As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nudJiBie As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnCDQX As System.Windows.Forms.Button
    Friend WithEvents btnPSW As System.Windows.Forms.Button
    Friend WithEvents btnXiuGaiJiBie As System.Windows.Forms.Button
    Friend WithEvents bntRename As System.Windows.Forms.Button
    Friend WithEvents txtNewName As System.Windows.Forms.TextBox
End Class
