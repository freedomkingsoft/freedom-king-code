﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZTInfo
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgQY = New System.Windows.Forms.DataGridView
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.kemujishu = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.qiyongriqi = New System.Windows.Forms.DateTimePicker
        Me.nianfen = New System.Windows.Forms.CheckBox
        Me.bianhaoweishu = New System.Windows.Forms.TextBox
        Me.zhangtaomingcheng = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.dgQY, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgQY
        '
        Me.dgQY.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgQY.Location = New System.Drawing.Point(12, 310)
        Me.dgQY.Name = "dgQY"
        Me.dgQY.RowHeadersVisible = False
        Me.dgQY.RowHeadersWidth = 85
        Me.dgQY.RowTemplate.Height = 23
        Me.dgQY.Size = New System.Drawing.Size(394, 135)
        Me.dgQY.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(71, 466)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(109, 32)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "保    存"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(260, 466)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(109, 32)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "退　　　出"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.kemujishu)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.qiyongriqi)
        Me.GroupBox1.Controls.Add(Me.nianfen)
        Me.GroupBox1.Controls.Add(Me.bianhaoweishu)
        Me.GroupBox1.Controls.Add(Me.zhangtaomingcheng)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(382, 267)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "企业资料设定"
        '
        'kemujishu
        '
        Me.kemujishu.Location = New System.Drawing.Point(109, 231)
        Me.kemujishu.Name = "kemujishu"
        Me.kemujishu.Size = New System.Drawing.Size(115, 21)
        Me.kemujishu.TabIndex = 36
        Me.kemujishu.Text = "433222222"
        Me.kemujishu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(23, 234)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 12)
        Me.Label7.TabIndex = 35
        Me.Label7.Text = "各级科目位数:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(231, 234)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(101, 12)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "默认为:433332222"
        '
        'Label3
        '
        Me.Label3.AutoEllipsis = True
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("黑体", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(21, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(346, 40)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "设置账套的核心基本信息," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "启用后除名称外,其他各项不要修改!"
        '
        'qiyongriqi
        '
        Me.qiyongriqi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.qiyongriqi.Location = New System.Drawing.Point(108, 113)
        Me.qiyongriqi.Name = "qiyongriqi"
        Me.qiyongriqi.Size = New System.Drawing.Size(150, 21)
        Me.qiyongriqi.TabIndex = 32
        Me.qiyongriqi.Value = New Date(2013, 10, 18, 0, 0, 0, 0)
        '
        'nianfen
        '
        Me.nianfen.AutoSize = True
        Me.nianfen.Location = New System.Drawing.Point(45, 197)
        Me.nianfen.Name = "nianfen"
        Me.nianfen.Size = New System.Drawing.Size(300, 16)
        Me.nianfen.TabIndex = 29
        Me.nianfen.Text = "单证编号中的年份使用4位数字,不选该项则使用２位"
        Me.nianfen.UseVisualStyleBackColor = True
        '
        'bianhaoweishu
        '
        Me.bianhaoweishu.Location = New System.Drawing.Point(110, 157)
        Me.bianhaoweishu.Name = "bianhaoweishu"
        Me.bianhaoweishu.Size = New System.Drawing.Size(63, 21)
        Me.bianhaoweishu.TabIndex = 28
        Me.bianhaoweishu.Text = "3"
        '
        'zhangtaomingcheng
        '
        Me.zhangtaomingcheng.Location = New System.Drawing.Point(108, 77)
        Me.zhangtaomingcheng.Name = "zhangtaomingcheng"
        Me.zhangtaomingcheng.Size = New System.Drawing.Size(223, 21)
        Me.zhangtaomingcheng.TabIndex = 27
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(179, 160)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 12)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "注:可选2,3,4,5,6"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 160)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 12)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "编号位数:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(43, 117)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 12)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "启用日期:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(43, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 12)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "企业名称:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 295)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "自由定义企业信息"
        '
        'ZTInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 510)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgQY)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ZTInfo"
        Me.Text = "企业资料设置"
        CType(Me.dgQY, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgQY As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents qiyongriqi As System.Windows.Forms.DateTimePicker
    Friend WithEvents nianfen As System.Windows.Forms.CheckBox
    Friend WithEvents bianhaoweishu As System.Windows.Forms.TextBox
    Friend WithEvents zhangtaomingcheng As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents kemujishu As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
