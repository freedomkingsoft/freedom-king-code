﻿Imports System.Windows.Forms

Public Class FKGZJZ

    Private Sub FKKCJZ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        FillDG()
    End Sub

    Private Function FillDG() As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        mdb.DataBind(Me.dgGZJZQJ, "select NY as 年月,ksrq as 起始日期,zzrq as 截止日期,GZJZ AS 工资结账 from FKZQ WHERE NY LIKE '" & FKG.myselfG.NianFen & "%'")
    End Function

    Private Sub JinDu()
        Dim dlgTS As New FKG.JinDu("正在结账，请稍候......", 1)
        dlgTS.ShowDialog()
    End Sub

    Private Sub btnKCJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCJZ.Click
        Dim mdbOpen As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        Try

            '首先判断是否存在读写冲突
            Dim iT As Integer = 0
            Do While FKG.myselfG.bCanBackup = False
                System.Threading.Thread.Sleep(50)
                iT = iT + 1

                If iT >= 150 Then
                    MsgBox("数据库正在使用当中，没有完成结帐！")
                    Exit Sub
                End If
            Loop

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            Dim iNY As String
            Dim ds As DataSet
            ds = mdb.Reader("select NY from FKZQ where GZJZ='false' and NY like '" & FKG.myselfG.NianFen & "%' order by NY")
            If ds.Tables(0).Rows.Count = 0 Then
                Exit Sub
            Else
                iNY = ds.Tables(0).Rows(0).Item(0)
                Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

                If iyue <= FKG.myselfG.YueFen Then
                    '禁用自动定时备份
                    FKG.myselfG.bCanBackup = False

                    Dim objTh As New Threading.Thread(AddressOf JinDu)
                    'objTh.Start()

                    Try
                        mdbOpen.oleConnect.Open()
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
                        mdbOpen.oleConnect.Close()
                        '启用自动定时备份
                        FKG.myselfG.bCanBackup = True
                        Exit Sub
                    End Try

                    '可以进行工资结账
                    '第一步，进行工资余额表的结账
                    mdbOpen.oleCommand.CommandText = "update FKZQ SET GZJZ='true' WHERE NY='" & FKG.myselfG.NianFen & iyue.ToString.PadLeft(2, "0") & "'"
                    mdbOpen.oleCommand.ExecuteNonQuery()

                    '生成工资余额表
                    Dim dsKCYE As New DataSet
                    mdbOpen.oleCommand.CommandText = "select 科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from GONGZI where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " and 科目 in (select kmdm from kmdm where nian=" & FKG.myselfG.NianFen & " and hslbgz='true') group by 科目"
                    mdbOpen.oleDataAdapter.Fill(dsKCYE)

                    Dim i As Integer
                    For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                        Dim dsbExsit As New DataSet
                        mdbOpen.oleCommand.CommandText = "select KMDM from GZYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                        mdbOpen.oleDataAdapter.Fill(dsbExsit)

                        If dsbExsit.Tables(0).Rows.Count > 0 Then
                            'update
                            mdbOpen.oleCommand.CommandText = "update GZYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(1) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                            mdbOpen.oleCommand.ExecuteNonQuery()

                        Else
                            'insert
                            mdbOpen.oleCommand.CommandText = "insert into GZYE (kmdm,nian,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & ")"
                            mdbOpen.oleCommand.ExecuteNonQuery()
                        End If
                    Next

                    '写入非末级科目的余额
                    dsKCYE.Clear()
                    dsKCYE = New DataSet

                    mdbOpen.oleCommand.CommandText = "select KMDM,sum(借数),sum(借金),sum(贷数),sum(贷金) from (select KMDM, TYE.* from KMDM,(select 科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from GONGZI where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目) as TYE where (KMDM.HSLBGZ='true') AND KMDM.NIAN= " & FKG.myselfG.NianFen & " AND KMDM.SFMJ='false' AND (charindex(TYE.科目,kmdm.kmdm)=1 )) as ttt group by KMDM"
                    mdbOpen.oleDataAdapter.Fill(dsKCYE)
                    'Dim i As Integer
                    For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                        Dim dsbExsit2 As New DataSet
                        mdbOpen.oleCommand.CommandText = "select KMDM from GZYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                        mdbOpen.oleDataAdapter.Fill(dsbExsit2)

                        If dsbExsit2.Tables(0).Rows.Count > 0 Then
                            'update
                            mdbOpen.oleCommand.CommandText = "update GZYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(1) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                            mdbOpen.oleCommand.ExecuteNonQuery()

                        Else
                            'insert
                            mdbOpen.oleCommand.CommandText = "insert into GZYE (kmdm,nian,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & ")"
                            mdbOpen.oleCommand.ExecuteNonQuery()
                        End If
                    Next


                    '可以进行工资结账
                    '第二步，进行委外余额表的结账
                    'mdbOpen.oleCommand.CommandText = "update FKZQ SET GZJZ='true' WHERE NY='" & FKG.myselfG.NianFen & iyue.ToString.PadLeft(2, "0") & "'"
                    'mdbOpen.oleCommand.ExecuteNonQuery()

                    '生成工资余额表
                    'Dim dsKCYE As New DataSet
                    dsKCYE.Clear()
                    dsKCYE = New DataSet
                    mdbOpen.oleCommand.CommandText = "select 科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from GONGZI where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " and 科目 in (select kmdm from kmdm where nian=" & FKG.myselfG.NianFen & " and hslbww='true')group by 科目"
                    mdbOpen.oleDataAdapter.Fill(dsKCYE)

                    'Dim i As Integer
                    For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                        Dim dsbExsit As New DataSet
                        mdbOpen.oleCommand.CommandText = "select KMDM from WWYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                        mdbOpen.oleDataAdapter.Fill(dsbExsit)

                        If dsbExsit.Tables(0).Rows.Count > 0 Then
                            'update
                            mdbOpen.oleCommand.CommandText = "update WWYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(1) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                            mdbOpen.oleCommand.ExecuteNonQuery()

                        Else
                            'insert
                            mdbOpen.oleCommand.CommandText = "insert into WWYE (kmdm,nian,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & ")"
                            mdbOpen.oleCommand.ExecuteNonQuery()
                        End If
                    Next

                    '写入非末级科目的余额
                    dsKCYE.Clear()
                    dsKCYE = New DataSet

                    mdbOpen.oleCommand.CommandText = "select KMDM,sum(借数),sum(借金),sum(贷数),sum(贷金) from (select KMDM, TYE.* from KMDM,(select 科目,sum(借方数量) as 借数,sum(借方金额) as 借金,sum(贷方数量) as 贷数,sum(贷方金额) as 贷金 from GONGZI where 年份=" & FKG.myselfG.NianFen & " and 月份=" & iyue & " group by 科目) as TYE where (KMDM.HSLBWW='true') AND KMDM.NIAN= " & FKG.myselfG.NianFen & " AND KMDM.SFMJ='false' AND (charindex(TYE.科目,kmdm.kmdm)=1 )) as ttt group by KMDM"
                    mdbOpen.oleDataAdapter.Fill(dsKCYE)
                    'Dim i As Integer
                    For i = 0 To dsKCYE.Tables(0).Rows.Count - 1
                        Dim dsbExsit2 As New DataSet
                        mdbOpen.oleCommand.CommandText = "select KMDM from WWYE where KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                        mdbOpen.oleDataAdapter.Fill(dsbExsit2)

                        If dsbExsit2.Tables(0).Rows.Count > 0 Then
                            'update
                            mdbOpen.oleCommand.CommandText = "update WWYE set jfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(1) & ",jfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(2) & ",dfsl" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(3) & ",dfje" & iyue & "=" & dsKCYE.Tables(0).Rows(i).Item(4) & " where  KMDM='" & dsKCYE.Tables(0).Rows(i).Item(0) & "'  and Nian=" & FKG.myselfG.NianFen
                            mdbOpen.oleCommand.ExecuteNonQuery()

                        Else
                            'insert
                            mdbOpen.oleCommand.CommandText = "insert into WWYE (kmdm,nian,jfsl" & iyue & ",jfje" & iyue & ",dfsl" & iyue & ",dfje" & iyue & ") values ('" & dsKCYE.Tables(0).Rows(i).Item(0) & "'," & FKG.myselfG.NianFen & "," & dsKCYE.Tables(0).Rows(i).Item(1) & "," & dsKCYE.Tables(0).Rows(i).Item(2) & "," & dsKCYE.Tables(0).Rows(i).Item(3) & "," & dsKCYE.Tables(0).Rows(i).Item(4) & ")"
                            mdbOpen.oleCommand.ExecuteNonQuery()
                        End If
                    Next

                    mdbOpen.oleConnect.Close()

                    FillDG()

                    objTh.Abort()

                    '启用自动定时备份
                    FKG.myselfG.bCanBackup = True
                Else
                    '启用自动定时备份
                    FKG.myselfG.bCanBackup = True
                    Exit Sub
                End If
            End If
            '启用自动定时备份
            FKG.myselfG.bCanBackup = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
            MsgBox("因出现异常,本次结帐未成功,请重新记账!", MsgBoxStyle.Information, "提示")

            btnKCFJZ_Click(sender, e)
        Finally
            mdbOpen.oleConnect.Close()
        End Try
    End Sub

    Private Sub btnKCFJZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKCFJZ.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim iNY As String
        Dim ds As DataSet
        ds = mdb.Reader("select NY from FKZQ where GZJZ='true' and NY >= '" & FKG.myselfG.dQiYongRiQi.Year.ToString & FKG.myselfG.dQiYongRiQi.Month.ToString.PadLeft(2, "0") & "' and NY like '" & FKG.myselfG.NianFen & "%' order by NY desc")
        If ds.Tables(0).Rows.Count = 0 Then
            Exit Sub
        Else
            iNY = ds.Tables(0).Rows(0).Item(0)
            Dim iyue As Integer = CType(iNY.Substring(4, 2), Integer)

            mdb.Write("update GZYE set jfsl" & iyue & "=0,jfje" & iyue & "=0,dfsl" & iyue & "=0,dfje" & iyue & "=0 where  Nian=" & FKG.myselfG.NianFen)
            mdb.Write("update WWYE set jfsl" & iyue & "=0,jfje" & iyue & "=0,dfsl" & iyue & "=0,dfje" & iyue & "=0 where  Nian=" & FKG.myselfG.NianFen) '新增，清除委外余额结账数据
            mdb.Write("update FKZQ SET GZJZ='false' WHERE NY='" & FKG.myselfG.NianFen & iyue.ToString.PadLeft(2, "0") & "'")
            FillDG()
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
