--存储过程查询的备忘
-- 1，保存到ZDYBB中，注意isBaoBiao和isMingXi列都要为True。
-- 2，Biao 列保存 ‘存储过程’，不可以为其他值。
-- 3，MRTianJian 列保存 存储过程名称。
-- 4，Lie 列保存参数列表，参数格式为：参数名,参数值;参数名,参数值
--	  如果与下面五个默认参数有关，就不要再重复保存了。
-- 5，LieSetup 列一般留空就可以。YHKJTJ也留空

--需要更改数据库名字
USE [FreedomKingData]
GO

/****** Object:  StoredProcedure [dbo].[cx_kmdm]    Script Date: 04/17/2015 17:21:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--需要更改存储过程名称
CREATE PROCEDURE [dbo].[cx_kmdm]
--以下五个为必须有的参数，但是可以不用
@MinDate datetime,
@MaxDate datetime,
@BianHao nvarchar(max),
@KeMu nvarchar(max),
@Xinghao nvarchar(max),
	-- Add the parameters for the stored procedure here
	--再多的参数就需要保存到 Lie 字段中去。
	@nian int
AS
BEGIN
	SET NOCOUNT ON;
	set @nian=datepart(yy,@MaxDate)
    -- Insert statements for procedure here
	select * from KMDM where Nian=@nian and KMDM=@KeMu
END

GO


