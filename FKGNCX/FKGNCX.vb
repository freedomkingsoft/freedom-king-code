﻿Public Class FKGNCX


    Private Sub FKGNCX_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FillLBGongNeng()
        Me.Icon = FKG.myselfG.FKIcon

        Me.myMinDate.Value = "#" & FKG.myselfG.NianFen & "-" & FKG.myselfG.YueFen & "-1#"
        Me.myMaxDate.Value = Today

        Me.myMinDate.MinDate = FKG.myselfG.dQiYongRiQi
        Me.myMinDate.MaxDate = Today

        Me.myMaxDate.MinDate = FKG.myselfG.dQiYongRiQi
        Me.myMaxDate.MaxDate = Today

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select KMDM from KMDM where HSLBKC='true' AND nian=" & FKG.myselfG.NianFen & "  ")
        Try
            Me.myMinKM.Text = ds.Tables(0).Rows(0).Item(0)
            Me.myMaxKM.Text = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(0)
        Catch ex As Exception

        End Try

        FillCangKu()
        FillFZSX()
        FillDYKM()
    End Sub

    Private Sub FillDYKM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsDYKM As DataSet
        dsDYKM = mdb.Reader("select distinct 对应科目 from 库存表 where 显示='true'")

        Dim i As Integer
        For i = 0 To dsDYKM.Tables(0).Rows.Count - 1
            Me.cbDYKM.Items.Add(dsDYKM.Tables(0).Rows(i).Item(0).ToString)
        Next

        Me.cbDYKM.Items.Insert(0, "不限定对应科目")
        Me.cbDYKM.SelectedIndex = 0
    End Sub

    Private Sub FillFZSX()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsFZSX As DataSet
        dsFZSX = mdb.Reader("select DISTINCT 辅助属性名称 from KMFZSX,KMDM where KMDM.KMDM=KMFZSX.科目代码 and KMDM.Nian=KMFZSX.Nian and KMDM.Nian=" & FKG.myselfG.NianFen & " and KMDM.HSLBKC='true'")

        Dim I As Integer
        For I = 0 To dsFZSX.Tables(0).Rows.Count - 1
            Me.lbFZSX.Items.Add(dsFZSX.Tables(0).Rows(I).Item(0).ToString)
        Next



    End Sub

    Private Sub FillLBGongNeng()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsGongNeng As DataSet
        dsGongNeng = mdb.Reader("select distinct Context from GongnengShu,GN_KuCun where  GongNengshu.ID=GN_KuCun.GNSID")
        Dim i As Integer
        For i = 0 To dsGongNeng.Tables(0).Rows.Count - 1
            Me.lbGongNeng.Items.Add(dsGongNeng.Tables(0).Rows(i).Item(0).ToString)
        Next

        '增加期初和期末余额
        Me.lbGongNeng.Items.Insert(0, "期末")
        Me.lbGongNeng.Items.Insert(0, "期初")
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim th As New Threading.Thread(AddressOf JinDu)
        'th.Start()

        Me.dgv.DataSource = Nothing
        gnCX()

        Me.btnSave.Enabled = True
        th.Abort()
    End Sub

    Private Sub JinDu()
        Dim dlg As New FKG.JinDu("正在统计,请稍候...", 0)
        dlg.ShowDialog()
    End Sub

    Private Function gnCX() As Boolean
        If Me.lbSelectedGN.Items.Count = 0 Then
            Return False
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim i As Integer

        Dim sLie As String = ""
        Dim sTable As String = ""
        Dim sJoin1 As String = ""
        Dim sJoin As String = ""
        Dim sCaseFSE As String = ""
        Dim sCaseKM As String = ""

        '日期条件
        sCaseFSE = " and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'"

        '设定仓库
        If Me.cbCangKuID.SelectedIndex = 0 Then
            Dim dsKuCase As DataSet
            If myWCK.Checked Then
                dsKuCase = mdb.Reader("select ID from CangKuInfo where shuoming='外存库' ")
                'Dim i As Integer
                For i = 0 To dsKuCase.Tables(0).Rows.Count - 1
                    sCaseFSE = sCaseFSE & " and 仓库<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                    ' sCaseID = sCaseID & " and 仓库编号<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                Next
            Else

            End If

            If myFPK.Checked Then
                dsKuCase = mdb.Reader("select ID from CangKuInfo where shuoming='废品库' ")
                'Dim i As Integer
                For i = 0 To dsKuCase.Tables(0).Rows.Count - 1
                    sCaseFSE = sCaseFSE & " and 仓库<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                    '  sCaseID = sCaseID & " and 仓库编号<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                Next
            Else

            End If

        Else
            sCaseFSE = sCaseFSE & " and  仓库=" & Me.cbCangKuID.SelectedItem.ToString
        End If

        If Me.cbDYKM.SelectedIndex > 0 Then
            sCaseFSE = sCaseFSE & " and 对应科目='" & Me.cbDYKM.SelectedItem.ToString & "'"
        End If
        '设定科目
        sCaseKM = " and KMDM.Nian=" & Me.myMaxDate.Value.Year & " and KMDM.KMDM >= '" & Me.myMinKM.Text & "' and KMDM.KMDM < '" & (CType(Me.myMaxKM.Text, Double) + 1).ToString & "' "
        sCaseFSE = sCaseFSE & " and 科目DM >= '" & Me.myMinKM.Text & "' and  科目DM < '" & (CType(Me.myMaxKM.Text, Integer) + 1).ToString & "' "

        For i = 0 To Me.lbSelectedGN.Items.Count - 1
            If i = 0 Then
                If Me.lbSelectedGN.Items(i) = "期初" Then

                    sLie = " '1' as isMJKM, TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"


                    sTable = " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期初借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期初借方金额,0 as 期初贷方数量,0 as 期初贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM, isNull(SUM(借方数量),0) AS QCJFSL, isNull(sum(借方金额),0) as QCJFJE, isNull(sum(贷方数量),0) as QCDFSL, isnull(sum(贷方金额),0) as QCDFJE FROM 库存表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<'" & Me.myMinDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期初 on kmdm.kmdm=t期初.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期初发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & " " & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期初 on kmdm.kmdm=t期初.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期初发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","
                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                ElseIf Me.lbSelectedGN.Items(0) = "期末" Then
                    '      sCaseKM = " and KMDM.Nian=" & Me.myMaxDate.Value.Year & sCaseKMDM

                    sLie = " '1' as isMJKM, TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期末借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期末借方金额,0 as 期末贷方数量,0 as 期末贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 年份>=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期末 on kmdm.kmdm=t期末.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期末发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期末 on kmdm.kmdm=t期末.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期末发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                Else
                    '  sCaseKM = " and KMDM.Nian=" & FKG.myselfG.NianFen & sCaseKMDM

                    sLie = " '1' as isMJKM, TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = " (SELECT KMDM as 科目代码,KMMC as 科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额 from KMDM  LEFT JOIN (SELECT 科目DM,SUM(借方数量) AS " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额  FROM 库存表 where 功能名称='" & Me.lbSelectedGN.Items(i).ToString & "' " & sCaseFSE & "  group by 科目DM) as T" & Me.lbSelectedGN.Items(i).ToString & " on kmdm.kmdm=t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM WHERE HSLBKC='true' AND SFMJ='true'" & sCaseKM & "  ) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                End If

            Else
                If Me.lbSelectedGN.Items(i) = "期初" Then

                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期初借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期初借方金额,0 as 期初贷方数量,0 as 期初贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<'" & Me.myMinDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期初 on kmdm.kmdm=t期初.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期初发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & " " & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期初 on kmdm.kmdm=t期初.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期初发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                ElseIf Me.lbSelectedGN.Items(i) = "期末" Then

                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期末借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期末借方金额,0 as 期末贷方数量,0 as 期末贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 年份>=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期末 on kmdm.kmdm=t期末.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期末发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期末 on kmdm.kmdm=t期末.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期末发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                Else
                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT KMDM as 科目代码,KMMC as 科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额 from KMDM  LEFT JOIN (SELECT 科目DM,SUM(借方数量) AS " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额  FROM 库存表 where 功能名称='" & Me.lbSelectedGN.Items(i).ToString & "' " & sCaseFSE & "   group by 科目DM) as T" & Me.lbSelectedGN.Items(i).ToString & " on kmdm.kmdm=t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM WHERE HSLBKC='true' AND SFMJ='true'" & sCaseKM & "  ) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                End If
            End If
        Next

        For i = 0 To Me.lbSelectedFZSX.Items.Count - 1

            sLie = sLie & Me.lbSelectedFZSX.Items(i).ToString & ","

            sTable = sTable & " (select KMDM as 科目代码," & Me.lbSelectedFZSX.Items(i).ToString & " from KMDM left join (select 科目代码,kmfzsx.NIAN,辅助属性值 as " & Me.lbSelectedFZSX.Items(i).ToString & " from KMFZSX where KMFZSX.辅助属性名称='" & Me.lbSelectedFZSX.Items(i).ToString & "' ) as T" & Me.lbSelectedFZSX.Items(i).ToString & " on KMDM.KMDM=T" & Me.lbSelectedFZSX.Items(i).ToString & ".科目代码 AND KMDM.NIAN=T" & Me.lbSelectedFZSX.Items(i).ToString & ".NIAN where  KMDM.HSLBKC='true' AND KMDM.SFMJ='true' " & sCaseKM & " ) AS TTT" & Me.lbSelectedFZSX.Items(i).ToString & ","


            sJoin = sJoin & sJoin1 & "=TTT" & Me.lbSelectedFZSX.Items(i).ToString & ".科目代码 and "

        Next

        Dim sSQL As String

        If Me.lbSelectedGN.Items.Count + Me.lbSelectedFZSX.Items.Count > 1 Then
            sSQL = " select " & sLie.Remove(sLie.Length - 1, 1) & " from " & sTable.Remove(sTable.Length - 1, 1) & " where " & sJoin.Remove(sJoin.Length - 5, 5)
        Else
            sSQL = " select " & sLie.Remove(sLie.Length - 1, 1) & " from " & sTable.Remove(sTable.Length - 1, 1)
        End If

        '计算非末级科目发生额
        sJoin = ""

        ''sfmj=false

        For i = 0 To Me.lbSelectedGN.Items.Count - 1
            If i = 0 Then
                If Me.lbSelectedGN.Items(i) = "期初" Then
                    '           sCaseKM = " and KMDM.Nian=" & Me.myMinDate.Value.Year & sCaseKMDM

                    sLie = " '0' as isMJKM,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称, " & Me.lbSelectedGN.Items(i).ToString & "借方数量, " & Me.lbSelectedGN.Items(i).ToString & "借方金额,  " & Me.lbSelectedGN.Items(i).ToString & "贷方数量, " & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"


                    sTable = " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期初借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期初借方金额,0 as 期初贷方数量,0 as 期初贷方金额 from (select 科目代码,科目名称,sum(qcjfsl) as QCJFSL,SUM(QCJFJE) AS QCJFJE,SUM(QCDFSL) AS QCDFSL,SUM(QCDFJE) AS QCDFJE from (SELECT KMDM as 科目代码,KMMC as 科目名称,QCJFSL,QCJFJE,QCDFSL,QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<'" & Me.myMinDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期初 on (charindex(T期初.科目DM,kmdm.kmdm)=1) WHERE HSLBKC='true' AND sfmj='false' " & sCaseKM & "  ) as tempT1 group by 科目代码,科目名称) as T期初发生,(select 科目代码,科目名称,sum(NCJFSL) AS NCJFSL ,SUM(NCJFYE) AS NCJFYE,SUM(NCDFSL) AS NCDFSL,SUM(NCDFYE) AS NCDFYE from (SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期初 on (T期初.科目代码=kmdm.kmdm) WHERE sfmj='false' " & sCaseKM & "  )  as tempT2 group by 科目代码,科目名称) AS TNCYE where T期初发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                ElseIf Me.lbSelectedGN.Items(i) = "期末" Then
                    '        sCaseKM = " and KMDM.Nian=" & Me.myMaxDate.Value.Year & sCaseKMDM

                    sLie = " '0' as isMJKM,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称, " & Me.lbSelectedGN.Items(i).ToString & "借方数量, " & Me.lbSelectedGN.Items(i).ToString & "借方金额,  " & Me.lbSelectedGN.Items(i).ToString & "贷方数量, " & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期末借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期末借方金额,0 as 期末贷方数量,0 as 期末贷方金额 from (select 科目代码,科目名称,sum(qcjfsl) as QCJFSL,SUM(QCJFJE) AS QCJFJE,SUM(QCDFSL) AS QCDFSL,SUM(QCDFJE) AS QCDFJE from (SELECT KMDM as 科目代码,KMMC as 科目名称,QCJFSL,QCJFJE,QCDFSL,QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 年份>=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期末 on (charindex(t期末.科目DM,kmdm.kmdm)=1) WHERE HSLBKC='true' AND sfmj='false' " & sCaseKM & "  )  as tempT3 group by 科目代码,科目名称) as T期末发生,(select 科目代码,科目名称,sum(NCJFSL) AS NCJFSL ,SUM(NCJFYE) AS NCJFYE,SUM(NCDFSL) AS NCDFSL,SUM(NCDFYE) AS NCDFYE from (SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期末 on (t期末.科目代码=kmdm.kmdm) WHERE sfmj='false' " & sCaseKM & "  )  as tempT4  group by 科目代码,科目名称) AS TNCYE where T期末发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                Else
                    '      sCaseKM = " and KMDM.Nian=" & FKG.myselfG.NianFen & sCaseKMDM

                    sLie = " '0' as isMJKM,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称, " & Me.lbSelectedGN.Items(i).ToString & "借方数量, " & Me.lbSelectedGN.Items(i).ToString & "借方金额,  " & Me.lbSelectedGN.Items(i).ToString & "贷方数量, " & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = " (SELECT KMDM as 科目代码,KMMC as 科目名称,sum(" & Me.lbSelectedGN.Items(i).ToString & "借方数量) as " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(" & Me.lbSelectedGN.Items(i).ToString & "借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(" & Me.lbSelectedGN.Items(i).ToString & "贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(" & Me.lbSelectedGN.Items(i).ToString & "贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额 from KMDM  LEFT JOIN (SELECT 科目DM,SUM(借方数量) AS " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额  FROM 库存表 where 功能名称='" & Me.lbSelectedGN.Items(i).ToString & "' " & sCaseFSE & "   group by 科目DM) as T" & Me.lbSelectedGN.Items(i).ToString & " on (charindex(t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM,kmdm.kmdm)=1)  WHERE HSLBKC='true' AND sfmj='false'" & sCaseKM & " group by kmdm,kmmc) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                End If

            Else
                If Me.lbSelectedGN.Items(i) = "期初" Then
                    '        sCaseKM = " and KMDM.Nian=" & Me.myMinDate.Value.Year & sCaseKMDM

                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量, " & Me.lbSelectedGN.Items(i).ToString & "借方金额,  " & Me.lbSelectedGN.Items(i).ToString & "贷方数量, " & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期初借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期初借方金额,0 as 期初贷方数量,0 as 期初贷方金额 from (select 科目代码,科目名称,sum(qcjfsl) as QCJFSL,SUM(QCJFJE) AS QCJFJE,SUM(QCDFSL) AS QCDFSL,SUM(QCDFJE) AS QCDFJE from (SELECT KMDM as 科目代码,KMMC as 科目名称,QCJFSL,QCJFJE,QCDFSL,QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<'" & Me.myMinDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期初 on (charindex(t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM,kmdm.kmdm)=1) WHERE HSLBKC='true' AND sfmj='false' " & sCaseKM & "  )  as tempT5 group by 科目代码,科目名称) as T期初发生,(select 科目代码,科目名称,sum(NCJFSL) AS NCJFSL ,SUM(NCJFYE) AS NCJFYE,SUM(NCDFSL) AS NCDFSL,SUM(NCDFYE) AS NCDFYE from (SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期初 on (t" & Me.lbSelectedGN.Items(i).ToString & ".科目代码=kmdm.kmdm) WHERE sfmj='false' " & sCaseKM & "  )  as tempT6 group by 科目代码,科目名称) AS TNCYE where T期初发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","
                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                ElseIf Me.lbSelectedGN.Items(i) = "期末" Then
                    '   sCaseKM = " and KMDM.Nian=" & Me.myMaxDate.Value.Year & sCaseKMDM

                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量, " & Me.lbSelectedGN.Items(i).ToString & "借方金额,  " & Me.lbSelectedGN.Items(i).ToString & "贷方数量, " & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期末借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期末借方金额,0 as 期末贷方数量,0 as 期末贷方金额 from (select 科目代码,科目名称,sum(qcjfsl) as QCJFSL,SUM(QCJFJE) AS QCJFJE,SUM(QCDFSL) AS QCDFSL,SUM(QCDFJE) AS QCDFJE from (SELECT KMDM as 科目代码,KMMC as 科目名称,QCJFSL,QCJFJE,QCDFSL,QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 年份>=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", " and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'") & " group by 科目DM) as T期末 on (charindex(t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM,kmdm.kmdm)=1) WHERE HSLBKC='true' AND sfmj='false' " & sCaseKM & "  )  as tempT7 group by 科目代码,科目名称) as T期末发生,(select 科目代码,科目名称,sum(NCJFSL) AS NCJFSL ,SUM(NCJFYE) AS NCJFYE,SUM(NCDFSL) AS NCDFSL,SUM(NCDFYE) AS NCDFYE from (SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 年份=" & Me.myMinDate.Value.Year & sCaseFSE.Replace(" and 日期>='" & Me.myMinDate.Value.ToShortDateString & "' and 日期<='" & Me.myMaxDate.Value.ToShortDateString & "'", "").Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " group by 科目代码) as T期末 on (t" & Me.lbSelectedGN.Items(i).ToString & ".科目代码=kmdm.kmdm) WHERE sfmj='false' " & sCaseKM & "  )  as tempT8 group by 科目代码,科目名称) AS TNCYE where T期末发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","
                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                Else

                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量, " & Me.lbSelectedGN.Items(i).ToString & "借方金额,  " & Me.lbSelectedGN.Items(i).ToString & "贷方数量, " & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT KMDM as 科目代码,KMMC as 科目名称,sum(" & Me.lbSelectedGN.Items(i).ToString & "借方数量) as " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(" & Me.lbSelectedGN.Items(i).ToString & "借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(" & Me.lbSelectedGN.Items(i).ToString & "贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(" & Me.lbSelectedGN.Items(i).ToString & "贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额 from KMDM  LEFT JOIN (SELECT 科目DM,SUM(借方数量) AS " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额  FROM 库存表 where 功能名称='" & Me.lbSelectedGN.Items(i).ToString & "' " & sCaseFSE & "   group by 科目DM) as T" & Me.lbSelectedGN.Items(i).ToString & " on (charindex(t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM,kmdm.kmdm)=1)  WHERE HSLBKC='true' AND sfmj='false'" & sCaseKM & " group by kmdm,kmmc) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "
                End If
            End If
        Next

        For i = 0 To Me.lbSelectedFZSX.Items.Count - 1

            sLie = sLie & Me.lbSelectedFZSX.Items(i).ToString & ","


            sTable = sTable & " (select KMDM as 科目代码," & Me.lbSelectedFZSX.Items(i).ToString & " from KMDM left join (select 科目代码,kmfzsx.NIAN,辅助属性值 as " & Me.lbSelectedFZSX.Items(i).ToString & " from KMFZSX where KMFZSX.辅助属性名称='" & Me.lbSelectedFZSX.Items(i).ToString & "' ) as T" & Me.lbSelectedFZSX.Items(i).ToString & " on KMDM.KMDM=T" & Me.lbSelectedFZSX.Items(i).ToString & ".科目代码 AND KMDM.NIAN=T" & Me.lbSelectedFZSX.Items(i).ToString & ".NIAN where  KMDM.HSLBKC='true' AND KMDM.sfmj='false' " & sCaseKM & " ) AS TTT" & Me.lbSelectedFZSX.Items(i).ToString & ","

            sJoin = sJoin & sJoin1 & "=TTT" & Me.lbSelectedFZSX.Items(i).ToString & ".科目代码 and "

        Next

        Dim sSQLFMJKM As String
        If Me.lbSelectedGN.Items.Count + Me.lbSelectedFZSX.Items.Count > 1 Then
            sSQLFMJKM = " select " & sLie.Remove(sLie.Length - 1, 1) & " from " & sTable.Remove(sTable.Length - 1, 1) & " where " & sJoin.Remove(sJoin.Length - 5, 5)
        Else
            sSQLFMJKM = " select " & sLie.Remove(sLie.Length - 1, 1) & " from " & sTable.Remove(sTable.Length - 1, 1)
        End If

        FillGNdgv(sSQL, sSQLFMJKM, sCaseFSE, sCaseKM, Me.myXJKM.Checked, Me.cbJinE.Checked)
        Return True

    End Function

    Private Sub FillGNdgv(ByVal sSqlMJKM As String, ByVal sSqlFMJKM As String, ByVal sCaseFSE As String, ByVal sCaseKM As String, ByVal bXSXJKM As Boolean, ByVal bXSJE As Boolean)
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim dsMJKM As DataSet
        dsMJKM = mdb.Reader(sSqlMJKM)

        '2015年4月份修改，不再计算非末级科目的发生额
        'Dim dsFMJKM As DataSet
        'dsFMJKM = mdb.Reader(sSqlFMJKM)

        'dsMJKM.Merge(dsFMJKM)


        Dim dvRow As DataRow
        dvRow = dsMJKM.Tables(0).NewRow

        dsMJKM.Tables(0).Rows.Add(dvRow)

        Dim iRow, iCol As Integer
        Dim dvHJ As New DataView
        dvHJ = dsMJKM.Tables(0).DefaultView


        dvHJ.RowFilter = " isMJKM='1'"

        '求一级科目到合计值
        For iCol = 3 To Me.lbSelectedGN.Items.Count * 4 + 1
            Dim dHJ As Decimal = 0
            For iRow = 0 To dvHJ.Count - 1
                If IsDBNull(dvHJ.Item(iRow).Item(iCol)) Then
                Else
                    dHJ = dHJ + dvHJ.Item(iRow).Item(iCol)
                End If
            Next
            dsMJKM.Tables(0).Rows(dsMJKM.Tables(0).Rows.Count - 1).Item(iCol) = dHJ
        Next
        dsMJKM.Tables(0).Rows(dsMJKM.Tables(0).Rows.Count - 1).Item(1) = "合计"
        dsMJKM.Tables(0).Rows(dsMJKM.Tables(0).Rows.Count - 1).Item(2) = ""

        dsMJKM.Tables(0).Columns.Remove("isMJKM")

        Dim dv As DataView
        dv = dsMJKM.Tables(0).DefaultView

        Dim sFTJ As String = ""
        If bXSXJKM Then
            '显示下级，无需特殊处理
            '在自定义报表到时候,可以均默认为显示下级科目
            sFTJ = ""
        Else
            '不显示下级
            Dim iKMLong As Integer
            If myMinKM.Text.Length > myMaxKM.Text.Length Then
                iKMLong = myMinKM.Text.Length
            Else
                iKMLong = myMaxKM.Text.Length
            End If
            sFTJ = " (len(科目代码) <= " & iKMLong & " or 科目名称='合计')"
        End If


        dv.RowFilter = sFTJ

        dv.Sort = "科目代码"
        Me.dgv.DataSource = dv


        ''不显示0
        Dim i1, n1 As Int16
        For i1 = 0 To dv.Count - 1
            For n1 = 2 To dsMJKM.Tables(0).Columns.Count - 1
                If dsMJKM.Tables(0).Columns(n1).DataType.Name = "String" Then
                    Continue For
                End If

                If IsDBNull(dv.Item(i1).Item(n1)) Then
                    Continue For
                End If

                If dv.Item(i1).Item(n1).ToString = "" OrElse dv.Item(i1).Item(n1) = 0 Then
                    dv.Item(i1).Item(n1) = DBNull.Value
                End If
            Next
        Next

        If bXSJE Then
            '不显示金额
            Dim i As Integer
            For i = 3 To Me.lbSelectedGN.Items.Count * 4 + 1 Step 2
                Me.dgv.Columns(i).Visible = False
            Next
        Else
            '显示所有科目，无需特殊处理
        End If

    End Sub

    Private Sub lbGongNeng_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbGongNeng.DoubleClick, btnAddSelected.Click
        If Me.lbGongNeng.SelectedIndex < 0 Then
            Exit Sub
        End If
        Me.lbSelectedGN.Items.Add(Me.lbGongNeng.SelectedItem)
        Me.lbGongNeng.Items.Remove(Me.lbGongNeng.SelectedItem)

        Me.btnSave.Enabled = False
    End Sub

    Private Sub lbSelectedGN_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectedGN.DoubleClick, btnRemoveSelect.Click
        If Me.lbSelectedGN.SelectedIndex < 0 Then
            Exit Sub
        End If
        Me.lbGongNeng.Items.Add(Me.lbSelectedGN.SelectedItem)
        Me.lbSelectedGN.Items.Remove(Me.lbSelectedGN.SelectedItem)

        Me.btnSave.Enabled = False
    End Sub


    Private Sub btnAddAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAll.Click
        Dim i As Integer
        For i = 0 To Me.lbGongNeng.Items.Count - 1
            Me.lbSelectedGN.Items.Add(Me.lbGongNeng.Items(i))
        Next

        Me.lbGongNeng.Items.Clear()
    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAll.Click
        Dim i As Integer
        For i = 0 To Me.lbSelectedGN.Items.Count - 1
            Me.lbGongNeng.Items.Add(Me.lbSelectedGN.Items(i))
        Next

        Me.lbSelectedGN.Items.Clear()
    End Sub


    Private Sub FillCangKu()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As New DataSet
        ds = mdb.Reader("select ID,Name,ShuoMing from CangkuInfo order by Name")
        Dim i As Integer

        Me.cbCangKu.Items.Add("所有仓库")
        Me.cbCangKuID.Items.Add(0)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.cbCangKu.Items.Add(ds.Tables(0).Rows(i).Item(1).ToString)
            Me.cbCangKuID.Items.Add(ds.Tables(0).Rows(i).Item(0))
        Next

        If Me.cbCangKu.Items.Count > 0 Then
            Me.cbCangKu.SelectedIndex = 0
        End If
    End Sub

    Private Sub cbCangKu_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCangKu.SelectedIndexChanged, cbDYKM.SelectedIndexChanged
        If IsNothing(Me.cbCangKu.SelectedIndex) = False Then
            Me.cbCangKuID.SelectedIndex = Me.cbCangKu.SelectedIndex
        End If

        If Me.cbCangKu.SelectedIndex = 0 Then
            Me.myWCK.Enabled = True
            Me.myFPK.Enabled = True
        Else
            Me.myWCK.Enabled = False
            Me.myFPK.Enabled = False
        End If
    End Sub

    Private Sub myKMDM_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles myMinKM.KeyUp, myMaxKM.KeyUp

        bValided = False
    End Sub

    Dim bValided As Boolean
    Private Sub myKMDM_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles myMinKM.MouseDown, myMaxKM.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            getKMDM(sender)
        End If
    End Sub

    Private Sub myKMDM_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles myMinKM.Validating, myMaxKM.Validating
        If bValided Then
            Exit Sub
        End If

        If CType(sender, Windows.Forms.TextBox).Text = "" Then
        Else
            If FKF.FKF.YanZhengKMDM(CType(sender, Windows.Forms.TextBox).Text) = True Then
            Else
                getKMDM(sender)
            End If
        End If
    End Sub

    Private Sub getKMDM(ByVal sender As Object)
        Dim dlg As New FKKM.FKXKM("HSLBKC='true'", False)
        dlg.sKey = CType(sender, Windows.Forms.TextBox).Text
        dlg.ShowDialog()
        CType(sender, Windows.Forms.TextBox).Text = FKF.FKF.getKMDMfromQM(dlg.myKM.sKMDM)

        bValided = True
    End Sub

    Private Sub lbFZSX_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbFZSX.DoubleClick
        If Me.lbFZSX.SelectedIndex < 0 Then
            Exit Sub
        Else
            Me.lbSelectedFZSX.Items.Add(Me.lbFZSX.SelectedItem.ToString)
            Me.lbFZSX.Items.Remove(Me.lbFZSX.SelectedItem)

            Me.btnSave.Enabled = False
        End If
    End Sub

    Private Sub lbSelectedFZSX_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectedFZSX.DoubleClick
        If Me.lbSelectedFZSX.SelectedIndex < 0 Then
            Exit Sub
        Else
            Me.lbFZSX.Items.Add(Me.lbSelectedFZSX.SelectedItem.ToString)
            Me.lbSelectedFZSX.Items.Remove(Me.lbSelectedFZSX.SelectedItem)

            Me.btnSave.Enabled = False
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Me.lbSelectedGN.Items.Count = 0 Then
            Exit Sub
        End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        Dim i As Integer

        Dim sLie As String = ""
        Dim sTable As String = ""
        Dim sJoin1 As String = ""
        Dim sJoin As String = ""
        Dim sCaseFSE As String = ""
        Dim sCaseKM As String = ""

        '设定仓库
        If Me.cbCangKuID.SelectedIndex = 0 Then
            Dim dsKuCase As DataSet
            If myWCK.Checked Then
                dsKuCase = mdb.Reader("select ID from CangKuInfo where shuoming='外存库' ")
                'Dim i As Integer
                For i = 0 To dsKuCase.Tables(0).Rows.Count - 1
                    sCaseFSE = sCaseFSE & " and 仓库<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                Next
            Else

            End If

            If myFPK.Checked Then
                dsKuCase = mdb.Reader("select ID from CangKuInfo where shuoming='废品库' ")
                'Dim i As Integer
                For i = 0 To dsKuCase.Tables(0).Rows.Count - 1
                    sCaseFSE = sCaseFSE & " and 仓库<>" & dsKuCase.Tables(0).Rows(i).Item(0)
                Next
            Else

            End If

        Else
            sCaseFSE = " and  仓库=" & Me.cbCangKuID.SelectedItem.ToString
        End If

        sCaseKM = " and KMDM.Nian= FK_QCQM_YHDYKMDMNFCASE  and KMDM.KMDM >= '" & Me.myMinKM.Text & "' and KMDM.KMDM < '" & (CType(Me.myMaxKM.Text, Integer) + 1).ToString & "' "
        sCaseFSE = sCaseFSE & " and 科目DM >= '" & Me.myMinKM.Text & "' and  科目DM < '" & (CType(Me.myMaxKM.Text, Integer) + 1).ToString & "' "

        For i = 0 To Me.lbSelectedGN.Items.Count - 1
            If i = 0 Then
                If Me.lbSelectedGN.Items(i) = "期初" Then
                    sLie = " '1' as isMJKM,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"


                    sTable = " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期初借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期初借方金额,0 as 期初贷方数量,0 as 期初贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 1=1 " & sCaseFSE & " FK_QCQM_QC_YHDYCASE group by 科目DM) as T期初 on kmdm.kmdm=t期初.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期初发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 1=1 " & sCaseFSE.Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " FK_QCQM_QC_YHDYNFCASE group by 科目代码) as T期初 on kmdm.kmdm=t期初.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期初发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                ElseIf Me.lbSelectedGN.Items(i) = "期末" Then
                    sLie = " '1' as isMJKM,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"


                    sTable = " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期末借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期末借方金额,0 as 期末贷方数量,0 as 期末贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 1=1 " & sCaseFSE & " FK_QCQM_QM_YHDYCASE group by 科目DM) as T期末 on kmdm.kmdm=t期末.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期末发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 1=1 " & sCaseFSE.Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " FK_QCQM_QC_YHDYNFCASE group by 科目代码) as T期末 on kmdm.kmdm=t期末.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期末发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                Else
                    sLie = " '1' as isMJKM,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码,TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = " (SELECT KMDM as 科目代码,KMMC as 科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额 from KMDM  LEFT JOIN (SELECT 科目DM,SUM(借方数量) AS " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额  FROM 库存表 where 功能名称='" & Me.lbSelectedGN.Items(i).ToString & "' " & sCaseFSE & " FK_YHDYCASE  group by 科目DM) as T" & Me.lbSelectedGN.Items(i).ToString & " on kmdm.kmdm=t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM WHERE HSLBKC='true' AND SFMJ='true'" & sCaseKM & "  ) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin1 = "TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码"

                End If

            Else
                If Me.lbSelectedGN.Items(i) = "期初" Then
                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"


                    sTable = sTable & " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期初借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期初借方金额,0 as 期初贷方数量,0 as 期初贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 1=1 " & sCaseFSE & " FK_QCQM_QC_YHDYCASE group by 科目DM) as T期初 on kmdm.kmdm=t期初.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期初发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 1=1 " & sCaseFSE.Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " FK_QCQM_QC_YHDYNFCASE group by 科目代码) as T期初 on kmdm.kmdm=t期初.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期初发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","
                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                ElseIf Me.lbSelectedGN.Items(i) = "期末" Then
                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT TNCYE.科目代码,TNCYE.科目名称,QCJFSL+ncjfsl-QCDFSL-ncdfsl AS 期末借方数量,QCJFJE+ncjfye-QCDFJE-ncdfye as 期末借方金额,0 as 期末贷方数量,0 as 期末贷方金额 from (SELECT KMDM as 科目代码,KMMC as 科目名称,isnull(QCJFSL,0) as QCJFSL,isnull(QCJFJE,0) as QCJFJE,isnull(QCDFSL,0) as QCDFSL,isnull(QCDFJE,0) as QCDFJE from KMDM LEFT JOIN (SELECT 科目DM,isNull( SUM(借方数量) , 0 ) AS QCJFSL,isNull( sum(借方金额) , 0 ) as QCJFJE,isNull( sum(贷方数量) , 0 ) as QCDFSL,isNull( sum(贷方金额) , 0 ) as QCDFJE FROM 库存表 where 1=1 " & sCaseFSE & " FK_QCQM_QM_YHDYCASE group by 科目DM) as T期末 on kmdm.kmdm=t期末.科目DM WHERE HSLBKC='true' AND SFMJ='true' " & sCaseKM & "  ) as T期末发生,(SELECT KMDM as 科目代码,KMMC as 科目名称,NCJFSL,NCJFYE,NCDFSL,NCDFYE from KMDM LEFT JOIN (SELECT 科目代码,isNull( SUM(年初借方数量) , 0 ) AS NCJFSL,isNull( sum(年初借方余额) , 0 ) as NCJFYE,isNull( sum(年初贷方数量) , 0 ) as NCDFSL,isNull( sum(年初贷方余额) , 0 ) as NCDFYE FROM 库存余额表 where 1=1 " & sCaseFSE.Replace("仓库", "仓库编号").Replace("科目DM", "科目代码") & " FK_QCQM_QC_YHDYNFCASE group by 科目代码) as T期末 on kmdm.kmdm=t期末.科目代码 WHERE SFMJ='true' " & sCaseKM & "  ) AS TNCYE where T期末发生.科目代码=TNCYE.科目代码) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                Else
                    sLie = sLie & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额,"

                    sTable = sTable & " (SELECT KMDM as 科目代码,KMMC as 科目名称," & Me.lbSelectedGN.Items(i).ToString & "借方数量," & Me.lbSelectedGN.Items(i).ToString & "借方金额," & Me.lbSelectedGN.Items(i).ToString & "贷方数量," & Me.lbSelectedGN.Items(i).ToString & "贷方金额 from KMDM  LEFT JOIN (SELECT 科目DM,SUM(借方数量) AS " & Me.lbSelectedGN.Items(i).ToString & "借方数量,sum(借方金额) as " & Me.lbSelectedGN.Items(i).ToString & "借方金额,sum(贷方数量) as " & Me.lbSelectedGN.Items(i).ToString & "贷方数量,sum(贷方金额) as " & Me.lbSelectedGN.Items(i).ToString & "贷方金额  FROM 库存表 where 功能名称='" & Me.lbSelectedGN.Items(i).ToString & "' " & sCaseFSE & " FK_YHDYCASE   group by 科目DM) as T" & Me.lbSelectedGN.Items(i).ToString & " on kmdm.kmdm=t" & Me.lbSelectedGN.Items(i).ToString & ".科目DM WHERE HSLBKC='true' AND SFMJ='true'" & sCaseKM & "  ) as TTT" & Me.lbSelectedGN.Items(i).ToString & ","

                    sJoin = sJoin & sJoin1 & "= TTT" & Me.lbSelectedGN.Items(i).ToString & ".科目代码 and "

                End If
            End If
        Next

        For i = 0 To Me.lbSelectedFZSX.Items.Count - 1
            'If i = 0 Then
            sLie = sLie & Me.lbSelectedFZSX.Items(i).ToString & ","

            sTable = sTable & " (select KMDM as 科目代码," & Me.lbSelectedFZSX.Items(i).ToString & " from KMDM left join (select 科目代码,NIAN,辅助属性值 as " & Me.lbSelectedFZSX.Items(i).ToString & " from KMFZSX where KMFZSX.辅助属性名称='" & Me.lbSelectedFZSX.Items(i).ToString & "' ) as T" & Me.lbSelectedFZSX.Items(i).ToString & " on KMDM.KMDM=T" & Me.lbSelectedFZSX.Items(i).ToString & ".科目代码 AND KMDM.NIAN=T" & Me.lbSelectedFZSX.Items(i).ToString & ".NIAN where  KMDM.HSLBKC='true' AND KMDM.SFMJ='true' " & sCaseKM & " ) AS TTT" & Me.lbSelectedFZSX.Items(i).ToString & ","


            sJoin = sJoin & sJoin1 & "=TTT" & Me.lbSelectedFZSX.Items(i).ToString & ".科目代码 and "
            'End If
        Next

        Dim sSQL As String
        If Me.lbSelectedGN.Items.Count > 1 OrElse Me.lbSelectedFZSX.Items.Count > 0 Then
            sSQL = " select " & sLie.Remove(sLie.Length - 1, 1) & " from " & sTable.Remove(sTable.Length - 1, 1) & " where " & sJoin.Remove(sJoin.Length - 5, 5)
        Else
            sSQL = " select " & sLie.Remove(sLie.Length - 1, 1) & " from " & sTable.Remove(sTable.Length - 1, 1)
        End If

        'MsgBox(sSQL)
        Dim sSQLFMJKM As String = ""

        Dim dlg As New FKMXBName
        Dim sMXB As String
        Dim sMXBShuoMing As String
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sMXB = dlg.txtMXBiao.Text
            If sMXB = "" Then
                MsgBox("报表名称不能为空!", MsgBoxStyle.Information, "提示")
                Exit Sub
            End If
            sMXBShuoMing = dlg.txtShuoMing.Text
        Else
            Exit Sub
        End If

        Dim sLieSetup As String = ""
        'Dim i As Integer
        For i = 0 To dgv.Columns.Count - 1
            sLieSetup = sLieSetup & CType(Me.dgv.Columns(i).Visible, Integer) & "," & Me.dgv.Columns(i).HeaderText & "," & Int(Me.dgv.Columns(i).Width * 100 / Me.dgv.Width) & "," & Me.dgv.Columns(i).DefaultCellStyle.Format & ","
        Next
        sLieSetup = sLieSetup.Remove(sLieSetup.Length - 1, 1)

        sSQL = sSQL.Replace("'", Chr(34))
        sSQLFMJKM = sSQLFMJKM.Replace("'", Chr(34))

        If mdb.bExsit("select * from ZDYBB WHERE CONTEXT='" & sMXB & "'") Then
            '同名报表已存在
            If MsgBox("同名报表已经存在,是否覆盖?", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then
                '保存报表
                mdb.Write("delete from ZDYBB WHERE CONTEXT='" & sMXB & "'")
                mdb.Write("Insert into ZDYBB (ConText,ParentID,[Index],TiShi,isBaoBiao,isMingXi,Biao,Lie,MRTiaoJian,LieSetup) VALUES ('" & sMXB & "',1,1,'" & sMXBShuoMing & "','true','False','库存表','" & sSQL & "','" & sSQLFMJKM & "','" & sLieSetup & "')")
            Else
                Exit Sub
            End If
        Else
            '保存报表
            mdb.Write("Insert into ZDYBB (ConText,ParentID,[Index],TiShi,isBaoBiao,isMingXi,Biao,Lie,MRTiaoJian,LieSetup) VALUES ('" & sMXB & "',1,1,'" & sMXBShuoMing & "','true','False','库存表','" & sSQL & "','" & sSQLFMJKM & "','" & sLieSetup & "')")
        End If

        Dim dlgTJ As New FKBBCS.FKBBCS(sMXB, "库存表")
        dlgTJ.ShowDialog()

    End Sub

    Private Sub btnLie_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLie.Click
        Dim dlg As New FKXSLie.LieSetup(Me.dgv, True, False, "")
        dlg.ShowDialog()
    End Sub
End Class

