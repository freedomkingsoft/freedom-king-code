﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKGNCX
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.btnOK = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.cbCangKuID = New System.Windows.Forms.ComboBox
        Me.lbSelectedGN = New System.Windows.Forms.ListBox
        Me.lbGongNeng = New System.Windows.Forms.ListBox
        Me.btnRemoveSelect = New System.Windows.Forms.Button
        Me.btnRemoveAll = New System.Windows.Forms.Button
        Me.cbCangKu = New System.Windows.Forms.ComboBox
        Me.cbJinE = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.myWCK = New System.Windows.Forms.CheckBox
        Me.myFPK = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.myXJKM = New System.Windows.Forms.CheckBox
        Me.myMinDate = New System.Windows.Forms.DateTimePicker
        Me.myMaxDate = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.myMinKM = New System.Windows.Forms.TextBox
        Me.myMaxKM = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lbFZSX = New System.Windows.Forms.ListBox
        Me.lbSelectedFZSX = New System.Windows.Forms.ListBox
        Me.btnLie = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.cbDYKM = New System.Windows.Forms.ComboBox
        Me.btnAddSelected = New System.Windows.Forms.Button
        Me.btnAddAll = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(12, 188)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowTemplate.Height = 23
        Me.dgv.Size = New System.Drawing.Size(998, 346)
        Me.dgv.TabIndex = 0
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(866, 147)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(63, 28)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "查询"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 12
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.15152!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.15152!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.15152!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.15152!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.15152!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.12121!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.12121!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.cbCangKuID, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.lbSelectedGN, 7, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lbGongNeng, 5, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btnRemoveSelect, 6, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.btnRemoveAll, 6, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.cbCangKu, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cbJinE, 3, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.myWCK, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.myFPK, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.myXJKM, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.myMinDate, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.myMaxDate, 3, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.myMinKM, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.myMaxKM, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 8, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 9, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lbFZSX, 8, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lbSelectedFZSX, 9, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btnOK, 10, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.btnLie, 10, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.btnSave, 10, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cbDYKM, 2, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAddSelected, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAddAll, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 7, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1028, 182)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'cbCangKuID
        '
        Me.cbCangKuID.FormattingEnabled = True
        Me.cbCangKuID.Location = New System.Drawing.Point(3, 147)
        Me.cbCangKuID.Name = "cbCangKuID"
        Me.cbCangKuID.Size = New System.Drawing.Size(14, 20)
        Me.cbCangKuID.TabIndex = 21
        Me.cbCangKuID.Visible = False
        '
        'lbSelectedGN
        '
        Me.lbSelectedGN.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbSelectedGN.FormattingEnabled = True
        Me.lbSelectedGN.ItemHeight = 12
        Me.lbSelectedGN.Location = New System.Drawing.Point(561, 39)
        Me.lbSelectedGN.Name = "lbSelectedGN"
        Me.TableLayoutPanel1.SetRowSpan(Me.lbSelectedGN, 4)
        Me.lbSelectedGN.Size = New System.Drawing.Size(111, 136)
        Me.lbSelectedGN.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.lbSelectedGN, "双击删除选定功能")
        '
        'lbGongNeng
        '
        Me.lbGongNeng.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbGongNeng.FormattingEnabled = True
        Me.lbGongNeng.ItemHeight = 12
        Me.lbGongNeng.Location = New System.Drawing.Point(399, 39)
        Me.lbGongNeng.Name = "lbGongNeng"
        Me.TableLayoutPanel1.SetRowSpan(Me.lbGongNeng, 4)
        Me.lbGongNeng.Size = New System.Drawing.Size(111, 136)
        Me.lbGongNeng.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.lbGongNeng, "双击增加功能")
        '
        'btnRemoveSelect
        '
        Me.btnRemoveSelect.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnRemoveSelect.Location = New System.Drawing.Point(517, 120)
        Me.btnRemoveSelect.Name = "btnRemoveSelect"
        Me.btnRemoveSelect.Size = New System.Drawing.Size(36, 21)
        Me.btnRemoveSelect.TabIndex = 9
        Me.btnRemoveSelect.Text = "<-"
        Me.ToolTip1.SetToolTip(Me.btnRemoveSelect, "删除选定项")
        Me.btnRemoveSelect.UseVisualStyleBackColor = True
        '
        'btnRemoveAll
        '
        Me.btnRemoveAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnRemoveAll.Location = New System.Drawing.Point(517, 158)
        Me.btnRemoveAll.Name = "btnRemoveAll"
        Me.btnRemoveAll.Size = New System.Drawing.Size(36, 21)
        Me.btnRemoveAll.TabIndex = 9
        Me.btnRemoveAll.Text = "<<<"
        Me.ToolTip1.SetToolTip(Me.btnRemoveAll, "删除全部已选项")
        Me.btnRemoveAll.UseVisualStyleBackColor = True
        '
        'cbCangKu
        '
        Me.cbCangKu.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cbCangKu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbCangKu.FormattingEnabled = True
        Me.cbCangKu.Location = New System.Drawing.Point(140, 44)
        Me.cbCangKu.Name = "cbCangKu"
        Me.cbCangKu.Size = New System.Drawing.Size(111, 20)
        Me.cbCangKu.TabIndex = 11
        '
        'cbJinE
        '
        Me.cbJinE.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cbJinE.AutoSize = True
        Me.cbJinE.Checked = True
        Me.cbJinE.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbJinE.Location = New System.Drawing.Point(270, 155)
        Me.cbJinE.Name = "cbJinE"
        Me.cbJinE.Size = New System.Drawing.Size(84, 16)
        Me.cbJinE.TabIndex = 5
        Me.cbJinE.Text = "不显示金额"
        Me.cbJinE.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(257, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 12)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "设定起止日期:"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(140, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 12)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "设定仓库:"
        '
        'myWCK
        '
        Me.myWCK.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myWCK.AutoSize = True
        Me.myWCK.Checked = True
        Me.myWCK.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myWCK.Location = New System.Drawing.Point(147, 82)
        Me.myWCK.Name = "myWCK"
        Me.myWCK.Size = New System.Drawing.Size(96, 16)
        Me.myWCK.TabIndex = 12
        Me.myWCK.Text = "不包括外存库"
        Me.myWCK.UseVisualStyleBackColor = True
        '
        'myFPK
        '
        Me.myFPK.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myFPK.AutoSize = True
        Me.myFPK.Checked = True
        Me.myFPK.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myFPK.Location = New System.Drawing.Point(147, 118)
        Me.myFPK.Name = "myFPK"
        Me.myFPK.Size = New System.Drawing.Size(96, 16)
        Me.myFPK.TabIndex = 13
        Me.myFPK.Text = "不包括废品库"
        Me.myFPK.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 12)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "设定科目:"
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(70, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(17, 12)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "至"
        '
        'myXJKM
        '
        Me.myXJKM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myXJKM.AutoSize = True
        Me.myXJKM.Checked = True
        Me.myXJKM.CheckState = System.Windows.Forms.CheckState.Checked
        Me.myXJKM.Location = New System.Drawing.Point(30, 155)
        Me.myXJKM.Name = "myXJKM"
        Me.myXJKM.Size = New System.Drawing.Size(96, 16)
        Me.myXJKM.TabIndex = 18
        Me.myXJKM.Text = "显示下级科目"
        Me.myXJKM.UseVisualStyleBackColor = True
        '
        'myMinDate
        '
        Me.myMinDate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMinDate.Location = New System.Drawing.Point(257, 43)
        Me.myMinDate.Name = "myMinDate"
        Me.myMinDate.Size = New System.Drawing.Size(111, 21)
        Me.myMinDate.TabIndex = 19
        '
        'myMaxDate
        '
        Me.myMaxDate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMaxDate.Location = New System.Drawing.Point(257, 115)
        Me.myMaxDate.Name = "myMaxDate"
        Me.myMaxDate.Size = New System.Drawing.Size(111, 21)
        Me.myMaxDate.TabIndex = 20
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(304, 84)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(17, 12)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "至"
        '
        'myMinKM
        '
        Me.myMinKM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMinKM.Location = New System.Drawing.Point(31, 43)
        Me.myMinKM.Name = "myMinKM"
        Me.myMinKM.Size = New System.Drawing.Size(95, 21)
        Me.myMinKM.TabIndex = 15
        '
        'myMaxKM
        '
        Me.myMaxKM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.myMaxKM.Location = New System.Drawing.Point(31, 115)
        Me.myMaxKM.Name = "myMaxKM"
        Me.myMaxKM.Size = New System.Drawing.Size(95, 21)
        Me.myMaxKM.TabIndex = 16
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(678, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 12)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "可选辅助属性:"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(772, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 12)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "已选辅助属性:"
        '
        'lbFZSX
        '
        Me.lbFZSX.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbFZSX.FormattingEnabled = True
        Me.lbFZSX.ItemHeight = 12
        Me.lbFZSX.Location = New System.Drawing.Point(678, 39)
        Me.lbFZSX.Name = "lbFZSX"
        Me.TableLayoutPanel1.SetRowSpan(Me.lbFZSX, 4)
        Me.lbFZSX.Size = New System.Drawing.Size(88, 136)
        Me.lbFZSX.TabIndex = 23
        Me.ToolTip1.SetToolTip(Me.lbFZSX, "双击项目增加选择")
        '
        'lbSelectedFZSX
        '
        Me.lbSelectedFZSX.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbSelectedFZSX.FormattingEnabled = True
        Me.lbSelectedFZSX.ItemHeight = 12
        Me.lbSelectedFZSX.Location = New System.Drawing.Point(772, 39)
        Me.lbSelectedFZSX.Name = "lbSelectedFZSX"
        Me.TableLayoutPanel1.SetRowSpan(Me.lbSelectedFZSX, 4)
        Me.lbSelectedFZSX.Size = New System.Drawing.Size(88, 136)
        Me.lbSelectedFZSX.TabIndex = 24
        Me.ToolTip1.SetToolTip(Me.lbSelectedFZSX, "双击项目删除选择")
        '
        'btnLie
        '
        Me.btnLie.Location = New System.Drawing.Point(866, 111)
        Me.btnLie.Name = "btnLie"
        Me.btnLie.Size = New System.Drawing.Size(63, 28)
        Me.btnLie.TabIndex = 1
        Me.btnLie.Text = "列设置"
        Me.btnLie.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(866, 39)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(63, 28)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "保存报表"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cbDYKM
        '
        Me.cbDYKM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cbDYKM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbDYKM.FormattingEnabled = True
        Me.cbDYKM.Location = New System.Drawing.Point(140, 153)
        Me.cbDYKM.Name = "cbDYKM"
        Me.cbDYKM.Size = New System.Drawing.Size(111, 20)
        Me.cbDYKM.TabIndex = 11
        '
        'btnAddSelected
        '
        Me.btnAddSelected.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnAddSelected.Location = New System.Drawing.Point(517, 75)
        Me.btnAddSelected.Name = "btnAddSelected"
        Me.btnAddSelected.Size = New System.Drawing.Size(36, 21)
        Me.btnAddSelected.TabIndex = 9
        Me.btnAddSelected.Text = "->"
        Me.ToolTip1.SetToolTip(Me.btnAddSelected, "增加选定项")
        Me.btnAddSelected.UseVisualStyleBackColor = True
        '
        'btnAddAll
        '
        Me.btnAddAll.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnAddAll.Location = New System.Drawing.Point(517, 39)
        Me.btnAddAll.Name = "btnAddAll"
        Me.btnAddAll.Size = New System.Drawing.Size(36, 21)
        Me.btnAddAll.TabIndex = 9
        Me.btnAddAll.Text = ">>>"
        Me.ToolTip1.SetToolTip(Me.btnAddAll, "增加全部项")
        Me.btnAddAll.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(399, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 12)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "可选功能"
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(561, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 12)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "已选功能"
        '
        'FKGNCX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 545)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.dgv)
        Me.Name = "FKGNCX"
        Me.Text = "按功能统计发生额"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbJinE As System.Windows.Forms.CheckBox
    Friend WithEvents lbGongNeng As System.Windows.Forms.ListBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lbSelectedGN As System.Windows.Forms.ListBox
    Friend WithEvents btnAddAll As System.Windows.Forms.Button
    Friend WithEvents btnAddSelected As System.Windows.Forms.Button
    Friend WithEvents btnRemoveSelect As System.Windows.Forms.Button
    Friend WithEvents btnRemoveAll As System.Windows.Forms.Button
    Friend WithEvents cbCangKu As System.Windows.Forms.ComboBox
    Friend WithEvents myWCK As System.Windows.Forms.CheckBox
    Friend WithEvents myFPK As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents myMaxKM As System.Windows.Forms.TextBox
    Friend WithEvents myMinKM As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents myXJKM As System.Windows.Forms.CheckBox
    Friend WithEvents myMinDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents myMaxDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnLie As System.Windows.Forms.Button
    Friend WithEvents cbCangKuID As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lbFZSX As System.Windows.Forms.ListBox
    Friend WithEvents lbSelectedFZSX As System.Windows.Forms.ListBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cbDYKM As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
End Class
