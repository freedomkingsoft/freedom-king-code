﻿Public Class Restore

    Private Sub Restore_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        fillFileList()
    End Sub

    Private Sub fillFileList()
        Dim sF As String
        sF = My.Application.Info.DirectoryPath & "\FKDataBackup"

        Dim sFiles() As String
        sFiles = IO.Directory.GetFiles(sF & "\HourBackup")

        Me.DataGridView1.RowCount = sFiles.Length
        Dim i As Integer
        For i = 0 To sFiles.Length - 1
            Me.DataGridView1.Rows(i).Cells(1).Value = sFiles(i).Replace(sF & "\HourBackup\", "")
            Me.DataGridView1.Rows(i).Cells(2).Value = IO.File.GetLastWriteTime(sFiles(i))
        Next
        Me.DataGridView1.Sort(Me.DataGridView1.Columns(2), ComponentModel.ListSortDirection.Descending)
    End Sub

    Private Sub ComboBox1_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.DropDown
        Dim dlg As New System.Windows.Forms.OpenFileDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.ComboBox1.Text = dlg.FileName
        End If
    End Sub

   
    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 0 Then
            If Me.DataGridView1.Rows(e.RowIndex).Cells(0).Value = True Then
                Me.DataGridView1.Rows(e.RowIndex).Cells(0).Value = False

                Me.ComboBox1.Enabled = True
            Else
                Dim i As Integer
                For i = 0 To Me.DataGridView1.RowCount - 1
                    Me.DataGridView1.Rows(i).Cells(0).Value = False
                Next
                Me.DataGridView1.Rows(e.RowIndex).Cells(0).Value = True
                Me.ComboBox1.Enabled = False
            End If
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If System.Windows.Forms.MessageBox.Show("你确定要恢复备份数据吗，此操作可能导致最近输入的数据丢失，请确认一下！", "确实要恢复吗？", Windows.Forms.MessageBoxButtons.YesNoCancel, Windows.Forms.MessageBoxIcon.Warning, Windows.Forms.MessageBoxDefaultButton.Button2, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly, False) = Windows.Forms.DialogResult.Yes Then
            If System.Windows.Forms.MessageBox.Show("再问一遍，你确定要恢复备份数据吗，此操作可能导致最近输入的数据丢失，请再确认一下！", "确实要恢复吗？", Windows.Forms.MessageBoxButtons.YesNoCancel, Windows.Forms.MessageBoxIcon.Warning, Windows.Forms.MessageBoxDefaultButton.Button2, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly, False) = Windows.Forms.DialogResult.Yes Then

                Dim sPathofYuan As String
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                sPathofYuan = mdb.oleConnect.DataSource

                If Me.ComboBox1.Enabled = True Then
                    '恢复指定的文件
                    Dim sfile As String = Me.ComboBox1.Text
                    Try
                        IO.File.Copy(sfile, sPathofYuan, True)
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                        Exit Sub
                    End Try
                    MsgBox("数据恢复完成，请检查并补充丢失的数据", MsgBoxStyle.OkOnly, "提示")
                    MsgBox("系统马上要进行重新启动，请将所有正在运行的系统重新启动！", MsgBoxStyle.OkOnly, "提示")
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                Else
                    '恢复自动备份的数据
                    Dim sfile As String
                    Dim i As Integer
                    For i = 0 To Me.DataGridView1.RowCount - 1
                        If Me.DataGridView1.Rows(i).Cells(0).Value = True Then
                            sfile = My.Application.Info.DirectoryPath & "\FKDataBackup\HourBackup\" & Me.DataGridView1.Rows(i).Cells(1).Value
                            IO.File.Copy(sfile, sPathofYuan, True)
                            MsgBox("数据恢复完成，请检查并补充丢失的数据！", MsgBoxStyle.OkOnly, "提示")
                            MsgBox("系统马上要进行重新启动，请将所有正在运行的系统重新启动！", MsgBoxStyle.OkOnly, "提示")
                            Me.DialogResult = Windows.Forms.DialogResult.OK
                            Exit Sub
                        End If
                    Next
                End If
            End If
        End If
    End Sub

End Class