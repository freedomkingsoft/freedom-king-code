using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Excel;  

namespace FKOUT
{
    public class FKEXCEL123
   {
        private Microsoft.Office.Interop.Excel.Application excel;
       System.Windows.Forms.DataGridView myDGV;
        public bool DataGridviewShowToExcel(System.Windows.Forms.DataGridView dgv, bool isShowExcle)
        {
            myDGV = dgv;
            if (myDGV.Rows.Count == 0)
                return false;
            //建立Excel对象    
            excel = new Microsoft.Office.Interop.Excel.Application();
            excel.Application.Workbooks.Add(true);
            excel.Visible = false;

            //填充数据    
            int iCol = 1;
            for (int iColDgv = 0; iColDgv < myDGV.ColumnCount; iColDgv++)
            {
                if (myDGV.Columns[iColDgv].Visible == true)
                {
                    excel.Cells[1, iCol] = myDGV.Columns[iColDgv].Name;
                    iCol++;
                }
            }

            iCol = 1;
            for (int iColDgv = 0; iColDgv < myDGV.ColumnCount; iColDgv++)
            {
                if (dgv.Columns[iColDgv].Visible == true)
                {
                    for (int iRowDgv = 0; iRowDgv < myDGV.RowCount; iRowDgv++)
                    {
                        if (myDGV.Rows[iRowDgv].Cells[iColDgv].ValueType == typeof(string))
                            excel.Cells[iRowDgv + 2, iCol] = "'" + myDGV.Rows[iRowDgv].Cells[iColDgv].Value;
                        else
                            excel.Cells[iRowDgv + 2, iCol] = myDGV.Rows[iRowDgv].Cells[iColDgv].Value;
                    }
                    iCol++;
                }
            }

            excel.Visible = isShowExcle;
            
            return true;
        }

     }
}
