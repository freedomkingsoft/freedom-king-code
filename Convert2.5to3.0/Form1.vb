Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.TextBox2.Text = "" OrElse Me.TextBox1.Text = "" Then
            MsgBox("请安装要求指定文件")
            Exit Sub
        End If
        If IO.File.Exists(Me.TextBox1.Text) = False Then
            MsgBox("你指定的源文件不存在")
            Exit Sub
        End If

        If IO.File.Exists(Me.TextBox2.Text) Then
            MsgBox("你指定的目标文件已经存在,请重新指定文件名!")
            Exit Sub
        End If
        'Dim sConV2 As String
        Dim sConV3 As String
        'sConV2 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\FreedomKing\FreedomKingData2.mdb;Mode=Read;Jet OLEDB:Database Password='Zhao04Dan03Tong09'"
        sConV3 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Me.TextBox2.Text & ";Mode=ReadWrite;Jet OLEDB:Database Password='Zh!o04#Dn$3Tng09#'"

        'Dim mdb2 As New MDBReadWrite.MDBReadWrite(sConV2)
        'Dim mdb3 As New MDBReadWrite.MDBReadWrite(sConV3)

        Dim oleConnect As New OleDb.OleDbConnection()
        Dim oleDataAdapter As New OleDb.OleDbDataAdapter()
        Dim oleCommand As New OleDb.OleDbCommand()

        oleConnect.ConnectionString = sConV3
        oleCommand.Connection = oleConnect
        oleDataAdapter.SelectCommand = oleCommand

        '第一步,文件复制,将V3.0数据库清空,并复制到指定位置
        IO.File.Copy(My.Application.Info.DirectoryPath & "\FreedomKingData3.mdb", Me.TextBox2.Text)

        Dim sExTable As String = ""
        Try
            Dim dsTables As New DataTable

            If oleConnect.State = ConnectionState.Open Then
            Else
                oleConnect.Open()
            End If

            dsTables = oleConnect.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, "TABLE"})
            Me.DataGridView1.DataSource = dsTables

            '.Reader("select name from MsysObjects where type=1")
            '第二步, 清除目标数据库中所有数据, 包括基本信息
            Dim I As Integer
            For I = 0 To dsTables.Rows.Count - 1
                oleCommand.CommandText = "DELETE FROM " & dsTables.Rows(I).Item(2)
                oleCommand.ExecuteNonQuery()
            Next

            '第三步,进行实际数据的导入
            For I = 0 To dsTables.Rows.Count - 1

                oleCommand.CommandText = "insert into " & dsTables.Rows(I).Item(2) & " select  * from [';database=" & Me.TextBox1.Text & ";pwd=Zhao04Dan03Tong09']." & dsTables.Rows(I).Item(2)
                sExTable = dsTables.Rows(I).Item(2)
                oleCommand.ExecuteNonQuery()
            Next

            MsgBox("导入操作成功!")
        Catch ex As Exception
            MsgBox(sExTable)
            MsgBox(ex.ToString)
            MsgBox("导入操作没有成功,请检查原因并重新运行导入!")
        Finally
            oleConnect.Close()
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.TextBox1.Text = Me.OpenFileDialog1.FileName
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            If IO.File.Exists(Me.SaveFileDialog1.FileName) Then
                MsgBox("你指定的文件已经存在,请重新指定文件名!")
                Exit Sub
            End If
            Me.TextBox2.Text = Me.SaveFileDialog1.FileName
        End If
    End Sub


End Class
