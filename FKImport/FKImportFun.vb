﻿Public Class FKImportFun


    Public Function ImportOut(ByVal iGNID As Integer) As Boolean

        Dim dlg As New Windows.Forms.SaveFileDialog()
        dlg.OverwritePrompt = False
        dlg.AddExtension = True
        dlg.DefaultExt = ".xml"

        Dim sDFile As String
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sDFile = dlg.FileName
        Else
            Return False
        End If

        'If System.IO.File.Exists(sDFile) Then

        'Else

        '    Try
        '        System.IO.File.Copy(My.Application.Info.DirectoryPath & "\NewFunction.xml", sDFile)
        '    Catch ex As Exception
        '        MsgBox(ex.ToString)
        '        Return False
        '    End Try

        'End If

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds(5) As DataSet

        ds(0) = mdb.Reader("select * from GongNengShu where ID=" & iGNID)
        ds(1) = mdb.Reader("select * from GongNeng where GNSID=" & iGNID)
        ds(2) = mdb.Reader("select * from GN_KuCun where GNSID=" & iGNID)
        ds(3) = mdb.Reader("select * from GN_GongZi where GNSID=" & iGNID)
        ds(4) = mdb.Reader("select * from GN_PZ where GNSID=" & iGNID)

        ds(0).WriteXml(sDFile)
        ds(1).WriteXml(sDFile & "1")
        ds(2).WriteXml(sDFile & "2")
        ds(3).WriteXml(sDFile & "3")
        ds(4).WriteXml(sDFile & "4")

        MsgBox("功能导出成功！请手工保存对应的Excel模板文件", MsgBoxStyle.Information, "提示")
        Return True

        'MsgBox(ds(0).Tables(0).Rows(0).ToString)
        'MsgBox(ds(1).Tables(0).Rows(0).ToString)
        'MsgBox(ds(2).Tables(0).Rows(0).ToString)
        'MsgBox(ds(3).Tables(0).Rows(0).ToString)
        'MsgBox(ds(4).Tables(0).Rows(0).ToString)

        'Dim mdbW As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        ''mdbW.oleConnect.ConnectionString = mdbW.oleConnect.ConnectionString.Replace(mdbW.oleConnect.DataSource, sDFile)

        'If 0 Then
        '    MsgBox("你选中的功能库中已经存在当前选中的功能，无法再次复制", MsgBoxStyle.Information, "提示")
        '    Return False
        'Else
        '    mdbW.Write("insert into GongNengShu (Nian,ConText,ParentID,[Index],TiShi,isGongNeng,Kucun,KuCunSQL,KuCunHeJi,KuCunLK,KuCunDot,GongZi,GongZiSQL,GongZiHeJi,GongZiLK,GongZiDot,PZ,PZSQL,PZHeJi,PZLK,PZDot,KuCunPath,GongZiPath,PZPath,KuCunPrint,GongZiPrint,PZPrint,PZSCFS) values (" & ds(0).Tables(0).Rows(0).Item("Nian") & ",'" & ds(0).Tables(0).Rows(0).Item("ConText") & "'," & ds(0).Tables(0).Rows(0).Item("ParentID") & "," & ds(0).Tables(0).Rows(0).Item("Index") & ",'" & ds(0).Tables(0).Rows(0).Item("TiShi") & "'," & ds(0).Tables(0).Rows(0).Item("IsGongNeng") & "," & ds(0).Tables(0).Rows(0).Item("kucun") & ",'" & ds(0).Tables(0).Rows(0).Item("kucunsql") & "','" & ds(0).Tables(0).Rows(0).Item("kucunheji") & "','" & ds(0).Tables(0).Rows(0).Item("kucunlk") & "','" & ds(0).Tables(0).Rows(0).Item("kucundot") & "'," & ds(0).Tables(0).Rows(0).Item("GongZi") & ",'" & ds(0).Tables(0).Rows(0).Item("gongzisql") & "','" & ds(0).Tables(0).Rows(0).Item("gongziheji") & "','" & ds(0).Tables(0).Rows(0).Item("gongzilk") & "','" & ds(0).Tables(0).Rows(0).Item("gongzidot") & "'," & ds(0).Tables(0).Rows(0).Item("pz") & ",'" & ds(0).Tables(0).Rows(0).Item("pzsql") & "','" & ds(0).Tables(0).Rows(0).Item("pzheji") & "','" & ds(0).Tables(0).Rows(0).Item("pzlk") & "','" & ds(0).Tables(0).Rows(0).Item("pzdot") & "','" & ds(0).Tables(0).Rows(0).Item("kucunpath") & "','" & ds(0).Tables(0).Rows(0).Item("gongzipath") & "','" & ds(0).Tables(0).Rows(0).Item("pzpath") & "','" & ds(0).Tables(0).Rows(0).Item("kucunprint") & "','" & ds(0).Tables(0).Rows(0).Item("gongziprint") & "','" & ds(0).Tables(0).Rows(0).Item("PzPrint") & "'," & ds(0).Tables(0).Rows(0).Item("pzscfs") & ")")

        '    Dim iGNSID As Integer
        '    iGNSID = mdbW.Reader("select ID from GongNengShu where Context='" & ds(0).Tables(0).Rows(0).Item("ConText") & "'").Tables(0).Rows(0).Item(0)
        '    Dim i As Integer
        '    For i = 0 To ds(1).Tables(0).Rows.Count - 1
        '        mdbW.Write("insert into GongNeng (KJID,GNSID,Nian,Zhi,Qiyong,GeShi,X,Y,W,H,ZhiDu,TabIndex,Ziti,YanSe,BiTian,BangDing,Suoding) values ('" & ds(1).Tables(0).Rows(i).Item("KJID") & "'," & iGNSID & "," & ds(1).Tables(0).Rows(i).Item("Nian") & ",'" & ds(1).Tables(0).Rows(i).Item("zhi") & "'," & ds(1).Tables(0).Rows(i).Item("QiYong") & ",'" & ds(1).Tables(0).Rows(i).Item("GeShi") & "'," & ds(1).Tables(0).Rows(i).Item("X") & "," & ds(1).Tables(0).Rows(i).Item("Y") & "," & ds(1).Tables(0).Rows(i).Item("W") & "," & ds(1).Tables(0).Rows(i).Item("H") & "," & ds(1).Tables(0).Rows(i).Item("ZhiDu") & "," & ds(1).Tables(0).Rows(i).Item("TabIndex") & ",'" & ds(1).Tables(0).Rows(i).Item("ziti") & "','" & ds(1).Tables(0).Rows(i).Item("YanSe") & "'," & ds(1).Tables(0).Rows(i).Item("BiTian") & ",'" & ds(1).Tables(0).Rows(i).Item("Bangding") & "'," & ds(1).Tables(0).Rows(i).Item("SuoDing") & ")")

        '    Next

        '    For i = 0 To ds(2).Tables(0).Rows.Count - 1
        '        mdb.Write("insert into GN_KuCun (GNSID,Hang,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,显示) values (" & iGNSID & "," & ds(2).Tables(0).Rows(i).Item("Hang") & ",'" & ds(2).Tables(0).Rows(i).Item("编号") & "','" & ds(2).Tables(0).Rows(i).Item("日期") & "','" & ds(2).Tables(0).Rows(i).Item("摘要") & "','" & ds(2).Tables(0).Rows(i).Item("科目") & "','" & ds(2).Tables(0).Rows(i).Item("对应科目") & "','" & ds(2).Tables(0).Rows(i).Item("借方数量") & "','" & ds(2).Tables(0).Rows(i).Item("借方单位") & "','" & ds(2).Tables(0).Rows(i).Item("借方单价") & "','" & ds(2).Tables(0).Rows(i).Item("借方金额") & "','" & ds(2).Tables(0).Rows(i).Item("贷方数量") & "','" & ds(2).Tables(0).Rows(i).Item("贷方单位") & "','" & ds(2).Tables(0).Rows(i).Item("贷方单价") & "','" & ds(2).Tables(0).Rows(i).Item("贷方金额") & "','" & ds(2).Tables(0).Rows(i).Item("仓库") & "','" & ds(2).Tables(0).Rows(i).Item("经手人") & "','" & ds(2).Tables(0).Rows(i).Item("备用1") & "','" & ds(2).Tables(0).Rows(i).Item("备用2") & "','" & ds(2).Tables(0).Rows(i).Item("备用3") & "','" & ds(2).Tables(0).Rows(i).Item("备用4") & "','" & ds(2).Tables(0).Rows(i).Item("备用5") & "'," & ds(2).Tables(0).Rows(i).Item("显示") & ")")

        '    Next

        '    For i = 0 To ds(3).Tables(0).Rows.Count - 1
        '        mdb.Write("insert into GN_GongZi (GNSID,Hang,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,显示) values (" & iGNSID & "," & ds(3).Tables(0).Rows(i).Item("Hang") & ",'" & ds(3).Tables(0).Rows(i).Item("编号") & "','" & ds(3).Tables(0).Rows(i).Item("日期") & "','" & ds(3).Tables(0).Rows(i).Item("摘要") & "','" & ds(3).Tables(0).Rows(i).Item("科目") & "','" & ds(3).Tables(0).Rows(i).Item("对应科目") & "','" & ds(3).Tables(0).Rows(i).Item("借方数量") & "','" & ds(3).Tables(0).Rows(i).Item("借方单位") & "','" & ds(3).Tables(0).Rows(i).Item("借方单价") & "','" & ds(3).Tables(0).Rows(i).Item("借方金额") & "','" & ds(3).Tables(0).Rows(i).Item("贷方数量") & "','" & ds(3).Tables(0).Rows(i).Item("贷方单位") & "','" & ds(3).Tables(0).Rows(i).Item("贷方单价") & "','" & ds(3).Tables(0).Rows(i).Item("贷方金额") & "','" & ds(3).Tables(0).Rows(i).Item("仓库") & "','" & ds(3).Tables(0).Rows(i).Item("经手人") & "','" & ds(3).Tables(0).Rows(i).Item("备用1") & "','" & ds(3).Tables(0).Rows(i).Item("备用2") & "','" & ds(3).Tables(0).Rows(i).Item("备用3") & "','" & ds(3).Tables(0).Rows(i).Item("备用4") & "','" & ds(3).Tables(0).Rows(i).Item("备用5") & "'," & ds(3).Tables(0).Rows(i).Item("显示") & ")")

        '    Next

        '    For i = 0 To ds(4).Tables(0).Rows.Count - 1
        '        mdb.Write("insert into GN_PZ (GNSID,Hang,凭证号,编号,日期,摘要,科目,对应科目,借方数量,借方单位,借方单价,借方金额,贷方数量,贷方单位,贷方单价,贷方金额,仓库,经手人,备用1,备用2,备用3,备用4,备用5,显示) values (" & iGNSID & "," & ds(4).Tables(0).Rows(i).Item("Hang") & ",'" & ds(4).Tables(0).Rows(i).Item("凭证号") & "','" & ds(4).Tables(0).Rows(i).Item("编号") & "','" & ds(4).Tables(0).Rows(i).Item("日期") & "','" & ds(4).Tables(0).Rows(i).Item("摘要") & "','" & ds(4).Tables(0).Rows(i).Item("科目") & "','" & ds(4).Tables(0).Rows(i).Item("对应科目") & "','" & ds(4).Tables(0).Rows(i).Item("借方数量") & "','" & ds(4).Tables(0).Rows(i).Item("借方单位") & "','" & ds(4).Tables(0).Rows(i).Item("借方单价") & "','" & ds(4).Tables(0).Rows(i).Item("借方金额") & "','" & ds(4).Tables(0).Rows(i).Item("贷方数量") & "','" & ds(4).Tables(0).Rows(i).Item("贷方单位") & "','" & ds(4).Tables(0).Rows(i).Item("贷方单价") & "','" & ds(4).Tables(0).Rows(i).Item("贷方金额") & "','" & ds(4).Tables(0).Rows(i).Item("仓库") & "','" & ds(4).Tables(0).Rows(i).Item("经手人") & "','" & ds(4).Tables(0).Rows(i).Item("备用1") & "','" & ds(4).Tables(0).Rows(i).Item("备用2") & "','" & ds(4).Tables(0).Rows(i).Item("备用3") & "','" & ds(4).Tables(0).Rows(i).Item("备用4") & "','" & ds(4).Tables(0).Rows(i).Item("备用5") & "'," & ds(4).Tables(0).Rows(i).Item("显示") & ")")

        '    Next

        MsgBox("功能导出成功！请手工保存对应的Excel模板文件", MsgBoxStyle.Information, "提示")
        Return True

        'End If
    End Function

End Class
