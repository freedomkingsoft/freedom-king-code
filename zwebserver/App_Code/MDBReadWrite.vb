Imports Microsoft.VisualBasic
Imports System.Data


Public Class MDBReadWrite

    '定义内部全局变量
    'Dim oleConnect As New OleDb.OleDbConnection()
    'Dim oleDataAdapter As New OleDb.OleDbDataAdapter()
    'Dim oleCommand As New OleDb.OleDbCommand()
    Public oleConnect As New Data.OleDb.OleDbConnection()
    Public oleDataAdapter As New Data.OleDb.OleDbDataAdapter()
    Public oleCommand As New Data.OleDb.OleDbCommand()


    Dim ConnectString As String
    Public WriteOnly Property myConnectString() As String
        Set(ByVal Value As String)
            ConnectString = Value
        End Set
    End Property

    Dim sEx As String
    Public ReadOnly Property myExceptionInformation() As String
        Get
            myExceptionInformation = sEx
        End Get
    End Property

    Dim iDatasetRowsCount As Integer
    Public ReadOnly Property myDatasetRowsCount() As Integer
        Get
            myDatasetRowsCount = iDatasetRowsCount
        End Get
    End Property



    Public Sub New(ByVal sConnectionString As String)

        oleConnect.ConnectionString = sConnectionString
        oleCommand.Connection = oleConnect
        oleDataAdapter.SelectCommand = oleCommand

    End Sub

    Public Function ReadStoredProcedure(ByVal ConnectString As String, ByVal spName As String, ByVal sPara As String) As Data.DataSet
        'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值
        ConnectString = ConnectString.Replace("Provider=SQLOLEDB.1;", "")

        Dim dsDataset As New Data.DataSet()

        Try
            Dim sqlConn As New Data.SqlClient.SqlConnection(ConnectString)

            sqlConn.Open()

            Dim sqlDa As Data.SqlClient.SqlDataAdapter = New Data.SqlClient.SqlDataAdapter(spName, sqlConn)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.CommandTimeout = 300

            Dim sSP As String
            If sPara = "" Then

            Else
                For Each sSP In sPara.Split(";")
                    sqlDa.SelectCommand.Parameters.AddWithValue(sSP.Split(",")(0), sSP.Split(",")(1))
                Next
            End If

            sqlDa.Fill(dsDataset)
        Catch ex As Exception
            'MsgBox(ex.ToString)

        Finally
            oleConnect.Close()
        End Try

        Return dsDataset
    End Function

    Public Function ExecStoredProcedure(ByVal ConnectString As String, ByVal spName As String, ByVal sPara As String) As Integer
        'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值
        ConnectString = ConnectString.Replace("Provider=SQLOLEDB.1;", "")

        Dim iState As Integer = 0
        '0 代表执行未成功或者出现了异常，1 代表正常执行

        Try
            Dim sqlConn As New Data.SqlClient.SqlConnection(ConnectString)

            sqlConn.Open()

            'Dim sqlDa As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(spName, sqlConn)
            Dim sqlDa As New Data.SqlClient.SqlCommand(spName, sqlConn)
            sqlDa.CommandType = CommandType.StoredProcedure
            sqlDa.CommandTimeout = 300 '超时时间设置为150秒，默认为30秒

            Dim sSP As String
            For Each sSP In sPara.Split(";")
                sqlDa.Parameters.AddWithValue(sSP.Split(",")(0), sSP.Split(",")(1))
            Next

            sqlDa.ExecuteNonQuery()
            iState = 1
            'sqlDa.Fill(dsDataset)
        Catch ex As Exception
            ' MsgBox(ex.ToString)
            iState = 0
        Finally
            oleConnect.Close()
        End Try

        Return iState
    End Function

    Public Function ReaderOpen(ByVal sSQLSelectString As String) As Data.DataSet

        Dim dsDataset As New Data.DataSet()

        oleCommand.CommandText = sSQLSelectString

        Try
            If oleConnect.State = Data.ConnectionState.Open Then
            Else
                oleConnect.Open()
            End If

        Catch ex As System.Data.OleDb.OleDbException
            '判断异常来源
            'If ex.Message = "不能使用 ''；文件已在使用中。" Then
            '    'MsgBox(ex.ErrorCode.ToString)
            'Else
            '    'MsgBox(ex.ErrorCode.ToString)
            '    '启动自动恢复功能
            '    MsgBox("该提示非常重要，请认真阅读，并按照提示说明进行操作！" & Chr(10) & "系统检测到您的核心数据库存在异常，无法正常打开，其原因有可能是上次使用时非法退出系统（如突然断电等）所导致。" & Chr(10) & "系统已启动自动恢复功能，您可以选择时间最近的数据进行恢复，并检查最近录入的数据是否有丢失。也可以不恢复数据重启系统进行尝试。当出现注册界面时点击取消按钮即可退出系统。" & Chr(10) & "如问题仍无法解决请联系售后服务人员。", MsgBoxStyle.Exclamation, "重量提示")
            '    Dim dlg As New Restore
            '    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            '        '恢复成功，需要重新启动系统
            '        Return Nothing
            '    Else
            '        Return Nothing
            '    End If
            'End If

            '可在此处加入正版验证代码

            'Try
            '    Dim sPubCode As String
            '    sPubCode = Reader("select sPubKey from systemp  where sComputerName='" & Me.MCO(CPUID, HID) & "'").Tables(0).Rows(0).Item(0).ToString

            '    sPubCode = Decrypt(sPubCode, My.Application.Info.DirectoryPath & "\pra.key")
            '    sPubCode = sPubCode.Remove(0, sPubCode.IndexOf("</BitStrength>") + 14)

            '    FKG.myselfG.sMC = MCO(CPUID, HID)

            '    '                FKG.myselfG.sPubKey = sPubCode.Substring(0, 755 + 15)
            '    FKG.myselfG.sPubKey = sPubCode.Substring(0, 755)
            '    sPubCode = sPubCode.Remove(0, FKG.myselfG.sPubKey.Length + 9)

            '    FKG.myselfG.sRC = (sPubCode.Substring(0, sPubCode.IndexOf("</RedCode>")))

            '    Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(FKG.myselfG.sMC), FKG.myselfG.sRC)
            '        Case True
            '            System.Threading.Thread.Sleep(50)

            '            dsDataset = Reader(sSQLSelectString)
            '        Case False

            '            System.Threading.Thread.Sleep(50)

            '            Me.Write("update systemp set spraKey= replace(sPraKey,'0','o') where sComputerName='FreedomKing'")
            '            FKG.myselfG.setConStr("")
            '            dsDataset = Reader(sSQLSelectString)
            '    End Select

            'Catch ex2 As Exception
            '    FKG.myselfG.setConStr("")
            '    dsDataset = Reader(sSQLSelectString)
            '    'End
            'End Try

        Catch ex As Exception
            sEx = ex.ToString
            ' MsgBox(ex.Message, MsgBoxStyle.Information, "数据读取异常")
            Return Nothing
        Finally
            'oleConnect.Close()
        End Try
        If oleConnect.State = ConnectionState.Open Then
            Try
                oleDataAdapter.Fill(dsDataset)

                iDatasetRowsCount = dsDataset.Tables(0).Rows.Count

                Return dsDataset

            Catch ex As Exception
                sEx = ex.ToString
                ' MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
                Return Nothing
            Finally
                'oleConnect.Close()
            End Try

        End If

        Return dsDataset
    End Function

    Public Function Reader(ByVal sSQLSelectString As String) As Data.DataSet

        Dim dsDataset As New Data.DataSet()

        oleCommand.CommandText = sSQLSelectString

        Try
            If oleConnect.State = ConnectionState.Open Then
            Else
                oleConnect.Open()
            End If

        Catch ex As System.Data.OleDb.OleDbException
            '判断异常来源
            'If ex.Message = "不能使用 ''；文件已在使用中。" Then
            '    'MsgBox(ex.ErrorCode.ToString)
            'Else
            '    MsgBox(ex.ErrorCode.ToString)
            '    MsgBox(ex.Message)
            '    '启动自动恢复功能
            '    MsgBox("该提示非常重要，请认真阅读，并按照提示说明进行操作！" & Chr(10) & "系统检测到您的核心数据库存在异常，无法正常打开，其原因有可能是上次使用时非法退出系统（如突然断电等）所导致。" & Chr(10) & "系统已启动自动恢复功能，您可以选择时间最近的数据进行恢复，并检查最近录入的数据是否有丢失。也可以不恢复数据重启系统进行尝试。当出现注册界面时点击取消按钮即可退出系统。" & Chr(10) & "如问题仍无法解决请联系售后服务人员。", MsgBoxStyle.Exclamation, "重量提示")
            '    Dim dlg As New Restore
            '    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            '        '恢复成功，需要重新启动系统
            '        Return Nothing
            '    Else
            '        Return Nothing
            '    End If
            'End If
            ''可在此处加入正版验证代码
            'Try
            '    Dim sPubCode As String
            '    sPubCode = Reader("select sPubKey from systemp  where sComputerName='" & Me.MCO(CPUID, HID) & "'").Tables(0).Rows(0).Item(0).ToString

            '    sPubCode = Decrypt(sPubCode, My.Application.Info.DirectoryPath & "\pra.key")
            '    sPubCode = sPubCode.Remove(0, sPubCode.IndexOf("</BitStrength>") + 14)

            '    FKG.myselfG.sMC = MCO(CPUID, HID)

            '    '                FKG.myselfG.sPubKey = sPubCode.Substring(0, 755 + 15)
            '    FKG.myselfG.sPubKey = sPubCode.Substring(0, 755)
            '    sPubCode = sPubCode.Remove(0, FKG.myselfG.sPubKey.Length + 9)

            '    FKG.myselfG.sRC = (sPubCode.Substring(0, sPubCode.IndexOf("</RedCode>")))

            '    Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(FKG.myselfG.sMC), FKG.myselfG.sRC)
            '        Case True
            '            System.Threading.Thread.Sleep(50)

            '            dsDataset = Reader(sSQLSelectString)
            '        Case False

            '            System.Threading.Thread.Sleep(50)

            '            Me.Write("update systemp set spraKey= replace(sPraKey,'0','o') where sComputerName='FreedomKing'")
            '            FKG.myselfG.setConStr("")
            '            dsDataset = Reader(sSQLSelectString)
            '    End Select

            'Catch ex2 As Exception
            '    FKG.myselfG.setConStr("")
            '    dsDataset = Reader(sSQLSelectString)
            '    'End
            'End Try

        Catch ex As Exception
            sEx = ex.ToString
            ' MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
            Return Nothing
        Finally
            'oleConnect.Close()
        End Try
        If oleConnect.State = ConnectionState.Open Then
            Try
                oleDataAdapter.Fill(dsDataset)

                iDatasetRowsCount = dsDataset.Tables(0).Rows.Count

                Return dsDataset

            Catch ex As Exception
                sEx = ex.ToString
                '  MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
                Return Nothing
            Finally
                oleConnect.Close()
            End Try

        End If

        Return dsDataset

    End Function

    Public Sub DataBind(ByVal dg As System.Windows.Forms.DataGridView, ByVal sSelectCMD As String)
        sEx = ""

        Dim ds As New Data.DataSet()
        ds = Reader(sSelectCMD)

        If sEx = "" Then

            dg.DataSource = ds
            dg.DataMember = ds.Tables(0).TableName

        Else
            'MsgBox(sEx)
        End If
    End Sub

    Public Function Databind(ByVal cb As System.Windows.Forms.ComboBox, ByVal sSelectCMD As String) As Boolean
        Dim ds As New Data.DataSet()
        ds = Reader(sSelectCMD)

        If sEx = "" Then
            If iDatasetRowsCount = 0 Then
                Return False
            Else
                Dim i As Integer = 0
                For i = 0 To iDatasetRowsCount - 1
                    cb.Items.Add(ds.Tables(0).Rows(i).Item(0))
                Next
                Return True
            End If
        Else
            Return False
            'MsgBox(sEx, MsgBoxStyle.Information, "异常提示")
        End If
    End Function

    Public Function Write(ByVal sCMD As String) As Boolean
        oleCommand.CommandText = sCMD

        Try
            oleConnect.Open()
        Catch ex As System.Data.OleDb.OleDbException
            '可在此处加入正版验证代码

            'Try
            '    Do While IO.File.Exists(My.Application.Info.DirectoryPath & "\pra.key") = False
            '        'Write(sCMD)
            '    Loop

            '    If IO.File.Exists(My.Application.Info.DirectoryPath & "\pub.key") = False Then
            '        'Write(sCMD)
            '        'Dim dlg As New Regist
            '        'If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            '        'Else
            '        '    Login_Load(sender, e)
            '        'End If
            '    End If

            '    Dim sPubCode As String
            '    Dim streamReader As New IO.StreamReader(My.Application.Info.DirectoryPath & "\pub.key", True)
            '    sPubCode = streamReader.ReadToEnd
            '    streamReader.Close()

            '    sPubCode = Decrypt(sPubCode, My.Application.Info.DirectoryPath & "\pra.key")
            '    sPubCode = sPubCode.Remove(0, sPubCode.IndexOf("</BitStrength>") + 14)

            '    FKG.myselfG.sMC = MCO(CPUID, HID)

            '    'FKG.myselfG.sPubKey = sPubCode.Substring(0, 755 + 15)
            '    FKG.myselfG.sPubKey = sPubCode.Substring(0, 755)
            '    sPubCode = sPubCode.Remove(0, FKG.myselfG.sPubKey.Length + 9)

            '    FKG.myselfG.sRC = (sPubCode.Substring(0, sPubCode.IndexOf("</RedCode>")))

            '    Select Case SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(FKG.myselfG.sMC), FKG.myselfG.sRC)
            '        Case True
            '            System.Threading.Thread.Sleep(50)
            '            'oleConnect.Close()

            '            'Write(sCMD)
            '            sEx = ex.ToString
            '            Return Nothing
            '        Case False

            '            System.Threading.Thread.Sleep(50)
            '            'oleConnect.Close()
            '            FKG.myselfG.setConStr("")

            '            'Write(sCMD)
            '            sEx = ex.ToString
            '            Return Nothing
            '            'Dim dlg As New Regist

            '            'If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            '            '    Login_Load(sender, e)
            '            'Else
            '            '    End
            '            'End If

            '    End Select

            'Catch ex2 As Exception
            '    System.Threading.Thread.Sleep(50)
            '    'oleConnect.Close()
            '    FKG.myselfG.setConStr("")

            '    'Write(sCMD)
            '    sEx = ex.ToString
            '    Return Nothing
            '    'End
            'End Try
            'MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")

        Catch ex As Exception
            sEx = ex.ToString
            ' MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
            Return False
        Finally
            'oleConnect.Close()
        End Try

        If oleConnect.State = ConnectionState.Open Then
            Try
                oleCommand.ExecuteNonQuery()
                'oleConnect.Close()
                Return True

            Catch ex As Exception
                sEx = ex.ToString
                ' MsgBox(ex.Message, MsgBoxStyle.Information, "异常提示")
                Return False
            Finally
                oleConnect.Close()
            End Try

        End If

    End Function

    Public Function bExsit(ByVal sSQLSelectString As String) As Boolean

        Reader(sSQLSelectString)

        If iDatasetRowsCount = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Function Decrypt(ByVal p_inputString As String, ByVal p_strKeyPath As String) As String
        Dim fileString As String = ""
        Dim outString As String = ""

        'If IO.File.Exists(p_strKeyPath) Then
        '    Dim streamReader As New IO.StreamReader(p_strKeyPath, True)
        '    fileString = streamReader.ReadToEnd
        '    streamReader.Close()
        'End If

        Try
            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            fileString = Reader("select sPraKey from systemp  where sComputerName='FreedomKing'").Tables(0).Rows(0).Item(0).ToString

        Catch ex As Exception
            fileString = ""
        End Try
        '已改为从MDB读取

        If fileString <> "" Then
            Dim bitStrengthString As String = fileString.Substring(0, fileString.IndexOf("</BitStrength>") + 14)
            fileString = fileString.Replace(bitStrengthString, "")
            Dim bitStrength As Int32 = Convert.ToInt32(bitStrengthString.Replace("<BitStrength>", "").Replace("</BitStrength>", ""))
            Try
                outString = DecryptString(p_inputString, bitStrength, fileString)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End If

        Return outString

    End Function

    Private Function DecryptString(ByVal inputString As String, ByVal dwKeySize As Integer, ByVal xmlString As String) As String
        Dim rsaCryptoServiceProvider As New System.Security.Cryptography.RSACryptoServiceProvider(dwKeySize)
        rsaCryptoServiceProvider.FromXmlString(xmlString)

        Dim base64BlockSize As Integer
        If (dwKeySize / 8) Mod 3 <> 0 Then
            base64BlockSize = Int(((dwKeySize / 8) / 3) * 4) + 4
        Else
            base64BlockSize = Int((dwKeySize / 8) / 3) * 4

        End If

        Dim iterations As Integer = Int(inputString.Length / base64BlockSize)
        Dim arrayList As New System.Collections.ArrayList

        Dim i As Integer
        For i = 0 To iterations
            Dim encryptedBytes() As Byte
            Dim itemp As Integer

            If inputString.Length >= base64BlockSize Then
                itemp = base64BlockSize - 2
            Else
                itemp = inputString.Length
            End If

            encryptedBytes = Convert.FromBase64String(inputString.Substring(itemp * i, itemp))

            Array.Reverse(encryptedBytes)

            arrayList.AddRange(rsaCryptoServiceProvider.Decrypt(encryptedBytes, True))

        Next

        Return System.Text.Encoding.UTF32.GetString(TryCast(arrayList.ToArray(Type.[GetType]("System.Byte")), Byte()))

    End Function

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet As Object
        Dim cpu As Object

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    Private Function HID() As String
        Dim HDid As String = ""
        'Try
        '    Dim cimobject As New Management.ManagementClass("win32_diskdrive")
        '    Dim moc As Management.ManagementObjectCollection
        '    moc = cimobject.GetInstances()
        '    Dim mo As Management.ManagementObject

        '    For Each mo In moc
        '        'HDid = HDid & mo.Properties("signature").Value.ToString
        '        HDid = HDid & mo.Properties("PNPDeviceID").Value.ToString
        '        HDid = HDid.Replace("_", "")
        '        HDid = HDid.Remove(0, mo.Properties("Caption").Value.ToString.Length)
        '        Exit For
        '    Next
        'Catch ex As Exception
        '    HDid = ""
        'End Try

        Return HDid

    End Function


    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 36
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            sCDO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        End If

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
            Next
        End If


        'FKG.myselfG.sVersion = My.Application.Info.Version.ToString

        ''FKG.myselfG.sBanBen = "试用版"
        'FKG.myselfG.sBanBen = "开发版"
        ''FKG.myselfG.sBanBen = "正式版"
        ''FKG.myselfG.sBanBen = ""

        'FKG.myselfG.sClient = "服务器端"
        ' ''FKG.myselfG.sClient = "客户端"

        'Return sMCO & FKG.myselfG.sVersion & FKG.myselfG.sBanBen & FKG.myselfG.sClient
        Return ""

    End Function


    Private Function SignatureDeformatter(ByVal p_strKeyPublic As String, ByVal p_strHashbyteDeformatter As String, ByVal p_strDeformatterData As String) As Boolean
        Try
            Dim DeformatterData As Byte()
            Dim HashbyteDeformatter As Byte()

            HashbyteDeformatter = Convert.FromBase64String(p_strHashbyteDeformatter)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPublic)
            Dim RSADeformatter As New System.Security.Cryptography.RSAPKCS1SignatureDeformatter(RSA)
            '指定解密的时候HASH算法为MD5 
            RSADeformatter.SetHashAlgorithm("MD5")

            DeformatterData = Convert.FromBase64String(p_strDeformatterData)

            If RSADeformatter.VerifySignature(HashbyteDeformatter, DeformatterData) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
            'Throw ex
        End Try
    End Function

    Private Function GetHash(ByVal m_strSource As String) As String
        Try
            Dim strHashData As String
            '从字符串中取得Hash描述 
            Dim Buffer As Byte()
            Dim HashData As Byte()
            Dim MD5 As System.Security.Cryptography.HashAlgorithm = System.Security.Cryptography.HashAlgorithm.Create("MD5")
            Buffer = System.Text.Encoding.GetEncoding("GB2312").GetBytes(m_strSource)
            HashData = MD5.ComputeHash(Buffer)

            strHashData = Convert.ToBase64String(HashData)
            Return strHashData
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class

