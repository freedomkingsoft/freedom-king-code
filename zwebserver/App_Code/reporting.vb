﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

<WebService(Namespace:="https://bdsp.top/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class reporting
    Inherits System.Web.Services.WebService


    Function CheckToken(ByVal sToken As String) As Boolean
        Try
            Dim sName As String = Split(sToken, ",").GetValue(0).ToString
            Dim sPWS As String = Split(sToken, ",").GetValue(1).ToString

            Dim sT As String = ""
            Dim i As Integer
            For i = 0 To sName.Length - 1
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            Next

            Dim sTR As String = "A"
            For i = 0 To sT.Length.ToString - 1
                If i Mod 7 = 0 Then
                    sTR = sTR & sT.Substring(i, 1)
                End If
            Next

            If sPWS = sTR.ToString Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    'sLoginInfo传递登录信息，传递过来的是 userID，验证用户，sReportName传递报表名称，sPara传递参数。返回值为Dataset类型，直接绑定到DGV
    <WebMethod()> _
   Public Function reproting(ByVal sToken As String, ByVal sLoginInfo As String, ByVal sReportName As String, ByVal sPara As String) As Data.DataSet
        If CheckToken(sToken) = False Then
            Return Nothing
        End If

        Select Case sReportName
            Case "getZT"
                Return getZT(sPara)

            Case Else
                Return Nothing
        End Select
        'Return ds
    End Function

    Private Function getZT(ByVal sPara As String) As Data.DataSet
        Dim mdbr As New MDBReadWrite(System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString)
        Return mdbr.Reader("select * from FreedomKingData.dbo.zt")
    End Function

End Class
