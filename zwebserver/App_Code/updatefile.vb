﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

<WebService(Namespace:="https://www.bdsp.top/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class UpdateFileService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function UpdateFile() As String()
        Dim sUpdateFiles() As String
        Dim sDir As String = System.Web.HttpContext.Current.Server.MapPath("updatefile.asmx")
        sUpdateFiles = System.IO.File.ReadAllLines(sDir.Replace("updatefile.asmx", "updatefiles.txt"))

        Return sUpdateFiles
    End Function
End Class
