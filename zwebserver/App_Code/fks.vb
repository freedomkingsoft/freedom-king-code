Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

Imports System.Data

<WebService(Namespace:="http://bdsp.top/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld(ByVal sToken As String) As String
        If CheckToken(sToken) = False Then
            Return Nothing
        End If

        Return "Hello World"
    End Function

 

    <WebMethod()> _
   Public Function HelloVersion(ByVal sToken As String) As String
        If CheckToken(sToken) = False Then
            Return Nothing
        End If

        Return System.Configuration.ConfigurationManager.AppSettings("newVersion").ToString & "." & System.Configuration.ConfigurationManager.AppSettings("newRevision").ToString
    End Function

    <WebMethod()> _
    Public Function Hello(ByVal sToken As String, ByVal bBackup As Boolean) As String
        If CheckToken(sToken) = False Then
            Return Nothing
        End If

        Return "Hello"
        'If bBackup = False Then
        '    Return System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ConnectionString
        'Else
        '    Return System.Configuration.ConfigurationManager.AppSettings("BackupDataFilePath").ToString()
        'End If
    End Function

    '<WebMethod()> _ 
    '    '20200827 停用旧版本登录功能，可能引起修改时工资数量不一致的问题。解决 在修改状态下不自动刷新公式的bug
    'Public Function Login(ByVal sToken As String, ByVal sName As String, ByVal sSubName As String, ByVal sPWS As String, ByVal bPhone As Integer) As Data.DataSet
    '    Dim mdbr As MDBReadWrite = NewMDBR(System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ConnectionString)
    '    Dim sIP As String
    '    sIP = Me.Context.Request.UserHostAddress

    '    If CheckToken(sToken) = False Then
    '        mdbr.Write("insert into [yunLoginLog] ([LoginIP],[LoginState])  VALUES('" & sIP & "','EXCEPTION')")
    '        Return Nothing
    '    End If

    '    Dim dsResult As DataSet
    '    If bPhone = 1 Then
    '        dsResult = mdbr.Reader("select  * from yunUser join yunUserLoginName on yunUser.userID = yunUserLoginName.Userid where loginPhone='" + sName + "' and  pwdcompare('" + sPWS + "',loginPhonePWS)=1")
    '    Else
    '        dsResult = mdbr.Reader("select * from yunUser join yunUserLoginName on yunUser.userID = yunUserLoginName.Userid where loginName='" + sName + "' and subUserID='" + sSubName + "' and  pwdcompare('" + sPWS + "',subUserPWS)=1")
    '    End If

    '    '是否需要记录登录信息，以后再定

    '    If dsResult.Tables(0).Rows.Count = 0 Then
    '        '登录失败，保存登录信息

    '        mdbr.Write("insert into [yunLoginLog] ([LoginIP],[LoginState],[logPhone])  VALUES('" & sIP & "','no','" & sName & "')")
    '    Else
    '        '登录成功，保存登录信息
    '        mdbr.Write("insert into [yunLoginLog] ([LoginID],[LoginIP],[LoginState])  VALUES('" & dsResult.Tables(0).Rows(0).Item("loginID").ToString & "','" & sIP & "','ok')")

    '    End If

    '    Return dsResult
    'End Function

    <WebMethod()> _
    Public Function NewLogin(ByVal sToken As String, ByVal sName As String, ByVal sSubName As String, ByVal sPWS As String, ByVal bPhone As Integer, ByVal sVersion As String) As Data.DataSet
        Dim mdbr As MDBReadWrite = NewMDBR(System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ConnectionString)
        Dim sIP As String
        sIP = Me.Context.Request.UserHostAddress

        If CheckToken(sToken) = False Then
            mdbr.Write("insert into [yunLoginLog] ([LoginIP],[LoginState])  VALUES('" & sIP & "','EXCEPTION')")
            Return Nothing
        End If

        Dim dsResult As DataSet
        If bPhone = 1 Then
            dsResult = mdbr.Reader("select  * from yunUser join yunUserLoginName on yunUser.userID = yunUserLoginName.Userid where loginPhone='" + sName + "' and  pwdcompare('" + sPWS + "',loginPhonePWS)=1")
        Else
            dsResult = mdbr.Reader("select * from yunUser join yunUserLoginName on yunUser.userID = yunUserLoginName.Userid where loginName='" + sName + "' and subUserID='" + sSubName + "' and  pwdcompare('" + sPWS + "',subUserPWS)=1")
        End If

        '是否需要记录登录信息，以后再定

        If dsResult.Tables(0).Rows.Count = 0 Then
            '登录失败，保存登录信息

            mdbr.Write("insert into [yunLoginLog] ([LoginIP],[LoginState],[logPhone])  VALUES('" & sIP & "','no','" & sName & "')")
        ElseIf System.Configuration.ConfigurationManager.AppSettings("minRevision").ToString > sVersion Then
            '当期系统版本低于最低版本，无法登陆
            dsResult = Nothing
        Else
            '登录成功，保存登录信息
            mdbr.Write("insert into [yunLoginLog] ([LoginID],[LoginIP],[LoginState],[logPhone],[logVersion])  VALUES('" & dsResult.Tables(0).Rows(0).Item("loginID").ToString & "','" & sIP & "','ok','" & sName & "','" & sVersion & "')")

        End If

        Return dsResult
    End Function

    <WebMethod()> _
   Public Function CheckPhone(ByVal sToken As String, ByVal regPhone As String) As Boolean
        If CheckToken(sToken) = False Then
            Return Nothing
        End If

        '存在手机号则不允许重新创建
        Dim mdbr As MDBReadWrite = NewMDBR(System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ConnectionString)

        Return mdbr.bExsit("select regPhone from yunUser where regPhone='" + regPhone + "'")
    End Function

    <WebMethod()> _
    Public Function CreateNewUser(ByVal sToken As String, ByVal regPhone As String, ByVal regPWS As String, ByVal comName As String, ByVal selecthangye As String, ByVal sVIP As String, ByVal iUserNumber As Integer, ByVal shiyongtianshu As Integer) As Data.DataSet


        If CheckToken(sToken) = False Then
            Return Nothing
        End If

        Dim sConn As String = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ConnectionString

        Dim mdbW As MDBReadWrite
        mdbW = NewMDBR(sConn)

        Dim dsNewUser As DataSet = Nothing
        '随机生成数据库相关信息
        Randomize()

        Dim sUserLoginID As String
        Do
            sUserLoginID = CType(Int(1000 + 8999 * Rnd()), String)
            sUserLoginID = sUserLoginID & CType(Int(1000 + 8999 * Rnd()), String)

        Loop While mdbW.bExsit("select userLoginID from yunUser where userLoginID= '" + sUserLoginID + "'")

        Dim dFreeEndDate As Date = Now().Date.AddDays(shiyongtianshu)

        Dim sUserDatabase As String
        Do
            sUserDatabase = sVIP & Me.randString(11) 'TMP_ 开头的数据库都是自动创建的，付费之后更改数据库 为TVIP_ 开头

        Loop While mdbW.bExsit("select userDatabase from yunUser where userDatabase='" + sUserDatabase + "'")


        Dim sUserLogin As String
        Do
            sUserLogin = sVIP & Me.randString(11)  'TMP_ 开头的都是自动创建的，付费之后更改为 为VIP_ 开头

        Loop While mdbW.bExsit("select userLogin from yunUser where userLogin ='" + sUserLogin + "'")

        Dim sUserPWS As String = Me.randString(16, 1)

        Dim sSQL As String
        sSQL = "INSERT INTO [yunUser] ([userLoginID] ,[regPhone] ,[freeEndDate] ,[VIPMaturitydate],[userDatabase] ,[userLogin] ,[userPWS] ,[comName],[eMail],[loginNumber])     VALUES ('" & sUserLoginID & "','" & regPhone & "' ,'" & dFreeEndDate & "','" & dFreeEndDate & "','" & sUserDatabase & "','" & sUserLogin & "','" & sUserPWS & "','" & comName & "','" & selecthangye & "'," & iUserNumber & ")"

        mdbW.Write(sSQL)


        '生成根据公司号和子账号进行标准登录的登录信息
        Dim sSubUserID As String
        Do
            sSubUserID = CType(Int(1000 + 8999 * Rnd()), String)
        Loop While mdbW.bExsit("select loginName from yunUserLoginName where loginName='" & sUserLoginID & "' and subUserID='" & sSubUserID & "'")

        mdbW.Write("INSERT INTO [YunLog].[dbo].[yunUserLoginName] ([Userid] ,[loginName] ,[loginPhone] ,[loginPhonePWS] ,[loginState] ,[subUserID] ,[subUserPWS] ,[admin])     VALUES ((SELECT [userID] FROM [yunUser] where userLoginID='" & sUserLoginID & "' ),'" & sUserLoginID & "' ,'" & regPhone & "' ,pwdencrypt('" & regPWS & "') ,1 ,'" & sSubUserID & "' ,pwdencrypt('" & regPWS & "') ,1)")

        '创建登录名
        Try
            Dim sSqlLogin As String
            sSqlLogin = "CREATE LOGIN [" & sUserLogin & "] WITH PASSWORD=N'" & sUserPWS & "', DEFAULT_LANGUAGE=[简体中文], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF"
            mdbW.oleCommand.CommandText = sSqlLogin
            mdbW.oleConnect.Open()

            mdbW.oleCommand.ExecuteNonQuery()

        Catch ex As Exception
        Finally
            mdbW.oleConnect.Close()
        End Try


        '创建数据库

        '首先创建一个文件夹,
        Dim sDataFilePath As String = System.Configuration.ConfigurationManager.AppSettings("DataFilePath")


        mdbW.ExecStoredProcedure(sConn, "createmdf", "@databasename," + sUserDatabase + ";@datafilepath," + sDataFilePath + "")

        '复制表，复制触发器
        '复制视图
        '复制存储过程

        '以上创建数据库过程通过存储过程来实现
        Dim iDone As Integer
        iDone = mdbW.ExecStoredProcedure(sConn, "createdatabase", "@databasename," & sUserDatabase & ";@loginname," & sUserLogin & ";@loginpws," & sUserPWS & ";@datafilepath," & sDataFilePath & "")
        '复制数据

        'mdbW.ExecStoredProcedure(sConn, "insertData", "@databasename," & sUserDatabase & ";@comName,'" & comName & "'")
        'mdbW.ExecStoredProcedure(sConn, "insertData2", "@databasename," & sUserDatabase & ";@comName,'" & comName & "'")

        Dim sHangyeDatabase As String
        sHangyeDatabase = mdbW.Reader("select HYDatabase  from hangye where HangYeName ='" & selecthangye & "'").Tables(0).Rows(0).Item(0).ToString


        mdbW.ExecStoredProcedure(sConn, "insertData", "@databasename," & sUserDatabase & ";@selecthangye," & sHangyeDatabase & ";@comName,'" & comName & "'")
        mdbW.ExecStoredProcedure(sConn, "insertData2", "@databasename," & sUserDatabase & ";@selecthangye," & sHangyeDatabase & ";@comName,'" & comName & "'")

        '处理年份
        '及时更新标准数据库即可，无需特殊处理
        '更改用户设置的企业名称

        '改变登录名的默认数据库和数据库的所有者
        Try
            Dim sSqlLogin As String

            sSqlLogin = "ALTER LOGIN [" + sUserLogin + "] WITH DEFAULT_DATABASE=[" + sUserDatabase + "], DEFAULT_LANGUAGE=[简体中文], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF"
            mdbW.oleCommand.CommandText = sSqlLogin
            mdbW.oleConnect.Open()

            mdbW.oleCommand.ExecuteNonQuery()

        Catch ex As Exception
        Finally
            mdbW.oleConnect.Close()
        End Try

        mdbW.ExecStoredProcedure(sConn, "changedbowner", "@databasename," + sUserDatabase + ";@loginName," + sUserLogin + "")


        '数据库创建成功，返回用户登录成功的数据集

        dsNewUser = mdbW.Reader("select * from yunUser join yunUserLoginName on yunUser.userID = yunUserLoginName.Userid where regPhone='" + regPhone + "'")
        Return dsNewUser
    End Function


    Function CheckToken(ByVal sToken As String) As Boolean
        Try
            Dim sName As String = Split(sToken, ",").GetValue(0).ToString
            Dim sPWS As String = Split(sToken, ",").GetValue(1).ToString

            Dim sT As String = ""
            Dim i As Integer
            For i = 0 To sName.Length - 1
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            Next

            Dim sTR As String = "A"
            For i = 0 To sT.Length.ToString - 1
                If i Mod 7 = 0 Then
                    sTR = sTR & sT.Substring(i, 1)
                End If
            Next

            If sPWS = sTR.ToString Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try


    End Function


    Function NewMDBR(ByVal sConString As String) As MDBReadWrite
        Dim mdbr As MDBReadWrite

        mdbr = New MDBReadWrite(sConString)

        Return mdbr
    End Function

    Function randString(Optional ByVal iLength As Integer = 8, Optional ByVal bNumber As Boolean = 0) As String
        Dim sRand As String = ""

        Dim iRand As Integer

        Do
            Randomize()
            iRand = Int(Rnd() * 90 + 33)

            If bNumber = 0 Then
                If (iRand >= 65 And iRand <= 90) Or (iRand >= 97 And iRand <= 122) Then
                    sRand = sRand & CType(Chr(iRand), String)
                End If
            Else
                If (iRand >= 48 And iRand <= 57) Or (iRand >= 65 And iRand <= 90) Or (iRand >= 97 And iRand <= 122) Then
                    sRand = sRand & CType(Chr(iRand), String)
                End If
            End If

        Loop Until sRand.Length = iLength

        Return sRand
    End Function

End Class
