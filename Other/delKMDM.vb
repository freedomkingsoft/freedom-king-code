﻿Public Class delKMDM

    Private Sub delKMDM_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        fillKMDM()
    End Sub

    Private Sub fillKMDM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsKMDM As DataSet
        dsKMDM = mdb.Reader("select KMDM,KMMC from KMDM where Nian=" & FKG.myselfG.NianFen)
        Dim i As Integer
        For i = 0 To dsKMDM.Tables(0).Rows.Count - 1
            'Me.clbKMDM.Items.Add(dsKMDM.Tables(0).Rows(i).Item(0).ToString & "     " & dsKMDM.Tables(0).Rows(i).Item(1).ToString)
            Me.clbKMDM.Items.Add(dsKMDM.Tables(0).Rows(i).Item(0).ToString)
        Next
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim i As Integer
        For i = 0 To Me.clbKMDM.Items.Count - 1
            Me.clbKMDM.Items(i).checked = True
        Next
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim i As Integer
        For i = 0 To Me.clbKMDM.Items.Count - 1
            Me.clbKMDM.Items(i).checked = False
        Next
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim i As Integer
        For i = 0 To Me.clbKMDM.CheckedItems.Count - 1

            'If iWorkState = 2 Then
            '确定是否为末级科目
            If FKF.FKF.isMJKM(Me.clbKMDM.CheckedItems.Item(i).ToString) = False Then
                MsgBox("该科目不是末级科目，无法删除！", MsgBoxStyle.Information, "提示")
                Continue For
            End If

            'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            '搜索当前年度是否使用
            If mdb.bExsit("select * from jibencaozuo where (JieFangKeMu='" & Me.clbKMDM.CheckedItems.Item(i).ToString & "' or DaiFangKeMu='" & Me.clbKMDM.CheckedItems.Item(i).ToString & "') and 删除='false' and Nian=" & FKG.myselfG.NianFen) Then
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Continue For
            End If
            '搜索科目余额表,只需要判断年初余额和13月的借方和贷方累计发生额是否为0即可.
            If mdb.bExsit("select * from 科目余额表 where (科目代码='" & Me.clbKMDM.CheckedItems.Item(i).ToString & "' and (年初借方余额<>0 or 年初贷方余额<>0 or [13月借方累计发生额]<> 0 or [13月贷方累计发生额]<>0 )) and 年份=" & FKG.myselfG.NianFen) Then
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Continue For
            End If
            '搜索库存余额表
            If mdb.bExsit("select * from 库存余额表 where (科目代码='" & Me.clbKMDM.CheckedItems.Item(i).ToString & "' and (年初借方余额<>0 or 年初贷方余额<>0 or [13月借方累计发生额]<> 0 or [13月贷方累计发生额]<>0 )) and 年份=" & FKG.myselfG.NianFen) Then
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Continue For
            End If
            '搜索工资余额表
            If mdb.bExsit("select * from 工资余额表 where (科目代码='" & Me.clbKMDM.CheckedItems.Item(i).ToString & "' and (年初借方余额<>0 or 年初贷方余额<>0 or [13月借方累计发生额]<> 0 or [13月贷方累计发生额]<>0 )) and 年份=" & FKG.myselfG.NianFen) Then
                MsgBox("该科目在当前会计年度已使用，无法删除！", MsgBoxStyle.Information, "提示")
                Continue For
            End If

            If mdb.Write("delete from KMDM where kmdm='" & Me.clbKMDM.CheckedItems.Item(i).ToString & "' and Nian=" & FKG.myselfG.NianFen) Then
                '删除科目成功后,判断上级科目是否为末级科目
                If FKF.FKF.isMJKM(FKF.FKF.getSJKM(Me.clbKMDM.CheckedItems.Item(i).ToString)) Then
                    mdb.Write("Update KMDM set SFMJ='1' where KMDM='" & FKF.FKF.getSJKM(Me.clbKMDM.CheckedItems.Item(i).ToString) & "'  and Nian=" & FKG.myselfG.NianFen)
                End If

                '删除辅助属性
                mdb.Write("delete from KMFZSX where 科目代码='" & Me.clbKMDM.CheckedItems.Item(i).ToString & "' and Nian=" & FKG.myselfG.NianFen)
            End If
            'End If
        Next

        Me.clbKMDM.Items.Clear()
        fillKMDM()
    End Sub
End Class