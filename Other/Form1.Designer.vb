﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OtherMain
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.系统设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.重新登录 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.凭证类型设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.常用摘要定义 = New System.Windows.Forms.ToolStripMenuItem
        Me.打造主界面 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.退出系统 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能树管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.新建功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.修改功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.导入功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.导出功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.功能设计帮助 = New System.Windows.Forms.ToolStripMenuItem
        Me.证帐输出 = New System.Windows.Forms.ToolStripMenuItem
        Me.总帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.总帐余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.明细帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.日记帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存明细帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存日记帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存报表 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来明细账 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来日记账 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来报表 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资明细帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资日记帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.外存型号余额 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外明细账 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外日记账 = New System.Windows.Forms.ToolStripMenuItem
        Me.综合查询 = New System.Windows.Forms.ToolStripMenuItem
        Me.期末处理 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存结账 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资结账 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator
        Me.单证审核 = New System.Windows.Forms.ToolStripMenuItem
        Me.期间记账 = New System.Windows.Forms.ToolStripMenuItem
        Me.帐库对账 = New System.Windows.Forms.ToolStripMenuItem
        Me.生成转账凭证 = New System.Windows.Forms.ToolStripMenuItem
        Me.期末结帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.年度处理 = New System.Windows.Forms.ToolStripMenuItem
        Me.帐套打印 = New System.Windows.Forms.ToolStripMenuItem
        Me.高级功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.科目代码设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.初始余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.科目余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.初始工余额装入资 = New System.Windows.Forms.ToolStripMenuItem
        Me.初始委外余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能树 = New System.Windows.Forms.ToolStripMenuItem
        Me.报表树管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.企业资料设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.仓库定义 = New System.Windows.Forms.ToolStripMenuItem
        Me.用户管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.特殊用户设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator
        Me.导入 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator
        Me.备份数据 = New System.Windows.Forms.ToolStripMenuItem
        Me.恢复数据 = New System.Windows.Forms.ToolStripMenuItem
        Me.定制开发 = New System.Windows.Forms.ToolStripMenuItem
        Me.四工序机工工资 = New System.Windows.Forms.ToolStripMenuItem
        Me.帮助 = New System.Windows.Forms.ToolStripMenuItem
        Me.简介 = New System.Windows.Forms.ToolStripMenuItem
        Me.使用流程 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能设计 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.注册 = New System.Windows.Forms.ToolStripMenuItem
        Me.关于 = New System.Windows.Forms.ToolStripMenuItem
        Me.其他功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(32, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(166, 70)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "清除数据　不包括用户和功能及科目代码"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(350, 12)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(166, 70)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "检查末级科目设置"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(350, 98)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(166, 70)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "设置余额方向"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(22, 288)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(241, 31)
        Me.Button8.TabIndex = 1
        Me.Button8.Text = "压缩数据库"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(336, 279)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(180, 31)
        Me.Button10.TabIndex = 1
        Me.Button10.Text = "导出菜单到数据库"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.系统设置, Me.功能管理, Me.证帐输出, Me.期末处理, Me.年度处理, Me.高级功能, Me.定制开发, Me.帮助, Me.其他功能})
        Me.MenuStrip1.Location = New System.Drawing.Point(9, 364)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(677, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        '系统设置
        '
        Me.系统设置.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.重新登录, Me.ToolStripSeparator1, Me.凭证类型设置, Me.常用摘要定义, Me.打造主界面, Me.ToolStripSeparator4, Me.退出系统})
        Me.系统设置.Name = "系统设置"
        Me.系统设置.ShortcutKeyDisplayString = ""
        Me.系统设置.Size = New System.Drawing.Size(83, 20)
        Me.系统设置.Text = "系统设置(&S)"
        '
        '重新登录
        '
        Me.重新登录.Name = "重新登录"
        Me.重新登录.Size = New System.Drawing.Size(159, 22)
        Me.重新登录.Text = "重新登录"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(156, 6)
        '
        '凭证类型设置
        '
        Me.凭证类型设置.Name = "凭证类型设置"
        Me.凭证类型设置.Size = New System.Drawing.Size(159, 22)
        Me.凭证类型设置.Text = "凭证类型设置"
        Me.凭证类型设置.Visible = False
        '
        '常用摘要定义
        '
        Me.常用摘要定义.Name = "常用摘要定义"
        Me.常用摘要定义.Size = New System.Drawing.Size(159, 22)
        Me.常用摘要定义.Text = "常用摘要定义"
        '
        '打造主界面
        '
        Me.打造主界面.Name = "打造主界面"
        Me.打造主界面.Size = New System.Drawing.Size(159, 22)
        Me.打造主界面.Text = "打造主界面"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(156, 6)
        '
        '退出系统
        '
        Me.退出系统.Name = "退出系统"
        Me.退出系统.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.退出系统.Size = New System.Drawing.Size(159, 22)
        Me.退出系统.Text = "退出系统"
        '
        '功能管理
        '
        Me.功能管理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.功能树管理, Me.ToolStripSeparator8, Me.新建功能, Me.修改功能, Me.ToolStripSeparator5, Me.导入功能, Me.导出功能, Me.ToolStripSeparator6, Me.功能设计帮助})
        Me.功能管理.Name = "功能管理"
        Me.功能管理.Size = New System.Drawing.Size(83, 20)
        Me.功能管理.Text = "功能管理(&G)"
        '
        '功能树管理
        '
        Me.功能树管理.Name = "功能树管理"
        Me.功能树管理.Size = New System.Drawing.Size(142, 22)
        Me.功能树管理.Text = "功能树管理"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(139, 6)
        '
        '新建功能
        '
        Me.新建功能.Name = "新建功能"
        Me.新建功能.Size = New System.Drawing.Size(142, 22)
        Me.新建功能.Text = "新建功能"
        '
        '修改功能
        '
        Me.修改功能.Name = "修改功能"
        Me.修改功能.Size = New System.Drawing.Size(142, 22)
        Me.修改功能.Text = "修改功能"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(139, 6)
        '
        '导入功能
        '
        Me.导入功能.Name = "导入功能"
        Me.导入功能.Size = New System.Drawing.Size(142, 22)
        Me.导入功能.Text = "导入功能"
        '
        '导出功能
        '
        Me.导出功能.Name = "导出功能"
        Me.导出功能.Size = New System.Drawing.Size(142, 22)
        Me.导出功能.Text = "导出功能"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(139, 6)
        '
        '功能设计帮助
        '
        Me.功能设计帮助.Name = "功能设计帮助"
        Me.功能设计帮助.Size = New System.Drawing.Size(142, 22)
        Me.功能设计帮助.Text = "功能设计帮助"
        '
        '证帐输出
        '
        Me.证帐输出.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.总帐, Me.库存帐, Me.往来帐, Me.工资帐, Me.委外帐, Me.综合查询})
        Me.证帐输出.Name = "证帐输出"
        Me.证帐输出.Size = New System.Drawing.Size(83, 20)
        Me.证帐输出.Text = "报表输出(&Z)"
        '
        '总帐
        '
        Me.总帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.总帐余额表, Me.明细帐, Me.日记帐})
        Me.总帐.Name = "总帐"
        Me.总帐.Size = New System.Drawing.Size(118, 22)
        Me.总帐.Text = "总帐"
        '
        '总帐余额表
        '
        Me.总帐余额表.Name = "总帐余额表"
        Me.总帐余额表.Size = New System.Drawing.Size(130, 22)
        Me.总帐余额表.Text = "总帐余额表"
        '
        '明细帐
        '
        Me.明细帐.Name = "明细帐"
        Me.明细帐.Size = New System.Drawing.Size(130, 22)
        Me.明细帐.Text = "明细帐"
        '
        '日记帐
        '
        Me.日记帐.Name = "日记帐"
        Me.日记帐.Size = New System.Drawing.Size(130, 22)
        Me.日记帐.Text = "日记帐"
        '
        '库存帐
        '
        Me.库存帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.库存余额表, Me.库存明细帐, Me.库存日记帐, Me.库存报表})
        Me.库存帐.Name = "库存帐"
        Me.库存帐.Size = New System.Drawing.Size(118, 22)
        Me.库存帐.Text = "库存帐"
        '
        '库存余额表
        '
        Me.库存余额表.Name = "库存余额表"
        Me.库存余额表.Size = New System.Drawing.Size(130, 22)
        Me.库存余额表.Text = "库存余额表"
        '
        '库存明细帐
        '
        Me.库存明细帐.Name = "库存明细帐"
        Me.库存明细帐.Size = New System.Drawing.Size(130, 22)
        Me.库存明细帐.Text = "库存明细帐"
        '
        '库存日记帐
        '
        Me.库存日记帐.Name = "库存日记帐"
        Me.库存日记帐.Size = New System.Drawing.Size(130, 22)
        Me.库存日记帐.Text = "库存日记帐"
        '
        '库存报表
        '
        Me.库存报表.Name = "库存报表"
        Me.库存报表.Size = New System.Drawing.Size(130, 22)
        Me.库存报表.Text = "库存报表"
        '
        '往来帐
        '
        Me.往来帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.往来余额表, Me.往来明细账, Me.往来日记账, Me.往来报表})
        Me.往来帐.Name = "往来帐"
        Me.往来帐.Size = New System.Drawing.Size(118, 22)
        Me.往来帐.Text = "往来帐"
        '
        '往来余额表
        '
        Me.往来余额表.Name = "往来余额表"
        Me.往来余额表.Size = New System.Drawing.Size(130, 22)
        Me.往来余额表.Text = "往来余额表"
        '
        '往来明细账
        '
        Me.往来明细账.Name = "往来明细账"
        Me.往来明细账.Size = New System.Drawing.Size(130, 22)
        Me.往来明细账.Text = "往来明细账"
        '
        '往来日记账
        '
        Me.往来日记账.Name = "往来日记账"
        Me.往来日记账.Size = New System.Drawing.Size(130, 22)
        Me.往来日记账.Text = "往来日记账"
        '
        '往来报表
        '
        Me.往来报表.Name = "往来报表"
        Me.往来报表.Size = New System.Drawing.Size(130, 22)
        Me.往来报表.Text = "往来报表"
        '
        '工资帐
        '
        Me.工资帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.工资余额表, Me.工资明细帐, Me.工资日记帐})
        Me.工资帐.Name = "工资帐"
        Me.工资帐.Size = New System.Drawing.Size(118, 22)
        Me.工资帐.Text = "工资帐"
        '
        '工资余额表
        '
        Me.工资余额表.Name = "工资余额表"
        Me.工资余额表.Size = New System.Drawing.Size(130, 22)
        Me.工资余额表.Text = "工资余额表"
        '
        '工资明细帐
        '
        Me.工资明细帐.Name = "工资明细帐"
        Me.工资明细帐.Size = New System.Drawing.Size(130, 22)
        Me.工资明细帐.Text = "工资明细帐"
        '
        '工资日记帐
        '
        Me.工资日记帐.Name = "工资日记帐"
        Me.工资日记帐.Size = New System.Drawing.Size(130, 22)
        Me.工资日记帐.Text = "工资日记帐"
        '
        '委外帐
        '
        Me.委外帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.外存型号余额, Me.委外余额表, Me.委外明细账, Me.委外日记账})
        Me.委外帐.Name = "委外帐"
        Me.委外帐.Size = New System.Drawing.Size(118, 22)
        Me.委外帐.Text = "委外帐"
        '
        '外存型号余额
        '
        Me.外存型号余额.Name = "外存型号余额"
        Me.外存型号余额.Size = New System.Drawing.Size(142, 22)
        Me.外存型号余额.Text = "外存型号余额"
        '
        '委外余额表
        '
        Me.委外余额表.Name = "委外余额表"
        Me.委外余额表.Size = New System.Drawing.Size(142, 22)
        Me.委外余额表.Text = "委外余额表"
        '
        '委外明细账
        '
        Me.委外明细账.Name = "委外明细账"
        Me.委外明细账.Size = New System.Drawing.Size(142, 22)
        Me.委外明细账.Text = "委外明细帐"
        '
        '委外日记账
        '
        Me.委外日记账.Name = "委外日记账"
        Me.委外日记账.Size = New System.Drawing.Size(142, 22)
        Me.委外日记账.Text = "委外日记帐"
        '
        '综合查询
        '
        Me.综合查询.Name = "综合查询"
        Me.综合查询.Size = New System.Drawing.Size(118, 22)
        Me.综合查询.Text = "综合查询"
        '
        '期末处理
        '
        Me.期末处理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.库存结账, Me.工资结账, Me.ToolStripSeparator9, Me.单证审核, Me.期间记账, Me.帐库对账, Me.生成转账凭证, Me.期末结帐})
        Me.期末处理.Name = "期末处理"
        Me.期末处理.Size = New System.Drawing.Size(83, 20)
        Me.期末处理.Text = "期末处理(&Q)"
        '
        '库存结账
        '
        Me.库存结账.Name = "库存结账"
        Me.库存结账.Size = New System.Drawing.Size(142, 22)
        Me.库存结账.Text = "库存结账"
        '
        '工资结账
        '
        Me.工资结账.Name = "工资结账"
        Me.工资结账.Size = New System.Drawing.Size(142, 22)
        Me.工资结账.Text = "工资结账"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(139, 6)
        '
        '单证审核
        '
        Me.单证审核.Name = "单证审核"
        Me.单证审核.Size = New System.Drawing.Size(142, 22)
        Me.单证审核.Text = "凭证审核"
        '
        '期间记账
        '
        Me.期间记账.Name = "期间记账"
        Me.期间记账.Size = New System.Drawing.Size(142, 22)
        Me.期间记账.Text = "期间记账"
        '
        '帐库对账
        '
        Me.帐库对账.Name = "帐库对账"
        Me.帐库对账.Size = New System.Drawing.Size(142, 22)
        Me.帐库对账.Text = "帐库对账"
        '
        '生成转账凭证
        '
        Me.生成转账凭证.Name = "生成转账凭证"
        Me.生成转账凭证.Size = New System.Drawing.Size(142, 22)
        Me.生成转账凭证.Text = "生成转账凭证"
        '
        '期末结帐
        '
        Me.期末结帐.Name = "期末结帐"
        Me.期末结帐.Size = New System.Drawing.Size(142, 22)
        Me.期末结帐.Text = "期末结帐"
        '
        '年度处理
        '
        Me.年度处理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.帐套打印})
        Me.年度处理.Name = "年度处理"
        Me.年度处理.Size = New System.Drawing.Size(83, 20)
        Me.年度处理.Text = "年度处理(&H)"
        '
        '帐套打印
        '
        Me.帐套打印.Name = "帐套打印"
        Me.帐套打印.Size = New System.Drawing.Size(118, 22)
        Me.帐套打印.Text = "帐套打印"
        '
        '高级功能
        '
        Me.高级功能.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.科目代码设置, Me.初始余额装入, Me.功能树, Me.报表树管理, Me.企业资料设置, Me.ToolStripSeparator2, Me.仓库定义, Me.用户管理, Me.特殊用户设置, Me.ToolStripSeparator10, Me.导入, Me.ToolStripSeparator11, Me.备份数据, Me.恢复数据})
        Me.高级功能.Name = "高级功能"
        Me.高级功能.Size = New System.Drawing.Size(65, 20)
        Me.高级功能.Text = "高级功能"
        '
        '科目代码设置
        '
        Me.科目代码设置.Name = "科目代码设置"
        Me.科目代码设置.Size = New System.Drawing.Size(142, 22)
        Me.科目代码设置.Text = "科目代码设置"
        '
        '初始余额装入
        '
        Me.初始余额装入.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.科目余额装入, Me.库存余额装入, Me.初始工余额装入资, Me.初始委外余额装入})
        Me.初始余额装入.Name = "初始余额装入"
        Me.初始余额装入.Size = New System.Drawing.Size(142, 22)
        Me.初始余额装入.Text = "初始余额装入"
        '
        '科目余额装入
        '
        Me.科目余额装入.Name = "科目余额装入"
        Me.科目余额装入.Size = New System.Drawing.Size(166, 22)
        Me.科目余额装入.Text = "初始科目余额装入"
        '
        '库存余额装入
        '
        Me.库存余额装入.Name = "库存余额装入"
        Me.库存余额装入.Size = New System.Drawing.Size(166, 22)
        Me.库存余额装入.Text = "初始库存余额装入"
        '
        '初始工余额装入资
        '
        Me.初始工余额装入资.Name = "初始工余额装入资"
        Me.初始工余额装入资.Size = New System.Drawing.Size(166, 22)
        Me.初始工余额装入资.Text = "初始工资余额装入"
        '
        '初始委外余额装入
        '
        Me.初始委外余额装入.Name = "初始委外余额装入"
        Me.初始委外余额装入.Size = New System.Drawing.Size(166, 22)
        Me.初始委外余额装入.Text = "初始委外余额装入"
        '
        '功能树
        '
        Me.功能树.Name = "功能树"
        Me.功能树.Size = New System.Drawing.Size(142, 22)
        Me.功能树.Text = "功能树管理"
        '
        '报表树管理
        '
        Me.报表树管理.Name = "报表树管理"
        Me.报表树管理.Size = New System.Drawing.Size(142, 22)
        Me.报表树管理.Text = "报表树管理"
        '
        '企业资料设置
        '
        Me.企业资料设置.Name = "企业资料设置"
        Me.企业资料设置.Size = New System.Drawing.Size(142, 22)
        Me.企业资料设置.Text = "企业资料设置"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(139, 6)
        '
        '仓库定义
        '
        Me.仓库定义.Name = "仓库定义"
        Me.仓库定义.Size = New System.Drawing.Size(142, 22)
        Me.仓库定义.Text = "仓库定义"
        '
        '用户管理
        '
        Me.用户管理.Name = "用户管理"
        Me.用户管理.Size = New System.Drawing.Size(142, 22)
        Me.用户管理.Text = "用户管理"
        '
        '特殊用户设置
        '
        Me.特殊用户设置.Name = "特殊用户设置"
        Me.特殊用户设置.Size = New System.Drawing.Size(142, 22)
        Me.特殊用户设置.Text = "特殊用户设置"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(139, 6)
        '
        '导入
        '
        Me.导入.Name = "导入"
        Me.导入.Size = New System.Drawing.Size(142, 22)
        Me.导入.Text = "导入功能"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(139, 6)
        '
        '备份数据
        '
        Me.备份数据.Name = "备份数据"
        Me.备份数据.Size = New System.Drawing.Size(142, 22)
        Me.备份数据.Text = "备份数据"
        '
        '恢复数据
        '
        Me.恢复数据.Name = "恢复数据"
        Me.恢复数据.Size = New System.Drawing.Size(142, 22)
        Me.恢复数据.Text = "恢复数据"
        '
        '定制开发
        '
        Me.定制开发.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.四工序机工工资})
        Me.定制开发.Name = "定制开发"
        Me.定制开发.Size = New System.Drawing.Size(65, 20)
        Me.定制开发.Text = "定制开发"
        '
        '四工序机工工资
        '
        Me.四工序机工工资.Name = "四工序机工工资"
        Me.四工序机工工资.Size = New System.Drawing.Size(154, 22)
        Me.四工序机工工资.Text = "四工序机工工资"
        '
        '帮助
        '
        Me.帮助.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.简介, Me.使用流程, Me.功能设计, Me.ToolStripSeparator7, Me.注册, Me.关于})
        Me.帮助.Name = "帮助"
        Me.帮助.Size = New System.Drawing.Size(59, 20)
        Me.帮助.Text = "帮助(&H)"
        '
        '简介
        '
        Me.简介.Name = "简介"
        Me.简介.Size = New System.Drawing.Size(118, 22)
        Me.简介.Text = "简介"
        '
        '使用流程
        '
        Me.使用流程.Name = "使用流程"
        Me.使用流程.Size = New System.Drawing.Size(118, 22)
        Me.使用流程.Text = "使用流程"
        '
        '功能设计
        '
        Me.功能设计.Name = "功能设计"
        Me.功能设计.Size = New System.Drawing.Size(118, 22)
        Me.功能设计.Text = "功能设计"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(115, 6)
        '
        '注册
        '
        Me.注册.Name = "注册"
        Me.注册.Size = New System.Drawing.Size(118, 22)
        Me.注册.Text = "注册"
        '
        '关于
        '
        Me.关于.Name = "关于"
        Me.关于.Size = New System.Drawing.Size(118, 22)
        Me.关于.Text = "关于"
        '
        '其他功能
        '
        Me.其他功能.Name = "其他功能"
        Me.其他功能.Size = New System.Drawing.Size(65, 20)
        Me.其他功能.Text = "其他功能"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(32, 98)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(166, 70)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "批量删除科目"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(32, 194)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(166, 70)
        Me.Button5.TabIndex = 0
        Me.Button5.Text = "SQLExecute"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(350, 194)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(166, 70)
        Me.Button6.TabIndex = 0
        Me.Button6.Text = "测试设置条件项"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(336, 325)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(180, 31)
        Me.Button7.TabIndex = 1
        Me.Button7.Text = "批量修改Excel"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'OtherMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(528, 397)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Name = "OtherMain"
        Me.Text = "其他功能"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents 系统设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 重新登录 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 凭证类型设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 常用摘要定义 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 打造主界面 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 退出系统 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能树管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 新建功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 修改功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 导入功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 导出功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 功能设计帮助 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 证帐输出 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 总帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 总帐余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 明细帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 日记帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存明细帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存日记帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存报表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来明细账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来日记账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来报表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资明细帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资日记帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 外存型号余额 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外明细账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外日记账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 综合查询 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 期末处理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存结账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资结账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 单证审核 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 期间记账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 帐库对账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 生成转账凭证 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 期末结帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 年度处理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 帐套打印 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 高级功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 科目代码设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 初始余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 科目余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 初始工余额装入资 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 初始委外余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能树 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 报表树管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 企业资料设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 仓库定义 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 用户管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 特殊用户设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 导入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 备份数据 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 恢复数据 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 定制开发 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 四工序机工工资 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 帮助 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 简介 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 使用流程 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能设计 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 注册 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 关于 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 其他功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button

End Class
