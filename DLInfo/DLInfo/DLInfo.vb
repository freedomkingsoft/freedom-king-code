Public Class dlInfo
    Public dlTitle As String
    Public dlName As String
    Public dlHomePic As String
    Public dlWebSite As String
    Public dlLogoPic As String
    Public dlLoginPic As String

    Public dlDownload As String
    Public dlHelp As String
    Public dlFeedback As String
    Public dlForget As String
    Public dlMyCenter As String

    Public dlFileURL As String

    Public Sub New(ByVal sDL As String)
        Select Case sDL
            Case "1" '代理编号，1代表云元汇
                dlTitle = "云元汇软件"
                dlName = "云元汇进销存财务一体化系统"
                dlHomePic = "yyh_banner.png"
                dlLogoPic = "yyhICO.png"
                dlLoginPic = "yyhLogin.png"

                dlWebSite = "http://www.nb673.com"

                'dlDownload = "https://www.bdsp.top/download.aspx?dlid=1"
                'dlHelp = "https://www.bdsp.top/help.aspx?dlid=1"
                'dlFeedback = "https://www.bdsp.top/feedback.aspx?dlid=1"
                'dlForget = "https://www.bdsp.top/iforgot.aspx?dlid=1"

                dlDownload = "download.aspx?dlid=1"
                dlHelp = "yyhhelp.aspx?dlid=1"
                dlFeedback = "feedback.aspx?dlid=1"
                dlForget = "iforgot.aspx?dlid=1"
                dlMyCenter = "mycenter.aspx?dlid=1"

                dlFileURL = "http://www.bdsp.top/download/yyh/免费试用.zip"

            Case Else   '无效参数时使用自由王数据
                dlTitle = "自由王软件"
                dlName = "自由王进销存财务一体化系统"
                dlHomePic = "app_banner.png"
                dlLogoPic = "FKLogo.png"
                dlLoginPic = "logo.png"

                dlWebSite = "http://www.bdsp.top"

                'dlDownload = "https://www.bdsp.top/download.aspx"
                'dlHelp = "https://www.bdsp.top/help.aspx"
                'dlFeedback = "https://www.bdsp.top/feedback.aspx"
                'dlForget = "https://www.bdsp.top/iforgot.aspx"

                dlDownload = "download.aspx"
                dlHelp = "help.aspx"
                dlFeedback = "feedback.aspx"
                dlForget = "iforgot.aspx"
                dlMyCenter = "mycenter.aspx"

                dlFileURL = "http://www.bdsp.top/download/自由王云进销存财务一体化.zip"
        End Select
    End Sub
End Class
