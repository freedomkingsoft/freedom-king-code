﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FreedomKingReport
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FreedomKingReport))
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.tslZHT = New System.Windows.Forms.ToolStripStatusLabel
        Me.tslYH = New System.Windows.Forms.ToolStripStatusLabel
        Me.tslZHQ = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tpDef = New System.Windows.Forms.TabPage
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.tpData = New System.Windows.Forms.TabPage
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.txtFileName = New System.Windows.Forms.TextBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.视图模式 = New System.Windows.Forms.ToolStripSplitButton
        Me.设计视图ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.数据视图ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbtnNew = New System.Windows.Forms.ToolStripButton
        Me.tsbtnOpen = New System.Windows.Forms.ToolStripButton
        Me.tsbtnSave = New System.Windows.Forms.ToolStripButton
        Me.tsbtnSaveAs = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbtnBianZhi = New System.Windows.Forms.ToolStripButton
        Me.tsbtnChaXun = New System.Windows.Forms.ToolStripButton
        Me.导出文件 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbtnInsertRow = New System.Windows.Forms.ToolStripButton
        Me.tsbtnInsertColumn = New System.Windows.Forms.ToolStripButton
        Me.tsbtnDeleteRow = New System.Windows.Forms.ToolStripButton
        Me.tsbtnDeleteColumn = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.公式向导 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripContainer2 = New System.Windows.Forms.ToolStripContainer
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.合并单元格 = New System.Windows.Forms.ToolStripButton
        Me.StatusStrip.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tpDef.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.ToolStripContainer2.BottomToolStripPanel.SuspendLayout()
        Me.ToolStripContainer2.ContentPanel.SuspendLayout()
        Me.ToolStripContainer2.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer2.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip
        '
        Me.StatusStrip.BackColor = System.Drawing.Color.LightSteelBlue
        Me.StatusStrip.Dock = System.Windows.Forms.DockStyle.None
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tslZHT, Me.tslYH, Me.tslZHQ, Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 0)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(733, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'tslZHT
        '
        Me.tslZHT.AutoSize = False
        Me.tslZHT.Margin = New System.Windows.Forms.Padding(5, 3, 20, 2)
        Me.tslZHT.Name = "tslZHT"
        Me.tslZHT.Size = New System.Drawing.Size(29, 17)
        Me.tslZHT.Text = "帐套"
        '
        'tslYH
        '
        Me.tslYH.AutoSize = False
        Me.tslYH.Margin = New System.Windows.Forms.Padding(0, 3, 20, 2)
        Me.tslYH.Name = "tslYH"
        Me.tslYH.Size = New System.Drawing.Size(29, 17)
        Me.tslYH.Text = "用户"
        '
        'tslZHQ
        '
        Me.tslZHQ.AutoSize = False
        Me.tslZHQ.Margin = New System.Windows.Forms.Padding(0, 3, 20, 2)
        Me.tslZHQ.Name = "tslZHQ"
        Me.tslZHQ.Size = New System.Drawing.Size(29, 17)
        Me.tslZHQ.Text = "帐期"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.AutoSize = False
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(35, 17)
        Me.ToolStripStatusLabel.Text = "就绪"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.Info
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFileName, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(733, 471)
        Me.TableLayoutPanel1.TabIndex = 9
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tpDef)
        Me.TabControl1.Controls.Add(Me.tpData)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(13, 83)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(707, 436)
        Me.TabControl1.TabIndex = 1
        '
        'tpDef
        '
        Me.tpDef.Controls.Add(Me.dgv)
        Me.tpDef.Location = New System.Drawing.Point(4, 21)
        Me.tpDef.Name = "tpDef"
        Me.tpDef.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDef.Size = New System.Drawing.Size(699, 411)
        Me.tpDef.TabIndex = 0
        Me.tpDef.Text = "设计视图"
        Me.tpDef.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 3)
        Me.dgv.Name = "dgv"
        Me.dgv.RowTemplate.Height = 23
        Me.dgv.Size = New System.Drawing.Size(693, 405)
        Me.dgv.TabIndex = 0
        '
        'tpData
        '
        Me.tpData.Controls.Add(Me.dgvData)
        Me.tpData.Location = New System.Drawing.Point(4, 21)
        Me.tpData.Name = "tpData"
        Me.tpData.Padding = New System.Windows.Forms.Padding(3)
        Me.tpData.Size = New System.Drawing.Size(699, 411)
        Me.tpData.TabIndex = 1
        Me.tpData.Text = "数据视图"
        Me.tpData.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(3, 3)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowTemplate.Height = 23
        Me.dgvData.Size = New System.Drawing.Size(693, 405)
        Me.dgvData.TabIndex = 1
        '
        'txtFileName
        '
        Me.txtFileName.Location = New System.Drawing.Point(13, 3)
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.Size = New System.Drawing.Size(241, 21)
        Me.txtFileName.TabIndex = 2
        Me.txtFileName.Visible = False
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.视图模式, Me.ToolStripSeparator2, Me.tsbtnNew, Me.tsbtnOpen, Me.tsbtnSave, Me.tsbtnSaveAs, Me.ToolStripSeparator1, Me.tsbtnBianZhi, Me.tsbtnChaXun, Me.导出文件, Me.ToolStripSeparator3, Me.tsbtnInsertRow, Me.tsbtnInsertColumn, Me.tsbtnDeleteRow, Me.tsbtnDeleteColumn, Me.ToolStripSeparator4, Me.公式向导})
        Me.ToolStrip1.Location = New System.Drawing.Point(3, 35)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(730, 35)
        Me.ToolStrip1.TabIndex = 2
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        '视图模式
        '
        Me.视图模式.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.设计视图ToolStripMenuItem, Me.数据视图ToolStripMenuItem})
        Me.视图模式.Image = Global.FreedomKingReport.My.Resources.Resources.image018
        Me.视图模式.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.视图模式.Name = "视图模式"
        Me.视图模式.Size = New System.Drawing.Size(69, 32)
        Me.视图模式.Text = "视图模式"
        Me.视图模式.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '设计视图ToolStripMenuItem
        '
        Me.设计视图ToolStripMenuItem.CheckOnClick = True
        Me.设计视图ToolStripMenuItem.Name = "设计视图ToolStripMenuItem"
        Me.设计视图ToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.设计视图ToolStripMenuItem.Text = "设计视图"
        '
        '数据视图ToolStripMenuItem
        '
        Me.数据视图ToolStripMenuItem.CheckOnClick = True
        Me.数据视图ToolStripMenuItem.Name = "数据视图ToolStripMenuItem"
        Me.数据视图ToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.数据视图ToolStripMenuItem.Text = "数据视图"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 35)
        '
        'tsbtnNew
        '
        Me.tsbtnNew.Image = Global.FreedomKingReport.My.Resources.Resources.image018
        Me.tsbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnNew.Name = "tsbtnNew"
        Me.tsbtnNew.Size = New System.Drawing.Size(57, 32)
        Me.tsbtnNew.Text = "新建报表"
        Me.tsbtnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsbtnOpen
        '
        Me.tsbtnOpen.Image = CType(resources.GetObject("tsbtnOpen.Image"), System.Drawing.Image)
        Me.tsbtnOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnOpen.Name = "tsbtnOpen"
        Me.tsbtnOpen.Size = New System.Drawing.Size(81, 32)
        Me.tsbtnOpen.Text = "打开报表格式"
        Me.tsbtnOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsbtnSave
        '
        Me.tsbtnSave.Image = Global.FreedomKingReport.My.Resources.Resources.image003
        Me.tsbtnSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnSave.Name = "tsbtnSave"
        Me.tsbtnSave.Size = New System.Drawing.Size(81, 32)
        Me.tsbtnSave.Text = "保存报表格式"
        Me.tsbtnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsbtnSaveAs
        '
        Me.tsbtnSaveAs.Image = CType(resources.GetObject("tsbtnSaveAs.Image"), System.Drawing.Image)
        Me.tsbtnSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnSaveAs.Name = "tsbtnSaveAs"
        Me.tsbtnSaveAs.Size = New System.Drawing.Size(81, 32)
        Me.tsbtnSaveAs.Text = "另存报表格式"
        Me.tsbtnSaveAs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 35)
        '
        'tsbtnBianZhi
        '
        Me.tsbtnBianZhi.Image = CType(resources.GetObject("tsbtnBianZhi.Image"), System.Drawing.Image)
        Me.tsbtnBianZhi.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnBianZhi.Name = "tsbtnBianZhi"
        Me.tsbtnBianZhi.Size = New System.Drawing.Size(57, 32)
        Me.tsbtnBianZhi.Text = "编制报表"
        Me.tsbtnBianZhi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.tsbtnBianZhi.ToolTipText = "编制报表"
        '
        'tsbtnChaXun
        '
        Me.tsbtnChaXun.Image = CType(resources.GetObject("tsbtnChaXun.Image"), System.Drawing.Image)
        Me.tsbtnChaXun.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnChaXun.Name = "tsbtnChaXun"
        Me.tsbtnChaXun.Size = New System.Drawing.Size(57, 32)
        Me.tsbtnChaXun.Text = "查询报表"
        Me.tsbtnChaXun.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.tsbtnChaXun.ToolTipText = "编制报表"
        '
        '导出文件
        '
        Me.导出文件.Image = CType(resources.GetObject("导出文件.Image"), System.Drawing.Image)
        Me.导出文件.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.导出文件.Name = "导出文件"
        Me.导出文件.Size = New System.Drawing.Size(57, 32)
        Me.导出文件.Text = "导出报表"
        Me.导出文件.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 35)
        '
        'tsbtnInsertRow
        '
        Me.tsbtnInsertRow.Image = CType(resources.GetObject("tsbtnInsertRow.Image"), System.Drawing.Image)
        Me.tsbtnInsertRow.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnInsertRow.Name = "tsbtnInsertRow"
        Me.tsbtnInsertRow.Size = New System.Drawing.Size(45, 32)
        Me.tsbtnInsertRow.Text = "插入行"
        Me.tsbtnInsertRow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsbtnInsertColumn
        '
        Me.tsbtnInsertColumn.Image = CType(resources.GetObject("tsbtnInsertColumn.Image"), System.Drawing.Image)
        Me.tsbtnInsertColumn.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnInsertColumn.Name = "tsbtnInsertColumn"
        Me.tsbtnInsertColumn.Size = New System.Drawing.Size(45, 32)
        Me.tsbtnInsertColumn.Text = "插入列"
        Me.tsbtnInsertColumn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsbtnDeleteRow
        '
        Me.tsbtnDeleteRow.Image = CType(resources.GetObject("tsbtnDeleteRow.Image"), System.Drawing.Image)
        Me.tsbtnDeleteRow.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnDeleteRow.Name = "tsbtnDeleteRow"
        Me.tsbtnDeleteRow.Size = New System.Drawing.Size(45, 32)
        Me.tsbtnDeleteRow.Text = "删除行"
        Me.tsbtnDeleteRow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsbtnDeleteColumn
        '
        Me.tsbtnDeleteColumn.Image = CType(resources.GetObject("tsbtnDeleteColumn.Image"), System.Drawing.Image)
        Me.tsbtnDeleteColumn.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbtnDeleteColumn.Name = "tsbtnDeleteColumn"
        Me.tsbtnDeleteColumn.Size = New System.Drawing.Size(45, 32)
        Me.tsbtnDeleteColumn.Text = "删除列"
        Me.tsbtnDeleteColumn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 35)
        '
        '公式向导
        '
        Me.公式向导.Image = CType(resources.GetObject("公式向导.Image"), System.Drawing.Image)
        Me.公式向导.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.公式向导.Name = "公式向导"
        Me.公式向导.Size = New System.Drawing.Size(57, 32)
        Me.公式向导.Text = "公式向导"
        Me.公式向导.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripContainer2
        '
        '
        'ToolStripContainer2.BottomToolStripPanel
        '
        Me.ToolStripContainer2.BottomToolStripPanel.Controls.Add(Me.StatusStrip)
        '
        'ToolStripContainer2.ContentPanel
        '
        Me.ToolStripContainer2.ContentPanel.Controls.Add(Me.TableLayoutPanel1)
        Me.ToolStripContainer2.ContentPanel.Size = New System.Drawing.Size(733, 471)
        Me.ToolStripContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStripContainer2.LeftToolStripPanelVisible = False
        Me.ToolStripContainer2.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripContainer2.Name = "ToolStripContainer2"
        Me.ToolStripContainer2.RightToolStripPanelVisible = False
        Me.ToolStripContainer2.Size = New System.Drawing.Size(733, 563)
        Me.ToolStripContainer2.TabIndex = 10
        Me.ToolStripContainer2.Text = "ToolStripContainer2"
        '
        'ToolStripContainer2.TopToolStripPanel
        '
        Me.ToolStripContainer2.TopToolStripPanel.Controls.Add(Me.ToolStrip2)
        Me.ToolStripContainer2.TopToolStripPanel.Controls.Add(Me.ToolStrip1)
        '
        'ToolStrip2
        '
        Me.ToolStrip2.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton3, Me.ToolStripButton2, Me.ToolStripButton1, Me.合并单元格})
        Me.ToolStrip2.Location = New System.Drawing.Point(3, 0)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(204, 35)
        Me.ToolStrip2.TabIndex = 9
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(45, 32)
        Me.ToolStripButton3.Text = "左对齐"
        Me.ToolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(33, 32)
        Me.ToolStripButton2.Text = "居中"
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(45, 32)
        Me.ToolStripButton1.Text = "右对齐"
        Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '合并单元格
        '
        Me.合并单元格.Image = CType(resources.GetObject("合并单元格.Image"), System.Drawing.Image)
        Me.合并单元格.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.合并单元格.Name = "合并单元格"
        Me.合并单元格.Size = New System.Drawing.Size(69, 32)
        Me.合并单元格.Text = "合并单元格"
        Me.合并单元格.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'FreedomKingReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(733, 563)
        Me.Controls.Add(Me.ToolStripContainer2)
        Me.Name = "FreedomKingReport"
        Me.Text = "自由王报表"
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tpDef.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ToolStripContainer2.BottomToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer2.BottomToolStripPanel.PerformLayout()
        Me.ToolStripContainer2.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer2.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer2.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer2.ResumeLayout(False)
        Me.ToolStripContainer2.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents tslZHT As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslYH As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslZHQ As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpDef As System.Windows.Forms.TabPage
    Friend WithEvents tpData As System.Windows.Forms.TabPage
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbtnNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbtnOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbtnSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbtnSaveAs As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbtnInsertRow As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbtnInsertColumn As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbtnDeleteRow As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbtnDeleteColumn As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripContainer2 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 公式向导 As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 合并单元格 As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtFileName As System.Windows.Forms.TextBox
    Friend WithEvents 导出文件 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbtnBianZhi As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbtnChaXun As System.Windows.Forms.ToolStripButton
    Friend WithEvents 视图模式 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents 设计视图ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 数据视图ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
End Class
