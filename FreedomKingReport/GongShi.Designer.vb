﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GongShi
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbLYB1 = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cbKMDM = New System.Windows.Forms.ComboBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.cbYueFen = New System.Windows.Forms.ComboBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cbJDFX = New System.Windows.Forms.ComboBox
        Me.txtGongShi = New System.Windows.Forms.TextBox
        Me.btnAdd = New System.Windows.Forms.Button
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.rb1 = New System.Windows.Forms.RadioButton
        Me.rb2 = New System.Windows.Forms.RadioButton
        Me.rb3 = New System.Windows.Forms.RadioButton
        Me.rb4 = New System.Windows.Forms.RadioButton
        Me.rb5 = New System.Windows.Forms.RadioButton
        Me.Done = New System.Windows.Forms.Button
        Me.cbYJ = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox2, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox3, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtGongShi, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAdd, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Done, 1, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(474, 380)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbLYB1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(3, 83)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(231, 116)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "选择数据来源"
        '
        'cbLYB1
        '
        Me.cbLYB1.AutoSize = True
        Me.cbLYB1.Checked = True
        Me.cbLYB1.Location = New System.Drawing.Point(19, 31)
        Me.cbLYB1.Name = "cbLYB1"
        Me.cbLYB1.Size = New System.Drawing.Size(83, 16)
        Me.cbLYB1.TabIndex = 0
        Me.cbLYB1.TabStop = True
        Me.cbLYB1.Text = "总账余额表"
        Me.cbLYB1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbYJ)
        Me.GroupBox2.Controls.Add(Me.cbKMDM)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(240, 83)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(231, 116)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "选择科目代码"
        '
        'cbKMDM
        '
        Me.cbKMDM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbKMDM.FormattingEnabled = True
        Me.cbKMDM.Location = New System.Drawing.Point(12, 61)
        Me.cbKMDM.Name = "cbKMDM"
        Me.cbKMDM.Size = New System.Drawing.Size(193, 20)
        Me.cbKMDM.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cbYueFen)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(3, 205)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(231, 116)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "选择月份"
        '
        'cbYueFen
        '
        Me.cbYueFen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbYueFen.FormattingEnabled = True
        Me.cbYueFen.Items.AddRange(New Object() {"0_本月", "1_一月", "2_二月", "3_三月", "4_四月", "5_五月", "6_六月", "7_七月", "8_八月", "9_九月", "10_十月", "11_十一月", "12_十二月", "13_上年本月", ""})
        Me.cbYueFen.Location = New System.Drawing.Point(16, 28)
        Me.cbYueFen.Name = "cbYueFen"
        Me.cbYueFen.Size = New System.Drawing.Size(193, 20)
        Me.cbYueFen.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cbJDFX)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox3.Location = New System.Drawing.Point(240, 205)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(231, 116)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "选择借贷方向"
        '
        'cbJDFX
        '
        Me.cbJDFX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbJDFX.FormattingEnabled = True
        Me.cbJDFX.Items.AddRange(New Object() {"CJ_期初借方余额", "CD_期初贷方余额", "MJ_期末借方余额", "MD_期末贷方余额", "JF_借方发生额", "DF_贷方发生额", "JL_借方累计发生额", "DL_贷方累计发生额"})
        Me.cbJDFX.Location = New System.Drawing.Point(13, 28)
        Me.cbJDFX.Name = "cbJDFX"
        Me.cbJDFX.Size = New System.Drawing.Size(192, 20)
        Me.cbJDFX.TabIndex = 0
        '
        'txtGongShi
        '
        Me.txtGongShi.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtGongShi, 2)
        Me.txtGongShi.Location = New System.Drawing.Point(3, 4)
        Me.txtGongShi.Name = "txtGongShi"
        Me.txtGongShi.Size = New System.Drawing.Size(468, 21)
        Me.txtGongShi.TabIndex = 1
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAdd.Location = New System.Drawing.Point(59, 334)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(118, 35)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "增加"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel2.ColumnCount = 5
        Me.TableLayoutPanel1.SetColumnSpan(Me.TableLayoutPanel2, 2)
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.rb1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.rb2, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.rb3, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.rb4, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.rb5, 4, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 34)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(468, 41)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'rb1
        '
        Me.rb1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rb1.AutoSize = True
        Me.rb1.Checked = True
        Me.rb1.Location = New System.Drawing.Point(32, 12)
        Me.rb1.Name = "rb1"
        Me.rb1.Size = New System.Drawing.Size(29, 16)
        Me.rb1.TabIndex = 0
        Me.rb1.TabStop = True
        Me.rb1.Text = "="
        Me.rb1.UseVisualStyleBackColor = True
        '
        'rb2
        '
        Me.rb2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rb2.AutoSize = True
        Me.rb2.Location = New System.Drawing.Point(125, 12)
        Me.rb2.Name = "rb2"
        Me.rb2.Size = New System.Drawing.Size(29, 16)
        Me.rb2.TabIndex = 0
        Me.rb2.Text = "+"
        Me.rb2.UseVisualStyleBackColor = True
        '
        'rb3
        '
        Me.rb3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rb3.AutoSize = True
        Me.rb3.Location = New System.Drawing.Point(218, 12)
        Me.rb3.Name = "rb3"
        Me.rb3.Size = New System.Drawing.Size(29, 16)
        Me.rb3.TabIndex = 0
        Me.rb3.Text = "-"
        Me.rb3.UseVisualStyleBackColor = True
        '
        'rb4
        '
        Me.rb4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rb4.AutoSize = True
        Me.rb4.Location = New System.Drawing.Point(311, 12)
        Me.rb4.Name = "rb4"
        Me.rb4.Size = New System.Drawing.Size(29, 16)
        Me.rb4.TabIndex = 0
        Me.rb4.Text = "*"
        Me.rb4.UseVisualStyleBackColor = True
        '
        'rb5
        '
        Me.rb5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.rb5.AutoSize = True
        Me.rb5.Location = New System.Drawing.Point(405, 12)
        Me.rb5.Name = "rb5"
        Me.rb5.Size = New System.Drawing.Size(29, 16)
        Me.rb5.TabIndex = 0
        Me.rb5.Text = "/"
        Me.rb5.UseVisualStyleBackColor = True
        '
        'Done
        '
        Me.Done.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Done.Location = New System.Drawing.Point(294, 334)
        Me.Done.Name = "Done"
        Me.Done.Size = New System.Drawing.Size(122, 35)
        Me.Done.TabIndex = 3
        Me.Done.Text = "完成"
        Me.Done.UseVisualStyleBackColor = True
        '
        'cbYJ
        '
        Me.cbYJ.AutoSize = True
        Me.cbYJ.Checked = True
        Me.cbYJ.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbYJ.Location = New System.Drawing.Point(13, 31)
        Me.cbYJ.Name = "cbYJ"
        Me.cbYJ.Size = New System.Drawing.Size(108, 16)
        Me.cbYJ.TabIndex = 1
        Me.cbYJ.Text = "只显示一级科目"
        Me.cbYJ.UseVisualStyleBackColor = True
        '
        'GongShi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(474, 380)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GongShi"
        Me.Text = "公式向导"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cbLYB1 As System.Windows.Forms.RadioButton
    Friend WithEvents txtGongShi As System.Windows.Forms.TextBox
    Friend WithEvents cbKMDM As System.Windows.Forms.ComboBox
    Friend WithEvents cbYueFen As System.Windows.Forms.ComboBox
    Friend WithEvents cbJDFX As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents rb1 As System.Windows.Forms.RadioButton
    Friend WithEvents rb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb3 As System.Windows.Forms.RadioButton
    Friend WithEvents rb4 As System.Windows.Forms.RadioButton
    Friend WithEvents rb5 As System.Windows.Forms.RadioButton
    Friend WithEvents Done As System.Windows.Forms.Button
    Friend WithEvents cbYJ As System.Windows.Forms.CheckBox
End Class
