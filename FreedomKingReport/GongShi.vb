﻿Public Class GongShi

    Private Sub GongShi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        'FillKMDM()
        Me.cbYueFen.SelectedIndex = 0
        Me.cbJDFX.SelectedIndex = 0
    End Sub

    'Private Sub FillKMDM()
    '    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
    '    Dim sSQL As String
    '    If cbYJ.Checked Then
    '        sSQL = "select KMDM & '__' & KMMC from KMDM where len(KMDM)= " & CType(FKG.myselfG.sKMJS.Substring(0, 1), Integer) & " and Nian = " & FKG.myselfG.NianFen
    '    Else
    '        sSQL = "select KMDM & '__' & KMMC from KMDM where Nian=" & FKG.myselfG.NianFen
    '    End If

    '    mdb.DataBind(Me.cbKMDM, sSQL)
    'End Sub

   
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim sGongShi As String = ""
        If Me.rb1.Checked Then
            sGongShi = "="
        ElseIf Me.rb2.Checked Then
            sGongShi = "+"
        ElseIf Me.rb3.Checked Then
            sGongShi = "-"
        ElseIf Me.rb4.Checked Then
            sGongShi = "*"
        ElseIf Me.rb5.Checked Then
            sGongShi = "/"
        End If

        '来源表
        If Me.cbLYB1.Checked Then
            sGongShi = sGongShi & "ZZ("
        End If

        '科目代码
        sGongShi = sGongShi & Split(Me.cbKMDM.Text, "__").GetValue(0)
        '月份
        sGongShi = sGongShi & "," & Split(Me.cbYueFen.Text, "_").GetValue(0)
        '借贷方向
        sGongShi = sGongShi & "," & Split(Me.cbJDFX.Text, "_").GetValue(0) & ")"

        Me.txtGongShi.Text = Me.txtGongShi.Text & sGongShi.Replace(" ", "")

        Me.cbKMDM.SelectedIndex = 0
        Me.cbYueFen.SelectedIndex = 0
        Me.cbJDFX.SelectedIndex = 0
    End Sub

    Private Sub Done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Done.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub cbYJ_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbYJ.CheckedChanged
        'Me.cbKMDM.Items.Clear()

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sSQL As String
        If cbYJ.Checked Then
            sSQL = "select KMDM & '__' & KMMC from KMDM where len(KMDM)= " & CType(FKG.myselfG.sKMJS.Substring(0, 1), Integer) & " and Nian = " & FKG.myselfG.NianFen
        Else
            sSQL = "select KMDM & '__' & KMMC from KMDM where Nian=" & FKG.myselfG.NianFen
        End If

        mdb.DataBind(Me.cbKMDM, sSQL)

        Me.cbKMDM.SelectedIndex = 0

    End Sub

   
End Class