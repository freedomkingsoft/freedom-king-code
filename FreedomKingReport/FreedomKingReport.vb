﻿Public Class FreedomKingReport

    Public dlgLogin As Global.FreedomKingReport.Login

    ' Private sFileName As String = "" '当前正在操作的报表格式文件
    '改为使用一个TextBox保存，方便在更改操作文件时改变标题

    Public iBByue As Integer = FKG.myselfG.YueFen '报表月份为启动时登陆的月份


    Private Sub FreedomKingReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MsgBox("您确定要退出" & My.Application.Info.Title, MsgBoxStyle.YesNo, "提示") = MsgBoxResult.Yes Then
            End
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub FreedomKingReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        '设置状态栏
        Me.tslZHT.Text = "帐套:" & dlgLogin.lblQiyeming.Text
        Me.tslZHT.Width = Me.Width * 0.28
        Me.tslYH.Text = "用户:" & dlgLogin.txtYongHu.Text
        Me.tslYH.Width = Me.Width * 0.28
        Me.tslZHQ.Text = "登录日期:" & dlgLogin.dtpLogin.Text
        Me.tslZHQ.Width = Me.Width * 0.28

        '        tsbtnNew_Click(sender, e)

        'Me.导出文件.Enabled = False

        'Me.TabControl1.TabPages.Remove(Me.tpData)
        'Me.TabControl1.TabPages.Remove(Me.tpDef)

        Me.数据视图ToolStripMenuItem.Checked = True
    End Sub

    Private Sub FreedomKingReport_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        '设置状态栏
        Me.tslZHT.Width = Me.Width * 0.28
        Me.tslYH.Width = Me.Width * 0.28
        Me.tslZHQ.Width = Me.Width * 0.28

    End Sub

    Private Sub dgv_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgv.CellMouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then
            Me.dgv.CurrentCell = Me.dgv.Item(e.ColumnIndex, e.RowIndex)
            公式向导_Click(sender, e)
        End If
    End Sub

    Private Sub dgv_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
        For xh As Integer = 0 To dgv.Rows.Count - 1
            If e.ColumnIndex = 0 AndAlso e.RowIndex = xh Then
                e.Graphics.DrawString(System.Convert.ToString(xh + 1), Me.dgv.Font, Brushes.Red, 20, e.CellBounds.Top + 6)
            End If
        Next
        e.Graphics.DrawString("序号", Me.dgv.Font, Brushes.Black, 8, 5)
    End Sub

    Private Sub SaveDGV()
        Dim dt As New DataSet("myDT")
        dt.Tables.Add("Table1")

        Dim i As Integer
        For i = 0 To Me.dgv.ColumnCount - 1
            dt.Tables(0).Columns.Add(Me.dgv.Columns(i).Name)
        Next
        For i = 0 To Me.dgv.RowCount - 1
            dt.Tables(0).Rows.Add()
        Next

        Dim n As Integer
        For n = 0 To Me.dgv.RowCount - 1
            For i = 0 To Me.dgv.ColumnCount - 1
                dt.Tables(0).Rows(n).Item(i) = Me.dgv.Item(i, n).Value
            Next
        Next
        If Me.txtFileName.Text = "" Then
            SetFileName()
            If Me.txtFileName.Text = "" Then
                Exit Sub
            End If
            IO.File.Delete(Me.txtFileName.Text & "schema")
            IO.File.Delete(Me.txtFileName.Text)

            dt.WriteXmlSchema(Me.txtFileName.Text & "schema")
            dt.WriteXml(Me.txtFileName.Text)
        Else
            IO.File.Delete(Me.txtFileName.Text & "schema")
            IO.File.Delete(Me.txtFileName.Text)

            dt.WriteXmlSchema(Me.txtFileName.Text & "schema")
            dt.WriteXml(Me.txtFileName.Text)
        End If
    End Sub

    Private Sub LoadDGV()
        Dim dt As New DataSet
        GetFileName()
        If Me.txtFileName.Text = "" Then
            Exit Sub
        Else
            Try
                dt.ReadXmlSchema(Me.txtFileName.Text & "schema")
                dt.ReadXml(Me.txtFileName.Text)
            Catch ex As Exception
                Exit Sub
            End Try

            Me.dgv.DataSource = Nothing
            Me.dgv.Update()

            Me.dgv.ColumnCount = dt.Tables(0).Columns.Count
            Me.dgv.RowCount = dt.Tables(0).Rows.Count

            Dim i As Integer
            Dim n As Integer
            For i = 0 To dt.Tables(0).Columns.Count - 1
                Me.dgv.Columns(i).Name = dt.Tables(0).Columns(i).ColumnName
            Next
            For n = 0 To dt.Tables(0).Rows.Count - 1
                For i = 0 To dt.Tables(0).Columns.Count - 1
                    Me.dgv.Rows(n).Cells(i).Value = dt.Tables(0).Rows(n).Item(i)
                Next
            Next
        End If
    End Sub

    Private Sub SetFileName()
        Dim dlg As New System.Windows.Forms.SaveFileDialog
        dlg.Filter = "自由王报表格式文件|*.rpt"
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtFileName.Text = dlg.FileName
        End If
    End Sub

    Private Sub GetFileName()
        Dim dlg As New System.Windows.Forms.OpenFileDialog
        dlg.Filter = "自由王报表格式文件|*.rpt"
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtFileName.Text = dlg.FileName
        End If
    End Sub

    Private Sub tsbtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnSave.Click
        SaveDGV()
    End Sub

    Private Sub tsbtnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnOpen.Click
        LoadDGV()

        Me.SetColumnWidth(Me.dgv)
    End Sub

    Private Sub tsbtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnNew.Click
        Me.txtFileName.Text = ""

        Me.dgv.DataSource = Nothing
        Me.dgv.Update()

        Me.dgv.Columns.Clear()

        Me.dgv.ColumnCount = 8
        ReNamCol()
        Me.dgv.RowCount = 5

        Me.SetColumnWidth(Me.dgv)
    End Sub

    Private Function AddLieName(ByVal sLastName As String) As String
        Dim sLastChr As String
        If sLastName.Length = 0 Then
            Return "A"
        End If
        sLastChr = sLastName.Substring(sLastName.Length - 1, 1)
       
        If sLastChr = "Z" Then
            Return AddLieName(sLastName.Remove(sLastName.Length - 1, 1)) & "A"
        Else
            Return sLastName.Remove(sLastName.Length - 1, 1) & Chr(Asc(sLastName.Substring(sLastName.Length - 1, 1)) + 1)
        End If
    End Function



    Private Sub ReNamCol()
        If Me.dgv.Columns.Count = 0 Then
            Exit Sub
        End If
        Me.dgv.Columns(0).Name = "A"
        Me.dgv.Columns(0).HeaderText = "A"
        Me.dgv.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable

        Dim i As Integer
        For i = 1 To Me.dgv.ColumnCount - 1
            Me.dgv.Columns(i).Name = AddLieName(Me.dgv.Columns(i - 1).Name)
            Me.dgv.Columns(i).HeaderText = Me.dgv.Columns(i).Name
            Me.dgv.Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
        Next
    End Sub

    Private Sub tsbtnInsertRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnInsertRow.Click
        If IsNothing(Me.dgv.CurrentCell) Then
            Exit Sub
        End If

        If Me.dgv.RowCount = 0 Then
            Me.dgv.Rows.Insert(0, 1)
        Else
            Me.dgv.Rows.Insert(Me.dgv.CurrentCell.RowIndex, 1)
        End If
    End Sub

    Private Sub tsbtnInsertColumn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnInsertColumn.Click
        If IsNothing(Me.dgv.CurrentCell) Then
            Exit Sub
        End If

        Dim newColumn As New DataGridViewTextBoxColumn
        If Me.dgv.ColumnCount = 0 Then
            Me.dgv.Columns.Insert(0, newColumn)
        Else
            Me.dgv.Columns.Insert(Me.dgv.CurrentCell.ColumnIndex, newColumn)
        End If

        ReNamCol()
        Me.SetColumnWidth(Me.dgv)
    End Sub

    Private Sub tsbtnDeleteRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnDeleteRow.Click
        If Me.dgv.RowCount = 0 OrElse IsNothing(Me.dgv.CurrentCell) Then
            Exit Sub
        End If
        Me.dgv.Rows.RemoveAt(Me.dgv.CurrentCell.RowIndex)

    End Sub

    Private Sub tsbtnDeleteColumn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnDeleteColumn.Click
        If Me.dgv.ColumnCount = 0 OrElse IsNothing(Me.dgv.CurrentCell) Then
            Exit Sub
        End If
        Me.dgv.Columns.RemoveAt(Me.dgv.CurrentCell.ColumnIndex)
        
        ReNamCol()
        Me.SetColumnWidth(Me.dgv)

    End Sub

  
    Private Sub 公式向导_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 公式向导.Click
        Dim DLG As New GongShi
        If DLG.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.dgv.CurrentCell.Value = DLG.txtGongShi.Text
        End If
    End Sub

    Private Function FKGongShi(ByVal sGongshi As String) As Object
        Dim sBiao As String = ""

        Select Case Split(sGongshi, "(").GetValue(0)
            Case "ZZ"
                sBiao = "科目余额表"
                sGongshi = sGongshi.Replace("ZZ(", "").Replace(")", "")

                Select Case Split(sGongshi, ",").GetValue(2)
                    Case "CJ"
                        Return FKZZ(sGongshi) - Me.FKZZ(sGongshi.ToUpper.Replace("CJ", "CD"))
                    Case "CD"
                        Return FKZZ(sGongshi) - Me.FKZZ(sGongshi.ToUpper.Replace("CD", "CJ"))
                    Case "MJ"
                        Return FKZZ(sGongshi) - Me.FKZZ(sGongshi.ToUpper.Replace("MJ", "MD"))
                    Case "MD"
                        Return FKZZ(sGongshi) - Me.FKZZ(sGongshi.ToUpper.Replace("MD", "MJ"))
                    Case Else
                        Return FKZZ(sGongshi)
                End Select

                'Return FKZZ(sGongshi)
            Case Else
                Return sGongshi
        End Select

    End Function

    Private Function FKZZ(ByVal sGongshi As String) As Object
        Dim sBiao As String = "科目余额表"
        Dim sKMDM As String
        Dim iYue As Int16
        Dim sJDFX As String
        sKMDM = Split(sGongshi, ",").GetValue(0)
        iYue = Split(sGongshi, ",").GetValue(1)
        sJDFX = Split(sGongshi, ",").GetValue(2)

        Dim sLieName As String
        Select Case sJDFX
            Case "CJ"
                If iYue = 0 Or iYue = 13 Then
                    If iBByue = 1 Then
                        sLieName = "年初借方余额"
                    Else
                        sLieName = iBByue - 1 & "月借方期末余额"
                    End If
                Else
                    If iYue = 1 Then
                        sLieName = "年初借方余额"
                    Else
                        sLieName = iYue - 1 & "月借方期末余额"
                    End If
                End If
            Case "CD"
                If iYue = 0 Or iYue = 13 Then
                    If iBByue = 1 Then
                        sLieName = "年初贷方余额"
                    Else
                        sLieName = iBByue - 1 & "月贷方期末余额"
                    End If
                Else
                    If iYue = 1 Then
                        sLieName = "年初贷方余额"
                    Else
                        sLieName = iYue - 1 & "月贷方期末余额"
                    End If
                End If
            Case "MJ"
                If iYue = 0 Or iYue = 13 Then
                    sLieName = iBByue & "月借方期末余额"
                Else
                    sLieName = iYue & "月借方期末余额"
                End If
            Case "MD"
                If iYue = 0 Or iYue = 13 Then
                    sLieName = iBByue & "月贷方期末余额"
                Else
                    sLieName = iYue & "月贷方期末余额"
                End If
            Case "JF"
                If iYue = 0 Or iYue = 13 Then
                    sLieName = iBByue & "月借方发生额"
                Else
                    sLieName = iYue & "月借方发生额"
                End If
            Case "DF"
                If iYue = 0 Or iYue = 13 Then
                    sLieName = iBByue & "月贷方发生额"
                Else
                    sLieName = iYue & "月贷方发生额"
                End If
            Case "JL"
                If iYue = 0 Or iYue = 13 Then
                    sLieName = iBByue & "月借方累计发生额"
                Else
                    sLieName = iYue & "月借方累计发生额"
                End If
            Case "DL"
                If iYue = 0 Or iYue = 13 Then
                    sLieName = iBByue & "月贷方累计发生额"
                Else
                    sLieName = iYue & "月贷方累计发生额"
                End If
            Case Else
                sLieName = ""
        End Select

        Dim sSQL As String
        If iYue = 13 Then
            sSQL = "select iif(isNull([" & sLieName & "]),0,[" & sLieName & "]) from " & sBiao & " where 科目代码='" & sKMDM & "' and 年份=" & FKG.myselfG.NianFen - 1
        Else
            sSQL = "select iif(isNull([" & sLieName & "]),0,[" & sLieName & "]) from " & sBiao & " where 科目代码='" & sKMDM & "' and 年份=" & FKG.myselfG.NianFen
        End If

        Dim ds As New DataSet
        mdb.oleDataAdapter.SelectCommand.CommandText = sSQL

        If mdb.oleConnect.State = ConnectionState.Open Then
            mdb.oleDataAdapter.Fill(ds)
        Else
            MDBOpen()
            mdb.oleDataAdapter.Fill(ds)
        End If
        Return ds.Tables(0).Rows(0).Item(0)
        'Return mdb.Reader(sSQL).Tables(0).Rows(0).Item(0)
    End Function

    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
    Private Sub MDBOpen()
        Try
            mdb.oleConnect.Open()
        Catch ex As Exception

        End Try

    End Sub

    Private Function GongShiGeShi(ByVal sName As String) As Integer
        Select Case Split(sName, "(").GetValue(0)
            Case "ZZ"
                Return 1 '总账函数
            Case Else

        End Select
        If isCellAddress(sName) Then
            Return 2 '引用单元格
        End If
        Return 3 '直接返回
    End Function

    Private Function GetCol(ByVal sName As String) As String
        Dim sCol As String = ""
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If (sName.Substring(i, 1) >= "A" And sName.Substring(i, 1) <= "Z") OrElse (sName.Substring(i, 1) >= "a" And sName.Substring(i, 1) <= "z") Then
                sCol = sCol & sName.Substring(i, 1).ToUpper
            End If
        Next
        Return sCol
    End Function

    Private Function GetRow(ByVal sName As String) As Integer
        Dim iRow As Integer = 0
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If sName.Substring(i, 1) >= "0" And sName.Substring(i, 1) <= "9" Then
                iRow = iRow * 10 + CType(sName.Substring(i, 1), Integer)
            End If
        Next
        Return iRow - 1
    End Function

    Private Function isCellAddress(ByVal sName As String) As Boolean
        Dim bCol As Boolean = False
        Dim bRow As Boolean = False
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If (sName.Substring(i, 1) >= "A" And sName.Substring(i, 1) <= "Z" And bRow = False) OrElse (sName.Substring(i, 1) >= "a" And sName.Substring(i, 1) <= "z" And bRow = False) Then
                bCol = True
            ElseIf sName.Substring(i, 1) >= "0" And sName.Substring(i, 1) <= "9" And bCol = True Then
                bCol = True
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Function YuanSuan(ByVal sGongshi As Object) As Object
        If IsDBNull(sGongshi) OrElse sGongshi = "" Then
            Return ""
        End If

        Dim ssGS(0) As Object
        Dim i As Integer
        For i = 0 To sGongshi.ToString.Length - 1
            If sGongshi.ToString.Substring(i, 1) = "+" OrElse sGongshi.ToString.Substring(i, 1) = "-" OrElse sGongshi.ToString.Substring(i, 1) = "*" OrElse sGongshi.ToString.Substring(i, 1) = "/" Then
                Array.Resize(ssGS, ssGS.Length + 1)
                ssGS(ssGS.Length - 1) = sGongshi.ToString.Substring(i, 1)
                Array.Resize(ssGS, ssGS.Length + 1)
            Else
                ssGS(ssGS.Length - 1) = ssGS(ssGS.Length - 1) & sGongshi.ToString.Substring(i, 1)
            End If
        Next

        Try
            '先进行运算，然后再按照优先级进行四则运算
            For i = 0 To ssGS.Length - 1 Step 2
                Select Case GongShiGeShi(ssGS(i))
                    Case 1
                        ssGS(i) = FKGongShi(ssGS(i))
                    Case 2
                        Try
                            ssGS(i) = Me.dgvData.Rows(GetRow(ssGS(i))).Cells(GetCol(ssGS(i))).Value
                        Catch exOver As System.StackOverflowException
                            MsgBox("您设计的公式中存在循环引用，请检查！", MsgBoxStyle.Information, "提示")
                            ssGS(i) = 0
                        Catch ex As Exception

                        End Try
                    Case 3
                        'Return sGongshi
                    Case Else
                        Return ""
                End Select
            Next
            '先算乘除、后算加减
            For i = 1 To ssGS.Length - 1 Step 2
                If i > ssGS.Length - 1 Then
                    Exit For
                End If


                If ssGS(i) = "*" OrElse ssGS(i) = "/" Then
                    If ssGS(i) = "*" Then
                        ssGS(i - 1) = ssGS(i - 1) * ssGS(i + 1)
                    Else
                        ssGS(i - 1) = ssGS(i - 1) / ssGS(i + 1)
                    End If

                    Array.ConstrainedCopy(ssGS, i + 2, ssGS, i, ssGS.Length - i - 2)
                    Array.Resize(ssGS, ssGS.Length - 2)
                    i = i - 2
                End If
            Next

            For i = 1 To ssGS.Length - 1 Step 2
                If ssGS(i) = "+" Then
                    Try
                        ssGS(0) = CType(ssGS(0), Decimal) + CType(ssGS(i + 1), Decimal)
                    Catch ex As Exception
                        ssGS(0) = ssGS(0) + ssGS(i + 1)
                    End Try

                End If
                If ssGS(i) = "-" Then
                    ssGS(0) = ssGS(0) - ssGS(i + 1)
                End If
            Next
            Return ssGS(0)
        Catch ex As Exception
            'MsgBox("公式错误！", MsgBoxStyle.Information, "提示")
            Return "公式错误"
        End Try
       
    End Function

  
    Private Sub tpData_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tpData.Enter
        Me.dgvData.Columns.Clear()
        Me.dgvData.Rows.Clear()

        Dim i As Integer
        For i = 0 To Me.dgv.Columns.Count - 1
            Me.dgvData.Columns.Add(Me.dgv.Columns(i).Name, Me.dgv.Columns(i).HeaderText)
            Me.dgvData.Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
        Next
        Me.dgvData.Rows.Add(Me.dgv.Rows.Count)

        MDBOpen()
        '打开数据库连接并且保持打开状态

        Dim irow As Integer
        Dim icol As Integer
        For irow = 0 To Me.dgv.RowCount - 1
            For icol = 0 To Me.dgv.ColumnCount - 1
                If IsDBNull(Me.dgv.Rows(irow).Cells(icol).Value) OrElse IsNothing(Me.dgv.Rows(irow).Cells(icol).Value) OrElse Me.dgv.Rows(irow).Cells(icol).Value = "" Then
                    Me.dgvData.Rows(irow).Cells(icol).Value = ""
                Else
                    If Me.dgv.Rows(irow).Cells(icol).Value.ToString.Substring(0, 1) = "=" Then

                        Me.dgvData.Rows(irow).Cells(icol).Value = YuanSuan(Me.dgv.Rows(irow).Cells(icol).Value.ToString.Remove(0, 1))
                    Else
                        Me.dgvData.Rows(irow).Cells(icol).Value = Me.dgv.Rows(irow).Cells(icol).Value
                    End If
                    Me.dgvData.Rows(irow).Cells(icol).Style = Me.dgv.Rows(irow).Cells(icol).Style
                End If
            Next
        Next

        mdb.oleConnect.Close()
        '关闭数据库连接

        Me.导出文件.Enabled = True

        Me.tsbtnSave.Enabled = False
        Me.tsbtnSaveAs.Enabled = False

        Me.SetColumnWidth(Me.dgvData)
    End Sub

    Private Sub dgv_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp
        If e.KeyData = Keys.Delete Then
            Dim i As Integer
            For i = 0 To Me.dgv.SelectedCells.Count - 1
                Me.dgv.SelectedCells.Item(i).Value = ""
            Next
        End If
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Dim i As Integer
        For i = 0 To Me.dgv.SelectedCells.Count - 1
            Me.dgv.SelectedCells.Item(i).Style.Alignment = DataGridViewContentAlignment.MiddleLeft
        Next
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim i As Integer
        For i = 0 To Me.dgv.SelectedCells.Count - 1
            Me.dgv.SelectedCells.Item(i).Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        Next
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Dim i As Integer
        For i = 0 To Me.dgv.SelectedCells.Count - 1
            Me.dgv.SelectedCells.Item(i).Style.Alignment = DataGridViewContentAlignment.MiddleRight
        Next
    End Sub

    Private Sub 合并单元格_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 合并单元格.Click
        Dim iCol(2) As Integer
        Dim iRow(2) As Integer
        iCol(0) = Me.dgv.SelectedCells(0).ColumnIndex
        iRow(0) = Me.dgv.SelectedCells(0).RowIndex

        iCol(1) = Me.dgv.SelectedCells(Me.dgv.SelectedCells.Count - 1).ColumnIndex
        iRow(1) = Me.dgv.SelectedCells(Me.dgv.SelectedCells.Count - 1).RowIndex

        Dim iBRow, iBCol As Integer
        Dim iEcol, iErow As Integer

        iBRow = Math.Min(iRow(0), iRow(1))
        iErow = Math.Max(iRow(0), iRow(1))

        iBCol = Math.Min(iCol(0), iCol(1))
        iEcol = Math.Max(iCol(0), iCol(1))

        Me.TabControl1.TabPages.Remove(Me.tpDef)
        Me.TabControl1.TabPages.Add(Me.tpData)
        Me.tpData_Enter(sender, e)
    End Sub

    Private Sub tsbtnSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbtnSaveAs.Click
        Me.txtFileName.Clear()
        Me.SaveDGV()
    End Sub

    Private Sub txtFileName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFileName.TextChanged
        If Me.txtFileName.Text = "" Then
            Me.Text = "自由王报表——未命名"
        Else
            Me.Text = "自由王报表——" & Me.txtFileName.Text
        End If
    End Sub

    Private Sub 导出文件_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出文件.Click

        If IO.File.Exists(Me.txtFileName.Text.Replace(".rpt", ".xls")) Then
            Dim FKp As New FKPrn.ImportOut
            FKp.Importout(Me.dgvData, Me.txtFileName.Text.Replace(".rpt", ".xls"), False)
        Else
            Dim FKp As New FKPrn.ImportOut
            FKp.Importout(Me.dgvData, "", False)
        End If

    End Sub

    Private Sub tpDef_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tpDef.Enter
        Me.tsbtnSave.Enabled = True
        Me.tsbtnSaveAs.Enabled = True

        Me.导出文件.Enabled = False
    End Sub


    Private Sub dgvdata_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvData.CellPainting
        For xh As Integer = 0 To dgvData.Rows.Count - 1
            If e.ColumnIndex = 0 AndAlso e.RowIndex = xh Then
                e.Graphics.DrawString(System.Convert.ToString(xh + 1), Me.dgv.Font, Brushes.Red, 20, e.CellBounds.Top + 6)
            End If
        Next
        e.Graphics.DrawString("序号", Me.dgv.Font, Brushes.Black, 8, 5)
    End Sub

    Dim eCell(0) As String

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged

        If Array.IndexOf(eCell, e.RowIndex.ToString & "," & e.ColumnIndex.ToString) >= 0 Then
            '可以确定是循环引用，给出提示
            Me.dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Selected = True
                MsgBox("您设计的公式中存在循环引用，相关数值会出现错误，请仔细检查！", MsgBoxStyle.Information, "提示")

            Array.Resize(eCell, 1)
            Exit Sub
        End If

        Array.Resize(eCell, eCell.Length + 1)
        eCell(eCell.Length - 1) = e.RowIndex.ToString & "," & e.ColumnIndex.ToString

        Dim iCol, iRow As Integer
        For iRow = 0 To Me.dgv.Rows.Count - 1
            For iCol = 0 To Me.dgv.ColumnCount - 1
                If (Not IsNothing(Me.dgv.Rows(iRow).Cells(iCol).Value)) AndAlso Me.dgv.Rows(iRow).Cells(iCol).Value.ToString.StartsWith("=") Then
                    If Me.dgv.Rows(iRow).Cells(iCol).Value.ToString.ToUpper.Contains(dgv.Columns(e.ColumnIndex).Name & (e.RowIndex + 1).ToString) Then
                        Me.dgvData.Rows(iRow).Cells(iCol).Value = Me.YuanSuan(Me.dgv.Rows(iRow).Cells(iCol).Value.ToString.Remove(0, 1))
                    End If
                Else

                End If
            Next
        Next
        Array.Resize(eCell, 1)

    End Sub

    Private Sub SetColumnWidth(ByRef dg As DataGridView)
        Dim i As Integer
        For i = 0 To dg.Columns.Count - 1
            dg.Columns(i).Width = (dg.Width * 0.95 - 30) / dg.Columns.Count
        Next
    End Sub

    Private Sub dgv_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SizeChanged
        Me.SetColumnWidth(Me.dgv)
    End Sub

    Private Sub 数据视图ToolStripMenuItem_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles 数据视图ToolStripMenuItem.CheckStateChanged
        If Me.数据视图ToolStripMenuItem.Checked Then
            Me.设计视图ToolStripMenuItem.Checked = False
            Me.视图模式.Text = "数据视图"

            '设置显示按钮
            Me.tsbtnBianZhi.Visible = True

        End If
    End Sub

    Private Sub 设计视图ToolStripMenuItem_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles 设计视图ToolStripMenuItem.CheckStateChanged
        If Me.设计视图ToolStripMenuItem.Checked Then
            Me.数据视图ToolStripMenuItem.Checked = False
            Me.视图模式.Text = "设计视图"

        End If
    End Sub

    Private Sub 视图模式_ButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 视图模式.ButtonClick
        If Me.设计视图ToolStripMenuItem.Checked Then
            Me.数据视图ToolStripMenuItem.Checked = True
        Else
            Me.设计视图ToolStripMenuItem.Checked = True
        End If
    End Sub
End Class