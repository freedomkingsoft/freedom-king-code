﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FreedomKingMain
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    Friend WithEvents ToolStripContainer As System.Windows.Forms.ToolStripContainer
    Friend WithEvents SplitContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FreedomKingMain))
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.tslZHT = New System.Windows.Forms.ToolStripStatusLabel
        Me.tslYH = New System.Windows.Forms.ToolStripStatusLabel
        Me.tslZHQ = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripContainer = New System.Windows.Forms.ToolStripContainer
        Me.SplitContainer = New System.Windows.Forms.SplitContainer
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Fkgns1 = New FKGNS.myGNS
        Me.FKBBS = New FKBBSGL.myBBS
        Me.tlpDi = New System.Windows.Forms.TableLayoutPanel
        Me.tlpMainControl = New System.Windows.Forms.TableLayoutPanel
        Me.msZDY = New System.Windows.Forms.MenuStrip
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.系统设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.重新登录 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.凭证类型设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.常用摘要定义 = New System.Windows.Forms.ToolStripMenuItem
        Me.打造主界面 = New System.Windows.Forms.ToolStripMenuItem
        Me.显示功能树 = New System.Windows.Forms.ToolStripMenuItem
        Me.系统基础配置 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.退出系统 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能树管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.新建功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.修改功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.导入功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.导出功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.功能设计帮助 = New System.Windows.Forms.ToolStripMenuItem
        Me.证帐输出 = New System.Windows.Forms.ToolStripMenuItem
        Me.总帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.总帐余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.明细帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.日记帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.按编号查找ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.库存帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存明细帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存日记帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存报表 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来明细账 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来日记账 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来报表 = New System.Windows.Forms.ToolStripMenuItem
        Me.往来客户对账 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资明细帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资日记帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.外存型号余额 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外余额表 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外明细账 = New System.Windows.Forms.ToolStripMenuItem
        Me.委外日记账 = New System.Windows.Forms.ToolStripMenuItem
        Me.综合查询 = New System.Windows.Forms.ToolStripMenuItem
        Me.期末处理 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存结账 = New System.Windows.Forms.ToolStripMenuItem
        Me.工资结账 = New System.Windows.Forms.ToolStripMenuItem
        Me.期末审核 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator
        Me.凭证处理 = New System.Windows.Forms.ToolStripMenuItem
        Me.插入凭证 = New System.Windows.Forms.ToolStripMenuItem
        Me.凭证整理 = New System.Windows.Forms.ToolStripMenuItem
        Me.列表式凭证查询 = New System.Windows.Forms.ToolStripMenuItem
        Me.单证审核 = New System.Windows.Forms.ToolStripMenuItem
        Me.期间记账 = New System.Windows.Forms.ToolStripMenuItem
        Me.帐库对账 = New System.Windows.Forms.ToolStripMenuItem
        Me.生成转账凭证 = New System.Windows.Forms.ToolStripMenuItem
        Me.期末结帐 = New System.Windows.Forms.ToolStripMenuItem
        Me.年度处理 = New System.Windows.Forms.ToolStripMenuItem
        Me.帐套打印 = New System.Windows.Forms.ToolStripMenuItem
        Me.高级功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.科目代码设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.初始余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.科目余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.库存余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.初始工余额装入资 = New System.Windows.Forms.ToolStripMenuItem
        Me.初始委外余额装入 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能树 = New System.Windows.Forms.ToolStripMenuItem
        Me.报表树管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.企业资料设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.仓库定义 = New System.Windows.Forms.ToolStripMenuItem
        Me.用户管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.特殊用户设置 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator
        Me.导入 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator
        Me.备份数据 = New System.Windows.Forms.ToolStripMenuItem
        Me.恢复数据 = New System.Windows.Forms.ToolStripMenuItem
        Me.定制开发 = New System.Windows.Forms.ToolStripMenuItem
        Me.四工序机工工资 = New System.Windows.Forms.ToolStripMenuItem
        Me.三工序机工工资 = New System.Windows.Forms.ToolStripMenuItem
        Me.条形码保存 = New System.Windows.Forms.ToolStripMenuItem
        Me.帮助 = New System.Windows.Forms.ToolStripMenuItem
        Me.简介 = New System.Windows.Forms.ToolStripMenuItem
        Me.使用流程 = New System.Windows.Forms.ToolStripMenuItem
        Me.功能设计 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.注册 = New System.Windows.Forms.ToolStripMenuItem
        Me.关于 = New System.Windows.Forms.ToolStripMenuItem
        Me.管理员工具 = New System.Windows.Forms.ToolStripMenuItem
        Me.其他功能 = New System.Windows.Forms.ToolStripMenuItem
        Me.系统管理 = New System.Windows.Forms.ToolStripMenuItem
        Me.SQL语句工具 = New System.Windows.Forms.ToolStripMenuItem
        Me.myTimer = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip.SuspendLayout()
        Me.ToolStripContainer.BottomToolStripPanel.SuspendLayout()
        Me.ToolStripContainer.ContentPanel.SuspendLayout()
        Me.ToolStripContainer.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer.SuspendLayout()
        Me.SplitContainer.Panel1.SuspendLayout()
        Me.SplitContainer.Panel2.SuspendLayout()
        Me.SplitContainer.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.tlpDi.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip
        '
        Me.StatusStrip.BackColor = System.Drawing.Color.LightSteelBlue
        Me.StatusStrip.Dock = System.Windows.Forms.DockStyle.None
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tslZHT, Me.tslYH, Me.tslZHQ, Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 0)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(849, 22)
        Me.StatusStrip.TabIndex = 6
        Me.StatusStrip.Text = "StatusStrip"
        '
        'tslZHT
        '
        Me.tslZHT.AutoSize = False
        Me.tslZHT.Margin = New System.Windows.Forms.Padding(5, 3, 20, 2)
        Me.tslZHT.Name = "tslZHT"
        Me.tslZHT.Size = New System.Drawing.Size(29, 17)
        Me.tslZHT.Text = "帐套"
        '
        'tslYH
        '
        Me.tslYH.AutoSize = False
        Me.tslYH.Margin = New System.Windows.Forms.Padding(0, 3, 20, 2)
        Me.tslYH.Name = "tslYH"
        Me.tslYH.Size = New System.Drawing.Size(29, 17)
        Me.tslYH.Text = "用户"
        '
        'tslZHQ
        '
        Me.tslZHQ.AutoSize = False
        Me.tslZHQ.Margin = New System.Windows.Forms.Padding(0, 3, 20, 2)
        Me.tslZHQ.Name = "tslZHQ"
        Me.tslZHQ.Size = New System.Drawing.Size(29, 17)
        Me.tslZHQ.Text = "帐期"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.AutoSize = False
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(35, 17)
        Me.ToolStripStatusLabel.Text = "就绪"
        '
        'ToolStripContainer
        '
        '
        'ToolStripContainer.BottomToolStripPanel
        '
        Me.ToolStripContainer.BottomToolStripPanel.Controls.Add(Me.StatusStrip)
        '
        'ToolStripContainer.ContentPanel
        '
        Me.ToolStripContainer.ContentPanel.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripContainer.ContentPanel.Controls.Add(Me.SplitContainer)
        Me.ToolStripContainer.ContentPanel.Size = New System.Drawing.Size(849, 499)
        Me.ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStripContainer.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripContainer.Margin = New System.Windows.Forms.Padding(0)
        Me.ToolStripContainer.Name = "ToolStripContainer"
        Me.ToolStripContainer.Size = New System.Drawing.Size(849, 570)
        Me.ToolStripContainer.TabIndex = 7
        Me.ToolStripContainer.Text = "ToolStripContainer1"
        '
        'ToolStripContainer.TopToolStripPanel
        '
        Me.ToolStripContainer.TopToolStripPanel.Controls.Add(Me.msZDY)
        Me.ToolStripContainer.TopToolStripPanel.Controls.Add(Me.MenuStrip1)
        '
        'SplitContainer
        '
        Me.SplitContainer.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer.Name = "SplitContainer"
        '
        'SplitContainer.Panel1
        '
        Me.SplitContainer.Panel1.Controls.Add(Me.SplitContainer1)
        Me.SplitContainer.Panel1MinSize = 0
        '
        'SplitContainer.Panel2
        '
        Me.SplitContainer.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SplitContainer.Panel2.Controls.Add(Me.tlpDi)
        Me.SplitContainer.Size = New System.Drawing.Size(849, 499)
        Me.SplitContainer.SplitterDistance = 211
        Me.SplitContainer.TabIndex = 0
        Me.SplitContainer.TabStop = False
        Me.SplitContainer.Text = "SplitContainer1"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Fkgns1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.FKBBS)
        Me.SplitContainer1.Size = New System.Drawing.Size(211, 499)
        Me.SplitContainer1.SplitterDistance = 283
        Me.SplitContainer1.SplitterWidth = 1
        Me.SplitContainer1.TabIndex = 0
        '
        'Fkgns1
        '
        Me.Fkgns1.BackGroundImage = Nothing
        Me.Fkgns1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Fkgns1.Location = New System.Drawing.Point(0, 0)
        Me.Fkgns1.Margin = New System.Windows.Forms.Padding(0)
        Me.Fkgns1.Name = "Fkgns1"
        Me.Fkgns1.ShowNodeToolTips = True
        Me.Fkgns1.Size = New System.Drawing.Size(211, 283)
        Me.Fkgns1.TabIndex = 0
        '
        'FKBBS
        '
        Me.FKBBS.BackGroundImage = Nothing
        Me.FKBBS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FKBBS.Location = New System.Drawing.Point(0, 0)
        Me.FKBBS.Margin = New System.Windows.Forms.Padding(0)
        Me.FKBBS.Name = "FKBBS"
        Me.FKBBS.ShowNodeToolTips = True
        Me.FKBBS.Size = New System.Drawing.Size(211, 215)
        Me.FKBBS.TabIndex = 1
        '
        'tlpDi
        '
        Me.tlpDi.BackColor = System.Drawing.Color.Transparent
        Me.tlpDi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tlpDi.ColumnCount = 3
        Me.tlpDi.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.tlpDi.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.0!))
        Me.tlpDi.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.tlpDi.Controls.Add(Me.tlpMainControl, 1, 1)
        Me.tlpDi.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpDi.Location = New System.Drawing.Point(0, 0)
        Me.tlpDi.Margin = New System.Windows.Forms.Padding(0)
        Me.tlpDi.Name = "tlpDi"
        Me.tlpDi.RowCount = 3
        Me.tlpDi.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.tlpDi.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.0!))
        Me.tlpDi.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.tlpDi.Size = New System.Drawing.Size(634, 499)
        Me.tlpDi.TabIndex = 1
        '
        'tlpMainControl
        '
        Me.tlpMainControl.BackColor = System.Drawing.Color.Transparent
        Me.tlpMainControl.ColumnCount = 30
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.tlpMainControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpMainControl.Location = New System.Drawing.Point(31, 24)
        Me.tlpMainControl.Margin = New System.Windows.Forms.Padding(0)
        Me.tlpMainControl.Name = "tlpMainControl"
        Me.tlpMainControl.RowCount = 30
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpMainControl.Size = New System.Drawing.Size(570, 449)
        Me.tlpMainControl.TabIndex = 0
        '
        'msZDY
        '
        Me.msZDY.Dock = System.Windows.Forms.DockStyle.None
        Me.msZDY.Location = New System.Drawing.Point(0, 0)
        Me.msZDY.Name = "msZDY"
        Me.msZDY.Size = New System.Drawing.Size(849, 24)
        Me.msZDY.TabIndex = 1
        Me.msZDY.Text = "MenuStrip2"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.MenuStrip1.Enabled = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.系统设置, Me.功能管理, Me.证帐输出, Me.期末处理, Me.年度处理, Me.高级功能, Me.定制开发, Me.帮助, Me.管理员工具})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 24)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(849, 25)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        Me.MenuStrip1.Visible = False
        '
        '系统设置
        '
        Me.系统设置.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.重新登录, Me.ToolStripSeparator1, Me.凭证类型设置, Me.常用摘要定义, Me.打造主界面, Me.显示功能树, Me.系统基础配置, Me.ToolStripSeparator4, Me.退出系统})
        Me.系统设置.Name = "系统设置"
        Me.系统设置.ShortcutKeyDisplayString = ""
        Me.系统设置.Size = New System.Drawing.Size(68, 21)
        Me.系统设置.Text = "系统设置"
        '
        '重新登录
        '
        Me.重新登录.Name = "重新登录"
        Me.重新登录.Size = New System.Drawing.Size(169, 22)
        Me.重新登录.Text = "重新登录"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(166, 6)
        '
        '凭证类型设置
        '
        Me.凭证类型设置.Name = "凭证类型设置"
        Me.凭证类型设置.Size = New System.Drawing.Size(169, 22)
        Me.凭证类型设置.Text = "凭证类型设置"
        Me.凭证类型设置.Visible = False
        '
        '常用摘要定义
        '
        Me.常用摘要定义.Name = "常用摘要定义"
        Me.常用摘要定义.Size = New System.Drawing.Size(169, 22)
        Me.常用摘要定义.Text = "常用摘要定义"
        '
        '打造主界面
        '
        Me.打造主界面.Name = "打造主界面"
        Me.打造主界面.Size = New System.Drawing.Size(169, 22)
        Me.打造主界面.Text = "打造主界面"
        '
        '显示功能树
        '
        Me.显示功能树.CheckOnClick = True
        Me.显示功能树.Name = "显示功能树"
        Me.显示功能树.Size = New System.Drawing.Size(169, 22)
        Me.显示功能树.Text = "显示功能树"
        '
        '系统基础配置
        '
        Me.系统基础配置.Name = "系统基础配置"
        Me.系统基础配置.Size = New System.Drawing.Size(169, 22)
        Me.系统基础配置.Text = "系统基础配置"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(166, 6)
        '
        '退出系统
        '
        Me.退出系统.Name = "退出系统"
        Me.退出系统.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.退出系统.Size = New System.Drawing.Size(169, 22)
        Me.退出系统.Text = "退出系统"
        '
        '功能管理
        '
        Me.功能管理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.功能树管理, Me.ToolStripSeparator8, Me.新建功能, Me.修改功能, Me.ToolStripSeparator5, Me.导入功能, Me.导出功能, Me.ToolStripSeparator6, Me.功能设计帮助})
        Me.功能管理.Name = "功能管理"
        Me.功能管理.Size = New System.Drawing.Size(68, 21)
        Me.功能管理.Text = "功能管理"
        '
        '功能树管理
        '
        Me.功能树管理.Name = "功能树管理"
        Me.功能树管理.Size = New System.Drawing.Size(148, 22)
        Me.功能树管理.Text = "功能树管理"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(145, 6)
        '
        '新建功能
        '
        Me.新建功能.Name = "新建功能"
        Me.新建功能.Size = New System.Drawing.Size(148, 22)
        Me.新建功能.Text = "新建功能"
        '
        '修改功能
        '
        Me.修改功能.Name = "修改功能"
        Me.修改功能.Size = New System.Drawing.Size(148, 22)
        Me.修改功能.Text = "修改功能"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(145, 6)
        '
        '导入功能
        '
        Me.导入功能.Name = "导入功能"
        Me.导入功能.Size = New System.Drawing.Size(148, 22)
        Me.导入功能.Text = "导入功能"
        '
        '导出功能
        '
        Me.导出功能.Name = "导出功能"
        Me.导出功能.Size = New System.Drawing.Size(148, 22)
        Me.导出功能.Text = "导出功能"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(145, 6)
        '
        '功能设计帮助
        '
        Me.功能设计帮助.Name = "功能设计帮助"
        Me.功能设计帮助.Size = New System.Drawing.Size(148, 22)
        Me.功能设计帮助.Text = "功能设计帮助"
        '
        '证帐输出
        '
        Me.证帐输出.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.总帐, Me.库存帐, Me.往来帐, Me.工资帐, Me.委外帐, Me.综合查询})
        Me.证帐输出.Name = "证帐输出"
        Me.证帐输出.Size = New System.Drawing.Size(68, 21)
        Me.证帐输出.Text = "报表输出"
        '
        '总帐
        '
        Me.总帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.总帐余额表, Me.明细帐, Me.日记帐, Me.按编号查找ToolStripMenuItem})
        Me.总帐.Name = "总帐"
        Me.总帐.Size = New System.Drawing.Size(152, 22)
        Me.总帐.Text = "总帐"
        '
        '总帐余额表
        '
        Me.总帐余额表.Name = "总帐余额表"
        Me.总帐余额表.Size = New System.Drawing.Size(152, 22)
        Me.总帐余额表.Text = "总帐余额表"
        '
        '明细帐
        '
        Me.明细帐.Name = "明细帐"
        Me.明细帐.Size = New System.Drawing.Size(152, 22)
        Me.明细帐.Text = "明细帐"
        '
        '日记帐
        '
        Me.日记帐.Name = "日记帐"
        Me.日记帐.Size = New System.Drawing.Size(152, 22)
        Me.日记帐.Text = "日记帐"
        '
        '按编号查找ToolStripMenuItem
        '
        Me.按编号查找ToolStripMenuItem.Name = "按编号查找ToolStripMenuItem"
        Me.按编号查找ToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.按编号查找ToolStripMenuItem.Text = "按编号查找"
        '
        '库存帐
        '
        Me.库存帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.库存余额表, Me.库存明细帐, Me.库存日记帐, Me.库存报表})
        Me.库存帐.Name = "库存帐"
        Me.库存帐.Size = New System.Drawing.Size(152, 22)
        Me.库存帐.Text = "库存帐"
        '
        '库存余额表
        '
        Me.库存余额表.Name = "库存余额表"
        Me.库存余额表.Size = New System.Drawing.Size(136, 22)
        Me.库存余额表.Text = "库存余额表"
        '
        '库存明细帐
        '
        Me.库存明细帐.Name = "库存明细帐"
        Me.库存明细帐.Size = New System.Drawing.Size(136, 22)
        Me.库存明细帐.Text = "库存明细帐"
        '
        '库存日记帐
        '
        Me.库存日记帐.Name = "库存日记帐"
        Me.库存日记帐.Size = New System.Drawing.Size(136, 22)
        Me.库存日记帐.Text = "库存日记帐"
        '
        '库存报表
        '
        Me.库存报表.Name = "库存报表"
        Me.库存报表.Size = New System.Drawing.Size(136, 22)
        Me.库存报表.Text = "库存报表"
        '
        '往来帐
        '
        Me.往来帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.往来余额表, Me.往来明细账, Me.往来日记账, Me.往来报表, Me.往来客户对账})
        Me.往来帐.Name = "往来帐"
        Me.往来帐.Size = New System.Drawing.Size(152, 22)
        Me.往来帐.Text = "往来帐"
        '
        '往来余额表
        '
        Me.往来余额表.Name = "往来余额表"
        Me.往来余额表.Size = New System.Drawing.Size(148, 22)
        Me.往来余额表.Text = "往来余额表"
        '
        '往来明细账
        '
        Me.往来明细账.Name = "往来明细账"
        Me.往来明细账.Size = New System.Drawing.Size(148, 22)
        Me.往来明细账.Text = "往来明细账"
        '
        '往来日记账
        '
        Me.往来日记账.Name = "往来日记账"
        Me.往来日记账.Size = New System.Drawing.Size(148, 22)
        Me.往来日记账.Text = "往来日记账"
        '
        '往来报表
        '
        Me.往来报表.Name = "往来报表"
        Me.往来报表.Size = New System.Drawing.Size(148, 22)
        Me.往来报表.Text = "往来报表"
        '
        '往来客户对账
        '
        Me.往来客户对账.Name = "往来客户对账"
        Me.往来客户对账.Size = New System.Drawing.Size(148, 22)
        Me.往来客户对账.Text = "往来客户对账"
        '
        '工资帐
        '
        Me.工资帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.工资余额表, Me.工资明细帐, Me.工资日记帐})
        Me.工资帐.Name = "工资帐"
        Me.工资帐.Size = New System.Drawing.Size(152, 22)
        Me.工资帐.Text = "工资帐"
        '
        '工资余额表
        '
        Me.工资余额表.Name = "工资余额表"
        Me.工资余额表.Size = New System.Drawing.Size(136, 22)
        Me.工资余额表.Text = "工资余额表"
        '
        '工资明细帐
        '
        Me.工资明细帐.Name = "工资明细帐"
        Me.工资明细帐.Size = New System.Drawing.Size(136, 22)
        Me.工资明细帐.Text = "工资明细帐"
        '
        '工资日记帐
        '
        Me.工资日记帐.Name = "工资日记帐"
        Me.工资日记帐.Size = New System.Drawing.Size(136, 22)
        Me.工资日记帐.Text = "工资日记帐"
        '
        '委外帐
        '
        Me.委外帐.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.外存型号余额, Me.委外余额表, Me.委外明细账, Me.委外日记账})
        Me.委外帐.Name = "委外帐"
        Me.委外帐.Size = New System.Drawing.Size(152, 22)
        Me.委外帐.Text = "委外帐"
        '
        '外存型号余额
        '
        Me.外存型号余额.Name = "外存型号余额"
        Me.外存型号余额.Size = New System.Drawing.Size(148, 22)
        Me.外存型号余额.Text = "外存型号余额"
        '
        '委外余额表
        '
        Me.委外余额表.Name = "委外余额表"
        Me.委外余额表.Size = New System.Drawing.Size(148, 22)
        Me.委外余额表.Text = "委外余额表"
        '
        '委外明细账
        '
        Me.委外明细账.Name = "委外明细账"
        Me.委外明细账.Size = New System.Drawing.Size(148, 22)
        Me.委外明细账.Text = "委外明细帐"
        '
        '委外日记账
        '
        Me.委外日记账.Name = "委外日记账"
        Me.委外日记账.Size = New System.Drawing.Size(148, 22)
        Me.委外日记账.Text = "委外日记帐"
        '
        '综合查询
        '
        Me.综合查询.Name = "综合查询"
        Me.综合查询.Size = New System.Drawing.Size(152, 22)
        Me.综合查询.Text = "综合查询"
        '
        '期末处理
        '
        Me.期末处理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.库存结账, Me.工资结账, Me.期末审核, Me.ToolStripSeparator9, Me.凭证处理, Me.单证审核, Me.期间记账, Me.帐库对账, Me.生成转账凭证, Me.期末结帐})
        Me.期末处理.Name = "期末处理"
        Me.期末处理.Size = New System.Drawing.Size(68, 21)
        Me.期末处理.Text = "期末处理"
        '
        '库存结账
        '
        Me.库存结账.Name = "库存结账"
        Me.库存结账.Size = New System.Drawing.Size(148, 22)
        Me.库存结账.Text = "库存结账"
        '
        '工资结账
        '
        Me.工资结账.Name = "工资结账"
        Me.工资结账.Size = New System.Drawing.Size(148, 22)
        Me.工资结账.Text = "工资结账"
        '
        '期末审核
        '
        Me.期末审核.Name = "期末审核"
        Me.期末审核.Size = New System.Drawing.Size(148, 22)
        Me.期末审核.Text = "期末审核"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(145, 6)
        '
        '凭证处理
        '
        Me.凭证处理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.插入凭证, Me.凭证整理, Me.列表式凭证查询})
        Me.凭证处理.Name = "凭证处理"
        Me.凭证处理.Size = New System.Drawing.Size(148, 22)
        Me.凭证处理.Text = "凭证处理"
        '
        '插入凭证
        '
        Me.插入凭证.Name = "插入凭证"
        Me.插入凭证.Size = New System.Drawing.Size(160, 22)
        Me.插入凭证.Text = "插入凭证"
        '
        '凭证整理
        '
        Me.凭证整理.Name = "凭证整理"
        Me.凭证整理.Size = New System.Drawing.Size(160, 22)
        Me.凭证整理.Text = "凭证整理"
        '
        '列表式凭证查询
        '
        Me.列表式凭证查询.Name = "列表式凭证查询"
        Me.列表式凭证查询.Size = New System.Drawing.Size(160, 22)
        Me.列表式凭证查询.Text = "列表式凭证查询"
        '
        '单证审核
        '
        Me.单证审核.Name = "单证审核"
        Me.单证审核.Size = New System.Drawing.Size(148, 22)
        Me.单证审核.Text = "凭证审核"
        '
        '期间记账
        '
        Me.期间记账.Name = "期间记账"
        Me.期间记账.Size = New System.Drawing.Size(148, 22)
        Me.期间记账.Text = "期间记账"
        '
        '帐库对账
        '
        Me.帐库对账.Name = "帐库对账"
        Me.帐库对账.Size = New System.Drawing.Size(148, 22)
        Me.帐库对账.Text = "帐库对账"
        '
        '生成转账凭证
        '
        Me.生成转账凭证.Name = "生成转账凭证"
        Me.生成转账凭证.Size = New System.Drawing.Size(148, 22)
        Me.生成转账凭证.Text = "生成转账凭证"
        '
        '期末结帐
        '
        Me.期末结帐.Name = "期末结帐"
        Me.期末结帐.Size = New System.Drawing.Size(148, 22)
        Me.期末结帐.Text = "期末结帐"
        '
        '年度处理
        '
        Me.年度处理.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.帐套打印})
        Me.年度处理.Name = "年度处理"
        Me.年度处理.Size = New System.Drawing.Size(68, 21)
        Me.年度处理.Text = "年度处理"
        '
        '帐套打印
        '
        Me.帐套打印.Name = "帐套打印"
        Me.帐套打印.Size = New System.Drawing.Size(124, 22)
        Me.帐套打印.Text = "帐套打印"
        '
        '高级功能
        '
        Me.高级功能.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.科目代码设置, Me.初始余额装入, Me.功能树, Me.报表树管理, Me.企业资料设置, Me.ToolStripSeparator2, Me.仓库定义, Me.用户管理, Me.特殊用户设置, Me.ToolStripSeparator10, Me.导入, Me.ToolStripSeparator11, Me.备份数据, Me.恢复数据})
        Me.高级功能.Name = "高级功能"
        Me.高级功能.Size = New System.Drawing.Size(68, 21)
        Me.高级功能.Text = "高级功能"
        '
        '科目代码设置
        '
        Me.科目代码设置.Name = "科目代码设置"
        Me.科目代码设置.Size = New System.Drawing.Size(148, 22)
        Me.科目代码设置.Text = "科目代码设置"
        '
        '初始余额装入
        '
        Me.初始余额装入.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.科目余额装入, Me.库存余额装入, Me.初始工余额装入资, Me.初始委外余额装入})
        Me.初始余额装入.Name = "初始余额装入"
        Me.初始余额装入.Size = New System.Drawing.Size(148, 22)
        Me.初始余额装入.Text = "初始余额装入"
        '
        '科目余额装入
        '
        Me.科目余额装入.Name = "科目余额装入"
        Me.科目余额装入.Size = New System.Drawing.Size(172, 22)
        Me.科目余额装入.Text = "初始科目余额装入"
        '
        '库存余额装入
        '
        Me.库存余额装入.Name = "库存余额装入"
        Me.库存余额装入.Size = New System.Drawing.Size(172, 22)
        Me.库存余额装入.Text = "初始库存余额装入"
        '
        '初始工余额装入资
        '
        Me.初始工余额装入资.Name = "初始工余额装入资"
        Me.初始工余额装入资.Size = New System.Drawing.Size(172, 22)
        Me.初始工余额装入资.Text = "初始工资余额装入"
        '
        '初始委外余额装入
        '
        Me.初始委外余额装入.Name = "初始委外余额装入"
        Me.初始委外余额装入.Size = New System.Drawing.Size(172, 22)
        Me.初始委外余额装入.Text = "初始委外余额装入"
        '
        '功能树
        '
        Me.功能树.Name = "功能树"
        Me.功能树.Size = New System.Drawing.Size(148, 22)
        Me.功能树.Text = "功能树管理"
        '
        '报表树管理
        '
        Me.报表树管理.Name = "报表树管理"
        Me.报表树管理.Size = New System.Drawing.Size(148, 22)
        Me.报表树管理.Text = "报表树管理"
        '
        '企业资料设置
        '
        Me.企业资料设置.Name = "企业资料设置"
        Me.企业资料设置.Size = New System.Drawing.Size(148, 22)
        Me.企业资料设置.Text = "企业资料设置"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(145, 6)
        '
        '仓库定义
        '
        Me.仓库定义.Name = "仓库定义"
        Me.仓库定义.Size = New System.Drawing.Size(148, 22)
        Me.仓库定义.Text = "仓库定义"
        '
        '用户管理
        '
        Me.用户管理.Name = "用户管理"
        Me.用户管理.Size = New System.Drawing.Size(148, 22)
        Me.用户管理.Text = "用户管理"
        '
        '特殊用户设置
        '
        Me.特殊用户设置.Name = "特殊用户设置"
        Me.特殊用户设置.Size = New System.Drawing.Size(148, 22)
        Me.特殊用户设置.Text = "特殊用户设置"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(145, 6)
        '
        '导入
        '
        Me.导入.Name = "导入"
        Me.导入.Size = New System.Drawing.Size(148, 22)
        Me.导入.Text = "导入功能"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(145, 6)
        '
        '备份数据
        '
        Me.备份数据.Name = "备份数据"
        Me.备份数据.Size = New System.Drawing.Size(148, 22)
        Me.备份数据.Text = "备份数据"
        '
        '恢复数据
        '
        Me.恢复数据.Name = "恢复数据"
        Me.恢复数据.Size = New System.Drawing.Size(148, 22)
        Me.恢复数据.Text = "恢复数据"
        '
        '定制开发
        '
        Me.定制开发.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.四工序机工工资, Me.三工序机工工资, Me.条形码保存})
        Me.定制开发.Name = "定制开发"
        Me.定制开发.Size = New System.Drawing.Size(68, 21)
        Me.定制开发.Text = "定制开发"
        '
        '四工序机工工资
        '
        Me.四工序机工工资.Name = "四工序机工工资"
        Me.四工序机工工资.Size = New System.Drawing.Size(160, 22)
        Me.四工序机工工资.Text = "四工序机工工资"
        '
        '三工序机工工资
        '
        Me.三工序机工工资.Name = "三工序机工工资"
        Me.三工序机工工资.Size = New System.Drawing.Size(160, 22)
        Me.三工序机工工资.Text = "三工序机工工资"
        '
        '条形码保存
        '
        Me.条形码保存.Name = "条形码保存"
        Me.条形码保存.Size = New System.Drawing.Size(160, 22)
        Me.条形码保存.Text = "条形码保存"
        '
        '帮助
        '
        Me.帮助.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.简介, Me.使用流程, Me.功能设计, Me.ToolStripSeparator7, Me.注册, Me.关于})
        Me.帮助.Name = "帮助"
        Me.帮助.Size = New System.Drawing.Size(44, 21)
        Me.帮助.Text = "帮助"
        '
        '简介
        '
        Me.简介.Name = "简介"
        Me.简介.Size = New System.Drawing.Size(124, 22)
        Me.简介.Text = "简介"
        '
        '使用流程
        '
        Me.使用流程.Name = "使用流程"
        Me.使用流程.Size = New System.Drawing.Size(124, 22)
        Me.使用流程.Text = "使用流程"
        '
        '功能设计
        '
        Me.功能设计.Name = "功能设计"
        Me.功能设计.Size = New System.Drawing.Size(124, 22)
        Me.功能设计.Text = "功能设计"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(121, 6)
        '
        '注册
        '
        Me.注册.Name = "注册"
        Me.注册.Size = New System.Drawing.Size(124, 22)
        Me.注册.Text = "注册"
        '
        '关于
        '
        Me.关于.Name = "关于"
        Me.关于.Size = New System.Drawing.Size(124, 22)
        Me.关于.Text = "关于"
        '
        '管理员工具
        '
        Me.管理员工具.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.其他功能, Me.系统管理, Me.SQL语句工具})
        Me.管理员工具.Name = "管理员工具"
        Me.管理员工具.Size = New System.Drawing.Size(80, 21)
        Me.管理员工具.Text = "管理员工具"
        '
        '其他功能
        '
        Me.其他功能.Name = "其他功能"
        Me.其他功能.Size = New System.Drawing.Size(147, 22)
        Me.其他功能.Text = "其他功能"
        '
        '系统管理
        '
        Me.系统管理.Name = "系统管理"
        Me.系统管理.Size = New System.Drawing.Size(147, 22)
        Me.系统管理.Text = "系统管理"
        '
        'SQL语句工具
        '
        Me.SQL语句工具.Name = "SQL语句工具"
        Me.SQL语句工具.Size = New System.Drawing.Size(147, 22)
        Me.SQL语句工具.Text = "SQL语句工具"
        '
        'myTimer
        '
        Me.myTimer.Enabled = True
        Me.myTimer.Interval = 600000
        '
        'FreedomKingMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(849, 570)
        Me.Controls.Add(Me.ToolStripContainer)
        Me.DoubleBuffered = True
        Me.HelpButton = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FreedomKingMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "自由王进销存财务一体化管理系统"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ToolStripContainer.BottomToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer.BottomToolStripPanel.PerformLayout()
        Me.ToolStripContainer.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer.ResumeLayout(False)
        Me.ToolStripContainer.PerformLayout()
        Me.SplitContainer.Panel1.ResumeLayout(False)
        Me.SplitContainer.Panel2.ResumeLayout(False)
        Me.SplitContainer.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.tlpDi.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Fkgns1 As FKGNS.myGNS
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents 系统设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 证帐输出 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 期末处理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 年度处理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 重新登录 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 凭证类型设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 常用摘要定义 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 退出系统 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 新建功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 修改功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 导入功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 导出功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 功能设计帮助 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 总帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 总帐余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 明细帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 日记帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存明细帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存日记帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存报表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资明细帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资日记帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 综合查询 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 单证审核 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 期间记账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 期末结帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 帐套打印 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 帮助 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 简介 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 使用流程 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能设计 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 注册 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 关于 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能树管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tslZHT As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslYH As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslZHQ As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents 高级功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能树 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 导入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存结账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 帐库对账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 生成转账凭证 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 工资结账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 备份数据 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 恢复数据 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 用户管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 仓库定义 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 初始余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 科目余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 库存余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 企业资料设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 特殊用户设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 科目代码设置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 初始工余额装入资 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来明细账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来日记账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来报表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外帐 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外余额表 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外明细账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 委外日记账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 报表树管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FKBBS As FKBBSGL.myBBS
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents 外存型号余额 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 初始委外余额装入 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 定制开发 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 四工序机工工资 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msZDY As System.Windows.Forms.MenuStrip
    Friend WithEvents tlpMainControl As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents 打造主界面 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tlpDi As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents 管理员工具 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 其他功能 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 系统管理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SQL语句工具 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 往来客户对账 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 期末审核 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 显示功能树 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 条形码保存 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 凭证处理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 插入凭证 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 凭证整理 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 列表式凭证查询 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 三工序机工工资 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 系统基础配置 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents myTimer As System.Windows.Forms.Timer
    Friend WithEvents 按编号查找ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
