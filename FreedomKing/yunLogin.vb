Public Class yunLogin

    Private Declare Function GetSystemDefaultLCID Lib "kernel32" () As Long
    Private Declare Function SetLocaleInfo Lib "kernel32" Alias "SetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String) As Boolean
    Private Const LOCALE_SLONGDATE = &H20
    Private Const LOCALE_SSHORTDATE = &H1F
    Private Const LOCALE_STIME = &H1E

    Private Sub setCustomCulture()
        Try
            Dim lngLocale As Long
            lngLocale = GetSystemDefaultLCID()
            SetLocaleInfo(lngLocale, LOCALE_SLONGDATE, "yyyy'年'MM'月'dd'日'")
            SetLocaleInfo(lngLocale, LOCALE_SSHORTDATE, "yyyy'-'MM'-'dd")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub yunLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '检查并设置日期格式
        setCustomCulture()

        Me.Hide()
        Me.Icon = FKG.myselfG.FKIcon

        Try
            Me.SplitContainer1.IsSplitterFixed = True
            Me.SplitContainer1.SplitterDistance = Me.Width - 290
            Me.SplitContainer1.FixedPanel = FixedPanel.Panel2


            Me.SplitContainer1.Panel2.BackgroundImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\FKData\Pictrue\left.jpg")
            Me.tpLogin.BackgroundImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\FKData\Pictrue\left.jpg")
            Me.tpReg.BackgroundImage = System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\FKData\Pictrue\left.jpg")
            Me.tcLogin.ImageList.Images.Add(System.Drawing.Image.FromFile(My.Application.Info.DirectoryPath & "\FKData\Pictrue\left.jpg"))
        Catch ex As Exception

        End Try


        setIP()
        ChangeVM()

        Me.btnLogin.Visible = True
        Me.btnLoginCom.Visible = False

        Dim th As New Threading.Thread(AddressOf setWeb)
        th.Start()

        Me.Show()

        Me.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub setWeb()

        Dim sContect As String
        sContect = IO.File.ReadAllText(Application.StartupPath & "\config.txt")

        Dim sIP As String = Split(sContect, ",").GetValue(0)
        If sIP = "www.bdsp.top" Then
            sIP = "www.bdsp.top/start/start.htm"
        End If
        Me.wbMain.Navigate(sIP)
    End Sub

    '设置主页地址，获取默认连接字符串
    Private sToken As String = ""
    Private Sub setIP()


        Dim ws As New wsbdsp.Service

        '通过一个随机数生成sToken，进行webservice签名验证
        Dim sRND As String
        sRND = My.Computer.Info.OSPlatform & My.Computer.Info.OSVersion & My.Application.GetHashCode.ToString

        Dim sT As String = ""
        Dim i As Integer
        For i = 0 To sRND.Length - 1
            If Asc(sRND.Substring(i, 1)) > 122 Then

            Else
                sT = sT & (Asc(sRND.Substring(i, 1)) * i).ToString
            End If
        Next

        Dim sTR As String = "A"
        For i = 0 To sT.Length.ToString - 1
            If i Mod 7 = 0 Then
                sTR = sTR & sT.Substring(i, 1)
            End If
        Next

        sToken = sRND & "," & sTR

        Dim sConn As String
        Try
            sConn = ws.Hello(sToken, False)

        Catch ex As Exception
            MsgBox(ex.ToString)
            MsgBox("远程服务器连接错误，请检查网络是否可用。系统将自动退出")
            End
            sConn = ""
        End Try

        FKG.myselfG.setConStr(sConn)

        FKG.myselfG.setSetupStr(Application.StartupPath & "\FKData", "PicturePath")
        FKG.myselfG.setSetupStr(ws.Hello(sToken, True), "BackupPath")
        FKG.myselfG.setSetupStr(7, "BackupTianShu")

        Me.lblCurrentVersion.Text = Me.lblCurrentVersion.Text & My.Application.Info.Version.Major.ToString & "." & My.Application.Info.Version.Minor.ToString & "." & My.Application.Info.Version.Build.ToString & My.Application.Info.Version.Revision.ToString

        Me.lblNewVersion.Text = Me.lblNewVersion.Text & ws.HelloVersion(sToken)
        ' MsgBox(sConn)
    End Sub

    Private Sub reg_phone_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles reg_phone.Validated
        Dim ws As New wsbdsp.Service
        If ws.CheckPhone(sToken, Me.reg_phone.Text) Then
            Me.lbl_InfoPhone.Text = "您输入的手机号已被注册！"
        Else
            Me.lbl_InfoPhone.Text = ""
        End If
    End Sub

    Private Sub reg_pws2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles reg_pws2.Validated
        If Me.reg_pws.Text <> Me.reg_pws2.Text Then
            Me.lbl_InfoPWS.Text = "您两次输入的密码不一致！"
        Else
            Me.lbl_InfoPWS.Text = ""
        End If
    End Sub


    Private Sub ChangeVM()
        Me.lbl_vm.Text = randString(4, 2)
        Me.lbl_In_vm.Text = randString(4, 2)
    End Sub

    Private Function randString(Optional ByVal iLength As Integer = 8, Optional ByVal bNumber As Integer = 0) As String
        Dim sRand As String = ""

        Dim iRand As Integer

        Do
            Randomize()
            iRand = Int(Rnd() * 90 + 33)

            If bNumber = 0 Then
                If (iRand >= 65 And iRand <= 90) Or (iRand >= 97 And iRand <= 122) Then
                    sRand = sRand & CType(Chr(iRand), String)
                End If
            ElseIf bNumber = 1 Then
                If (iRand >= 48 And iRand <= 57) Or (iRand >= 65 And iRand <= 90) Or (iRand >= 97 And iRand <= 122) Then
                    sRand = sRand & CType(Chr(iRand), String)
                End If
            Else
                If (iRand >= 48 And iRand <= 57) Then
                    sRand = sRand & CType(Chr(iRand), String)
                End If
            End If

        Loop Until sRand.Length = iLength

        Return sRand
    End Function

    Private Sub lbl_vm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbl_vm.Click
        ChangeVM()
    End Sub


    Private Sub reg_vm_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles reg_vm.Validated
        If Me.reg_vm.Text <> Me.lbl_vm.Text Then
            Me.lbl_Info.Text = "您输入的验证码错误，点击可刷新验证码！"
        Else
            Me.lbl_Info.Text = ""
        End If
    End Sub

    Private Sub btnReg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReg.Click
        If Me.reg_phone.Text = "" OrElse Me.reg_pws.Text = "" OrElse Me.reg_pws2.Text = "" OrElse Me.reg_vm.Text = "" OrElse Me.reg_comname.Text = "" Then
            Me.lbl_result.Text = "有必填项为空通过，请检查输入是否为空。"
            Exit Sub

        End If
        If Me.lbl_Info.Text <> "" OrElse Me.lbl_InfoPhone.Text <> "" OrElse Me.lbl_InfoPWS.Text <> "" Then
            Me.lbl_result.Text = "验证没有通过，请检查输入是否有提示。"
        Else
            Dim ws As New wsbdsp.Service
            Dim ds As DataSet
            ds = ws.CreateNewUser(sToken, Me.reg_phone.Text, Me.reg_pws.Text, Me.reg_comname.Text, Me.reg_email.Text, "TMP_", 1)

            If ds.Tables(0).Rows.Count > 0 Then
                ' Me.lbl_result.Text = "注册成功，欢迎登录"
                Me.rtxtResult.Text = "注册成功，欢迎登录。您的公司号为：" & ds.Tables(0).Rows(0).Item("loginName").ToString & "，员工号为：" & ds.Tables(0).Rows(0).Item("subUserID").ToString & "，您可以使用您刚才注册的手机号 " & ds.Tables(0).Rows(0).Item("regPhone").ToString & " 或者员工号进行登录。"
            End If
        End If
    End Sub

    Dim dsComList As DataSet
    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        If Me.in_Phone.Text = "" OrElse Me.in_PWS.Text = "" OrElse Me.in_VM.Text = "" Then
            Me.lbl_in_Info.Text = "手机号，密码和验证码均不可用为空"
            Exit Sub
        End If
        If Me.in_VM.Text <> Me.lbl_In_vm.Text Then
            Me.lbl_in_Info.Text = "验证码错误，请重新输入"
            Exit Sub
        End If

        Dim ws As New wsbdsp.Service
        'dsComList = ws.Login(sToken, Me.in_Phone.Text, "", Me.in_PWS.Text, 1)
        Dim sReVersion = My.Application.Info.Version.Build.ToString & My.Application.Info.Version.Revision.ToString
        dsComList = ws.NewLogin(sToken, Me.in_Phone.Text, "", Me.in_PWS.Text, 1, sReVersion)

        Try
            If dsComList.Tables(0).Rows.Count > 0 Then
                Dim bChangepws As Int16
                bChangepws = dsComList.Tables(0).Rows(0).Item("changepws")
                Dim bVIP As Boolean = False
                bVIP = dsComList.Tables(0).Rows(0).Item("VIP")
                If bChangepws = 0 And bVIP = True Then
                    MsgBox("您现在使用的密码为默认密码，为保护数据安全，请登录手机端修改密码。请使用注册手机号进入手机端，然后点击右下角 我的，进行密码修改。修改密码后方可登录。" & Chr(13) & Chr(10) & "点击确定后系统将自动关闭", MsgBoxStyle.Information, "请修改默认密码")
                    ' MsgBox("请使用注册手机号进入手机端，然后点击右下角 我的，进行密码修改。")
                    End
                End If

                Me.lbl_in_Info.Text = "登录成功"

                '保存账号密码
                If Me.cbSave.Checked Then
                    savePWS()
                Else
                    clearPWS()
                End If

                '填充公司列表，等待用户选择，隐藏登录按钮，显示登录账户按钮
                If dsComList.Tables(0).Rows.Count = 1 Then
                    btnLoginCom_Click(sender, e)
                Else
                    Dim i As Integer
                    For i = 0 To dsComList.Tables(0).Rows.Count - 1
                        Me.cbComList.Items.Add(dsComList.Tables(0).Rows(i).Item("comName"))
                    Next

                    Me.cbComList.Visible = True

                    Me.btnLogin.Visible = False
                    Me.btnLoginCom.Visible = True
                End If

            Else
                Me.btnLogin.Visible = True
                Me.btnLoginCom.Visible = False
                Me.lbl_in_Info.Text = "登录失败，用户名或密码错误"
            End If
        Catch ex As Exception
            Me.btnLogin.Visible = True
            Me.btnLoginCom.Visible = False
            Me.lbl_in_Info.Text = "登录失败"

            Me.Show()
        End Try

        ChangeVM()
    End Sub

    Private Sub btnLoginCom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoginCom.Click
        '设置全局变量
        If dsComList.Tables(0).Rows.Count = 1 Then
            FKG.myselfG.YunUserInfo = dsComList.Tables(0).Rows(0)
        Else
            FKG.myselfG.YunUserInfo = dsComList.Tables(0).Rows(Me.cbComList.SelectedIndex)
        End If

        Dim sUserConn As String
        sUserConn = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & FKG.myselfG.YunUserInfo.Item("userLogin").ToString & ";Password=" & FKG.myselfG.YunUserInfo.Item("userPWS").ToString & ";Initial Catalog=" & FKG.myselfG.YunUserInfo.Item("userDatabase").ToString & ";Data Source=" & FKG.myselfG.YunUserInfo.Item("DataSource").ToString & ""


        FKG.myselfG.setConStr(sUserConn)

        Me.Hide()

        Dim dlgLogin As New Login
        dlgLogin.ShowDialog()
    End Sub

    Private Sub in_VM_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles in_VM.TextChanged
        Me.lbl_in_Info.Text = ""
    End Sub

    Private Sub lbl_In_vm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbl_In_vm.Click
        ChangeVM()
    End Sub

    Private Sub savePWS()
        FKG.myselfG.setSetupStr(Me.in_Phone.Text, "in_phone")
        FKG.myselfG.setSetupStr(Me.in_PWS.Text, "in_PWS")

        FKG.myselfG.setSetupStr(Me.in_COMID.Text, "in_COMID")
        FKG.myselfG.setSetupStr(Me.in_WORKID.Text, "in_WORKID")
        FKG.myselfG.setSetupStr(Me.in_COMPWS.Text, "in_COMPWS")

    End Sub

    Private Sub clearPWS()
        FKG.myselfG.setSetupStr("", "in_phone")
        FKG.myselfG.setSetupStr("", "in_PWS")

        FKG.myselfG.setSetupStr("", "in_COMID")
        FKG.myselfG.setSetupStr("", "in_WORKID")
        FKG.myselfG.setSetupStr("", "in_COMPWS")

    End Sub

    Private Sub cbSave_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSave.CheckedChanged
        If cbSave.Checked Then
            Me.in_Phone.Text = FKG.myselfG.getSetupStr("in_phone")
            Me.in_PWS.Text = FKG.myselfG.getSetupStr("in_PWS")
        Else
            Me.in_COMID.Text = FKG.myselfG.getSetupStr("in_COMID")
            Me.in_WORKID.Text = FKG.myselfG.getSetupStr("in_WORKID")
            Me.in_COMPWS.Text = FKG.myselfG.getSetupStr("in_COMPWS")
        End If
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim AutoUpdate As String
        AutoUpdate = My.Application.Info.DirectoryPath & "\AutoUpdate.exe"
        Shell(AutoUpdate)
        End
    End Sub


    Private Sub btnHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHome.Click
        Dim sContect As String
        sContect = IO.File.ReadAllText(Application.StartupPath & "\config.txt")

        Dim sIP As String = Split(sContect, ",").GetValue(0)
        Me.wbMain.Navigate(sIP)
    End Sub

    Private Sub btnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShow.Click
        If Me.scRight.Panel2.Visible = True Then
            Me.scRight.Panel2.Visible = False
            Me.btnShow.Text = "显   示   登   录    窗    口"
            Me.SplitContainer1.SplitterDistance = Me.Width - 25
        Else
            Me.scRight.Panel2.Visible = True
            Me.btnShow.Text = "隐   藏   登   录    窗    口"
            Me.SplitContainer1.SplitterDistance = Me.Width - 290
        End If
    End Sub
End Class