﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class yunLogin
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.wbMain = New System.Windows.Forms.WebBrowser
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.scRight = New System.Windows.Forms.SplitContainer
        Me.btnShow = New System.Windows.Forms.Button
        Me.btnHome = New System.Windows.Forms.Button
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.lblCurrentVersion = New System.Windows.Forms.Label
        Me.lblNewVersion = New System.Windows.Forms.Label
        Me.tcLogin = New System.Windows.Forms.TabControl
        Me.tpLogin = New System.Windows.Forms.TabPage
        Me.cbComList = New System.Windows.Forms.ComboBox
        Me.cbSave = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.in_WORKID = New System.Windows.Forms.TextBox
        Me.in_COMID = New System.Windows.Forms.TextBox
        Me.in_COMPWS = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.in_Phone = New System.Windows.Forms.TextBox
        Me.in_PWS = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.btnLoginCom = New System.Windows.Forms.Button
        Me.btnLogin = New System.Windows.Forms.Button
        Me.in_VM = New System.Windows.Forms.TextBox
        Me.lbl_In_vm = New System.Windows.Forms.Label
        Me.lbl_in_Info = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.tpReg = New System.Windows.Forms.TabPage
        Me.rtxtResult = New System.Windows.Forms.RichTextBox
        Me.lbl_vm = New System.Windows.Forms.Label
        Me.lbl_InfoPhone = New System.Windows.Forms.Label
        Me.lbl_InfoPWS = New System.Windows.Forms.Label
        Me.lbl_Info = New System.Windows.Forms.Label
        Me.btnReg = New System.Windows.Forms.Button
        Me.reg_vm = New System.Windows.Forms.TextBox
        Me.reg_email = New System.Windows.Forms.TextBox
        Me.reg_pws2 = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.reg_pws = New System.Windows.Forms.TextBox
        Me.lbl_result = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.reg_comname = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.reg_phone = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.scRight.Panel1.SuspendLayout()
        Me.scRight.Panel2.SuspendLayout()
        Me.scRight.SuspendLayout()
        Me.tcLogin.SuspendLayout()
        Me.tpLogin.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tpReg.SuspendLayout()
        Me.SuspendLayout()
        '
        'wbMain
        '
        Me.wbMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wbMain.Location = New System.Drawing.Point(0, 0)
        Me.wbMain.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbMain.Name = "wbMain"
        Me.wbMain.Size = New System.Drawing.Size(799, 653)
        Me.wbMain.TabIndex = 0
        Me.wbMain.Url = New System.Uri("", System.UriKind.Relative)
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.wbMain)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.scRight)
        Me.SplitContainer1.Size = New System.Drawing.Size(1079, 653)
        Me.SplitContainer1.SplitterDistance = 799
        Me.SplitContainer1.TabIndex = 1
        '
        'scRight
        '
        Me.scRight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.scRight.Location = New System.Drawing.Point(0, 0)
        Me.scRight.Name = "scRight"
        '
        'scRight.Panel1
        '
        Me.scRight.Panel1.Controls.Add(Me.btnShow)
        Me.scRight.Panel1.Controls.Add(Me.btnHome)
        '
        'scRight.Panel2
        '
        Me.scRight.Panel2.Controls.Add(Me.btnUpdate)
        Me.scRight.Panel2.Controls.Add(Me.lblCurrentVersion)
        Me.scRight.Panel2.Controls.Add(Me.lblNewVersion)
        Me.scRight.Panel2.Controls.Add(Me.tcLogin)
        Me.scRight.Size = New System.Drawing.Size(276, 653)
        Me.scRight.SplitterDistance = 29
        Me.scRight.TabIndex = 3
        '
        'btnShow
        '
        Me.btnShow.Location = New System.Drawing.Point(0, 217)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Padding = New System.Windows.Forms.Padding(5)
        Me.btnShow.Size = New System.Drawing.Size(30, 337)
        Me.btnShow.TabIndex = 1
        Me.btnShow.Text = "隐   藏   登   录    窗    口"
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'btnHome
        '
        Me.btnHome.Location = New System.Drawing.Point(0, 44)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Padding = New System.Windows.Forms.Padding(5)
        Me.btnHome.Size = New System.Drawing.Size(30, 146)
        Me.btnHome.TabIndex = 0
        Me.btnHome.Text = "官  网  首  页"
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdate.BackColor = System.Drawing.Color.Transparent
        Me.btnUpdate.Location = New System.Drawing.Point(148, 618)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 2
        Me.btnUpdate.Text = "自动升级"
        Me.btnUpdate.UseVisualStyleBackColor = False
        '
        'lblCurrentVersion
        '
        Me.lblCurrentVersion.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblCurrentVersion.AutoSize = True
        Me.lblCurrentVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrentVersion.Location = New System.Drawing.Point(8, 600)
        Me.lblCurrentVersion.Name = "lblCurrentVersion"
        Me.lblCurrentVersion.Size = New System.Drawing.Size(65, 12)
        Me.lblCurrentVersion.TabIndex = 1
        Me.lblCurrentVersion.Text = "当前版本："
        '
        'lblNewVersion
        '
        Me.lblNewVersion.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblNewVersion.AutoSize = True
        Me.lblNewVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblNewVersion.Location = New System.Drawing.Point(8, 628)
        Me.lblNewVersion.Name = "lblNewVersion"
        Me.lblNewVersion.Size = New System.Drawing.Size(65, 12)
        Me.lblNewVersion.TabIndex = 1
        Me.lblNewVersion.Text = "最新版本："
        '
        'tcLogin
        '
        Me.tcLogin.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tcLogin.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.tcLogin.Controls.Add(Me.tpLogin)
        Me.tcLogin.Controls.Add(Me.tpReg)
        Me.tcLogin.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.tcLogin.Location = New System.Drawing.Point(10, 34)
        Me.tcLogin.Margin = New System.Windows.Forms.Padding(0)
        Me.tcLogin.Name = "tcLogin"
        Me.tcLogin.Padding = New System.Drawing.Point(0, 0)
        Me.tcLogin.SelectedIndex = 0
        Me.tcLogin.Size = New System.Drawing.Size(225, 527)
        Me.tcLogin.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.tcLogin.TabIndex = 0
        '
        'tpLogin
        '
        Me.tpLogin.BackColor = System.Drawing.Color.Transparent
        Me.tpLogin.Controls.Add(Me.cbComList)
        Me.tpLogin.Controls.Add(Me.cbSave)
        Me.tpLogin.Controls.Add(Me.GroupBox2)
        Me.tpLogin.Controls.Add(Me.RadioButton1)
        Me.tpLogin.Controls.Add(Me.GroupBox1)
        Me.tpLogin.Controls.Add(Me.RadioButton2)
        Me.tpLogin.Controls.Add(Me.btnLoginCom)
        Me.tpLogin.Controls.Add(Me.btnLogin)
        Me.tpLogin.Controls.Add(Me.in_VM)
        Me.tpLogin.Controls.Add(Me.lbl_In_vm)
        Me.tpLogin.Controls.Add(Me.lbl_in_Info)
        Me.tpLogin.Controls.Add(Me.Label12)
        Me.tpLogin.Controls.Add(Me.Label11)
        Me.tpLogin.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.tpLogin.Location = New System.Drawing.Point(4, 29)
        Me.tpLogin.Margin = New System.Windows.Forms.Padding(0)
        Me.tpLogin.Name = "tpLogin"
        Me.tpLogin.Size = New System.Drawing.Size(217, 494)
        Me.tpLogin.TabIndex = 0
        Me.tpLogin.Text = "用户登录"
        Me.tpLogin.UseVisualStyleBackColor = True
        '
        'cbComList
        '
        Me.cbComList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbComList.FormattingEnabled = True
        Me.cbComList.Location = New System.Drawing.Point(6, 412)
        Me.cbComList.Name = "cbComList"
        Me.cbComList.Size = New System.Drawing.Size(205, 24)
        Me.cbComList.TabIndex = 6
        Me.cbComList.Visible = False
        '
        'cbSave
        '
        Me.cbSave.AutoSize = True
        Me.cbSave.Checked = True
        Me.cbSave.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbSave.Location = New System.Drawing.Point(65, 387)
        Me.cbSave.Name = "cbSave"
        Me.cbSave.Size = New System.Drawing.Size(91, 20)
        Me.cbSave.TabIndex = 4
        Me.cbSave.Text = "保存密码"
        Me.cbSave.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.in_WORKID)
        Me.GroupBox2.Controls.Add(Me.in_COMID)
        Me.GroupBox2.Controls.Add(Me.in_COMPWS)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(6, 174)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox2.Size = New System.Drawing.Size(202, 141)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        '
        'in_WORKID
        '
        Me.in_WORKID.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.in_WORKID.Location = New System.Drawing.Point(59, 59)
        Me.in_WORKID.Name = "in_WORKID"
        Me.in_WORKID.Size = New System.Drawing.Size(116, 26)
        Me.in_WORKID.TabIndex = 1
        '
        'in_COMID
        '
        Me.in_COMID.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.in_COMID.Location = New System.Drawing.Point(59, 20)
        Me.in_COMID.Name = "in_COMID"
        Me.in_COMID.Size = New System.Drawing.Size(116, 26)
        Me.in_COMID.TabIndex = 1
        '
        'in_COMPWS
        '
        Me.in_COMPWS.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.in_COMPWS.Location = New System.Drawing.Point(61, 101)
        Me.in_COMPWS.Name = "in_COMPWS"
        Me.in_COMPWS.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.in_COMPWS.Size = New System.Drawing.Size(114, 26)
        Me.in_COMPWS.TabIndex = 1
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label23.Location = New System.Drawing.Point(3, 59)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(72, 16)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "员工号："
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(178, 59)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(16, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "*"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "公司号："
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(178, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(16, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "*"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 101)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "密码："
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(178, 101)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(16, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "*"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(26, 16)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(106, 20)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "手机号登录"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.in_Phone)
        Me.GroupBox1.Controls.Add(Me.in_PWS)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 29)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox1.Size = New System.Drawing.Size(202, 112)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'in_Phone
        '
        Me.in_Phone.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.in_Phone.Location = New System.Drawing.Point(59, 26)
        Me.in_Phone.Name = "in_Phone"
        Me.in_Phone.Size = New System.Drawing.Size(116, 26)
        Me.in_Phone.TabIndex = 1
        '
        'in_PWS
        '
        Me.in_PWS.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.in_PWS.Location = New System.Drawing.Point(61, 65)
        Me.in_PWS.Name = "in_PWS"
        Me.in_PWS.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.in_PWS.Size = New System.Drawing.Size(114, 26)
        Me.in_PWS.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "手机："
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(178, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "*"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "密码："
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(178, 65)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(16, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "*"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Enabled = False
        Me.RadioButton2.Location = New System.Drawing.Point(26, 156)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(122, 20)
        Me.RadioButton2.TabIndex = 3
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "员工账号登录"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'btnLoginCom
        '
        Me.btnLoginCom.Location = New System.Drawing.Point(66, 443)
        Me.btnLoginCom.Name = "btnLoginCom"
        Me.btnLoginCom.Size = New System.Drawing.Size(89, 29)
        Me.btnLoginCom.TabIndex = 2
        Me.btnLoginCom.Text = "登录账套"
        Me.btnLoginCom.UseVisualStyleBackColor = True
        '
        'btnLogin
        '
        Me.btnLogin.Location = New System.Drawing.Point(65, 443)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(89, 29)
        Me.btnLogin.TabIndex = 2
        Me.btnLogin.Text = "登  录"
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'in_VM
        '
        Me.in_VM.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.in_VM.Location = New System.Drawing.Point(70, 334)
        Me.in_VM.Name = "in_VM"
        Me.in_VM.Size = New System.Drawing.Size(65, 26)
        Me.in_VM.TabIndex = 1
        '
        'lbl_In_vm
        '
        Me.lbl_In_vm.AutoSize = True
        Me.lbl_In_vm.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbl_In_vm.ForeColor = System.Drawing.Color.Red
        Me.lbl_In_vm.Location = New System.Drawing.Point(138, 334)
        Me.lbl_In_vm.Name = "lbl_In_vm"
        Me.lbl_In_vm.Size = New System.Drawing.Size(16, 16)
        Me.lbl_In_vm.TabIndex = 0
        Me.lbl_In_vm.Text = "*"
        '
        'lbl_in_Info
        '
        Me.lbl_in_Info.AutoSize = True
        Me.lbl_in_Info.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbl_in_Info.ForeColor = System.Drawing.Color.Red
        Me.lbl_in_Info.Location = New System.Drawing.Point(36, 367)
        Me.lbl_in_Info.Name = "lbl_in_Info"
        Me.lbl_in_Info.Size = New System.Drawing.Size(0, 12)
        Me.lbl_in_Info.TabIndex = 0
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Red
        Me.Label12.Location = New System.Drawing.Point(187, 334)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(16, 16)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "*"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label11.Location = New System.Drawing.Point(3, 334)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "验证码："
        '
        'tpReg
        '
        Me.tpReg.BackColor = System.Drawing.Color.Transparent
        Me.tpReg.Controls.Add(Me.rtxtResult)
        Me.tpReg.Controls.Add(Me.lbl_vm)
        Me.tpReg.Controls.Add(Me.lbl_InfoPhone)
        Me.tpReg.Controls.Add(Me.lbl_InfoPWS)
        Me.tpReg.Controls.Add(Me.lbl_Info)
        Me.tpReg.Controls.Add(Me.btnReg)
        Me.tpReg.Controls.Add(Me.reg_vm)
        Me.tpReg.Controls.Add(Me.reg_email)
        Me.tpReg.Controls.Add(Me.reg_pws2)
        Me.tpReg.Controls.Add(Me.Label13)
        Me.tpReg.Controls.Add(Me.reg_pws)
        Me.tpReg.Controls.Add(Me.lbl_result)
        Me.tpReg.Controls.Add(Me.Label14)
        Me.tpReg.Controls.Add(Me.Label15)
        Me.tpReg.Controls.Add(Me.Label16)
        Me.tpReg.Controls.Add(Me.reg_comname)
        Me.tpReg.Controls.Add(Me.Label17)
        Me.tpReg.Controls.Add(Me.Label18)
        Me.tpReg.Controls.Add(Me.Label19)
        Me.tpReg.Controls.Add(Me.reg_phone)
        Me.tpReg.Controls.Add(Me.Label20)
        Me.tpReg.Controls.Add(Me.Label21)
        Me.tpReg.Controls.Add(Me.Label22)
        Me.tpReg.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.tpReg.Location = New System.Drawing.Point(4, 29)
        Me.tpReg.Margin = New System.Windows.Forms.Padding(0)
        Me.tpReg.Name = "tpReg"
        Me.tpReg.Size = New System.Drawing.Size(217, 494)
        Me.tpReg.TabIndex = 1
        Me.tpReg.Text = "用户注册"
        Me.tpReg.UseVisualStyleBackColor = True
        '
        'rtxtResult
        '
        Me.rtxtResult.ForeColor = System.Drawing.SystemColors.WindowText
        Me.rtxtResult.Location = New System.Drawing.Point(3, 401)
        Me.rtxtResult.Name = "rtxtResult"
        Me.rtxtResult.ReadOnly = True
        Me.rtxtResult.Size = New System.Drawing.Size(211, 90)
        Me.rtxtResult.TabIndex = 24
        Me.rtxtResult.Text = "请联系客服或者代理商注册您的账套"
        '
        'lbl_vm
        '
        Me.lbl_vm.AutoSize = True
        Me.lbl_vm.ForeColor = System.Drawing.Color.Red
        Me.lbl_vm.Location = New System.Drawing.Point(144, 297)
        Me.lbl_vm.Name = "lbl_vm"
        Me.lbl_vm.Size = New System.Drawing.Size(56, 16)
        Me.lbl_vm.TabIndex = 23
        Me.lbl_vm.Text = "验证码"
        '
        'lbl_InfoPhone
        '
        Me.lbl_InfoPhone.AutoSize = True
        Me.lbl_InfoPhone.ForeColor = System.Drawing.Color.Red
        Me.lbl_InfoPhone.Location = New System.Drawing.Point(22, 52)
        Me.lbl_InfoPhone.Name = "lbl_InfoPhone"
        Me.lbl_InfoPhone.Size = New System.Drawing.Size(0, 16)
        Me.lbl_InfoPhone.TabIndex = 22
        '
        'lbl_InfoPWS
        '
        Me.lbl_InfoPWS.AutoSize = True
        Me.lbl_InfoPWS.ForeColor = System.Drawing.Color.Red
        Me.lbl_InfoPWS.Location = New System.Drawing.Point(22, 154)
        Me.lbl_InfoPWS.Name = "lbl_InfoPWS"
        Me.lbl_InfoPWS.Size = New System.Drawing.Size(0, 16)
        Me.lbl_InfoPWS.TabIndex = 22
        '
        'lbl_Info
        '
        Me.lbl_Info.AutoSize = True
        Me.lbl_Info.ForeColor = System.Drawing.Color.Red
        Me.lbl_Info.Location = New System.Drawing.Point(33, 330)
        Me.lbl_Info.Name = "lbl_Info"
        Me.lbl_Info.Size = New System.Drawing.Size(0, 16)
        Me.lbl_Info.TabIndex = 22
        '
        'btnReg
        '
        Me.btnReg.Enabled = False
        Me.btnReg.Location = New System.Drawing.Point(76, 333)
        Me.btnReg.Name = "btnReg"
        Me.btnReg.Size = New System.Drawing.Size(89, 29)
        Me.btnReg.TabIndex = 21
        Me.btnReg.Text = "马上注册"
        Me.btnReg.UseVisualStyleBackColor = True
        '
        'reg_vm
        '
        Me.reg_vm.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.reg_vm.Location = New System.Drawing.Point(76, 292)
        Me.reg_vm.Name = "reg_vm"
        Me.reg_vm.Size = New System.Drawing.Size(65, 26)
        Me.reg_vm.TabIndex = 20
        '
        'reg_email
        '
        Me.reg_email.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.reg_email.Location = New System.Drawing.Point(76, 237)
        Me.reg_email.Name = "reg_email"
        Me.reg_email.Size = New System.Drawing.Size(114, 26)
        Me.reg_email.TabIndex = 19
        '
        'reg_pws2
        '
        Me.reg_pws2.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.reg_pws2.Location = New System.Drawing.Point(76, 182)
        Me.reg_pws2.Name = "reg_pws2"
        Me.reg_pws2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.reg_pws2.Size = New System.Drawing.Size(114, 26)
        Me.reg_pws2.TabIndex = 18
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(193, 182)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(16, 16)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "*"
        '
        'reg_pws
        '
        Me.reg_pws.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.reg_pws.Location = New System.Drawing.Point(76, 127)
        Me.reg_pws.Name = "reg_pws"
        Me.reg_pws.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.reg_pws.Size = New System.Drawing.Size(114, 26)
        Me.reg_pws.TabIndex = 17
        '
        'lbl_result
        '
        Me.lbl_result.AutoSize = True
        Me.lbl_result.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbl_result.Location = New System.Drawing.Point(22, 379)
        Me.lbl_result.Name = "lbl_result"
        Me.lbl_result.Size = New System.Drawing.Size(0, 16)
        Me.lbl_result.TabIndex = 10
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label14.Location = New System.Drawing.Point(9, 292)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(72, 16)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "验证码："
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(193, 127)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(16, 16)
        Me.Label15.TabIndex = 13
        Me.Label15.Text = "*"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label16.Location = New System.Drawing.Point(9, 237)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(56, 16)
        Me.Label16.TabIndex = 4
        Me.Label16.Text = "邮箱："
        '
        'reg_comname
        '
        Me.reg_comname.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.reg_comname.Location = New System.Drawing.Point(59, 77)
        Me.reg_comname.Name = "reg_comname"
        Me.reg_comname.Size = New System.Drawing.Size(131, 26)
        Me.reg_comname.TabIndex = 16
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label17.Location = New System.Drawing.Point(4, 182)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 16)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "确认密码"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Red
        Me.Label18.Location = New System.Drawing.Point(193, 77)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(16, 16)
        Me.Label18.TabIndex = 6
        Me.Label18.Text = "*"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label19.Location = New System.Drawing.Point(9, 127)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 16)
        Me.Label19.TabIndex = 9
        Me.Label19.Text = "密码："
        '
        'reg_phone
        '
        Me.reg_phone.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.reg_phone.Location = New System.Drawing.Point(59, 24)
        Me.reg_phone.Name = "reg_phone"
        Me.reg_phone.Size = New System.Drawing.Size(131, 26)
        Me.reg_phone.TabIndex = 15
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label20.Location = New System.Drawing.Point(9, 77)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(56, 16)
        Me.Label20.TabIndex = 8
        Me.Label20.Text = "企业："
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Red
        Me.Label21.Location = New System.Drawing.Point(193, 24)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(16, 16)
        Me.Label21.TabIndex = 7
        Me.Label21.Text = "*"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label22.Location = New System.Drawing.Point(9, 24)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 16)
        Me.Label22.TabIndex = 5
        Me.Label22.Text = "手机："
        '
        'yunLogin
        '
        Me.AcceptButton = Me.btnLogin
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1079, 653)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "yunLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "欢迎使用"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.scRight.Panel1.ResumeLayout(False)
        Me.scRight.Panel2.ResumeLayout(False)
        Me.scRight.Panel2.PerformLayout()
        Me.scRight.ResumeLayout(False)
        Me.tcLogin.ResumeLayout(False)
        Me.tpLogin.ResumeLayout(False)
        Me.tpLogin.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tpReg.ResumeLayout(False)
        Me.tpReg.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wbMain As System.Windows.Forms.WebBrowser
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tcLogin As System.Windows.Forms.TabControl
    Friend WithEvents tpLogin As System.Windows.Forms.TabPage
    Friend WithEvents in_Phone As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tpReg As System.Windows.Forms.TabPage
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents in_VM As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents in_PWS As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnReg As System.Windows.Forms.Button
    Friend WithEvents reg_vm As System.Windows.Forms.TextBox
    Friend WithEvents reg_email As System.Windows.Forms.TextBox
    Friend WithEvents reg_pws2 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents reg_pws As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents reg_comname As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents reg_phone As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents in_COMID As System.Windows.Forms.TextBox
    Friend WithEvents in_COMPWS As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents in_WORKID As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lbl_Info As System.Windows.Forms.Label
    Friend WithEvents lbl_vm As System.Windows.Forms.Label
    Friend WithEvents lbl_InfoPhone As System.Windows.Forms.Label
    Friend WithEvents lbl_InfoPWS As System.Windows.Forms.Label
    Friend WithEvents lbl_result As System.Windows.Forms.Label
    Friend WithEvents lbl_in_Info As System.Windows.Forms.Label
    Friend WithEvents lbl_In_vm As System.Windows.Forms.Label
    Friend WithEvents cbSave As System.Windows.Forms.CheckBox
    Friend WithEvents lblNewVersion As System.Windows.Forms.Label
    Friend WithEvents lblCurrentVersion As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents rtxtResult As System.Windows.Forms.RichTextBox
    Friend WithEvents cbComList As System.Windows.Forms.ComboBox
    Friend WithEvents btnLoginCom As System.Windows.Forms.Button
    Friend WithEvents scRight As System.Windows.Forms.SplitContainer
    Friend WithEvents btnHome As System.Windows.Forms.Button
    Friend WithEvents btnShow As System.Windows.Forms.Button
End Class
