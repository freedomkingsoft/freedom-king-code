﻿Public Class Login
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Me.txtPS.Text = "" Then
            MsgBox("密码不能为空！", MsgBoxStyle.Information)
        Else
            If Vpass() Then
                '登录成功
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

                'MessageBox.Show(mdb.ReadStoredProcedure(FKG.myselfG.asasR, "test", "userid,5").Tables.Count.ToString)
                'MessageBox.Show(mdb.ReadStoredProcedure(FKG.myselfG.asasR, "test", "userid,8").Tables(0).Rows.Count)

                '试用版进行数据量验证()
                Select Case FKG.myselfG.sBanBen
                    Case "开发版"
                    Case "正式版"
                    Case Else
                        Dim iTimes As Integer = Int(Rnd() * 10) + 101
                        If mdb.Reader("select count(*) from JiBenCaoZuo ").Tables(0).Rows(0).Item(0) > iTimes Then
                            Exit Sub
                        End If
                End Select

                Dim ds As DataSet
                ds = mdb.Reader("select * from ZT")
                With ds.Tables(0).Rows(0)
                    FKG.myselfG.iBianHaoWeiShu = .Item("BianHaoWeiShu")
                    If .Item("NianFenWeiShu") = True Then
                        FKG.myselfG.iNianFenWeiShu = 4
                    Else
                        FKG.myselfG.iNianFenWeiShu = 2
                    End If
                    FKG.myselfG.sKMJS = .Item("KeMuWeiShu")
                    FKG.myselfG.dQiYongRiQi = .Item("QiYongRiQi")

                End With

                Dim d As Date
                d = Me.dtpLogin.Value

                If d < FKG.myselfG.dQiYongRiQi Then
                    MsgBox("登录日期不能早于系统启用日期!", MsgBoxStyle.Information, "提示")
                    Exit Sub
                End If

                If d.Date > mdb.Reader("select top 1 zzrq from FKZQ order by ny desc").Tables(0).Rows(0).Item(0) Then
                    'MsgBox(d.Year & "年度　帐套还没有启用!", MsgBoxStyle.Information, "提示")
                    If d.Year - CType(mdb.Reader("select top 1 zzrq from FKZQ order by ny desc").Tables(0).Rows(0).Item(0), Date).Year = 1 Then
                        If MsgBox(d.Year & "年度　帐套还没有启用!您是否想要现在就启用" & d.Year & "年度帐套？", MsgBoxStyle.YesNoCancel, "年度帐套未启用") = MsgBoxResult.Yes Then
                            '创建新年度帐套并复制上年度科目代码
                            CopyKMDM()
                        Else
                            Exit Sub
                        End If
                    Else
                        MsgBox("您选择的登录年份错误，请重新选择！", MsgBoxStyle.Information, "提示")
                        Exit Sub
                    End If

                End If
                FKG.myselfG.NianFen = d.Year
                FKG.myselfG.YueFen = d.Month
                FKG.myselfG.RiQi = d
                FKG.myselfG.YongHu = Me.txtYongHu.Text


                If IO.File.Exists(My.Application.Info.DirectoryPath & "\FirstRun.txt") Then
                    Dim dlg As New FKAdmin.FKAdmin
                    Me.Hide()
                    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                        IO.File.Delete(My.Application.Info.DirectoryPath & "\FirstRun.txt")
                        Me.Show()
                    End If
                Else
                    Dim dlg As New FreedomKing.FreedomKingMain
                    dlg.dlgLogin = Me
                    Me.Hide()
                    'Me.Close()
                    dlg.ShowDialog()
                End If

                'Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                Me.txtPS.Clear()
                Me.ActiveControl = Me.txtPS
            End If
        End If
    End Sub

    Private Sub CopyKMDM()
      
        Dim mdbD As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdbD.Write("insert into KMDM (KMDM,Nian,ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe) select KMDM," & Me.dtpLogin.Value.Year & ",ZJF,KMMC,KMQM,KMLB,YEFX,ZYLX,ZYGS,SLDW,WBDW,KMJB,SFMJ,FJDM,SFTH,SFXJ,SFYH,SFJY,SFXJDJW,HSLBKC,HSLBGZ,HSLBWL,HSLBWW,SFZF,HangYe from KMDM where Nian=" & Me.dtpLogin.Value.Year - 1 & "  and KMDM not in (select KMDM from KMDM where Nian=" & Me.dtpLogin.Value.Year & ")")

        'CopyKMFZSC
        'mdbD.Write("delete from KMFZSX where Nian=" & iNian + 1)
        mdbD.Write("insert into KMFZSX (科目代码,Nian,辅助属性名称,辅助属性值) select 科目代码," & Me.dtpLogin.Value.Year & ",辅助属性名称,辅助属性值 from KMFZSX where Nian=" & Me.dtpLogin.Value.Year - 1 & " and 科目代码 not in (select 科目代码 from KMFZSX WHERE NIAN=" & Me.dtpLogin.Value.Year & ")")

        '创建FKZQ
        Dim i As Integer
        Dim dateStart As Date
        dateStart = "#" & Me.dtpLogin.Value.Year & "-1" & "-1#"
        mdbD.Write("delete from FKZQ where NY like '" & dateStart.Year & "%'")
        For i = 1 To 12

            mdbD.Write("insert into FKZQ (NY,KSRQ,ZZRQ,KCJZ,GZJZ,JIZHANG,JIEZHANG) VALUES ('" & (Me.dtpLogin.Value.Year).ToString & dateStart.Month.ToString.PadLeft(2, "0") & "','" & dateStart & "','" & dateStart.AddMonths(1).AddDays(-1) & "','false','false','false','false')")
            dateStart = dateStart.AddMonths(1)
        Next

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        End
    End Sub

    Private Sub JinChan()

        On Error GoTo ANTI

        Dim Lujing As String
        Dim Suiji(7) As Byte
        Dim n As Long
        Dim Panduan As Double
        Lujing = Application.ExecutablePath

        For n = 1 To 7
            Randomize()
            Suiji(n) = Int(Rnd() * 2)
            Panduan = Panduan + Suiji(n)
        Next n


        If Panduan > 0 Then
            Shell(Lujing, AppWinStyle.NormalFocus)
            End
        End If
        Exit Sub
ANTI:
        End
    End Sub

    Private Sub Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '通过读取配置文件动态更改各要素
        Dim sInfo As String
        Dim sCopyright As String
        Dim sContect As String
        Try
            sContect = IO.File.ReadAllText(My.Application.Info.DirectoryPath & "\Config.txt")

            sInfo = Split(sContect, ",").GetValue(9)
            sCopyright = Split(sContect, ",").GetValue(10)
        Catch ex As Exception
            sCopyright = ""
            sContect = ""
            sInfo = My.Application.Info.Title
        End Try

        Me.Label2.Text = sInfo
        Me.Label3.Text = sCopyright

        Try

            '首先确定数据库连接字符串是否正确,否则就提示配置字符串.

            FKG.myselfG.asasR = FKG.myselfG.getConStr
            FKG.myselfG.asasW = FKG.myselfG.getConStr

            Dim mdbt As New OleDb.OleDbConnection

            Dim sEx As String = ""
            Try
                mdbt.ConnectionString = FKG.myselfG.asasR
                mdbt.Open()
            Catch ex As Exception
                sEx = ex.Message.ToString
            Finally
                mdbt.Close()
            End Try

            If sEx = "" Then
            Else
                '启动配置界面
                Dim dlgConfig As New DBConfig(sEx)
                If dlgConfig.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Login_Load(sender, e)
                Else
                    End
                End If
            End If

            Dim sOld As String = FKG.myselfG.asasR
            Dim sOlW As String = FKG.myselfG.asasW
            FKG.myselfG.asasR = ""
            FKG.myselfG.asasW = ""

            Dim Suiji(7) As Byte
            Dim n As Long
            Dim Panduan As Double

AAASCT:
            For n = 1 To 6
                Randomize()
                Suiji(n) = Int(Rnd() * 2)
                Panduan = Panduan + Suiji(n)
            Next

            Try

                Suiji(0) = Now.Second / Panduan
                Panduan = 0
            Catch ex As Exception
                FKG.myselfG.asasR = sOld
                FKG.myselfG.asasW = sOlW

                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

                Dim sMCO As String = Me.MCO(CPUID, HID)

                Dim sPubCode As String = ""
                Dim ds As DataSet
                ds = mdb.Reader("select sPubKey from systemp where sComputerName='" & sMCO & "'")
                If ds.Tables(0).Rows.Count > 0 Then
                    sPubCode = ds.Tables(0).Rows(0).Item(0).ToString
                End If

                '从网络验证，不再需要注册码文件
                Dim ws As New wsbdsp.Service
                Dim sToken As String = ""
                sToken = Me.getToken(sMCO.Replace("开发版服务器端", ""))

                If ws.HelloWorld(sToken) = "" AndAlso sPubCode = "" Then
                    MCO(CPUID, HID) '此操作是为了保证全局版本号的一致性
                    Dim dlg As New FKreg.Freg
                    dlg.TopLevel = True
                    If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                        sPubCode = mdb.Reader("select sPubKey from systemp where sComputerName='" & sMCO & "'").Tables(0).Rows(0).Item(0).ToString
                    Else
                        End
                        Login_Load(sender, e)
                    End If
                End If
                '已改为从MDB读取

                If sPubCode <> "" Then
                    sPubCode = Decrypt(sPubCode, Application.StartupPath & "\pra.key")
                    sPubCode = sPubCode.Remove(0, sPubCode.IndexOf("</BitStrength>") + 14)
                    FKG.myselfG.sMC = sMCO

                    '试用版进行数据量验证
                    Select Case FKG.myselfG.sBanBen
                        Case "开发版"

                        Case "正式版"

                        Case Else
                            Dim iTimes As Integer = Int(Rnd() * 10) + 101
                            If mdb.Reader("select count(*) from JiBenCaoZuo ").Tables(0).Rows(0).Item(0) > iTimes Then
                                Exit Sub
                            End If
                    End Select

                    FKG.myselfG.sPubKey = sPubCode.Substring(0, 755)
                    'FKG.myselfG.sPubKey = sPubCode.Substring(0, 755 + 15)
                    sPubCode = sPubCode.Remove(0, FKG.myselfG.sPubKey.Length + 9)

                    FKG.myselfG.sRC = (sPubCode.Substring(0, sPubCode.IndexOf("</RedCode>")))
                End If

                Select Case (ws.HelloWorld(sToken) <> "") OrElse SignatureDeformatter(FKG.myselfG.sPubKey, GetHash(FKG.myselfG.sMC), FKG.myselfG.sRC)
                    Case True

                        Select Case FKG.myselfG.sClient
                            Case "服务器端"
                                My.Settings.Item("sClientFile") = My.Application.Info.DirectoryPath & "\FKData\Freedomkingdata.mdb"
                            Case "客户端"
                                'ComDB.COMDB.CompactAccessDB(FKG.myselfG.asasW, My.Settings.Item("sClientFile"))
                            Case Else
                                End
                        End Select


                        'If System.IO.File.Exists(My.Settings.Item("sClientFile")) Then 不用再判断数据库文件是否存在
                        If True Then


                            '  FillYonghu() 更改为先压缩数据库,然后再进行此操作

                            Select Case FKG.myselfG.sClient
                                Case "服务器端"

                                Case "客户端"

                                Case Else
                                    End
                            End Select

                            FillYonghu() '更改为先压缩数据库,然后再进行此操作

                        Else
                            FKG.myselfG.asasR = ""
                            MsgBox(Decrypt("iVG4I2BEAIKl8VucXoDQIPqcKlNze92yj5idkdcX+wRWmHMXC0XaCoF0rtPEZHbtzncRk9PFbCCJ+2A+pbT6hQcvca6ze9cMun+c2uKneKO7ju0seFBoaxnWT9BgFVIExF4fWNeLRMviB+7o9W89zh+TA4E7BVUF1IUH3+paBVE=", Application.StartupPath & "\pra.key"), MsgBoxStyle.Information, "提示")
                            End
                        End If
                    Case False

                        Dim dlg As New FKreg.Freg
                        dlg.TopLevel = True
                        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                            Login_Load(sender, e)
                        Else
                            End
                        End If
                End Select

                Exit Sub
            End Try
            GoTo AAASCT

        Catch exXML As Xml.XmlException
            MsgBox("Record文件异常，请检查相关文件", MsgBoxStyle.Information, "提示")
            FKG.myselfG.asasR = ""
            Exit Sub
        Catch ex As Exception
            Dim dlg As New FKreg.Freg
            dlg.TopLevel = True
            If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                Login_Load(sender, e)
            Else
                End
            End If

            '           End
        End Try

    End Sub

    Private Function FillYonghu() As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Try
            Me.lblQiyeming.Text = mdb.Reader("select Qiyeming from zt").Tables(0).Rows(0).Item(0).ToString
        Catch ex As Exception
            MsgBox("出现关键性错误，系统无法运行！", MsgBoxStyle.Information, "提示")
            End
        End Try
        FKG.myselfG.QiYeMingCheng = Me.lblQiyeming.Text

        Dim iLoginNumber As Integer = FKG.myselfG.YunUserInfo.Item("LoginNumber")
        If mdb.DataBind(Me.txtYongHu, "select top " & iLoginNumber & " YongHuName from Yonghu where yonghujibie <>5") Then
            If Me.txtYongHu.Items.Count > 0 Then
                Me.txtYongHu.SelectedIndex = 0
            End If
            Return True
        Else
            Return False
        End If
    End Function

    Private Function Vpass() As Boolean
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Dim ds As DataSet

        If Me.txtYongHu.SelectedIndex < 0 Then
            ds = mdb.Reader("select pwdcompare('" & Me.txtPS.Text & "',YongHuMiMa),YongHuID from YongHu where YongHuName='" & Me.txtYongHu.Text & "' and YongHuJiBie=5 and YongHuID=5")
        Else
            ds = mdb.Reader("select pwdcompare('" & Me.txtPS.Text & "',YongHuMiMa),YongHuID from YongHu where YongHuName='" & Me.txtYongHu.Text & "'")
        End If


        If ds.Tables(0).Rows.Count = 0 Then
            MsgBox("登录用户不存在,请重新输入！", MsgBoxStyle.Information, "提示")
            Return False
        End If
        'If Me.txtPS.Text = ds.Tables(0).Rows(0).Item(0) Then
        If ds.Tables(0).Rows(0).Item(0) = 1 Then
            FKG.myselfG.YongHuID = ds.Tables(0).Rows(0).Item(1)

            '用户状态的确定
            ds.Clear()
            ds = mdb.Reader("select * from YHZhuangTai where YongHuName='" & Me.txtYongHu.Text & "'")
            FKG.myselfG.dLogin = Now

            Dim tLogin As String
            tLogin = Format(Now, "yyyy-MM-dd hh:mm:ss")
            FKG.myselfG.dLogin = tLogin
            If ds.Tables(0).Rows.Count = 0 Then
                mdb.Write("insert into YHZhuangTai (YongHuName,YHMAC,A_JBID,Logintime) values ('" & Me.txtYongHu.Text & "','" & My.Computer.Name & "','-1', '" & tLogin & "')")
                Return True
            Else
                If My.Computer.Name = ds.Tables(0).Rows(0).Item("YHMAC") Then
                    '本机已登录
                    If ds.Tables(0).Rows(0).Item("A_JBID") <> "-1" Then
                        '稍等，再判断再处理,等待超过一定时间则认为上次为异常退出。
                        System.Threading.Thread.Sleep(500)
                        Dim iJBID As String = ds.Tables(0).Rows(0).Item("A_JBID")

                        ds = mdb.Reader("select * from YHZhuangTai where YongHuName='" & Me.txtYongHu.Text & "'")
                        If iJBID = ds.Tables(0).Rows(0).Item("A_JBID") Then
                            '确认上次为异常退出，执行有关处理
                            CancelSave(iJBID)
                        End If
                    End If
                    mdb.Write("update  YHZhuangTai set Logintime='" & tLogin & "',A_JBID='-1' where YongHuName='" & Me.txtYongHu.Text & "'")
                    Return True
                Else
                    '其他电脑已登录
                    If ds.Tables(0).Rows(0).Item("A_JBID") <> "-1" Then
                        '稍等，再判断再处理,等待超过一定时间则认为上次为异常退出。
                        System.Threading.Thread.Sleep(500)
                        Dim iJBID As String = ds.Tables(0).Rows(0).Item("A_JBID")

                        ds = mdb.Reader("select * from YHZhuangTai where YongHuName='" & Me.txtYongHu.Text & "'")
                        If iJBID = ds.Tables(0).Rows(0).Item("A_JBID") Then
                            '确认上次为异常退出，执行有关处理
                            CancelSave(iJBID)
                        End If
                    End If
                    mdb.Write("update YHZhuangTai set YHMAC='" & My.Computer.Name & "',Logintime='" & tLogin & "',A_JBID='-1' where YongHuName='" & Me.txtYongHu.Text & "'")
                    Return True
                End If

            End If
            Return True
        Else
            MsgBox("登录密码错误,请重新输入！", MsgBoxStyle.Information, "提示")
            Return False
        End If


    End Function

    Private Sub CancelSave(ByVal iJiBenCaoZuoid As String)
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)

        If mdb.Write("delete from KuCun where A_JBID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
            Exit Sub
        End If

        If mdb.Write("delete from GongZi where A_JBID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
            Exit Sub
        End If

        If mdb.Write("delete from PZ where A_JBID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
            Exit Sub
        End If
        If mdb.Write("delete from JiBenCaoZuo where  A_ID='" & iJiBenCaoZuoid & "'") = False Then
            MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
            Exit Sub
        End If
        'Dim sGongNeng As String
        'sGongNeng = mdb.Reader("select ConText from GongNengShu where ID=" & iGongnengID).Tables(0).Rows(0).Item(0)
        'If mdb.Write("update JiBenCaoZuo set 删除=true,删除时间=#" & Now & "#, 删除人='" & FKG.myselfG.YongHu & "',功能名称='" & sGongNeng & "' where A_ID=" & iJiBenCaoZuoid) = False Then
        '    MsgBox("出现严重异常，并且未能成功撤销保存操作，请手工删除当前操作以保证数据的正确性！", MsgBoxStyle.Exclamation, "警告")
        'End If

        ''重新编写行数
        'mdb.Write("UPDATE kucun AS t SET 行数 = dcount(""行数"",""Kucun"",""编号='" & sBianHao & "' and 行数<="" & t.行数) WHERE 编号='" & sBianHao & "'")
        'mdb.Write("UPDATE GongZi AS t SET 行数 = dcount(""行数"",""GongZi"",""编号='" & sBianHao & "' and 行数<="" & t.行数) WHERE 编号='" & sBianHao & "'")
        'mdb.Write("UPDATE PZ AS t SET 行数 = dcount(""行数"",""PZ"",""凭证号=(select distinct 凭证号 from pz where 编号='" & sBianHao & "') and 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen & "  and 行数<="" & t.行数) WHERE 凭证号=(select distinct 凭证号 from pz where 编号='" & sBianHao & "') and 年份=" & FKG.myselfG.NianFen & " and 月份=" & FKG.myselfG.YueFen)
        'mdb.Write("UPDATE PZ AS t SET 行数 = dcount(""行数"",""PZ"",""编号='" & sBianHao & "' and 行数<="" & t.行数) WHERE 编号='" & sBianHao & "'")
    End Sub

    Private Function Decrypt(ByVal p_inputString As String, ByVal p_strKeyPath As String) As String
        Dim fileString As String = ""
        Dim outString As String = ""

        'If IO.File.Exists(p_strKeyPath) Then
        '    Dim streamReader As New IO.StreamReader(p_strKeyPath, True)
        '    fileString = streamReader.ReadToEnd
        '    streamReader.Close()
        'End If

        Try
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            fileString = mdb.Reader("select sPraKey from systemp where sComputerName='FreedomKing'").Tables(0).Rows(0).Item(0).ToString

            If fileString.Contains("0") Then
            Else
                Return ""
            End If
        Catch ex As Exception
            fileString = ""
        End Try

        If fileString <> "" Then
            Dim bitStrengthString As String = fileString.Substring(0, fileString.IndexOf("</BitStrength>") + 14)
            fileString = fileString.Replace(bitStrengthString, "")
            Dim bitStrength As Int32 = Convert.ToInt32(bitStrengthString.Replace("<BitStrength>", "").Replace("</BitStrength>", ""))
            Try
                outString = DecryptString(p_inputString, bitStrength, fileString)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End If

        Return outString

    End Function

    Private Function DecryptString(ByVal inputString As String, ByVal dwKeySize As Integer, ByVal xmlString As String) As String
        Dim rsaCryptoServiceProvider As New System.Security.Cryptography.RSACryptoServiceProvider(dwKeySize)
        rsaCryptoServiceProvider.FromXmlString(xmlString)

        Dim base64BlockSize As Integer
        If (dwKeySize / 8) Mod 3 <> 0 Then
            base64BlockSize = Int(((dwKeySize / 8) / 3) * 4) + 4
        Else
            base64BlockSize = Int((dwKeySize / 8) / 3) * 4

        End If

        Dim iterations As Integer = Int(inputString.Length / base64BlockSize)
        Dim arrayList As New System.Collections.ArrayList

        Dim i As Integer
        For i = 0 To iterations
            Dim encryptedBytes() As Byte
            Dim itemp As Integer

            If inputString.Length >= base64BlockSize Then
                itemp = base64BlockSize - 2
            Else
                itemp = inputString.Length
            End If

            encryptedBytes = Convert.FromBase64String(inputString.Substring(itemp * i, itemp))

            Array.Reverse(encryptedBytes)

            arrayList.AddRange(rsaCryptoServiceProvider.Decrypt(encryptedBytes, True))

        Next

        Return System.Text.Encoding.UTF32.GetString(TryCast(arrayList.ToArray(Type.[GetType]("System.Byte")), Byte()))

    End Function

    Private Function CPUID() As String
        Dim sCPUID As String = ""
        Dim cpuSet
        Dim cpu

        Try
            cpuSet = GetObject("winmgmts:{impersonationLevel=impersonate}").InstancesOf("Win32_Processor")
            For Each cpu In cpuSet
                sCPUID = sCPUID & cpu.ProcessorId
            Next
        Catch ex As Exception
            sCPUID = ""
        End Try

        Return sCPUID

    End Function

    Private Function HID() As String
        Dim HDid As String = ""
        Try
            Dim cimobject As New Management.ManagementClass("win32_diskdrive")
            Dim moc As Management.ManagementObjectCollection
            moc = cimobject.GetInstances()
            Dim mo As Management.ManagementObject

            For Each mo In moc
                'HDid = HDid & mo.Properties("signature").Value.ToString
                HDid = HDid & mo.Properties("PNPDeviceID").Value.ToString
                HDid = HDid.Replace("_", "")
                HDid = HDid.Remove(0, mo.Properties("Caption").Value.ToString.Length)
                Exit For
            Next
        Catch ex As Exception
            HDid = ""
        End Try

        Return HDid

    End Function

    Private Function MCO(ByVal sCDO As String, ByVal sHDO As String) As String
        Dim sMCO As String = ""
        Dim i As Integer

        sCDO = sCDO & sHDO

        Do While sCDO.Length < 36
            sCDO = sCDO & sCDO
        Loop

        If sCDO = "" Then
            sCDO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        End If

        If sCDO = "" Then
            Return ""
        Else
            Dim sZ As String
            Dim iZ As Integer
            Dim iJG As Integer
            Dim sJG As String
            For i = 0 To 35
                sZ = sCDO.Substring(i, 1)
                iZ = Asc(sZ)

                iJG = ((iZ + i) Mod 26) + 65
                sJG = Chr(iJG)
                sMCO = sMCO & sJG
            Next
        End If


        FKG.myselfG.sVersion = My.Application.Info.Version.ToString

        'FKG.myselfG.sBanBen = "试用版"
        FKG.myselfG.sBanBen = "开发版"
        'FKG.myselfG.sBanBen = "正式版"
        'FKG.myselfG.sBanBen = ""

        FKG.myselfG.sClient = "服务器端"
        ''FKG.myselfG.sClient = "客户端"

        Return sMCO & FKG.myselfG.sVersion & FKG.myselfG.sBanBen & FKG.myselfG.sClient

    End Function


    Private Function SignatureDeformatter(ByVal p_strKeyPublic As String, ByVal p_strHashbyteDeformatter As String, ByVal p_strDeformatterData As String) As Boolean
        Try
            Dim DeformatterData As Byte()
            Dim HashbyteDeformatter As Byte()

            HashbyteDeformatter = Convert.FromBase64String(p_strHashbyteDeformatter)
            Dim RSA As New System.Security.Cryptography.RSACryptoServiceProvider()

            RSA.FromXmlString(p_strKeyPublic)
            Dim RSADeformatter As New System.Security.Cryptography.RSAPKCS1SignatureDeformatter(RSA)
            '指定解密的时候HASH算法为MD5 
            RSADeformatter.SetHashAlgorithm("MD5")

            DeformatterData = Convert.FromBase64String(p_strDeformatterData)

            If RSADeformatter.VerifySignature(HashbyteDeformatter, DeformatterData) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
            'Throw ex
        End Try
    End Function

    Private Function GetHash(ByVal m_strSource As String) As String
        Try
            Dim strHashData As String
            '从字符串中取得Hash描述 
            Dim Buffer As Byte()
            Dim HashData As Byte()
            Dim MD5 As System.Security.Cryptography.HashAlgorithm = System.Security.Cryptography.HashAlgorithm.Create("MD5")
            Buffer = System.Text.Encoding.GetEncoding("GB2312").GetBytes(m_strSource)
            HashData = MD5.ComputeHash(Buffer)

            strHashData = Convert.ToBase64String(HashData)
            Return strHashData
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function getToken(ByVal sName As String) As String
        Dim sToken As String = ""
        Dim sT As String = ""
        Dim i As Integer
        For i = 0 To sName.Length - 1
            If Asc(sName.Substring(i, 1)) > 122 Then

            Else
                sT = sT & (Asc(sName.Substring(i, 1)) * i).ToString
            End If
        Next

        Dim sTR As String = "A"
        For i = 0 To sT.Length.ToString - 1
            If i Mod 7 = 0 Then
                sTR = sTR & sT.Substring(i, 1)
            End If
        Next

        sToken = sName & "," & sTR
        Return sToken
    End Function
End Class
