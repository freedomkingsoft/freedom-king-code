﻿Imports System.Windows.Forms

Public Class Dialog1

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub Dialog1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '通过读取配置文件动态更改各要素
        Dim sInfo As String
        Dim sContect As String
        Try
            sContect = IO.File.ReadAllText(My.Application.Info.DirectoryPath & "\config.txt")

            sInfo = Split(sContect, ",").GetValue(8)
        Catch ex As Exception
            sContect = ""
            sInfo = My.Application.Info.Title
        End Try

        Dim sURL As String
        sURL = My.Application.Info.DirectoryPath & "\HTML\" & sInfo

        Me.webB.Navigate(sURL)
    End Sub
End Class
