﻿Public NotInheritable Class AboutBox1

    Private Sub AboutBox1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '通过读取配置文件动态更改各要素
        Dim sInfo As String
        Dim sContect As String
        Try
            sContect = IO.File.ReadAllText(My.Application.Info.DirectoryPath & "\config.txt")

            sInfo = Split(sContect, ",").GetValue(2)
        Catch ex As Exception
            sContect = ""
            sInfo = My.Application.Info.Title
        End Try

        ' 设置此窗体的标题。
        Dim ApplicationTitle As String
        If My.Application.Info.Title <> "" Then
            ApplicationTitle = sInfo
        Else
            ApplicationTitle = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If
        Me.Text = String.Format("关于 {0}", ApplicationTitle)
        ' 初始化“关于”对话框显示的所有文字。
        ' TODO: 在项目的“应用程序”窗格中自定义此应用程序的程序集信息 
        '    属性对话框(在“项目”菜单下)。
        Me.LabelProductName.Text = sInfo
        Me.LabelVersion.Text = String.Format("版本 {0}", My.Application.Info.Version.ToString)
        Me.LabelCopyright.Text = Split(sContect, ",").GetValue(5)
        Me.LabelCompanyName.Text = Split(sContect, ",").GetValue(10)
        Me.TextBoxDescription.Text = Split(sContect, ",").GetValue(4) & vbNewLine & Split(sContect, ",").GetValue(5) & vbNewLine & Split(sContect, ",").GetValue(6) & vbNewLine & Split(sContect, ",").GetValue(7)
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub

End Class
