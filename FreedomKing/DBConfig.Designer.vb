﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DBConfig
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtDataSource = New System.Windows.Forms.TextBox
        Me.txtDababase = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtYonghu = New System.Windows.Forms.TextBox
        Me.txtPS = New System.Windows.Forms.TextBox
        Me.lblMSG = New System.Windows.Forms.Label
        Me.txtTuPian = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtBackupPath = New System.Windows.Forms.TextBox
        Me.PicPath = New System.Windows.Forms.FolderBrowserDialog
        Me.BackupPath = New System.Windows.Forms.FolderBrowserDialog
        Me.btnPic = New System.Windows.Forms.Button
        Me.btnBackup = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.nudBackup = New System.Windows.Forms.NumericUpDown
        Me.Button3 = New System.Windows.Forms.Button
        Me.btnSaveas = New System.Windows.Forms.Button
        Me.btnRead = New System.Windows.Forms.Button
        CType(Me.nudBackup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Button1.Location = New System.Drawing.Point(232, 344)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(101, 49)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "保存配置"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "请正确配置数据库参数"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(50, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "数据源"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.Location = New System.Drawing.Point(50, 143)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "数据库"
        '
        'txtDataSource
        '
        Me.txtDataSource.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtDataSource.Location = New System.Drawing.Point(52, 107)
        Me.txtDataSource.Name = "txtDataSource"
        Me.txtDataSource.Size = New System.Drawing.Size(216, 26)
        Me.txtDataSource.TabIndex = 2
        '
        'txtDababase
        '
        Me.txtDababase.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtDababase.Location = New System.Drawing.Point(52, 167)
        Me.txtDababase.Name = "txtDababase"
        Me.txtDababase.Size = New System.Drawing.Size(216, 26)
        Me.txtDababase.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(404, 344)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 49)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "关闭"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.Location = New System.Drawing.Point(50, 208)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "用户ID"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label5.Location = New System.Drawing.Point(50, 271)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "密码"
        '
        'txtYonghu
        '
        Me.txtYonghu.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtYonghu.Location = New System.Drawing.Point(52, 235)
        Me.txtYonghu.Name = "txtYonghu"
        Me.txtYonghu.ReadOnly = True
        Me.txtYonghu.Size = New System.Drawing.Size(216, 26)
        Me.txtYonghu.TabIndex = 2
        '
        'txtPS
        '
        Me.txtPS.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtPS.Location = New System.Drawing.Point(52, 295)
        Me.txtPS.Name = "txtPS"
        Me.txtPS.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPS.ReadOnly = True
        Me.txtPS.Size = New System.Drawing.Size(216, 26)
        Me.txtPS.TabIndex = 2
        '
        'lblMSG
        '
        Me.lblMSG.AutoSize = True
        Me.lblMSG.Font = New System.Drawing.Font("黑体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblMSG.ForeColor = System.Drawing.Color.Red
        Me.lblMSG.Location = New System.Drawing.Point(12, 25)
        Me.lblMSG.Name = "lblMSG"
        Me.lblMSG.Size = New System.Drawing.Size(0, 16)
        Me.lblMSG.TabIndex = 1
        '
        'txtTuPian
        '
        Me.txtTuPian.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtTuPian.Location = New System.Drawing.Point(343, 107)
        Me.txtTuPian.Name = "txtTuPian"
        Me.txtTuPian.Size = New System.Drawing.Size(216, 26)
        Me.txtTuPian.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label6.Location = New System.Drawing.Point(341, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "图片保存位置"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label7.Location = New System.Drawing.Point(341, 143)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(136, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "备份文件保存位置"
        '
        'txtBackupPath
        '
        Me.txtBackupPath.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBackupPath.Location = New System.Drawing.Point(343, 167)
        Me.txtBackupPath.Name = "txtBackupPath"
        Me.txtBackupPath.Size = New System.Drawing.Size(216, 26)
        Me.txtBackupPath.TabIndex = 5
        '
        'btnPic
        '
        Me.btnPic.Location = New System.Drawing.Point(561, 102)
        Me.btnPic.Name = "btnPic"
        Me.btnPic.Size = New System.Drawing.Size(57, 41)
        Me.btnPic.TabIndex = 6
        Me.btnPic.Text = "浏览"
        Me.btnPic.UseVisualStyleBackColor = True
        '
        'btnBackup
        '
        Me.btnBackup.Location = New System.Drawing.Point(561, 157)
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.Size = New System.Drawing.Size(57, 41)
        Me.btnBackup.TabIndex = 7
        Me.btnBackup.Text = "浏览"
        Me.btnBackup.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label8.Location = New System.Drawing.Point(341, 238)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(136, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "备份文件保存天数"
        '
        'nudBackup
        '
        Me.nudBackup.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.nudBackup.Location = New System.Drawing.Point(483, 232)
        Me.nudBackup.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.nudBackup.Minimum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudBackup.Name = "nudBackup"
        Me.nudBackup.Size = New System.Drawing.Size(88, 29)
        Me.nudBackup.TabIndex = 8
        Me.nudBackup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudBackup.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(80, 344)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(83, 49)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "清除配置"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnSaveas
        '
        Me.btnSaveas.Location = New System.Drawing.Point(519, 349)
        Me.btnSaveas.Name = "btnSaveas"
        Me.btnSaveas.Size = New System.Drawing.Size(89, 38)
        Me.btnSaveas.TabIndex = 0
        Me.btnSaveas.Text = "配置另存为"
        Me.btnSaveas.UseVisualStyleBackColor = True
        '
        'btnRead
        '
        Me.btnRead.Location = New System.Drawing.Point(519, 295)
        Me.btnRead.Name = "btnRead"
        Me.btnRead.Size = New System.Drawing.Size(89, 38)
        Me.btnRead.TabIndex = 0
        Me.btnRead.Text = "读取配置文件"
        Me.btnRead.UseVisualStyleBackColor = True
        '
        'DBConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(630, 405)
        Me.Controls.Add(Me.nudBackup)
        Me.Controls.Add(Me.btnBackup)
        Me.Controls.Add(Me.btnPic)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtBackupPath)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtTuPian)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.txtPS)
        Me.Controls.Add(Me.txtYonghu)
        Me.Controls.Add(Me.txtDababase)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtDataSource)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblMSG)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnRead)
        Me.Controls.Add(Me.btnSaveas)
        Me.Controls.Add(Me.Button1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DBConfig"
        Me.Text = "配置数据库"
        CType(Me.nudBackup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDataSource As System.Windows.Forms.TextBox
    Friend WithEvents txtDababase As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtYonghu As System.Windows.Forms.TextBox
    Friend WithEvents txtPS As System.Windows.Forms.TextBox
    Friend WithEvents lblMSG As System.Windows.Forms.Label
    Friend WithEvents txtTuPian As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtBackupPath As System.Windows.Forms.TextBox
    Friend WithEvents PicPath As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents BackupPath As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents btnPic As System.Windows.Forms.Button
    Friend WithEvents btnBackup As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents nudBackup As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnSaveas As System.Windows.Forms.Button
    Friend WithEvents btnRead As System.Windows.Forms.Button
End Class
