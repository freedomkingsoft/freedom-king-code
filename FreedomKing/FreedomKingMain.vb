﻿'Imports System.Diagnostics
Imports System.Windows.Forms

Public Class FreedomKingMain

    '指示我们是否正在以编程的方式更改树视图的选定节点
    Private Sub AutoBackup()
        '三信系统不兼容，暂时关闭自动备份功能
        Dim autobc As New AutoBackup
        autobc.Backup()
    End Sub


    Public dlgLogin As FreedomKing.Login

    Private Sub FreedomKingMain_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        End
    End Sub
    Private Sub FreedomKingMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '暂时隐藏所有控件
        Me.ToolStripContainer.Visible = False

        Me.Icon = FKG.myselfG.FKIcon
        'Me.tlpDi.GetType.GetProperty("DoubleBuffered", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).SetValue(Me.tlpDi, True, Nothing)


        '通过读取配置文件动态更改各要素
        Dim sInfo As String
        Dim sContect As String
        Try
            sContect = IO.File.ReadAllText(My.Application.Info.DirectoryPath & "\config.txt")

            sInfo = Split(sContect, ",").GetValue(2)
        Catch ex As Exception
            sContect = ""
            sInfo = Me.Text
        End Try

        Me.Text = sInfo

        'Me.Text = Me.Text & "_" & FKG.myselfG.sBanBen & "_" & FKG.myselfG.sClient
        Me.Text = Me.Text & "         "
        If FKG.myselfG.YunUserInfo.Item("VIP") Then
            Me.Text = Me.Text & "付费用户     商户ID:" & FKG.myselfG.YunUserInfo.Item("userLoginID").ToString & "    到期日: " & FKG.myselfG.YunUserInfo.Item("VIPMaturitydate").ToString
        Else
            Me.Text = Me.Text & "免费用户     商户ID:" & FKG.myselfG.YunUserInfo.Item("userLoginID").ToString & "    到期日: " & FKG.myselfG.YunUserInfo.Item("freeEndDate").ToString
        End If


        If FKG.myselfG.YongHu = "" Then
            End
        Else
            '登录成功, 可以继续操作

            '确定是否隐藏功能树
            Dim xml As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKConfig.xml", True, "config")
            If xml.Read("HideTree", "name", "config") = "True" Then
                HideTree(True)
            Else
                HideTree(False)
            End If

            '设置 UI
            LoadTree()
            '根据权限设置菜单()
            setQX()

            '设置状态栏
            Me.tslZHT.Text = "帐套:" & dlgLogin.lblQiyeming.Text
            Me.tslZHT.Width = Me.Width * 0.28
            Me.tslYH.Text = "用户:" & dlgLogin.txtYongHu.Text
            Me.tslYH.Width = Me.Width * 0.28
            Me.tslZHQ.Text = "登录日期:" & dlgLogin.dtpLogin.Text
            Me.tslZHQ.Width = Me.Width * 0.28
            'Me.ToolStripStatusLabel.Width = Me.Width * 0.8
        End If

        Me.ToolStripContainer.Visible = True

        '启用新线程，处理自动备份，验证是否注册等
        Dim objTh As New Threading.Thread(AddressOf AutoBackup)
        objTh.Start()
    End Sub

    '隐藏功能树和报表数
    Private Sub HideTree(Optional ByVal bHide As Boolean = False)
        If bHide Then
            Me.SplitContainer.SplitterDistance = 0
            Me.SplitContainer.SplitterWidth = 5
        Else
            Me.SplitContainer.SplitterDistance = Me.Size.Width * 0.25
            Me.SplitContainer.SplitterWidth = 1
        End If
    End Sub

    Private Sub LoadTree()
        ' TODO: 添加代码以向树视图添加项
        Me.Fkgns1.Nodes.Clear()
        Me.Fkgns1.TvShows()
        '根据用户权限设置功能树,没有权限使用的功能将不显示

        Me.FKBBS.Nodes.Clear()
        Me.FKBBS.TvShows()

    End Sub

    Private Sub Fkgns1_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles Fkgns1.AfterCollapse, Fkgns1.AfterExpand
        Me.Fkgns1.Refresh()
    End Sub

    'Dim isOpenGN As Boolean = False

    Public Sub Fkgns1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fkgns1.DoubleClick

        Try
            If Me.Fkgns1.SelectedNode.Tag = False Or FKG.myselfG.isOpendedGN Then
                Exit Sub
            Else
                Dim dlg As New FKGN.FKGN("NMVsAQx4YC6PlShS/NfrGHLDkJDaJtzKGb0aTgYy/zbzz5Sifdxal4QzCbUEnP3f8aV9MuhRtpnfdKUkGcOBBfszFvvt60k8ANun5pmRLCI29hkPkxPZXw4WWh34UQflKEbfrqAoiWMMrVAg4yzmQBxK0Ap3fFkpBRDaH58dbwxgOVtJr715tNPD5KHchsnH0ackUkvK116or2BaNHnZs8gVTiDmMU/naY6awQ+akTimuK9nrzDmkW0diHBCvbiAhelrLZ3idIBh/6VM+5GW59Mw4pInsAsdxXIe7M8kutjCc4Djefanz8BICTsdPgQKFD/pR7dMR22iN96bssvYUT5VT68+4ZqQoHvVvmuhrJ8I9d74+i9i9IVPGMX+x0fY4jJrkboDzAZEIqhbLsf4xPfRdvgsKibUrgvaXKKrAKsTZMt2Rjm8e2+FVnB0XLawFF+hCvpE34TDm1WlHLaQ3SSuMuMRDL2C8qCA0LeTQDDer8weG147fIU1f/bZFoKy/GlYEzX8TUylCZOjMncEgndKkfLQniySYmeeYrqz0abkUpmuFqI5L80LHJev5148Z+VK/0HazhdYtbzbCZNHG2MeWOk5G3xQSiuwa9ymjSnS+0gPI1Py8eFg9yGsoYWxpJgsFzaXs9tS2ABXsUxpA5Ndb1SdYVdwFzOcAuQVmHQ=")
                If dlg.DialogResult = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
                dlg.myGongneng = Me.Fkgns1.SelectedNode.Name
                dlg.myGongNengName = Me.Fkgns1.SelectedNode.Text
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                Dim ds As DataSet
                ds = mdb.Reader("select PZSCFS,DJZhangShu from GongNengShu where id=" & Me.Fkgns1.SelectedNode.Name)
                dlg.myPZSCFS = ds.Tables(0).Rows(0).Item(0)
                dlg.myPZZhangShu = ds.Tables(0).Rows(0).Item(1)

                '             Me.Hide()
                dlg.sZBMC = Me.tslZHT.Text
                FKG.myselfG.isOpendedGN = True
                dlg.Show(Me)
                'Me.isOpenGN = False
                'If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
                '    '                Me.Show()
                'Else
                '    重新登录_Click(sender, e)
                'End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            Me.Show()
            Exit Sub
        End Try

    End Sub

    Private Sub FKBBS_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles FKBBS.AfterCollapse, FKBBS.AfterExpand
        Me.FKBBS.Refresh()
    End Sub

    Public Sub FKBBS_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles FKBBS.DoubleClick
        Try
            If Me.FKBBS.SelectedNode.Tag = False Then
                Exit Sub
            Else
                Dim dlg As New FKBCX.FKBCX("J2EoXO+ZYR9QjQYfAmoNILZXefTnbO5V8LhbuWNb9rS/6P/toD+LqCCuhjOiKVivfhLo6nukVXyrkxhdYNq34i1bDPs1nWRhSnFlF+BORq7kvIsARYS5LO2k7NxEAxt0wRGIJzR4B8Pam3N4lgysEHENp6cr+eyxWS+Jd0BolwybO03ISp9Xh45REfLNkUL8p2dmJiiQByAR0QqzzS5+FpvE9Z44cBtlpy1D6otPg81z0DJp9Iv3qyLqONPiHPbbbcRhvLHE68PvERozycfBbNnLgdJk0XyLcAXbKhARLVPYPG3kigJPqvggXIW9HPHP8/Frrx45U+6BsWrgpUorlxYEpIZtMHJLEDn7yIxH6iCFKY5bX6/aT7z2dw/vk4udofuttP8uBJaUtaqfVDWfjMS5Q+dK+G2eKMd8Jw8L6R3D96JNRm+ARikFOdSWwP0at2s4z47IFfXungx1gNAaZ1Ug8pz/mSgCaK0t9kv6OOBPOPBXKRh60V5+johCkdkmP0ocDtL7xYIFrZhFBS8Ft9J7YB96IlGFl8Ad/vrWwVgC/z74RyFsp3eGnhAV64hwXuL+bsW/VxxFuf/ihefOnQkRsIfmhYA4NybpSJX8fuypQ4TljVJtRP0Tq32353uetVw7OAqNZOoGyYr92F74D7+5xePNllpunYwVwc+AARI=")
                If dlg.DialogResult = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
                dlg.sCXName = Me.FKBBS.SelectedNode.Text

                dlg.Show(Me)
                'If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then

                'End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            'Me.Show()
            Exit Sub
        End Try
    End Sub

    Private Function isQX(ByVal sBtnName As String) As Boolean
        Dim i As Integer
        Dim n As Integer
        For i = 0 To Me.msZDY.Items.Count - 1
            For n = 0 To CType(Me.msZDY.Items(i), ToolStripMenuItem).DropDownItems.Count - 1 '更正
                Try

                    If IsNothing(Me.GetCDFromName(sBtnName, CType(Me.msZDY.Items(i), ToolStripMenuItem).DropDownItems(n))) Then
                        Continue For
                    Else
                        Return True
                    End If
                Catch ex As Exception
                    Continue For
                End Try
            Next '更正

            Try

                '以下为处理二级菜单
                If IsNothing(Me.GetCDFromName(sBtnName, CType(Me.msZDY.Items(i), ToolStripMenuItem))) Then
                    Continue For
                Else
                    Return True
                End If
            Catch ex As Exception
                Continue For
            End Try
        Next
        Return False
    End Function

    Private sTag As String = ""   '通过一个私有变量来传递tab附加信息

    Private Sub objmyBtn_click(ByVal sender As Object, ByVal e As System.EventArgs)
        CType(sender, Label).Height = CType(sender, Label).Height * 0.75
        CType(sender, Label).Width = CType(sender, Label).Width * 0.75

        Dim sBtnName As String = CType(sender, Label).Name
        Dim dv As DataView = CType(CType(sender, Label).Tag, DataSet).Tables(0).DefaultView
        '  Dim sTag As String
        'sTag = dv.Item(0).Item("tab").ToString
        sTag = CType(sender, Label).AccessibleDescription

        dv.RowFilter = "BTNNAME='" & sBtnName & "'"  '为按照命令名称筛选，对于同一个命令不适用
        Try
            Select Case dv.Item(0).Item("BtnType")
                Case "菜单命令"
                    '确定是否拥有相应的权限
                    If isQX(sBtnName) Then
                        '首先根据按钮从数据库中获取附加信息，然后传递给相关命令
                        CallByName(Me, sBtnName & "_click", CallType.Get, New Object() {sender, e})
                    End If

                Case "功能树"
                    If FKG.myselfG.isOpendedGN Then
                        Exit Sub
                    End If

                    '先确定是否有权限使用功能或报表
                    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

                    If mdb.bExsit("select * from yonghuquanxian where yonghuid=" & FKG.myselfG.YongHuID & " and yonghuquanxian= " & dv.Item(0).Item("GNBBID")) Then
                    Else
                        MsgBox("您无权使用该功能!", MsgBoxStyle.Information, "提示")
                        Exit Sub
                    End If

                    Dim dlg As New FKGN.FKGN("hviB98hpdKaF+7VkPb2wyAeL697KcUuuiqkRxW2RCvZ3LqImlGU0bjFWcCFHOOhb4tn0ucYYLCHv/sc2xpc5WyyaUQ4adjl6MCGDOVqNBStJMlKy/knRHyUASFR8zjgDNr6FrtXrIrqX+ncN8QNQfc+1re6YIYmxKfg6SLJ5NjYkjQfcL1l0mGf8a5uxgzMrzico0b0Dmrg3vdPN9el6o4mLYvZc3Ne84RyY+4YBghP5BXZM50ZtJH/OYuA7b1/DrT6wxDj+2s6PaEbM0UXHFxKamKfpRE4hn2MkE61mqLndlacv1MQyxflOKQLVvWyApGt0yu4x2qQO6cOUqutyZR9rdyMqkqrQWVLs+3iF+LgRfUoG5EuMBgC6h4XctMjE2sGTy8Kfy3axcrE3FQKhxXPuBLTL03JXbAYKYJ+vg6A2B6AIbSrOHB1mDzFeblojIEs85umk7Oj/6xfMMhzSNvydlvf53Sv3VLUj8eOoRFwS1BdPo0BB+BteS9RQyj8HOba40BkZr7T+6z1Y9oeYkAfCrWiFg8tSsUyqypIpvDy8Fu5N1V2Yh8RLYvBLWaifNhGAXkJJNWUolSUjsZBEaNU1CVuyYZ0H8GqUXK6HmiqvpZc3Uc8EydpOZG7ILfu8dMLLhpp9Lw4KIzcEtZ4MaAmVHWg6tIEXNMyKsioTQuo=")
                    If dlg.DialogResult = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If

                    dlg.myGongneng = dv.Item(0).Item("GNBBID")
                    dlg.myGongNengName = dv.Item(0).Item("GNBBName")
                    'Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                    Dim ds As DataSet
                    ds = mdb.Reader("select PZSCFS,DJZhangShu from GongNengShu where id=" & dv.Item(0).Item("GNBBid"))
                    dlg.myPZSCFS = ds.Tables(0).Rows(0).Item(0)
                    dlg.myPZZhangShu = ds.Tables(0).Rows(0).Item(1)

                    '                   Me.Hide()
                    dlg.sZBMC = Me.tslZHT.Text
                    FKG.myselfG.isOpendedGN = True

                    dlg.Show(Me)
                    'If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    '    '                      Me.Show()
                    'Else
                    '    重新登录_Click(sender, e)
                    'End If

                Case "报表树"
                    '先确定是否有权限使用功能或报表
                    Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

                    If mdb.bExsit("select * from BAOBIAOQUANXIAN,ZDYBB where yonghuid=" & FKG.myselfG.YongHuID & " and ConText= '" & dv.Item(0).Item("GNBBName") & "' and YONGHUQUANXIAN=ID") Then
                    Else
                        MsgBox("您无权使用该报表!", MsgBoxStyle.Information, "提示")
                        Exit Sub
                    End If

                    Dim dlg As New FKBCX.FKBCX("hviB98hpdKaF+7VkPb2wyAeL697KcUuuiqkRxW2RCvZ3LqImlGU0bjFWcCFHOOhb4tn0ucYYLCHv/sc2xpc5WyyaUQ4adjl6MCGDOVqNBStJMlKy/knRHyUASFR8zjgDNr6FrtXrIrqX+ncN8QNQfc+1re6YIYmxKfg6SLJ5NjYkjQfcL1l0mGf8a5uxgzMrzico0b0Dmrg3vdPN9el6o4mLYvZc3Ne84RyY+4YBghP5BXZM50ZtJH/OYuA7b1/DrT6wxDj+2s6PaEbM0UXHFxKamKfpRE4hn2MkE61mqLndlacv1MQyxflOKQLVvWyApGt0yu4x2qQO6cOUqutyZR9rdyMqkqrQWVLs+3iF+LgRfUoG5EuMBgC6h4XctMjE2sGTy8Kfy3axcrE3FQKhxXPuBLTL03JXbAYKYJ+vg6A2B6AIbSrOHB1mDzFeblojIEs85umk7Oj/6xfMMhzSNvydlvf53Sv3VLUj8eOoRFwS1BdPo0BB+BteS9RQyj8HOba40BkZr7T+6z1Y9oeYkAfCrWiFg8tSsUyqypIpvDy8Fu5N1V2Yh8RLYvBLWaifNhGAXkJJNWUolSUjsZBEaNU1CVuyYZ0H8GqUXK6HmiqvpZc3Uc8EydpOZG7ILfu8dMLLhpp9Lw4KIzcEtZ4MaAmVHWg6tIEXNMyKsioTQuo=")

                    If dlg.DialogResult = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                    dlg.sCXName = dv.Item(0).Item("GNBBName")

                    dlg.Show(Me)
                    'If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then

                    'End If

                Case Else

            End Select
        Catch ex As Exception

        End Try

        CType(sender, Label).Height = CType(sender, Label).Height / 0.75
        CType(sender, Label).Width = CType(sender, Label).Width / 0.75
    End Sub

    Private Sub objmyBtn_Move(ByVal sender As Object, ByVal e As MouseEventArgs)
        CType(sender, Label).BorderStyle = BorderStyle.Fixed3D
    End Sub

    Private Sub objmyBtn_Leave(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).BorderStyle = BorderStyle.None
    End Sub

    Private Sub FillMainControl(ByVal dsbtn As DataSet)
        Dim i As Integer
        Dim objBtn(dsbtn.Tables(0).Rows.Count) As Label
        For i = 0 To dsbtn.Tables(0).Rows.Count - 1
            objBtn(i) = New Label
            objBtn(i).Visible = False
            objBtn(i).GetType.GetProperty("DoubleBuffered", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).SetValue(objBtn(i), True, Nothing)

            'Me.tlpDi.GetType.GetProperty("DoubleBuffered", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).SetValue(Me.tlpDi, True, Nothing)


            objBtn(i).Cursor = Cursors.Hand

            'AddHandler objBtn(i).MouseMove, AddressOf objmyBtn_Move
            'AddHandler objBtn(i).MouseLeave, AddressOf objmyBtn_Leave
            '如果启用此事件，当鼠标经过按钮时，会导致整个地图闪烁


            AddHandler objBtn(i).Click, AddressOf objmyBtn_click
            '          
            objBtn(i).AutoSize = False

            objBtn(i).Name = dsbtn.Tables(0).Rows(i).Item("BTNNAME")
            objBtn(i).Text = dsbtn.Tables(0).Rows(i).Item("BTNTEXT")
            objBtn(i).Tag = dsbtn

            Try
                objBtn(i).AccessibleDescription = dsbtn.Tables(0).Rows(i).Item("Tab")
                objBtn(i).Height = Me.tlpMainControl.Height / Me.tlpMainControl.RowCount * dsbtn.Tables(0).Rows(i).Item("btnSize") / 100
                objBtn(i).Width = Me.tlpMainControl.Width / Me.tlpMainControl.ColumnCount * dsbtn.Tables(0).Rows(i).Item("btnSize") / 100
            Catch ex As Exception

            End Try
            Try
                Dim s As String = sFilePath & dsbtn.Tables(0).Rows(i).Item("btnDT")
                objBtn(i).BackgroundImage = Drawing.Image.FromFile(sFilePath & dsbtn.Tables(0).Rows(i).Item("btnDT"))
                objBtn(i).BackgroundImageLayout = ImageLayout.Zoom
            Catch ex As Exception
                MsgBox("你设置的图片路径 " & ex.Message.ToString & "不存在")
            End Try
            Try
                objBtn(i).Font = StringToFont(dsbtn.Tables(0).Rows(i).Item("btnZT"))
            Catch ex As Exception

            End Try

            Select Case dsbtn.Tables(0).Rows(i).Item("btnAlign") '文字位置
                Case "左上"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.TopLeft
                Case "中上"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.TopCenter
                Case "右上"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.TopRight
                Case "左中"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.MiddleLeft
                Case "中中"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.MiddleCenter
                Case "右中"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.MiddleRight
                Case "左下"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.BottomLeft
                Case "中下"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.BottomCenter
                Case "右下"
                    objBtn(i).TextAlign = Drawing.ContentAlignment.BottomRight
                Case Else
            End Select

            Me.tlpMainControl.Controls.Add(objBtn(i), dsbtn.Tables(0).Rows(i).Item("ICOLUMN"), dsbtn.Tables(0).Rows(i).Item("IROW"))
            'Me.tlpMainControl.Controls.Add(objBtn(i), dsbtn.Tables(0).Rows(i).Item("IROW"), dsbtn.Tables(0).Rows(i).Item("ICOLUMN"))
        Next
        For i = 0 To dsbtn.Tables(0).Rows.Count - 1
            objBtn(i).Visible = True
        Next
    End Sub

    Private Function FontToString(ByVal fFont As Drawing.Font) As String
        Dim sFont As String = ""
        sFont = fFont.Name & ","
        sFont = sFont & fFont.SizeInPoints.ToString & ","
        sFont = sFont & fFont.Style & ","
        sFont = sFont & fFont.Strikeout.ToString & ","
        sFont = sFont & fFont.Underline.ToString

        Return sFont
    End Function

    Private Function StringToFont(ByVal sFont As String)
        Dim fFont As Drawing.Font
        Dim sF() As String
        sF = Split(sFont, ",")
        If sF(0) = "" Then
            fFont = New Drawing.Font("宋体", 9, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point)
        Else
            fFont = New Drawing.Font(sF(0), sF(1), sF(2), Drawing.GraphicsUnit.Point)
        End If

        Return fFont
    End Function

    Private Sub DTZTSet(ByVal DSDTZT As DataSet)
        If IsNothing(DSDTZT) Then
            Exit Sub
        End If

        Me.tlpMainControl.Visible = False
        Try
            With DSDTZT.Tables(0).Rows(0)
                Try
                    Me.tlpMainControl.RowCount = .Item("McHANG")
                    Me.tlpMainControl.ColumnCount = .Item("MCLIE")
                    '是否可以在此处设置宽度和高度
                    '宽度和高度可以保存到“MCZT”字段中

                    Dim sGaoKuan As String
                    sGaoKuan = .Item("MCZT")
                    Dim sGao, sKuan As String
                    sGao = Split(sGaoKuan, ",").GetValue(0)
                    sKuan = Split(sGaoKuan, ",").GetValue(1)

                    Dim i As Integer
                    For i = 0 To Me.tlpMainControl.RowCount - 1
                        Me.tlpMainControl.RowStyles(i).Height = Me.tlpMainControl.Height * CInt(Split(sGao).GetValue(i)) / 100
                    Next

                    For i = 0 To Me.tlpMainControl.ColumnCount - 1
                        Me.tlpMainControl.ColumnStyles(i).Width = Me.tlpMainControl.Width * CInt(Split(sKuan).GetValue(i)) / 100
                    Next

                Catch ex As Exception

                End Try

                Try
                    Me.Fkgns1.BackGroundImage = Drawing.Image.FromFile(sFilePath & .Item("GNDT"))
                Catch ex As Exception

                End Try
                Try
                    Me.Fkgns1.Font = StringToFont(.Item("GNZT"))
                Catch ex As Exception

                End Try
                Try
                    Me.FKBBS.BackGroundImage = Drawing.Image.FromFile(sFilePath & .Item("BBDT"))
                Catch ex As Exception

                End Try
                Try
                    Me.FKBBS.Font = StringToFont(.Item("BBZT"))
                Catch ex As Exception

                End Try
                Try
                    Me.msZDY.Font = StringToFont(.Item("MMZT"))
                Catch ex As Exception

                End Try
                Try
                    Me.StatusStrip.Font = StringToFont(.Item("MMZT"))
                Catch ex As Exception

                End Try

                Try
                    Me.tlpDi.BackgroundImage = Drawing.Image.FromFile(sFilePath & .Item("MCDT"))
                Catch ex As Exception

                End Try
            End With
        Catch ex As Exception

        End Try
        Me.tlpMainControl.Visible = True
    End Sub

    Dim sFilePath As String = ""

    Private Sub setQX()

        Dim mdbCD As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        'sFilePath = FKG.myselfG.getSetupStr("PicturePath").TrimEnd("\") & ""

        sFilePath = Application.StartupPath & "\FKData"  '将图片存放位置固定在应用程序的启动目录下。放的data文件夹里面Pictrue文件夹里面

        Dim iJiBie As Integer
        iJiBie = mdbCD.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0)

        Dim dsCD As DataSet
        dsCD = mdbCD.Reader("select * from CD where JiBie <= " & iJiBie & " order by sy")
        Dim i As Integer
        Dim dvMain As DataView
        dvMain = dsCD.Tables(0).DefaultView
        dvMain.RowFilter = "isMain"
        For i = 0 To dvMain.Count - 1
            Dim objZhuCD As New ToolStripMenuItem
            objZhuCD.Name = dvMain.Item(i).Item(1)
            objZhuCD.Text = dvMain.Item(i).Item(2)
            Me.msZDY.Items.Add(objZhuCD)
            'Me.msZDY.Items.Add(dvMain.Item(i).Item(2).ToString)
        Next

        Dim j As Integer

        Dim dsCDQX As DataSet

        dsCDQX = mdbCD.Reader("select distinct CDQuanXian from yonghucdqx where yonghuid=" & FKG.myselfG.YongHuID)
        If IsNothing(mdbCD.myExceptionInformation) = False AndAlso mdbCD.myExceptionInformation.ToString <> "" Then
            If mdbCD.myExceptionInformation.Contains("数据库引擎找不到输入表或查询") Then
                '自动增加权限表,并增加权限
                Dim mdbQX As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
                mdbQX.Write("Create TABLE YongHuCDQX (YongHuID int,CDQuanXian varchar(255))")
                mdbQX.Write("insert into YongHuCDQX(yonghuid,CDQuanXian) select yonghuid,cdname from yonghu,cd")

                dsCDQX = mdbCD.Reader("select distinct CDQuanXian from yonghucdqx where yonghuid=" & FKG.myselfG.YongHuID)
            Else
                dsCDQX = Nothing
                End
            End If
        End If


        Dim lCDQX As New List(Of String)
        For i = 0 To dsCDQX.Tables(0).Rows.Count - 1
            lCDQX.Add(dsCDQX.Tables(0).Rows(i).Item(0).ToString)
        Next

        For i = 0 To Me.msZDY.Items.Count - 1
            dvMain.RowFilter = "isMain='false' and Main='" & Me.msZDY.Items(i).Name & "'"
            'Create TABLE YongHuCDQX (YongHuID int,CDQuanXian varchar(255)) 增加了一个新表,保存菜单权限

            For j = 0 To dvMain.Count - 1
                Dim objZDYCD As ToolStripMenuItem
                Dim m As Integer
                For m = 0 To Me.MenuStrip1.Items.Count - 1
                    '在此处判断是否有权限
                    If lCDQX.Contains(dvMain.Item(j).Item(1)) Then

                    Else
                        '无此菜单权限
                        Continue For
                    End If

                    objZDYCD = GetCDFromName(dvMain.Item(j).Item(1), Me.MenuStrip1.Items(m))
                    If Not IsNothing(objZDYCD) Then
                        'objZDYCD.Name = dvMain.Item(j).Item(1)
                        objZDYCD.Text = dvMain.Item(j).Item(2)
                        CType(Me.msZDY.Items(i), ToolStripMenuItem).DropDownItems.Add(objZDYCD)
                        Continue For
                    Else
                        If dvMain.Item(j).Item("CDName") = "分割线" Then
                            Dim msSpe As New Windows.Forms.ToolStripSeparator
                            CType(Me.msZDY.Items(i), ToolStripMenuItem).DropDownItems.Add(msSpe)
                            Exit For
                        End If
                    End If
                Next
            Next
        Next

        Try
            Dim dsDTZT As DataSet
            dsDTZT = mdbCD.Reader("select * from DTZT where UserID=" & FKG.myselfG.YongHuID)
            DTZTSet(dsDTZT)

            Dim dsMCBtn As DataSet
            dsMCBtn = mdbCD.Reader("select * from MCBtn where UserID=" & FKG.myselfG.YongHuID)
            FillMainControl(dsMCBtn)
        Catch ex As Exception

        End Try


    End Sub

    Private Function GetCDFromName(ByVal sName As String, ByRef objMS As ToolStripMenuItem) As ToolStripMenuItem
        'Dim objCD As New ToolStripMenuItem
        Dim objCD As New Object
        For Each objCD In objMS.DropDownItems
            Try
                If objCD.GetType.Name <> "ToolStripMenuItem" Then
                    Continue For
                End If
                If objCD.DropDownItems.Count > 0 Then
                    GetCDFromName(sName, objCD)
                    'objCD = GetCDFromName(sName, objCD)
                    'If IsNothing(objCD) Then
                    '    Continue For
                    'Else
                    '    Return objCD
                    'End If
                End If
                If objCD.Name = sName Then
                    
                    Return objCD
                End If

            Catch ex As Exception

            End Try
        Next
        Return Nothing
    End Function

    Private Sub Fkgns1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Fkgns1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Fkgns1_DoubleClick(sender, Nothing)
        End If
    End Sub


    Private Sub FreedomKingMain_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        '设置状态栏
        ' Me.tslZHT.Text = "帐套:" & dlg.lblQiyeming.Text
        Me.tslZHT.Width = Me.Width * 0.28
        '  Me.tslYH.Text = "用户:" & dlg.txtYongHu.Text
        Me.tslYH.Width = Me.Width * 0.28
        '  Me.tslZHQ.Text = "登录日期:" & dlg.dtpLogin.Text
        Me.tslZHQ.Width = Me.Width * 0.28
        'Me.ToolStripStatusLabel.Width = Me.Width * 0.8

    End Sub

    Public Sub 重新登录_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 重新登录.Click

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("delete from YHZhuangTai where YongHuName='" & FKG.myselfG.YongHu & "' and LoginTime='" & FKG.myselfG.dLogin & "'")

        'Dim backup As New AutoBackup
        'backup.BackupFile()

        Dim lujing As String
        lujing = Application.ExecutablePath

        Shell(lujing, AppWinStyle.NormalFocus)
        End
    End Sub

    Public Sub 用户管理_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 用户管理.Click
        Dim dlg As New FKZT.YongHu
        'If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
        dlg.ShowDialog()
        LoadTree()
        'End If
    End Sub

    Public Sub 科目代码设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 科目代码设置.Click
        '启动科目代码设置时，附加筛选科目条件，条件来自于按钮所限定信息，方便对不同类型的科目进行在桌面快捷设置。20200411升级 
        '首先获取按钮名称
        '然后通过数据库获取附加信息，并将附加信息复制给 scase
        Dim sCase As String = ""
        If Me.sTag = "" Then
        Else
            Dim sTiaoJian As String()
            sTiaoJian = Split(sTag, ",")
            Dim i As Integer
            Dim sTJ As String = ""
            For i = 0 To sTiaoJian.Length - 1
                If sTJ = "" Then
                    sTJ = sTJ & " KMDM like '" & sTiaoJian(i) & "%'"
                Else
                    sTJ = sTJ & "  or KMDM like '" & sTiaoJian(i) & "%' "
                End If
            Next
            sCase = " and (" & sTJ & ")"
        End If
        Dim dlg As New FKKM.KMDM("Tt3o4gvTKumPQvmp4f8E3vFN/x7u9UtYUpbaJkYqAFv6zuv1yONwRhFJ5dbki7GOVwzcYjWmwoGJ9Akr++GADhf7mAYRruAAsmY6/noND86umiv9xEFkeFN1agyfm/tCXGzUVJGuLs3XEjc5yoPmSUUGtsMEzanlH6bt5bblx4+HtFGZKhUNuceCTWFXmefZhNRtYfoAtsiPgBOqExuCEDZ7iQyvBhIpvgHptkyQ530Y5dX3UbP2EBHKRF0M/CuFN9xIx8DA5oc++YT+S7k9rlpz3ZFVIzLmkII2VWCZrPDvNXC2bXveWrrNqKJ+5DrDnbOr8vtInycs2YRoPFOHr3eGHzkH5WVKZ13ZId46kxNMFJ5jA7WmQTQgDGnfm8l7d5pk619SZVbG0WpsjvTlnGnoQ5nMv/bQNSLX4yMj8Cb52RSCTUdfxin02qHA/f2RmexGLji9rtH0GU9VS+DE9Xq/+zv6SSEozKNJ5eEGCV6XIkBFAW6ykWse3m5cea1IRN8E9gKuDsFBKXSeS+DrClrvYVdFjzfHiUR2W/s8QZ6vQ5XphmfqK1J5qhkALXU2XMFtJatgaQi8NwQiJH00/JaS49X70txXMFDq64S9jmu0hLoTNjymK4o8MFp3ZD2ySdxnHwe5i/e6kBvd7cXeLBF3OGU+abQMLqdV8ij8Occ=", scase)

        Me.sTag = ""
        If dlg.DialogResult = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        dlg.ShowDialog()
    End Sub

    Public Sub 企业资料设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 企业资料设置.Click
        Dim dlg As New FKZT.ZTInfo
        dlg.ShowDialog()
    End Sub

    Public Sub 退出系统_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 退出系统.Click
        If MsgBox("您确定要退出" & My.Application.Info.Title, MsgBoxStyle.YesNo, "提示") = MsgBoxResult.Yes Then

            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            mdb.Write("delete from YHZhuangTai where YongHuName='" & FKG.myselfG.YongHu & "' and LoginTime='" & FKG.myselfG.dLogin & "'")

            Dim backup As New AutoBackup
            backup.BackupFile()
            'backup.DeleteFile() '手工执行删除太多文件操作

            End
        Else

        End If
    End Sub

    Private Sub FreedomKingMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MsgBox("您确定要退出系统吗", MsgBoxStyle.YesNo, "提示") = MsgBoxResult.Yes Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            mdb.Write("delete from YHZhuangTai where YongHuName='" & FKG.myselfG.YongHu & "' and LoginTime='" & FKG.myselfG.dLogin & "'")

            Dim backup As New AutoBackup
            backup.BackupFile()

            'End
        Else
            e.Cancel = True
        End If
    End Sub

    Public Sub 功能树管理_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 功能树管理.Click
        Dim dlg As New FKGNS.GNSGL
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            LoadTree()
        End If
    End Sub

    Public Sub 新建功能_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 新建功能.Click
        Select Case FKG.myselfG.sBanBen
            Case "试用版"
            Case "正式版"
            Case "开发版"
                Dim dlg As New Freedom.FreedomCore("BM2t00g5eyqFQN/Ndf43tnncn4X1dUL1HYYdKAiwt74M0qCtZfcUZBZEib75X6SHMSu+fOcedbqgOHKTFObD9rGJHn8R8VSfUd2jYMX51xkYzKAqiD3LpfSjSPSWZcrt5mxRgR7JqPUCxA5V8lrbzfKXo/M26Ko2jmLZiNrgj1/RLoDHXGT3kZwbfyNkOfyKlzMoytRE3H2ygsxd/ofo5B+Rfh4+tOsKGi9j/d01iRG5aMs0F92SWW33L4RDi+OhCDeuIsmwwD+6FEL3LT8GTqgzFn9ejEDMQ+eG+pSwS5oOTRqq87VVMls3+niVTeweee9YmP21esNmS5ZEK5/CZlTaEKF9eSBWVxXceoS8WY1C+bNlqVFAgUSuLIyVLV2V7cobYMm+pq0shvUFDGO7mv0k2GPO65IL8ucYcwN/Z7Dm5ZDg4DjQPRIPA+9oYbSO2e8yZ6VWiVOoruDck5w2wxKuVq6G/TvwNzk6CmmG9npa3rU1VCKYUsO0EEYk63e/7J0R7wEaHsoqJedTU53oxkPfQOlgaq7Ud2a/5jP5or1jAClBeaD/UzZNAOB7Jcy0Lqsjipsi8tM1ZskBJmTLHjZCBD4MQCUkEoIy+T1JbR0KjxzKjMWguy4/uy6Wdt1hc4sRwhUG0Zo2LTtVCibHNMgHpLNsIjQkPt2LfS8EjU8=")
                If dlg.DialogResult = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
                dlg.myGongneng = -1
                dlg.Show(Me)
                LoadTree()
        End Select

    End Sub

    Public Sub 修改功能_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 修改功能.Click
        Select Case FKG.myselfG.sBanBen
            Case "试用版"

            Case "正式版"
            Case "开发版"
                Try
                    If Me.Fkgns1.SelectedNode.Tag = False Then
                        MsgBox("请首先选择一个功能,你目前选中的是一个节点", MsgBoxStyle.Information, "提示")
                        Exit Sub
                    End If
                    If IsNothing(Me.Fkgns1.SelectedNode) Then
                        MsgBox("请首先选择一个功能", MsgBoxStyle.Information, "提示")
                        Exit Sub
                    End If
                Catch ex As Exception
                    MsgBox("请首先选择一个功能", MsgBoxStyle.Information, "提示")
                    Exit Sub
                End Try

                Dim dlg As New Freedom.FreedomCore("J88h96ryk+LzFCuHkxt+Z9uAkxRcwFzsm4WUGffFKY1DaJi4czosOOKeolSdFaRzBRhrbZbYhet/I6oiiwFauWkP/d2BX4LvrTBeOPsMdXapvlmJd9asU1hqfmAJ2IFYTgHtw9NpIPv48ei5loxJ+cw8yiTm8PEfqVqFTSEs1g+qJygVrXwTCYYrX8f55ci3PhTgIx7SGYt2RG5VUqz5IP/BL10+74kTx8UpfmXRyrC6dOcIda6aH7NdUrlCKAN/qA8ww+SdcFFrNrER4iVraou76yI1WJcfqUd6ySFzUEentTcuKykJAFrtiRDGFGX9uLLsyrAPjJjVracajbT8kt5DPD+2MUa1CFENgNG6jALskAlPHCEcuKx9ebtZ1+BwiTQ59cq0R/DKrLPaBsLk9kFLOIe0wFju8FoG7T3ATbZGEAKlnUgt3HX5/PaoY+rYdyYnRFQaRLZYjxnhNnO6aXtAfXpAJaT743rmsEnH0bJJygr4152l2BQJexwKsdPK4UTMi5CnQupNjBFWvtcLdOd9wAu4yMdQNyMfAC8WovMrnQKxt41VZ9AKAyRGYzOt5s8JNH00eRk718EJleThc83E9JFMs/te9Lpc0Wb/YxuZD71lqsvqneEW66yerVVdlW8HP03kpwfYCkMfOOnd5+C1WcwxG1+6el+YzlqIgGk=")

                If dlg.DialogResult = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
                Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
                dlg.myGongneng = mdb.Reader("select ID from GongNengShu where Context='" & Me.Fkgns1.SelectedNode.Text & "'").Tables(0).Rows(0).Item(0)

                dlg.Show(Me)
                LoadTree()
        End Select

    End Sub

    Public Sub 导入功能_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导入功能.Click

        Dim dlg As New FKImportInFun.SelectGNS
        dlg.ShowDialog()

    End Sub

    Public Sub 功能设计帮助_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 功能设计帮助.Click

    End Sub

    Public Sub 总帐余额表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 总帐余额表.Click
        Dim dlg As New FKKMYEB.KMYE
        dlg.Show(Me)
    End Sub


    Public Sub 明细帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 明细帐.Click
        Dim dlg As New FKKMYEB.KMMXZ
        dlg.Show(Me)
    End Sub

    Public Sub 日记帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 日记帐.Click
        Dim dlg As New FKKMYEB.KMRJZ
        dlg.Show(Me)
    End Sub


    Public Sub 特殊用户设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 特殊用户设置.Click
        Dim dlg As New FKZT.ZGInfo
        dlg.ShowDialog()
    End Sub

    Public Sub 仓库定义_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 仓库定义.Click
        Dim dlg As New FKZT.CangKuInfo
        dlg.ShowDialog()
    End Sub

    Public Sub 科目余额装入_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 科目余额装入.Click
        Dim dlg As New FKWLYEB.iniKMYEB
        dlg.ShowDialog()
    End Sub

    Public Sub 库存余额装入_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存余额装入.Click
        Dim dlg As New FKKCYEB.iniKCYEB
        dlg.Show(Me)
    End Sub


    Public Sub 初始工余额装入资_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 初始工余额装入资.Click
        Dim dlg As New FKGZYEB.iniGZYEB
        dlg.ShowDialog()
    End Sub


    Public Sub 初始委外余额装入_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 初始委外余额装入.Click
        Dim dlg As New FKWWYEB.iniWWYEB
        dlg.ShowDialog()
    End Sub

    Public Sub 库存余额表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存余额表.Click
        Dim dlg As New FKKCYEB.KCYE
        dlg.Show(Me)
    End Sub


    Public Sub 库存明细帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存明细帐.Click
        Dim dlg As New FKKCYEB.KCMXZ
        dlg.Show(Me)
    End Sub

    Public Sub 库存日记帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存日记帐.Click
        Dim dlg As New FKKCYEB.KCRJZ
        dlg.Show(Me)
    End Sub


    Public Sub 工资余额表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 工资余额表.Click
        Dim dlg As New FKGZYEB.GZYE
        dlg.Show(Me)
    End Sub

    Public Sub 工资明细帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 工资明细帐.Click
        Dim dlg As New FKGZYEB.GZMXZ
        dlg.Show(Me)
    End Sub

    Public Sub 工资日记帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 工资日记帐.Click
        Dim dlg As New FKGZYEB.GZRJZ
        dlg.Show(Me)
    End Sub

    Public Sub 功能树_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 功能树.Click
        Dim dlg As New FKGNS.GNSGL
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            LoadTree()
        End If
    End Sub

    Public Sub 导入_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导入.Click
        Dim dlg As New FKImportInFun.SelectGNS
        dlg.ShowDialog()

    End Sub

    Public Sub 库存结账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存结账.Click
        Select Case FKG.myselfG.sBanBen
            Case "试用版"

            Case Else
                Select Case FKG.myselfG.sClient
                    Case "服务器端"
                    Case Else
                        MsgBox("请在服务器端进行此操作！", MsgBoxStyle.Information, "提示")
                        Exit Sub
                End Select
                '该功能用来实现如下功能
                '１.锁定库存表，防止用户修改或新增
                '２.生成库存余额表，将本期发生额写入　ＫＣＹＥ
                '３　只要主管会计才可使用此功能
                Dim dlg As New FKKCJZ.FKKCJZ
                dlg.ShowDialog()
        End Select

    End Sub

    Public Sub 工资结账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 工资结账.Click
        Select Case FKG.myselfG.sClient
            Case "服务器端"
            Case Else
                Exit Sub
        End Select
        '该功能用来实现如下功能
        '１.锁定工资表，防止用户修改或新增
        '２.生成工资余额表，将本期发生额写入GZYE
        '３　只要主管会计才可使用此功能
        Dim dlg As New FKGZJZ.FKGZJZ
        dlg.ShowDialog()
    End Sub

    Public Sub 单证审核_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 单证审核.Click
        '该功能用来实现如下功能
        '１.根据凭证号锁定凭证，防止用户修改或删除
        '２　只要主管会计才可使用此功能
        '３　审核过的凭证，在功能模块中可以加盖“已审”签章
        '４　打印逐单凭证
        Dim dlg As New FKSHPZ.FKSHPZ(False, FKG.myselfG.NianFen)
        dlg.ShowDialog()
    End Sub

    Public Sub 期间记账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 期间记账.Click
        Select Case FKG.myselfG.sBanBen
            Case " 试用版"

            Case Else
                Select Case FKG.myselfG.sClient
                    Case "服务器端"
                    Case Else
                        MsgBox("请在服务器端进行此操作！", MsgBoxStyle.Information, "提示")
                        Exit Sub
                End Select
                '１　将本期发生额写入ＫＭＹＥ表
                Dim dlg As New FKPZJZ.FKKCJZ
                dlg.ShowDialog()
        End Select

    End Sub

    Public Sub 帐库对账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 帐库对账.Click
        Select Case FKG.myselfG.sClient
            Case "服务器端"
            Case Else
                Exit Sub
        End Select

        '将库存中科目的数量与总账中的数量相比对
        '可以截止到二级科目或三级科目，有用户指定
    End Sub

    Public Sub 生成转账凭证_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 生成转账凭证.Click
        Select Case FKG.myselfG.sClient
            Case "服务器端"
            Case Else
                Exit Sub
        End Select
        '生成自动转账凭证
        '重新审核并记账
    End Sub

    Public Sub 期末结帐_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 期末结帐.Click
        Select Case FKG.myselfG.sBanBen
            Case " 试用版"

            Case Else
                Select Case FKG.myselfG.sClient
                    Case "服务器端"
                    Case Else
                        MsgBox("请在服务器端进行此操作！", MsgBoxStyle.Information, "提示")
                        Exit Sub
                End Select
                '关闭本期账目，不能进行任何修改操作
                '如果为１２月结账，则自动生成下一年度余额，及科目，及有关内容

                Dim dlg As New FKQMJZ.FKKCJZ
                dlg.ShowDialog()
        End Select
    End Sub


    Public Sub 帐套打印_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 帐套打印.Click
        Dim dlg As New FKZTPrint.FKZTPrint
        dlg.Show(Me)
    End Sub


    Public Sub 往来余额表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 往来余额表.Click
        Dim dlg As New FKWLYEB.WLYE
        dlg.Show(Me)
    End Sub

    Public Sub 往来明细账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 往来明细账.Click
        Dim dlg As New FKWLYEB.WLMXZ
        dlg.Show(Me)
    End Sub

    Public Sub 往来日记账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 往来日记账.Click
        Dim dlg As New FKWLYEB.WLRJZ
        dlg.Show(Me)
    End Sub

    Public Sub 委外余额表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 委外余额表.Click
        Dim dlg As New FKWWYEB.WWYE
        dlg.Show(Me)
    End Sub

    Public Sub 委外明细账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 委外明细账.Click
        Dim dlg As New FKWWYEB.WWMXZ
        dlg.Show(Me)
    End Sub

    Public Sub 委外日记账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 委外日记账.Click
        Dim dlg As New FKWWYEB.WWRJZ
        dlg.Show(Me)
    End Sub

    Public Sub 库存报表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 库存报表.Click
        Dim dlg As New FKGNCX.FKGNCX
        dlg.Show(Me)
    End Sub

    Public Sub 综合查询_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 综合查询.Click
        Dim DLG As New FKZHCX.FKZHCX
        DLG.Show(Me)
    End Sub

    Public Sub 报表树管理_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 报表树管理.Click
        Dim dlg As New FKBBSGL.BBSGL
        dlg.ShowDialog()

        LoadTree()
    End Sub

    Public Sub 外存型号余额_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 外存型号余额.Click
        Dim DLG As New FKWWYEB.WWXHYE
        DLG.Show(Me)
    End Sub

    Public Sub 四工序机工工资_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 四工序机工工资.Click
        Dim xml As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKConfig.xml", True, "config")

        Dim dlg As New DZFKGNJGGZ.DZFKGNJGGZ
        dlg.myGongneng = xml.Read("机工工资专用", "name", "config")
        dlg.myGongNengName = xml.Read("机工工资专用功能名称", "name", "config")
        dlg.iGXNumber = 4
        dlg.myPZSCFS = 0
        dlg.Show(Me)
    End Sub

    Private Sub 三工序机工工资_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 三工序机工工资.Click
        Dim xml As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKConfig.xml", True, "config")

        Dim dlg As New DZFKGNJGGZ.DZFKGNJGGZ
        dlg.myGongneng = xml.Read("机工工资专用三工序", "name", "config")
        dlg.myGongNengName = xml.Read("机工工资专用功能名称三工序", "name", "config")
        dlg.iGXNumber = 3
        dlg.myPZSCFS = 0
        dlg.Show(Me)
    End Sub

    Public Sub 打造主界面_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 打造主界面.Click
        Dim dlg As New FKZDY.FKZDY
        dlg.Show(Me)
    End Sub

    Public Sub 凭证类型设置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 凭证类型设置.Click

    End Sub

    Public Sub 常用摘要定义_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 常用摘要定义.Click

    End Sub

    Public Sub 导出功能_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出功能.Click
        Dim dlg As New FKImport.FKImportFun
        If Me.Fkgns1.SelectedNode.Name = -1 Then
        Else
            dlg.ImportOut(Me.Fkgns1.SelectedNode.Name)
        End If
    End Sub

    Public Sub 往来报表_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 往来报表.Click

    End Sub

    Private Sub 其他功能_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 其他功能.Click
        Dim dlg As New Other.OtherMain
        dlg.Show(Me)
    End Sub

    Private Sub 系统管理_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 系统管理.Click
        Dim dlg As New FKAdmin.FKAdmin
        dlg.Show(Me)
    End Sub


    Private Sub SQL语句工具_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SQL语句工具.Click
        Dim dlg As New FKExec.FKExec
        dlg.Show(Me)
    End Sub

    Private Sub FKBBS_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles FKBBS.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.FKBBS_DoubleClick(sender, e)
        End If
    End Sub

    Public Sub 往来客户对账_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 往来客户对账.Click
        Dim dlg As New WLKHDZH.FKWLKHDZH
        dlg.Show()
    End Sub

    Private Sub SplitContainer_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles SplitContainer.MouseMove
        If e.Location.X <= 3 Then
            HideTree(False)
        End If
    End Sub

    Private Sub tlpDi_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlpDi.MouseEnter
        '确定是否隐藏功能树
        Dim xml As New FKSetting.XMLRWer(My.Application.Info.DirectoryPath & "\FKConfig.xml", True, "config")
        If xml.Read("HideTree", "name", "config") = "True" Then
            HideTree(True)
        Else
            ' HideTree(False)
        End If
    End Sub

    Private Sub 备份数据_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 备份数据.Click
        Dim dlgsave As New FolderBrowserDialog
        'dlgsave.FileName = "自由王备份" & Now.ToString.Replace(" ", "").Replace(":", "-")
        'dlgsave.DefaultExt = ".MDB"
        If dlgsave.ShowDialog = Windows.Forms.DialogResult.OK Then
            'IO.File.Copy(My.Settings.sClientFile, dlgsave.FileName)
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
            mdb.ExecStoredProcedure(FKG.myselfG.asasR, "backupDatabase", "@sFileName," & dlgsave.SelectedPath & ";@bShouGongBeiFen,1")
        End If
    End Sub

    Private Sub 恢复数据_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 恢复数据.Click
        Dim dlg As New FKRestore.Restore
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.重新登录_Click(sender, e)
        End If
    End Sub

    Private Sub 期末审核_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 期末审核.Click
        Select Case FKG.myselfG.sBanBen
            Case "试用版"

            Case Else
                Dim dlg As New FKSHPZ.FKQMSH
                dlg.Show()
        End Select

    End Sub

    Private Sub 关于_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 关于.Click
        Dim dlg As New AboutBox1
        dlg.ShowDialog()
    End Sub

    Private Sub 简介_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 简介.Click
        Dim dlg As New Dialog1
        dlg.ShowDialog()
    End Sub

    Private Sub 插入凭证_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 插入凭证.Click
        Dim dlg As New FKPZCL.FKPZInsert
        dlg.ShowDialog()
    End Sub

    Private Sub 凭证整理_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 凭证整理.Click
        Dim dlg As New FKPZCL.FKPZZL
        dlg.ShowDialog()
    End Sub

    Private Sub 列表式凭证查询_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 列表式凭证查询.Click
        Dim dlg As New FKPZCL.FKPZLBSCX
        dlg.ShowDialog()
    End Sub


    Private Sub 系统基础配置_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 系统基础配置.Click
        Dim mdbr As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdbr.Reader("select YongHuJiBie from YongHu where YongHuID=" & FKG.myselfG.YongHuID).Tables(0).Rows(0).Item(0) < 5 Then
            Me.系统基础配置.Enabled = False
        Else
            Dim dlg As New DBConfig("")
            dlg.ShowDialog()
        End If
   
    End Sub

    Private Sub LoginState()
        Dim mdbr As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        If mdbr.bExsit("select * from YHZhuangTai where YongHuName='" & FKG.myselfG.YongHu & "' and YHMAC='" & My.Computer.Name & "' and LoginTime='" & FKG.myselfG.dLogin & "'") Then

        Else
            MsgBox("该用户已经在其他地方登录，你已被迫注销，系统将自动关闭，请重新登录！", MsgBoxStyle.Information, "提示")
            End
        End If
    End Sub

    Private Sub myTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles myTimer.Tick
        LoginState()
    End Sub

    Private Sub 按编号查找ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 按编号查找ToolStripMenuItem.Click
        Dim dlg As New FKCKDZ.FKCKDZ("")
        dlg.Show()

    End Sub
End Class
