﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' 有关程序集的常规信息通过下列属性集
' 控制。更改这些属性值可修改
' 与程序集关联的信息。

' 查看程序集属性的值

<Assembly: AssemblyTitle("自由王进销存财务一体化管理系统")> 
<Assembly: AssemblyDescription("自由王进销存财务一体化管理系统 可以为中小规模企业提供根据客户需要而配置的一款集库存、工资和财务与一体的整体解决方案。")> 
<Assembly: AssemblyCompany("赵建涛")> 
<Assembly: AssemblyProduct("FreedomKing")> 
<Assembly: AssemblyCopyright("版权(C)赵建涛")> 
<Assembly: AssemblyTrademark("自由王软件")> 

<Assembly: ComVisible(False)>

'如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
<Assembly: Guid("77899e53-3cbf-4264-8ee5-b62ee12119c0")> 

' 程序集的版本信息由下面四个值组成:
'
'      主版本
'      次版本
'      内部版本号
'      修订号
'
' 可以指定所有这些值，也可以使用“内部版本号”和“修订号”的默认值，
' 方法是按如下所示使用“*”:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("8.0.240.315")> 
<Assembly: AssemblyFileVersion("0.24.03.15")> 
