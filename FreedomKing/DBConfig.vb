﻿Public Class DBConfig

    Private Sub DBConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.lblMSG.Text = sMsg

        Dim sconn(8, 1) As String

        If FKG.myselfG.asasR.IndexOf("=") < 0 Then
            Me.txtDataSource.Text = ""
            Me.txtDababase.Text = ""
            Me.txtYonghu.Text = ""
            Me.txtPS.Text = ""

        Else
            Dim i, n As Integer
            i = 0
            n = 0
            For Each Strtemp As String In FKG.myselfG.asasR.Split(";")
                sconn(i, 0) = Strtemp.Split("=").GetValue(0)
                sconn(i, 1) = Strtemp.Split("=").GetValue(1)

                Select Case sconn(i, 0)
                    Case "Data Source"
                        Me.txtDataSource.Text = sconn(i, 1)
                    Case "Initial Catalog"
                        Me.txtDababase.Text = sconn(i, 1)
                    Case "User ID"
                        Me.txtYonghu.Text = sconn(i, 1)
                    Case "Password"
                        Me.txtPS.Text = sconn(i, 1)
                End Select

            Next
        End If


        'Me.txtTuPian.Text = FKG.myselfG.getSetupStr("PicturePath")

        Me.txtTuPian.Text = Application.StartupPath & "\FKData"
        Me.txtBackupPath.Text = FKG.myselfG.getSetupStr("BackupPath")
        Me.nudBackup.Value = FKG.myselfG.getSetupStr("BackupTianShu")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim sConn As String
        sConn = "Provider=SQLOLEDB.1;User ID=" & Me.txtYonghu.Text & ";Password=" & Me.txtPS.Text & ";Persist Security Info=True;Initial Catalog=" & Me.txtDababase.Text & ";Data Source=" & Me.txtDataSource.Text

        FKG.myselfG.setConStr(sConn)

        FKG.myselfG.setSetupStr(Me.txtTuPian.Text, "PicturePath")
        FKG.myselfG.setSetupStr(Me.txtBackupPath.Text, "BackupPath")
        FKG.myselfG.setSetupStr(Me.nudBackup.Value, "BackupTianShu")

    End Sub

    Private sMsg As String

    Public Sub New(ByVal myMSG As String)

        ' 此调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        sMsg = myMSG

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub btnPic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPic.Click
        If Me.PicPath.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtTuPian.Text = Me.PicPath.SelectedPath
        End If
    End Sub

    Private Sub btnBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackup.Click
        If Me.BackupPath.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtBackupPath.Text = Me.BackupPath.SelectedPath
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FKG.myselfG.setConStr("")

        Me.txtYonghu.ReadOnly = False
        Me.txtPS.ReadOnly = False


    End Sub

    Private Sub btnSaveas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveas.Click
        Dim sContect As String
        sContect = Me.txtDataSource.Text & ","
        sContect = sContect & Me.txtDababase.Text & ","
        sContect = sContect & Me.txtYonghu.Text & ","
        sContect = sContect & Me.txtPS.Text & ","
        sContect = sContect & Me.txtBackupPath.Text & ","
        sContect = sContect & Me.txtTuPian.Text

        IO.File.WriteAllText(Application.StartupPath & "\config.txt", sContect)

        MsgBox("配置已保存")
    End Sub

    Private Sub btnRead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRead.Click
        Dim sContect As String
        Try
            sContect = IO.File.ReadAllText(Application.StartupPath & "\config.txt")

            Me.txtDataSource.Text = sContect.Split(",")(0).ToString
            Me.txtDababase.Text = sContect.Split(",")(1).ToString
            Me.txtYonghu.Text = sContect.Split(",")(2).ToString
            Me.txtPS.Text = sContect.Split(",")(3).ToString
            Me.txtBackupPath.Text = sContect.Split(",")(4).ToString
            Me.txtTuPian.Text = sContect.Split(",")(5).ToString
        Catch ex As Exception

        End Try

    End Sub
End Class