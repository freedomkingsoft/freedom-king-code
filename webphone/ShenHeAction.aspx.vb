﻿
Partial Class ShenHeAction
    Inherits System.Web.UI.Page

    Private dsLoginInfo As Data.DataSet

    Public sActShenHe As String = "none"
    Public sActShouZhi As String = "none"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            Select Case Request.Params("act")
                Case "shenqing"
                    sActShenHe = "block"
                Case "shouzhi"
                    sActShouZhi = "block"
            End Select
            getClientList(Request.Params("clientPhone").ToString)

            If Page.IsPostBack = False Then

            End If

            'jisuanJiaGe()
            End If
    End Sub


    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Public templeteTable As String
    ' Public itemtemplate As Data.DataSet
    Public sGenZong As String

    Private Sub getClientList(ByVal dlPhone As String)
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)

        Dim dsClientList As Data.DataSet

        Try
            Dim sSQL As String = ""
            Select Case Request.Params("act")
                Case "shenqing"
                    'sSQL = "select * from shenqing where loginphone='" & dlPhone & "'"
                    sSQL = "select * from shenqing where ISNULL(jindu,1) <> '通过' and  loginphone='" & dlPhone & "'"
                Case "shouzhi"
                    'sSQL = "select * from dlShouZhiShenQing where ID='" & dlPhone & "'"
                    sSQL = "select * from dlShouZhiShenQing where ISNULL(shenhe,1) <>'审核通过' and  ID='" & dlPhone & "'"
                Case Else
                    sSQL = "select * from shenqing where loginphone='" & dlPhone & "'"
            End Select

            '  Dim sSQL As String = "select  * from dlList left join dlJiBie on dlList.dlJiBie=dlJiBie.ID where loginphone='" & dlPhone & "'"
            dsClientList = mdb.Reader(sSQL)


        Catch ex As Exception
            Exit Sub
        End Try

        If dsClientList.Tables(0).Rows.Count > 0 Then
            ' sGenZong = dsClientList.Tables(0).Rows(0).Item("by1").ToString

            Dim i As Integer
            Dim n As Integer
            Dim sBaoBiao As New StringBuilder
            '编写报表内容

            For i = 0 To dsClientList.Tables(0).Columns.Count - 2
                sBaoBiao.Append("<div data-node=""cardList"" class=""card-list"">")
                sBaoBiao.Append(" <div class=""card card4 line-around"" id=""boxId_1589690143613_9"">")
                If i Mod 2 = 0 Then
                    sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:azure"">")
                Else
                    sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:cornsilk"">")
                End If
                sBaoBiao.Append("<div class=""box-col txt-cut""> ")

                sBaoBiao.Append("<span class=""span-table"" style=""width:")
                sBaoBiao.Append("40")
                sBaoBiao.Append("%"">")
                sBaoBiao.Append(dsClientList.Tables(0).Columns(i).ColumnName)
                sBaoBiao.Append("</span> ")

                For n = 0 To dsClientList.Tables(0).Rows.Count - 1
                    sBaoBiao.Append("<span class=""span-table"" style=""width:")
                    sBaoBiao.Append("55")
                    sBaoBiao.Append("%"">")
                    sBaoBiao.Append(dsClientList.Tables(0).Rows(n).Item(i).ToString)
                    sBaoBiao.Append("</span> ")

                Next
                sBaoBiao.Append("</div> ")
                sBaoBiao.Append("</div> ")
                sBaoBiao.Append(" </div></div>")
            Next
            templeteTable = sBaoBiao.ToString
        End If

    End Sub


    Protected Sub btnGenZong_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenZong.Click
        Dim sGenZong As String
        sGenZong = Request.Form("genzonglog").ToString
        If sGenZong = "" Then
        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
            sGenZong = Date.Today.ToShortDateString & ":" & sGenZong & "<P>"
            Dim ssql As String = "update shenqing set by1=isnull(by1,'')+'" & sGenZong & "' where loginPhone='" & Request.Params("clientPhone").ToString & "'"
            mdb.Write(ssql)
            ' Response.Redirect("clientDetail.aspx?clientPhone=" & Request.Params("clientPhone").ToString)
        End If
    End Sub

    Public FeiYong As Decimal = 0

    Protected Sub btnShenHe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShenHe.Click
        shenhedaili()

    End Sub

    Private Sub shenhedaili()
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim sss As String = "update shenqing set jindu='" & Request.Form("shenheJieGuo") & "',fankui='" & Request.Form("FanKui") & "',fankuidate='" & Today.ToShortDateString & "' where loginPhone='" & Request.Params("clientPhone").ToString & "'"
        mdb.Write(sss)
        If Request.Form("shenheJieGuo").ToString = "通过" Then
            Dim sInsert As String
            sInsert = "insert into dlList (dlPhone,dlJiBie,beiwang,xinYongEDu ) values ('" & Request.Params("clientPhone").ToString & "',1,'',0)"
            mdb.Write(sInsert)
        End If

        'Response.Write("<script>alert('代理商申请已处理。');window.location.href='mycenter.aspx'</script>")
        Response.Write("<script>alert('代理商申请已处理。');</script>")
    End Sub

    Protected Sub btnShouZhi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShouZhi.Click
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim sss As String = "update dlShouZhiShenQing set shenhe='" & Request.Form("shouzhiJieGuo").ToString & "',fankui='" & Request.Form("shouzhiFanKui").ToString & "' where ID='" & Request.Params("clientPhone").ToString & "'"
        mdb.Write(sss)
        If Request.Form("shouzhiJieGuo").ToString = "审核通过" Then
            Dim sInsert As String
            sInsert = "insert into dlShouZhi (shijian,dlPhone,jiefang,daifang,kehuphone,xjdlphone, shuoming)  select shijian,dlPhone,jiefang,0,'','','代理商充值' from dlShouZhiShenQing where ID='" & Request.Params("clientPhone").ToString & "'"
            mdb.Write(sInsert)
        End If

        Response.Write("<script>alert('收支申请已处理。');</script>")
    End Sub
End Class
