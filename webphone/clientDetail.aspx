﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clientDetail.aspx.vb" Inherits="clientDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="http://u1.sinaimg.cn/upload/h5/img/apple-touch-icon.png">
    <title>客户详情</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />
</head>
<body style="padding-top: 50px; overflow-x: hidden;">
    <div class="fixed_topw">
        <div class="self_info">
            <img src="images/FKLogo.png" alt="logo" /></div>
        <ul>
            <li>
                <h1>
                    <a href="default.aspx">代理中心</a></h1>
            </li>
        </ul>
    </div>
    <div id="box" class="container stage-setting">
        <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
    <div class="title-group">
      <h1 class="title txt-cut">客户详情</h1>
    </div>
  </header>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
            <h3 class="title mct-b txt-xs" data-node="gTitle">
            </h3>
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="boxId_1589690143613_3">
                    <a href="#" class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="mct-a ">个人资料</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span></a>
                </div>
            </div>
        </div>
        <div class="card11 card-combine" data-node="group" id="Div14">
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="Div15">
                    <div class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut" style="text-align: right">
                            <span class="mct-a "><a href="shenqing.aspx">申请成为代理商 </a></span><span class="mct-a ">
                                <a href="dlCenter.aspx">代理商管理入口</a></span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span>
                    </div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="card11 card-combine" data-node="group" id="Div4">
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div7">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span1" class="mct-a " style="display: -moz-inline-box; display: inline-block;">
                                    年数</span> <span class="mct-a" style="text-align: right">
                                        <asp:DropDownList ID="ddNian"  runat="server" AutoPostBack="true" Style="width: 2rem">
                                            <asp:ListItem Value="1" Text="1">1</asp:ListItem>
                                            <asp:ListItem Value="2" Text="2">2</asp:ListItem>
                                            <asp:ListItem Value="3" Text="3">3</asp:ListItem>
                                            <asp:ListItem Value="4" Text="4">4</asp:ListItem>
                                            <asp:ListItem Value="5" Text="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                        用户数
                                        <asp:DropDownList ID="ddYongHu"  runat="server" AutoPostBack="true" Style="width: 2rem">
                                            <asp:ListItem Value="2" Text="2">2</asp:ListItem>
                                            <asp:ListItem Value="1" Text="1">1</asp:ListItem>
                                            <asp:ListItem Value="3" Text="3">3</asp:ListItem>
                                            <asp:ListItem Value="4" Text="4">4</asp:ListItem>
                                            <asp:ListItem Value="5" Text="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                        <%-- <select id="Select1" name="NianShu" style="width: 3rem">
                                            <option value="1" selected>1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        用户数<select id="Select2" name="YongHuShu" style="width: 3rem">
                                            <option value="1" selected>1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>--%>
                                    </span><span>费用<%=FeiYong %></span>
                                <input id="Hidden1" type="hidden" name="txtFeiYong" value="<%=FeiYong %>" />
                            </div>
                            <span data-node="arrow" class="plus plus-s"><span data-node="arrow" class="plus plus-s">
                                <i class="icon-font icon-font-arrow-right txt-s">
                                    <asp:Button runat="server" ID="btnJiaoFei" Text="缴费" /></i></span> </span>
                        </div>
                        <div class="infotishi" style="display: <%= tishiMM%>">
                            <span>
                                <%=tishiMM%>
                            </span>
                            <asp:Button runat="server" ID="btnQueRenJF" Text="确认缴费" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card11 card-combine" data-node="group" id="Div2">
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div3">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span16" class="mct-a " style="width: 25%; display: -moz-inline-box; display: inline-block;">
                                    跟踪记录： </span><span class="mct-a" style="text-align: right">
                                        <input name="genzonglog" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><span data-node="arrow" class="plus plus-s">
                                <i class="icon-font icon-font-arrow-right txt-s">
                                    <asp:Button runat="server" ID="btnGenZong" Text="保存记录" /></i></span> </span>
                        </div>
                    </div>
                </div>
            </div>
            
             <div class="card11 card-combine" data-node="group" id="Div8"  style="display:<%=ShowAdmin %>">
        </div>
        <div class="card card6" id="Div9"  style="display:<%=ShowAdmin %>">
                <asp:Button ID="BtnResetPWS" runat="server" Text="重置密码" />
        </div>
        </form>
        <div id="infotishi" style="display: <%= sGenZong%>">
            <span>
                <%=sGenZong%>
            </span>
        </div>
        <div class="card11 card-combine" data-node="group" id="Div1">
           
            <%=templeteTable%>
           
        </div>
         
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
        </div>
        <div class="card card6" id="boxId_1589690143613_13">
            <a href="#top" data-act-type="hover" class=" btn-red">返回顶部 </a>
        </div>
        <div id="boxId_1589690143613_14">
        </div>
        <div class="card card6" id="Div5">
            <a href="#top" data-act-type="hover" class=" btn-red"></a>
        </div>
        <div id="Div6">
        </div>
    </div>
    <div class="footer">
            <ul>
                <li class="active"><a href="main.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\home.png" /></i></p>
                    <h4>
                        首页</h4>
                </a></li>
                <li><a href="download.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\down.png" /></i></p>
                    <h4>
                        下载</h4>
                </a></li>
                <li><a href="help.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\help.png" /></i></p>
                    <h4>
                        帮助</h4>
                </a></li>
                <li><a href="mycenter.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\mine.png" /></i></p>
                    <h4>
                        我的</h4>
                </a></li>
            </ul>
        </div>
</body>
</html>
