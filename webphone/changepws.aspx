﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="changepws.aspx.vb" Inherits="changepws" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>修改密码</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />
</head>
<body style="padding-top: 50px; overflow-x: hidden;">
    <div class="fixed_topw">
        <div class="self_info">
            <img src="images/FKLogo.png" alt="logo" /></div>
        <ul>
            <li>
                <h1>
                    <a href="default.aspx">
                        <%=Session("QiYeName")%>
                    </a>
                </h1>
            </li>
        </ul>
    </div>
    <div id="box" class="container stage-setting">
        <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
    <div class="title-group">
      <h1 class="title txt-cut">修改密码</h1>
    </div>
  </header>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
            <h3 class="title mct-b txt-xs" data-node="gTitle">
            </h3>
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="boxId_1589690143613_3">
                    <a href="#" class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="mct-a ">修改密码</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span></a>
                </div>
            </div>
        </div>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
        </div>
        <form runat="server" id="shenhe" method="post">
            
            
            <div class="card11 card-combine" data-node="group" id="Div1">
                <h3 class="title mct-c txt-xs" data-node="gTitle">
                    当前您正在使用的是默认密码，请先修改密码再使用。
                </h3>
                <div data-node="cardList" class="card-list">
                    <div class="card card4 line-around" id="Div9">
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span5" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请输入旧密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="oldPWS" id="oldPWS" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span8" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请输入新密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="newPWS1" id="Text3" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span9" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                    请确认新密码： </span><span class="mct-a" style="text-align: right">
                                        <input name="newPWS2" id="Text4" type="password" class="ui-autocomplete-input" />
                                    </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s">>><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                        <div class="layout-box" data-act-type="hover">
                            <div class="box-col txt-cut">
                                <span id="Span10" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                                   <%-- <%=tishiMM %>--%>
                                </span><span class="mct-b" style="text-align: right">
                                    <asp:Button ID="btnUpdatePWS" runat="server" Text="修改密码" Style="right: 12px" />
                                </span>
                            </div>
                            <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            </i></span>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div id="Div2">
            </div>
            <div class="layout-box" data-act-type="hover">
                <div class="box-col txt-cut">
                    <span id="Span17" class="mct-a " style="width: 30%; display: -moz-inline-box; display: inline-block;">
                       
                    </span><span class="mct-b" style="text-align: right">
                        <asp:Button ID="btnExitLogin" runat="server" Text="退出当前登录" />
                    </span>
                </div>
                <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                </i></span>
            </div>
        </form>
        <div class="card card6" id="boxId_1589690143613_13">
            <a href="#top" data-act-type="hover" class=" btn-red">返回顶部 </a>
        </div>
        <div id="boxId_1589690143613_14">
        </div>
        <div class="card card6" id="Div5">
            <a href="#top" data-act-type="hover" class=" btn-red"> <%=tishiMM %></a>
        </div>
        <div id="Div6">
        </div>
    </div>
    
</body>
</html>
