﻿
Partial Class Shenqing
    Inherits System.Web.UI.Page

    Public dsLoginInfo As New Data.DataSet

    Public loginPhone As String

    Public shenqingren As String
    Public xiangxidizhi As String
    Public weixin As String
    Public qqhao As String
    Public zhiye As String
    Public gerenshuoming As String
    Public jindu As String
    Public fankui As String
    Public fankuidate As String

    'Public subUserID As String
    'Public admin As String
    'Public regTime As String

    Public tishiMM As String = ""

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        'scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & System.Configuration.ConfigurationManager.AppSettings("DATASOURCE")
        scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString

        Return scon
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            loginPhone = dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString

            'regPhone = dsLoginInfo.Tables(0).Rows(0).Item("regPhone").ToString
            'regDate = dsLoginInfo.Tables(0).Rows(0).Item("regDate").ToString
            'freeEndDate = dsLoginInfo.Tables(0).Rows(0).Item("freeEndDate").ToString
            'VIP = dsLoginInfo.Tables(0).Rows(0).Item("VIP").ToString
            'VIPMaturitydate = dsLoginInfo.Tables(0).Rows(0).Item("VIPMaturitydate").ToString
            'LoginNumber = dsLoginInfo.Tables(0).Rows(0).Item("LoginNumber").ToString
            'comName = dsLoginInfo.Tables(0).Rows(0).Item("comName").ToString
            'eMail = dsLoginInfo.Tables(0).Rows(0).Item("eMail").ToString
            'LoginName = dsLoginInfo.Tables(0).Rows(0).Item("loginName").ToString
            'subUserID = dsLoginInfo.Tables(0).Rows(0).Item("subUserID").ToString
            'admin = dsLoginInfo.Tables(0).Rows(0).Item("admin").ToString
            'regTime = dsLoginInfo.Tables(0).Rows(0).Item("regTime").ToString
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim dsdl As Data.DataSet
            dsdl = mdb.Reader("select * from shenqing where loginPhone='" & loginPhone & "'")
            '先判断是否在申请表中已存在，如存在则进入修改功能页面
            If dsdl.Tables(0).Rows.Count = 0 Then

            Else
                shenqingren = dsdl.Tables(0).Rows(0).Item("shenqingren").ToString
                xiangxidizhi = dsdl.Tables(0).Rows(0).Item("xiangxidizhi").ToString

                weixin = dsdl.Tables(0).Rows(0).Item("weixin").ToString
                qqhao = dsdl.Tables(0).Rows(0).Item("qqhao").ToString
                zhiye = dsdl.Tables(0).Rows(0).Item("zhiye").ToString
                gerenshuoming = dsdl.Tables(0).Rows(0).Item("gerenshuoming").ToString
                jindu = dsdl.Tables(0).Rows(0).Item("jindu").ToString
                fankui = dsdl.Tables(0).Rows(0).Item("fankui").ToString
                fankuidate = dsdl.Tables(0).Rows(0).Item("fankuidate").ToString

                btnSubmit.Text = "修改申请资料"
            End If
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text = "提交申请" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim sb As New StringBuilder
            sb.Append("insert into shenqing ")
            sb.Append("([loginPhone],[shenqingren],[diqu],[xiangxidizhi],[weixin],[qqhao],[zhiye],[gerenshuoming])")
            sb.Append(" values ")
            sb.Append("('")
            sb.Append(loginPhone)
            sb.Append("','")
            sb.Append(Request.Form("shenqingren"))
            sb.Append("','")
            sb.Append(Request.Form("selectResult"))
            sb.Append("','")
            sb.Append(Request.Form("xiangxidizhi"))
            sb.Append("','")
            sb.Append(Request.Form("weixin"))
            sb.Append("','")
            sb.Append(Request.Form("qqhao"))
            sb.Append("','")
            sb.Append(Request.Form("zhiye"))
            sb.Append("','")
            sb.Append(Request.Form("gerenshuoming"))
            sb.Append("')")

            mdb.Write(sb.ToString)

            tishiMM = "申请成功，请等待反馈"
        Else
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            Dim sb As New StringBuilder
            sb.Append("update shenqing set ")
            sb.Append("[shenqingren]=")
            sb.Append("'")
            sb.Append(Request.Form("shenqingren"))
            sb.Append("',diqu='")
            sb.Append(Request.Form("selectResult"))
            sb.Append("',xiangxidizhi='")
            sb.Append(Request.Form("xiangxidizhi"))
            sb.Append("',weixin='")
            sb.Append(Request.Form("weixin"))
            sb.Append("',qqhao='")
            sb.Append(Request.Form("qqhao"))
            sb.Append("',zhiye='")
            sb.Append(Request.Form("zhiye"))
            sb.Append("',gerenshuoming='")
            sb.Append(Request.Form("gerenshuoming"))
            sb.Append("' ")
            sb.Append(" where [loginPhone]='")
            sb.Append(loginPhone)
            sb.Append("'")

            mdb.Write(sb.ToString)

            tishiMM = "修改成功"
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        
    End Sub


    Protected Sub btnExitLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitLogin.Click
        Session.Abandon()
        Response.Cookies("myLoginName").Expires = Now.AddDays(-1)
        Response.Redirect("default.aspx")
    End Sub
End Class
