﻿
Partial Class Detail
    Inherits System.Web.UI.Page

    Dim dsLoginInfo As Data.DataSet
    Public getminDate As String
    Public getmaxDate As String

    Public shenheren As String
    Public shenhejieguo As String

    Public dlLogoPic As String
    Public dlLoginPic As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sDL As DLInfo.dlInfo

        If IsNothing(Session("DLInfo")) Then
            Dim sDLID As String = Request.Params.Item("dlid")
            sDL = New DLInfo.dlInfo(sDLID)
            'Response.Redirect("Default.aspx")
        Else
            sDL = Session("DLInfo")
        End If

        'dlTitle = sDL.dlTitle
        'dlName = sDL.dlName
        'dlHomePic = sDL.dlHomePic
        dlLogoPic = sDL.dlLogoPic
        dlLoginPic = sDL.dlLoginPic
        'dlWebSite = sDL.dlWebSite

        'dlDownload = sDL.dlDownload
        'dlHelp = sDL.dlHelp
        'dlFeedback = sDL.dlFeedback
        'dlForget = sDL.dlForget
        'dlMyCenter = sDL.dlMyCenter

        'dlFileURL = sDL.dlFileURL
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("Default.aspx")
        Else
            getBaoBiao(Request.Params("bh"))
        End If

    End Sub

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        'scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & System.Configuration.ConfigurationManager.AppSettings("DATASOURCE")
        scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & dsLoginInfo.Tables(0).Rows(0).Item("DATASOURCE").ToString
        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Public ItemTemplateTitle As String
    Public ItemTemplate As String
    Private Sub getBaoBiao(ByVal BaoBiaoID As String)
        Dim dsBaoBiao As New Data.DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)

        '通过编号获取所用功能树ID
        Dim sSQL As String
        Dim dsGongNeng As Data.DataSet
        sSQL = "select * from GongNengShu where ID=(select top 1 A_GNSID from JiBenCaoZuo where BianHao='" & BaoBiaoID.Replace(" ", "") & "')"
        dsGongNeng = mdb.Reader(sSQL)
        If dsGongNeng.Tables(0).Rows.Count = 0 Then
            Response.Write(" <script>history.back(-1) </script>")
            Exit Sub
        End If
        '查询显示那个表
        If dsGongNeng.Tables(0).Rows(0).Item("Kucun") Then
            sSQL = dsGongNeng.Tables(0).Rows(0).Item("KuCunSQL")
            sSQL = "SELECT " & sSQL & " FROM 库存表 where 显示=1 and 编号='" & BaoBiaoID.Replace(" ", "") & "'"
        ElseIf dsGongNeng.Tables(0).Rows(0).Item("GongZi") Then
            sSQL = dsGongNeng.Tables(0).Rows(0).Item("GongZiSQL")
            sSQL = "SELECT " & sSQL & " FROM 工资表 where 显示=1 and 编号='" & BaoBiaoID.Replace(" ", "") & "'"
        ElseIf dsGongNeng.Tables(0).Rows(0).Item("PZ") Then
            sSQL = dsGongNeng.Tables(0).Rows(0).Item("PZSQL")
            sSQL = "SELECT " & sSQL & " FROM 凭证表 where 显示=1 and 编号='" & BaoBiaoID.Replace(" ", "") & "'"

        Else
            'sSQL = "SELECT 异常编号 as 提示"
            Exit Sub
        End If
        '显示查询项

        dsBaoBiao = mdb.Reader(sSQL)

        'Dim irecordPerPage As Integer = 5
        Dim irecordPerPage As Integer = System.Configuration.ConfigurationManager.AppSettings("irecordPerPage")
        Dim iPage As Integer
        If dsBaoBiao.Tables(0).Rows.Count > 0 Then
            Dim i As Integer
            Dim n As Integer
            Dim sBaoBiao As New StringBuilder
            '编写报表内容
            For iPage = 1 To Int(dsBaoBiao.Tables(0).Rows.Count / irecordPerPage * -1) / -1
                For i = 0 To dsBaoBiao.Tables(0).Columns.Count - 1
                    sBaoBiao.Append("<div data-node=""cardList"" class=""card-list"">")
                    sBaoBiao.Append(" <div class=""card card4 line-around"" id=""boxId_1589690143613_9"">")
                    If i Mod 2 = 0 Then
                        sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:azure"">")
                    Else
                        sBaoBiao.Append("<div class=""layout-box"" data-act-type=""hover"" style=""background-color:cornsilk"">")
                    End If
                    sBaoBiao.Append("<div class=""box-col txt-cut""> ")

                    sBaoBiao.Append("<span class=""span-table"" style=""width:")
                    sBaoBiao.Append(myMax(18, Int(95 / (dsBaoBiao.Tables(0).Rows.Count + 1))).ToString)
                    sBaoBiao.Append("%"">")
                    sBaoBiao.Append(dsBaoBiao.Tables(0).Columns(i).ColumnName)
                    sBaoBiao.Append("</span> ")

                    For n = (iPage - 1) * irecordPerPage To myMin(iPage * irecordPerPage - 1, dsBaoBiao.Tables(0).Rows.Count - 1) 'dsBaoBiao.Tables(0).Rows.Count - 1
                        ' If n Mod irecordPerPage = 0 OrElse (n > 0 AndAlso dsBaoBiao.Tables(0).Rows(n).Item(i) <> dsBaoBiao.Tables(0).Rows(n - 1).Item(i)) Then
                        If n Mod irecordPerPage = 0 OrElse (n > 0) Then
                            If dsBaoBiao.Tables(0).Columns(i).ColumnName.Contains("编号") Then
                                sBaoBiao.Append("<a href=""detail.aspx?bh=")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append(""">")

                                sBaoBiao.Append("<span class=""span-table"" style=""width:")
                                sBaoBiao.Append(myMax(15, Int(95 / (dsBaoBiao.Tables(0).Rows.Count + 1))).ToString)
                                sBaoBiao.Append("%"">")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append("</span> ")

                                sBaoBiao.Append("</a>")
                            ElseIf dsBaoBiao.Tables(0).Columns(i).ColumnName.Contains("客户") Then
                                sBaoBiao.Append("<a href=""BaoBiao.aspx?id=73&name=应收明细账&km=")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append(""">")

                                sBaoBiao.Append("<span class=""span-table"" style=""width:")
                                sBaoBiao.Append(myMax(15, Int(95 / (dsBaoBiao.Tables(0).Rows.Count + 1))).ToString)
                                sBaoBiao.Append("%"">")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append("</span> ")

                                sBaoBiao.Append("</a>")
                            ElseIf dsBaoBiao.Tables(0).Columns(i).ColumnName.Contains("供应商") Then
                                sBaoBiao.Append("<a href=""BaoBiao.aspx?id=74&name=应付明细账&km=")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append(""">")

                                sBaoBiao.Append("<span class=""span-table"" style=""width:")
                                sBaoBiao.Append(myMax(15, Int(95 / (dsBaoBiao.Tables(0).Rows.Count + 1))).ToString)
                                sBaoBiao.Append("%"">")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append("</span> ")

                                sBaoBiao.Append("</a>")
                            ElseIf dsBaoBiao.Tables(0).Columns(i).ColumnName.Contains("型号") Then
                                sBaoBiao.Append("<a href=""BaoBiao.aspx?id=75&name=库存商品明细账&xh=")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append(""">")

                                sBaoBiao.Append("<span class=""span-table"" style=""width:")
                                sBaoBiao.Append(myMax(15, Int(95 / (dsBaoBiao.Tables(0).Rows.Count + 1))).ToString)
                                sBaoBiao.Append("%"">")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append("</span> ")

                                sBaoBiao.Append("</a>")
                            Else
                                sBaoBiao.Append("<span class=""span-table"" style=""width:")
                                sBaoBiao.Append(myMax(15, Int(95 / (dsBaoBiao.Tables(0).Rows.Count + 1))).ToString)
                                sBaoBiao.Append("%"">")
                                sBaoBiao.Append(dsBaoBiao.Tables(0).Rows(n).Item(i).ToString)
                                sBaoBiao.Append("</span> ")
                            End If
                        End If
                    Next
                    sBaoBiao.Append("</div> ")
                    sBaoBiao.Append("</div> ")
                    sBaoBiao.Append(" </div></div>")
                Next
                If iPage < Int(dsBaoBiao.Tables(0).Rows.Count / irecordPerPage * -1) / -1 Then
                    sBaoBiao.Append(" <div data-node=""cardList"" class=""card-list""> <div class=""card card4 line-around"" ><div class=""layout-box"" data-act-type=""hover""><div class=""box-col txt-cut""> <span class=""span-table"" style=""width:85%"">下一页</span>  </div> </div>  </div></div>")
                End If

            Next

            ItemTemplate = sBaoBiao.ToString

            sSQL = "select top 1 审核 from jibencaozuo where bianhao='" & BaoBiaoID.Replace(" ", "") & "'"
            shenheren = mdb.Reader(sSQL).Tables(0).Rows(0).Item(0).ToString
        End If
    End Sub

    Private Function myMin(ByVal n1 As Integer, ByVal n2 As Integer) As Integer
        If n1 < n2 Then
            Return n1
        Else
            Return n2
        End If
    End Function

    Private Function myMax(ByVal n1 As Integer, ByVal n2 As Integer) As Integer
        If n1 < n2 Then
            Return n2
        Else
            Return n1
        End If
    End Function

    Protected Sub btnShenHe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShenHe.Click
        dsLoginInfo = Session("LoginInfo")
        shenheren = Request.Form("inShenheren")

        If shenheren = "" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            '开始审核
            Dim sSqljiben As String
            Dim sSqlKucun As String
            Dim sSqlGongzi As String
            Dim sSqlPZ As String
            Dim sSHSign As String
            If dsLoginInfo.Tables(0).Rows(0).Item("LoginSign").ToString <> "" Then
                sSHSign = dsLoginInfo.Tables(0).Rows(0).Item("LoginSign").ToString
            Else
                sSHSign = dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString
            End If
            sSqljiben = "update jibencaozuo set 审核='" & sSHSign & "' where bianhao='" & Request.Params("bh").Replace(" ", "") & "'"
            sSqlKucun = "update kucun set  审核='" & sSHSign & "' where 编号='" & Request.Params("bh").Replace(" ", "") & "'"
            sSqlGongzi = "update gongzi set  审核='" & sSHSign & "' where 编号='" & Request.Params("bh").Replace(" ", "") & "'"
            sSqlPZ = "update pz set  审核='" & sSHSign & "' where 编号='" & Request.Params("bh").Replace(" ", "") & "'"

            If mdb.Write(sSqljiben) AndAlso mdb.Write(sSqlKucun) AndAlso mdb.Write(sSqlGongzi) AndAlso mdb.Write(sSqlPZ) Then
                shenheren = sSHSign
                shenhejieguo = "审核成功"
            End If
        Else
            shenhejieguo = "已审核无需再审"
        End If
    End Sub

    Protected Sub btnQuXiao_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuXiao.Click
        shenheren = Request.Form("inShenheren")
        If shenheren <> "" Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString())
            '开始销审
            Dim sSqljiben As String
            Dim sSqlKucun As String
            Dim sSqlGongzi As String
            Dim sSqlPZ As String
            sSqljiben = "update jibencaozuo set 审核='' where bianhao='" & Request.Params("bh").Replace(" ", "") & "' and (审核='" & dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString & "' or 审核='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginSign").ToString & "')"
            sSqlKucun = "update kucun set  审核='' where 编号='" & Request.Params("bh").Replace(" ", "") & "' and  (审核='" & dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString & "' or 审核='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginSign").ToString & "')"
            sSqlGongzi = "update gongzi set  审核='' where 编号='" & Request.Params("bh").Replace(" ", "") & "' and (审核='" & dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString & "' or 审核='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginSign").ToString & "')"
            sSqlPZ = "update pz set  审核='' where 编号='" & Request.Params("bh").Replace(" ", "") & "' and (审核='" & dsLoginInfo.Tables(0).Rows(0).Item("loginPhone").ToString & "' or 审核='" & dsLoginInfo.Tables(0).Rows(0).Item("LoginSign").ToString & "')"

            If mdb.Write(sSqljiben) AndAlso mdb.Write(sSqlKucun) AndAlso mdb.Write(sSqlGongzi) AndAlso mdb.Write(sSqlPZ) Then
                shenheren = ""
                shenhejieguo = "消审成功"

            End If
        Else
            shenhejieguo = "未审核"
        End If
    End Sub
End Class
