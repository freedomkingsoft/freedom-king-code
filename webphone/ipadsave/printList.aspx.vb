﻿
Partial Class ipadsave_printList
    Inherits System.Web.UI.Page

    Dim dsLoginInfo As Data.DataSet

    Public comeFrom As String = ""

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        dsLoginInfo = Session("LoginInfo")

        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("../Default.aspx?dlid=" & Request.Params.Item("dlid"))
        Else
            If Page.IsPostBack = False Then
                'getBaoBiaoList()
                Dim sbianhao As String
                sbianhao = Request("bianhao")

                If sbianhao.ToString = "" Then
                    FillBianHao()
                Else
                    FillBianHao()
                    FillGV(sbianhao)
                End If
                'FillGV(sbianhao)
            End If
        End If

    End Sub

    Private Sub FillBianHao()
        '显示最近20个编号
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim ds As Data.DataSet
        Try
            ds = mdb.Reader("select top 20 bianhao  from yunsavelist group by bianhao order by max(jianlishijian) desc")
        Catch ex As Exception
            Response.Redirect("tishi.htm")
            Exit Sub
        End Try

        Me.DropDownList1.Items.Add("")
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.DropDownList1.Items.Add(ds.Tables(0).Rows(i).Item(0).ToString)
        Next
       
    End Sub

    Private Sub FillGV(ByVal sBianHao As String)

        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim ds As Data.DataSet

       
        ds = mdb.Reader("select [ID],[riqi] as 日期,[danzhengleixing],[bianhao] as 编号,[cangku],[kehu] as 客户,[xhKMDM],[xhKMMC] as 型号,[zhongliang],convert(FLOAT,[shuliang]) as 数量,convert(FLOAT,[danjia]) as 单价,convert(FLOAT,[jine]) as 金额,[dundanjia],[beizhu] as 备注,[lururen],[jianlishijian] from yunsavelist where bianhao like '%" & sBianHao & "%'")

        Me.GridView1.DataSource = ds.Tables(0).DefaultView
        Me.GridView1.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            Me.setKehu.Text = "客户：" & ds.Tables(0).Rows(0).Item(5).ToString
            Me.setBianHao.Text = "编号：" & ds.Tables(0).Rows(0).Item(3).ToString
        Else
            Me.setKehu.Text = "客户："
            Me.setBianHao.Text = "编号："
        End If
    End Sub

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        'scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString
        scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & dsLoginInfo.Tables(0).Rows(0).Item("DATASOURCE").ToString
        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        Me.comeFrom = "ddl"
        FillGV(Me.DropDownList1.SelectedItem.Value.ToString)

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.TextBox1.Text = "" Then
            Exit Sub
        End If
        Me.comeFrom = "btn"
        FillGV(Me.TextBox1.Text)
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If comeFrom = "ddl" Then
            e.Row.Cells(0).Visible = False
            'e.Row.Cells(1).Visible = False
            e.Row.Cells(2).Visible = False
            e.Row.Cells(3).Visible = False
            e.Row.Cells(4).Visible = False
            e.Row.Cells(5).Visible = False
            e.Row.Cells(6).Visible = False
            'e.Row.Cells(7).Visible = False
            e.Row.Cells(8).Visible = False
            'e.Row.Cells(9).Visible = False
            'e.Row.Cells(10).Visible = False
            'e.Row.Cells(11).Visible = False
            e.Row.Cells(12).Visible = False
            'e.Row.Cells(13).Visible = False
            e.Row.Cells(14).Visible = False
            e.Row.Cells(15).Visible = False
        Else
            e.Row.Cells(0).Visible = False
            'e.Row.Cells(1).Visible = False
            e.Row.Cells(2).Visible = False
            'e.Row.Cells(3).Visible = False
            e.Row.Cells(4).Visible = False
            'e.Row.Cells(5).Visible = False
            e.Row.Cells(6).Visible = False
            'e.Row.Cells(7).Visible = False
            e.Row.Cells(8).Visible = False
            'e.Row.Cells(9).Visible = False
            'e.Row.Cells(10).Visible = False
            'e.Row.Cells(11).Visible = False
            e.Row.Cells(12).Visible = False
            'e.Row.Cells(13).Visible = False
            e.Row.Cells(14).Visible = False
            e.Row.Cells(15).Visible = False
        End If



    End Sub
End Class
