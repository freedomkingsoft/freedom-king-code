﻿
Partial Class ipadsave_ipadsave
    Inherits System.Web.UI.Page

    Public templeteTable As String = ""
    Public XianShishuliang As String = "hidden"
    'Public shopcard As String = ""

    Dim dsLoginInfo As Data.DataSet
    Public dlID As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dlID = Request.Params.Item("dlid")

        'Me.txtZhongLiang.Attributes.Add("onkeypress", "if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 46) event.returnValue = false;")
    End Sub

    Dim lururen As String = ""

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        dsLoginInfo = Session("LoginInfo")
        If IsNothing(Session("LoginInfo")) Then
            Response.Redirect("../Default.aspx?dlid=" & Request.Params.Item("dlid"))
        Else

            If Page.IsPostBack = False Then
                Dim ds As Data.DataSet
                ds = Session("LoginInfo")

                lururen = ds.Tables(0).Rows(0).Item("loginName").ToString

                'getBaoBiaoList()
                fillList()
            End If
        End If
    End Sub

    Private Function getConnectString() As String
        Dim scon As String
        dsLoginInfo = Session("LoginInfo")

        'scon = System.Configuration.ConfigurationManager.ConnectionStrings("debugDatabase").ToString
        scon = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & dsLoginInfo.Tables(0).Rows(0).Item("userLogin").ToString & ";Password=" & dsLoginInfo.Tables(0).Rows(0).Item("userPWS").ToString & ";Initial Catalog=" & dsLoginInfo.Tables(0).Rows(0).Item("userDatabase").ToString & ";Data Source=" & dsLoginInfo.Tables(0).Rows(0).Item("DATASOURCE").ToString
        Return scon
    End Function
    'ConnectString为连接字符串,spName为存储过程名,sPara为参数,参数格式为 参数名,参数值;参数名,参数值

    Private Sub fillList()
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim dsKeHu As Data.DataSet
        dsKeHu = mdb.Reader("select distinct kmmc from KMDM where HSLBWL=1 and kmdm like '1122%' order by kmmc")

        '绑定客户
        Dim i As Integer
        Me.setkehu.Items.Add("")
        For i = 0 To dsKeHu.Tables(0).Rows.Count - 1
            Me.setkehu.Items.Add(dsKeHu.Tables(0).Rows(i).Item("kmmc").ToString)
        Next

        '绑定单证类型，绑定最近一次开单类型
        Try
            dsKeHu = mdb.Reader("select top 1 danzhengleixing from yunsavelist order by jianlishijian desc")
        Catch ex As Exception
            Response.Redirect("tishi.htm")
            Exit Sub
        End Try
        If dsKeHu.Tables(0).Rows.Count > 0 Then
            Me.setDanZhang.Text = dsKeHu.Tables(0).Rows(0).Item(0).ToString
            '绑定仓库
            dsKeHu = mdb.Reader("select top 1 cangku from yunsavelist order by jianlishijian desc")
            Me.setCangKu.Text = dsKeHu.Tables(0).Rows(0).Item(0).ToString

        End If

        '填充日期
        Me.setNian.Items.Add(Today.Year - 1)
        Me.setNian.Items.Add(Today.Year)

        Me.setNian.Text = Today.Year.ToString
        Me.setYue.Text = Today.Month.ToString
        Me.setRi.Text = Today.Day.ToString


     
    End Sub

    Protected Sub btnTongJi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTongJi.Click
        Dim sCangKu As String = Me.setCangKu.Text()
        Dim sDanZhang As String = Me.setDanZhang.Text
        Dim sKeHu As String = Me.setkehu.Text
        Dim sXingHao As String = Me.setxinghao.Text
        'Dim bKuCun As Boolean = Me.cbKuCun.Checked

        Dim dToday As Date = Now.Date.ToString


        Dim dsBaoBiao As New Data.DataSet
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)

        Dim spara As New StringBuilder
        spara.Append("@MinDate")
        spara.Append(",")
        spara.Append(Now.Year.ToString & "-01-01")
        spara.Append(";")

        spara.Append("@MaxDate")
        spara.Append(",")
        spara.Append(Now.Year.ToString & "-" & Now.Month.ToString & "-" & Now.Day.ToString)
        spara.Append(";")

        spara.Append("@BianHao")
        spara.Append(",")
        spara.Append(sCangKu)
        spara.Append(";")


        spara.Append("@KeMu")
        spara.Append(",")
        spara.Append(sKeHu)
        spara.Append(";")

        spara.Append("@Xinghao")
        spara.Append(",")
        spara.Append(Me.setxinghao.Text)

        spara.Append(";")

        spara.Append("@YouKuCun")
        spara.Append(",")
        spara.Append(Me.cbYouKuCun.Checked)

        spara.Append(";")

        spara.Append("@KeHuXiangGuan")
        spara.Append(",")
        spara.Append(Me.cbKeHuXiangGuan.Checked)

        'spara.Append(";")

        'Dim sss As Object = Request.Form
        Dim sp As String = spara.ToString

        Dim sproduce As String
        'Dim sLieWidth As String()
        'Dim dsZdybb As Data.DataSet
        'dsZdybb = mdb.Reader("select MRTiaoJian,liesetup from ZDYYUNBB where ID=7") '- -固定为库存余额表
        'sLieWidth = Split(dsZdybb.Tables(0).Rows(0).Item(1).ToString, ",")

        sproduce = "yun_save_KC"
        Try
            dsBaoBiao = mdb.ReadStoredProcedure(getConnectString, sproduce, sp)
        Catch ex As Exception
            Exit Sub
        End Try

        If dsBaoBiao.Tables.Count = 0 Then
            templeteTable = "没有符合条件的数据"
            Exit Sub
        End If

        '使用gridview控件，将获得的数据绑定到gridview
        If dsBaoBiao.Tables(0).Rows.Count = 0 Then
            templeteTable = "没有符合条件的数据"
        End If

        Me.GridView1.DataSource = dsBaoBiao.Tables(0).DefaultView

        Me.GridView1.DataBind()

        '隐藏科目代码列
        'Response.Write("<script>alert('" & Me.GridView1.Columns.Count & "')</script>")
        'Me.GridView1.Rows.Item(1).Visible = False
        'Me.GridView1.Columns(1).Visible = False

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        e.Row.Cells(1).Visible = False
        '可以使用此办法设定列宽
        'e.Row.Cells(2).Width = 100
    End Sub


    '选择之后弹出窗口，输入数量和价格
    Public sKey As String
    Public sZhongliang As String



    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        sKey = Me.GridView1.SelectedRow.Cells(2).Text
        Me.selectXingHao.Text = sKey
        sZhongliang = Me.GridView1.SelectedRow.Cells(4).Text
        Me.txtZhongLiang.Text = sZhongliang

        Me.XianShishuliang = "visible"
        Dim dDunjiage As Decimal
        Dim Jiage As Decimal
       
        '填充上次该客户该型号保存的吨价格和单价
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim ds As Data.DataSet
        ds = mdb.Reader("select top 1 dundanjia,danjia from YunSaveList where xhKMDM='" & Me.GridView1.SelectedRow.Cells(1).Text & "' and kehu='" & Me.setkehu.Text & "' order by jianlishijian DESC")

        If ds.Tables(0).Rows.Count > 0 Then
            dDunjiage = ds.Tables(0).Rows(0).Item("dundanjia").ToString.TrimEnd("0").TrimEnd(".")
            Me.txtDunJiaGe.Text = dDunjiage
            Jiage = ds.Tables(0).Rows(0).Item("danjia").ToString.TrimEnd("0").TrimEnd(".")
            Me.danjia.Text = Jiage

        Else
            Me.danjia.Text = 0

            ds = mdb.Reader("select top 1 dundanjia from YunSaveList where  kehu='" & Me.setkehu.Text & "' order by jianlishijian DESC")
            If ds.Tables(0).Rows.Count > 0 Then
                dDunjiage = ds.Tables(0).Rows(0).Item("dundanjia").ToString.TrimEnd("0").TrimEnd(".")
                Me.txtDunJiaGe.Text = dDunjiage
                Try
                    Me.danjia.Text = Math.Ceiling(Me.txtZhongLiang.Text * Me.txtDunJiaGe.Text / 1000)
                Catch ex As Exception

                End Try
            Else
                Me.txtDunJiaGe.Text = 0
                Me.shuliang.Text = 0
                Me.jine.Text = 0
                Me.beizhu.Text = ""
            End If
           
        End If

        Me.SetFocus(Me.shuliang)
    End Sub



    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If Me.selectXingHao.Text = "" Then
            Exit Sub
        End If

        Dim tShopcard As New Data.DataTable
     
        If Me.setDanZhang.Text.ToString.Trim = "" Then
            Response.Write("<script>alert('单证类型为必填项，不能为空！')</script>")
        End If

        If IsNothing(Session("shopcard")) Then
            tShopcard.Columns.Add("型号代码")
            tShopcard.Columns.Add("型号")
            tShopcard.Columns.Add("数量")
            tShopcard.Columns.Add("单价")
            tShopcard.Columns.Add("金额")
            tShopcard.Columns.Add("备注")
            tShopcard.Columns.Add("吨价格")
        Else
            tShopcard = Session("shopcard")
        End If
        tShopcard.Rows.Add(Me.GridView1.SelectedRow.Cells(1).Text, Me.GridView1.SelectedRow.Cells(2).Text, Me.shuliang.Text, Me.danjia.Text, Me.jine.Text, Me.beizhu.Text, Me.txtDunJiaGe.Text)

        Session.Add("shopcard", tShopcard)

        Me.GridView2.DataSource = tShopcard.DefaultView
        Me.GridView2.DataBind()

        '隐藏科目代码列

        'Me.GridView2.Columns(0).Visible = False '第一列科目代码不显示

        Me.shuliang.Text = 0
        Me.danjia.Text = 0
        Me.jine.Text = 0
        Me.beizhu.Text = ""

        sKey = ""
        Me.selectXingHao.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.shuliang.Text = 0
        Me.danjia.Text = 0
        Me.jine.Text = 0
        Me.beizhu.Text = ""
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        '提交数据，保存到数据库
        '首先验证日期格式，错误则设置为当天日期
        Dim sRiqi As String
        sRiqi = Me.setNian.Text & "-" & Me.setYue.Text & "-" & Me.setRi.Text
        If IsDate(sRiqi) Then

        Else
            sRiqi = Today.Date.ToShortDateString
        End If

        '保存到报表
        Dim sBianhao As String = ""
        'Dim sRiqi As String
        Dim danzhengleixing As String = ""
        Dim cangku As String = ""
        Dim sKeHu As String = ""
        Dim sxhKMDM As String = ""
        Dim sxhKMMC As String = ""
        Dim shuliang As String = 0
        Dim danjia As String = 0
        Dim jine As String = 0
        Dim zhongliang As String = 0
        Dim dunjiage As String = 0
        Dim beizhu As String = ""

        Dim sBianhaoDone As String

        Dim i As Integer
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim sValue As String = ""
        Dim sSql As String = ""
        If Me.GridView2.Rows.Count = 0 Then
            Exit Sub '没有数据直接跳过
        Else
            sBianhao = Me.setDanZhang.Text
            Dim sMaxBH As String
            Try
                sMaxBH = mdb.Reader("select top 1 bianhao from YunSaveList where bianhao like '" & sBianhao & "%' order by bianhao desc").Tables(0).Rows(0).Item(0).ToString
                sBianhao = sBianhao & ((Microsoft.VisualBasic.Strings.Right(sMaxBH, 5) + 1)).ToString.PadLeft(5, "0")
            Catch ex As Exception
                sBianhao = sBianhao & "00001"
            End Try
            sBianhaoDone = sBianhao

            sRiqi = AddYinHao(sRiqi)
            sBianhao = AddYinHao(sBianhao)

            danzhengleixing = AddYinHao(Me.setDanZhang.Text)
            cangku = AddYinHao(Me.setCangKu.Text)
            sKeHu = AddYinHao(Me.setkehu.Text)

        End If
        For i = 0 To Me.GridView2.Rows.Count - 1
            sxhKMDM = AddYinHao(Me.GridView2.Rows(i).Cells(0).Text)
            sxhKMMC = AddYinHao(Me.GridView2.Rows(i).Cells(1).Text)
            shuliang = AddDouHao(Me.GridView2.Rows(i).Cells(2).Text)
            danjia = AddDouHao(Me.GridView2.Rows(i).Cells(3).Text)
            jine = AddDouHao(Me.GridView2.Rows(i).Cells(4).Text)
            dunjiage = AddDouHao(Me.GridView2.Rows(i).Cells(6).Text)
            beizhu = AddYinHao(Me.GridView2.Rows(i).Cells(5).Text.Replace("&nbsp;", ""))
            zhongliang = AddDouHao(0)

            lururen = "'" & dsLoginInfo.Tables(0).Rows(0).Item("loginSign").ToString & "'" '最后一个参数不能再加逗号

            sValue = sValue + "("

            sValue = sValue + sRiqi & danzhengleixing & sBianhao & cangku & sKeHu & sxhKMDM & sxhKMMC & zhongliang & shuliang & danjia & jine & dunjiage & beizhu & lururen

            sValue = sValue + "),"
            'Ssql = " INSERT INTO [YunSaveList] ([riqi] ,[danzhengleixing] ,[bianhao] ,[cangku] ,[kehu],[xhKMDM] ,[xhKMMC] ,[zhongliang] ,[shuliang],[danjia] ,[jine],[dundanjia] ,[beizhu] ,[lururen]) " & _
            '"  VALUES(" & sRiqi & danzhengleixing & sBianhao & cangku & sKeHu & sxhKMDM & sxhKMMC & zhongliang & shuliang & danjia & jine & dunjiage & beizhu & lururen & ")"

        Next
        sValue = sValue.TrimEnd(",")

        sSql = " INSERT INTO [YunSaveList] ([riqi] ,[danzhengleixing] ,[bianhao] ,[cangku] ,[kehu],[xhKMDM] ,[xhKMMC] ,[zhongliang] ,[shuliang],[danjia] ,[jine],[dundanjia] ,[beizhu] ,[lururen]) values "

        sSql = sSql + sValue

        If Ssql <> "" Then
            Try
                mdb.Write(sSql)



            Catch ex As Exception
                Response.Write("<script>alert('" & ex.Message.ToString & "')</script>")
            End Try

            btnClear_Click(sender, e)
            '跳转到打印
            Dim dayin As Integer = 0
            Response.Redirect("printList.aspx?bianhao=" & sBianhaoDone)
        End If

    End Sub

    Private Function AddYinHao(ByVal s As String) As String
        Return "'" & s & "',"
    End Function

    Private Function AddDouHao(ByVal i As String) As String
        Return i.ToString & ","
    End Function

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        '清空列表
        Session.Add("shopcard", Nothing)

        Me.GridView2.DataSource = Nothing
        Me.GridView2.DataBind()
    End Sub

    Protected Sub cbKeHu_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbKeHu.CheckedChanged
        Dim mdb As New MDBReadWrite.MDBReadWrite(getConnectString)
        Dim dsKeHu As Data.DataSet
        If Me.cbKeHu.Checked Then
            dsKeHu = mdb.Reader("select distinct kmmc from KMDM where HSLBWL=1 and kmdm like '1122%' order by kmmc")
        Else
            dsKeHu = mdb.Reader("select distinct kmmc from KMDM where HSLBWL=1  order by kmmc")
        End If

        '先清空列表
        Me.setkehu.Items.Clear()

        '绑定客户
        Dim i As Integer
        Me.setkehu.Items.Add("")
        For i = 0 To dsKeHu.Tables(0).Rows.Count - 1
            Me.setkehu.Items.Add(dsKeHu.Tables(0).Rows(i).Item("kmmc").ToString)
        Next
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        e.Row.Cells(0).Visible = False
    End Sub

    Protected Sub setkehu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles setkehu.SelectedIndexChanged
        Me.btnClear_Click(sender, e)
    End Sub
End Class
