﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="savelist.aspx.vb" Inherits="ipadsave_ipadsave" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>开具单证</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%--    <link rel="Stylesheet" href="../css/ipadsave.css" type="text/css" />
--%>

    <script language="javascript" type="text/javascript">
    function OnFouceTextbox(kjid) {
                            document.getElementById(kjid).select();                            
                            return false
                            }
     function checkzhongliang() {
                           if (isFinite(document.getElementById("<%= txtZhongLiang.ClientID %>").value)==false){
                                alert('重量必须为数字，请重新输入！');
                                document.getElementById("<%= txtZhongLiang.ClientID %>").value=0;
                           }
                           document.getElementById("<%= danjia.ClientID %>").value= Math.ceil(document.getElementById("<%= txtZhongLiang.ClientID %>").value * document.getElementById("<%= txtDunJiaGe.ClientID %>").value / 1000);
                           return false
                            }                        
      function checkdunjiage() {
                           if (isFinite(document.getElementById("<%= txtDunJiaGe.ClientID %>").value)==false){
                                alert('吨价格必须为数字，请重新输入！');
                                document.getElementById("<%= txtDunJiaGe.ClientID %>").value=0;
                                return false
                           }
                           else{
                          document.getElementById("<%= danjia.ClientID %>").value= Math.ceil(document.getElementById("<%= txtZhongLiang.ClientID %>").value * document.getElementById("<%= txtDunJiaGe.ClientID %>").value / 1000);
                           }
                           
                             return false
                            }
       function checkdanjia() {
                           if (isFinite(document.getElementById("<%= danjia.ClientID %>").value)==false){
                                alert('单价必须为数字，请重新输入！');
                                document.getElementById("<%= danjia.ClientID %>").value=0;
                                document.getElementById("<%= jine.ClientID %>").value=0;
                           }
                           else {
                            document.getElementById("<%= jine.ClientID %>").value=document.getElementById("<%= shuliang.ClientID %>").value *document.getElementById("<%= danjia.ClientID %>").value ;
                        
                           }
                             return false
                            }
         function checkshuliang() {
                           if (isFinite(document.getElementById("<%= shuliang.ClientID %>").value)==false){
                                alert('数量必须为数字，请重新输入！');
                                document.getElementById("<%= shuliang.ClientID %>").value=0;
                                document.getElementById("<%= jine.ClientID %>").value=0;
                           }
                           else {
                            document.getElementById("<%= jine.ClientID %>").value=document.getElementById("<%= shuliang.ClientID %>").value *document.getElementById("<%= danjia.ClientID %>").value ;
                           }
                             return false
                            }
      function checkjine() {
                           if (isFinite(document.getElementById("<%= jine.ClientID %>").value)==false){
                                alert('金额必须为数字，请重新输入！');
                                document.getElementById("<%= jine.ClientID %>").value=0;
                           }
                             return false
                            }
    </script>

    <style type="text/css">
body {
    font-family:"Lucida Sans", Verdana, sans-serif;
}

.main img {
    width:100%;
}

h1{
    font-size:1.625em;
}

h2{
    font-size:1.375em;
    margin:0;
}

h3{
font-size:1.125em;
margin:0;
text-align: center;
}

p{
margin:4%;
}
.header {
    padding:1.0121457489878542510121457489879%;
    background-color:#f1f1f1;
    border:1px solid #e9e9e9;
}

.menuitem {
    margin:2.0661157024793388429752066115702%;
    margin-left:0;
    margin-top:0;
    padding:2.0661157024793388429752066115702%;
    border-bottom:1px solid #e9e9e9;
    cursor:pointer;
}

.main {
    padding:2.0661157024793388429752066115702%;
}

.right {
    padding:2.0661157024793388429752066115702%;
   
}

.footer {
    padding:1.0121457489878542510121457489879%;
    text-align:center;
    background-color:#f1f1f1;
    
    font-size:0.625em;
}

.gridcontainer {
	width:100%;
}

.gridwrapper {
	overflow:hidden;
}

.gridbox {
    margin-bottom:2.0242914979757085020242914979757%;
    
    float:left;
}

.gridheader {
    width:100%;
}

.gridmenu {
    width:23.481781376518218623481781376518%;
}

.gridmain {
    width:48.987854251012145748987854251012%;
}

.gridright {
    width:23.481781376518218623481781376518%;
    margin-right:0;
    margin-left:2%;
}

.gridfooter {
    width:100%;
    margin-bottom:0;
}

.gridselectlist{
    float: right;
    width: 45%;
}

@media only screen and (max-width: 500px) {
    .gridmenu {
        width:100%;
    }

    .menuitem {
        margin:1.0121457489878542510121457489879%;
        padding:1.0121457489878542510121457489879%;
    }

    .gridmain {
        width:100%;
    }

    .main {
        padding:1.0121457489878542510121457489879%;
    }

    .gridright {
        width:100%;
    }

    .right {
        padding:1.0121457489878542510121457489879%;
    }

    .gridbox {
        margin-right:0;
        float:left;
    }
    
    
.gridselectlist{
    width: 100%;
   position: inherit;
}

   
}

</style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="gridcontainer">
            <div class="gridwrapper">
                <div class="gridbox gridmain">
                    <div class="gridbox gridmain">
                        <div class="menuitem">
                            类型
                            <asp:TextBox Style="width: 60%;" ID="setDanZhang" runat="server"></asp:TextBox></div>
                        <div class="menuitem">
                            日期<asp:DropDownList ID="setNian" runat="server">
                            </asp:DropDownList>年<asp:DropDownList ID="setYue" runat="server">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                            </asp:DropDownList>月<asp:DropDownList ID="setRi" runat="server">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>26</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>28</asp:ListItem>
                                <asp:ListItem>29</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>31</asp:ListItem>
                            </asp:DropDownList>日</div>
                        <div class="menuitem">
                            仓库<asp:TextBox Style="width: 60%;font-size:1em; height:2em;" ID="setCangKu" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="gridbox gridmain">
                        <div class="menuitem">
                            关键字<asp:TextBox Style="width: 60%;" ID="setxinghao" runat="server"></asp:TextBox>
                        </div>
                        <div class="menuitem">
                            <asp:CheckBox ID="cbKeHu" runat="server" Checked="True" Text="仅显示客户" AutoPostBack="true" />
                            <asp:DropDownList ID="setkehu" runat="server" style="font-size:1em; height:2em;">
                            </asp:DropDownList></div>
                        <div class="menuitem">
                            <asp:CheckBox ID="cbYouKuCun" runat="server" Checked="True" Text="有库存" />
                            <asp:CheckBox ID="cbKeHuXiangGuan" runat="server" Checked="True" Text="客户相关" /></div>
                        <div>
                            <asp:Button ID="btnTongJi" runat="server" Text="统计" Style="float: right; padding: 1%;
                                font-size: 1.375em; margin-right:45%;" /></div>
                    </div>
                    <div class="gridbox gridheader">
                    <div class="main">
                       
                        <asp:GridView ID="GridView1" runat="server" CellSpacing="0" CellPadding="6" style="width:98%;">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                        </asp:GridView>
                        <%=templeteTable%>
                       
                    </div>
                </div>
                    
                </div>
                
                <div class="gridbox gridright">
                    <div class="right">
                        <h3>
                            型号：
                            <asp:Label ID="selectXingHao" runat="server"></asp:Label>
                        </h3>
                        <p>
                            重量<asp:TextBox context="noContext" Style="width: 60%;height:1.5em; font-size:1em;" ID="txtZhongLiang" runat="server" onchange="javascript:checkzhongliang();"
                                onFocus="javascript:OnFouceTextbox('txtZhongLiang');" MaxLength="5">0</asp:TextBox></p>
                        <p>
                            金额<asp:TextBox context="noContext" Style="width: 60%;height:1.5em; font-size:1em;" ID="jine" runat="server" onchange="javascript:checkjine();"
                                onFocus="javascript:OnFouceTextbox('jine');">0</asp:TextBox>
                        </p>
                        <p>
                            备注<asp:TextBox context="noContext" Style="width: 60%;height:1.5em; font-size:1em;" ID="beizhu" runat="server" onFocus="javascript:OnFouceTextbox('beizhu');"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div class="gridbox gridright">
                    <div class="right">
                        <p>
                            单价<asp:TextBox context="noContext" Style="width: 60%; height:1.5em; font-size:1.2em;" ID="danjia" runat="server" onchange="javascript:checkdanjia();"
                                onFocus="javascript:OnFouceTextbox('danjia');" AutoCompleteType="Disabled">0</asp:TextBox>
                        </p>
                        <p>
                            吨价<asp:TextBox context="noContext" Style="width: 60%;height:1.5em;font-size:1.2em;" ID="txtDunJiaGe" runat="server" onchange="javascript:checkdunjiage();"
                                onFocus="javascript:OnFouceTextbox('txtDunJiaGe');" MaxLength="5" AutoCompleteType="Disabled">0</asp:TextBox></p>
                        <p>
                            数量<asp:TextBox context="noContext"  Style="width: 60%;height:1.5em;font-size:1.2em;" ID="shuliang" runat="server" onchange="javascript:checkshuliang();"
                                onFocus="javascript:OnFouceTextbox('shuliang');" AutoCompleteType="Disabled">0</asp:TextBox>
                        </p>
                        <h2>
                            <asp:Button ID="btnOk" runat="server" Text="添加" Style="float: left; padding: 1%;
                                font-size: 1em; margin-top: 2%; margin-left:15%;" />
                            <asp:Button ID="btnCancel" runat="server" Text="取消" Style="float: right; padding: 1%;
                                font-size: 0.625em; margin-top: 2%; margin-right:5%" />
                        </h2>
                    </div>
                </div>
                <%-- <div class="gridbox gridfooter">--%>
                
                <div class="gridbox gridselectlist">
                    <table style="width: 95%;">
                        <tr>
                            <td>
                                <h3 style="float: left; padding-left: 5em;  margin-top:5%;">
                                    型号清单</h3>
                                <span>
                                    <asp:Button ID="btnSubmit" runat="server" Text="提交" Style="float: right; padding: 1%;
                                        font-size: 1.375em;" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GridView2" runat="server" Style="width: 98%;">
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2 style="float: left; padding-left: 5em; vertical-align: middle;">
                                    <a href="../Main.aspx">返回首页</a></h2>
                                <span>
                                    <asp:Button ID="btnClear" runat="server" Text="清空" Style="float: right; padding: 1%;
                                        font-size: 1.375em;" /></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <%-- </div>--%>
            </div>
        </div>
        <div style="position: fixed; bottom: 6px; right: 10px; text-align: center; font-size: 0.6em;">
            <div style="border: 1px solid red; margin: auto">
            </div>
        </div>
    </form>
</body>
</html>
