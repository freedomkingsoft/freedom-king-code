﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="dlChongZhi.aspx.vb" Inherits="dlChongZhi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="http://u1.sinaimg.cn/upload/h5/img/apple-touch-icon.png">
    <title>充值中心</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="Stylesheet" href="css/Style.css" type="text/css" />
</head>
<body style="padding-top: 50px; overflow-x: hidden;">
    <div class="fixed_topw">
        <div class="self_info">
            <img src="images/FKLogo.png" alt="logo" /></div>
        <ul>
            <li>
                <h1>
                    <a href="default.aspx">代理中心</a></h1>
            </li>
        </ul>
    </div>
    <div id="box" class="container stage-setting">
        <header class="module-topbar" id="boxId_1589690143613_1"> <a class="fl iconf iconf_navbar_back" href="javascript:window.history.go(-1);" title="返回" data-act-type="hover"> </a> <a class="fr iconf iconf_navbar_more isNew" href="javascript:;" data-node="more" title="" data-act-type="hover" data-newmsg="40" data-newfs="1" data-newwb=""> </a>
    <div class="title-group">
      <h1 class="title txt-cut">充值中心</h1>
    </div>
  </header>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_2">
            <h3 class="title mct-b txt-xs" data-node="gTitle">
            </h3>
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="boxId_1589690143613_3">
                    <a href="#" class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="mct-a ">个人资料</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span></a>
                </div>
            </div>
        </div>
        <div class="card11 card-combine" data-node="group" id="Div14">
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="Div15">
                    <div class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut" style="text-align: right">
                            <span class="mct-a "><a href="shenqing.aspx">申请成为代理商 </a></span><span class="mct-a ">
                                <a href="dlCenter.aspx">代理商管理入口</a></span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="infotishi" style="display: <%= tishiMM%>">
                            <span>
                                <%=tishiMM%>
                            </span>
                        </div>
        <form runat="server">
        <div class="card11 card-combine" data-node="group" id="Div3">
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="Div4">
                    <div class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut" style="text-align: right">
                            <span class="mct-a ">自动充值功能正在开发中，请手工扫码充值</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span>
                    </div>
                    <div class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut"   style="text-align:center" >
                            <span class="mct-a ">充值金额</span>
                            <span class="mct-a ">
                                <input name="chzhJinE" id="Text1" type="text"  /></span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                            <asp:Button ID="btnChongZhi" runat="server" Text="提交审核" />
                        </i></span>
                    </div>
                    <div class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut" style="text-align: right">
                            <span class="mct-a ">为快速审核，请多冲几分。例如充值：500.16 元</span>
                        </div>
                        <span data-node="arrow" class="plus plus-s"><i class="icon-font icon-font-arrow-right txt-s">
                        </i></span>
                    </div>
                </div>
            </div>
        </div></form>
        <div  style="text-align:center">
            <img alt="微信支付" src="images/wxskm.jpg" style="width:95%;" /></div>
        <div class="card11 card-combine" data-node="group" id="Div1">
        <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="Div7">
                    <div class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="span-table" style="width: 85%">提交记录及反馈</span></div>
                    </div>
                </div>
            </div>
            <div data-node="cardList" class="card-list">
                <div class="card card4 line-around" id="boxId_1589690143613_9">
                    <div class="layout-box" data-act-type="hover">
                        <div class="box-col txt-cut">
                            <span class="span-table" style="width: 25%">日期</span><span class="span-table" style="width: 30%">说明</span><span
                                class="span-table" style="width: 18%">金额</span><span class="span-table" style="width: 22%">审核反馈</span></div>
                    </div>
                </div>
            </div>
            <asp:Repeater ID="clientList" runat="server">
                <ItemTemplate>
                    <div data-node="cardList" class="card-list">
                        <div class="card card4 line-around" id="Div2">
                            <div class="layout-box" data-act-type="hover" style="background-color: azure">
                                <div class="box-col txt-cut">
                                    
                                     <span class="span-table" style="width: 25%">
                                        <%#Container.DataItem("shijian")%>
                                    </span><span class="span-table" style="width: 25%">
                                        <%#Container.DataItem("shuoming")%>
                                    </span><span class="span-table" style="width: 30%">
                                        <%#Container.DataItem("jiefang")%>
                                    </span><span class="span-table" style="width: 18%">
                                        <%#Container.DataItem("shenhe")%>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="card11 card-combine" data-node="group" id="boxId_1589690143613_8">
        </div>
        <div class="card card6" id="boxId_1589690143613_13">
            <a href="#top" data-act-type="hover" class=" btn-red">返回顶部 </a>
        </div>
        <div id="boxId_1589690143613_14">
        </div>
        <div class="card card6" id="Div5">
            <a href="#top" data-act-type="hover" class=" btn-red"></a>
        </div>
        <div id="Div6">
        </div>
    </div>
    <div class="footer">
            <ul>
                <li class="active"><a href="main.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\home.png" /></i></p>
                    <h4>
                        首页</h4>
                </a></li>
                <li><a href="download.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\down.png" /></i></p>
                    <h4>
                        下载</h4>
                </a></li>
                <li><a href="help.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\help.png" /></i></p>
                    <h4>
                        帮助</h4>
                </a></li>
                <li><a href="mycenter.aspx">
                    <p>
                        <i class="icons">
                            <img alt="home" src="images\mine.png" /></i></p>
                    <h4>
                        我的</h4>
                </a></li>
            </ul>
        </div>
</body>
</html>
