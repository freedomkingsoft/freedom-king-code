﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Collections
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Runtime.InteropServices

Public Class noImeFrom999
    Inherits System.Windows.Forms.Form
    '声明一些API函数 
    <DllImport("imm32.dll")> _
    Public Shared Function ImmGetContext(ByVal hwnd As IntPtr) As IntPtr
    End Function
    <DllImport("imm32.dll")> _
    Public Shared Function ImmGetOpenStatus(ByVal himc As IntPtr) As Boolean
    End Function
    <DllImport("imm32.dll")> _
    Public Shared Function ImmSetOpenStatus(ByVal himc As IntPtr, ByVal b As Boolean) As Boolean
    End Function
    <DllImport("imm32.dll")> _
    Public Shared Function ImmGetConversionStatus(ByVal himc As IntPtr, ByRef lpdw As Integer, ByRef lpdw2 As Integer) As Boolean
    End Function
    <DllImport("imm32.dll")> _
    Public Shared Function ImmSimulateHotKey(ByVal hwnd As IntPtr, ByVal lngHotkey As Integer) As Integer
    End Function
    Private Const IME_CMODE_FULLSHAPE As Integer = &H8
    Private Const IME_CHOTKEY_SHAPE_TOGGLE As Integer = &H11
    '重载Form的OnActivated 
    Protected Overloads Overrides Sub OnActivated(ByVal e As EventArgs)
        MyBase.OnActivated(e)
        Dim HIme As IntPtr = ImmGetContext(Me.Handle)
        If ImmGetOpenStatus(HIme) Then
            '如果输入法处于打开状态 
            Dim iMode As Integer = 0
            Dim iSentence As Integer = 0
            Dim bSuccess As Boolean = ImmGetConversionStatus(HIme, iMode, iSentence)
            '检索输入法信息 
            If bSuccess Then
                If (iMode And IME_CMODE_FULLSHAPE) > 0 Then
                    '如果是全角 
                    ImmSimulateHotKey(Me.Handle, IME_CHOTKEY_SHAPE_TOGGLE)
                    '转换成半角 
                End If
            End If
        End If
    End Sub
End Class

