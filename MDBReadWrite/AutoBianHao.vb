Public Class AutoBianHao

    Public Function AutoBianHao(ByVal sconnectstrng As String, ByVal sType As String, Optional ByVal bStorehouse As Boolean = True) As String
        Try
            Dim mdb As New MDBReadWrite(sconnectstrng)
            Dim sID As String
            Dim ds As DataSet
            If bStorehouse Then
                ds = mdb.Reader("select distinct serialnumber from storehouse where Cancellation = false and ( type =" & sType & ") order by serialnumber desc")
            Else
                ds = mdb.Reader("select distinct Billofdocument from [money] where cancellation = false and inorout = " & sType & " order by billofDocument desc")
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                Return ""
                Exit Function
            Else
                Dim iOldIndex As Integer
                Dim iNewIndex As Integer
                sID = ds.Tables(0).Rows(0).Item(0)
                iOldIndex = sID.Substring(sID.Length - 3, 3)

                iNewIndex = iOldIndex + 1
                sID = sID.Remove(sID.Length - 3, 3) & iNewIndex.ToString.PadLeft(3, "0")
                'sID = sID.Replace(iOldIndex.ToString, iNewIndex.ToString)
                Return sID
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString, MsgBoxStyle.Information, "��ʾ")
            Return ""
        End Try

    End Function

    Public Function AutoBianHaoGongZi(ByVal sconnectstrng As String) As String
        Try
            Dim mdb As New MDBReadWrite(sconnectstrng)
            Dim sID As String
            Dim ds As DataSet
          
            ds = mdb.Reader("select distinct serialnumber from wages where Cancellation = false  order by serialnumber desc")

            If ds.Tables(0).Rows.Count = 0 Then
                Return ""
                Exit Function
            Else
                Dim iOldIndex As Integer
                Dim iNewIndex As Integer
                sID = ds.Tables(0).Rows(0).Item(0)
                iOldIndex = sID.Substring(sID.Length - 3, 3)

                iNewIndex = iOldIndex + 1
                sID = sID.Remove(sID.Length - 3, 3) & iNewIndex.ToString.PadLeft(3, "0")
                'sID = sID.Replace(iOldIndex.ToString, iNewIndex.ToString)
                Return sID
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString, MsgBoxStyle.Information, "��ʾ")
            Return ""
        End Try
    End Function

End Class
