﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BBSGL
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.btnDel = New System.Windows.Forms.Button
        Me.btnUp = New System.Windows.Forms.Button
        Me.btnDown = New System.Windows.Forms.Button
        Me.sts = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.stsText = New System.Windows.Forms.ToolStripStatusLabel
        Me.Fkgns1 = New FKBBSGL.myBBS
        Me.sts.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(261, 363)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(87, 32)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "完　　成"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(261, 104)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(87, 38)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "新建节点"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.Location = New System.Drawing.Point(262, 168)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(87, 38)
        Me.btnDel.TabIndex = 4
        Me.btnDel.Text = "删除节点"
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Location = New System.Drawing.Point(262, 232)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(87, 38)
        Me.btnUp.TabIndex = 4
        Me.btnUp.Text = "剪切"
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'btnDown
        '
        Me.btnDown.Location = New System.Drawing.Point(261, 296)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(87, 38)
        Me.btnDown.TabIndex = 4
        Me.btnDown.Text = "粘贴"
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'sts
        '
        Me.sts.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.stsText})
        Me.sts.Location = New System.Drawing.Point(0, 413)
        Me.sts.Name = "sts"
        Me.sts.Size = New System.Drawing.Size(370, 22)
        Me.sts.SizingGrip = False
        Me.sts.TabIndex = 5
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(35, 17)
        Me.ToolStripStatusLabel1.Text = "提示:"
        '
        'stsText
        '
        Me.stsText.Name = "stsText"
        Me.stsText.Size = New System.Drawing.Size(89, 17)
        Me.stsText.Text = "你选择的是节点"
        '
        'Fkgns1
        '
        Me.Fkgns1.AllowDrop = True
        Me.Fkgns1.LabelEdit = True
        Me.Fkgns1.Location = New System.Drawing.Point(3, 94)
        Me.Fkgns1.Name = "Fkgns1"
        Me.Fkgns1.Size = New System.Drawing.Size(227, 301)
        Me.Fkgns1.TabIndex = 3
        '
        'BBSGL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 435)
        Me.Controls.Add(Me.sts)
        Me.Controls.Add(Me.btnDown)
        Me.Controls.Add(Me.btnUp)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.Fkgns1)
        Me.Controls.Add(Me.btnExit)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BBSGL"
        Me.Text = "报表树管理"
        Me.sts.ResumeLayout(False)
        Me.sts.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Fkgns1 As Global.FKBBSGL.myBBS
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents sts As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents stsText As System.Windows.Forms.ToolStripStatusLabel
End Class
