﻿Public Class BBSGL


    Private Sub GNSGL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.Fkgns1.TvShows(True)
        Me.Fkgns1.SelectedNode = Me.Fkgns1.Nodes(0)
    End Sub


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'If Me.Fkgns1.Nodes.IndexOfKey("新建节点") = -1 Then
        '    Me.Fkgns1.Nodes.Find()
        'End If
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("insert into ZDYBB ([ConText],ParentID,[Index],[isBaoBiao] ,[isMingXi],[Biao],[Lie],[MRTiaoJian],[LieSetup],[YHKJTJ],[GeShiFile],[HideLie]) values ('新建节点'," & Me.Fkgns1.SelectedNode.Name & ",9,0,0,'','','','','','','')")
        Me.Fkgns1.Nodes.Clear()
        Me.Fkgns1.TvShows(True)

    End Sub

    Private Sub Fkgns1_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles Fkgns1.AfterLabelEdit
        If e.Label = "" Then
            Exit Sub
        End If
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        mdb.Write("update ZDYBB set ConText='" & e.Label & "' where ID =" & Me.Fkgns1.SelectedNode.Name)
        'MsgBox(e.Label)
    End Sub

    Private Sub Fkgns1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles Fkgns1.AfterSelect
        If Me.Fkgns1.SelectedNode.Name = "1" Then
            Me.stsText.Text = "您选择了根节点!"
            Me.btnNew.Enabled = True
            Me.btnDel.Enabled = False
            Me.btnUp.Enabled = False
            Me.btnDown.Enabled = True
        Else
            If Me.Fkgns1.SelectedNode.Tag = False Then
                Me.stsText.Text = "您选择了一个节点"
                Me.btnNew.Enabled = True
                Me.btnDel.Enabled = True
                Me.btnUp.Enabled = True
                Me.btnDown.Enabled = True
            Else
                Me.stsText.Text = "您选择了一个功能,不能给它建立下级节点!"
                Me.btnNew.Enabled = False
                Me.btnDel.Enabled = True
                Me.btnUp.Enabled = True
                Me.btnDown.Enabled = False
            End If
        End If
    End Sub


    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click
        If IsNothing(Me.Fkgns1.SelectedNode) Then
            Exit Sub
        End If
        If Me.Fkgns1.SelectedNode.GetNodeCount(True) = 0 Then
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            If Me.Fkgns1.SelectedNode.Tag = True Then

                If MsgBox("你确定要删除选中的报表吗?", MsgBoxStyle.YesNoCancel, "提示") = MsgBoxResult.Yes Then
                    mdb.Write("delete from BaoBiaoQuanXian where YongHuQuanXian =" & Me.Fkgns1.SelectedNode.Name)
                    mdb.Write("delete from ZDYBB where ID=" & Me.Fkgns1.SelectedNode.Name)
                Else
                    Exit Sub
                End If
            Else
                mdb.Write("delete from BaoBiaoQuanXian where YongHuQuanXian=" & Me.Fkgns1.SelectedNode.Name)
                mdb.Write("delete from ZDYBB where ID=" & Me.Fkgns1.SelectedNode.Name)
            End If


            Me.Fkgns1.Nodes.Clear()
            Me.Fkgns1.TvShows(True)
        Else
            MsgBox("你选择的节点还有下级节点,无法删除!", MsgBoxStyle.Information, "提示")
        End If
    End Sub

    Dim iNodeID As Integer = 0
    Dim objNode As Windows.Forms.TreeNode
    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        If IsNothing(Me.Fkgns1.SelectedNode) Then
            Exit Sub
        End If
        If iNodeID = 0 Then
            iNodeID = Me.Fkgns1.SelectedNode.Name
            objNode = Me.Fkgns1.SelectedNode
            Me.Fkgns1.SelectedNode.Remove()
        Else
            MsgBox("您还没有将上次剪切的节点进行粘贴,无法再次剪切", MsgBoxStyle.Information, "提示")
        End If
    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        If iNodeID = 0 Then
        Else
            Me.Fkgns1.SelectedNode.Nodes.Add(objNode)
            Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
            mdb.Write("update ZDYBB set ParentID=" & Me.Fkgns1.SelectedNode.Name & " where ID=" & iNodeID)
            iNodeID = 0
            objNode = Nothing
        End If
    End Sub

End Class