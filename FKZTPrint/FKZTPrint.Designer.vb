﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKZTPrint
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.nuMaxMonth = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.nuMinMonth = New System.Windows.Forms.NumericUpDown
        Me.nuYear = New System.Windows.Forms.NumericUpDown
        Me.rbAll = New System.Windows.Forms.RadioButton
        Me.rbYi = New System.Windows.Forms.RadioButton
        Me.rbJia = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.rbYJKM = New System.Windows.Forms.RadioButton
        Me.btnAddAllKM = New System.Windows.Forms.Button
        Me.btnAddKM = New System.Windows.Forms.Button
        Me.btnDelAllKM = New System.Windows.Forms.Button
        Me.btnDelKM = New System.Windows.Forms.Button
        Me.dgvYXKM = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvKM = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GNID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.KMQM = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.btnPrintview = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnPVZFLZ = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnDaoChu = New System.Windows.Forms.Button
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.btnDaoChuZFLZ = New System.Windows.Forms.Button
        Me.btnPZFLZ = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        CType(Me.nuMaxMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuMinMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvYXKM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvKM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.nuMaxMonth)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.nuMinMonth)
        Me.GroupBox1.Controls.Add(Me.nuYear)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(544, 64)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "选择账期"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.Location = New System.Drawing.Point(485, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 19)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "月"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(356, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "月 至"
        '
        'nuMaxMonth
        '
        Me.nuMaxMonth.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.nuMaxMonth.Location = New System.Drawing.Point(420, 24)
        Me.nuMaxMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nuMaxMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nuMaxMonth.Name = "nuMaxMonth"
        Me.nuMaxMonth.Size = New System.Drawing.Size(62, 29)
        Me.nuMaxMonth.TabIndex = 0
        Me.nuMaxMonth.Value = New Decimal(New Integer() {12, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.Location = New System.Drawing.Point(46, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 19)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "账  期:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(204, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "年"
        '
        'nuMinMonth
        '
        Me.nuMinMonth.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.nuMinMonth.Location = New System.Drawing.Point(288, 24)
        Me.nuMinMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nuMinMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nuMinMonth.Name = "nuMinMonth"
        Me.nuMinMonth.Size = New System.Drawing.Size(62, 29)
        Me.nuMinMonth.TabIndex = 0
        Me.nuMinMonth.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'nuYear
        '
        Me.nuYear.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.nuYear.Location = New System.Drawing.Point(123, 24)
        Me.nuYear.Name = "nuYear"
        Me.nuYear.Size = New System.Drawing.Size(78, 29)
        Me.nuYear.TabIndex = 0
        '
        'rbAll
        '
        Me.rbAll.AutoSize = True
        Me.rbAll.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.rbAll.Location = New System.Drawing.Point(43, 20)
        Me.rbAll.Name = "rbAll"
        Me.rbAll.Size = New System.Drawing.Size(103, 23)
        Me.rbAll.TabIndex = 4
        Me.rbAll.Text = "全部科目"
        Me.rbAll.UseVisualStyleBackColor = True
        '
        'rbYi
        '
        Me.rbYi.AutoSize = True
        Me.rbYi.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.rbYi.Location = New System.Drawing.Point(356, 20)
        Me.rbYi.Name = "rbYi"
        Me.rbYi.Size = New System.Drawing.Size(160, 23)
        Me.rbYi.TabIndex = 3
        Me.rbYi.Text = "数量金额式科目"
        Me.rbYi.UseVisualStyleBackColor = True
        '
        'rbJia
        '
        Me.rbJia.AutoSize = True
        Me.rbJia.Checked = True
        Me.rbJia.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.rbJia.Location = New System.Drawing.Point(192, 20)
        Me.rbJia.Name = "rbJia"
        Me.rbJia.Size = New System.Drawing.Size(122, 23)
        Me.rbJia.TabIndex = 2
        Me.rbJia.TabStop = True
        Me.rbJia.Text = "三栏式科目"
        Me.rbJia.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbYJKM)
        Me.GroupBox2.Controls.Add(Me.rbAll)
        Me.GroupBox2.Controls.Add(Me.btnAddAllKM)
        Me.GroupBox2.Controls.Add(Me.rbYi)
        Me.GroupBox2.Controls.Add(Me.rbJia)
        Me.GroupBox2.Controls.Add(Me.btnAddKM)
        Me.GroupBox2.Controls.Add(Me.btnDelAllKM)
        Me.GroupBox2.Controls.Add(Me.btnDelKM)
        Me.GroupBox2.Controls.Add(Me.dgvYXKM)
        Me.GroupBox2.Controls.Add(Me.dgvKM)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Location = New System.Drawing.Point(19, 84)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(539, 460)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "选择科目"
        '
        'rbYJKM
        '
        Me.rbYJKM.AutoSize = True
        Me.rbYJKM.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.rbYJKM.Location = New System.Drawing.Point(204, 59)
        Me.rbYJKM.Name = "rbYJKM"
        Me.rbYJKM.Size = New System.Drawing.Size(103, 23)
        Me.rbYJKM.TabIndex = 4
        Me.rbYJKM.Text = "一级科目"
        Me.rbYJKM.UseVisualStyleBackColor = True
        '
        'btnAddAllKM
        '
        Me.btnAddAllKM.Location = New System.Drawing.Point(250, 166)
        Me.btnAddAllKM.Name = "btnAddAllKM"
        Me.btnAddAllKM.Size = New System.Drawing.Size(43, 25)
        Me.btnAddAllKM.TabIndex = 14
        Me.btnAddAllKM.Text = "|->"
        Me.btnAddAllKM.UseVisualStyleBackColor = True
        '
        'btnAddKM
        '
        Me.btnAddKM.Location = New System.Drawing.Point(250, 225)
        Me.btnAddKM.Name = "btnAddKM"
        Me.btnAddKM.Size = New System.Drawing.Size(43, 25)
        Me.btnAddKM.TabIndex = 11
        Me.btnAddKM.Text = "-->"
        Me.btnAddKM.UseVisualStyleBackColor = True
        '
        'btnDelAllKM
        '
        Me.btnDelAllKM.Location = New System.Drawing.Point(250, 357)
        Me.btnDelAllKM.Name = "btnDelAllKM"
        Me.btnDelAllKM.Size = New System.Drawing.Size(43, 25)
        Me.btnDelAllKM.TabIndex = 12
        Me.btnDelAllKM.Text = "<-|"
        Me.btnDelAllKM.UseVisualStyleBackColor = True
        '
        'btnDelKM
        '
        Me.btnDelKM.Location = New System.Drawing.Point(250, 288)
        Me.btnDelKM.Name = "btnDelKM"
        Me.btnDelKM.Size = New System.Drawing.Size(43, 25)
        Me.btnDelKM.TabIndex = 13
        Me.btnDelKM.Text = "<--"
        Me.btnDelKM.UseVisualStyleBackColor = True
        '
        'dgvYXKM
        '
        Me.dgvYXKM.AllowUserToAddRows = False
        Me.dgvYXKM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvYXKM.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.dgvYXKM.Location = New System.Drawing.Point(311, 117)
        Me.dgvYXKM.Name = "dgvYXKM"
        Me.dgvYXKM.RowHeadersVisible = False
        Me.dgvYXKM.RowTemplate.Height = 23
        Me.dgvYXKM.Size = New System.Drawing.Size(223, 325)
        Me.dgvYXKM.TabIndex = 8
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "科目代码"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "科目名称"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "KMQM"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'dgvKM
        '
        Me.dgvKM.AllowUserToAddRows = False
        Me.dgvKM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKM.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.GNID, Me.KMQM})
        Me.dgvKM.Location = New System.Drawing.Point(7, 117)
        Me.dgvKM.Name = "dgvKM"
        Me.dgvKM.RowHeadersVisible = False
        Me.dgvKM.RowTemplate.Height = 23
        Me.dgvKM.Size = New System.Drawing.Size(223, 325)
        Me.dgvKM.TabIndex = 8
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "科目代码"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'GNID
        '
        Me.GNID.HeaderText = "科目名称"
        Me.GNID.Name = "GNID"
        Me.GNID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'KMQM
        '
        Me.KMQM.HeaderText = "KMQM"
        Me.KMQM.Name = "KMQM"
        Me.KMQM.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label6.Location = New System.Drawing.Point(382, 89)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 19)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "已选科目"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label5.Location = New System.Drawing.Point(63, 90)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 19)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "可选科目"
        '
        'btnPrintview
        '
        Me.btnPrintview.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnPrintview.Location = New System.Drawing.Point(7, 28)
        Me.btnPrintview.Name = "btnPrintview"
        Me.btnPrintview.Size = New System.Drawing.Size(60, 29)
        Me.btnPrintview.TabIndex = 2
        Me.btnPrintview.Text = "预览"
        Me.btnPrintview.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(77, 28)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(60, 29)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "打印"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnClose.Location = New System.Drawing.Point(490, 574)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(77, 38)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "关闭"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnPVZFLZ
        '
        Me.btnPVZFLZ.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnPVZFLZ.Location = New System.Drawing.Point(9, 28)
        Me.btnPVZFLZ.Name = "btnPVZFLZ"
        Me.btnPVZFLZ.Size = New System.Drawing.Size(66, 29)
        Me.btnPVZFLZ.TabIndex = 2
        Me.btnPVZFLZ.Text = "预览"
        Me.btnPVZFLZ.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnDaoChu)
        Me.GroupBox3.Controls.Add(Me.btnPrintview)
        Me.GroupBox3.Controls.Add(Me.btnPrint)
        Me.GroupBox3.Location = New System.Drawing.Point(19, 554)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(222, 72)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "明细分类帐"
        '
        'btnDaoChu
        '
        Me.btnDaoChu.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnDaoChu.Location = New System.Drawing.Point(152, 27)
        Me.btnDaoChu.Name = "btnDaoChu"
        Me.btnDaoChu.Size = New System.Drawing.Size(60, 29)
        Me.btnDaoChu.TabIndex = 3
        Me.btnDaoChu.Text = "导出"
        Me.btnDaoChu.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btnDaoChuZFLZ)
        Me.GroupBox4.Controls.Add(Me.btnPZFLZ)
        Me.GroupBox4.Controls.Add(Me.btnPVZFLZ)
        Me.GroupBox4.Location = New System.Drawing.Point(256, 554)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(222, 72)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "总分类帐"
        '
        'btnDaoChuZFLZ
        '
        Me.btnDaoChuZFLZ.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnDaoChuZFLZ.Location = New System.Drawing.Point(156, 29)
        Me.btnDaoChuZFLZ.Name = "btnDaoChuZFLZ"
        Me.btnDaoChuZFLZ.Size = New System.Drawing.Size(60, 29)
        Me.btnDaoChuZFLZ.TabIndex = 4
        Me.btnDaoChuZFLZ.Text = "导出"
        Me.btnDaoChuZFLZ.UseVisualStyleBackColor = True
        '
        'btnPZFLZ
        '
        Me.btnPZFLZ.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnPZFLZ.Location = New System.Drawing.Point(83, 29)
        Me.btnPZFLZ.Name = "btnPZFLZ"
        Me.btnPZFLZ.Size = New System.Drawing.Size(66, 29)
        Me.btnPZFLZ.TabIndex = 2
        Me.btnPZFLZ.Text = "打印"
        Me.btnPZFLZ.UseVisualStyleBackColor = True
        '
        'FKZTPrint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(576, 639)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FKZTPrint"
        Me.Text = "明细分类帐打印"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nuMaxMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuMinMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvYXKM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvKM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents nuMaxMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nuMinMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents nuYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnAddKM As System.Windows.Forms.Button
    Friend WithEvents btnDelAllKM As System.Windows.Forms.Button
    Friend WithEvents btnDelKM As System.Windows.Forms.Button
    Friend WithEvents dgvKM As System.Windows.Forms.DataGridView
    Friend WithEvents btnAddAllKM As System.Windows.Forms.Button
    Friend WithEvents dgvYXKM As System.Windows.Forms.DataGridView
    Friend WithEvents btnPrintview As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GNID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KMQM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rbYi As System.Windows.Forms.RadioButton
    Friend WithEvents rbJia As System.Windows.Forms.RadioButton
    Friend WithEvents rbAll As System.Windows.Forms.RadioButton
    Friend WithEvents rbYJKM As System.Windows.Forms.RadioButton
    Friend WithEvents btnPVZFLZ As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnPZFLZ As System.Windows.Forms.Button
    Friend WithEvents btnDaoChu As System.Windows.Forms.Button
    Friend WithEvents btnDaoChuZFLZ As System.Windows.Forms.Button
End Class
