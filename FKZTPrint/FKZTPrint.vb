Public Class FKZTPrint

    '待开发
    '    1，甲乙式明细账
    '   2，改变年份，则改变科目
    '3，确定选定账期是否已结账

    Private Sub FKZTPrint_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bPrint Then
            e.Cancel = True
        End If
    End Sub

    Private Sub FKZTPrint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon

        Me.nuYear.Minimum = FKG.myselfG.dQiYongRiQi.Year
        Me.nuYear.Maximum = FKG.myselfG.NianFen
        Me.nuYear.Value = FKG.myselfG.NianFen

        FillKM(FKG.myselfG.NianFen, 2, True)
    End Sub

    Private Sub FillKM(ByVal iYear As Integer, ByVal iSL As Int16, Optional ByVal bLoad As Boolean = False)

        Me.dgvKM.Rows.Clear()
        Me.dgvYXKM.Rows.Clear()

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim sSL As String
        Select Case iSL
            Case 1
                sSL = " "
            Case 2
                sSL = " and KMDM.ZYGS='6'"
            Case 3
                sSL = " and KMDM.ZYGS='7'"
            Case 4
                Dim ds1 As DataSet
                ds1 = mdb.Reader("select distinct KMDM.KMDM,KMDM.KMMC,KMDM.KMQM from KMDM  join PZ on charindex(kmdm.KMDM,PZ.科目)=1 and KMDM.Nian=PZ.年份 and len(KMDM.KMDM)=" & CType(FKG.myselfG.sKMJS.Substring(0, 1), Integer) & " and kmdm.nian=" & iYear & " union select distinct KMDM.KMDM,KMDM.KMMC,KMDM.KMQM from KMDM  join kmye on kmdm.KMDM=kmye.kmdm and KMDM.Nian=KMYE.nian and kmdm.SFMJ='true'  and len(KMDM.KMDM)=" & CType(FKG.myselfG.sKMJS.Substring(0, 1), Integer) & " and kmdm.nian=" & iYear & " where KMYE.ncjfye<>0 union select distinct KMDM.KMDM,KMDM.KMMC,KMDM.KMQM from KMDM  join kmye on kmdm.KMDM=kmye.kmdm and KMDM.Nian=KMYE.nian and kmdm.SFMJ='true'  and len(KMDM.KMDM)=" & CType(FKG.myselfG.sKMJS.Substring(0, 1), Integer) & " and kmdm.nian=" & iYear & " where KMYE.ncdfye<>0 ")
                Dim i1 As Integer
                For i1 = 0 To ds1.Tables(0).Rows.Count - 1
                    Me.dgvKM.Rows.Add(ds1.Tables(0).Rows(i1).Item(0), ds1.Tables(0).Rows(i1).Item(1), ds1.Tables(0).Rows(i1).Item(2))
                Next
                Exit Sub
            Case Else
                sSL = " "
        End Select
        Dim ds As DataSet
        ds = mdb.Reader("select distinct KMDM.KMDM,KMDM.KMMC,KMDM.KMQM from KMDM  join PZ on kmdm.KMDM=PZ.科目 and KMDM.Nian=PZ.年份 and kmdm.nian=" & iYear & sSL & " union select distinct KMDM.KMDM,KMDM.KMMC,KMDM.KMQM from KMDM  join kmye on kmdm.KMDM=kmye.kmdm and KMDM.Nian=KMYE.nian and kmdm.SFMJ='true'  and kmdm.nian=" & iYear & sSL & " where KMYE.ncjfye<>0 union select distinct KMDM.KMDM,KMDM.KMMC,KMDM.KMQM from KMDM  join kmye on kmdm.KMDM=kmye.kmdm and KMDM.Nian=KMYE.nian and kmdm.SFMJ='true'  and kmdm.nian=" & iYear & sSL & " where KMYE.ncdfye<>0 ")

        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.dgvKM.Rows.Add(ds.Tables(0).Rows(i).Item(0), ds.Tables(0).Rows(i).Item(1), ds.Tables(0).Rows(i).Item(2))
        Next
    End Sub

    '需要判断打印的月份是否已结账，不结账不能打印

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If bPrint Then

        Else
            Me.Close()
        End If
    End Sub

    Private Sub btnAddAllKM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAllKM.Click
        Dim i As Integer
        For i = 0 To Me.dgvKM.Rows.Count - 1
            Me.dgvYXKM.Rows.Add(Me.dgvKM.Rows(i).Cells(0).Value, Me.dgvKM.Rows(i).Cells(1).Value, Me.dgvKM.Rows(i).Cells(2).Value)
        Next
        Me.dgvKM.Rows.Clear()

        Me.dgvYXKM.Sort(Me.dgvYXKM.Columns(0), ComponentModel.ListSortDirection.Ascending)
    End Sub

    Private Sub btnAddKM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddKM.Click
        If IsNothing(Me.dgvKM.CurrentRow) Then
            Exit Sub
        End If
        Me.dgvYXKM.Rows.Add(Me.dgvKM.CurrentRow.Cells(0).Value, Me.dgvKM.CurrentRow.Cells(1).Value, Me.dgvKM.CurrentRow.Cells(2).Value)

        Me.dgvKM.Rows.Remove(Me.dgvKM.CurrentRow)

        Me.dgvYXKM.Sort(Me.dgvYXKM.Columns(0), ComponentModel.ListSortDirection.Ascending)

    End Sub

    Private Sub btnDelKM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelKM.Click
        If IsNothing(Me.dgvYXKM.CurrentRow) Then
            Exit Sub
        End If

        Me.dgvKM.Rows.Add(Me.dgvYXKM.CurrentRow.Cells(0).Value, Me.dgvYXKM.CurrentRow.Cells(1).Value, Me.dgvYXKM.CurrentRow.Cells(2).Value)
        Me.dgvYXKM.Rows.Remove(Me.dgvYXKM.CurrentRow)
        Me.dgvKM.Sort(Me.dgvKM.Columns(0), ComponentModel.ListSortDirection.Ascending)
    End Sub

    Private Sub btnDelAllKM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelAllKM.Click
        Dim i As Integer
        For i = 0 To Me.dgvYXKM.Rows.Count - 1
            Me.dgvKM.Rows.Add(Me.dgvYXKM.Rows(i).Cells(0).Value, Me.dgvYXKM.Rows(i).Cells(1).Value, Me.dgvYXKM.Rows(i).Cells(2).Value)
        Next
        Me.dgvYXKM.Rows.Clear()
        Me.dgvKM.Sort(Me.dgvKM.Columns(0), ComponentModel.ListSortDirection.Ascending)
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If Me.dgvYXKM.Rows.Count = 0 Then
            Exit Sub
        End If

        '确定选定的账期是否已结账
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select JieZhang from FKZQ where ny='" & Me.nuYear.Value.ToString & Me.nuMaxMonth.Value.ToString.PadLeft(2, "0") & "'")
        If ds.Tables(0).Rows(0).Item(0) = True Then
        Else
            MsgBox("您要打印的账期中，还有月份没有结账，请先结账再打印！", MsgBoxStyle.Information, "结账提示")
            Exit Sub
        End If

        Dim th As New Threading.Thread(AddressOf Print)
        th.Start()
    End Sub

    Private Sub btnPrintview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintview.Click
        If Me.dgvYXKM.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim th As New Threading.Thread(AddressOf PrintView)
        th.Start()
    End Sub

    Dim bPrint As Boolean = False
    Private Sub PrintView()
        bPrint = True
        
        Dim i As Integer
        For i = 0 To Me.dgvYXKM.Rows.Count - 1
            Dim fkP As New FKPrn.Print("报表", 0, 1)

            If Me.rbJia.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbAll.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbYi.Checked Then
                fkP.sGeShiFile = "乙式明细分类帐_FKSYS.xls"
            Else
                fkP.sGeShiFile = "总分类帐_FKSYS.xls"
            End If

            fkP.bDeleteLast = False

            fkP.sKMDMBB = Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            fkP.sZQ = Me.nuYear.Value & " 年　" & Me.nuMinMonth.Value & " 月至 " & Me.nuMaxMonth.Value & " 月" '打印期间

            fkP.dvBaoBiao = FillDG(Me.dgvYXKM.Rows(i).Cells(0).Value, Me.nuYear.Value, Me.nuMinMonth.Value, Me.nuMaxMonth.Value, False, True, False)

            fkP.PrintPreview()
        Next
        bPrint = False
    End Sub

    Private Sub Print()
        bPrint = True

        Dim i As Integer
        For i = 0 To Me.dgvYXKM.Rows.Count - 1
            Dim fkP As New FKPrn.Print("报表", 0, 1)

            If Me.rbJia.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbAll.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbYi.Checked Then
                fkP.sGeShiFile = "乙式明细分类帐_FKSYS.xls"
            Else
                fkP.sGeShiFile = "总分类帐_FKSYS.xls"
            End If

            fkP.bDeleteLast = False

            fkP.sKMDMBB = Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            fkP.sZQ = Me.nuYear.Value & " 年　" & Me.nuMinMonth.Value & " 月至 " & Me.nuMaxMonth.Value & " 月" '打印期间

            fkP.dvBaoBiao = FillDG(Me.dgvYXKM.Rows(i).Cells(0).Value, Me.nuYear.Value, Me.nuMinMonth.Value, Me.nuMaxMonth.Value, False, True, False)

            fkP.Print()
        Next
        bPrint = False
    End Sub

    Dim indexHJ(24) As Integer
    Dim iHj As Integer = 0

    Public Function FillDG(ByVal sKMDM As String, ByVal iNian As Integer, ByVal iBYue As Integer, ByVal iEYue As Integer, Optional ByVal bAll As Boolean = True, Optional ByVal bSL As Boolean = False, Optional ByVal bFromTscKMDM As Boolean = False) As DataView
        'sKMDMMX = sKMDM

        'iCNian = iNian
        'iCYue = iBYue
        'iCYue2 = iEYue
        ''iCCangKuID = iCangkuID
        iHj = 0
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        'If bKM Then
        '    Dim sKMQM As String
        '    sKMQM = mdb.Reader("select KMQM from KMDM where KMDM = '" & sKMDM & "' and Nian=" & iNian).Tables(0).Rows(0).Item(0)

        '    Me.lblKM.Text = "科目: " & sKMDM & "  " & sKMQM
        '    Me.Label2.Text = "公司: " & FKG.myselfG.QiYeMingCheng
        '    Me.Label3.Text = "日期: " & iNian & "年" & iBYue & "月 -- " & iEYue & "月"

        '    Me.Label4.Text = "操作员: " & FKG.myselfG.YongHu
        '    Me.Label5.Text = "时间:　" & Today.ToShortDateString
        'End If

        Dim sCase As String
        If bAll Then
            sCase = ""
        Else
            sCase = " and 记帐 <> ''"
        End If

        Dim sSQL As String
        Dim dtAll As New DataTable
        Dim dsQC As New DataSet

        Dim dr As DataRow
        Dim iNianLJ(9), iYueHJ(9) As Double
        Dim i As Integer
        For i = 0 To 9
            iNianLJ(i) = 0
            iYueHJ(i) = 0
        Next

        Dim iYue As Int16
        Try
            mdb.oleConnect.Open()
        Catch ex As Exception
            MsgBox(ex.ToString)
            mdb.oleConnect.Close()
            Return Nothing
            Exit Function
        End Try
        Dim iStartYue As Integer
        If iNian = FKG.myselfG.dQiYongRiQi.Year Then
            iStartYue = FKG.myselfG.dQiYongRiQi.Month
        Else
            iStartYue = 1
        End If

        For iYue = iStartYue To iEYue
            Dim ds As New DataSet
            '取出凭证明细
            'sSQL = "SELECT 月份 AS 月, day(日期) AS 日, 凭证号, 摘要, 借方数量, IIF(借方数量=0,0,借方金额/借方数量) AS 借方单价, 借方金额, 贷方数量,  iif(贷方数量=0,0,贷方金额/贷方数量) AS 贷方单价,贷方金额,'' as [借/贷],1.11 as 结余数量,1.11 as 结余单价,1.11 as 结余金额 FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' and 月份=" & iYue & sCase
            sSQL = "SELECT 月份 AS 月, day(日期) AS 日, 凭证号, 摘要, sum(借方数量) as 借方数量, case when  sum(借方数量)=0 then 0 else sum(借方金额)/sum(借方数量) end  AS 借方单价, sum(借方金额) as 借方金额, sum(贷方数量) as 贷方数量,  case when  sum(贷方数量)=0 then 0 else sum(贷方金额)/sum(贷方数量) end  AS 贷方单价,sum(贷方金额) as 贷方金额,'' as [借/贷],1.11 as 结余数量,1.11 as 结余单价,1.11 as 结余金额 FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' and 月份=" & iYue & sCase & " group by 月份,日期,凭证号,摘要"

            Try
                mdb.oleCommand.CommandText = sSQL

                mdb.oleDataAdapter.Fill(ds)
            Catch ex As Exception
                MsgBox(ex.ToString)
                mdb.oleConnect.Close()
                Return Nothing
                Exit Function
            End Try

            'ds = mdb.Reader(sSQL)
            dtAll.Merge(ds.Tables(0))

            '取出年初结余作为月初余额
            If iYue = iStartYue Then
                If iYue = 1 Then
                    'sSQL = "select isnull(年初借方数量,0) as 借方数量, iif(借方数量=0,0,借方金额/借方数量) AS 借方单价, isnull(年初借方余额,0) as 借方金额, isnull(年初贷方数量,0) as 贷方数量, iif(贷方数量=0,0,贷方金额/贷方数量) AS 贷方单价, isnull(年初贷方余额,0) as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"
                    'sSQL = "select isnull(年初借方数量,0) as 借方数量, case when  借方数量=0 then 0 else 借方金额/借方数量 end  AS 借方单价, isnull(年初借方余额,0) as 借方金额, isnull(年初贷方数量,0) as 贷方数量, case when  贷方数量=0 then 0 else 贷方金额/贷方数量 end  AS 贷方单价, isnull(年初贷方余额,0) as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"

                    sSQL = "select isNull( ncjfsl , 0 ) as 借方数量, case when  isNull( ncjfsl , 0 )=0 then 0 else ncjfye /ncjfsl end  AS 借方单价, isNull( ncjfye, 0 ) as 借方金额, isNull( ncdfsl, 0 ) as 贷方数量, case when  isNull( ncdfsl, 0 )=0 then 0 else isNull( ncdfye, 0 )/isNull( ncdfsl, 0 ) end  AS 贷方单价, isNull( ncdfye, 0 ) as 贷方金额 from KMYE  where NIAN=" & iNian & " and KMDM='" & sKMDM & "'"
                Else

                    'sSQL = "select isnull(年初借方数量+[" & iYue - 1 & "月借方累计发生数量],0) as 借方数量, iif(借方数量=0,0,借方金额/借方数量) AS 借方单价, isnull(年初借方余额+[" & iYue - 1 & "月借方累计发生额],0) as 借方金额,isnull(年初贷方数量+[" & iYue - 1 & "月贷方累计发生数量],0) as 贷方数量, iif(贷方数量=0,0,贷方金额/贷方数量) AS 贷方单价, isnull(年初贷方余额+[" & iYue - 1 & "月贷方累计发生额],0) as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"
                    'sSQL = "select isnull(年初借方数量+[" & iYue - 1 & "月借方累计发生数量],0) as 借方数量, case when  借方数量=0 then 0 else 借方金额/借方数量 end  AS 借方单价, isnull(年初借方余额+[" & iYue - 1 & "月借方累计发生额],0) as 借方金额,isnull(年初贷方数量+[" & iYue - 1 & "月贷方累计发生数量],0) as 贷方数量, case when  贷方数量=0 then 0 else 贷方金额/贷方数量 end  AS 贷方单价, isnull(年初贷方余额+[" & iYue - 1 & "月贷方累计发生额],0) as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"

                    sSQL = "select isNull( ncjfsl , 0 ) as 借方数量, case when  isNull( ncjfsl , 0 )=0 then 0 else ncjfye /ncjfsl end  AS 借方单价, isNull( ncjfye, 0 ) as 借方金额, isNull( ncdfsl, 0 ) as 贷方数量, case when  isNull( ncdfsl, 0 )=0 then 0 else isNull( ncdfye, 0 )/isNull( ncdfsl, 0 ) end  AS 贷方单价, isNull( ncdfye, 0 ) as 贷方金额 from KMYE  where NIAN=" & iNian & " and KMDM='" & sKMDM & "'"
                End If

                Try
                    'mdb.oleConnect.Open()
                    mdb.oleCommand.CommandText = sSQL

                    mdb.oleDataAdapter.Fill(dsQC)
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    mdb.oleConnect.Close()
                    Return Nothing
                    Exit Function
                End Try
                'dsQC = mdb.Reader(sSQL)

                Dim iQC(5) As Double
                If IsNothing(dsQC) OrElse dsQC.Tables(0).Rows.Count = 0 Then
                    iQC(0) = 0
                    iQC(1) = 0
                    iQC(2) = 0
                    iQC(3) = 0
                    iQC(4) = 0
                    iQC(5) = 0
                Else
                    iQC(0) = dsQC.Tables(0).Rows(0).Item(0)
                    iQC(1) = dsQC.Tables(0).Rows(0).Item(1)
                    iQC(2) = dsQC.Tables(0).Rows(0).Item(2)
                    iQC(3) = dsQC.Tables(0).Rows(0).Item(3)
                    iQC(4) = dsQC.Tables(0).Rows(0).Item(4)
                    iQC(5) = dsQC.Tables(0).Rows(0).Item(5)
                End If
                dr = dtAll.NewRow

                dr.Item(0) = iYue
                dr.Item(1) = DBNull.Value
                dr.Item(2) = DBNull.Value
                dr.Item(3) = "月初余额"
                dr.Item(4) = DBNull.Value
                dr.Item(5) = DBNull.Value
                dr.Item(6) = DBNull.Value
                dr.Item(7) = DBNull.Value
                dr.Item(8) = DBNull.Value
                dr.Item(9) = DBNull.Value

                If iQC(2) > iQC(5) Then
                    dr.Item(10) = "借"
                    dr.Item(11) = iQC(0) - iQC(3)
                ElseIf iQC(2) < iQC(5) Then
                    dr.Item(10) = "贷"
                    dr.Item(11) = iQC(3) - iQC(0)
                Else
                    dr.Item(10) = "平"
                    dr.Item(11) = iQC(0) - iQC(3)
                End If
                dr.Item(13) = System.Math.Abs(iQC(2) - iQC(5))
                If dr.Item(11) = 0 Then
                    dr.Item(12) = 0
                Else
                    dr.Item(12) = Math.Abs(dr.Item(13) / dr.Item(11))
                End If
                '以上为月初余额
                dtAll.Rows.InsertAt(dr, 0)
                indexHJ(iHj) = 0
                iHj = 1

            End If

            '设置本月合计行
            sSQL = "SELECT  isnull(sum(借方数量),0) , isnull(sum(借方金额),0) ,  isnull(sum(贷方数量),0) , isnull(sum(贷方金额),0)  FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' and 月份=" & iYue & sCase
            dsQC.Clear()
            dsQC = New DataSet
            Try
                'mdb.oleConnect.Open()
                mdb.oleCommand.CommandText = sSQL

                mdb.oleDataAdapter.Fill(dsQC)
            Catch ex As Exception
                MsgBox(ex.ToString)
                mdb.oleConnect.Close()
                Return Nothing
                Exit Function
            End Try
            'dsQC = mdb.Reader(sSQL)
            If IsNothing(dsQC) OrElse dsQC.Tables(0).Rows.Count = 0 Then
                iYueHJ(0) = 0
                iYueHJ(1) = 0
                iYueHJ(2) = 0
                iYueHJ(3) = 0
            Else
                iYueHJ(0) = dsQC.Tables(0).Rows(0).Item(0)
                iNianLJ(0) = iNianLJ(0) + iYueHJ(0)
                iYueHJ(1) = dsQC.Tables(0).Rows(0).Item(1)
                iNianLJ(1) = iNianLJ(1) + iYueHJ(1)
                iYueHJ(2) = dsQC.Tables(0).Rows(0).Item(2)
                iNianLJ(2) = iNianLJ(2) + iYueHJ(2)
                iYueHJ(3) = dsQC.Tables(0).Rows(0).Item(3)
                iNianLJ(3) = iNianLJ(3) + iYueHJ(3)
            End If
            dr = dtAll.NewRow

            dr.Item(0) = iYue
            dr.Item(1) = DBNull.Value
            dr.Item(2) = DBNull.Value
            dr.Item(3) = "本月合计"
            dr.Item(4) = iYueHJ(0)
            dr.Item(5) = DBNull.Value
            dr.Item(6) = iYueHJ(1)
            dr.Item(7) = iYueHJ(2)
            dr.Item(8) = DBNull.Value
            dr.Item(9) = iYueHJ(3)
            dtAll.Rows.Add(dr)

            indexHJ(iHj) = dtAll.Rows.Count - 1
            iHj = iHj + 1

            '本年合计
            dr = dtAll.NewRow

            dr.Item(0) = iYue
            dr.Item(1) = DBNull.Value
            dr.Item(2) = DBNull.Value
            dr.Item(3) = "本年累计"
            dr.Item(4) = iNianLJ(0)
            dr.Item(5) = DBNull.Value
            dr.Item(6) = iNianLJ(1)
            dr.Item(7) = iNianLJ(2)
            dr.Item(8) = DBNull.Value
            dr.Item(9) = iNianLJ(3)
            dtAll.Rows.Add(dr)

            indexHJ(iHj) = dtAll.Rows.Count - 1
            iHj = iHj + 1

        Next
        mdb.oleConnect.Close()

        Dim dv As DataView
        dv = dtAll.DefaultView

        '计算余额
        Dim iJYShu, iJYE As Double
        dv.RowFilter = "摘要<>'本年累计'"
        Dim n As Int16
        For n = 1 To dv.Count - 1
            If dv.Item(n).Item("摘要") = "本月合计" Then
                dv.Item(n).Item(13) = dv.Item(n - 1).Item(13)
                dv.Item(n).Item(12) = dv.Item(n - 1).Item(12)
                dv.Item(n).Item(11) = dv.Item(n - 1).Item(11)
                dv.Item(n).Item(10) = dv.Item(n - 1).Item(10)
            Else
                If dv.Item(n - 1).Item(10) = "借" Then
                    iJYE = dv.Item(n - 1).Item(13) + dv.Item(n).Item(6) - dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) + dv.Item(n).Item(4) - dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                        dv.Item(n).Item(10) = "借"
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "贷"
                        dv.Item(n).Item(13) = Math.Abs(iJYE)
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = 0
                        dv.Item(n).Item(11) = iJYShu

                    End If

                ElseIf dv.Item(n - 1).Item(10) = "贷" Then
                    iJYE = dv.Item(n - 1).Item(13) - dv.Item(n).Item(6) + dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) - dv.Item(n).Item(4) + dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                        dv.Item(n).Item(10) = "贷"
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "借"
                        dv.Item(n).Item(13) = Math.Abs(iJYE)
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = 0
                        dv.Item(n).Item(11) = iJYShu

                    End If

                ElseIf dv.Item(n - 1).Item(10) = "平" Then
                    iJYE = dv.Item(n - 1).Item(13) + dv.Item(n).Item(6) - dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) + dv.Item(n).Item(4) - dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(10) = "借"
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "贷"
                        dv.Item(n).Item(13) = -iJYE
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu

                    End If
                Else
                    MsgBox("wrong")
                End If

                If dv.Item(n).Item(11) = 0 Then
                    dv.Item(n).Item(12) = 0
                Else
                    dv.Item(n).Item(12) = Math.Abs(dv.Item(n).Item(13) / dv.Item(n).Item(11))
                End If
            End If
        Next

        dv.RowFilter = Nothing

        '筛选所选月份
        If iBYue > 1 Then
            dv.RowFilter = "月>=" & iBYue & " or (月=" & iBYue - 1 & " and 摘要='本月合计')"
            dv.Item(0).Item("月") = iBYue
            dv.Item(0).Item("摘要") = "月初余额"

            '清除上月的本月合计数据，否则将显示到月初余额行
            dv.Item(0).Item("借方数量") = DBNull.Value
            dv.Item(0).Item("借方单价") = DBNull.Value
            dv.Item(0).Item("借方金额") = DBNull.Value
            dv.Item(0).Item("贷方数量") = DBNull.Value
            dv.Item(0).Item("贷方单价") = DBNull.Value
            dv.Item(0).Item("贷方金额") = DBNull.Value
        End If

        '不显示0
        Dim irow, icol As Integer
        For irow = 0 To dv.Count - 1
            For icol = 4 To dv.Table.Columns.Count - 1
                If Not icol = 10 AndAlso Not dv.Item(irow).Item(icol).ToString = "" AndAlso dv.Item(irow).Item(icol) = 0 Then
                    dv.Item(irow).Item(icol) = DBNull.Value
                End If
            Next
        Next


        Return dv
        'If bFromTscKMDM Then
        '    Exit Sub
        '    '如果是通过下拉框改变的科目代码，则不进行列的调整
        'End If

        'If bSL Then
        'Else
        '    Me.dgMX.Columns(4).Visible = False
        '    Me.dgMX.Columns(5).Visible = False
        '    Me.dgMX.Columns(7).Visible = False
        '    Me.dgMX.Columns(8).Visible = False
        '    Me.dgMX.Columns(11).Visible = False
        '    Me.dgMX.Columns(12).Visible = False
        'End If
        'MsgBox((Now - t).ToString)

        'TiaoZhengLie()
    End Function


    Private Sub nuYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nuYear.ValueChanged
        If rbJia.Checked Then
            FillKM(Me.nuYear.Value, 2)
        ElseIf rbAll.Checked Then
            FillKM(Me.nuYear.Value, 1)
        ElseIf rbYi.Checked Then
            FillKM(Me.nuYear.Value, 3)
        Else
            FillKM(Me.nuYear.Value, 4)
        End If
    End Sub

    Private Sub rbAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        If Me.rbAll.Checked Then
            FillKM(Me.nuYear.Value, 1)
        End If
    End Sub

    Private Sub rbJia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbJia.CheckedChanged
        If rbJia.Checked Then
            FillKM(Me.nuYear.Value, 2)
        End If
    End Sub

    Private Sub rbYi_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbYi.CheckedChanged
        If rbYi.Checked Then
            FillKM(Me.nuYear.Value, 3)
        End If
    End Sub

    Private Sub rbYJKM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbYJKM.CheckedChanged
        If rbYJKM.Checked Then
            FillKM(Me.nuYear.Value, 4)
        End If
    End Sub

    Private Sub PVZFLZ()
        bPrint = True

        Dim i As Integer
        For i = 0 To Me.dgvYXKM.Rows.Count - 1
            Dim fkP As New FKPrn.Print("报表", 0, 1)

            If Me.rbJia.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbAll.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbYi.Checked Then
                fkP.sGeShiFile = "乙式明细分类帐_FKSYS.xls"
            Else
                fkP.sGeShiFile = "总分类帐_FKSYS.xls"
            End If

            fkP.bDeleteLast = False

            fkP.sKMDMBB = Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            fkP.sZQ = Me.nuYear.Value & " 年　" & Me.nuMinMonth.Value & " 月至 " & Me.nuMaxMonth.Value & " 月" '打印期间

            Dim dv As New DataView
            dv = FillDG(Me.dgvYXKM.Rows(i).Cells(0).Value, Me.nuYear.Value, Me.nuMinMonth.Value, Me.nuMaxMonth.Value, False, True, False)

            dv.RowFilter = "月>=" & Me.nuMinMonth.Value & " and  (摘要='本月合计' or 摘要='本年累计' or 摘要='月初余额')"

            fkP.dvBaoBiao = dv

            fkP.PrintPreview()
        Next
        bPrint = False
    End Sub

    Private Sub btnPVZFLZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPVZFLZ.Click
        If Me.dgvYXKM.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim th As New Threading.Thread(AddressOf PVZFLZ)
        th.Start()
    End Sub

    Private Sub PZFLZ()
        bPrint = True

        Dim i As Integer
        For i = 0 To Me.dgvYXKM.Rows.Count - 1
            Dim fkP As New FKPrn.Print("报表", 0, 1)

            If Me.rbJia.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbAll.Checked Then
                fkP.sGeShiFile = "甲式明细分类帐_FKSYS.xls"
            ElseIf Me.rbYi.Checked Then
                fkP.sGeShiFile = "乙式明细分类帐_FKSYS.xls"
            Else
                fkP.sGeShiFile = "总分类帐_FKSYS.xls"
            End If

            fkP.bDeleteLast = False

            fkP.sKMDMBB = Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            fkP.sZQ = Me.nuYear.Value & " 年　" & Me.nuMinMonth.Value & " 月至 " & Me.nuMaxMonth.Value & " 月" '打印期间

            Dim dv As New DataView
            dv = FillDG(Me.dgvYXKM.Rows(i).Cells(0).Value, Me.nuYear.Value, Me.nuMinMonth.Value, Me.nuMaxMonth.Value, False, True, False)

            dv.RowFilter = "月>=" & Me.nuMinMonth.Value & " and  (摘要='本月合计' or 摘要='本年累计' or 摘要='月初余额')"

            fkP.dvBaoBiao = dv

            fkP.Print()
        Next
        bPrint = False
    End Sub

    Private Sub btnPZFLZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPZFLZ.Click
        If Me.dgvYXKM.Rows.Count = 0 Then
            Exit Sub
        End If

        '确定选定的账期是否已结账
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select JieZhang from FKZQ where ny='" & Me.nuYear.Value.ToString & Me.nuMaxMonth.Value.ToString.PadLeft(2, "0") & "'")
        If ds.Tables(0).Rows(0).Item(0) = True Then
        Else
            MsgBox("您要打印的账期中，还有月份没有结账，请先结账再打印！", MsgBoxStyle.Information, "结账提示")
            Exit Sub
        End If

        Dim th As New Threading.Thread(AddressOf PZFLZ)
        th.Start()
    End Sub


    Private Sub nuMinMonth_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nuMinMonth.ValueChanged
        Me.nuMaxMonth.Minimum = Me.nuMinMonth.Value
    End Sub

    '以下为导出电子账册
    Private Sub DaoChuMX()
        bPrint = True

        Dim sGeShiFile As String
        If Me.rbJia.Checked Then
            sGeShiFile = "甲式明细分类帐_FKSYS.xls"
        ElseIf Me.rbAll.Checked Then
            sGeShiFile = "甲式明细分类帐_FKSYS.xls"
        ElseIf Me.rbYi.Checked Then
            sGeShiFile = "乙式明细分类帐_FKSYS.xls"
        Else
            sGeShiFile = "总分类帐_FKSYS.xls"
        End If

        'Dim sSaveFile As String
        Dim iFileNumber As Integer = 1
        
        openExcel(sGeShiFile)


        Dim i As Integer
        Dim iRow, iCol As Integer
        Dim iStartRow, iStartCol As Integer
        Try
            iStartRow = eSheet.Cells(1, 1).value
            iStartCol = eSheet.Cells(1, 2).value
            If IsNumeric(iStartRow) = False Then
                iStartRow = 5
            End If
            If IsNumeric(iStartCol) = False Then
                iStartCol = 2
            End If
        Catch ex As Exception
            iStartRow = 5
            iStartCol = 2
        End Try
        
        Dim dvZhang As DataView
        For i = 0 To Me.dgvYXKM.Rows.Count - 1
            If eBook.Sheets.Count > 250 Then
                eSheet = eBook.Sheets(1)
                eSheet.Activate()
                eSheet.Cells.Clear()
                eApp.displayalerts = False
                eSheet.Delete()
                eApp.displayalerts = True

                eBook.SaveAs(sSaveFile & "_" & iFileNumber.ToString)
                iFileNumber = iFileNumber + 1
                '                closeExcel()

                openExcel(sGeShiFile)
            End If
            eSheet = eBook.Sheets(1)
            eSheet.Activate()
            eSheet.Copy(eBook.Sheets(1))
            eSheet.Name = Me.dgvYXKM.Rows(i).Cells(0).Value & "_" & Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名


            eSheet.Cells(1, 13) = Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            eSheet.Cells(1, 14) = Me.nuYear.Value & " 年　" & Me.nuMinMonth.Value & " 月至 " & Me.nuMaxMonth.Value & " 月" '打印期间

            dvZhang = FillDG(Me.dgvYXKM.Rows(i).Cells(0).Value, Me.nuYear.Value, Me.nuMinMonth.Value, Me.nuMaxMonth.Value, False, True, False)


            For iRow = 0 To dvZhang.Count - 1
                For iCol = 0 To dvZhang.Table.Columns.Count - 1
                    eSheet.Cells(iRow + iStartRow, iCol + iStartCol) = dvZhang.Item(iRow).Item(iCol)
                Next
            Next
            eSheet.PageSetup.RightFooter = Me.dgvYXKM.Rows(i).Cells(0).Value & "_" & Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            eSheet.PageSetup.LeftFooter = "单位： " & FKG.myselfG.QiYeMingCheng

        Next

        i = eBook.Sheets.Count

        eSheet = eBook.Sheets(1)
        eSheet.Activate()
        eSheet.Cells.Clear()
        eApp.displayalerts = False
        eSheet.Delete()
        eApp.displayalerts = True

        i = eBook.Sheets.Count

        eBook.SaveAs(sSaveFile & "_" & iFileNumber.ToString)
        closeExcel()
        'eApp.Visible = True
        bPrint = False
        MsgBox("导出成功！", MsgBoxStyle.Information, "自由王")
    End Sub

    Dim sSaveFile As String = ""
    Private Sub btnDaoChu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoChu.Click
        If Me.dgvYXKM.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim dlg As New System.Windows.Forms.SaveFileDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sSaveFile = dlg.FileName
        Else
            Exit Sub
        End If


        Dim th As New Threading.Thread(AddressOf DaoChuMX)
        th.Start()

        System.Windows.Forms.MessageBox.Show("导出过程可能需要较长时间，请耐心等待。直到出现导出成功提示", "提示", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information, Windows.Forms.MessageBoxDefaultButton.Button1)

    End Sub

    Dim eApptype As Type = Type.GetTypeFromProgID("Excel.Application")
    'Dim eApp As New Excel.Application
    Dim eApp As Object = Activator.CreateInstance(eApptype)

    Dim eBook As Microsoft.Office.Interop.Excel.Workbook
    Dim eSheet As Microsoft.Office.Interop.Excel.Worksheet

    Public Sub openExcel(ByVal myExcelFileName As String)
        If IsNothing(eApp) Then
            eApp = Activator.CreateInstance(eApptype)
        End If

        eApp.Workbooks.Open(My.Application.Info.DirectoryPath & "\Templates\" & myExcelFileName, , True)
        eBook = eApp.ActiveWorkbook
        eSheet = eBook.Sheets(1)
        eApp.Visible = False
    End Sub

    Public Sub closeExcel()

        eBook.Close(False)
        eApp.Quit()

        System.Runtime.InteropServices.Marshal.ReleaseComObject(eSheet)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eBook)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(eApp)

        eSheet = Nothing
        eBook = Nothing
        eApp = Nothing

        System.GC.Collect()
    End Sub

  
    Private Sub btnDaoChuZFLZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoChuZFLZ.Click
        If Me.dgvYXKM.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim dlg As New System.Windows.Forms.SaveFileDialog
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            sSaveFile = dlg.FileName
        Else
            Exit Sub
        End If

        Dim th As New Threading.Thread(AddressOf DaoChuZFLZ)
        th.Start()


        System.Windows.Forms.MessageBox.Show("导出过程可能需要较长时间，请耐心等待。直到出现导出成功提示", "提示", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information, Windows.Forms.MessageBoxDefaultButton.Button1)

    End Sub


    '以下为导出电子账册
    Private Sub DaoChuZFLZ()
        bPrint = True

        Dim sGeShiFile As String
        If Me.rbJia.Checked Then
            sGeShiFile = "甲式明细分类帐_FKSYS.xls"
        ElseIf Me.rbAll.Checked Then
            sGeShiFile = "甲式明细分类帐_FKSYS.xls"
        ElseIf Me.rbYi.Checked Then
            sGeShiFile = "乙式明细分类帐_FKSYS.xls"
        Else
            sGeShiFile = "总分类帐_FKSYS.xls"
        End If

        'Dim sSaveFile As String
        Dim iFileNumber As Integer = 1

        openExcel(sGeShiFile)


        Dim i As Integer
        Dim iRow, iCol As Integer
        Dim iStartRow, iStartCol As Integer
        Try
            iStartRow = eSheet.Cells(1, 1).value
            iStartCol = eSheet.Cells(1, 2).value
            If IsNumeric(iStartRow) = False Then
                iStartRow = 5
            End If
            If IsNumeric(iStartCol) = False Then
                iStartCol = 2
            End If
        Catch ex As Exception
            iStartRow = 5
            iStartCol = 2
        End Try

        Dim dvZhang As DataView
        For i = 0 To Me.dgvYXKM.Rows.Count - 1
            If eBook.Sheets.Count > 3 Then
                eSheet = eBook.Sheets(1)
                eSheet.Activate()
                eSheet.Cells.Clear()
                eSheet.Delete()
                eBook.SaveAs(sSaveFile & "_" & iFileNumber.ToString)
                iFileNumber = iFileNumber + 1
                '                closeExcel()

                openExcel(sGeShiFile)
            End If
            eSheet = eBook.Sheets(1)
            eSheet.Activate()
            eSheet.Copy(eBook.Sheets(1))
            eSheet.Name = Me.dgvYXKM.Rows(i).Cells(0).Value & "_" & Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名


            eSheet.Cells(1, 13) = Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            eSheet.Cells(1, 14) = Me.nuYear.Value & " 年　" & Me.nuMinMonth.Value & " 月至 " & Me.nuMaxMonth.Value & " 月" '打印期间

            dvZhang = FillDG(Me.dgvYXKM.Rows(i).Cells(0).Value, Me.nuYear.Value, Me.nuMinMonth.Value, Me.nuMaxMonth.Value, False, True, False)

            dvZhang.RowFilter = "月>=" & Me.nuMinMonth.Value & " and  (摘要='本月合计' or 摘要='本年累计' or 摘要='月初余额')"

            For iRow = 0 To dvZhang.Count - 1
                For iCol = 0 To dvZhang.Table.Columns.Count - 1
                    eSheet.Cells(iRow + iStartRow, iCol + iStartCol) = dvZhang.Item(iRow).Item(iCol)
                Next
            Next
            eSheet.PageSetup.RightFooter = Me.dgvYXKM.Rows(i).Cells(0).Value & "_" & Me.dgvYXKM.Rows(i).Cells(2).Value '科目全名
            eSheet.PageSetup.LeftFooter = "单位： " & FKG.myselfG.QiYeMingCheng

        Next
        eSheet = eBook.Sheets(1)
        eSheet.Activate()
        eSheet.Cells.Clear()
        eSheet.Delete()
        eBook.SaveAs(sSaveFile & "_" & iFileNumber.ToString)
        closeExcel()
        'eApp.Visible = True
        bPrint = False
        MsgBox("导出成功！", MsgBoxStyle.Information, "自由王")
    End Sub

End Class