﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKWLKHDZH
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dtJZDate = New System.Windows.Forms.DateTimePicker
        Me.dtQSDate = New System.Windows.Forms.DateTimePicker
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.bcWLBH = New System.Windows.Forms.TextBox
        Me.bcRQ = New System.Windows.Forms.DateTimePicker
        Me.bcSM = New System.Windows.Forms.TextBox
        Me.bcGXBH = New System.Windows.Forms.TextBox
        Me.bcYE = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.bcYEFX = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.SCSFBH = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.SCGXBH = New System.Windows.Forms.TextBox
        Me.SCYEFX = New System.Windows.Forms.TextBox
        Me.SCYE = New System.Windows.Forms.TextBox
        Me.SCRQ = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.SCSM = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnDaoChu = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnJiLu = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnSelect = New System.Windows.Forms.Button
        Me.cbWLKH = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.dgvMXZH = New System.Windows.Forms.DataGridView
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.dgvYE = New System.Windows.Forms.DataGridView
        Me.dgvWL = New System.Windows.Forms.DataGridView
        Me.dgvXiangxi = New System.Windows.Forms.DataGridView
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.dgvCGye = New System.Windows.Forms.DataGridView
        Me.dgvFKxx = New System.Windows.Forms.DataGridView
        Me.dgvCGxx = New System.Windows.Forms.DataGridView
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvMXZH, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvYE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvWL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvXiangxi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvCGye, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvFKxx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCGxx, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 293.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(875, 644)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dtJZDate)
        Me.Panel1.Controls.Add(Me.dtQSDate)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Controls.Add(Me.btnDaoChu)
        Me.Panel1.Controls.Add(Me.btnPrint)
        Me.Panel1.Controls.Add(Me.btnJiLu)
        Me.Panel1.Controls.Add(Me.btnOK)
        Me.Panel1.Controls.Add(Me.btnSelect)
        Me.Panel1.Controls.Add(Me.cbWLKH)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(23, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(837, 287)
        Me.Panel1.TabIndex = 0
        '
        'dtJZDate
        '
        Me.dtJZDate.Location = New System.Drawing.Point(450, 147)
        Me.dtJZDate.Name = "dtJZDate"
        Me.dtJZDate.Size = New System.Drawing.Size(107, 21)
        Me.dtJZDate.TabIndex = 5
        '
        'dtQSDate
        '
        Me.dtQSDate.Location = New System.Drawing.Point(182, 147)
        Me.dtQSDate.Name = "dtQSDate"
        Me.dtQSDate.Size = New System.Drawing.Size(107, 21)
        Me.dtQSDate.TabIndex = 4
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.bcWLBH)
        Me.GroupBox2.Controls.Add(Me.bcRQ)
        Me.GroupBox2.Controls.Add(Me.bcSM)
        Me.GroupBox2.Controls.Add(Me.bcGXBH)
        Me.GroupBox2.Controls.Add(Me.bcYE)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.bcYEFX)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Location = New System.Drawing.Point(58, 181)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(611, 91)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "本次对账"
        '
        'bcWLBH
        '
        Me.bcWLBH.Location = New System.Drawing.Point(492, 25)
        Me.bcWLBH.Name = "bcWLBH"
        Me.bcWLBH.Size = New System.Drawing.Size(105, 21)
        Me.bcWLBH.TabIndex = 1
        '
        'bcRQ
        '
        Me.bcRQ.Enabled = False
        Me.bcRQ.Location = New System.Drawing.Point(96, 24)
        Me.bcRQ.Name = "bcRQ"
        Me.bcRQ.Size = New System.Drawing.Size(94, 21)
        Me.bcRQ.TabIndex = 4
        '
        'bcSM
        '
        Me.bcSM.Location = New System.Drawing.Point(364, 56)
        Me.bcSM.Name = "bcSM"
        Me.bcSM.Size = New System.Drawing.Size(247, 21)
        Me.bcSM.TabIndex = 1
        '
        'bcGXBH
        '
        Me.bcGXBH.Location = New System.Drawing.Point(288, 25)
        Me.bcGXBH.Name = "bcGXBH"
        Me.bcGXBH.Size = New System.Drawing.Size(105, 21)
        Me.bcGXBH.TabIndex = 1
        '
        'bcYE
        '
        Me.bcYE.Location = New System.Drawing.Point(224, 56)
        Me.bcYE.Name = "bcYE"
        Me.bcYE.ReadOnly = True
        Me.bcYE.Size = New System.Drawing.Size(81, 21)
        Me.bcYE.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(409, 28)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 12)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "截至收付编号"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(329, 59)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(29, 12)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "说明"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(165, 59)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 12)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "对账余额"
        '
        'bcYEFX
        '
        Me.bcYEFX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.bcYEFX.Enabled = False
        Me.bcYEFX.FormattingEnabled = True
        Me.bcYEFX.Items.AddRange(New Object() {"清", "应收", "应付"})
        Me.bcYEFX.Location = New System.Drawing.Point(73, 56)
        Me.bcYEFX.Name = "bcYEFX"
        Me.bcYEFX.Size = New System.Drawing.Size(74, 20)
        Me.bcYEFX.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(205, 28)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(77, 12)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "截至购销编号"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(14, 59)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(53, 12)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "余额方向"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(14, 28)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(77, 12)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "对账截至日期"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SCSFBH)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.SCGXBH)
        Me.GroupBox1.Controls.Add(Me.SCYEFX)
        Me.GroupBox1.Controls.Add(Me.SCYE)
        Me.GroupBox1.Controls.Add(Me.SCRQ)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.SCSM)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(58, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(611, 91)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "上次对账情况"
        '
        'SCSFBH
        '
        Me.SCSFBH.Location = New System.Drawing.Point(483, 25)
        Me.SCSFBH.Name = "SCSFBH"
        Me.SCSFBH.ReadOnly = True
        Me.SCSFBH.Size = New System.Drawing.Size(105, 21)
        Me.SCSFBH.TabIndex = 1
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(341, 56)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(247, 21)
        Me.TextBox6.TabIndex = 1
        '
        'SCGXBH
        '
        Me.SCGXBH.Location = New System.Drawing.Point(279, 25)
        Me.SCGXBH.Name = "SCGXBH"
        Me.SCGXBH.ReadOnly = True
        Me.SCGXBH.Size = New System.Drawing.Size(105, 21)
        Me.SCGXBH.TabIndex = 1
        '
        'SCYEFX
        '
        Me.SCYEFX.Location = New System.Drawing.Point(69, 56)
        Me.SCYEFX.Name = "SCYEFX"
        Me.SCYEFX.ReadOnly = True
        Me.SCYEFX.Size = New System.Drawing.Size(63, 21)
        Me.SCYEFX.TabIndex = 1
        '
        'SCYE
        '
        Me.SCYE.Location = New System.Drawing.Point(201, 56)
        Me.SCYE.Name = "SCYE"
        Me.SCYE.ReadOnly = True
        Me.SCYE.Size = New System.Drawing.Size(81, 21)
        Me.SCYE.TabIndex = 1
        '
        'SCRQ
        '
        Me.SCRQ.Location = New System.Drawing.Point(96, 25)
        Me.SCRQ.Name = "SCRQ"
        Me.SCRQ.ReadOnly = True
        Me.SCRQ.Size = New System.Drawing.Size(81, 21)
        Me.SCRQ.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(400, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 12)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "截至收付编号"
        '
        'SCSM
        '
        Me.SCSM.AutoSize = True
        Me.SCSM.Location = New System.Drawing.Point(306, 59)
        Me.SCSM.Name = "SCSM"
        Me.SCSM.Size = New System.Drawing.Size(29, 12)
        Me.SCSM.TabIndex = 0
        Me.SCSM.Text = "说明"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(142, 59)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 12)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "对账余额"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(196, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "截至购销编号"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 12)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "余额方向"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "对账截至日期"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(694, 231)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(77, 31)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "保　存"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnDaoChu
        '
        Me.btnDaoChu.Location = New System.Drawing.Point(694, 125)
        Me.btnDaoChu.Name = "btnDaoChu"
        Me.btnDaoChu.Size = New System.Drawing.Size(77, 31)
        Me.btnDaoChu.TabIndex = 2
        Me.btnDaoChu.Text = "导  出"
        Me.btnDaoChu.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(694, 178)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(77, 31)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "预览/打印"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnJiLu
        '
        Me.btnJiLu.Location = New System.Drawing.Point(694, 72)
        Me.btnJiLu.Name = "btnJiLu"
        Me.btnJiLu.Size = New System.Drawing.Size(77, 31)
        Me.btnJiLu.TabIndex = 2
        Me.btnJiLu.Text = "对账记录"
        Me.btnJiLu.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnOK.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnOK.Location = New System.Drawing.Point(569, 142)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(100, 33)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "对　账"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnSelect
        '
        Me.btnSelect.Location = New System.Drawing.Point(694, 6)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(77, 31)
        Me.btnSelect.TabIndex = 2
        Me.btnSelect.Text = "选　择"
        Me.btnSelect.UseVisualStyleBackColor = True
        '
        'cbWLKH
        '
        Me.cbWLKH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbWLKH.FormattingEnabled = True
        Me.cbWLKH.Location = New System.Drawing.Point(179, 12)
        Me.cbWLKH.Name = "cbWLKH"
        Me.cbWLKH.Size = New System.Drawing.Size(490, 20)
        Me.cbWLKH.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(335, 151)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(101, 12)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "设定对账截至日期"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(72, 151)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(101, 12)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "设定对账起始日期"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(72, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "请选择对账客户："
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(23, 296)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(837, 345)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgvMXZH)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(821, 319)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "明细账对账方式"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgvMXZH
        '
        Me.dgvMXZH.AllowUserToAddRows = False
        Me.dgvMXZH.AllowUserToDeleteRows = False
        Me.dgvMXZH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMXZH.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMXZH.Location = New System.Drawing.Point(3, 3)
        Me.dgvMXZH.Name = "dgvMXZH"
        Me.dgvMXZH.ReadOnly = True
        Me.dgvMXZH.RowTemplate.Height = 23
        Me.dgvMXZH.Size = New System.Drawing.Size(815, 313)
        Me.dgvMXZH.TabIndex = 1
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.TextBox3)
        Me.TabPage3.Controls.Add(Me.TextBox4)
        Me.TabPage3.Controls.Add(Me.dgvYE)
        Me.TabPage3.Controls.Add(Me.dgvWL)
        Me.TabPage3.Controls.Add(Me.dgvXiangxi)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(829, 319)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "销售及收款详细信息"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'dgvYE
        '
        Me.dgvYE.AllowUserToAddRows = False
        Me.dgvYE.AllowUserToDeleteRows = False
        Me.dgvYE.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvYE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvYE.Location = New System.Drawing.Point(3, 239)
        Me.dgvYE.Name = "dgvYE"
        Me.dgvYE.ReadOnly = True
        Me.dgvYE.RowTemplate.Height = 23
        Me.dgvYE.Size = New System.Drawing.Size(770, 77)
        Me.dgvYE.TabIndex = 0
        '
        'dgvWL
        '
        Me.dgvWL.AllowUserToAddRows = False
        Me.dgvWL.AllowUserToDeleteRows = False
        Me.dgvWL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvWL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWL.Location = New System.Drawing.Point(3, 132)
        Me.dgvWL.Name = "dgvWL"
        Me.dgvWL.ReadOnly = True
        Me.dgvWL.RowTemplate.Height = 23
        Me.dgvWL.Size = New System.Drawing.Size(770, 110)
        Me.dgvWL.TabIndex = 0
        '
        'dgvXiangxi
        '
        Me.dgvXiangxi.AllowUserToAddRows = False
        Me.dgvXiangxi.AllowUserToDeleteRows = False
        Me.dgvXiangxi.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvXiangxi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvXiangxi.Location = New System.Drawing.Point(3, 3)
        Me.dgvXiangxi.Name = "dgvXiangxi"
        Me.dgvXiangxi.ReadOnly = True
        Me.dgvXiangxi.RowTemplate.Height = 23
        Me.dgvXiangxi.Size = New System.Drawing.Size(770, 131)
        Me.dgvXiangxi.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TextBox2)
        Me.TabPage1.Controls.Add(Me.TextBox1)
        Me.TabPage1.Controls.Add(Me.dgvCGye)
        Me.TabPage1.Controls.Add(Me.dgvFKxx)
        Me.TabPage1.Controls.Add(Me.dgvCGxx)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(829, 319)
        Me.TabPage1.TabIndex = 3
        Me.TabPage1.Text = "采购及付款详细信息"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'dgvCGye
        '
        Me.dgvCGye.AllowUserToAddRows = False
        Me.dgvCGye.AllowUserToDeleteRows = False
        Me.dgvCGye.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCGye.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCGye.Location = New System.Drawing.Point(1, 239)
        Me.dgvCGye.Name = "dgvCGye"
        Me.dgvCGye.ReadOnly = True
        Me.dgvCGye.RowTemplate.Height = 23
        Me.dgvCGye.Size = New System.Drawing.Size(772, 77)
        Me.dgvCGye.TabIndex = 3
        '
        'dgvFKxx
        '
        Me.dgvFKxx.AllowUserToAddRows = False
        Me.dgvFKxx.AllowUserToDeleteRows = False
        Me.dgvFKxx.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvFKxx.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFKxx.Location = New System.Drawing.Point(1, 130)
        Me.dgvFKxx.Name = "dgvFKxx"
        Me.dgvFKxx.ReadOnly = True
        Me.dgvFKxx.RowTemplate.Height = 23
        Me.dgvFKxx.Size = New System.Drawing.Size(772, 112)
        Me.dgvFKxx.TabIndex = 2
        '
        'dgvCGxx
        '
        Me.dgvCGxx.AllowUserToAddRows = False
        Me.dgvCGxx.AllowUserToDeleteRows = False
        Me.dgvCGxx.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCGxx.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCGxx.Location = New System.Drawing.Point(1, 2)
        Me.dgvCGxx.Name = "dgvCGxx"
        Me.dgvCGxx.ReadOnly = True
        Me.dgvCGxx.RowTemplate.Height = 23
        Me.dgvCGxx.Size = New System.Drawing.Size(772, 131)
        Me.dgvCGxx.TabIndex = 1
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(779, 35)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(25, 248)
        Me.TextBox1.TabIndex = 5
        Me.TextBox1.Text = "当前应收为负数则代表是应付。"
        '
        'TextBox2
        '
        Me.TextBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(804, 76)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(25, 194)
        Me.TextBox2.TabIndex = 5
        Me.TextBox2.Text = "以明细对账方式结余为准"
        '
        'TextBox3
        '
        Me.TextBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(802, 74)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(25, 194)
        Me.TextBox3.TabIndex = 7
        Me.TextBox3.Text = "以明细对账方式结余为准"
        '
        'TextBox4
        '
        Me.TextBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(777, 33)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(25, 248)
        Me.TextBox4.TabIndex = 6
        Me.TextBox4.Text = "当前应收为负数则代表是应付。"
        '
        'FKWLKHDZH
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(875, 644)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "FKWLKHDZH"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "往来客户对账"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvMXZH, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvYE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvWL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvXiangxi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvCGye, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvFKxx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCGxx, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnSelect As System.Windows.Forms.Button
    Friend WithEvents cbWLKH As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SCSM As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents SCSFBH As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents SCGXBH As System.Windows.Forms.TextBox
    Friend WithEvents SCYEFX As System.Windows.Forms.TextBox
    Friend WithEvents SCYE As System.Windows.Forms.TextBox
    Friend WithEvents SCRQ As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents bcWLBH As System.Windows.Forms.TextBox
    Friend WithEvents bcSM As System.Windows.Forms.TextBox
    Friend WithEvents bcGXBH As System.Windows.Forms.TextBox
    Friend WithEvents bcYE As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents dgvMXZH As System.Windows.Forms.DataGridView
    Friend WithEvents dtJZDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtQSDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents bcRQ As System.Windows.Forms.DateTimePicker
    Friend WithEvents bcYEFX As System.Windows.Forms.ComboBox
    Friend WithEvents btnJiLu As System.Windows.Forms.Button
    Friend WithEvents btnDaoChu As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents dgvXiangxi As System.Windows.Forms.DataGridView
    Friend WithEvents dgvWL As System.Windows.Forms.DataGridView
    Friend WithEvents dgvYE As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents dgvCGye As System.Windows.Forms.DataGridView
    Friend WithEvents dgvFKxx As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCGxx As System.Windows.Forms.DataGridView
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
End Class
