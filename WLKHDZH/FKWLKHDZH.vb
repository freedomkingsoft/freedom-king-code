﻿Public Class FKWLKHDZH

    Private Sub FKWLKHDZH_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Icon = FKG.myselfG.FKIcon
        Me.btnDaoChu.Enabled = False
        Me.btnPrint.Enabled = False
        Me.btnSave.Enabled = False

        FillWLKHDM()
        ' Me.ReportViewer1.RefreshReport()
    End Sub

    Private Sub FillWLKHDM()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select KMDM + '      ' + KMMC from KMDM where  HSLBWL='true' AND SFJY='n' AND Nian=" & FKG.myselfG.NianFen & " and SFMJ='true' order by KMDM")

        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Me.cbWLKH.Items.Add(ds.Tables(0).Rows(i).Item(0).ToString)
        Next
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim dlg As New FKKM.FKXKM(" HSLBWL='true' AND SFJY='n'", True, False)
        dlg.sKey = Me.cbWLKH.Text
        dlg.ShowDialog()
        Me.cbWLKH.Text = dlg.myKM.sKMDM & "      " & dlg.myKM.sKMMC
    End Sub

    Private Sub cbWLKH_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbWLKH.SelectedIndexChanged
        FillSCDZH()

        sWLKH = Split(Me.cbWLKH.Text, "      ").GetValue(1).ToString
        Me.btnDaoChu.Enabled = False
        Me.btnPrint.Enabled = False
        Me.btnSave.Enabled = False

        Me.dgvMXZH.DataSource = Nothing
        Me.dgvMXZH.Refresh()
    End Sub

    Private Sub FillSCDZH()
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)

        Dim dsDZH As DataSet
        dsDZH = mdb.Reader("select top 2 * from WLKHDZH where WLKHDM='" & FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text) & "' order by DZHDate Desc ")

        If dsDZH.Tables(0).Rows.Count = 0 Then
            Me.SCRQ.Text = ""
            Me.SCGXBH.Text = ""
            Me.SCSFBH.Text = ""
            Me.SCYEFX.Text = "清"
            Me.SCYE.Text = "0"
            Me.SCSM.Text = ""

            'Me.bcRQ.Text = ""
            Me.bcGXBH.Clear()
            Me.bcWLBH.Clear()
            Me.bcYEFX.Text = "清"
            Me.bcYE.Text = 0
            Me.bcSM.Clear()

            Me.btnOK.Enabled = True

            Me.dtQSDate.Value = FKG.myselfG.dQiYongRiQi
        Else
            '如果有对账纪录，则判断在对账日期之前是否有业务发生，如果有发生，则需要将最近的对账纪录删除，否则禁止使用客户对账功能。
            Dim dzhDate As Date
            dzhDate = CType(dsDZH.Tables(0).Rows(0).Item("DZHDATE"), Date).Date
            '查询对账日期之前是否有业务
            If mdb.bExsit("select * from pz where 科目='" & FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text) & "' and 日期 <= '" & dzhDate & "' and (对账时间='' or isNull(对账时间,'')='')") Then
                MsgBox("警告！该客户在上次所保存的对账日期当日或之前有增加业务，造成对账数据与明细账数据不一致，请删除最近一次对账纪录，重新对账以保证数据的正确性。建议在将对账日期或之前的所有业务录入到系统之后再进行保存对账记录操作。", MsgBoxStyle.Information, "请删除最近一次对账纪录")
                Me.btnOK.Enabled = False
                btnJiLu_Click(Nothing, Nothing)
                Exit Sub
            Else
                Me.btnOK.Enabled = True

                Me.SCRQ.Text = CType(dsDZH.Tables(0).Rows(0).Item("DZHDATE"), Date).Date
                Me.SCGXBH.Text = dsDZH.Tables(0).Rows(0).Item("JZYWBH").ToString
                Me.SCSFBH.Text = dsDZH.Tables(0).Rows(0).Item("JZWLBH").ToString
                Me.SCYEFX.Text = dsDZH.Tables(0).Rows(0).Item("YEFX").ToString
                Me.SCYE.Text = dsDZH.Tables(0).Rows(0).Item("YEJINE")
                Me.SCSM.Text = dsDZH.Tables(0).Rows(0).Item("SHUOMING").ToString

                Me.dtQSDate.Text = CType(dsDZH.Tables(0).Rows(0).Item("DZHDATE"), Date).AddDays(1)
            End If
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Me.dtJZDate.Value < Me.dtQSDate.Value Then
            MsgBox("对账期间设定错误，截止日期必须在起始日期之后，请重新设定！", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        If Me.cbWLKH.Text = "" Then
            Exit Sub
        End If

        FillMXZH()

        fillXiangXi()

        Me.btnSave.Enabled = True
        Me.btnPrint.Enabled = True
        Me.btnDaoChu.Enabled = True
    End Sub

    Private Sub fillXiangXi()
        '填充明细对账单表
        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim dsMX As New DataSet
        Dim spMX As String = "KHZZ_JINXIAOMINGXI"

        Dim sPara As String
        sPara = "KHDM," & FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text) & ";"
        sPara = sPara & "sMinDate," & Me.dtQSDate.Value & ";"
        sPara = sPara & "sMaxDate," & Me.dtJZDate.Value & ";"

        Dim iScye As Decimal = 0
        If Me.SCYEFX.Text = "应付" Then
            iScye = 0 - Me.SCYE.Text
        Else
            iScye = Me.SCYE.Text
        End If
        sPara = sPara & "ShangCiYuE," & iScye & ""

        dsMX = mdb.ReadStoredProcedure(FKG.myselfG.asasR, spMX, sPara)

        If dsMX.Tables.Count = 5 Then
            Me.dgvXiangxi.DataSource = dsMX.Tables(0).DefaultView
            Me.dgvWL.DataSource = dsMX.Tables(1).DefaultView
            'Me.dgvYE.DataSource = dsMX.Tables(2).DefaultView

            Me.dgvCGxx.DataSource = dsMX.Tables(2).DefaultView
            Me.dgvFKxx.DataSource = dsMX.Tables(3).DefaultView

            Me.dgvYE.DataSource = dsMX.Tables(4).DefaultView
            Me.dgvCGye.DataSource = dsMX.Tables(4).DefaultView


        End If


    End Sub

    Private Sub FillMXZH()
        '       FillDG(FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text), FKG.myselfG.NianFen, 1, 12)
        '       此语句有问题，限制了对账年度不能跨年度。
        FillDG(FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text), Me.dtQSDate.Value.Year, 1, 12)

        If Me.dgvMXZH.RowCount > 0 Then
            If Me.dgvMXZH.Rows(Me.dgvMXZH.RowCount - 1).Cells("借/贷").Value = "借" Then
                Me.bcYEFX.Text = "应收"
            ElseIf Me.dgvMXZH.Rows(Me.dgvMXZH.RowCount - 1).Cells("借/贷").Value = "贷" Then
                Me.bcYEFX.Text = "应付"
            Else
                Me.bcYEFX.Text = "清"
            End If

            Me.bcYE.Text = Me.dgvMXZH.Rows(Me.dgvMXZH.RowCount - 1).Cells("结余金额").Value.ToString
        Else
            Me.bcYEFX.Text = Me.SCYEFX.Text
            Me.bcYE.Text = Me.SCYE.Text
        End If

        If Me.bcYE.Text = "" OrElse Me.bcYE.Text = 0 Then
            Me.bcYE.Text = 0
            Me.bcYEFX.Text = "清"
        End If
    End Sub

    Public Sub FillDG(ByVal sKMDM As String, ByVal iNian As Integer, ByVal iBYue As Integer, ByVal iEYue As Integer, Optional ByVal bAll As Boolean = True, Optional ByVal bSL As Boolean = False)

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
       
        Dim sCase As String
        If bAll Then
            sCase = ""
        Else
            sCase = " and 记帐 <> ''"
        End If

        Dim sSQL As String
        Dim dtAll As New DataTable
        Dim dsQC As New DataSet

        Dim dr As DataRow
       
        '  Dim iYue As Int16
        Try
            mdb.oleConnect.Open()
        Catch ex As Exception
            MsgBox(ex.ToString)
            mdb.oleConnect.Close()
            Exit Sub
        End Try
        Dim iStartYue As Integer
        If iNian = FKG.myselfG.dQiYongRiQi.Year Then
            iStartYue = FKG.myselfG.dQiYongRiQi.Month
        Else
            iStartYue = 1
        End If

        '        For iYue = iStartYue To iEYue
        '不明白为什么要按月取出凭证
        For iNian = Me.dtQSDate.Value.Year To Me.dtJZDate.Value.Year
            Dim ds As New DataSet
            '取出凭证明细
            '准备取消对于摘要的显示

            'sSQL = "SELECT 月份 AS 月, day(日期) AS 日, 编号, '' as 摘要, sum(借方数量) as 借方数量, IIF(sum(借方数量)=0,0,sum(借方金额)/sum(借方数量)) AS 借方单价, sum(借方金额) as 借方金额, sum(贷方数量) as 贷方数量,  iif(sum(贷方数量)=0,0,sum(贷方金额)/sum(贷方数量)) AS 贷方单价,sum(贷方金额) as 贷方金额,'' as [借/贷],1.11 as 结余数量,1.11 as 结余单价,1.11 as 结余金额 FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' and 月份=" & iYue & sCase & " group by 月份,日期,编号"
            sSQL = "SELECT 月份 AS 月, 日期, 编号, '' as 摘要, sum(借方数量) as 借方数量, case when  sum(借方数量)=0 then 0 else sum(借方金额)/sum(借方数量) end  AS 借方单价, sum(借方金额) as 借方金额, sum(贷方数量) as 贷方数量,  case when  sum(贷方数量)=0 then 0 else sum(贷方金额)/sum(贷方数量) end  AS 贷方单价,sum(贷方金额) as 贷方金额,'' as [借/贷],1.11 as 结余数量,1.11 as 结余单价,1.11 as 结余金额 FROM pz where 年份=" & iNian & " and 科目 like '" & sKMDM & "%' " & sCase & " group by 月份,日期,编号"

            Try
                mdb.oleCommand.CommandText = sSQL

                mdb.oleDataAdapter.Fill(ds)
            Catch ex As Exception
                MsgBox(ex.ToString)
                mdb.oleConnect.Close()
                Exit Sub
            End Try

            'ds = mdb.Reader(sSQL)
            dtAll.Merge(ds.Tables(0))

            '取出年初结余作为月初余额
            If iNian = Me.dtQSDate.Value.Year Then

                If iStartYue = 1 Then
                    'sSQL = "select  isnull(年初借方数量,0)  as 借方数量, case when  借方数量=0 then 0 else 借方金额/借方数量 end  AS 借方单价,  isnull(年初借方余额,0)  as 借方金额,  isnull(年初贷方数量,0)  as 贷方数量, case when  贷方数量=0 then 0 else 贷方金额/贷方数量 end  AS 贷方单价,  isnull(年初贷方余额,0)  as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"
                    sSQL = "select isNull( ncjfsl , 0 ) as 借方数量, case when  isNull( ncjfsl , 0 )=0 then 0 else ncjfye /ncjfsl end  AS 借方单价, isNull( ncjfye, 0 ) as 借方金额, isNull( ncdfsl, 0 ) as 贷方数量, case when  isNull( ncdfsl, 0 )=0 then 0 else isNull( ncdfye, 0 )/isNull( ncdfsl, 0 ) end  AS 贷方单价, isNull( ncdfye, 0 ) as 贷方金额 from KMYE  where NIAN=" & iNian & " and KMDM='" & sKMDM & "'"

                Else
                    'sSQL = "select  isnull(年初借方数量+[" & iStartYue - 1 & "月借方累计发生数量],0)  as 借方数量, case when  借方数量=0 then 0 else 借方金额/借方数量 end  AS 借方单价,  isnull(年初借方余额+[" & iStartYue - 1 & "月借方累计发生额],0)  as 借方金额, isnull(年初贷方数量+[" & iStartYue - 1 & "月贷方累计发生数量],0)  as 贷方数量, case when  贷方数量=0 then 0 else 贷方金额/贷方数量 end  AS 贷方单价,  isnull(年初贷方余额+[" & iStartYue - 1 & "月贷方累计发生额],0)  as 贷方金额 from 科目余额表 where 年份=" & iNian & " and 科目代码='" & sKMDM & "'"
                    sSQL = "select isNull( ncjfsl , 0 ) as 借方数量, case when  isNull( ncjfsl , 0 )=0 then 0 else ncjfye /ncjfsl end  AS 借方单价, isNull( ncjfye, 0 ) as 借方金额, isNull( ncdfsl, 0 ) as 贷方数量, case when  isNull( ncdfsl, 0 )=0 then 0 else isNull( ncdfye, 0 )/isNull( ncdfsl, 0 ) end  AS 贷方单价, isNull( ncdfye, 0 ) as 贷方金额 from KMYE  where NIAN=" & iNian & " and KMDM='" & sKMDM & "'"

                End If

                Try
                    'mdb.oleConnect.Open()
                    mdb.oleCommand.CommandText = sSQL

                    mdb.oleDataAdapter.Fill(dsQC)
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    mdb.oleConnect.Close()
                    Exit Sub
                End Try
                'dsQC = mdb.Reader(sSQL)

                Dim iQC(5) As Double
                If IsNothing(dsQC) OrElse dsQC.Tables(0).Rows.Count = 0 Then
                    iQC(0) = 0
                    iQC(1) = 0
                    iQC(2) = 0
                    iQC(3) = 0
                    iQC(4) = 0
                    iQC(5) = 0
                Else
                    iQC(0) = dsQC.Tables(0).Rows(0).Item(0)
                    iQC(1) = dsQC.Tables(0).Rows(0).Item(1)
                    iQC(2) = dsQC.Tables(0).Rows(0).Item(2)
                    iQC(3) = dsQC.Tables(0).Rows(0).Item(3)
                    iQC(4) = dsQC.Tables(0).Rows(0).Item(4)
                    iQC(5) = dsQC.Tables(0).Rows(0).Item(5)
                End If
                dr = dtAll.NewRow

                dr.Item(0) = DBNull.Value
                dr.Item(1) = DBNull.Value
                dr.Item(2) = DBNull.Value
                dr.Item(3) = "月初余额"
                dr.Item(4) = DBNull.Value
                dr.Item(5) = DBNull.Value
                dr.Item(6) = DBNull.Value
                dr.Item(7) = DBNull.Value
                dr.Item(8) = DBNull.Value
                dr.Item(9) = DBNull.Value

                If iQC(2) > iQC(5) Then
                    dr.Item(10) = "借"
                    dr.Item(11) = iQC(0) - iQC(3)
                ElseIf iQC(2) < iQC(5) Then
                    dr.Item(10) = "贷"
                    dr.Item(11) = iQC(3) - iQC(0)
                Else
                    dr.Item(10) = "平"
                    dr.Item(11) = iQC(0) - iQC(3)
                End If
                dr.Item(13) = System.Math.Abs(iQC(2) - iQC(5))
                If dr.Item(11) = 0 Then
                    dr.Item(12) = 0
                Else
                    dr.Item(12) = Math.Abs(dr.Item(13) / dr.Item(11))
                End If
                '以上为月初余额
                dtAll.Rows.InsertAt(dr, 0)
                'indexHJ(iHj) = 0
                'iHj = 1

            End If

        Next
        mdb.oleConnect.Close()

        Dim dv As DataView
        dv = dtAll.DefaultView

        '计算余额
        Dim iJYShu, iJYE As Double
        dv.RowFilter = "摘要<>'本年累计'"
        Dim n As Int16
        For n = 1 To dv.Count - 1
            If dv.Item(n).Item("摘要") = "本月合计" Then
                dv.Item(n).Item(13) = dv.Item(n - 1).Item(13)
                dv.Item(n).Item(12) = dv.Item(n - 1).Item(12)
                dv.Item(n).Item(11) = dv.Item(n - 1).Item(11)
                dv.Item(n).Item(10) = dv.Item(n - 1).Item(10)
            Else
                If dv.Item(n - 1).Item(10) = "借" Then
                    iJYE = dv.Item(n - 1).Item(13) + dv.Item(n).Item(6) - dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) + dv.Item(n).Item(4) - dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                        dv.Item(n).Item(10) = "借"
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "贷"
                        dv.Item(n).Item(13) = Math.Abs(iJYE)
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = 0
                        dv.Item(n).Item(11) = iJYShu

                    End If

                ElseIf dv.Item(n - 1).Item(10) = "贷" Then
                    iJYE = dv.Item(n - 1).Item(13) - dv.Item(n).Item(6) + dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) - dv.Item(n).Item(4) + dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                        dv.Item(n).Item(10) = "贷"
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "借"
                        dv.Item(n).Item(13) = Math.Abs(iJYE)
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = 0
                        dv.Item(n).Item(11) = iJYShu

                    End If

                ElseIf dv.Item(n - 1).Item(10) = "平" Then
                    iJYE = dv.Item(n - 1).Item(13) + dv.Item(n).Item(6) - dv.Item(n).Item(9)
                    iJYShu = dv.Item(n - 1).Item(11) + dv.Item(n).Item(4) - dv.Item(n).Item(7)
                    If iJYE > 0 Then
                        dv.Item(n).Item(10) = "借"
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu
                    ElseIf iJYE < 0 Then
                        dv.Item(n).Item(10) = "贷"
                        dv.Item(n).Item(13) = -iJYE
                        dv.Item(n).Item(11) = -iJYShu

                    Else
                        dv.Item(n).Item(10) = "平"
                        dv.Item(n).Item(13) = iJYE
                        dv.Item(n).Item(11) = iJYShu

                    End If
                Else
                    MsgBox("wrong")
                End If

                If dv.Item(n).Item(11) = 0 Then
                    dv.Item(n).Item(12) = 0
                Else
                    dv.Item(n).Item(12) = Math.Abs(dv.Item(n).Item(13) / dv.Item(n).Item(11))
                End If
            End If
        Next

        dv.RowFilter = Nothing

        '不显示0
        Dim irow, icol As Integer
        For irow = 0 To dv.Count - 1
            For icol = 4 To dv.Table.Columns.Count - 2
                If Not icol = 10 AndAlso Not dv.Item(irow).Item(icol).ToString = "" AndAlso dv.Item(irow).Item(icol) = 0 Then
                    dv.Item(irow).Item(icol) = DBNull.Value
                End If
            Next
        Next

        '筛选指定期间
        '筛选条件有问题，跨年度查询时将只显示起始年份的初始余额
        If SCRQ.Text = "" Then
            If Me.dtQSDate.Value = FKG.myselfG.dQiYongRiQi Then
                dv.RowFilter = "(日期>='" & Me.dtQSDate.Value & "' and 日期<='" & Me.dtJZDate.Value & "') or  (摘要='月初余额')"
                dv.Item(0).Item(2) = "期初余额"
            Else
                dv.RowFilter = "(日期<'" & Me.dtQSDate.Value & "' ) or  (摘要='月初余额')"
                If dv.Count = 0 Then

                Else
                    dv.Item(dv.Count - 1).Item(2) = "起始余额"
                    dv.Item(dv.Count - 1).Item(3) = "起始余额"
                    dv.RowFilter = "(日期>='" & Me.dtQSDate.Value & "' and 日期<='" & Me.dtJZDate.Value & "') or  (摘要='起始余额')"
                    Try
                        dv.Item(0).Item(4) = DBNull.Value
                        dv.Item(0).Item(5) = DBNull.Value
                        dv.Item(0).Item(6) = DBNull.Value
                        dv.Item(0).Item(7) = DBNull.Value
                        dv.Item(0).Item(8) = DBNull.Value
                        dv.Item(0).Item(9) = DBNull.Value
                    Catch ex As Exception

                    End Try
                End If
            End If

        ElseIf Me.dtQSDate.Value = CType(Me.SCRQ.Text, Date).AddDays(1) Then
            dv.RowFilter = "(日期>='" & Me.dtQSDate.Value & "' and 日期<='" & Me.dtJZDate.Value & "') or  (摘要='月初余额')"
            Try
                dv.Item(0).Item(0) = CType(Me.SCRQ.Text, Date).Month
                dv.Item(0).Item(1) = CType(Me.SCRQ.Text, Date).Date

                dv.Item(0).Item(2) = "上次对账余额"

                dv.Item(0).Item(4) = DBNull.Value
                dv.Item(0).Item(5) = DBNull.Value
                dv.Item(0).Item(6) = DBNull.Value
                dv.Item(0).Item(7) = DBNull.Value
                dv.Item(0).Item(8) = DBNull.Value
                dv.Item(0).Item(9) = DBNull.Value
                If Me.SCYEFX.Text = "应收" Then
                    dv.Item(0).Item(10) = "借"
                    dv.Item(0).Item(13) = Me.SCYE.Text
                ElseIf Me.SCYEFX.Text = "应付" Then
                    dv.Item(0).Item(10) = "贷"
                    dv.Item(0).Item(13) = Me.SCYE.Text
                Else
                    dv.Item(0).Item(10) = "平"
                    dv.Item(0).Item(13) = Me.SCYE.Text  '20200225日增加该语句，无此语句将会显示异常数据
                End If
            Catch ex As Exception

            End Try
        Else '已经有过对记录，但本次对账日期与上次未衔接
            '首先取出起始日期时的余额
            dv.RowFilter = "(日期<'" & Me.dtQSDate.Value & "' ) or  (摘要='月初余额')"
            If dv.Count = 0 Then

            Else
                dv.Item(dv.Count - 1).Item(2) = "起始余额"
                dv.Item(dv.Count - 1).Item(3) = "起始余额"
                dv.RowFilter = "(日期>='" & Me.dtQSDate.Value & "' and 日期<='" & Me.dtJZDate.Value & "') or  (摘要='起始余额')"
                Try
                    dv.Item(0).Item(4) = DBNull.Value
                    dv.Item(0).Item(5) = DBNull.Value
                    dv.Item(0).Item(6) = DBNull.Value
                    dv.Item(0).Item(7) = DBNull.Value
                    dv.Item(0).Item(8) = DBNull.Value
                    dv.Item(0).Item(9) = DBNull.Value
                Catch ex As Exception

                End Try
            End If
        End If

        Me.dgvMXZH.DataSource = dv

        If bSL Then
        Else
            Me.dgvMXZH.Columns(4).Visible = False
            Me.dgvMXZH.Columns(5).Visible = False
            Me.dgvMXZH.Columns(7).Visible = False
            Me.dgvMXZH.Columns(8).Visible = False
            Me.dgvMXZH.Columns(11).Visible = False
            Me.dgvMXZH.Columns(12).Visible = False
        End If
        Me.dgvMXZH.Columns("摘要").Visible = False

        TiaoZhengLie()
    End Sub

    Private Sub setColor()
        Dim iRow As Integer
        For iRow = 0 To Me.dgvMXZH.RowCount - 1
            If IsDBNull(Me.dgvMXZH.Rows(iRow).Cells(1).Value) Then
                Me.dgvMXZH.Rows(iRow).DefaultCellStyle.BackColor = FKG.myselfG.HJColor(Drawing.Color.AliceBlue, False)
            End If
        Next
    End Sub
    Private Sub TiaoZhengLie()
        With Me.dgvMXZH

            Dim i As Integer
            For i = 0 To .Columns.Count - 1
                .Columns(i).SortMode = Windows.Forms.DataGridViewColumnSortMode.NotSortable
            Next

            .Columns(0).Width = 35
        End With

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Me.cbWLKH.Text = "" OrElse IsNothing(Me.dgvMXZH) OrElse Me.dgvMXZH.RowCount = 0 Then
            Exit Sub
        End If
        Dim DZHTIME As Date
        DZHTIME = Now

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasW)
        Try
            If CType(mdb.Reader("select dzhdate from wlkhdzh where wlkhdm='" & FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text) & "' order by dzhdate desc").Tables(0).Rows(0).Item(0), Date).Date = Today.Date Then
                mdb.Write("UPDATE WLKHDZH SET YEFX='" & Me.bcYEFX.Text & "',YEJINE=" & Me.bcYE.Text & ",SHUOMING='" & Me.bcSM.Text & "',JZYWBH='" & Me.bcGXBH.Text & "',JZWLBH='" & Me.bcWLBH.Text & "',DZHTIME='" & DZHTIME & "',备用='' WHERE WLKHDM='" & FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text) & "' AND DZHDATE = '" & Me.bcRQ.Value.Date & "'")
            Else
                mdb.Write("insert into WLKHDZH (WLKHDM,DZHDATE,YEFX,YEJINE,SHUOMING,JZYWBH,JZWLBH,DZHTIME,备用) VALUES ('" & FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text) & "','" & Me.bcRQ.Value.Date & "','" & Me.bcYEFX.Text & "'," & Me.bcYE.Text & ",'" & Me.bcSM.Text & "','" & Me.bcGXBH.Text & "','" & Me.bcWLBH.Text & "','" & DZHTIME & "','')")

                '将所涉及的单证审核，审核人为“对账”
                Dim i As Integer
                For i = 1 To Me.dgvMXZH.Rows.Count - 1
                    mdb.Write("update JiBenCaoZuo set dzhtime='" & DZHTIME & "' where Bianhao='" & Me.dgvMXZH.Rows(i).Cells("编号").Value & "'")
                    mdb.Write("update PZ set 对账时间='" & DZHTIME & "' where 编号='" & Me.dgvMXZH.Rows(i).Cells("编号").Value & "'")
                Next

            End If
        Catch ex As Exception
            mdb.Write("insert into WLKHDZH (WLKHDM,DZHDATE,YEFX,YEJINE,SHUOMING,JZYWBH,JZWLBH,DZHTIME,备用) VALUES ('" & FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text) & "','" & Me.bcRQ.Value.Date & "','" & Me.bcYEFX.Text & "'," & Me.bcYE.Text & ",'" & Me.bcSM.Text & "','" & Me.bcGXBH.Text & "','" & Me.bcWLBH.Text & "','" & Now & "','')")

            '将所涉及的单证审核，审核人为“对账”
            Dim i As Integer
            For i = 1 To Me.dgvMXZH.Rows.Count - 1
                mdb.Write("update JiBenCaoZuo set dzhtime='" & DZHTIME & "' where Bianhao='" & Me.dgvMXZH.Rows(i).Cells("编号").Value & "'")
                mdb.Write("update PZ set 对账时间='" & DZHTIME & "' where 编号='" & Me.dgvMXZH.Rows(i).Cells("编号").Value & "'")
            Next

        End Try

        MsgBox("往来客户对账信息已保存！", MsgBoxStyle.Information, "提示")
    End Sub


    Private Sub PrintView()
        'Dim Prn As New FKPrn.ImportOut
        'Prn.Importout(Me.dgvMXZH)


        ' --20220723 导出到对账表格式

        Dim fkP As New FKPrn.Print("报表", 0, 1)

        fkP.sGeShiFile = "往来客户对账表_FKSYS.xls"
        fkP.dvBaoBiao = Me.dgvMXZH.DataSource
        fkP.sKMDMBB = sWLKH
        If 1 Then
            fkP.bDeleteLast = False
        Else
            fkP.bDeleteLast = True
        End If

        fkP.PrintPreview(True)
        fkP = Nothing
        System.GC.Collect()

    End Sub

    Private Sub btnJiLu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnJiLu.Click
        Dim dlg As New WLDZHJL
        dlg.Label1.Text = "往来客户：" & Me.cbWLKH.Text
        dlg.FillJL(FKF.FKF.getKMDMfromQM(Me.cbWLKH.Text))
        dlg.ShowDialog()
    End Sub

    Dim sWLKH As String = ""
    Private Sub Print()
        Dim prn As New FKPrn.Print("报表", 0, 1)
        prn.dvBaoBiao = Me.dgvMXZH.DataSource
        prn.sGeShiFile = "往来客户对账表_FKSYS.xls"
        prn.sKMDMBB = sWLKH
        prn.bDeleteLast = False
        prn.PrintPreview()
        prn = Nothing
        System.GC.Collect()
    End Sub


    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If IsNothing(Me.dgvMXZH) OrElse Me.dgvMXZH.RowCount = 0 Then
            Exit Sub
        End If

        Dim th As New Threading.Thread(AddressOf Print)
        th.Start()
    End Sub

    Private Sub btnDaoChu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDaoChu.Click
        If IsNothing(Me.dgvMXZH) OrElse Me.dgvMXZH.RowCount = 0 Then
            Exit Sub
        End If

        Dim th As New Threading.Thread(AddressOf PrintView)
        th.Start()
    End Sub

    Private Sub dtJZDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtJZDate.ValueChanged
        Me.bcRQ.Value = dtJZDate.Value
    End Sub

    Private Sub dtQSDate_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtQSDate.Validated
        If Me.dtQSDate.Value < FKG.myselfG.dQiYongRiQi Then
            MsgBox("设定对账起始日期不能早于系统启用日期！", MsgBoxStyle.Information, "日期错误")
            Me.dtQSDate.Value = FKG.myselfG.dQiYongRiQi
        End If
    End Sub

    Private Sub dtQSDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtQSDate.ValueChanged

        If Me.SCRQ.Text = "" Then
            Exit Sub
        End If

        If Me.dtQSDate.Value = CType(Me.SCRQ.Text, Date).AddDays(1) Then
            Me.btnSave.Enabled = True
        Else
            Me.btnSave.Enabled = False
        End If


    End Sub


End Class