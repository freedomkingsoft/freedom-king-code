﻿Imports System.Windows.Forms

Public Class ZZTJ

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        '首先进行对条件的验证
        If Me.myNian.Value > FKG.myselfG.dQiYongRiQi.Year OrElse (Me.myNian.Value = FKG.myselfG.dQiYongRiQi.Year And Me.myYue.Value >= FKG.myselfG.dQiYongRiQi.Month) Then

        Else
            MsgBox("开始时间不能早于启用日期", MsgBoxStyle.Information, "提示")
            Exit Sub
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ZZTJ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.myNian.Value = FKG.myselfG.NianFen
        Me.myYue.Value = FKG.myselfG.YueFen

        Dim mdb As New MDBReadWrite.MDBReadWrite(FKG.myselfG.asasR)
        Dim ds As DataSet
        ds = mdb.Reader("select KMDM from KMDM where HSLBWW='true' AND  nian=" & FKG.myselfG.NianFen & " order by KMDM")
        Try
            Me.myMinKM.Text = ds.Tables(0).Rows(0).Item(0)
            Me.myMaxKM.Text = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item(0)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub myKMDM_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles myMinKM.KeyUp, myMaxKM.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.ActiveControl = Me.OK_Button
        End If
        bValided = False
    End Sub

    Dim bValided As Boolean
    Private Sub myKMDM_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles myMinKM.MouseDown, myMaxKM.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            getKMDM(sender)
        End If
    End Sub

    Private Sub myKMDM_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles myMinKM.Validating, myMaxKM.Validating
        If bValided Then
            Exit Sub
        End If

        If CType(sender, TextBox).Text = "" Then
        Else
            If FKF.FKF.YanZhengKMDM(CType(sender, TextBox).Text) = True Then
            Else
                getKMDM(sender)
            End If
        End If
    End Sub

    Private Sub getKMDM(ByVal sender As Object)
        Dim dlg As New FKKM.FKXKM("HSLBWW='true'", False)
        dlg.sKey = CType(sender, TextBox).Text
        dlg.ShowDialog()
        Me.Refresh()
        CType(sender, TextBox).Text = FKF.FKF.getKMDMfromQM(dlg.myKM.sKMDM)
        'Me.myKMQM.Text = dlg.myKM.sKMQM
        'If dlg.myKM.iZYGS = 6 Then
        '    Me.myXSSL.Enabled = False
        '    Me.myXSSL.Checked = False
        'Else
        '    Me.myXSSL.Enabled = True
        '    Me.myXSSL.Checked = True
        'End If
        bValided = True
    End Sub

End Class
