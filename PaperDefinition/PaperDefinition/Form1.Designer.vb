﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.selectHive = New System.Windows.Forms.ComboBox
        Me.ReadValueButton = New System.Windows.Forms.Button
        Me.showSubKey = New System.Windows.Forms.TextBox
        Me.Value = New System.Windows.Forms.TextBox
        Me.History = New System.Windows.Forms.ListBox
        Me.SetValueButton = New System.Windows.Forms.Button
        Me.CreateButton = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.zhiName = New System.Windows.Forms.TextBox
        Me.zhiHeight = New System.Windows.Forms.TextBox
        Me.zhiWidth = New System.Windows.Forms.TextBox
        Me.btnDaoRu = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'selectHive
        '
        Me.selectHive.FormattingEnabled = True
        Me.selectHive.Location = New System.Drawing.Point(76, 294)
        Me.selectHive.Name = "selectHive"
        Me.selectHive.Size = New System.Drawing.Size(332, 20)
        Me.selectHive.TabIndex = 0
        '
        'ReadValueButton
        '
        Me.ReadValueButton.Location = New System.Drawing.Point(469, 294)
        Me.ReadValueButton.Name = "ReadValueButton"
        Me.ReadValueButton.Size = New System.Drawing.Size(141, 57)
        Me.ReadValueButton.TabIndex = 1
        Me.ReadValueButton.Text = "ReadValueButton"
        Me.ReadValueButton.UseVisualStyleBackColor = True
        '
        'showSubKey
        '
        Me.showSubKey.Location = New System.Drawing.Point(71, 330)
        Me.showSubKey.Name = "showSubKey"
        Me.showSubKey.Size = New System.Drawing.Size(245, 21)
        Me.showSubKey.TabIndex = 2
        '
        'Value
        '
        Me.Value.Location = New System.Drawing.Point(49, 359)
        Me.Value.Name = "Value"
        Me.Value.Size = New System.Drawing.Size(228, 21)
        Me.Value.TabIndex = 3
        '
        'History
        '
        Me.History.FormattingEnabled = True
        Me.History.ItemHeight = 12
        Me.History.Location = New System.Drawing.Point(43, 427)
        Me.History.Name = "History"
        Me.History.Size = New System.Drawing.Size(245, 88)
        Me.History.TabIndex = 5
        '
        'SetValueButton
        '
        Me.SetValueButton.Location = New System.Drawing.Point(475, 372)
        Me.SetValueButton.Name = "SetValueButton"
        Me.SetValueButton.Size = New System.Drawing.Size(134, 57)
        Me.SetValueButton.TabIndex = 6
        Me.SetValueButton.Text = "SetValueButton"
        Me.SetValueButton.UseVisualStyleBackColor = True
        '
        'CreateButton
        '
        Me.CreateButton.Location = New System.Drawing.Point(479, 456)
        Me.CreateButton.Name = "CreateButton"
        Me.CreateButton.Size = New System.Drawing.Size(130, 46)
        Me.CreateButton.TabIndex = 7
        Me.CreateButton.Text = "CreateButton"
        Me.CreateButton.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.zhiName, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.zhiHeight, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.zhiWidth, 1, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(71, 72)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(528, 129)
        Me.TableLayoutPanel1.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "自定义纸张名称"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "自定义纸张高度(CM)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "自定义纸张宽度(CM)"
        '
        'zhiName
        '
        Me.zhiName.Location = New System.Drawing.Point(267, 3)
        Me.zhiName.Name = "zhiName"
        Me.zhiName.Size = New System.Drawing.Size(138, 21)
        Me.zhiName.TabIndex = 1
        '
        'zhiHeight
        '
        Me.zhiHeight.Location = New System.Drawing.Point(267, 46)
        Me.zhiHeight.Name = "zhiHeight"
        Me.zhiHeight.Size = New System.Drawing.Size(138, 21)
        Me.zhiHeight.TabIndex = 1
        '
        'zhiWidth
        '
        Me.zhiWidth.Location = New System.Drawing.Point(267, 89)
        Me.zhiWidth.Name = "zhiWidth"
        Me.zhiWidth.Size = New System.Drawing.Size(138, 21)
        Me.zhiWidth.TabIndex = 1
        '
        'btnDaoRu
        '
        Me.btnDaoRu.Location = New System.Drawing.Point(71, 19)
        Me.btnDaoRu.Name = "btnDaoRu"
        Me.btnDaoRu.Size = New System.Drawing.Size(161, 28)
        Me.btnDaoRu.TabIndex = 9
        Me.btnDaoRu.Text = "导入自定义纸张"
        Me.btnDaoRu.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(76, 226)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(161, 28)
        Me.btnAdd.TabIndex = 9
        Me.btnAdd.Text = "增加自定义纸张"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(649, 527)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnDaoRu)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.CreateButton)
        Me.Controls.Add(Me.SetValueButton)
        Me.Controls.Add(Me.History)
        Me.Controls.Add(Me.Value)
        Me.Controls.Add(Me.showSubKey)
        Me.Controls.Add(Me.ReadValueButton)
        Me.Controls.Add(Me.selectHive)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents selectHive As System.Windows.Forms.ComboBox
    Friend WithEvents ReadValueButton As System.Windows.Forms.Button
    Friend WithEvents showSubKey As System.Windows.Forms.TextBox
    Friend WithEvents Value As System.Windows.Forms.TextBox
    Friend WithEvents History As System.Windows.Forms.ListBox
    Friend WithEvents SetValueButton As System.Windows.Forms.Button
    Friend WithEvents CreateButton As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents zhiName As System.Windows.Forms.TextBox
    Friend WithEvents zhiHeight As System.Windows.Forms.TextBox
    Friend WithEvents zhiWidth As System.Windows.Forms.TextBox
    Friend WithEvents btnDaoRu As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button

End Class
