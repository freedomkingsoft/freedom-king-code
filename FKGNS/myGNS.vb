﻿Public Class myGNS
    'Inherits System.Windows.Forms.TreeView
    Inherits NewControls.TreeViewWithPaint

    'Dim FKG As New FKG.myselfG

    Dim ds As DataSet

    Public Sub TvShows(Optional ByVal bGL As Boolean = False)
        'Dim sConn As String
        'sConn = Global.FKG.myselfG.asas
        Dim mdb As New MDBReadWrite.MDBReadWrite(Global.FKG.myselfG.asasR)
        'If bGL Then
        '    ds = mdb.Reader("select gongnengshu.* from gongnengshu order by index")
        'Else
        ds = mdb.Reader("select gongnengshu.* from gongnengshu left join yonghuquanxian on gongnengshu.id=yonghuquanxian.yonghuquanxian where  yonghuquanxian.yonghuid=" & FKG.myselfG.YongHuID & " or isGongNeng='false' order by [index]")
        'End If
        ShowTV(0, Nothing)
        If bGL Then

        Else
            Dim iClearTimes As Integer
            For iClearTimes = 0 To 3
                ClearNullNode(Me.Nodes(0))
            Next

        End If
    End Sub

    Private Sub ClearNullNode(ByVal objNode As TreeNode)
        Dim node As TreeNode
        Dim i As Integer
        For i = objNode.Nodes.Count - 1 To 0 Step -1
            node = objNode.Nodes(i)
            If node.Tag = True Then
                Continue For
            Else
                If node.Nodes.Count = 0 Then
                    Me.Nodes.Remove(node)
                Else
                    ClearNullNode(node)
                End If
            End If
        Next
    End Sub

    Public Sub AddNode(ByVal ParentID As Integer, ByVal sConText As String)
        Me.Nodes.Insert(ParentID, sConText)
    End Sub


    '̀递归添加树的节点
    Private Sub ShowTV(ByVal ParentID As Integer, ByVal pNode As TreeNode)
        Dim Node As TreeNode
        Dim dvTree As New DataView()
        dvTree = New DataView(ds.Tables(0))
        '过滤ParentID,得到当前的所有子节点
        dvTree.RowFilter = "PARENTID = " + ParentID.ToString

        Dim Row As DataRowView
        For Each Row In dvTree
            If pNode Is Nothing Then '判断是否根节点
                '̀添加根节点
                Node = Me.Nodes.Add(Row("ID"), Row("context").ToString())
                Node.Tag = Row("isGongNeng")
                'node=me.Nodes.Add(
                'pNode.ToolTipText = Row("TiShi").ToString
                '̀再次递归
                ShowTV(Int32.Parse(Row("ID").ToString()), Node)
            Else
                '添加当前节点的子节点
                Node = pNode.Nodes.Add(Row("ID"), Row("context").ToString())
                Node.Tag = Row("isGongNeng")
                Node.ToolTipText = Row("TiShi").ToString
                '̀再次递归
                ShowTV(Int32.Parse(Row("ID").ToString()), Node)
            End If
            Node.EnsureVisible()
        Next
    End Sub


End Class
